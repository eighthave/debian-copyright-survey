Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Out Of Order
Upstream-Contact: Tim Furnish <tim@hungrysoftware.com>
Source: http://outoforder.adventuredevelopers.com

Files: *
Copyright: 2000 - 2003 Tim Furnish
License: Custom
 [...] Out Of Order [...] should be provided in the
 same packagey bundle as this text file. Or vice versa, depending on point
 of view. My point is this: Out Of Order is a comedy sci-fi adventure game,
 set somewhere in some time period, featuring a guy in silly slippers and
 written originally for Windows. Or, rather, it was originally written for
 the freeware SLUDGE adventure game engine back in the day when there was
 only a Windows version. However, the times are very much a-changin' and
 the engine has since been ported to other platforms. Hooray for people
 with more time on their hands than myself! I admire and adore them all.
 As should you, because without them you'd not be able to play this fine
 game on your non-Windows machine. Hang on, was that actually my point?
 No, wait. THIS was my point: the initial distribution of Out Of Order
 contained the Windows engine. But I hereby give permission for you to
 possess a copy of the data file and a copy of the engine for another
 operating system of your choosing and experience the joys of the former
 via the modern miracle of the latter. But please - don't charge for it.
 And don't break the package apart and distribute bits of it individually.
 And don't try to reverse engineer it. And don't prod and poke at it with a
 HEX editor to make the characters say rude things about people you know,
 or to make it look like you wrote it. Just play it and love it and get in
 a grump with it when you can't get past some part of it. [...]
 .
 - Tim Furnish, June 2010
Comment:
 The license text is from an email sent from tim@hungrysoftware.com to
 tobias.hansen@cern.ch on 28 June 2010. While it does not explicitly
 mention redistribution, the same mail also contained the following:
 .
 "Sore, by all means make the existing OOO data file available for Linux
 - with the engine if that's what you're suggesting. I must admit, despite
 having used Linux on and off for over 10 years, I've never packaged
 anything up for it for distribution so don't really have that much of a
 clue about what's involved. But yeah, go for it. The data file's freely
 distributable at the moment as part of the installer... so I guess there's
 no harm whatsoever in make it available outside of it." 

Files: debian/*
Copyright: 2011 Tobias Hansen
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

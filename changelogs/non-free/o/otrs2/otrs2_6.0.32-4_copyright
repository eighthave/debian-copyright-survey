Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Disclaimer: This package is non-free, because it heavily depends on third party
 javascript libraries, which versions could not be satisfied by the Debian
 system packages.

Files: *
Copyright: 2001-2021, OTRS AG, http://otrs.com
License: AGPL

Files: Kernel/cpan-lib/Apache2/*
Copyright: no-info-found
License: Apache-2.0

Files: Kernel/cpan-lib/Locale/*
Copyright: 2010-2014, Sullivan Beck
  2002-2009, Neil Bowers
  2001, Canon Research Centre Europe (CRE)
License: Perl

Files: Kernel/cpan-lib/Locale/Codes/Constants.pm
  Kernel/cpan-lib/Locale/Codes/Country.pm
  Kernel/cpan-lib/Locale/Codes/Currency.pm
  Kernel/cpan-lib/Locale/Codes/Language.pm
  Kernel/cpan-lib/Locale/Codes/Script.pm
Copyright: 2010-2014, Sullivan Beck
  2002-2009, Neil Bowers
  2001, Canon Research Centre Europe (CRE)
License: Perl

Files: Kernel/cpan-lib/Locale/Codes/LangExt.pm
  Kernel/cpan-lib/Locale/Codes/LangFam.pm
  Kernel/cpan-lib/Locale/Codes/LangVar.pm
Copyright: 2011-2014, Sullivan Beck
License: Perl

Files: Kernel/cpan-lib/MIME/Decoder/*
Copyright: no-info-found
License: Perl

Files: Kernel/cpan-lib/Text/Diff/Config.pm
Copyright: 2010, Shlomi Fish
License: Expat

Files: Kernel/cpan-lib/URI/_ldap.pm
  Kernel/cpan-lib/URI/ldap.pm
Copyright: 1998, 2002, 2004-2006, Graham Barr <gbarr@pobox.com>.
License: Perl

Files: bin/Cron.sh
Copyright: 2001-2012, OTRS AG, http://otrs.org/"
  2001-2012, OTRS AG, http://otrs.org/
License: AGPL

Files: scripts/backup.pl
  scripts/restore.pl
Copyright: 2001-2015, OTRS AG, http://otrs.com/";
  2001-2015, OTRS AG, http://otrs.com/
License: AGPL

License: AGPL
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU AFFERO General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 or see http://www.gnu.org/licenses/agpl.txt.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS"BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in '/usr/share/common-licenses/Apache-2.0'.

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: Perl
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
 a) the GNU General Public License as published by the Free Software
    Foundation; either version 1, or (at your option) any later
    version, or
 .
 b) the "Artistic License" which comes with Perl.

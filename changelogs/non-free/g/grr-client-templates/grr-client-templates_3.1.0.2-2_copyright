Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: grr
Source: <https://github.com/google/grr>
Comment: This package contains software that has been licensed under
 DFSG-compatible licenses. However, none of the binaries can currently
 be built on a Debian system. Programs intended to be run on Windows
 have been built using the (non-free) Microsoft compiler toolchain.
 Programs intended to be run on MacOSX have been built using the (at
 least partially non-free) Apple compiler toolchain. Programs intended
 to be run on Linux systems have been built on outdated CentOS5 and
 Ubuntu 12.10 VMs in order to achieve decent GLIBC ABI compatibility.

Files: *
Copyright: 2010-2012 Google Inc.
License: Apache-2.0

Files: executables/windows/templates/unzipsfx/*
Copyright: 1990-2009 Info-ZIP.  All rights reserved.
Comment: Source available from
 ftp://ftp.info-zip.org/pub/infozip/src/unzip60.tgz or via the unzip
 package
License: InfoZip
 For the purposes of this copyright and license, "Info-ZIP" is
 defined as the following set of individuals:
 .
 Mark Adler, John Bush, Karl Davis, Harald Denker, Jean-Michel Dubois,
 Jean-loup Gailly, Hunter Goatley, Ed Gordon, Ian Gorman, Chris
 Herborth, Dirk Haase, Greg Hartwig, Robert Heath, Jonathan Hudson,
 Paul Kienitz, David Kirschbaum, Johnny Lee, Onno van der Linden, Igor
 Mandrichenko, Steve P. Miller, Sergio Monesi, Keith Owens, George
 Petrov, Greg Roelofs, Kai Uwe Rommel, Steve Salisbury, Dave Smith,
 Steven M. Schweda, Christian Spieler, Cosmin Truta, Antoine
 Verheijen, Paul von Behren, Rich Wales, Mike White.
 .
 This software is provided "as is," without warranty of any kind,
 express or implied. In no event shall Info-ZIP or its contributors be
 held liable for any direct, indirect, incidental, special or
 consequential damages arising out of the use of or inability to use
 this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the above disclaimer and the following
 restrictions:
 .
 1. Redistributions of source code (in whole or in part) must retain
    the above copyright notice, definition, disclaimer, and this
    list of conditions.
 .
 2. Redistributions in binary form (compiled executables and
    libraries) must reproduce the above copyright notice, definition,
    disclaimer, and this list of conditions in documentation and/or
    other materials provided with the distribution. Additional
    documentation is not needed for executables where a command line
    license option provides these and a note regarding this option is
    in the executable's startup banner. The sole exception to this
    condition is redistribution of a standard UnZipSFX binary
    (including SFXWiz) as part of a self-extracting archive; that is
    permitted without inclusion of this license, as long as the normal
    SFX banner has not been removed from the binary or disabled.
 .
 3. Altered versions--including, but not limited to, ports to new
    operating systems, existing ports with new graphical interfaces,
    versions with modified or added functionality, and dynamic,
    shared, or static library versions not from Info-ZIP--must be
    plainly marked as such and must not be misrepresented as being the
    original source or, if binaries, compiled from the original
    source. Such altered versions also must not be misrepresented as
    being Info-ZIP releases--including, but not limited to, labeling
    of the altered versions with the names "Info-ZIP" (or any
    variation thereof, including, but not limited to, different
    capitalizations), "Pocket UnZip," "WiZ" or "MacZip" without the
    explicit permission of Info-ZIP. Such altered versions are further
    prohibited from misrepresentative use of the Zip-Bugs or Info-ZIP
    e-mail addresses or the Info-ZIP URL(s), such as to imply Info-ZIP
    will provide support for the altered versions.
 .
 4. Info-ZIP retains the right to use the names "Info-ZIP," "Zip,"
    "UnZip," "UnZipSFX," "WiZ," "Pocket UnZip," "Pocket Zip," and
    "MacZip" for its own source and binary releases.

Files: grr-response-templates/components/grr-chipsec-component*
Copyright: 2010-2016, Intel Corporation
Comment: https://github.com/chipsec/chipsec
License: GPL-2.0

Files: grr-response-templates/components/grr-rekall_*
Copyright: 2007-2011 Volatile Systems
           2013 Google Inc.
License: GPL-2.0+

Files: debian/*
Copyright: 2017 Hilko Bengen <bengen@debian.org>,
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: GPL-2.0
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

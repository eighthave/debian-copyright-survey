Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: bug-gawk@gnu.org
Source: https://ftp.gnu.org/gnu/gawk/

Files: doc/*
Copyright: 1989-2016, Free Software Foundation, Inc.
License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with the
 Invariant Sections being "GNU General Public License", the Front-Cover
 texts being (a) (see below), and with the Back-Cover Texts being (b)
 (see below).
 .
 a. "A GNU Manual"
 .
 b. "You have the freedom to copy and modify this GNU manual.  Buying
     copies from the FSF supports it in developing GNU and promoting
     software freedom."
 .
 On Debian systems, the full text of the GNU Free Documentation License
 version 1.3 can be found in the file `/usr/share/common-licenses/GFDL-1.3'.

Files: debian/*
Copyright: 2006, James Troup <james@nocrew.org>
           2008-2009, Arthur Loiret <aloiret@debian.org>
           2012, Jeroen Schot <schot@a-eskwadraat.nl>
           2015-2016 Hideki Yamane <henrich@debian.org>
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

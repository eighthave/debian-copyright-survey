Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GMAP
Upstream-Contact: Thomas Wu <twu@gene.com>, Colin K. Watanabe <ckw@gene.com>
Source: http://research-pub.gene.com/gmap/

Files: *
Copyright: 2011-2014 Genentech, Inc.
License: other
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Package"), to use, copy, and distribute copies of the Package,
 without modifications, provided that the above copyright notice and
 this permission notice are included in all copies or substantial
 portions of the Package.  Distribution of this Package as part of a
 commercial software product requires prior arrangement with the
 Developers.
 .
 Permission is also hereby granted, free of charge, to any person
 obtaining a copy of this Package, to modify your copy or copies of the
 Package or any portion of it, provided that you use the modified
 Package only within your corporation or organization.  Distribution of
 a modified version of this Package requires prior arrangement with the
 Developers.
 .
 Genome databases, map files, and other result files produced as output
 from software in this Package do not automatically fall under the
 copyright of this Package, but belong to whoever generated them, and
 may be distributed freely.
 .
 IN NO EVENT SHALL GENENTECH, INC. BE LIABLE TO ANY PARTY FOR DIRECT,
 INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, OR OTHER
 LIABILITY, INCLUDING LOST PROFITS, ARISING FROM THE USE OF THIS
 SOFTWARE.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 GENENTECH, INC. HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
Comment:
 Some inspiration and code was taken from
 https://github.com/lemire/FastPFor,
 covered by Apache-2.0 License

Files: src/getopt* config/libtool.m4 config/missing config/compile
 config/config.sub config/config.guess config/ltmain.sh
Copyright: 1992-2009, Free Software Foundation, Inc.
License: LGPL-2.1+
 See `/usr/share/common-licenses/LGPL-2.1'.

Files: config/ax_mpi.m4 config/ax_check_compile_flag.m4
Copyright:
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
 2008, Matteo Frigo
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
 2008, Julian C. Cummings <cummings@cacr.caltech.edu>
 2013, Michael Petch <mpetch@capp-sysware.com>
 2008, Guido U. Draheim <guidod@gmx.de>
 2011, Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3
 See `/usr/share/common-licenses/GPL-3'.

Files: config/ltsugar.m4 config/ltoptions.m4 config/ltversion.m4 aclocal.m4
Copyright:
 1996-2014, Free Software Foundation, Inc.
License: FSFUL
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

Files: config/ax_ext.m4
Copyright:
 2007, Christophe Tournayre <turn3r@users.sourceforge.net>
 2013, Michael Petch <mpetch@capp-sysware.com>
License: all-permissive
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice and
 this notice are preserved.  This file is offered as-is, without any
 warranty.

Files: config/install-sh
Copyright: 1994 X Consortium
License: X11
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall
 not be used in advertising or otherwise to promote the sale, use or other
 dealings in this Software without prior written authorization from the X
 Consortium.

Files: src/saca-k*
Copyright: 2012 Ge Nong <issng@mail.sysu.edu.cn>
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: src/fastlog.h
Copyright: 2012 Paul Mineiro <paul@mineiro.com>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that the
 following conditions are met:
 .
     * Redistributions of source code must retain the
     above copyright notice, this list of conditions and
     the following disclaimer.
 .
     * Redistributions in binary form must reproduce the
     above copyright notice, this list of conditions and
     the following disclaimer in the documentation and/or
     other materials provided with the distribution.
 .
     * Neither the name of Paul Mineiro nor the names
     of other contributors may be used to endorse or promote
     products derived from this software without specific
     prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2011 Shaun Jackman <sjackman@debian.org>
           2012-2014 Andreas Tille <tille@debian.org>
           2017 Alex Mestiashvili <alex@biotec.tu-dresden.de>
License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

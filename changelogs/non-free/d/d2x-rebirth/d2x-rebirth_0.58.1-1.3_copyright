Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: d1x-rebirth
Upstream-Contact: Zico <zico@dxx-rebirth.com>
Source: https://github.com/dxx-rebirth/dxx-rebirth

Files: *
Copyright: 1993-1999 PARALLAX SOFTWARE CORPORATION
License: DXX-license

Files: arch/carbon/SDL_main.c
       include/tracker/tracker.h
Copyright: 1997-2004 Sam Lantinga
License: LGPL-2+

Files: utilities/tex2*
       utilities/txb2*
Copyright: 1995 Bryan Aamot
License: GPL-2+

Files: debian/*
Copyright: 2012-2013 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-3+

Files: debian/patches/*
Copyright: 2012-2013 Dmitry Smirnov <onlyjob@debian.org>
License: Expat

License: DXX-license
 Original Parallax License
 -------------------------
 THE COMPUTER CODE CONTAINED HEREIN IS THE SOLE PROPERTY OF PARALLAX
 SOFTWARE CORPORATION ("PARALLAX").  PARALLAX, IN DISTRIBUTING THE CODE TO
 END-USERS, AND SUBJECT TO ALL OF THE TERMS AND CONDITIONS HEREIN, GRANTS A
 ROYALTY-FREE, PERPETUAL LICENSE TO SUCH END-USERS FOR USE BY SUCH END-USERS
 IN USING, DISPLAYING,  AND CREATING DERIVATIVE WORKS THEREOF, SO LONG AS
 SUCH USE, DISPLAY OR CREATION IS FOR NON-COMMERCIAL, ROYALTY OR REVENUE
 FREE PURPOSES.  IN NO EVENT SHALL THE END-USER USE THE COMPUTER CODE
 CONTAINED HEREIN FOR REVENUE-BEARING PURPOSES.  THE END-USER UNDERSTANDS
 AND AGREES TO THE TERMS HEREIN AND ACCEPTS THE SAME BY USE OF THIS FILE.
 COPYRIGHT 1993-1998 PARALLAX SOFTWARE CORPORATION.  ALL RIGHTS RESERVED.
 .
 We make no warranties as to the usability or correctness of this code.
 -------------------------
 .
 The D1X-Rebirth project is the combination of the original Parallax code and the
 modifications and additions made to source code.  While the original code is
 only under the Original Parallax License the D1X-Rebirth project contains original
 code that was not made by Parallax.  This ADDED and/or CHANGED code has the
 following added restrictions:
 .
  1) By using this source you are agreeing to these terms.
 .
  2) D1X-Rebirth and derived works may only be modified if ONE of the following is done:
 .
    a) The modified version is placed under this license.  The source
    code used to create the modified version is freely and publicly
    available under this license.
 .
    b) The modified version is only used by the developer.
 .
  3) D1X-REBIRTH IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License 3
 can be found in "/usr/share/common-licenses/GPL-3".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
Comment:
 This license is also known as "MIT" however FSF considers "MIT" labelling
 ambiguous and copyright-format specification recommend to label such a license
 as "Expat".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 The complete text of the GNU General Public License 3
 can be found in "/usr/share/common-licenses/LGPL-2".

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://ngspice.sourceforge.net

Files: doc/*
Copyright: 1996 The Regents of the University of California
License: SPICEDOC

Files: manual/*
Copyright: 2010, Paolo Nenzi, Holger Vogt
License: SPICEDOC

Files: src/*
Copyright: Alan Gillespie <Alan.Gillespie@analog.com>
           Al Davis <aldavis@ieee.org>
           AMAKAWA Shuhei <sa264@cam.ac.uk>
           Andrew Tuckey <Tuckey@ieee.org>
           1989 Anthony E. Parker
           Antony Wilson <wilsona@earthlink.net>
           2000 Arno Peters <A.W.Peters@ieee.org>
           Beorn Johnson <beorn@eecs.berkeley.edu>
           Cecil Aswell <aswell@netcom.com>
           Charles D.H. Williams <C.D.H.Williams@exeter.ac.uk>
           Chris Inbody <cinbody@cowtown.net>
           Daniele Foci <d.foci@ieee.ing.uniroma1.it>
           David A. Gates
           1989 David J. Skellern
           Dietmar Warning <warning@danalyse.de>
           Emmanuel Rouat <emmanuel.rouat@wanadoo.fr>
           1984-2006 Free Software Foundation, Inc.
           Gary W. Ng
           2002 Georg Post <georg.post@wanadoo.fr>
           1991 Georgia Tech Research Corporation
           Giles C. Billingsley
           Glao S. Dezai <dezai@hotbot.com>
           Gordon M. Jacobs 
           2003 STARC (Hiroshima University STARC IGFET Model)
           Hitoshi Tanaka <HDA01055@nifty.com>
           2008 Holger Vogt <holger.vogt@uni-duisburg.de>
           Hong June Park
           Jaijeet S. Roychowdhury
           Jean-Marc Routure <routoure@greyc.ismra.fr>
           Jeffrey M. Hsu
           JianHui Huang
           Jon Engelbert <jon@beigebag.com>
           1993 K. Lee
           Kanwar Jit Singh
           Kartikeya Mayaram
           Kenneth H. Keller
           1985-1990 Kenneth S. Kundert
           1993 M. Shur
           Manfred Metzger <ManfredMetzger@gmx.de>
           Mansun Chan
           1994-1996 Macquarie University, Sydney Australia
           Mathew Lew
           Michael Widlok <widlok@uci.agh.edu.pl>
           Min-Chie Jeng
           2003-2008 Multigig Ltd
           Noah Friedman <friedman@prep.ai.mit.edu>
           2003-2009 Paolo Nenzi <pnenzi@ieee.org>
           1990-2006 Regents of the University of California
           Richard D. McRoberts <rdm@csn.net>
           Robert Lindsell <robertl@research.canon.com.au>
           Serban-Mihai Popescu <serbanp@ix.netcom.com>
           S. Hwang
           Stefan Jones <stefan.jones@multigig.com>
           Steven Borley <steven.borley@virgin.net>
           Steve Tell <tell@cs.unc.edu>
           Stuart Brorson <sdb@cloud9.net>
           1993 T. Ytterdal
           1993 T. A. Fjeldly
           Takayasu Sakurai
           1985 Thomas L. Quarles
           2000 owned by the United Kingdom Secretary of State for Defence
           1990 University of California, Berkeley
           Vera Albrecht <albrecht@danalyse.de>
           Wayne A. Christopher
           Weidong Liu
           Wolfgang Muees
License: MIT_MODERN

Files: src/spicelib/devices/adms/admst/*
Copyright: 2011 Noovela - Author Laurent Lemaitre
License: GPL-2+

Files: src/spicelib/devices/adms/admst/prengspice.xml
       src/spicelib/devices/adms/admst/ngspice.xml
Copyright: 2011 Noovela - Author Laurent Lemaitre
License: NOOVELA
 This code IS to be used for non-commercial usage ONLY.
 Note: publishing technical papers with the affiliation of a commercial company
 which results are produced by the tool are typical commercial usage of the tool. 
 Commercial usage are subject to special fees.
 Contact: r29173@noovela.com or www.noovela.com

Files: src/spicelib/devices/adms/psp102/*
Copyright: 2007, All Rights Reserved, NXP Semiconductors
License: None
 No license found, only copyright

Files: src/spicelib/devices/adms/mextram/*
Copyright: 2006 Delft University of Technology
License: ECL-1.0

Files: src/ciderlib/*
Copyright: 1991 Regents of the University of California
License: CIDER_LICENSE

Files: src/frontend/numparam/*
Copyright: 2002  Georg Post
License: LGPL-2+

Files:  src/maths/sparse/*
Copyright: 1985-1990 Kenneth S. Kundert and the University of California
License: MIT_SPARSE

Files: src/xspice/*
Copyright: 1991 Georgia Tech Research Corporation Atlanta, Georgia 30332
License: XSPICE_LICENSE

Files: src/tclspice.c
Copyright: 2003-2008 Multigig Ltd
License: LGPL-2

Files: debian/*
Copyright: 2008-2012 Gudjon I. Gudjonsson <gudjon@gudjon.org>
License: GPL-2

License: SPICEDOC
 Copyright 1996 The Regents of the University of California.
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation for educational, research and non-profit purposes,
 without fee, and without a written agreement is hereby granted,
 provided that the above copyright notice, this paragraph and the
 following thdree paragraphs appear in all copies.
 .
 This software program and documentation are copyrighted by The Regents
 of the University of California. The software program and
 documentation are supplied "as is", without any accompanying services
 from The Regents. The Regents does not warrant that the operation of
 the program will be uninterrupted or error-free. The end-user
 understands that the program was developed for research purposes and
 is advised not to rely exclusively on the program for any reason.
 .
 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND
 ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. THE UNIVERSITY OF
 CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS"
 BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO PROVIDE
 MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

License: XSPICE_LICENSE
 THE SOFTWARE PROGRAMS BELOW ARE IN THE PUBLIC DOMAIN AND ARE PROVIDED FREE OF 
 ANY CHARGE. THE GEORGIA TECH RESEARCH CORPORATION, THE GEORGIA INSTITUTE OF 
 TECHNOLOGY, AND/OR OTHER PARTIES PROVIDE THIS SOFTWARE "AS IS" WITHOUT WARRANTY
 OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE 
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE 
 ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH THE USER. 
 SHOULD THE PROGRAM PROVE DEFECTIVE, THE USER ASSUMES THE ENTIRE COST OF ALL 
 NECESSARY SERVICING, REPAIR OR CORRECTION. IN NO EVENT WILL THE GEORGIA TECH 
 RESEARCH CORPORATION, THE GEORGIA INSTITUTE OF TECHNOLOGY, AND/OR OTHER PARTIES 
 PROVIDING THE PROGRAMS BELOW BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY 
 GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR 
 INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA 
 BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A 
 FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS).

License: MIT_SPARSE
 Permission to use, copy, modify, and distribute this software and
 ts documentation for any purpose and without fee is hereby granted,
 provided that the copyright notices appear in all copies and
 supporting documentation and that the authors and the University of
 California are properly credited.  The authors and the University of
 California make no representations as to the suitability of this
 software for any purpose.  It is provided `as is', without express
 or implied warranty.

License: MIT_MODERN
 Copyright © 1985-1991 The Regents of the University of California.
 All rights reserved.
 .
 Permission is hereby granted, without written agreement and without license
 or royalty fees, to use, copy, modify, and distribute this software and its
 documentation for any purpose, provided that the above copyright notice and
 the following two paragraphs appear in all copies of this software.
 .
 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN
 "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO PROVIDE
 MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

License: CIDER_LICENSE
 RESEARCH SOFTWARE AGREEMENT
 .
 This file specifies the terms under which the software and
 documentation provided with this distribution are provided.
 .
 Software is distributed as is, completely without warranty
 or service support.  The University of California and its employ-
 ees are not liable for the condition or performance of the
 software.
 .
 The University does not warrant that it owns the copyright
 or other proprietary rights to all software and documentation
 provided under this agreement, notwithstanding any copyright
 notice, and shall not be liable for any infringement of copyright
 or proprietary rights brought by third parties against the reci-
 pient of the software and documentation provided under this
 agreement.
 .
 THE UNIVERSITY OF CALIFORNIA HEREBY DISCLAIMS ALL IMPLIED
 WARRANTIES, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE.  THE UNIVERSITY IS NOT
 LIABLE FOR ANY DAMAGES INCURRED BY THE RECIPIENT IN USE OF THE
 SOFTWARE AND DOCUMENTATION, INCLUDING DIRECT, INDIRECT, SPECIAL,
 INCIDENTAL, OR CONSEQUENTIAL DAMAGES.
 .
 The University of California grants the recipient the right
 to modify, copy, and redistribute the software and documentation,
 both within the recipient's organization and externally, subject
 to the following restrictions:
 .
 (a)  The recipient agrees not to charge for the University of
 California code itself.  The recipient may, however, charge
 for additions, extensions, or support.
 .
 (b)  In any product based on the software, the recipient agrees
 to acknowledge the research group that developed the
 software.  This acknowledgement shall appear in the product
 documentation.
 .
 ©  The recipient agrees to obey all U.S. Government restric-
 tions governing redistribution or export of the software and
 documentation.

License: ECL-1.0
 Educational Community License version 1.0
 .
 This Original Work, including software, source code, documents, or other related items, 
 is being provided by the copyright holder(s) subject to the terms of the Educational 
 Community License. By obtaining, using and/or copying this Original Work, you agree that 
 you have read, understand, and will comply with the following terms and conditions of 
 the Educational Community License:
 .
 Permission to use, copy, modify, merge, publish, distribute, and sublicense this Original 
 Work and its documentation, with or without modification, for any purpose, and without fee 
 or royalty to the copyright holder(s) is hereby granted, provided that you include the 
 following on ALL copies of the Original Work or portions thereof, including modifications 
 or derivatives, that you make:
 .
 The full text of the Educational Community License in a location viewable to users of the 
 redistributed or derivative work. 
 .
 Any pre-existing intellectual property disclaimers, notices, or terms and conditions. 
 .
 Notice of any changes or modifications to the Original Work, including the date the 
 changes were made. 
 .
 Any modifications of the Original Work must be distributed in such a manner as to avoid 
 any confusion with the Original Work of the copyright holders.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 The name and trademarks of copyright holder(s) may NOT be used in advertising or publicity 
 pertaining to the Original or Derivative Works without specific, written prior permission. 
 Title to copyright in the Original Work and any associated documentation will at all times 
 remain with the copyright holders. 

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation
 version 2.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation
 version 2 or any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation
 version 2.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation
 version 2 or any later version.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.


Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: maptools
Upstream-Contact: Nicholas J. Lewin-Koh, Roger Bivand, and others
Source: http://cran.r-project.org/web/packages/maptools/
Disclaimer: This package is not part of the Debian operating system.
 The r-cran-maptools source and binary Debian packages are part of the
 "non-free" area in our archive. The packages in this area are not part of the
 Debian system, although they have been configured for use with Debian. We
 nevertheless support the use of the r-cran-maptools source and binary Debian
 pacakges and provide infrastructure for non-free packages (such as our bug
 tracking system and mailing lists).
 .
 The r-cran-maptools package contains some data files, inst/shapes/baltim.*,
 inst/shapes/columbus.*, and inst/shapes/sids.* (see below), whose license does
 not permit modification.
 .
 A request about relicensing was send to Julia Koschinsky <jkoschin@asu.edu>
 but not answered until now.
 .
 The r-cran-maptools package also contains files of unclear ownership and
 licensing: `data/wrld_simpl.rda' and `inst/shapes/pointZ.*'.
X-Non-Free-Autobuild: yes
  The licence does not forbid Debian from using autobuilders to create binary
  packages.

Files: *
Copyright: © 2001-2008 Nicholas J. Lewin-Koh and Roger Bivand
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

Files:  data/wrld_simpl.rda
Copyright: unknown
License: unknown
 This file is a modified SpatialPolygonsDataFrame object
 generated from the TM_WORLD_BORDERS_SIMPL-0.2 shapefile downloaded from
 http://mappinghacks.com/data/TM_WORLD_BORDERS_SIMPL-0.2.zip with the 
 attribution: "The world_borders.zip dataset has been superseded by Bjørn 
 Sandvik's improved version, TM_WORLD_BORDERS-0.2.zip, mirrored from 
 thematicmapping.org." The country Polygons objects and the data slot data 
 frame row numbers have been set to the ISO 3166 three letter codes.

Files: inst/shapes/fylk-val.*
Copyright: Statens Kartverk 2001
License: PD
 These files were released (with a collection of other shapefiles) to the
 public domain in July 2001. The link to the relevant page under
 http://www.statkart.no has now expired, this copy downloaded 13 July 2001.

Files: inst/shapes/baltim.* inst/shapes/columbus.* and inst/shapes/sids.*
Copyright: 2005-2006 GeoDa Center for Geospatial Analysis and Computation
License:
 These files are derived by permission from SAL data files
 prepared by Luc Anselin; they were downloaded from:
 .
 http://sal.agecon.uiuc.edu/stuff/data.html (now http://geodacenter.asu.edu/sdata)
 .
 under the following conditions: "The data are provided as is, without
 any warranties. Please make sure to give proper credit when using
 them in any publications. Some of the data sets are purely illustrative
 and should not be used for substantive research."
Comment: Additional quote from an e-mail to debian-science list:
 see http://lists.debian.org/debian-science/2009/03/msg00056.html
 .
 Date: Sun, 15 Mar 2009 19:22:26 -0700
 From: Julia Koschinsky <jkoschin@asu.edu>
 To: Stuart Prescott <stuart+debian@nanonanonano.net>
 Cc: debian-science@lists.debian.org, Andreas Tille <tillea@rki.de>,
     Mark Hymers <ftpmaster@debian.org>, ...
 Subject: Re: r-cran-maptools_0.7.16-1_i386.changes REJECTED (fwd)
 .
 Hi Stuart,
 .
 As far as we are concerned at the GeoDa Center, you're welcome to use
 our sample data at http://geodacenter.asu.edu/sdata for all of the
 purposes you mention, including sharing them with others, uploading
 them on your local server for your students, listing them on your
 personal website, simplifying the data, improving it, or other
 educational uses. We appreciate listing the credit.
 .
 Our primary purpose for making the data available is for our tutorials
 and software use. If you or others find them useful for other
 purposes, please go ahead. Several of the sample datasets include ESRI
 shapefiles and a lot of them are based on US Census data - we're not
 sure what the legal restrictions are in this case but I can tell you
 that as far as the GeoDa Center is concerned, anyone who wants to use,
 add value, make available or any other use they see for our sample
 data for non-commercial purposes is welcome to it.
 .
 Enjoy.
 Julia
 .
 ---------------------------------------------
 In addition you might like to read
 http://www.census.gov/geo/www/tiger/faq.html#38

Files: inst/shapes/pointZ.*
Copyright: Michael Toews
License: unknown
 These files are a sample of points from a 3D point shapefile kindly
 contributed by Michael Toews to be used here for illustrative purposes; this
 sample is written out using writePointsShape().

Files: R/GE_png.R
Copyright: © 2007 by Duncan Golicher, David Forrest and Roger Bivand
License: GPL-2+

Files: R/point.in.polygon.R
Copyright: © 2004 Edzer Pebesma
License: GPL-2+
Comment: This file was copied from the R sp package, released under the GNU
 GPL version 2 or any later version.
 See http://downloads.sourceforge.net/r-spatial/sp_0.7-0.tar.gz for confirmation.

Files: R/readGPS.R
Copyright: © 2007 Patrick Giraudoux and Roger Bivand
License: GPL-2+

Files: R/sp2Mondrian.R
Copyright: © 2006-7 Patrick Hausmann and Roger Bivand
License: GPL-2+

Files: src/pip.c
Copyright: © 2004 Edzer Pebesma, with portion © 1998 by Joseph O'Rourke.
License: GPL-2+_with_portion
 Written by Joseph O'Rourke, contributions by Min Xu, June 1997.
 Questions to orourke@cs.smith.edu.
 .
 This code is Copyright 1998 by Joseph O'Rourke.  It may be freely 
 redistributed in its entirety provided that this copyright notice is 
 not removed.
Comment: This file was copied from the R sp package, released under the GNU
 GPL version 2 or any later version.
 See http://downloads.sourceforge.net/r-spatial/sp_0.7-0.tar.gz for confirmation.

Files: src/shapefil.h src/shpopen.c src/shptree.c
Copyright: © 1999, 2001, Frank Warmerdam <warmerdam@pobox.com>
License: MIT | LGPL
 This software is available under the following "MIT Style" license,
 or at the option of the licensee under the LGPL (see LICENSE.LGPL).  This
 option is discussed in more detail in shapelib.html.
 .
 --
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
Comment: quoting from http://shapelib.maptools.org/license.html :
 I am pleased to receive bug fixes, and improvements for Shapelib. Unless the
 submissions indicate otherwise I will assume that changes submitted to me
 remain under the the above "dual license" terms. If changes are made to the
 library with the intention that those changes should be protected by the LGPL
 then I should be informed upon submission. Note that I will not generally
 incorporate changes into the core of Shapelib that are protected under the LGPL
 as this would effectively limit the whole file and distribution to LGPL terms.

Files: src/Rcentroid.c
Copyright: © Joseph O'Rourke, Carl Anderson, Frank Warmerdam, Nicholas Lewin-Koh, Roger S. Bivand
License: GPL-2+

Files: src/Rgshhs.*
Copyright: © 1996-2008 P. Wessel and W. H. F. Smith
           © 2005-7 Roger Bivand
License: GPL-2+

Files: debian/*
Copyright: 2009 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Public
 License can be found in `/usr/share/common-licenses/GPL'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vmtk
Upstream-Contact: vmtk-users@lists.sourceforge.net
Source: http://www.vmtk.org
Disclaimer:
 This package is not part of the main Debian GNU/Linux distribution,
 but uploaded to the non-free section. This is because it includes
 non-free software (Tetgen). However, the (Tetgen) license does not
 forbid Debian from using autobuilders to create binary packages.
 .
 Most of the excluded files are non-distributable, and useless for Debian anyway.
Files-Exluced:
 vmtk-?.?/distribution/egg/windows_dll/

Files: *
Copyright: 2004-2012, Luca Antiga, David Steinman
License: BSD-3-clause

Files: debian/*
Copyright: 2008-2012, Johannes Ring <johannr@simula.no>
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITK.h
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKArchetypeImageSeriesReader.cxx
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKArchetypeImageSeriesReader.h
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKArchetypeImageSeriesScalarReader.cxx
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKArchetypeImageSeriesScalarReader.h
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKImageWriter.cxx
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKImageWriter.h
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKUtility.h
 vtkVmtk/Utilities/vtkvmtkITK/vtkvmtkITKWin32Header.h
Copyright: Brigham and Women's Hospital (BWH)
Comment:
 For more information, please see:
   http://www.slicer.org
 and
   http://wiki.na-mic.org/Wiki/index.php/Slicer3
 .
 The 3D Slicer license below is a BSD style license, with extensions
 to cover contributions and other issues specific to 3D Slicer.
License: BSD-style-1.0
 3D Slicer Contribution and Software License Agreement ("Agreement")
 Version 1.0 (December 20, 2005)
 .
 This Agreement covers contributions to and downloads from the 3D
 Slicer project ("Slicer") maintained by The Brigham and Women's
 Hospital, Inc. ("Brigham"). Part A of this Agreement applies to
 contributions of software and/or data to Slicer (including making
 revisions of or additions to code and/or data already in Slicer). Part
 B of this Agreement applies to downloads of software and/or data from
 Slicer. Part C of this Agreement applies to all transactions with
 Slicer. If you distribute Software (as defined below) downloaded from
 Slicer, all of the paragraphs of Part B of this Agreement must be
 included with and apply to such Software.
 .
 Your contribution of software and/or data to Slicer (including prior
 to the date of the first publication of this Agreement, each a
 "Contribution") and/or downloading, copying, modifying, displaying,
 distributing or use of any software and/or data from Slicer
 (collectively, the "Software") constitutes acceptance of all of the
 terms and conditions of this Agreement. If you do not agree to such
 terms and conditions, you have no right to contribute your
 Contribution, or to download, copy, modify, display, distribute or use
 the Software.
 .
 PART A. CONTRIBUTION AGREEMENT - License to Brigham with Right to
 Sublicense ("Contribution Agreement").
 .
 1. As used in this Contribution Agreement, "you" means the individual
    contributing the Contribution to Slicer and the institution or
    entity which employs or is otherwise affiliated with such
    individual in connection with such Contribution.
 .
 2. This Contribution Agreement applies to all Contributions made to
    Slicer, including without limitation Contributions made prior to
    the date of first publication of this Agreement. If at any time you
    make a Contribution to Slicer, you represent that (i) you are
    legally authorized and entitled to make such Contribution and to
    grant all licenses granted in this Contribution Agreement with
    respect to such Contribution; (ii) if your Contribution includes
    any patient data, all such data is de-identified in accordance with
    U.S. confidentiality and security laws and requirements, including
    but not limited to the Health Insurance Portability and
    Accountability Act (HIPAA) and its regulations, and your disclosure
    of such data for the purposes contemplated by this Agreement is
    properly authorized and in compliance with all applicable laws and
    regulations; and (iii) you have preserved in the Contribution all
    applicable attributions, copyright notices and licenses for any
    third party software or data included in the Contribution.
 .
 3. Except for the licenses granted in this Agreement, you reserve all
    right, title and interest in your Contribution.
 .
 4. You hereby grant to Brigham, with the right to sublicense, a
    perpetual, worldwide, non-exclusive, no charge, royalty-free,
    irrevocable license to use, reproduce, make derivative works of,
    display and distribute the Contribution. If your Contribution is
    protected by patent, you hereby grant to Brigham, with the right to
    sublicense, a perpetual, worldwide, non-exclusive, no-charge,
    royalty-free, irrevocable license under your interest in patent
    rights covering the Contribution, to make, have made, use, sell and
    otherwise transfer your Contribution, alone or in combination with
    any other code.
 .
 5. You acknowledge and agree that Brigham may incorporate your
    Contribution into Slicer and may make Slicer available to members
    of the public on an open source basis under terms substantially in
    accordance with the Software License set forth in Part B of this
    Agreement. You further acknowledge and agree that Brigham shall
    have no liability arising in connection with claims resulting from
    your breach of any of the terms of this Agreement.
 .
 6. YOU WARRANT THAT TO THE BEST OF YOUR KNOWLEDGE YOUR CONTRIBUTION
    DOES NOT CONTAIN ANY CODE THAT REQURES OR PRESCRIBES AN "OPEN
    SOURCE LICENSE" FOR DERIVATIVE WORKS (by way of non-limiting
    example, the GNU General Public License or other so-called
    "reciprocal" license that requires any derived work to be licensed
    under the GNU General Public License or other "open source
    license").
 .
 PART B. DOWNLOADING AGREEMENT - License from Brigham with Right to
 Sublicense ("Software License").
 .
 1. As used in this Software License, "you" means the individual
    downloading and/or using, reproducing, modifying, displaying and/or
    distributing the Software and the institution or entity which
    employs or is otherwise affiliated with such individual in
    connection therewith. The Brigham and Women?s Hospital,
    Inc. ("Brigham") hereby grants you, with right to sublicense, with
    respect to Brigham's rights in the software, and data, if any,
    which is the subject of this Software License (collectively, the
    "Software"), a royalty-free, non-exclusive license to use,
    reproduce, make derivative works of, display and distribute the
    Software, provided that:
 .
 (a) you accept and adhere to all of the terms and conditions of this
 Software License;
 .
 (b) in connection with any copy of or sublicense of all or any portion
 of the Software, all of the terms and conditions in this Software
 License shall appear in and shall apply to such copy and such
 sublicense, including without limitation all source and executable
 forms and on any user documentation, prefaced with the following
 words: "All or portions of this licensed product (such portions are
 the "Software") have been obtained under license from The Brigham and
 Women's Hospital, Inc. and are subject to the following terms and
 conditions:"
 .
 (c) you preserve and maintain all applicable attributions, copyright
 notices and licenses included in or applicable to the Software;
 .
 (d) modified versions of the Software must be clearly identified and
 marked as such, and must not be misrepresented as being the original
 Software; and
 .
 (e) you consider making, but are under no obligation to make, the
 source code of any of your modifications to the Software freely
 available to others on an open source basis.
 .
 2. The license granted in this Software License includes without
    limitation the right to (i) incorporate the Software into
    proprietary programs (subject to any restrictions applicable to
    such programs), (ii) add your own copyright statement to your
    modifications of the Software, and (iii) provide additional or
    different license terms and conditions in your sublicenses of
    modifications of the Software; provided that in each case your use,
    reproduction or distribution of such modifications otherwise
    complies with the conditions stated in this Software License.
 .
 3. This Software License does not grant any rights with respect to
    third party software, except those rights that Brigham has been
    authorized by a third party to grant to you, and accordingly you
    are solely responsible for (i) obtaining any permissions from third
    parties that you need to use, reproduce, make derivative works of,
    display and distribute the Software, and (ii) informing your
    sublicensees, including without limitation your end-users, of their
    obligations to secure any such required permissions.
 .
 4. The Software has been designed for research purposes only and has
    not been reviewed or approved by the Food and Drug Administration
    or by any other agency. YOU ACKNOWLEDGE AND AGREE THAT CLINICAL
    APPLICATIONS ARE NEITHER RECOMMENDED NOR ADVISED. Any
    commercialization of the Software is at the sole risk of the party
    or parties engaged in such commercialization. You further agree to
    use, reproduce, make derivative works of, display and distribute
    the Software in compliance with all applicable governmental laws,
    regulations and orders, including without limitation those relating
    to export and import control.
 .
 5. The Software is provided "AS IS" and neither Brigham nor any
    contributor to the software (each a "Contributor") shall have any
    obligation to provide maintenance, support, updates, enhancements
    or modifications thereto. BRIGHAM AND ALL CONTRIBUTORS SPECIFICALLY
    DISCLAIM ALL EXPRESS AND IMPLIED WARRANTIES OF ANY KIND INCLUDING,
    BUT NOT LIMITED TO, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR
    A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
    BRIGHAM OR ANY CONTRIBUTOR BE LIABLE TO ANY PARTY FOR DIRECT,
    INDIRECT, SPECIAL, INCIDENTAL, EXEMPLARY OR CONSEQUENTIAL DAMAGES
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY ARISING IN ANY WAY
    RELATED TO THE SOFTWARE, EVEN IF BRIGHAM OR ANY CONTRIBUTOR HAS
    BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. TO THE MAXIMUM
    EXTENT NOT PROHIBITED BY LAW OR REGULATION, YOU FURTHER ASSUME ALL
    LIABILITY FOR YOUR USE, REPRODUCTION, MAKING OF DERIVATIVE WORKS,
    DISPLAY, LICENSE OR DISTRIBUTION OF THE SOFTWARE AND AGREE TO
    INDEMNIFY AND HOLD HARMLESS BRIGHAM AND ALL CONTRIBUTORS FROM AND
    AGAINST ANY AND ALL CLAIMS, SUITS, ACTIONS, DEMANDS AND JUDGMENTS
    ARISING THEREFROM.
 .
 6. None of the names, logos or trademarks of Brigham or any of
    Brigham's affiliates or any of the Contributors, or any funding
    agency, may be used to endorse or promote products produced in
    whole or in part by operation of the Software or derived from or
    based on the Software without specific prior written permission
    from the applicable party.
 .
 7. Any use, reproduction or distribution of the Software which is not
    in accordance with this Software License shall automatically revoke
    all rights granted to you under this Software License and render
    Paragraphs 1 and 2 of this Software License null and void.
 .
 8. This Software License does not grant any rights in or to any
    intellectual property owned by Brigham or any Contributor except
    those rights expressly granted hereunder.
 .
 PART C. MISCELLANEOUS
 .
 This Agreement shall be governed by and construed in accordance with
 the laws of The Commonwealth of Massachusetts without regard to
 principles of conflicts of law. This Agreement shall supercede and
 replace any license terms that you may have agreed to previously with
 respect to Slicer.

Files: vtkVmtk/Segmentation/itkAnisotropicDiffusionVesselEnhancementFunction.h
 vtkVmtk/Segmentation/itkAnisotropicDiffusionVesselEnhancementFunction.txx
 vtkVmtk/Segmentation/itkAnisotropicDiffusionVesselEnhancementImageFilter.txx
 vtkVmtk/Segmentation/itkFastMarchingDirectionalFreezeImageFilter.h
 vtkVmtk/Segmentation/itkFastMarchingDirectionalFreezeImageFilter.txx
 vtkVmtk/Segmentation/itkFastMarchingUpwindGradientImageFilter.h
 vtkVmtk/Segmentation/itkFastMarchingUpwindGradientImageFilter.txx
 vtkVmtk/Segmentation/itkHessianSmoothed3DToVesselnessMeasureImageFilter.h
 vtkVmtk/Segmentation/itkHessianSmoothed3DToVesselnessMeasureImageFilter.txx
 vtkVmtk/Segmentation/itkSymmetricEigenVectorAnalysisImageFilter.h
Copyright: 1999-2009, Insight Software Consortium
License: BSD-3-clause

Files: vtkVmtk/Common/vtkvmtkWin32Header.h
 vtkVmtk/Misc/vtkvmtkStaticTemporalInterpolatedVelocityField.cxx
 vtkVmtk/Misc/vtkvmtkStaticTemporalStreamTracer.cxx
Copyright: 1993-2009, Ken Martin
 1993-2009, Will Schroeder
 1993-2009, Bill Lorensen
License: The-Visualization-Toolkit-(VTK)-License

Files: vtkVmtk/ComputationalGeometry/vtkvmtkAppendFilter.h
 vtkVmtk/Misc/vtkvmtkStaticTemporalInterpolatedVelocityField.h
 vtkVmtk/Misc/vtkvmtkStaticTemporalStreamTracer.h
Copyright: 1993-2009, Ken Martin
 1993-2009, Will Schroeder
 1993-2009, Bill Lorensen
 2004-2012, Luca Antiga
 2004-2012, David Steinman
License: The-Visualization-Toolkit-(VTK)-License

Files: vtkVmtk/Utilities/OpenNL/nl.h
 vtkVmtk/Utilities/OpenNL/nl_single_file.c
Copyright: 2004-2010, Bruno Levy <levy@loria.fr>
License: BSD-3-style
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 * Neither the name of the ALICE Project-Team nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 If you modify this software, you should include a notice giving the
 name of the person performing the modification, the date of modification,
 and the reason for such modification.
 .
 Contact: Bruno Levy
 .
    levy@loria.fr
 .
    ALICE Project
    LORIA, INRIA Lorraine,
    Campus Scientifique, BP 239
    54506 VANDOEUVRE LES NANCY CEDEX
    FRANCE

Files: vtkVmtk/Utilities/Stellar_1.0/*
Copyright: 1995-1998, Jonathan Richard Shewchuk
 2006-2008, Bryan Klingner
License: BSD-3-clause
Comment:
 There is no license information in these files but the license is stated
 on the Stellar webpage: http://www.cs.berkeley.edu/~jrs/stellar/#about

Files: vtkVmtk/Utilities/tetgen1.4.3/tetgen.cxx
 vtkVmtk/Utilities/tetgen1.4.3/tetgen.h
Copyright: 2002-2009, Hang Si <si@wias-berlin.de>
License: Modified-MIT
 Permission is hereby granted, free  of charge, to any person obtaining
 a  copy  of this  software  and  associated  documentation files  (the
 "Software"), to  deal in  the Software without  restriction, including
 without limitation  the rights to  use, copy, modify,  merge, publish,
 distribute,  sublicense and/or  sell copies  of the  Software,  and to
 permit persons to whom the Software  is furnished to do so, subject to
 the following conditions:
 .
 Distribution of  modified  versions  of this code is permissible UNDER
 THE CONDITION THAT  THIS CODE AND ANY MODIFICATIONS  MADE TO IT IN THE
 SAME SOURCE FILES  tetgen.h AND tetgen.cxx  REMAIN UNDER  COPYRIGHT OF
 THE  ORIGINAL AUTHOR,  BOTH  SOURCE AND OBJECT  CODE  ARE MADE  FREELY
 AVAILABLE  WITHOUT   CHARGE,   AND  CLEAR   NOTICE  IS  GIVEN  OF  THE
 MODIFICATIONS.
 .
 Distribution of this code for  any  commercial purpose  is permissible
 ONLY BY DIRECT ARRANGEMENT WITH THE COPYRIGHT OWNER.
 .
 The  above  copyright  notice  and  this permission  notice  shall  be
 included in all copies or substantial portions of the Software.
 .
 THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT  SHALL THE AUTHORS OR COPYRIGHT HOLDERS  BE LIABLE FOR ANY
 CLAIM, DAMAGES OR  OTHER LIABILITY, WHETHER IN AN  ACTION OF CONTRACT,
 TORT  OR OTHERWISE, ARISING  FROM, OUT  OF OR  IN CONNECTION  WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: vtkVmtk/Utilities/tetgen1.4.3/predicates.cxx
Copyright: 1996, Jonathan Richard Shewchuk
License: public-domain
 Routines for Arbitrary Precision Floating-point Arithmetic
 and Fast Robust Geometric Predicates
 (predicates.c)
 .
 May 18, 1996
 .
 Placed in the public domain by
 Jonathan Richard Shewchuk
 School of Computer Science
 Carnegie Mellon University
 5000 Forbes Avenue
 Pittsburgh, Pennsylvania  15213-3891
 jrs@cs.cmu.edu
 .
 This file contains C implementation of algorithms for exact addition
   and multiplication of floating-point numbers, and predicates for
   robustly performing the orientation and incircle tests used in
   computational geometry.  The algorithms and underlying theory are
   described in Jonathan Richard Shewchuk.  "Adaptive Precision Floating-
   Point Arithmetic and Fast Robust Geometric Predicates."  Technical
   Report CMU-CS-96-140, School of Computer Science, Carnegie Mellon
   University, Pittsburgh, Pennsylvania, May 1996.  (Submitted to
   Discrete & Computational Geometry.)
 .
 This file, the paper listed above, and other information are available
   from the Web page http://www.cs.cmu.edu/~quake/robust.html .

License: The-Visualization-Toolkit-(VTK)-License
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
 .
     * Neither name of Ken Martin, Will Schroeder, or Bill Lorensen nor the names
       of any contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of Orobix Srl, the Mario Negri Institute, of the
 University of Toronto, nor the names of its contributors may be used
 to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

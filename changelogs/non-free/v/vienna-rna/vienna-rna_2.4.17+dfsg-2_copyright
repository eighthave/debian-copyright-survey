Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ViennaRNA
Source: https://www.tbi.univie.ac.at/RNA/
        https://github.com/ViennaRNA/ViennaRNA/releases
Files-Excluded:
         src/json
         src/libsvm-*
        src/cthreadpool
        doc/latex
        doc/html
        doc/RNAlib-*.pdf
        RNA-Tutorial/Data/programs/alifoldz_adopted.tar.gz
        RNA-Tutorial/Data/programs/qrna-2.0.3d.tar.gz
        Makefile.in
        */Makefile.in
        */*/Makefile.in
        RNA-Tutorial/RNA-tutorial-*.pdf
        */*eps-converted-to.pdf
        */aclocal.m4
        src/*_cmdl.h
        src/*_cmdl.c
        compile
        config/ar-lib
        config/compile
        config/config.guess
        config/config.sub
        config/depcomp
        config.guess
        config.h.in
        config/install-sh
        config/ltmain.sh
        config/missing
        config.sub
        config/test-driver
        config/texinfo.tex
        configure
        depcomp
        ylwrap
        m4/libtool.m4
        missing
        src/ViennaRNA/static/misc/dna_mathews1999.hex
        src/ViennaRNA/static/misc/dna_mathews2004.hex
        src/ViennaRNA/static/misc/rna_andronescu2007.hex
        src/ViennaRNA/static/misc/rna_langdon2018.hex
        src/ViennaRNA/static/misc/rna_misc_special_hairpins.hex
        src/ViennaRNA/static/misc/rna_turner1999.hex
        src/ViennaRNA/static/misc/rna_turner2004.hex
        src/ViennaRNA/static/postscript/aln_macro_base.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_base.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_linear_data.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_sc_motifs.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_sd.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_turn.hex
        src/ViennaRNA/static/postscript/dot_plot_macro_ud.hex
        src/ViennaRNA/static/postscript/structure_plot_macro_base.hex
        src/ViennaRNA/static/postscript/structure_plot_macro_extras.hex
        man/RNA2Dfold.1
        man/RNALalifold.1
        man/RNALfold.1
        man/RNAPKplex.1
        man/RNAaliduplex.1
        man/RNAalifold.1
        man/RNAcofold.1
        man/RNAdistance.1
        man/RNAdos.1
        man/RNAduplex.1
        man/RNAeval.1
        man/RNAfold.1
        man/RNAheat.1
        man/RNAinverse.1
        man/RNAlib.info
        man/RNApaln.1
        man/RNAparconv.1
        man/RNApdist.1
        man/RNAplex.1
        man/RNAplfold.1
        man/RNAplot.1
        man/RNApvmin.1
        man/RNAsnoop.1
        man/RNAsubopt.1
        man/RNAup.1
        man/ct2db.1
        src/Kinfold/cmdline.c
        src/Kinfold/cmdline.h
        src/Kinfold/config.h.in
        src/Kinwalker/config.h.in
        src/RNAforester/config.h.in
        src/RNAforester/src/anchors/shape.cpp
        src/RNAforester/src/anchors/shape.hpp
        src/RNAlocmin/RNAlocmin_cmdline.c
        src/RNAlocmin/RNAlocmin_cmdline.h
        src/RNAlocmin/config.h.in
        */autom4te.cache
        doc/refman-html
        install-sh
        interfaces/Perl/RNA.pm
        interfaces/Perl/RNA_wrap.cpp
        interfaces/Perl/version.i
        interfaces/Python3/RNA.py
        interfaces/Python3/RNA_wrap.cpp
        interfaces/Python3/version.i
        packaging/macosx/resources/welcome.txt
        src/Kinfold/INSTALL
        src/Kinfold/configure
        src/Kinwalker/configure
        src/RNAforester/INSTALL
        src/RNAforester/configure
        src/RNAforester/g2-0.72/configure
        src/RNAlocmin/configure
        src/ViennaRNA/static/energy_parameter_sets.h
        src/ViennaRNA/static/templates_postscript.h
        src/*/ylwrap
        src/*/install-sh
        src/*/depcomp
        src/*/missing
        src/*/compile
        src/*/config.sub
        src/*/config.guess
        src/RNAforester/g2-0.72/include
        RNA-Tutorial/Data/programs
        m4/lt~obsolete.m4
        m4/ltversion.m4
        m4/ltoptions.m4
        m4/ltsugar.m4


Files: *
Copyright: 2009 Ivo Hofacker <ivo@tbi.univie.ac.at>
License: non-free
 Disclaimer and Copyright
 .
 The programs, library and source code of the Vienna RNA Package are free
 software. They are distributed in the hope that they will be useful
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 .
 Permission is granted for research, educational, and commercial use
 and modification so long as 1) the package and any derived works are not
 redistributed for any fee, other than media costs, 2) proper credit is
 given to the authors and the Institute for Theoretical Chemistry of the
 University of Vienna.
 .
 If you want to include this software in a commercial product, please contact
 the authors.
 .
 Note that the file ./src/ViennaRNA/plotting/naview.c has its own
 copyright attached.

Files: src/ViennaRNA/naview.c
Copyright: 1988 Robert E. Bruccoleri
License: non-commercial
 Copying of this software, in whole or in part, is permitted
 provided that the copies are not made for commercial purposes,
 appropriate credit for the use of the software is given, this
 copyright notice appears, and notice is given that the copying
 is by permission of Robert E. Bruccoleri. Any other copying
 requires specific permission.

Files: src/RNAforester/*
Copyright: 2012-2017 Matthias Hoechsmann (mhoechsm@techfak.uni-bielefeld.de)
                     Stefanie Schirmer (sschirme@techfak.uni-bielefeld.de)
License: non-free_no-fee
 The programs and source code of RNAforester are free
 software. They are distributed in the hope that they will be useful
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 .
 Permission is granted for research, educational, and commercial use
 and modification so long as 1) the package and any derived works are not
 redistributed for any fee, other than media costs, 2) proper credit is
 given to the authors.

Files: debian/*
Copyright: 2011 Alex Mestiashvili <alex@biotec.tu-dresden.de>
           2011      Andreas Tille <tille@debian.org>
           2014      Olivier Sallou <osallou@debian.org>
           2019-2020 Steffen Moeller <moeller@debian.org>
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SNAPHU
Source: https://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/
Disclaimer: this package is in the non-free archive because parts of the
            sources can be used only for noncommercial purposes

Files: *
Copyright: 2002-2020 Board of Trustees, Leland Stanford Jr. University
License: other
 Except as noted below, permission to use, copy, modify, and
 distribute, this software and its documentation for any purpose is
 hereby granted without fee, provided that the above copyright notice
 appear in all copies and that both that copyright notice and this
 permission notice appear in supporting documentation, and that the
 name of the copyright holders be used in advertising or publicity
 pertaining to distribution of the software with specific, written
 prior permission, and that no fee is charged for further distribution
 of this software, or any modifications thereof.  The copyright holder
 makes no representations about the suitability of this software for
 any purpose.  It is provided "as is" without express or implied
 warranty.
 .
 THE COPYRIGHT HOLDER DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA, PROFITS, QPA OR GPA, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 -
 The parts of this software derived from the CS2 minimum cost flow
 solver written by A. V. Goldberg and B. Cherkassky are governed by the
 terms of the copyright holder of that software.  Permission has been
 granted to use and distribute that software for strictly noncommercial
 purposes as part of this package, provided that the following
 copyright notice from the original distribution and URL accompany the
 software:
 .
   COPYRIGHT C 1995 IG Systems, Inc.  Permission to use for
   evaluation purposes is granted provided that proper
   acknowledgments are given.  For a commercial licence, contact
   igsys@eclipse.net.
 .
   This software comes with NO WARRANTY, expressed or implied. By way
   of example, but not limitation, we make no representations of
   warranties of merchantability or fitness for any particular
   purpose or that the use of the software components or
   documentation will not infringe any patents, copyrights,
   trademarks, or other rights.
 .
   http://www.igsystems.com/cs2

Files: src/snaphu_cs2*
Copyright: 1995 IG Systems, Inc
License: igsys
 Permission to use for evaluation purposes is granted provided that
 proper acknowledgments are given.  For a commercial licence, contact
 igsys@eclipse.net.
 .
 This software comes with NO WARRANTY, expressed or implied. By way
 of example, but not limitation, we make no representations of
 warranties of merchantability or fitness for any particular
 purpose or that the use of the software components or
 documentation will not infringe any patents, copyrights,
 trademarks, or other rights.
 .
 http://www.igsystems.com/cs2

Files: debian/*
Copyright: 2010-2020 Antonio Valentino <antonio.valentino@tiscali.it>
License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

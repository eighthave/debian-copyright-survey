Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: scribus
Upstream-Contact: Scribut Team <scribus@scribus.net>
Source: http://www.scribus.net

Files: debian/*
Copyright: 2005-2012 Oleksandr Moskalenko <malex@debian.org>
           2014 Mattia Rizzolo <mattia@mapreri.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: *
Copyright: 2003-2011 Scribus Team <scribus@scribus.net>, namely
                     Andreas Vox <avox@arcor.de>
                     Christoph Schäfer
                     Christoph <Schafer christoph-schaefer@gmx.de>
                     Craig Bradney <cbradney@zip.com.au>
                     Craig Ringer <craig@postnewspapers.com.au>
                     Franz Schmid <Franz.Schmid@altmuehlnet.de>
                     Gregory Pittman
                     Hermann Kraus <herm@scribus.info>
                     Jain Basil
                     Jean Ghali <jghali@libertysurf.fr>
                     Maciej Hanski
                     Oleksandr Moskalenko <malex@tagancha.org>
                     Peter Linnell <mrdocs@scribus.info>
                     Petr Vaněk <petr@yarpen.cz>
                     Riku Leino <riku@scribus.info>
License: OPLv1.0
 I. REQUIREMENTS ON BOTH UNMODIFIED AND MODIFIED VERSIONS
 .
 This online help manual “Scribus Online Help” may be reproduced and distributed
 in whole or in part, in any medium physical or electronic, provided that the
 terms of this license are adhered to, and that this license or an incorporation
 of it by reference is displayed in the reproduction.
 .
 Proper form for an incorporation by reference is as follows:
 .
 Copyright © 2003–2011 the Scribus team composed of Franz Schmid, Peter Linnell,
 Craig Bradney, Jean Ghali, Hermann Kraus, Riku Leino, Oleksandr Moskalenko,
 Craig Ringer, Christoph Schäfer, Petr Vaněk, Andreas Vox and Jain Basil, as
 well as Gregory Pittman. The material may be distributed only subject to the
 terms and conditions set forth in the Open Publication License, v1.0 or later
 (the latest version is presently available at
 http://www.opencontent.org/openpub/).
 .
 The reference must be immediately followed with the elected options (see
 section VI).
 .
 Commercial redistribution of Open Publication-licensed material is permitted.
 .
 II. COPYRIGHT
 .
 The copyright is owned by Franz Schmid, Peter Linnell, Craig Bradney, Jean
 Ghali, Hermann Kraus, Riku Leino, Oleksandr Moskalenko, Craig Ringer, Christoph
 Schäfer, Petr Vaněk, Andreas Vox and Jain Basil, as well as Gregory Pittman.
 .
 III. SCOPE OF LICENSE
 .
 Mere aggregation of this work or a portion of this work with other works or
 programs on the same media shall not cause this license to apply to those other
 works. The aggregate work shall contain a notice specifying the inclusion of
 the Open Publication material and appropriate copyright notice.
 .
 SEVERABILITY. If any part of this license is found to be unenforceable in any
 jurisdiction, the remaining portions of the license remain in force.
 .
 NO WARRANTY. Open Publication works are licensed and provided “as is” without
 warranty of any kind, express or implied, including, but not limited to, the
 implied warranties of merchantability and fitness for a particular purpose or a
 warranty of non-infringement.
 .
 IV. REQUIREMENTS ON MODIFIED WORKS
 .
 All modified versions of documents covered by this license, including
 translations, anthologies, compilations and partial documents, must meet the
 following requirements:
 .
 1) The modified version must be labeled as such.
 .
 2) The person making the modifications must be identified and the modifications
 dated.
 .
 3) Acknowledgement of the original author and publisher if applicable must be
 retained according to normal academic citation practices.
 .
 4) The location of the original unmodified document must be identified.
 .
 5) The original author’s name may not be used to assert or imply endorsement of
 the resulting document without the original author’s (or authors’) permission.
 .
 V. GOOD-PRACTICE RECOMMENDATIONS
 .
 In addition to the requirements of this license, it is requested from and
 strongly recommended of redistributors that:
 .
 1) If you are distributing Open Publication works on hardcopy or CD-ROM, you
 provide email notification to the authors of your intent to redistribute at
 least thirty days before your manuscript or media freeze, to give the authors
 time to provide updated documents. This notification should describe
 modifications, if any, made to the document.
 .
 2) All substantive modifications (including deletions) be either clearly marked
 up in the document or else described in an attachment to the document.
 .
 Finally, while it is not mandatory under this license, it is considered good
 form to offer a free copy of any hardcopy and CD-ROM expression of an Open
 Publication-licensed work to its author.
 .
 VI. ELECTED OPTIONS
 .
 1) Distribution of the work or derivative of the work in any standard (paper)
 book form is prohibited unless prior permission is obtained from the copyright
 holder. Other forms of distribution including CD-ROM, electronic, and magnetic
 media are permitted.


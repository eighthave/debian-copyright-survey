Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FreeSpace2 Open
Source: http://www.hard-light.net/forums/
Files-Excluded:
    code/directx
    code/fred2/hlp/afxhelp.hm
    code/globalincs/msvc
    code/graphics/gl/GLAUX.H
    code/graphics/gl/Gl.h
    code/graphics/gl/GLU.H
    code/io/sw*
    code/mm
    code/osapi/monopub.h
    */*/*.exe
    jansson
    libjpeg
    libpng
    lua
    oggvorbis
    openal
    projects
    speech
    STLport-5.2.1
    zlib
Comment:
 non-free license.

Files: *
Copyright: 1999 Volition, Inc.
           2013 Freespace Open
           1998 Bruce Dawson
License: Volition-freespace
 All source code herein is the property of Volition, Inc. You may not sell
 or otherwise commercially exploit the source or things you created based
 on the source.

Files: code/graphics/gl/Glext.h
       code/graphics/gl/wglext.h
Copyright: 2007-2010 The Khronos Group Inc.
License: Expat~Khronos
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

Files: m4/ax_cxx_compile_stdcxx_11.m4
Copyright:
    2008 Benjamin Kosnik <bkoz@redhat.com>
    2012 Zack Weinberg <zackw@panix.com>
    2013 Roy Stogner <roystgnr@ices.utexas.edu>
License: FSF-Note
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

Files: mongoose/*
Copyright: 2004-2012 Sergey Lyubka
License: Expat

Files: debian/*
Copyright: 2011-2018 Dmitry Smirnov <onlyjob@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
Comment:
 This license is also known as "MIT" however FSF considers "MIT" labelling
 ambiguous and copyright-format specification recommend to label such a license
 as "Expat".

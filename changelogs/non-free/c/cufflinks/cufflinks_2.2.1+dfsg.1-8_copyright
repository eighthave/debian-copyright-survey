Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cufflinks
Upstream-Contact: tophat.cufflinks@gmail.com.
Source: http://cufflinks.cbcb.umd.edu/downloads/cufflinks-2.2.0.tar.gz
Files-Excluded:
 cufflinks.xcodeproj/*
 doc/*
 tests/*
 external_tests/*
 src/cufflinks.xcodeproj/*
Comment: license: non-free
 Most of the source code of Cufflinks is under the Boost Software License
 version 1.0, with the exception of M4 macros under permissive licenses, and
 ‘locfit’, which contains an unfortunate non-free clause restricting benchmarks.
 .
 After compilation (in May 2012), Cufflinks was linked to Boost (BSL-1.0), the
 GNU libc (mostly LGPL-2.1+), the GNU libgcc and libstdc++ (GPL-3+ with version
 3.1 of the GCC Runtime Library Exception), and the zlib (permissive license).
 .
 The python script cuffmerge is also under the BSL-1.0, and can be executed
 with Python without third-party libraries.
Disclamer: This package is not part of the Debian operating system.
 The cufflinks source and binary Debian packages are part of the “non-free” area
 in our archive. The packages in this area are not part of the Debian system,
 although they have been configured for use with Debian. We nevertheless support
 the use of the cufflinks source and binary Debian packages and provide
 infrastructure for non-free packages (such as our bug tracking system and
 mailing lists). The licences of Cufflinks do not forbid Debian from using
 autobuilders to create binary packages.
 .
 The non-free component of Cufflinks is ‘locfit’ (see below).

Files: *
Copyright: Copyright (C) 2003-2010 Cole Trapnell et al
 © 2010 Adam Roberts
 © 2009 Geo Pertea
 © 2001 Vladimir Prus <ghost@cs.msu.su>
 © 2001 Jeremy Siek <jsiek@cs.indiana.edu>
 © 2009 Eric Bose-Wolf
License: BSL-1.0

Files: ax_bam.m4
Copyright: © 2010 Cole Trapnell <cole@cs.umd.edu>
License: GAP

Files: ax_boost_base.m4 ax_boost_thread.m4
Copyright: © 2008,2009 Thomas Porschberg <thomas@randspringer.de>
 © 2009 Michael Tindal
License: GAP
Source: http://autoconf-archive.cryp.to/

Files: ax_check_zlib.m4
Copyright: © 2008 Loic Dachary <loic@senga.org>
License: GPL-2+_with_Autoconf_exception
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
Comment: On Debian systems, the full text of the GNU General Public
 license version 2 can be consulted in ‘/usr/share/common-licenses/GPL-2’.
Source: http://www.gnu.org/software/autoconf-archive/ax_check_zlib.html

Files: src/lemon/*
Copyright: © 2003-2008 Egervary Jeno Kombinatorikus Optimalizalasi Kutatocsoport (Egervary Research Group on Combinatorial Optimization, EGRES)
 © 2003 The Trustees of Indiana University
 © Jeremy Siek 2000
License: BSL-1.0
Comment: The files contain the following header:
  This file is a part of LEMON, a generic C++ optimization library
 .
  Copyright (C) 2003-2008
  Egervary Jeno Kombinatorikus Optimalizalasi Kutatocsoport
  (Egervary Research Group on Combinatorial Optimization, EGRES).
 .
  Permission to use, modify and distribute this software is granted
  provided that this copyright notice appears in all copies. For
  precise terms see the accompanying LICENSE file.
 .
  This software is provided "AS IS" with no warranty of any kind,
  express or implied, and with no claim as to its suitability for any
  purpose.
 .
 The license file at http://lemon.cs.elte.hu/trac/lemon/browser indicates
 the Boost Software License, Version 1.0.
Source: http://lemon.cs.elte.hu/trac/lemon

Files: src/locfit/*
Copyright: © 1996-2001 Lucent Technologies, Bell Laboratories.
 © 1996-2001 Catherine Loader.
License: Locfit
  ABOUT ALGORITHMS:
 The Locfit implementation has been designed largely from the point
 of view of generality: to provide a set of functions that can be
 used for as wide a range of local fitting problems as possible.
 It is not intended to be the fastest or most efficient implementation
 possible. In addition to generality, the code in many places makes
 extensive trade-offs made between speed and numerical accuracy (some
 of which can be controlled through optional arguments). Many of Locfit's
 options will only be used in a small fraction of cases; for other cases,
 they add to the cost of overhead (an obvious example is multi-dimensional
 fitting: when used in 1-d, many loops reduce to for(i=0;i<1;i++)).
 Additionally, the user interfaces (i.e. the R and S-Plus code) add
 significantly to the computational overhead. For these reasons, the
 Locfit code, as distributed, should not, and can not, be used to
 derive meaningful benchmarks, either for the speed or accuracy of algorithms.
 Anyone wishing to use Locfit in any kind of comparative benchmark
 study must contact and obtain permission from the Author.
 .
  COPYRIGHT:
 .
  Copyright (c) 1996-2001 Lucent Technologies, Bell Laboratories.
  SCB code is
  Copyright (c) 1996-2001 Jiayang Sun.
 .
 Permission to use, copy, modify, and distribute this software for any
 purpose (with the exceptions noted in `About Algorithms' above)
 without fee is hereby granted, and provided that this entire notice is
 included in all copies of any software which is or includes a copy or
 modification of this software and in all copies of the supporting
 documentation for such software.
 .
 THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTY.  IN PARTICULAR, NEITHER THE AUTHOR NOR LUCENT TECHNOLOGIES
 MAKE ANY REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE
 MERCHANTABILITY OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
Comment: the files contain “See README file for details.” and the license
 displayed above is from the README file of the locfit source available at
 http://stat.bell-labs.com/project/locfit/dist/locfit.tgz
 .
 Fedora came to the same conclusion here:
   https://bugzilla.redhat.com/show_bug.cgi?id=694998
Source: http://stat.bell-labs.com/project/locfit

Files: debian/*
Copyright: 2014 Alex Mestiashvili <alex@biotec.tu-dresden.de>
License: BSL-1.0

License: BSL-1.0
 Boost Software License, Version 1.0
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.
Comment: GAP stands for GNU All Permissive, see:
 http://www.gnu.org/s/hello/manual/texinfo/All_002dpermissive-Copying-License.html

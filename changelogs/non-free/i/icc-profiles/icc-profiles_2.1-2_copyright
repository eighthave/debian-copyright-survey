Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ICC Profiles
Source:
 http://japancolor.jp/icc.html
 http://photogamut.org/D_ICC_Profil.html
 http://web.archive.org/web/20070218091856/http://www.srgb.com/usingsrgb.html
 http://www.color.org/srgbprofiles.xalter
 http://www.eci.org/en/downloads
 http://www.wan-ifra.org/articles/2015/09/30/newspaper-colour-profile-download
 https://www.idealliance.org/gracol/

Files:
 CGATS21*.icc
 GRACoL*.icc
 SWOP*.icc
Copyright: X-Rite, Inc. <http://www.xrite.com/>
License: IDEAlliance
Comment:
 Some files embed copyright but no license. License were found here:
 <http://www.color.org/registry/CGATS21_CRPC1.xalter>
 <http://www.color.org/registry/CGATS21_CRPC2.xalter>
 <http://www.color.org/registry/CGATS21_CRPC3.xalter>
 <http://www.color.org/registry/CGATS21_CRPC4.xalter>
 <http://www.color.org/registry/CGATS21_CRPC5.xalter>
 <http://www.color.org/registry/CGATS21_CRPC6.xalter>
 <http://www.color.org/registry/CGATS21_CRPC7.xalter>
 <http://www.color.org/registry/GRACoL2006_Coated1v2.xalter>
 <http://www.color.org/registry/GRACoL2013UNC_CRPC3.xalter>
 <http://www.color.org/registry/GRACoL2013_CRPC6.xalter>
 <http://www.color.org/registry/SWOP2006_Coated3v2.xalter>
 <http://www.color.org/registry/SWOP2006_Coated5v2.xalter>
 <http://www.color.org/registry/SWOP2013C3_CRPC5.xalter>
Comment-Last-Update: 2017-06-01

Files: JapanColor2011Coated.icc
Copyright: X-Rite, Inc. <http://www.xrite.com/>
License: JPMA
Comment:
 The file embeds copyright but no license. License were found here:
 <http://www.color.org/registry/JapanColor2011Coated.xalter>
Comment-Last-Update: 2017-06-01

Files:
 ITU-RBT709ReferenceDisplay.icc
 PRMG_RGB-sRGB_based.icc
 Probev1_ICCv2.icc
 Probev1_ICCv4.icc
 sRGB2014.icc
 sRGB_v4_ICC_preference.icc
 sRGB_v4_ICC_preference_displayclass.icc
Copyright: 2004, 2007, 2011, 2015 International Color Consortium
 <icc_users@lists.color.org>
License: ICC

Files: ISOnewspaper26v4*
Copyright: IFRA <info@ifra.com>
License: IFRA

Files:
 ISOcoated.icc
 ISOcoated_v2_300_eci.icc
 ISOcoated_v2_eci.icc
 ISOuncoated.icc
 ISOuncoatedyellowish.icc
 ISOwebcoated.icc
 PSRgravureMF.icc
 SC_paper_eci.icc
Copyright: 2000-2006 Heidelberger Druckmaschinen AG
License: HEIDELBERG

Files:
 PSO_Coated_300_NPscreen_ISO12647_eci.icc
 PSO_Coated_NPscreen_ISO12647_eci.icc
 PSO_LWC_Improved_eci.icc
 PSO_LWC_Standard_eci.icc
 PSO_MFC_Paper_eci.icc
 PSO_SNP_Paper_eci.icc
 PSO_Uncoated_ISO12647_eci.icc
 PSO_Uncoated_NPscreen_ISO12647_eci.icc
 PSR_LWC_PLUS_V2_PT.icc
 PSR_LWC_STD_V2_PT.icc
 PSR_SC_PLUS_V2_PT.icc
 PSR_SC_STD_V2_PT.icc
Copyright: 2000-2009 Heidelberger Druckmaschinen AG
License: ECI

Files: eciRGB_v2.icc
 eciRGB_v2_ICCv4.icc
Copyright: 2007 Color Solutions
License: eciRGBv2

Files: sRGB_IEC61966-2-1_black_scaled.icc
 sRGB_IEC61966-2-1_no_black_scaling.icc
Copyright: 2009 International Color Consortium
 <icc_users@lists.color.org>
License: ICC~legacy

Files: XCMYK 2017.icc
Copyright: ColorLogic GmbH.
License: IDEAlliance~ColorLogic

Files: sRGB_ICC_v4_Appearance.icc
Copyright: 2010-2016 Fuji Xerox Co., Ltd.
License: ICC~v4-appearance

Files: sRGB.icm
Copyright: Hewlett-Packard Company
License: sRGB

Files: ISO22028-2_ROMM-RGB.icc
Copyright: 2006 Hewlett-Packard
License: ICC~Hewlett-Packard

Files: PhotoGamutRGB*
Copyright: The PhotoGamutRGB working group <info@photogamut.org>
License: CC-BY-ND

Files: debian/*
Copyright: 2005-2012 Oleksandr Moskalenko <malex@debian.org>
  2016-2017 Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+

License: IDEAlliance
 This profile is made available by IDEAlliance®, with permission of
 X-Rite, Inc., and may be used, embedded, exchanged, and shared without
 restriction. It may not be altered, or sold without written permission
 of IDEAlliance.

License: IDEAlliance~ColorLogic
 This profile is made available by IDEAlliance, with permission of
 ColorLogic GmbH, and may be used, embedded, exchanged, and shared
 without restriction.

License: JPMA
 This profile is made available by JPMA, with permission of X-Rite,
 Incorporated and may be used, embedded, exchanged, and shared without
 restriction. However, it may not be altered, or sold (whether standing
 alone or incorporated into commercial products) without written
 permission of X-Rite.

License: ICC
 This profile is made available by the International Color Consortium,
 and may be copied, distributed, embedded, made, used, and sold without
 restriction. Altered versions of this profile shall have the original
 identification and copyright information removed and shall not be
 misrepresented as the original profile.
Comment:
 Apparently applies generally to color profiles with ICC listed as
 copyright holder in the embedded Creator field, according to
 <http://www.color.org/profiles2.xalter#license>.

License: ICC~Hewlett-Packard
 This profile is made available by ICC, and may be copied, distributed,
 embedded, made, used, and sold without restriction. Altered versions of
 this profile shall have the original identification and copyright
 information removed and shall not be misrepresented as the original
 profile.

License: ICC~v4-appearance
 To anyone who acknowledges that the file "sRGB_ICC_v4_Appearance.icc"
 is provided "AS IS" WITH NO EXPRESS OR IMPLIED WARRANTY, permission to
 use, copy and distribute these file for any purpose is hereby granted
 without fee, provided that the files are not changed including the ICC
 copyright notice tag, and that the name of ICC shall not be used in
 advertising or publicity pertaining to distribution of the software
 without specific, written prior permission. ICC makes no
 representations about the suitability of this software for any purpose.

License: ICC~legacy
 To anyone who acknowledges that the files
 "sRGB_IEC61966-2-1_no_black_scaling.icc" and
 "sRGB_IEC61966-2-1_black scaled.icc" are provided "AS IS" WITH NO
 EXPRESS OR IMPLIED WARRANTY, permission to use, copy and distribute
 these file for any purpose is hereby granted without fee, provided
 that the files are not changed including the ICC copyright notice
 tag, and that the name of ICC shall not be used in advertising or
 publicity pertaining to distribution of the software without
 specific, written prior permission. ICC makes no representations
 about the suitability of this software for any purpose.

License: IFRA
 Downloading of the standard profile is free of charge and without
 restriction. No technical guarantee for the smooth functioning of the
 profiles in the different IT environments can be given.

License: ECI
 This profile is made available by ECI European Color Initiative, with
 permission of Heidelberger Druckmaschinen AG, and may be used,
 embedded, exchanged, and shared without restriction. It may not be
 altered or sold without written permission of ECI European Color
 Initiative.

License: eciRGBv2
 Use of the PROFILE as well as redistribution of the PROFILE are
 permitted under the following conditions:
  1. The PROFILE shall not be modified. The copyright notice embedded in
     the PROFILE must always be maintained.
  2. The PROFILE may be used free of charge.
  3. The PROFILE may be distributed as long as no fee is charged for the
     PROFILE. Where the PROFILE is distributed as part of a commercial
     product or service, the fee for such product or service shall be
     the same as if the product or service were offered without the
     PROFILE.
  4. Whenever the PROFILE is redistributed this LICENSE must be
     distributed together with the PROFILE. Nevertheless, if the PROFILE
     is distributed embedded into production data - like for example,
     but not limited to, inside Photoshop, TIFF, JPEG or PDF files - it
     is not required that this LICENSE be distributed together with the
     PROFILE.
  5. The names "eciRGB", "eciRGB_v2", "ECI" and "European Color
     Initiative" or any variation thereof shall not be used to endorse
     or promote products which make use of the PROFILE or which support
     the PROFILE without prior written permission from the European
     Color Initiative (ECI; www.eci.org).
  6. The names "Color Solutions" or "Color Solutions Software" or any
     variation thereof shall not be used to endorse or promote products
     which make use of the PROFILE without prior written permission from
     Color Solutions.
 .
 THIS PROFILE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 NO EVENT SHALL COLOR SOLUTIONS SOFTWARE BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS PROFILE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: HEIDELBERG
 License terms ("LICENSE") for the use of HEIDELBERG ICC profiles with profile
 properties as defined in this license
 1 Definition
 1.1 For generic ICC profiles for the profiling of standard printing conditions
 which have been produced with software of Heidelberger Druckmaschinen AG
 ("HEIDELBERG") ("PROFILES"), HEIDELBERG agrees to a free distribution and use
 of the PROFILES, on condition that the distributor and/or user ("LICENSEE")
 agrees to the following terms.
 1.2 Standard printing conditions within the meaning of this LICENSE are
 standards defined by international or national standardization bodies such as
 ISO 12647 and the respective parts thereof or are a de facto standards in
 certain regions of the world.
 2 Scope of the Right of Use
 2.1 HEIDELBERG grants the LICENSEE a non-exclusive right to use the PROFILES.
 2.2 The LICENSEE is allowed to redistribute the PROFILES and to transfer this
 Right of Use to a third party on condition that these PROFILES are always
 distributed together with this LICENSE and that the third party accepts that
 the license transferred by the LICENSEE is subject to the terms of this
 LICENSE. The PROFILES may be redistributed also bundled together with other
 software.
 3 Exclusion of other Use
 3.1 It is explicitly prohibited to charge any kind of remuneration for the
 distribution of these PROFILES.
 3.2 It is not allowed to edit these PROFILES or to change the entries in the
 profile tags or to use the inherent technical solutions for any purpose other
 than the actual purpose of an ICC profile. If the PROFILES will be sold
 bundled together with commercial software, then it is not allowed to charge an
 extra price for the PROFILES.
 4 Warranty and Liability
 4.1 The PROFILES are provided "as is" free of charge and devoid of all claims
 against HEIDELBERG or the distributor of these PROFILES.
 4.2 This LICENSE excludes all liability, including the liability for damages.
 5 Copyright
 The copyright on the PROFILES remains with HEIDELBERG.
 6 General Terms
 6.1 The Right of Use as described in point 2.1 is void with immediate effect
 upon violation of this LICENSE.
 6.2 This LICENSE applies explicitly to generic profiles only for the profiling
 of printing standards, as specified in point 1. This LICENSE expressly does
 not apply to profiles produced with HEIDELBERG software which profile the
 properties of a specific device.
 .
 Heidelberg, September 2003
 .
 Reference:
 http://eci.org/eci/downloads/ECI-en/icc_profiles_from_eci/ECI_Offset_2004.zip
 http://eci.org/eci/downloads/ECI-en/icc_profiles_from_eci/eciRGBv10.zip

License: sRGB
 Profile Licensing Agreement: To anyone who acknowledges that the file
 "sRGB Color Space Profile.icm" is provided "AS IS" WITH NO EXPRESS OR IMPLIED
 WARRANTY: permission to use, copy and distribute this file for any purpose is
 hereby granted without fee, provided that the file is not changed including
 the HP copyright notice tag, and that the name of Hewlett-Packard Company not
 be used in advertising or publicity pertaining to distribution of the software
 without specific, written prior permission. Hewlett-Packard Company makes no
 representations about the suitability of this software for any purpose.
 .
 Reference: http://web.archive.org/web/20020603153925/srgb.com/usingsrgb.html

License: CC-BY-ND
 Attribution-NonCommercial-NoDerivs 2.5
 .
 http://creativecommons.org/licenses/by-nd/2.5/legalcode
 .
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS LICENSE DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE INFORMATION PROVIDED, AND
 DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM ITS USE.
 .
 License
 .
 THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE
 COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY
 COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS
 AUTHORIZED UNDER THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.
 .
 BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO
 BE BOUND BY THE TERMS OF THIS LICENSE. THE LICENSOR GRANTS YOU THE RIGHTS
 CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND
 CONDITIONS.
 .
 1. Definitions
   1. "Collective Work" means a work, such as a periodical issue, anthology
 or encyclopedia, in which the Work in its entirety in unmodified form, along
 with a number of other contributions, constituting separate and independent
 works in themselves, are assembled into a collective whole. A work that
 constitutes a Collective Work will not be considered a Derivative Work (as
 defined below) for the purposes of this License.
   2. "Derivative Work" means a work based upon the Work or upon the Work and
 other pre-existing works, such as a translation, musical arrangement,
 dramatization, fictionalization, motion picture version, sound recording, art
 reproduction, abridgment, condensation, or any other form in which the Work
 may be recast, transformed, or adapted, except that a work that constitutes a
 Collective Work will not be considered a Derivative Work for the purpose of
 this License. For the avoidance of doubt, where the Work is a musical
 composition or sound recording, the synchronization of the Work in
 timed-relation with a moving image ("synching") will be considered a
 Derivative Work for the purpose of this License.
   3. "Licensor" means the individual or entity that offers the Work under the
 terms of this License.
   4. "Original Author" means the individual or entity who created the Work.
   5. "Work" means the copyrightable work of authorship offered under the
 terms of this License.
   6. "You" means an individual or entity exercising rights under this License
 who has not previously violated the terms of this License with respect to the
 Work, or who has received express permission from the Licensor to exercise
 rights under this License despite a previous violation.
 .
 2. Fair Use Rights. Nothing in this license is intended to reduce, limit, or
 restrict any rights arising from fair use, first sale or other limitations on
 the exclusive rights of the copyright owner under copyright law or other
 applicable laws.
 .
 3. License Grant. Subject to the terms and conditions of this License,
 Licensor hereby grants You a worldwide, royalty-free, non-exclusive, perpetual
 (for the duration of the applicable copyright) license to exercise the rights
 in the Work as stated below: 
 .
   1. to reproduce the Work, to incorporate the Work into one or more
 Collective Works, and to reproduce the Work as incorporated in the Collective
 Works;
   2. to distribute copies or phonorecords of, display publicly, perform
 publicly, and perform publicly by means of a digital audio transmission the
 Work including as incorporated in Collective Works;
   3. The above rights may be exercised in all media and formats whether now
 known or hereafter devised. The above rights include the right to make such
 modifications as are technically necessary to exercise the rights in other
 media and formats, but otherwise you have no rights to make Derivative Works.
 All rights not expressly granted by Licensor are hereby reserved, including
 but not limited to the rights set forth in Sections 4(d) and 4(e).
 .
 4. Restrictions.The license granted in Section 3 above is expressly made
 subject to and limited by the following restrictions:
   a. You may distribute, publicly display, publicly perform, or publicly
 digitally perform the Work only under the terms of this License, and You must
 include a copy of, or the Uniform Resource Identifier for, this License with
 every copy or phonorecord of the Work You distribute, publicly display,
 publicly perform, or publicly digitally perform. You may not offer or impose
 any terms on the Work that alter or restrict the terms of this License or the
 recipients' exercise of the rights granted hereunder. You may not sublicense
 the Work. You must keep intact all notices that refer to this License and to
 the disclaimer of warranties. You may not distribute, publicly display,
 publicly perform, or publicly digitally perform the Work with any
 technological measures that control access or use of the Work in a manner
 inconsistent with the terms of this License Agreement. The above applies to
 the Work as incorporated in a Collective Work, but this does not require the
 Collective Work apart from the Work itself to be made subject to the terms of
 this License. If You create a Collective Work, upon notice from any Licensor
 You must, to the extent practicable, remove from the Collective Work any
 credit as required by clause 4(c), as requested.
   b. You may not exercise any of the rights granted to You in Section 3 above
 in any manner that is primarily intended for or directed toward commercial
 advantage or private monetary compensation. The exchange of the Work for other
 copyrighted works by means of digital file-sharing or otherwise shall not be
 considered to be intended for or directed toward commercial advantage or
 private monetary compensation, provided there is no payment of any monetary
 compensation in connection with the exchange of copyrighted works.
   c. If you distribute, publicly display, publicly perform, or publicly
 digitally perform the Work, You must keep intact all copyright notices for the
 Work and provide, reasonable to the medium or means You are utilizing: (i) the
 name of the Original Author (or pseudonym, if applicable) if supplied, and/or
 (ii) if the Original Author and/or Licensor designate another party or parties
 (e.g. a sponsor institute, publishing entity, journal) for attribution in
 Licensor's copyright notice, terms of service or by other reasonable means,
 the name of such party or parties; the title of the Work if supplied; and to
 the extent reasonably practicable, the Uniform Resource Identifier, if any,
 that Licensor specifies to be associated with the Work, unless such URI does
 not refer to the copyright notice or licensing information for the Work. Such
 credit may be implemented in any reasonable manner; provided, however, that in
 the case of a Collective Work, at a minimum such credit will appear where any
 other comparable authorship credit appears and in a manner at least as
 prominent as such other comparable authorship credit.
 .
   d. For the avoidance of doubt, where the Work is a musical composition:
 .
     i. Performance Royalties Under Blanket Licenses. Licensor reserves the
 exclusive right to collect, whether individually or via a performance rights
 society (e.g. ASCAP, BMI, SESAC), royalties for the public performance or
 public digital performance (e.g. webcast) of the Work if that performance is
 primarily intended for or directed toward commercial advantage or private
 monetary compensation.
     ii. Mechanical Rights and Statutory Royalties. Licensor reserves the
 exclusive right to collect, whether individually or via a music rights agency
 or designated agent (e.g. Harry Fox Agency), royalties for any phonorecord You
 create from the Work ("cover version") and distribute, subject to the
 compulsory license created by 17 USC Section 115 of the US Copyright Act (or
 the equivalent in other jurisdictions), if Your distribution of such cover
 version is primarily intended for or directed toward commercial advantage or
 private monetary compensation.
 .
   e. Webcasting Rights and Statutory Royalties. For the avoidance of doubt,
 where the Work is a sound recording, Licensor reserves the exclusive right to
 collect, whether individually or via a performance-rights society (e.g.
 SoundExchange), royalties for the public digital performance (e.g. webcast)
 of the Work, subject to the compulsory license created by 17 USC Section 114
 of the US Copyright Act (or the equivalent in other jurisdictions), if Your
 public digital performance is primarily intended for or directed toward
 commercial advantage or private monetary compensation.
 .
 5. Representations, Warranties and Disclaimer
 .
 UNLESS OTHERWISE MUTUALLY AGREED BY THE PARTIES IN WRITING, LICENSOR OFFERS
 THE WORK AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND
 CONCERNING THE WORK, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING,
 WITHOUT LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A
 PARTICULAR PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER
 DEFECTS, ACCURACY, OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT
 DISCOVERABLE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED
 WARRANTIES, SO SUCH EXCLUSION MAY NOT APPLY TO YOU.
 .
 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW,
 IN NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY
 SPECIAL, INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT
 OF THIS LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGES.
 .
 7. Termination
 .
   1. This License and the rights granted hereunder will terminate
 automatically upon any breach by You of the terms of this License. Individuals
 or entities who have received Collective Works from You under this License,
 however, will not have their licenses terminated provided such individuals or
 entities remain in full compliance with those licenses. Sections 1, 2, 5, 6,
 7, and 8 will survive any termination of this License.
   2. Subject to the above terms and conditions, the license granted here is
 perpetual (for the duration of the applicable copyright in the Work).
 Notwithstanding the above, Licensor reserves the right to release the Work
 under different license terms or to stop distributing the Work at any time;
 provided, however that any such election will not serve to withdraw this
 License (or any other license that has been, or is required to be, granted
 under the terms of this License), and this License will continue in full force
 and effect unless terminated as stated above.
 .
 8. Miscellaneous
 .
   a. Each time You distribute or publicly digitally perform the Work or a
 Collective Work, the Licensor offers to the recipient a license to the Work on
 the same terms and conditions as the license granted to You under this
 License.
   b. If any provision of this License is invalid or unenforceable under
 applicable law, it shall not affect the validity or enforceability of the
 remainder of the terms of this License, and without further action by the
 parties to this agreement, such provision shall be reformed to the minimum
 extent necessary to make such provision valid and enforceable.
   c. No term or provision of this License shall be deemed waived and no
 breach consented to unless such waiver or consent shall be in writing and
 signed by the party to be charged with such waiver or consent.
   d. This License constitutes the entire agreement between the parties with
 respect to the Work licensed here. There are no understandings, agreements or
 representations with respect to the Work not specified here. Licensor shall
 not be bound by any additional provisions that may appear in any communication
 from You. This License may not be modified without the mutual written
 agreement of the Licensor and You.
 .
 Reference: http://photogamut.org/E_ICC_profile.html

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

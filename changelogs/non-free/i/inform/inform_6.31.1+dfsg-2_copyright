Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Inform6
Upstream-Contact: Graham Nelson
Source: ftp://ftp.ifarchive.org/if-archive/infocom/compilers/inform6/
    Several files were removed from the Inform 6.31 tarball due to missing or
    unclear licensing and redistribution terms.
Disclaimer:
    The license conditions of this work, in aggregate, fail to grant
    the freedom to distribute modified works (required by DFSG §3) and
    freedom to redistribute for a fee (required by DFSG §1).
    .
    The result is a work redistributable by the Debian project only in
    the “non-free” archive.

Files: *
Copyright:
    © 1993–2001 Graham Nelson
License: Inform

Files: inform_DM4.pdf
Copyright:
    © 1993–2001 Graham Nelson
License: Inform

License: Inform
    Inform, the program and its source code, its example games and documentation,
    including the text and typography of this book, are copyright (C) Graham Nelson 1993,
    94, 95, 96, 97, 98, 99, 2000, 01.
    .
    The Inform software may be freely distributed provided that: (a) distributed copies
    are not substantially different from those archived by the author, (b) this and other
    copyright messages are always retained in full, and (c) no profit is involved. The
    same conditions apply to distribution or conversion of any electronic form of this book
    which may be made available by the author. Exceptions to these conditions must be
    negotiated directly with the author.
    .
    A story file produced with the Inform system belongs to whoever wrote it and may be
    sold for profit if so desired, without the need for royalty payment, provided that it prints
    a game banner conforming to the standard library's banner at an early stage in play: in
    particular, this banner must contain the information that the story file was compiled by
    Inform, and the version numbers of compiler and library used.
    .
    The author assumes no liability for errors and omissions in this book, or for damages
    or loss of revenue resulting from the use of the information contained herein, or the use
    of any of the software described herein. Inform software is supplied ``as is'' and carries
    no warranty, actual or implied.

Files: debian/*
Copyright:
    © 2015 Ben Finney <ben+debian@benfinney.id.au>
    © 2008 Jan Christoph Nordholz <hesso@pool.math.tu-berlin.de>
License: GPL-3+
License-Grant:
    This is free software; you may copy, modify, and/or distribute this
    work under the terms of the GNU General Public License as published
    by the Free Software Foundation; version 3 of that License or later.
    No warranty expressed or implied.

Files: inform-6.31.1/contrib/*
Copyright:
    © 1999–2000, 2002 Rupert Lane
    © 1997–1998 Michael Fessler
    © 1996 Gareth Rees
License: GPL-2+
License-Grant:
    inform-mode is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2, or (at your option)
    any later version.
    .
    inform-mode is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

Files: inform_BG.pdf
Copyright:
    © 2004 Roger Firth <roger@firthworks.com>
    © 2004 Sonja Kesserich <polilla@idecnet.com>
License: custom-beginnersguide
    This book and its associated example games are copyright (C) Roger Firth and
    Sonja Kesserich 2004. Their electronic forms may be freely distributed provided
    that: (a) distributed copies are not substantially different from those archived by
    the authors, (b) this and other copyright messages are always retained in full, and
    (c) no profit is involved. Exceptions to these conditions must be negotiated
    directly with the authors (roger@firthworks.com and polilla@idecnet.com).
    .
    The authors assume no liability for errors and omissions in this book, or for
    damages or loss of revenue resulting from the use of the information contained
    herein, or the use of any of the software described herein.

Files: inform-6.31.1/include/calyx_adjectives.h
Copyright: none
License: public-domain
    Written in 1997 by Miron Schmidt / Calyx. Placed in the
    Public Domain.

Files: inform-6.31.1/include/daemons.h
Copyright: none
License: public-domain
    This file is in the public domain.

Files: inform-6.31.1/include/easydoors.h
Copyright:
    © 2000–2001 Andrew MacKinnon
License: custom-easydoors
    May be distributed freely, proprietary incorporation allowed
    May be embedded in any game file created with Inform
    Credit for use is appreciated but not required

Files: inform-6.31.1/include/hintsms.h
Copyright:
    © 2001 Peer Schaefer
License: custom-hintsms
    Unaltered distribution of this sourcecode without profit allowed.
    Use it freely within your own games, commercial or otherwise.
    It would be nice if you give me some credit, but it's not required.

Files: inform-6.31.1/include/links.h  inform-6.31.1/include/links.inf
Copyright:
  © Jayson Smith
License: custom-links
    This library and the sample game may be distributed freely.  Having said
    that, if you use any of this code in a game, I'd appreciate at least a
    "Thank you" in your game.

Files: inform-6.31.1/include/longint.h
Copyright:
    © 1997–1998 Chris Hall <c@pobox.co.uk>
    © 1997–1998 Francis Irving <francis@pobox.co.uk>
License: custom-longint
    This source code is distributed free, but remains
    Copyright 1997-1998 Chris Hall and Francis Irving.

Files: inform-6.31.1/include/money.h
Copyright:
    © 1997 Erik Hetzner <egh@uclink4.berkeley.edu>
License: custom-money
    This file may be copied, modified and distributed freely, so long
    as this notice remains. If it is modified, I ask that you change
    the name somehow to indicate that changes have been made, and give
    both you and me credit. You may include this library in any game so
    long as you give me credit somewhere.

Files: inform-6.31.1/include/nameable.h
Copyright:
    © John Colagioia <JColagioia@csi.com>
License: custom-nameable
    Note:  With no licensing notice to the contrary in "Balances," it can be expected
    to be under copyright.  Therefore, the portions of this code should be considered
    as such until notified otherwise by Graham Nelson.  Any modifications to that code
    made by John Colagioia, however, are in the Public Domain, for whatever that's
    worth.

Files: inform-6.31.1/include/newbiegrammar.h
Copyright:
    © Emily Short <emshort@mindspring.com>
License: custom-newbiegrammar
    This library file may be treated as public domain.  It may be
    included with or without credit to the original author.  It may be
    modified at the user's discretion.  It may be freely redistributed.  

Files: inform-6.31.1/include/printtime.h  inform-6.31.1/include/timepiece.h
Copyright:
    © 1996 Erik Hetzner <egh@raisin.bagel.org>
License: GPL-1+
License-Grant:
    This program (can it be called that?) is released in the hopes
    that it will be useful, but without a warranty of any kind.
    It is distributed under the GNU General Public License.

Files: inform-6.31.1/include/trinitystat.h
Copyright:
    © 2000 Jonathan Rosebaugh
License: GPL-2
    Copyright (C) 2000 by Jonathan Rosebaugh. Released under GPL version 2.
    see http://www.gnu.org/copyleft/gpl.html

Files: inform-6.31.1/include/whatis.h
Copyright:
    © 1996 A.C. Murie
License: custom-whatis
    This code may be freely distributed and used in any
    program, commercial or otherwise. It would be nice if you
    give me some credit, but it is not required. Feel free to
    modify it if you want.


License: GPL-1+
    On Debian systems, the complete text of the GNU General Public
    License version 1 can be found in ‘/usr/share/common-licenses/GPL-1’.

License: GPL-2+
    On Debian systems, the complete text of the GNU General Public
    License version 2 can be found in ‘/usr/share/common-licenses/GPL-2’.

License: GPL-3+
    On Debian systems, the complete text of the GNU General Public
    License version 3 can be found in ‘/usr/share/common-licenses/GPL-3’.

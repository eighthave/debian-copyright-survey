Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: ftp://emboss.open-bio.org/pub/EMBOSS/PHYLIPNEW-3.69.tar.gz
Disclaimer: This package is not part of the Debian operating system.
 The embassy-phylip source and binary Debian packages are part of the
 “non-free” area in the Debian archive. The packages in this area are not part
 of the Debian system, although they have been configured for use with Debian.
 We nevertheless support the use of the seaview source and binary Debian
 packages and provide infrastructure for non-free packages (such as our bug
 tracking system and mailing lists).  The licence of PHYLIPNEW does not forbid
 Debian from using autobuilders to create binary packages.
Comment: It was confirmed on the EMBOSS User mailing list that the COPYING file
 (GNU GPL) is there by error and the Phylip copyright file applies on all the
 package. http://lists.open-bio.org/pipermail/emboss/2009-August/003713.html

Files: *
Copyright: © 1993-2004 by the University of Washington.
License: Phylip
 Written by Joseph Felsenstein, Akiko Fuseki, Sean Lamont, and Andrew Keeffe.
 Permission is granted to copy and use this program provided no fee is
 charged for it and provided that this copyright notice is not removed. 

Files: doc/* emboss-doc/*
Copyright: 1986-2002 by the University of Washington.
License: Phylip-doc
 Written by Joseph Felsenstein. Permission is granted to copy this document
 provided that no fee is charged for it and that this copyright notice is not
 removed. 

Files: debian/*
Copyright: 2009–2011 Charles Plessy <plessy@debian.org>
License: BOLA-1.0
 This work is provided 'as-is', without any express or implied warranty. In no
 event will the authors be held liable for any damages arising from the use of
 this work.
 .
 To all effects and purposes, this work is to be considered Public Domain.
 .
 .
 However, if you want to be "buena onda", you should:
 .
  1. Not take credit for it, and give proper recognition to the authors.
  2. Share your modifications, so everybody benefits from them.
  3. Do something nice for the authors.
  4. Help someone who needs it.
  5. Don't waste. Anything, but specially energy that comes from natural
     non-renewable resources.
  6. Be tolerant. Everything that's good in nature comes from cooperation.

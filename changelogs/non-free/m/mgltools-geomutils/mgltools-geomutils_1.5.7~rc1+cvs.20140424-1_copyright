This package was debianized by Steffen Moeller <moeller@debian.org> on
Fri, 18 Jul 2008 21:57:59 +0200

It was downloaded from http://mgltools.scripps.edu

Upstream Authors: 

    The main development is performed by the researchers
    and developers at the

	    Molecular Graphics Laboratory <mgltools@scripps.edu>

    led by

	    Michael F. Sanner <sanner@scripps.edu>

    with contributions by 

	    Anna Omelchenko <annao@scripps.edu>
	    Guillaume Vareille <vareille@scripps.edu>
	    Sophie Cohn
	    qzhang
	    Michael F. Sanner
	    Sowjanya Karnati
            Alex T. Gillet

    Major contributions in src/geomAlgorithms/*.{h,cpp} are 
    authored by Dan Sunday of softSurfer (www.softsurfer.com.

Copyright: 

    The respective authors and the Scripps Research Institute
    Copyright 1989-2013 Michael Sanner <sanner@scripps.edu>

    Major contributions in src/geomAlgorithms/*.{h,cpp} are 
    Copyright (C) 2001-2002 softSurfer (www.softsurfer.com)

License:

All files distributed or redistributed in this package are subject
to the here presented license, regardless of the information
that is explicitly stated or omitted in the respective files
themselves. The original copyright holders have made respective
agreements regarding this redistribution.


  1. Grant Of Limited License; Software Use Restrictions. The programs
     received by you will be used only for NON COMMERCIAL purposes.
     This license is issued to you as an individual.

     For COMMERCIAL use done with the software please contact Michel F. 
     Sanner for details about commercial usage license agreements.
     
     For any question regarding license agreements, please contact
             Michel Sanner:
             TSRI, Molecular Biology Department, TCP 26,
             10550 North Torrey Pines Road, La Jolla, CA 92037
             sanner@scripps.edu
             tel (858) 784-7742
             fax (858) 784-2341

  2. COMMERCIAL USAGE is defined as revenues generating activities. These
     include using this software for consulting activities and selling
     applications built on top of, or using this software. Scientific 
     research in an academic environment and teaching are considered 
     NON COMMERCIAL.

  3. Copying Restrictions. You will not sell or otherwise distribute commercially 
     these programs or derivatives to any other party, whether with or without 
     consideration.

  4. Ownership of Software. You will not obtain, and will not attempt to 
     obtain copyright coverage thereon without the express purpose written 
     consent of The Scripps Research Institute and Dr. Sanner.

  5. IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
     FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
     ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
     DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
     POSSIBILITY OF SUCH DAMAGE.

  6. THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
     INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
     FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
     IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
     NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
     MODIFICATIONS.

The contributions by softSurfer are put under the following license:

    This code may be freely used and modified for any purpose
    providing that this copyright notice is included with it.
    SoftSurfer makes no warranty for this code, and cannot be held
    liable for any real or imagined damage resulting from it's use.
    Users of this code must verify correctness for their application.

The Debian packaging is Copyright (C) 2007-2008, 
Steffen Moeller <moeller@debian.org> 
and is licensed under the GPL, see '/usr/share/common-licenses/GPL'.

In addition to the license above, the Debian project also got the explicit
permission from the copyright owner to distribute the software:
  Date: Mon, 20 May 2013 10:07:13 -0700
  From: Michel Sanner <sanner@scripps.edu>
  To: Thorsten Alteholz <debian-med@alteholz.de>
  Cc: "annao >> Anna Omelchenko" <annao@scripps.edu>
  Subject: Re: mgltools license

  Hello Thorsten

  Sorry for not following up this earlier. We are delighted to have Debian
  include the MGLTools in their distribution.
  So you need us to amend the License text or will this email suffice ?

  Thanks

  --

  -----------------------------------------------------------------------
     o
    /    Michel F. Sanner Ph.D.            The Scripps Research Institute
  o     Associate Professor               Department of Molecular Biology
    \                                      10550 North Torrey Pines Road
     o   Tel. (858) 784-7742               La Jolla, CA 92037, TPC 26
    /    Fax. (858) 784-2341
  o     sanner@scripps.edu                http://www.scripps.edu/~sanner
  -----------------------------------------------------------------------


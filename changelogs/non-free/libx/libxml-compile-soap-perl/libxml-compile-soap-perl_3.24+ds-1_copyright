Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Mark Overmeer <markov@cpan.org>
Upstream-Name: XML-Compile-SOAP
Source: https://metacpan.org/release/XML-Compile-SOAP
Files-Excluded: examples/temperature/convert.wsdl
Disclaimer: This package is not part of the Debian operating system.
 The libxml-compile-soap-perl source and binary Debian packages are part of the
 "non-free" area in our archive. The packages in this area are not part of the
 Debian system, although they have been configured for use with Debian. We
 nevertheless support the use of the libxml-compile-soap-perl source and binary
 Debian packages and provide infrastructure for non-free packages (such as our
 bug tracking system and mailing lists).
 .
 This package is part of the "non-free" area of the Debian archive because the
 following files are released under a license which does not permit
 modification:
 .
 -  lib/XML/Compile/WSDL11/xsd/wsdl-soap.xsd
 .
 Additionally, the upstream tarball is repackaged, because of an unknown
 license for
 .
 - examples/temperature/convert.wsdl

Files: *
Copyright: 2008-2018, Mark Overmeer <markov@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2016, Nick Morrott <knowledgejunkie@gmail.com>
License: Artistic or GPL-1+

Files: examples/document/namesservice.wsdl
       examples/document/namesservice.xsd
Copyright: 2007, Thomas Bayer <info@thomas-bayer.com>
License: Apache-2
Comment: The following explicit licensing information was received from Thomas
 Bayer as no license details were included in the source when packaging.
 .
 > ---------- Forwarded message ----------
 > From: Thomas Bayer <info@thomas-bayer.com>
 > Date: 20 March 2016 at 15:30
 > Subject: Re: License status of xsd/wsdl from namesservice.thomas-bayer.com
 > To: Nick Morrott <knowledgejunkie@gmail.com>
 >
 >
 > Hi,
 > you can redistribute the files under the ASF 2 licence.
 >
 > Cheers,
 > Thomas

Files: lib/XML/Compile/SOAP11/xsd/soap-encoding.xsd
       lib/XML/Compile/SOAP11/xsd/soap-envelope.xsd
Copyright: 2001, W3C (MIT, INRIA, Keio University)
 2001, DevelopMentor
License: W3C

Files: lib/XML/Compile/WSDL11/xsd/wsdl-soap.xsd
Copyright: 2001-2005, International Business Machines Corporation and Microsoft Corporation
License: License-for-WSDL-Schema-Files
Comment: Files released under this license do not permit modification

Files: lib/XML/Compile/XOP/xsd/200411-xmlmime.xsd
Copyright: 2004, W3C (MIT, INRIA, Keio University)
License: W3C

Files: lib/XML/Compile/XOP/xsd/200505-xmlmime.xsd
Copyright: 2005, W3C (MIT, INRIA, Keio University)
License: W3C

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License.
 .
 On Debian systems, the complete text of version 2 of the Apache
 License can be found in `/usr/share/common-licenses/Apache-2.0'.

License: W3C
 By obtaining, using and/or copying this work, you (the licensee) agree
 that you have read, understood, and will comply with the following terms
 and conditions.
 .
 Permission to copy, modify, and distribute this software and its
 documentation, with or without modification, for any purpose and without
 fee or royalty is hereby granted, provided that you include the following
 on ALL copies of the software and documentation or portions thereof,
 including modifications:
 .
 - The full text of this NOTICE in a location viewable to users of the
   redistributed or derivative work.
 - Any pre-existing intellectual property disclaimers, notices, or terms
   and conditions. If none exist, the W3C Software Short Notice should be
   included (hypertext is preferred, text is permitted) within the body of
   any redistributed or derivative code.
 - Notice of any changes or modifications to the files, including the date
   changes were made. (We recommend you provide URIs to the location from
   which the code is derived.)
 .
 Disclaimers
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS
 MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR
 PURPOSE OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY
 THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in advertising or
 publicity pertaining to the software without specific, written prior
 permission. Title to copyright in this software and any associated
 documentation will at all times remain with copyright holders.
 .
 Notes
 .
 This version: http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231
 .
 This formulation of W3C's notice and license became active on December 31
 2002. This version removes the copyright ownership notice such that this
 license can be used with materials other than those owned by the W3C, reflects
 that ERCIM is now a host of the W3C, includes references to this specific
 dated version of the license, and removes the ambiguous grant of "use".
 Otherwise, this version is the same as the previous version and is written so
 as to preserve the Free Software Foundation's assessment of GPL compatibility
 and OSI's certification under the Open Source Definition.

License: License-for-WSDL-Schema-Files
 The Authors grant permission to copy and distribute the WSDL Schema
 Files in any medium without fee or royalty as long as this notice and
 license are distributed with them.  The originals of these files can
 be located at:
 .
 http://schemas.xmlsoap.org/wsdl/soap/2003-02-11.xsd
 .
 THESE SCHEMA FILES ARE PROVIDED "AS IS," AND THE AUTHORS MAKE NO REPRESENTATIONS
 OR WARRANTIES, EXPRESS OR IMPLIED, REGARDING THESE FILES, INCLUDING, BUT NOT
 LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 NON-INFRINGEMENT OR TITLE.  THE AUTHORS WILL NOT BE LIABLE FOR ANY DIRECT,
 INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR
 RELATING TO ANY USE OR DISTRIBUTION OF THESE FILES.
 .
 The name and trademarks of the Authors may NOT be used in any manner,
 including advertising or publicity pertaining to these files or any program
 or service that uses these files, written prior permission.  Title to copyright
 in these files will at all times remain with the Authors.
 .
 No other rights are granted by implication, estoppel or otherwise.

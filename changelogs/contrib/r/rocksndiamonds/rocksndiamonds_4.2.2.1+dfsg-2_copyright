Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rocksndiamonds
Source: http://artsoft.org/
Files-Excluded: build-projects/android graphics levels lib music rocksndiamonds scores sounds
Disclaimer:
 This package is not part of the Debian distribution. The license for
 the game data is unclear, so it has been removed from the Debian
 package. As a result the package is provided in the contrib archive
 area: it can not run on its own, and offers to retrieve the necessary
 data from the upstream web site during installation.

Files: *
Copyright: 1995-2019 Artsoft Entertainment, Holger Schemel
License: GPL-2+

Files: debian/*
Copyright: 2006-2011 Dmitry E. Oboukhov
           2012 Christian Perrier
           2012 Igor Pashev
           2015, 2017-2020 Stephen Kitt
License: GPL-2+

Files: debian/rocksndiamonds.png
Copyright: CyberSkull
Comment: Downloaded from
 https://commons.wikimedia.org/wiki/File:Rocks%27n%27Diamonds.png
 and adapted to provide rocksdiamonds.xpm.
License: GPL-2+

Files: src/libgame/hash.*
Copyright: 2002 Christopher Clark
           1995-2014 Artsoft Entertainment, Holger Schemel
License: Expat-Clark
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies of the Software and its documentation and
 acknowledgment shall be given in the documentation and software
 packages that this Software was used.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/libgame/random.c
Copyright: 1983 Regents of the University of California
           1995-2014 Artsoft Entertainment, Holger Schemel
License: BSD-4
 Redistribution and use in source and binary forms are permitted
 provided that the above copyright notice and this paragraph are
 duplicated in all such forms and that any documentation,
 advertising materials, and other materials related to such
 distribution and use acknowledge that the software was developed
 by the University of California, Berkeley.  The name of the
 University may not be used to endorse or promote products derived
 from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.


License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License, version 2, can be found in
 `/usr/share/common-licenses/GPL-2'; the complete text of the GNU
 General Public License, version 3, can be found in
 `/usr/share/common-licenses/GPL-3'.

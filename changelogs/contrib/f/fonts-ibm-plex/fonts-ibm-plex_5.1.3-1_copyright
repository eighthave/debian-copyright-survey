Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IBM Plex
Source: https://github.com/IBM/plex
Disclaimer:
 IBM releases Plex as "open source", but unfortunately the freedom status of
 the font is less than ideal, as it requires non-free software to be built
 from source (see README.md, Requirements section, and [0]). The problematic
 step is the hinting, which is done with FontLab Studio, which is proprietary
 software. There is currently no Free Software program that is able to work
 with the high-level hinting command language used by FontLab Studio.
 .
 Moreover, the license Plex is released with includes a "Reserved Font Name"
 clause for the "Plex" name, stating that:
 .
  No Modified Version of the Font Software may use the Reserved Font
  Name(s) unless explicit written permission is granted by the corresponding
  Copyright Holder.
 .
 According to the Open Font License FAQ ([1], Question 5.9), a rebuilt font is
 considered a "Modified Version", and requires a name change to be distributed.
 .
 For these two reasons Plex is not distributed in "main" section of the Debian
 archive, but in the "contrib" section.
 .
 [0] https://github.com/IBM/plex/issues/98
 [1] https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl-faq_web
     The OFL-FAQ is at version 1.1-update5 at the time of writing.
Comment:
 "IBM" and "IBM Plex" are trademarks of IBM Corp.
 .
 The short-lived upstream version v1.1.4 was dual-licensed as "OFL-1.1
 or Apache-2.0". The Apache license has been pulled from v1.1.5 because
 of a request of the IBM legal team. There is an upstream issue where these
 licensing changes are discussed: https://github.com/IBM/plex/issues/190.

Files: *
Copyright: 2017-2018 IBM Corporation
License: OFL-1.1
Comment: OFL licensed with Reserved Font Name "Plex".

Files: debian/*
Copyright: 2018-2020 Paride Legovini <paride@debian.org>
License: ISC

Files: debian/*.metainfo.xml
Copyright: 2018-2019 Paride Legovini <pl@ninthfloor.org>
License: CC0-1.0

License: OFL-1.1
 Reserved Font Name "Plex"
 .
 This Font Software is licensed under the SIL Open Font License,
 Version 1.1.
 .
 This license is copied below, and is also available with a FAQ at:
 http://scripts.sil.org/OFL
 .
 -----------------------------------------------------------
 SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007
 -----------------------------------------------------------
 .
 PREAMBLE
 The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font creation
 efforts of academic and linguistic communities, and to provide a free and
 open framework in which fonts may be shared and improved in partnership
 with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded,
 redistributed and/or sold with any software provided that any reserved
 names are not used by derivative works. The fonts and derivatives,
 however, cannot be released under any other type of license. The
 requirement for fonts to remain under this license does not apply
 to any document created using the fonts or their derivatives.
 .
 DEFINITIONS
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software components as
 distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to, deleting,
 or substituting -- in part or in whole -- any of the components of the
 Original Version, by changing formats or by porting the Font Software to a
 new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed, modify,
 redistribute, and sell modified and unmodified copies of the Font
 Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
 in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
 redistributed and/or sold with any software, provided that each copy
 contains the above copyright notice and this license. These can be
 included either as stand-alone text files, human-readable headers or
 in the appropriate machine-readable metadata fields within text or
 binary files as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
 Name(s) unless explicit written permission is granted by the corresponding
 Copyright Holder. This restriction only applies to the primary font name as
 presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
 Software shall not be used to promote, endorse or advertise any
 Modified Version, except to acknowledge the contribution(s) of the
 Copyright Holder(s) and the Author(s) or with their explicit written
 permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
 must be distributed entirely under this license, and must not be
 distributed under any other license. The requirement for fonts to
 remain under this license does not apply to any document created
 using the Font Software.
 .
 TERMINATION
 This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: CC0-1.0
 On Debian systems, the full text of the Creative Commons CC0 1.0
 Universal can be found in the file "/usr/share/common-licenses/CC0-1.0".

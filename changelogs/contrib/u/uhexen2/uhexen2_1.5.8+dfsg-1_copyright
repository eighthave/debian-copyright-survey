Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: uhexen2
Source: <http://uhexen2.sf.net>

Files: *
Copyright: <2000-2016> Ozkan Sezer <sezeroz@gmail.com>
           <1997-1998> Raven Software Corp.
           <1996-1997> Id Software, Inc.
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: common/swap_68k.s
Copyright: Frank Wille <frank@phoenix.owl.de>
License: GPL-2.0+

Files: debian/*
Copyright: <2012-2016> gustavo panizzo <gfa@zumbi.com.ar>
License: GPL-2.0
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: common/strlcpy.c common/strlcat.c
Copyright: <1998-2006> Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  .
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


Files: libs/timidity/*
Copyright: <1995> Tuukka Toivonen <toivonen@clinet.fi>
           <2004> Konstantin Korikov <lostclus@ua.fm>
License: GPL-2.0+

Files: libs/timidity/dls1.h
Copyright: <1996> Sonic Foundry
License: public-domain
  Written by Sonic Foundry 1996.  Released for public use.

Files: libs/timidity/dls2.h
Copyright: <1998> Microsoft
License: public-domain
  Released for public use.

Files: libs/xdelta3/*
Copyright: <2001-2010> Joshua P. MacDonald
License: GPL-2.0+

Files: common/filenames.h
Copyright: <2000-2007> Free Software Foundation, Inc.
License: GPL-2.0+

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Source: https://github.com/worldforge/ember
Comment: This package currently downloads the upstream snap package on install.
 This is due to the current instability of the upstream codebase. Once upstream
 stabilizes, this package source should be returned to Debian. In the meantime,
 since it downloads files from outside the Debian archive, it is in contrib.

Files: debian/*
Copyright: 2009 Michael Koch <konqueror@gmx.de>
           2012 Stephen M. Webb <stephen.webb@bregmasoft.ca>
           2016-2020 Olek Wojnar <olek@debian.org>
License: GPL-3+

Files: debian/ember.desktop
       debian/ember.png
Copyright: 2001-2020 Adam Gregory <Adammgregory@yahoo.com>
                     Alexey Torkhov <atorkhov@gmail.com>
                     Alistair Davidson <lord_inh@yahoo.co.uk>
                     Alistair Riddoch <alriddoch@googlemail.com>
                     Erik Ogenvik (née Hjortsberg) <erik@ogenvik.org>
                     Hans Häggström <zzorn@iki.fi>
                     Lakin Wecker <nikal@nucleus.com>
                     Lee Begg <llnz@paradise.net.nz>
                     Martin Pollard <circlemaster@blueyonder.co.uk>
                     Miguel Guzmán <aglanor@telefonica.net>
                     Rômulo Fernandes Machado Leitão <abra185@gmail.com>
                     Sean Ryan <sryan@evercrack.com>
                     Simon Goodall
                     Tamas Bates <rhymer@gmail.com>
                     Tim Enderling <T.Enderling@web.de>
                     Olek Wojnar <olek@debian.org>
License: GPL-2+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PyOpenCL
Upstream-Contact: Andreas Klöckner <lists@informa.tiker.net>
Source: http://git.tiker.net/trees/pyopencl.git
 http://pypi.python.org/pypi/pyopencl/
 http://pypi.python.org/packages/source/p/pyopencl/

Files: *
Copyright: 2009-2011 Andreas Klöckner <lists@informa.tiker.net>
License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

Files: pyopencl/clrandom.py
Copyright: 1990 RSA Data Security, Inc.
License:
 Copyright (C) 1990, RSA Data Security, Inc. All rights reserved.
 .
 License to copy and use this software is granted provided that
 it is identified as the "RSA Data Security, Inc. MD5 Message
 Digest Algorithm" in all material mentioning or referencing this
 software or this function.
 .
 License is also granted to make and use derivative works
 provided that such works are identified as "derived from the RSA
 Data Security, Inc. MD5 Message Digest Algorithm" in all
 material mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning
 either the merchantability of this software or the suitability
 of this software for any particular purpose.  It is provided "as
 is" without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: pyopencl/reduction.py
Copyright: 2009-2011 Andreas Klöckner <lists@informa.tiker.net>
License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 Based on code/ideas by Mark Harris <mharris@nvidia.com>.
 None of the original source code remains.

Files: pyopencl/compyte/scan.py
Copyright: 2011 Andreas Kloeckner
           2008-2011 NVIDIA Corporation
License: Apache 2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 Derived from thrust/detail/backend/cuda/detail/fast_scan.h
 within the Thrust project, https://code.google.com/p/thrust/

Files: src/cl/pyopencl-airy.cl
       src/cl/pyopencl-cephes.cl
Copyright: 2012 Andreas Kloeckner
           1984-2000 Stephen L. Moshier
License:
 What you see here may be used freely, but it comes with no support or
 guarantee.

Files: src/cl/pyopencl-bessel-j.cl
Copyright: 2012 Andreas Kloeckner
           2006 Xiaogang Zhang, John Maddock
           1984-2000 Stephen L. Moshier
License:
 Xiaogang Zhang, John Maddock:
 Use, modification and distribution are subject to the
 Boost Software License, Version 1.0. (See
 http://www.boost.org/LICENSE_1_0.txt)
 .
 Stephen L. Moshier:
 What you see here may be used freely, but it comes with no support or
 guarantee.

Files: src/cl/pyopencl-complex.h
Copyright: 2012 Andreas Kloeckner
           1999 Silicon Graphics Computer Systems, Inc.
           1999 Boris Fomitchev
License:
 This material is provided "as is", with absolutely no warranty expressed
 or implied. Any use is at your own risk.
 .
 Permission to use or copy this software for any purpose is hereby granted
 without fee, provided the above notices are retained on all copies.
 Permission to modify the code and to distribute modified code is granted,
 provided the above notices are retained, and a notice that the code was
 modified is included with the above copyright notice.

Files: src/cl/pyopencl-ranluxcl.cl
Copyright: 2011 Ivar Ursin Nikolaisen
License:
 Copyright (c) 2011 Ivar Ursin Nikolaisen
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify,
 merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be included in all copies
 or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: examples/matrix-multiply.py
Copyright: 1993-2009 NVIDIA Corporation
License:
 Copyright 1993-2009 NVIDIA Corporation.  All rights reserved.
 .
 NVIDIA Corporation and its licensors retain all intellectual property and
 proprietary rights in and to this software and related documentation.
 Any use, reproduction, disclosure, or distribution of this software
 and related documentation without an express license agreement from
 NVIDIA Corporation is strictly prohibited.
 .
 Please refer to the applicable NVIDIA end user license agreement (EULA)
 associated with this source code for terms and conditions that govern
 your use of this NVIDIA software.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dhewm3
Source: https://github.com/dhewm/dhewm3
Comment: The removed files contained prebuilt binaries and we do not need those.
  The jpeg_memory_src.* have been removed due the missing README -- which
  defines the license for those two files.
Files-Excluded: neo/tools/common/PropTree/* neo/sys/win32/*
     neo/sys/linux/setup/*/bin neo/sys/linux/setup/lokisetup
     neo/sys/linux/setup/image-* neo/sys/linux/setup/makeself/*

Files: *
Copyright: 1999-2011 id Software LLC, a ZeniMax Media company
           2018 Daniel Gibson
Comment: See debian/license-assessment for an assessembent of the additional
 clauses in respect to GPL compatibility.
License: GPL-3-with-idsoft-extra-terms
 Doom 3 Source Code is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 Doom 3 Source Code is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
  .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.
 .
 In addition, the Doom 3 Source Code is also subject to certain additional
 terms. You should have received a copy of these additional terms immediately
 following the terms and conditions of the GNU General Public License which
 accompanied the Doom 3 Source Code.  If not, please request a copy in writing
 from id Software at the address below.
 .
 If you have questions concerning this license or the applicable additional
 terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc.,
 Suite 120, Rockville, Maryland 20850 USA.
 .
 ADDITIONAL TERMS APPLICABLE TO THE DOOM 3 GPL SOURCE CODE.
 .
 The following additional terms (Additional Terms) supplement and
 modify the GNU General Public License, Version 3 (GPL) applicable to the Doom
 3  GPL Source Code (Doom 3 Source Code).  In addition to the terms and
 conditions of the GPL, the Doom 3 Source Code is subject to the further
 restrictions below.
 .
 1. Replacement of Section 15.  Section 15 of the GPL shall be deleted in its
 entirety and replaced with the following:
 .
 15. Disclaimer of Warranty.
 .
 THE PROGRAM IS PROVIDED WITHOUT ANY WARRANTIES, WHETHER EXPRESSED OR IMPLIED,
 INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
 PURPOSE, NON-INFRINGEMENT, TITLE AND MERCHANTABILITY.  THE PROGRAM IS BEING
 DELIVERED OR MADE AVAILABLE 'AS IS', 'WITH ALL FAULTS' AND WITHOUT WARRANTY OR
 REPRESENTATION.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
 PROGRAM IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
 ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 .
 2. Replacement of Section 16.  Section 16 of the GPL shall be deleted in its
 entirety and replaced with the following:
 .
 16.	LIMITATION OF LIABILITY.
 .
 UNDER NO CIRCUMSTANCES SHALL ANY COPYRIGHT HOLDER OR ITS AFFILIATES, OR ANY
 OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE
 LIABLE TO YOU, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, FOR ANY
 DAMAGES OR OTHER LIABILITY, INCLUDING ANY GENERAL, DIRECT, INDIRECT, SPECIAL,
 INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE USE OR INABILITY TO USE THE PROGRAM OR OTHER DEALINGS WITH
 THE PROGRAM(INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED
 INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE
 PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), WHETHER OR NOT ANY COPYRIGHT HOLDER
 OR SUCH OTHER PARTY RECEIVES NOTICE OF ANY SUCH DAMAGES AND WHETHER OR NOT SUCH
 DAMAGES COULD HAVE BEEN FORESEEN.
 .
 3. LEGAL NOTICES; NO TRADEMARK LICENSE; ORIGIN.  You must reproduce faithfully
 all trademark, copyright and other proprietary and legal notices on any copies
 of the Program or any other required author attributions.  This license does not
 grant you rights to use any copyright holder or any other party's name, logo, or
 trademarks.  Neither the name of the copyright holder or its affiliates, or any
 other party who modifies and/or conveys the Program may be used to endorse or
 promote products derived from this software without specific prior written
 permission.  The origin of the Program must not be misrepresented; you must not
 claim that you wrote the original Program.  Altered source versions must be
 plainly marked as such, and must not be misrepresented as being the original
 Program.
 .
 4. INDEMNIFICATION.  IF YOU CONVEY A COVERED WORK AND AGREE WITH ANY RECIPIENT
 OF THAT COVERED WORK THAT YOU WILL ASSUME ANY LIABILITY FOR THAT COVERED WORK,
 YOU HEREBY AGREE TO INDEMNIFY, DEFEND AND HOLD HARMLESS THE OTHER LICENSORS AND
 AUTHORS OF THAT COVERED WORK FOR ANY DAMAEGS, DEMANDS, CLAIMS, LOSSES, CAUSES OF
 ACTION, LAWSUITS, JUDGMENTS EXPENSES (INCLUDING WITHOUT LIMITATION REASONABLE
 ATTORNEYS' FEES AND EXPENSES) OR ANY OTHER LIABLITY ARISING FROM, RELATED TO OR
 IN CONNECTION WITH YOUR ASSUMPTIONS OF LIABILITY.


Files: neo/framework/minizip/*
Copyright: 1999-2011 id Software LLC, a ZeniMax Media company.
           2012 Daniel Gibson
           1998-2010 - by Gilles Vollant and Mathias Svensson
License: zlib

Files: neo/idlib/hashing/MD5.*
Copyright:  1993 Colin Plumb, no copyright is claimed.
License: md5-publicdomain
 This code implements the MD5 message-digest algorithm.
 The algorithm is due to Ron Rivest.  This code was
 written by Colin Plumb in 1993, no copyright is claimed.
 This code is in the public domain; do with it what you wish.

Files: neo/idlib/hashing/MD4.*
Copyright: 1991-92 RSA Data Security, Inc.
License: rsa-md4
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD4 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD4 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: neo/idlib/hashing/CRC32.*
Copyright: 1995-1998 Mark Adler
License: zlib

Files: neo/idlib/Base64.cpp
Copyright: 1996 Lars Wirzenius
License: BSD-2-clause

Files: neo/sys/linux/setup/brandelf.c
Copyright: 1996 Søren Schmidt
License: BSD-2-clause

Files: debian/*
Copyright: 2015-2018 Tobias Frost <tobi@debian.org>
License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".


License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.


License: zlib
 Condition of use and distribution are the same than zlib :
 .
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

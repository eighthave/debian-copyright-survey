Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MaxMind GeoLite database downloader
Upstream-Contact: Ludovico Cavedon <cavedon@debian.org>
Source: http://git.debian.org/?p=collab-maint/geoip-database-contrib.git
Disclaimer: This package does not contain software/data from MaxMind. The
 GeoLite data created by MaxMind will be downloaded from http://www.maxmind.com
 upon installation of this package. Such data is distributed under the
 Creative Commons Attribution-ShareAlike 3.0 Unported License
 (http://creativecommons.org/licenses/by-sa/3.0/).

Files: *
Copyright: 2010/2013, Ludovico Cavedon <cavedon@debian.org>
                      Patrick Matthäi <pmatthaei@debian.org>
License: GPL-2+

Files: debian/po/*
Copyright: 2011, Ludovico Cavedon <cavedon@debian.org>
           2010, Michal Simunek <michal.simunek@gmail.com>
           2011, Helge Kreutzmann <debian@helgefjell.de>
           2010, Software in the Public Interest
           2010, Debian French l10n team <debian-l10n-french@lists.debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 In addition, as a special exception, the author of this program gives
 permission to link the code of its release with the OpenSSL project's
 "OpenSSL" library (or with modified versions of it that use the same license
 as the "OpenSSL" library), and distribute the linked executables. You must
 obey the GNU General Public License in all respects for all of the code used
 other than "OpenSSL".  If you modify this file, you may extend this exception
 to your version of the file, but you are not obligated to do so.  If you do
 not wish to do so, delete this exception statement from your version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License along with
 this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2'.

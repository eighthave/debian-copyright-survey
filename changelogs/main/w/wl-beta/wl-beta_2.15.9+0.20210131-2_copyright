Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Wanderlust
Source: https://github.com/wanderlust/wanderlust

Files: *
Copyright: 1998-2012, Yuuichi Teranishi <teranisi@gohome.org>
           2017, Kazuhiro Ito <kzhr@d1.dion.ne.jp>
           1998-2000, OKUNISHI Fujikazu <fuji0924@mbox.kyoto-inet.or.jp>
           1998-2001, Masahiro MURATA <muse@ba2.so-net.ne.jp>
           1999-2002, Kenichi OKADA <okada@opaopa.org>
           2000-2001, TAKAHASHI Kaoru <kaoru@kaisei.org>
           2000, OKAZAKI Tetsurou <okazaki@be.to>
           1998-2003, Daiki Ueno <ueno@unixuser.org>
           2010-2015, Erik Hetzner <egh@e6h.org>
           1999, Yoshiki Hayashi <yoshiki@xemacs.org>
           2000-2004, Katsumi Yamaoka <yamaoka@jpl.org>
           2003-2004, Hiroya Murata <lapis-lazuli@pop06.odn.ne.jp>
           2002, Yoichi NAKAYAMA <yoichi@eken.phys.nagoya-u.ac.jp>
           2004, Daishi Kato <daishi@axlight.com>
           2000, A. SAGATA <sagata@nttvdt.hil.ntt.co.jp>
           2003, Chihiro Kuroda <chee@iijmio-mail.jp>
           1998-2000, Shun-ichi GOTO <gotoh@taiyo.co.jp>
           1998-2000, Takeshi Chiba <chiba@d3.bs1.fc.nec.co.jp>
           2001, Kitamoto Tsuyoshi <tsuyoshi.kitamoto@city.sapporo.jp>
           1999-2000, TSUMURA Tomoaki <tsumura@kuis.kyoto-u.ac.jp>
           1995-1996, William M. Perry <wmperry@cs.indiana.edu>
           1985-2000, Free Software Foundation, Inc.
           2014-2015, Juliusz Chroboczek <jch@pps.univ-paris-diderot.fr>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: utils/wl-mailto.el
Copyright: 1999, Sen Nagata
License: GPL-2
 License: GPL 2
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: utils/wl-addrbook.el utils/wl-complete.el
Copyright: 1997-2001, Mew developing team
           2001, Masahiro Murata <muse@ba2.so-net.ne.jp>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the team nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE TEAM AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE TEAM OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: doc/txi-*.tex
Copyright: 1999-2020, Free Software Foundation.
License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

Files: doc/texinfo.tex
Copyright: 1985-2020, Free Software Foundation, Inc.
License: GPL-3+ with Texinfo exception
 This texinfo.tex file is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 .
 This texinfo.tex file is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, when this file is read by TeX when processing
 a Texinfo source document, you may use the result without
 restriction. This Exception is an additional permission under section 7
 of the GNU General Public License, version 3 ("GPLv3").

Files: doc/wl-ja.texi doc/wl.texi
Copyright: 1998-2002, Yuuichi Teranishi, Fujikazu Okunishi, Masahiro Murata, Kenichi Okada, Kaoru Takahashi, Bun Mizuhara, Masayuki Osada, Katsumi Yamaoka, Hiroya Murata and Yoichi Nakayama
License: copyleft-wl-texi
 Permission is granted to make and distribute verbatim copies of this
 manual provided the copyright notice and this permission notice are
 preserved on all copies.
 .
 Permission is granted to process this file through TeX and print the
 results, provided the printed document carries copying permission notice
 identical to this one except for the removal of this paragraph (this
 paragraph not being relevant to the printed manual).
 .
 Permission is granted to copy and distribute modified versions of this
 manual under the conditions for verbatim copying, provided that the
 entire resulting derived work is distributed under the terms of a
 permission notice identical to this one.
 .
 Permission is granted to copy and distribute translations of this manual
 into another language, under the above conditions for modified versions.

Files: debian/*
Copyright: 2003-2021, Tatsuya Kinoshita <tats@debian.org>
           1998-2003, Takuo KITAME <kitame@debian.org>
License: permissive-debian
 There is no restriction, so this package may be distributed under
 the same conditions as the upstream.

Files: debian/dot.*
Copyright: 2021, Tatsuya Kinoshita
License: CC0-1.0
 Released under the terms of the CC0 Public Domain Dedication.
 <https://creativecommons.org/publicdomain/zero/1.0/>
Comment:
 On Debian systems, the complete text of the Creative Commons CC0 1.0
 Universal Public Domain Dedication can be found in
 "/usr/share/common-licenses/CC0-1.0".

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wotsap

Files: *
Copyright: Copyright 2003, 2004 Jörgen Cederlöf <jc@lysator.liu.se>
License: GPL-2+

Files: wotsap
Copyright: Copyright 2003, 2004 Jörgen Cederlöf <jc@lysator.liu.se>
           Copyright 2004 Marco Bodrato <gpg@bodrato>
           Copyright 1984-1989, 1994 Adobe Systems Incorporated
           Copyright 1988, 1994 Digital Equipment Corporation
License: GPL-2+ and font-Adobe

Files: debian/*
Copyright: Copyright 2007-2013 Giovanni Mascellani <gio@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in
 `/usr/share/common-licenses/GPL-2'.

License: font-Adobe
 Adobe is a trademark of Adobe Systems Incorporated which may be
 registered in certain jurisdictions.
 Permission to use these trademarks is hereby granted only in
 association with the images described in this file.
  .
 Permission to use, copy, modify, distribute and sell this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notices appear in all
 copies and that both those copyright notices and this permission
 notice appear in supporting documentation, and that the names of
 Adobe Systems and Digital Equipment Corporation not be used in
 advertising or publicity pertaining to distribution of the software
 without specific, written prior permission.  Adobe Systems and
 Digital Equipment Corporation make no representations about the
 suitability of this software for any purpose.  It is provided "as
 is" without express or implied warranty.

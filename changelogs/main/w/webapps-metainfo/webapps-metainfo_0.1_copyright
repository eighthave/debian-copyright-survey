Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: ./metainfo/*
Copyright: Various authors
License: FSFAP

Files: ./icons/im.riot/*
Copyright: Vector/Riot team
License: Apache-2.0

Files: ./icons/io.devdocs/*
Copyright: 2013-2017, Thibaut Courouble
License: MPL-2.0

License: Apache-2.0
 On Debian systems the full text of the Apache Software License 2.0 can be
 found in the `/usr/share/common-licenses/Apache-2.0' file.

License: MPL-2.0
 On Debian systems the full text of the Mozilla Public License Version 2.0 can be
 found in the `/usr/share/common-licenses/MPL-2.0' file.

License: FSFAP
 Copying and distribution of this file, with or without modification, are permitted
 in any medium without royalty provided the copyright notice and this notice are preserved.
 This file is offered as-is, without any warranty.
 .
 See https://spdx.org/licenses/FSFAP.html

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: ./debian/*
 README.md
Copyright: 2017, Matthias Klumpp <mak@debian.org>
License: Expat

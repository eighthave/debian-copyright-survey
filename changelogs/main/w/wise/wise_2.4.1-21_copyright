Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://www.ebi.ac.uk/~birney/wise2/wise2.4.1.tar.gz

Files: *
Copyright: © 1996-2001 Genome Research Limited (GRL) and
 Ewan Birney <birney@sanger.ac.uk> and Sean Eddy
License: GRL
 All of the documentation and software included in the dyc directory of the
 Wise2 package directory is copyrighted by Genome Research Limited
 (GRL) and other individuals where appropriate. This license is modelled
 after the 'BSD' style license and is appropriate for use by both
 academic and commercial sites, without placing any restriction on the
 distribution of source code from other sources with this software.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  1.Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  2.Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
  3.Neither the name GRL nor the names of its contributors may be used
    to endorse or promote products derived from this software without
    specific prior written permission.
 .
 It is considered polite to indicate where appropriate that software which
 is developed using this source code includes a statement that it was developed
 using the Wise2 source code. This is not a requirement however.
 .
 THIS SOFTWARE IS PROVIDED BY THE GRL AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE GRL OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/HMMer2/*
Copyright: © 1992-1996 Sean R. Eddy
License: GPL-2
 This source code is distributed under the terms of the
 GNU General Public License. See the files COPYING and
 GNULICENSE for details.

Files: 
 src/dynlibsrc/histogram.dy src/dynlibsrc/histogram.pod
 src/dynlibsrc/histogram.tex
Copyright: © 1996-1999 Genome Research Limited (GRL) and Sean Eddy
License: LGPL-any
 Converted by Ewan Birney to Dynamite source June 98.
 Copyright is LGPL. For more info read READMEs
Comment: On Debian GNU/Linux systems, the complete text of the GNU Lesser
 General Public License version 3 can be found in `/usr/share/common-licenses/LGPL-3'.

Files: debian/*
Copyright: © 2006-2007 Philipp Benner <philipp@debian.org>
           © 2009 Steffen Moeller <moeller@debian.org>
           © 2009 Charles Plessy <plessy@debian.org>
           © 2009 Barry deFreese <bdefreese@debian.org>
           © 2014-2017 Andreas Tille <tille@debian.org>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian GNU/Linux systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2' and the
 complete text of the GNU Lesser General Public License can be found in
 `/usr/share/common-licenses/LGPL-3'.


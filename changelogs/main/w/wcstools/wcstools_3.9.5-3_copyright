Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wcstools
Source: http://tdc-www.harvard.edu/software/wcstools/
Upstream-Author: Jessica Mink <jmink@cfa.harvard.edu>

Files: * debian/iraf/*
Copyright: Copyright (C) 1996-2011, Smithsonian Astrophysical Observatory, Cambridge, MA USA
License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
Note: The files in debian/iraf/ are taken from the tarfile
 http://tdc-www.harvard.edu/software/wcstools/wcstools-3.9.1.iraf.tar.gz
 written by the upstream author under the same license as wcstools itself,
 and adjusted for the Debian package.

Files: libwcs/*
Copyright: Copyright (C) 1995-2002, Mark Calabretta
 Copyright (C) 1994-2011 Associated Universities, Inc. Washington DC, USA.
 Copyright (C) 1995-2011 Smithsonian Astrophysical Observatory, Cambridge, MA, USA
License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

Files: debian/*
Copyright: not-applicable
License: public-domain
 Debian packaging by Ole Streicher <debian@liska.ath.cx>

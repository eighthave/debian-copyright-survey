Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wcc
Source: <https://github.com/endrazine/wcc.git>
Files-Excluded: doc/WCC_internal_documentation.pdf doc/presentations/Jonathan_Brossard_Witchract_Compiler_Collection_Defcon24_2016.pdf doc/presentations/demos_defcon24_2016/bsd2linux/fmt.c.O0_amd64

Files: *
Copyright: 2016 Jonathan Brossard
           2016 Jonathan Brossard - endrazine@gmail.com
           2014 pc++
License: MIT

Files: src/wsh/luajit-2.0/*
Copyright: 2004-2016 Mike Pall.
           2015 Lua.org, PUC-Rio.
           2015 pc++
License: MIT

Files: src/wsh/linenoise/*
Copyright: 2010-2016, Salvatore Sanfilippo <antirez at gmail dot com>
           2010-2013, Pieter Noordhuis <pcnoordhuis at gmail dot com>
License: BSD-2-clause

Files: src/wsh/openlibm/*
Copyright: 2011-2014 The Julia Project
           2008 Stephen L. Moshier <steve@moshier.net>
           1992-2011 The FreeBSD Project
           1993, 2004 Sun Microsystems, Inc.
           2002-2005 David Schultz <das@FreeBSD.ORG>
	   2005-2016 Steven G. Kargl, David Schultz, Bruce D. Evans
           2013 Elliot Saba
           1993 Bruce D. Evans, Steven G. Kargl, David Schultz
           2009-2011 Bruce D. Evans, Steven G. Kargl, David Schultz
           1993 Dag-Erling Coïdan Smørgrav
           2014 Dahua Lin, 2014. Provided under the MIT license
License: MIT

Files: src/wsh/openlibm/test/test-float.c
       src/wsh/openlibm/test/libm-test.c
       src/wsh/openlibm/test/test-double.c
Copyright: 1997-2001 Free Software Foundation, Inc
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301, USA.

Files: src/wsh/openlibm/ld128/*
       src/wsh/openlibm/include/openlibm_complex.h
       src/wsh/openlibm/ld80/e_expl.c
       src/wsh/openlibm/ld80/e_lgammal_r.c
       src/wsh/openlibm/ld80/e_log10l.c
       src/wsh/openlibm/ld80/e_log2l.c
       src/wsh/openlibm/ld80/e_logl.c
       src/wsh/openlibm/ld80/e_powl.c
       src/wsh/openlibm/ld80/e_tgammal.c
       src/wsh/openlibm/ld80/s_erfl.c
       src/wsh/openlibm/ld80/s_expm1l.c
       src/wsh/openlibm/ld80/s_log1pl.c
       src/wsh/openlibm/src/polevll.c
       src/wsh/openlibm/src/s_cabs.c
       src/wsh/openlibm/src/s_cabsf.c
       src/wsh/openlibm/src/s_cabsl.c
       src/wsh/openlibm/src/s_cacosf.c
       src/wsh/openlibm/src/s_cacosh.c
       src/wsh/openlibm/src/s_cacoshf.c
       src/wsh/openlibm/src/s_cacoshl.c
       src/wsh/openlibm/src/s_cacosl.c
       src/wsh/openlibm/src/s_casin.c
       src/wsh/openlibm/src/s_casinf.c
       src/wsh/openlibm/src/s_casinh.c
       src/wsh/openlibm/src/s_casinhf.c
       src/wsh/openlibm/src/s_casinhl.c
       src/wsh/openlibm/src/s_casinl.c
       src/wsh/openlibm/src/s_catan.c
       src/wsh/openlibm/src/s_catanf.c
       src/wsh/openlibm/src/s_catanh.c
       src/wsh/openlibm/src/s_catanhl.c
       src/wsh/openlibm/src/s_catanl.c
       src/wsh/openlibm/src/s_ccos.c
       src/wsh/openlibm/src/s_ccosf.c
       src/wsh/openlibm/src/s_ccoshl.c
       src/wsh/openlibm/src/s_ccosl.c
       src/wsh/openlibm/src/s_cexpl.c
       src/wsh/openlibm/src/s_clog.c
       src/wsh/openlibm/src/s_clogf.c
       src/wsh/openlibm/src/s_clogl.c
       src/wsh/openlibm/src/s_cpow.c
       src/wsh/openlibm/src/s_cpowf.c
       src/wsh/openlibm/src/s_cpowl.c
       src/wsh/openlibm/src/s_csin.c
       src/wsh/openlibm/src/s_csinf.c
       src/wsh/openlibm/src/s_csinhl.c
       src/wsh/openlibm/src/s_csinl.c
       src/wsh/openlibm/src/s_ctan.c
       src/wsh/openlibm/src/s_ctanf.c
       src/wsh/openlibm/src/s_ctanhl.c
       src/wsh/openlibm/src/s_ctanl.c
       src/wsh/openlibm/src/s_cacos.c
       src/wsh/openlibm/src/s_catanhf.c
Copyright: 2011-2014 The Julia Project
           2008 Stephen L. Moshier <steve@moshier.net>
           1992-2011 The FreeBSD Project
           2002-2005 David Schultz <das@FreeBSD.ORG>
           2002-2005 David Schultz
           2008-2011 Martynas Venckus <martynas@openbsd.org>
           2005-2015 Steven G. Kargl, David Schultz, Bruce D. Evans
           1993,2004 Sun Microsystems, Inc.
License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES WITH REGARD
 TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THIS SOFTWARE.

Files: src/wsh/openlibm/ld128/invtrig.c
       src/wsh/openlibm/ld128/invtrig.h
       src/wsh/openlibm/ld128/s_exp2l.c
       src/wsh/openlibm/ld128/s_nanl.c
       src/wsh/openlibm/amd64/fenv.c
       src/wsh/openlibm/arm/fenv.c
       src/wsh/openlibm/i387/fenv.c
       src/wsh/openlibm/i387/invtrig.c
       src/wsh/openlibm/include/openlibm_fenv_amd64.h
       src/wsh/openlibm/include/openlibm_fenv_arm.h
       src/wsh/openlibm/include/openlibm_fenv_powerpc.h
       src/wsh/openlibm/ld80/invtrig.c
       src/wsh/openlibm/ld80/invtrig.h
       src/wsh/openlibm/ld80/s_exp2l.c
       src/wsh/openlibm/ld80/s_nanl.c
       src/wsh/openlibm/src/aarch64_fpmath.h
       src/wsh/openlibm/src/amd64_fpmath.h
       src/wsh/openlibm/src/e_remainderl.c
       src/wsh/openlibm/src/e_sqrtl.c
       src/wsh/openlibm/src/fpmath.h
       src/wsh/openlibm/src/i386_fpmath.h
       src/wsh/openlibm/src/k_expf.c
       src/wsh/openlibm/src/powerpc_fpmath.h
       src/wsh/openlibm/src/s_carg.c
       src/wsh/openlibm/src/s_cargf.c
       src/wsh/openlibm/src/s_cargl.c
       src/wsh/openlibm/src/s_ccosh.c
       src/wsh/openlibm/src/s_ccoshf.c
       src/wsh/openlibm/src/s_cexp.c
       src/wsh/openlibm/src/s_cexpf.c
       src/wsh/openlibm/src/s_cimag.c
       src/wsh/openlibm/src/s_cimagf.c
       src/wsh/openlibm/src/s_conj.c
       src/wsh/openlibm/src/s_conjf.c
       src/wsh/openlibm/src/s_conjl.c
       src/wsh/openlibm/src/s_copysignl.c
       src/wsh/openlibm/src/s_cproj.c
       src/wsh/openlibm/src/s_cprojf.c
       src/wsh/openlibm/src/s_cprojl.c
       src/wsh/openlibm/src/s_creal.c
       src/wsh/openlibm/src/s_crealf.c
       src/wsh/openlibm/src/s_csinh.c
       src/wsh/openlibm/src/s_csinhf.c
       src/wsh/openlibm/src/s_csqrt.c
       src/wsh/openlibm/src/s_csqrtf.c
       src/wsh/openlibm/src/s_csqrtl.c
       src/wsh/openlibm/src/s_ctanh.c
       src/wsh/openlibm/src/s_ctanhf.c
       src/wsh/openlibm/src/s_exp2.c
       src/wsh/openlibm/src/s_exp2f.c
       src/wsh/openlibm/src/s_fdim.c
       src/wsh/openlibm/src/s_fma.c
       src/wsh/openlibm/src/s_fmaf.c
       src/wsh/openlibm/src/s_fmal.c
       src/wsh/openlibm/src/s_fmax.c
       src/wsh/openlibm/src/s_fmaxf.c
       src/wsh/openlibm/src/s_fmaxl.c
       src/wsh/openlibm/src/s_fmin.c
       src/wsh/openlibm/src/s_fminf.c
       src/wsh/openlibm/src/s_fminl.c
       src/wsh/openlibm/src/s_fpclassify.c
       src/wsh/openlibm/src/s_frexpl.c
       src/wsh/openlibm/src/s_isfinite.c
       src/wsh/openlibm/src/s_isinf.c
       src/wsh/openlibm/src/s_isnan.c
       src/wsh/openlibm/src/s_isnormal.c
       src/wsh/openlibm/src/s_lrint.c
       src/wsh/openlibm/src/s_lround.c
       src/wsh/openlibm/src/s_modfl.c
       src/wsh/openlibm/src/s_nan.c
       src/wsh/openlibm/src/s_nearbyint.c
       src/wsh/openlibm/src/s_rintl.c
       src/wsh/openlibm/src/s_round.c
       src/wsh/openlibm/src/s_roundf.c
       src/wsh/openlibm/src/s_roundl.c
       src/wsh/openlibm/src/s_scalbln.c
       src/wsh/openlibm/src/s_signbit.c
       src/wsh/openlibm/src/s_sinl.c
       src/wsh/openlibm/src/s_tanl.c
       src/wsh/openlibm/src/s_tgammaf.c
       src/wsh/openlibm/amd64/fenv.c
       src/wsh/openlibm/amd64/e_sqrtf.S
       src/wsh/openlibm/amd64/e_sqrtl.S
       src/wsh/openlibm/amd64/e_sqrt.S
       src/wsh/openlibm/amd64/s_llrintl.S
       src/wsh/openlibm/amd64/s_lrintf.S
       src/wsh/openlibm/amd64/s_lrint.S
       src/wsh/openlibm/amd64/s_remquol.S
       src/wsh/openlibm/amd64/s_remquo.S
       src/wsh/openlibm/i387/s_llrintf.S
       src/wsh/openlibm/i387/s_llrintl.S
       src/wsh/openlibm/i387/s_llrint.S
       src/wsh/openlibm/i387/s_lrintf.S
       src/wsh/openlibm/i387/s_lrintl.S
       src/wsh/openlibm/i387/s_remquof.S
       src/wsh/openlibm/i387/s_remquo.S
       src/wsh/openlibm/include/openlibm_fenv_i387.h
       src/wsh/openlibm/powerpc/fenv.c
       src/wsh/openlibm/src/k_exp.c
       src/wsh/openlibm/src/s_cimagl.c
       src/wsh/openlibm/src/s_cosl.c
       src/wsh/openlibm/src/s_creall.c
Copyright: 2011-2014 The Julia Project
           2008 Stephen L. Moshier <steve@moshier.net>
           2014 The FreeBSD Foundation
           1992-2011 The FreeBSD Project
           1993,2004 Sun Microsystems, Inc.
           2002-2011 David Schultz <das@FreeBSD.ORG>
           2003-2007 Steven G. Kargl
           2005 Bruce D. Evans and Steven G. Kargl
           2005-2011 David Schultz
           2004 Stefan Farfeleder
           2003 Mike Barcroft <mike@FreeBSD.org>
License: BSD-2-clause


Files: include/arch.h
       include/config.h
       include/nametoalign.h
       include/nametoentsz.h
       include/nametoinfo.h
       include/nametolink.h
       include/nametotype.h
       src/wcc/wcc.c
       src/wsh/helper.c
       src/wsh/include/colors.h
       src/wsh/include/libwitch/helper.h
       src/wsh/include/libwitch/sigs.h
       src/wsh/include/libwitch/wsh.h
       src/wsh/include/libwitch/wsh_help.h
       src/wsh/include/lua.h
       src/wsh/luajit-2.0/COPYRIGHT
       src/wsh/luajit-2.0/src/lua.h
       src/wsh/luajit-2.0/src/luajit.h
       src/wsh/lua/src/lua.h
       src/wsh/wsh.c
       src/wsh/wshmain.c
Copyright: 2011-2016 Jonathan Brossard
           1994-2015 Lua.org, PUC-Rio
           2005-2016 Mike Pall.
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X
 CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not be
 used in advertising or otherwise to promote the sale, use or other dealings in
 this Software without prior written authorization from the X Consortium.
 .
 X Window System is a trademark of X Consortium, Inc.

Files: include/uthash.h
       include/utlist.h
Copyright: 2003-2013 Troy D. Hanson http:troydhanson.github.com/uthash/
License: BSD-2-clause

Files: src/wsh/openlibm/i387/bsd_ieeefp.h
Copyright: 2003 Petter Wemm.
           1990 Andrew Moore, Talke Studio
License: BSD-4-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. All advertising materials mentioning features or use of this software must
 display the following acknowledgement: 
 This product includes software developed by the Openlibm project.
 .
 4. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Files: src/wsh/openlibm/amd64/bsd_asm.h
       src/wsh/openlibm/amd64/bsd_fpu.h
       src/wsh/openlibm/bsdsrc/b_exp.c
       src/wsh/openlibm/bsdsrc/b_log.c
       src/wsh/openlibm/bsdsrc/b_tgamma.c
       src/wsh/openlibm/bsdsrc/mathimpl.h
       src/wsh/openlibm/i387/bsd_asm.h
       src/wsh/openlibm/i387/bsd_npx.h
       src/wsh/openlibm/src/bsd_cdefs.h
       src/wsh/openlibm/src/s_fabsl.c
Copyright: 2011-2014 The Julia Project
           2008 Stephen L. Moshier <steve@moshier.net>
           1992-2011 The FreeBSD Project
           1993, 2004 Sun Microsystems, Inc.
           2002-2004 David Schultz <das@FreeBSD.ORG>
           2003 Dag-Erling Coïdan Smørgrav
           1985-1995 The Regents of the University of California
           1985-1995 The Regents of the University of California.
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. 
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution. 
 .
 3. Neither the name of the copyright holder
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE. 

Files: debian/*
Copyright: 2017 Philippe Thierry <phil@reseau-libre.net>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


This package was debianized by Pau Garcia i Quiles <pgquiles@elpauer.org> on
Sat, 05 Jul 2008 10:37:30 +0100.

It was downloaded from http://www.webtoolkit.eu/

Upstream Author: Koen Deforche (koen@emweb.be), Wim Dumon (wim@emweb.be),
Pieter Libin (pieter@emweb.be)

Copyright: 2005-2010, Koen Deforche, Wim Dumon

License:

   This package is dual licensed under the GPLv2 and a commercial license

   * GPLv2

   In addition to the license terms of the GNU General Public License,
   Version 2, as copied below, Emweb bvba gives permission to link the
   code of its release of Wt with the OpenSSL project's "OpenSSL" library
   (or with modified versions of it that use the same license as the
   "OpenSSL" library), and distribute the linked executables. You must
   obey the GNU General Public License in all respects for all of the
   code used other than "OpenSSL". If you modify this file, you may
   extend this exception to your version of the file, but you are not
   obligated to do so. If you do not wish to do so, delete this exception
   statement from your version.

   Wt is licensed under the GNU GPL Version 2. Other versions of the GPL do
   not apply.

   This package is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this package; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

   On Debian systems, the complete text of the GNU General
   Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

   In addition to the license terms of the GNU General Public License,
   Version 2, as copied below, Emweb bvba gives permission to link the
   code of its release of Wt with the OpenSSL project's "OpenSSL" library
   (or with modified versions of it that use the same license as the
   "OpenSSL" library), and distribute the linked executables. You must
   obey the GNU General Public License in all respects for all of the
   code used other than "OpenSSL". If you modify this file, you may
   extend this exception to your version of the file, but you are not
   obligated to do so. If you do not wish to do so, delete this exception
   statement from your version."

   * Commercial

   Developer-based Commercial License Terms

   Each developer which uses the Wt API requires a Commercial License during
   the entire development and maintenance period of the web application. The
   Developer-based Commercial Licenses are valid for the duration of one year,
   and for any released version of Wt.

   There are no additional charges for distribution of the application, and the
   license allows the developer to work on an unlimited number of products.

   The license may not be passed on between developers, unless permission is
   granted by Emweb.

   Commercial licensed developers have access to the public community-driven
   mailing list for their support questions. A commercial license does not
   include guaranteed responses from Emweb's Wt developers, nor training. For
   support contracts, please consult our support offerings.

   Project-based Commercial License Terms

   As an alternative to the Developer-based Commercial License, a License may
   be purchased on a per-project basis. This License allows the usage of Wt by
   as many developers as necessary to complete a single pre-defined project.

   There are usually no additional charges for distribution of the application.

   Support options are usually negotiated together with the license. This
   guarantees, if required, personalized high-priority support in addition to
   the public, community-driven mailing lists.


Wt also includes in its tarball several other licenses for third party
components:

   * The 'libwt/src/http' directory is copyright (c) 2003-2006 
     Christopher M. Kohlhoff and copyright (c) 2008-2010 Emweb bvba and is under 
     the Boost license:

   Boost Software License - Version 1.0 - August 17th, 2003

   Permission is hereby granted, free of charge, to any person or organization
   obtaining a copy of the software and accompanying documentation covered by
   this license (the "Software") to use, reproduce, display, distribute,
   execute, and transmit the Software, and to prepare derivative works of the
   Software, and to permit third-parties to whom the Software is furnished to
   do so, all subject to the following:

   The copyright notices in the Software and this entire statement, including
   the above license grant, this restriction and the following disclaimer,
   must be included in all copies of the Software, in whole or in part, and
   all derivative works of the Software, unless such copies or derivative
   works are solely in the form of machine-executable object code generated by
   a source language processor.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
   SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
   FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
   DEALINGS IN THE SOFTWARE.


   * The 'libwt/src/threadpool' directory is copyright (c) 2005-2007
     Philipp Henkel and is under the Boost license (see above)


   * The 'libwt/src/web/randomdevice.cpp' file is copyright (c) 2000 Jens
     Maurer and is under the Boost license (see above)


   * The WtWithQt example is under a MIT license:

   Copyright (C) 2008 Emweb bvba, Kessel-Lo, Belgium.

   Permission is hereby granted, free of charge, to any person
   obtaining a copy of this software and associated documentation
   files (the "Software"), to deal in the Software without
   restriction, including without limitation the rights to use,
   copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following
   conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.


   * The src/Wt/Dbo/backend/amalgamation contains the sqlite 3.6.20 library in
     its amalgamated form

   Copyright (C) 2000-2009, D. Richard Hipp <drh@hwaci.com>

   The author disclaims all copyright. The library is in the public domain.


   * The src/rapidxml directory contains the RapidXML library and is licensed
     under the MIT license and the Boost Software License 1.0

    Boost Software License - Version 1.0 - August 17th, 2003

    Copyright (c) 2006, 2007 Marcin Kalicinski

    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:

    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

    MIT License

    Copyright (c) 2006, 2007 Marcin Kalicinski

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the 
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.


   * src/web/skeleton/jquery.min.js is a minified version of the JQuery library 
     together with the Sizzle.js library.
     
     JQuery is released under the MIT or GPL Version 2 licenses and the original 
     copyright (c) belongs to The JQuery Project

     Sizze.js is released under the MIT, BSD and GPL Version 2 licenses and the 
     original copyright belongs to The Dojo Foundation

   * The 'history' function in libwt/src/web/skeleton/Wt.js is a simplified
     version of code developed by Yahoo under the BSD license. Original 
     copyright (c) 2008, Yahoo! Inc. All rights reserved. Code licensed under 
     the BSD License: http://developer.yahoo.com/yui/license.html

   Software License Agreement (BSD License)
   Copyright (c) 2008, Yahoo! Inc.
   All rights reserved.

   Redistribution and use of this software in source and binary forms, with or
   without modification, are permitted provided that the following conditions
   are met:

      * Redistributions of source code must retain the above copyright notice,
        this list of conditions and the
        following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of Yahoo! Inc. nor the names of its contributors may
        be used to endorse or promote products derived from this software
        without specific prior written permission of Yahoo! Inc.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.


   * The 'arrayRemove' function in libwt/src/web/skeleton/Wt.js is copyright 
     (c) John Resig and is distributed under the MIT license (see above)


   * Patch 20_hurd_locking_style comes from the sqlite3 package in Debian,
     author is Laszlo Boszormenyi. Licensed under the GPL (see above)



   * The cmake/FindWt.cmake file is copyright (c) 2007-2008 Pau Garcia i
     Quiles, with modifications by Emweb bvba, and is distributed under the
     BSD license (see above)


   * The src/web/base64.h file is copyright (C) 2002 Ryan Petrie 
     (ryanpetrie@netscape.net) and released under the zlib license:

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

 

The Debian packaging is Copyright (C) 2008-2010, Pau Garcia i Quiles 
<pgquiles@elpauer.org> and is licensed under the GPL, see above.

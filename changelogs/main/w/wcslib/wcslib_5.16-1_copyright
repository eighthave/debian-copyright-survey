Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: WCSLIB
Upstram-Author: Mark Calabretta <mcalabre@atnf.csiro.au>
Source: http://www.atnf.csiro.au/people/mcalabre/WCS/

Files: *
Copyright: Copyright (C) 1995-2009, Mark Calabretta
 2011-2014 Ole Streicher <olebole@debian.org> (Debian files)
License: LGPL-3
 WCSLIB 4.23 - an implementation of the FITS WCS standard.
 Copyright (C) 1995-2014, Mark Calabretta
 .
 WCSLIB is free software: you can redistribute it and/or modify it under the
 terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 WCSLIB is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
 .
 Correspondence concerning WCSLIB may be directed to:
   Internet email: mcalabre@atnf.csiro.au
   Postal address: Dr. Mark Calabretta
                   Australia Telescope National Facility, CSIRO
                   PO Box 76
                   Epping NSW 1710
                   AUSTRALIA


Files: html/jquery.js debian/missing-sources/jquery.js
Copyright: 2011 John Resig,  2011, The Dojo Foundation
License: GPL-2 or MIT
Comment: These files are not used to generate the package.

License: GPL-2
  On Debian GNU/Linux systems, the complete text of the GNU General
  Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: MIT
  Copyright (c) 2011 John Resig, http://jquery.com/
  .
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:
  .
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

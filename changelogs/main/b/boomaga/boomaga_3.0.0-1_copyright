Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Boomaga
Upstream-Contact: Alexander Sokolov <sokoloff.a@gmail.com>
Source: https://github.com/Boomaga/boomaga

Files: *
Copyright: 2012-2013 Boomaga team
License: LGPL-2.1+

Files: debian/*
Copyright: 2012-2013 Boomaga team
           2012-2013 Alexander Sokoloff <sokoloff.a@gmail.com>
           2015-2019 Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>
License: LGPL-2.1+

Files: gui/pdfmerger/pdfmerger.h
Copyright: 2012-2013 Boomaga team
           2012-2013 Alexander Sokoloff <sokoloff.a@gmail.com>
License: GPL-2+

Files: src/boomaga/cmake/Findsnappy.cmake
Copyright: 2012 Louis Dionne
License: Apache-2.0

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-2+
 This program or library is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in the file `/usr/share/common-licenses/GPL-2'.

License: Apache-2.0
 Apache License
 Version 2.0, January 2004
 http://www.apache.org/licenses/
 .
 On Debian systems the complete text of the Apache 2.0 license can be found
 in the file `/usr/share/common-licenses/Apache-2.0'.

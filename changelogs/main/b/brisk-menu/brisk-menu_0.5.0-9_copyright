Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Brisk Menu
Upstream-Contact: Ikey Doherty <ikey@solus-project.com>
Source: https://github.com/solus-project/brisk-menu

Files: src/backend/all-items/all-backend.c
 src/backend/all-items/all-backend.h
 src/backend/all-items/all-section.c
 src/backend/all-items/all-section.h
 src/backend/apps/apps-backend.c
 src/backend/apps/apps-backend.h
 src/backend/apps/apps-item.c
 src/backend/apps/apps-item.h
 src/backend/apps/apps-section.c
 src/backend/apps/apps-section.h
 src/backend/backend.c
 src/backend/backend.h
 src/backend/favourites/favourites-backend.c
 src/backend/favourites/favourites-backend.h
 src/backend/favourites/favourites-desktop.c
 src/backend/favourites/favourites-section.c
 src/backend/favourites/favourites-section.h
 src/backend/item.c
 src/backend/item.h
 src/backend/section.c
 src/backend/section.h
 src/frontend/classic/category-button.c
 src/frontend/classic/category-button.h
 src/frontend/classic/classic-entry-button.c
 src/frontend/classic/classic-entry-button.h
 src/frontend/classic/classic-window.c
 src/frontend/classic/classic-window.h
 src/frontend/classic/desktop-button.c
 src/frontend/classic/desktop-button.h
 src/frontend/classic/sidebar-scroller.c
 src/frontend/classic/sidebar-scroller.h
 src/frontend/dash/category-button.c
 src/frontend/dash/category-button.h
 src/frontend/dash/dash-entry-button.c
 src/frontend/dash/dash-entry-button.h
 src/frontend/dash/dash-window.c
 src/frontend/dash/dash-window.h
 src/frontend/entry-button.c
 src/frontend/entry-button.h
 src/frontend/launcher.c
 src/frontend/launcher.h
 src/frontend/menu-context.c
 src/frontend/menu-grabs.c
 src/frontend/menu-keyboard.c
 src/frontend/menu-loader.c
 src/frontend/menu-private.h
 src/frontend/menu-search.c
 src/frontend/menu-session.c
 src/frontend/menu-settings.c
 src/frontend/menu-sort.c
 src/frontend/menu-window.c
 src/frontend/menu-window.h
 src/lib/key-binder.c
 src/lib/key-binder.h
 src/lib/styles.h
 src/lib/util.h
 src/mate-applet/applet.c
 src/mate-applet/applet.h
 src/mate-applet/main.c
 src/test/brisk-test-backends.c
Copyright: 2016-2017, Brisk Menu Developers
  2017, Brisk Menu Developers
License: GPL-2+

Files: .clang-format
 .github/Brisk_Menu_0.4.0.png
 .github/context.png
 .github/main.png
 .gitignore
 .tx/config
 AUTHORS
 LICENSE
 LICENSE.CC-BY-SA-4.0
 meson.build
 meson_post_install.sh
 mkrelease.sh
 README.md
 gen_authors.sh
 data/brisk.gresource.xml
 data/com.solus-project.brisk-menu.gschema.xml
 data/com.solus_project.brisk.BriskMenu.mate-panel-applet.in
 data/org.mate.panel.applet.BriskMenuFactory.service.in
 data/meson.build
 data/classic/styling.css
 data/dash/styling.css
 data/dash/styling-light.css
 po/meson.build
 src/backend/meson.build
 src/frontend/meson.build
 src/lib/authors.h
 src/lib/meson.build
 src/mate-applet/meson.build
 src/meson.build
 src/session/meson.build
 data/org.gnome.SessionManager.xml
 data/org.mate.ScreenSaver.xml
 update_format.sh
Copyright: 2016, Ikey Doherty <ikey@solus-project.com>
License: GPL-2+
Comment:
 License information not provided in these files. Assuming
 same license and copyright holdership as for the files
 in the src/ folder.

Files: po/*.po
 po/POTFILES.in
 po/brisk-menu.pot
 po/update_linguas.sh
 po/LINGUAS
Copyright: Alexe Moldovan <acristianmoldovan@gmail.com>, 2017
           Casio E <casiopopasio@t-online.de>, 2017
           Demiray Muhterem <mdemiray@msn.com>, 2017
           Dušan Kazik <prescott66@gmail.com>, 2017
           Dzej Kob <accountxyz@protonmail.com>, 2017
           Federico Damián Schonborn <federico.d.schonborn@gmail.com>, 2017
           Ferenc Papp <pappfer@gmail.com>, 2017
           BALLOON a.k.a. Fu-sen. <balloonakafusen@gmail.com>, 2017
           Geert Verheyde <geert.verheyde@gmail.com>, 2017
           Ikey Doherty <ikey@solus-project.com>, 2017
           Marcelo Ferreira <marceloffdeoliveira@gmail.com>, 2017
           Maxim Taranov <png2378@gmail.com>, 2017
           Moriel Ever <emoriel17@yahoo.com>, 2017
           Puppelimies <max.haureus@gmail.com>, 2017
           Stefan Ric <stfric369@gmail.com>, 2017
           Thanos Apostolou <thanosapostolou@outlook.com>, 2017
           Thisara Kasun <thisara07.dazz@gmail.com>, 2017
           utybo <utybo@users.noreply.github.com>, 2017
           Vlad Denysenko <vladdenisenko44@gmail.com>, 2017
           Yurizal Susanto <rizal.sagi@gmail.com>, 2017
License: GPL-2+
Comment:
 License information not provided in these files. Assuming
 same license and copyright holdership as for the files
 in the src/ folder.

Files: data/brisk_system-log-out-symbolic.svg
Copyright: Sam Hewitt
License: CC-BY-SA-4.0

Files: debian/*
Copyright: 2016 Mike Gabriel <sunweaver@debian.org>
           2017 Martin Wimpress <code@flexion.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Genaral Public License for more details.
 .
 You should have received a copy og the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: CC-BY-SA-4.0
 Creative Commons Attribution-ShareAlike 4.0 International Public
 License
 .
 By exercising the Licensed Rights (defined below), You accept and agree
 to be bound by the terms and conditions of this Creative Commons
 Attribution-ShareAlike 4.0 International Public License ("Public
 License"). To the extent this Public License may be interpreted as a
 contract, You are granted the Licensed Rights in consideration of Your
 acceptance of these terms and conditions, and the Licensor grants You
 such rights in consideration of benefits the Licensor receives from
 making the Licensed Material available under these terms and
 conditions.
 .
 .
 Section 1 -- Definitions.
 .
   a. Adapted Material means material subject to Copyright and Similar
      Rights that is derived from or based upon the Licensed Material
      and in which the Licensed Material is translated, altered,
      arranged, transformed, or otherwise modified in a manner requiring
      permission under the Copyright and Similar Rights held by the
      Licensor. For purposes of this Public License, where the Licensed
      Material is a musical work, performance, or sound recording,
      Adapted Material is always produced where the Licensed Material is
      synched in timed relation with a moving image.
 .
   b. Adapter's License means the license You apply to Your Copyright
      and Similar Rights in Your contributions to Adapted Material in
      accordance with the terms and conditions of this Public License.
 .
   c. BY-SA Compatible License means a license listed at
      creativecommons.org/compatiblelicenses, approved by Creative
      Commons as essentially the equivalent of this Public License.
 .
   d. Copyright and Similar Rights means copyright and/or similar rights
      closely related to copyright including, without limitation,
      performance, broadcast, sound recording, and Sui Generis Database
      Rights, without regard to how the rights are labeled or
      categorized. For purposes of this Public License, the rights
      specified in Section 2(b)(1)-(2) are not Copyright and Similar
      Rights.
 .
   e. Effective Technological Measures means those measures that, in the
      absence of proper authority, may not be circumvented under laws
      fulfilling obligations under Article 11 of the WIPO Copyright
      Treaty adopted on December 20, 1996, and/or similar international
      agreements.
 .
   f. Exceptions and Limitations means fair use, fair dealing, and/or
      any other exception or limitation to Copyright and Similar Rights
      that applies to Your use of the Licensed Material.
 .
   g. License Elements means the license attributes listed in the name
      of a Creative Commons Public License. The License Elements of this
      Public License are Attribution and ShareAlike.
 .
   h. Licensed Material means the artistic or literary work, database,
      or other material to which the Licensor applied this Public
      License.
 .
   i. Licensed Rights means the rights granted to You subject to the
      terms and conditions of this Public License, which are limited to
      all Copyright and Similar Rights that apply to Your use of the
      Licensed Material and that the Licensor has authority to license.
 .
   j. Licensor means the individual(s) or entity(ies) granting rights
      under this Public License.
 .
   k. Share means to provide material to the public by any means or
      process that requires permission under the Licensed Rights, such
      as reproduction, public display, public performance, distribution,
      dissemination, communication, or importation, and to make material
      available to the public including in ways that members of the
      public may access the material from a place and at a time
      individually chosen by them.
 .
   l. Sui Generis Database Rights means rights other than copyright
      resulting from Directive 96/9/EC of the European Parliament and of
      the Council of 11 March 1996 on the legal protection of databases,
      as amended and/or succeeded, as well as other essentially
      equivalent rights anywhere in the world.
 .
   m. You means the individual or entity exercising the Licensed Rights
      under this Public License. Your has a corresponding meaning.
 .
 .
 Section 2 -- Scope.
 .
   a. License grant.
 .
        1. Subject to the terms and conditions of this Public License,
           the Licensor hereby grants You a worldwide, royalty-free,
           non-sublicensable, non-exclusive, irrevocable license to
           exercise the Licensed Rights in the Licensed Material to:
 .
             a. reproduce and Share the Licensed Material, in whole or
                in part; and
 .
             b. produce, reproduce, and Share Adapted Material.
 .
        2. Exceptions and Limitations. For the avoidance of doubt, where
           Exceptions and Limitations apply to Your use, this Public
           License does not apply, and You do not need to comply with
           its terms and conditions.
 .
        3. Term. The term of this Public License is specified in Section
           6(a).
 .
        4. Media and formats; technical modifications allowed. The
           Licensor authorizes You to exercise the Licensed Rights in
           all media and formats whether now known or hereafter created,
           and to make technical modifications necessary to do so. The
           Licensor waives and/or agrees not to assert any right or
           authority to forbid You from making technical modifications
           necessary to exercise the Licensed Rights, including
           technical modifications necessary to circumvent Effective
           Technological Measures. For purposes of this Public License,
           simply making modifications authorized by this Section 2(a)
           (4) never produces Adapted Material.
 .
        5. Downstream recipients.
 .
             a. Offer from the Licensor -- Licensed Material. Every
                recipient of the Licensed Material automatically
                receives an offer from the Licensor to exercise the
                Licensed Rights under the terms and conditions of this
                Public License.
 .
             b. Additional offer from the Licensor -- Adapted Material.
                Every recipient of Adapted Material from You
                automatically receives an offer from the Licensor to
                exercise the Licensed Rights in the Adapted Material
                under the conditions of the Adapter's License You apply.
 .
             c. No downstream restrictions. You may not offer or impose
                any additional or different terms or conditions on, or
                apply any Effective Technological Measures to, the
                Licensed Material if doing so restricts exercise of the
                Licensed Rights by any recipient of the Licensed
                Material.
 .
        6. No endorsement. Nothing in this Public License constitutes or
           may be construed as permission to assert or imply that You
           are, or that Your use of the Licensed Material is, connected
           with, or sponsored, endorsed, or granted official status by,
           the Licensor or others designated to receive attribution as
           provided in Section 3(a)(1)(A)(i).
 .
   b. Other rights.
 .
        1. Moral rights, such as the right of integrity, are not
           licensed under this Public License, nor are publicity,
           privacy, and/or other similar personality rights; however, to
           the extent possible, the Licensor waives and/or agrees not to
           assert any such rights held by the Licensor to the limited
           extent necessary to allow You to exercise the Licensed
           Rights, but not otherwise.
 .
        2. Patent and trademark rights are not licensed under this
           Public License.
 .
        3. To the extent possible, the Licensor waives any right to
           collect royalties from You for the exercise of the Licensed
           Rights, whether directly or through a collecting society
           under any voluntary or waivable statutory or compulsory
           licensing scheme. In all other cases the Licensor expressly
           reserves any right to collect such royalties.
 .
 .
 Section 3 -- License Conditions.
 .
 Your exercise of the Licensed Rights is expressly made subject to the
 following conditions.
 .
   a. Attribution.
 .
        1. If You Share the Licensed Material (including in modified
           form), You must:
 .
             a. retain the following if it is supplied by the Licensor
                with the Licensed Material:
 .
                  i. identification of the creator(s) of the Licensed
                     Material and any others designated to receive
                     attribution, in any reasonable manner requested by
                     the Licensor (including by pseudonym if
                     designated);
 .
                 ii. a copyright notice;
 .
                iii. a notice that refers to this Public License;
 .
                 iv. a notice that refers to the disclaimer of
                     warranties;
 .
                  v. a URI or hyperlink to the Licensed Material to the
                     extent reasonably practicable;
 .
             b. indicate if You modified the Licensed Material and
                retain an indication of any previous modifications; and
 .
             c. indicate the Licensed Material is licensed under this
                Public License, and include the text of, or the URI or
                hyperlink to, this Public License.
 .
        2. You may satisfy the conditions in Section 3(a)(1) in any
           reasonable manner based on the medium, means, and context in
           which You Share the Licensed Material. For example, it may be
           reasonable to satisfy the conditions by providing a URI or
           hyperlink to a resource that includes the required
           information.
 .
        3. If requested by the Licensor, You must remove any of the
           information required by Section 3(a)(1)(A) to the extent
           reasonably practicable.
 .
   b. ShareAlike.
 .
      In addition to the conditions in Section 3(a), if You Share
      Adapted Material You produce, the following conditions also apply.
 .
        1. The Adapter's License You apply must be a Creative Commons
           license with the same License Elements, this version or
           later, or a BY-SA Compatible License.
 .
        2. You must include the text of, or the URI or hyperlink to, the
           Adapter's License You apply. You may satisfy this condition
           in any reasonable manner based on the medium, means, and
           context in which You Share Adapted Material.
 .
        3. You may not offer or impose any additional or different terms
           or conditions on, or apply any Effective Technological
           Measures to, Adapted Material that restrict exercise of the
           rights granted under the Adapter's License You apply.
 .
 .
 Section 4 -- Sui Generis Database Rights.
 .
 Where the Licensed Rights include Sui Generis Database Rights that
 apply to Your use of the Licensed Material:
 .
   a. for the avoidance of doubt, Section 2(a)(1) grants You the right
      to extract, reuse, reproduce, and Share all or a substantial
      portion of the contents of the database;
 .
   b. if You include all or a substantial portion of the database
      contents in a database in which You have Sui Generis Database
      Rights, then the database in which You have Sui Generis Database
      Rights (but not its individual contents) is Adapted Material,
      including for purposes of Section 3(b); and
 .
   c. You must comply with the conditions in Section 3(a) if You Share
      all or a substantial portion of the contents of the database.
 .
 For the avoidance of doubt, this Section 4 supplements and does not
 replace Your obligations under this Public License where the Licensed
 Rights include other Copyright and Similar Rights.
 .
 .
 Section 5 -- Disclaimer of Warranties and Limitation of Liability.
 .
   a. UNLESS OTHERWISE SEPARATELY UNDERTAKEN BY THE LICENSOR, TO THE
      EXTENT POSSIBLE, THE LICENSOR OFFERS THE LICENSED MATERIAL AS-IS
      AND AS-AVAILABLE, AND MAKES NO REPRESENTATIONS OR WARRANTIES OF
      ANY KIND CONCERNING THE LICENSED MATERIAL, WHETHER EXPRESS,
      IMPLIED, STATUTORY, OR OTHER. THIS INCLUDES, WITHOUT LIMITATION,
      WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR
      PURPOSE, NON-INFRINGEMENT, ABSENCE OF LATENT OR OTHER DEFECTS,
      ACCURACY, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT
      KNOWN OR DISCOVERABLE. WHERE DISCLAIMERS OF WARRANTIES ARE NOT
      ALLOWED IN FULL OR IN PART, THIS DISCLAIMER MAY NOT APPLY TO YOU.
 .
   b. TO THE EXTENT POSSIBLE, IN NO EVENT WILL THE LICENSOR BE LIABLE
      TO YOU ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION,
      NEGLIGENCE) OR OTHERWISE FOR ANY DIRECT, SPECIAL, INDIRECT,
      INCIDENTAL, CONSEQUENTIAL, PUNITIVE, EXEMPLARY, OR OTHER LOSSES,
      COSTS, EXPENSES, OR DAMAGES ARISING OUT OF THIS PUBLIC LICENSE OR
      USE OF THE LICENSED MATERIAL, EVEN IF THE LICENSOR HAS BEEN
      ADVISED OF THE POSSIBILITY OF SUCH LOSSES, COSTS, EXPENSES, OR
      DAMAGES. WHERE A LIMITATION OF LIABILITY IS NOT ALLOWED IN FULL OR
      IN PART, THIS LIMITATION MAY NOT APPLY TO YOU.
 .
   c. The disclaimer of warranties and limitation of liability provided
      above shall be interpreted in a manner that, to the extent
      possible, most closely approximates an absolute disclaimer and
      waiver of all liability.
 .
 .
 Section 6 -- Term and Termination.
 .
   a. This Public License applies for the term of the Copyright and
      Similar Rights licensed here. However, if You fail to comply with
      this Public License, then Your rights under this Public License
      terminate automatically.
 .
   b. Where Your right to use the Licensed Material has terminated under
      Section 6(a), it reinstates:
 .
        1. automatically as of the date the violation is cured, provided
           it is cured within 30 days of Your discovery of the
           violation; or
 .
        2. upon express reinstatement by the Licensor.
 .
      For the avoidance of doubt, this Section 6(b) does not affect any
      right the Licensor may have to seek remedies for Your violations
      of this Public License.
 .
  c. For the avoidance of doubt, the Licensor may also offer the
     Licensed Material under separate terms or conditions or stop
     distributing the Licensed Material at any time; however, doing so
     will not terminate this Public License.
 .
  d. Sections 1, 5, 6, 7, and 8 survive termination of this Public
     License.
 .
 .
 Section 7 -- Other Terms and Conditions.
 .
   a. The Licensor shall not be bound by any additional or different
      terms or conditions communicated by You unless expressly agreed.
 .
   b. Any arrangements, understandings, or agreements regarding the
      Licensed Material not stated herein are separate from and
      independent of the terms and conditions of this Public License.
 .
 .
 Section 8 -- Interpretation.
 .
   a. For the avoidance of doubt, this Public License does not, and
      shall not be interpreted to, reduce, limit, restrict, or impose
      conditions on any use of the Licensed Material that could lawfully
      be made without permission under this Public License.
 .
   b. To the extent possible, if any provision of this Public License is
      deemed unenforceable, it shall be automatically reformed to the
      minimum extent necessary to make it enforceable. If the provision
      cannot be reformed, it shall be severed from this Public License
      without affecting the enforceability of the remaining terms and
      conditions.
 .
   c. No term or condition of this Public License will be waived and no
      failure to comply consented to unless expressly agreed to by the
      Licensor.
 .
   d. Nothing in this Public License constitutes or may be interpreted
      as a limitation upon, or waiver of, any privileges and immunities
      that apply to the Licensor or You, including from the legal
      processes of any jurisdiction or authority.

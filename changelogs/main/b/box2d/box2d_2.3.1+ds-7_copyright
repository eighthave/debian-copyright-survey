Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: box2d
Upstream-Contact: Erin Catto <erincatto@gmail.com>
Source: https://github.com/erincatto/Box2D

Files: *
Copyright: 2006-2014, Erin Catto and contributors
           2009-2010, Mikko Mononen
License: Zlib

Files: Box2D/Testbed/Framework/stb_truetype.h
Copyright: 2009-2013, Sean Barrett
License: public-domain
 This software is in the public domain. Where that dedication is not
 recognized, you are granted a perpetual, irrevokable license to copy
 and modify this file as you see fit.

Files: Box2D/Build/Data/DroidSans.ttf
Copyright: 2006, 2007, 2008, 2009, 2010 Google Corp.
License: Apache-2.0
 On Debian systems, the complete text of the Apache License Version 2.0
 can be found in `/usr/share/common-licenses/Apache-2.0'.

Files: debian/*
Copyright: 2009,      Miriam Ruiz <little_miry@yahoo.es>,
           2013,      Barry deFreese <bdefreese@debian.org>
           2013-2018, Markus Koschany <apo@debian.org>
License: Zlib

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution

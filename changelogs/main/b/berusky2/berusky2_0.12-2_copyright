Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Berusky2
Source: https://anakreon.cz/berusky2.html

Files: *
Copyright: 1997,      Jochen Wilhelmy
           1998,      Juan Jesus Alcolea Picazo and Peter Palotas
           2011-2013, AnakreoN
           2011-2013, Michal Simonik <simonik@anakreon.cz>
           2011-2017, Martin Stransky <stransky@anakreon.cz>
           2020,      Asher Gordon <AsDaGo@posteo.net>
License: GPL-2+

Files: debian/*
Copyright: 2012,      Bart Martens <bartm@debian.org>
           2013-2020, Markus Koschany <apo@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 On Debian systems, the complete text of the GNU General Public License version
 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: src/kofola/codec.h
Copyright: 1994-2001, XIPHOPHORUS Company
License: Xiph-License
 THIS FILE IS PART OF THE OggVorbis SOFTWARE CODEC SOURCE CODE.
 USE, DISTRIBUTION AND REPRODUCTION OF THIS LIBRARY SOURCE IS
 GOVERNED BY A BSD-STYLE SOURCE LICENSE INCLUDED WITH THIS SOURCE
 IN 'COPYING'. PLEASE READ THESE TERMS BEFORE DISTRIBUTING.
 .
 THE OggVorbis SOURCE CODE IS (C) COPYRIGHT 1994-2001
 by the XIPHOPHORUS Company http://www.xiph.org/
 .
 Copyright (c) 2002, Xiph.org Foundation
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 - Neither the name of the Xiph.org Foundation nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
Comment:
 The file src/kofola/codec.h is an old copy from the package libvorbis also
 available in Debian.

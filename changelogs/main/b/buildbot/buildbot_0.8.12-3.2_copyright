Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: buildbot
Upstream-Contact: Dustin J. Mitchell <djmitche@gmail.com>
Source: http://trac.buildbot.net/

Files: *
Copyright: Buildbot Team Members
License: GPL-2

Files: debian/*
Copyright: 2011 Andriy Senkovych <jolly_roger@itblog.org.ua>
           2006-2010 Matthias Klose <doko@ubuntu.com>
License: GPL-2

Files: buildbot/steps/package/rpm/*
Copyright: Buildbot Team Members
           Steve 'Ashcrow' Milner <smilner+buildbot@redhat.com>
           Dan Radez <dradez+buildbot@redhat.com>
License: GPL-2

Files: contrib/generate_changelog.py
Copyright: 2008, Steve 'Ashcrow' Milner <smilner+buildbot@redhat.com>
License: GPL-2


Files: buildbot/status/web/status_json.py
Copyright: Buildbot Team Members
           2010, The Chromium Authors
License: GPL-2

Files: buildbot/libvirtbuildslave.py
Copyright: Buildbot Team Members
           2010, Isotoma Limited
License: GPL-2

Files: contrib/buildbot_cvs_mail.py
Copyright: 2002-2006 Barry Warsaw, Fred Drake, and contributors
License: GPL-2

Files: buildbot/ec2buildslave.py
Copyright: Buildbot Team Members
           2009, Canonical Ltd.
License: GPL-2

Files: contrib/svnpoller.py
Copyright: 2006 John Pye
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: contrib/bzr_buildbot.py
Copyright: 2008-2009, Canonical
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: GPL-2
 This file is part of Buildbot.  Buildbot is free software: you can
 redistribute it and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation, version 2.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: sqlalchemy/*
Copyright: 2005-2015, the SQLAlchemy authors and contributors
License: Expat

Files: sqlalchemy/lib/sqlalchemy/interfaces.py
Copyright: 2007, Jason Kirtland jek@discorporate.us
  2007-2015, the SQLAlchemy authors and contributors
License: Expat

Files: sqlalchemy/lib/sqlalchemy/dialects/sybase/base.py
Copyright: 2007, Fisch Asset Management
  2010-2015, the SQLAlchemy authors and contributors
License: Expat

Files: sqlalchemy/lib/sqlalchemy/cextension/processors.c
 lib/sqlalchemy/cextension/resultproxy.c
Copyright: 2010-2011, Gaetan de Menten <gdementen@gmail.com>
  2010-2015, the SQLAlchemy authors and contributors
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of this
 software and associated documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

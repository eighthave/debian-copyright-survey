Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Bouncy
Source: http://www.pyweek.org/e/bouncy

Files: *
Copyright: 2006, Richard Jones <richard@mechanicalcat.net
License: GPL-2+

Files: pyglyph/*
Copyright: 2006, Alex Holkner <Alex.Holkner@mail.google.com>
License: Expat

Files: euclid.py
Copyright: 2006, Alex Holkner <Alex.Holkner@mail.google.com>
License: LGPL-2.1+

Files: data/*.ttf
Copyright: 2003, Bitstream, Inc.
License: Bitstream-Vera-Font-License

Files: debian/*
Copyright: 2007-2008, Miriam Ruiz <miriam@debian.org>
           2007,      Jon Dowland
           2008,      Ansgar Burchardt <ansgar@43-1.org>
           2007-2009, Barry deFreese <bdefreese@debian.org>
           2009,      Gonéri Le Bouder, Evgeni Golov
           2013-2015, Markus Koschany <apo@debian.org>
License: GPL-2+


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; either version 2.1 of the License, or (at your
 option) any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Bitstream-Vera-Font-License
 Copyright (C) 2003 Bitstream, Inc.
 All Rights Reserved. Bitstream Vera is a trademark of Bitstream, Inc.
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute
 the Font Software, including without limitation the rights to use, copy,
 merge, publish, distribute, and/or sell copies of the Font Software, and
 to permit persons to whom the Font Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright and trademark notices and this permission notice
 shall be included in all copies of one or more of the Font Software
 typefaces.
 .
 The Font Software may be modified, altered, or added to, and in
 particular the designs of glyphs or characters in the Fonts may be
 modified and additional glyphs or characters may be added to the Fonts,
 only if the fonts are renamed to names not containing either the words
 "Bitstream" or the word "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts or
 Font Software that has been modified and is distributed under the
 "Bitstream Vera" names.
 .
 The Font Software may be sold as part of a larger software package but
 no copy of one or more of the Font Software typefaces may be sold by
 itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL
 BITSTREAM OR THE GNOME FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF THE USE OR INABILITY TO USE THE FONT
 SOFTWARE OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font
 Software without prior written authorization from the Gnome Foundation
 or Bitstream Inc., respectively. For further information, contact:
 <fonts@gnome.org>.

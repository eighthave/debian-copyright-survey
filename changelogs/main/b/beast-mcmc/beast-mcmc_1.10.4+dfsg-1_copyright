Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: BEAST-MCMC
Upstream-Contact: Alexei Drummond <alexei@cs.auckland.ac.nz>,
                  Andrew Rambaut <a.rambaut@ed.ac.uk>
Source: https://github.com/beast-dev/beast-mcmc
Files-Excluded: *.jar
                *.so
                *.o
                *.jnilib
                release*/Mac
                release*/Windows
                packaging_tools*

Files: *
Copyright: 2002-2015 Alexei Drummond and Andrew Rambaut
License: LGPL-3+

Files: src/dr/math/matrixAlgebra/RobustSingularValueDecomposition.java
Copyright: 1999 CERN - European Organization for Nuclear Research
           1999 Marc A. Suchard
License: Colt-License-Agreement
 Permission to use, copy, modify, distribute and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation. CERN makes no representations about the suitability of this
 software for any purpose. It is provided "as is" without expressed or
 implied warranty.

Files: src/dr/math/iterations/NewtonZeroFinder.java
       src/dr/math/iterations/BisectionZeroFinder.java
       src/dr/math/functionEval/DrMath.java
Copyright: 1999 Didier BESSET
License: LGPL-3+

Files: debian/*
Copyright: 2011-2015 Andreas Tille <tille@debian.org>
License: LGPL-3+

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On a Debian GNU/Linux system, the GNU Lesser GPL license version 3 is included
 in the file ‘/usr/share/common-licenses/LGPL-3’, and the GNU GPL license
 version 3 is included in the file ‘/usr/share/common-licenses/GPL-3’.


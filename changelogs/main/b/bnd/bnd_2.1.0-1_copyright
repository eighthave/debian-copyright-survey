Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: BND
Upstream-Contact: Peter Kriens, aQute SARL
Source: https://github.com/bndtools/bnd

Files: *
Copyright: 2006-2013, aQute SARL
License: Apache-2.0

Files: cnf/findbugs/*
Copyright: 2004,2005,  University of Maryland
           2005,       Chris Nappin
           2005, 2006, Etienne Giraudy, InStranet Inc
License: LGPL-2.1+

Files: aQute.libg/src/aQute/lib/filter/Filter.java
Copyright: OSGi Alliance
License: Apache-2.0
Comment:
 This file was originally licensed from Gatespace to OSGi in the year 2000. A
 member donated this as ASL 2.0 licensed matching BND's default license. See
 also https://github.com/bndtools/bnd/issues/944 for a clarification from
 upstream. This license header will be corrected in a future upstream release.

Files: biz.aQute.bndlib.tests/src/test/lib/NanoHTTPD.java
Copyright: 2001,2005-2011, Jarno Elonen <elonen@iki.fi>
           2010,           Konstantinos Togias <info@ktogias.gr>
License: BSD-3-clause

Files: biz.aQute.jpm/src/aQute/jpm/main/InstallCert.java
Copyright: 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: debian/*
Copyright: 2009, Ludovic Claude <ludovic.claude@laposte.net>
           2011, Damien Raude-Morvan <drazzib@debian.org>
           2015, Miguel Landaeta <nomadium@debian.org>
           2015, Markus Koschany <apo@gambaru.de>
License: Apache-2.0

License: Apache-2.0
  On Debian GNU/Linux system you can find the complete text of the
  Apache 2.0 license in '/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
 .
    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer. Redistributions in
    binary form must reproduce the above copyright notice, this list of
    conditions and the following disclaimer in the documentation and/or other
    materials provided with the distribution. The name of the author may not
    be used to endorse or promote products derived from this software without
    specific prior written permission.
 .
    THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems the full license text of the GNU Lesser General Public
 License 2.1 can be found in /usr/share/common-licenses/LGPL-2.1.


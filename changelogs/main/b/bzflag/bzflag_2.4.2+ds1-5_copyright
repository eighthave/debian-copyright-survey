This package was debianized by Anand Kumria <wildfire@progsoc.uts.edu.au> on
Sun, 12 Dec 1999 14:07:25 +1100.

It was downloaded from http://download.sourceforge.net/bzflag/

Upstream Authors:
    Tim Riker <Tim@Rikers.org>

Copyright:
    Copyright (c) 1993-2011 Tim Riker

License:

    bzflag - a multiplayer 3D tank battle game
    Copyright (c) 1993-2011 Tim Riker

    This package is free software;  you can redistribute it and/or
    modify it under the terms of the license found in the file
    named COPYING that should have accompanied this file.

    THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

The sources may be redistributed under the terms of the GNU Lesser General
Public License, Version 2.1 found in the file
'/usr/share/common-licenses/LGPL-2.1' on Debian systems.

The Debian packaging is:

    Copyright (C) 2001–2010 Tim Riker <Tim@Rikers.org>
    Copyright (C) 2012–2013 Ryan Kavanagh <rak@debian.org>

you can redistribute it and/or modify it under the terms of the GNU Lesser General
Public License version 2.1 as published by the Free Software Foundation.

The sources have been repacked to remove several libraries upstream bundles
which are already included in Debian. This is in order to avoid having to update
copyright information for five unused libraries. These libraries are:
 * PDcurses (use libncurses-dev)
 * ares (libc-ares-dev)
 * curl (libcurl4-gnutls-dev | libcurl4-dev | libcurl-dev)
 * glew (libglew1.5-dev | libglew-dev)
 * zlib
Please see the file debian/README.sources in the bzflag source package for more
details.

There are files with different and/or additional copyright holders. These are:

misc/stats/*:

  These files are under the same license as BZFlag, but have an additional
  copyright holder:

    Copyright (c) 2003-2012 Tucker McLean, Tim Riker

misc/man2html.c:

     Copyright (c) 1995 DJ Delorie, 334 North Road, Deerfield NH USA
     Distributed under the terms of the GNU GPL, version 2 or later.
     Note: The HTML output of this program is NOT considered a derived
     work of this program.

  On Debian systems, the GNU General Public License, version 2 or later, may
  be found in the file `/usr/share/common-licenses/GPL-2'.


tools/modeltool/*:

    This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
    For the latest info, see http://www.ogre3d.org/

    Copyright © 2000-2002 The OGRE Team
    Also see acknowledgements in Readme.html

    This program is free software; you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any later
    version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA, or go to
    http://www.gnu.org/copyleft/lesser.txt.

  The file tools/modeltool/Q3BSP.h is additionally:

    Copyright (C) 1999-2000 Id Software, Inc.

src/bzfs/base64.*:

  Copyright (C) 2002 Ryan Petrie (ryanpetrie@netscape.net)
  and released under the zlib license:

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

include/md5.h:

  converted to C++ class by Frank Thilo (thilo@unix-ag.org)
  for bzflag (http://www.bzflag.org)

   based on:

  This is the header file for the MD5 message-digest algorithm.
  The algorithm is due to Ron Rivest.  This code was
  written by Colin Plumb in 1993, no copyright is claimed.
  This code is in the public domain; do with it what you wish.

  Equivalent code is available from RSA Data Security, Inc.
  This code has been tested against that, and is equivalent,
  except that you don't need to include two pages of legalese
  with every copy.

  To compute the message digest of a chunk of bytes, declare an
  MD5Context structure, pass it to MD5Init, call MD5Update as
  needed on buffers full of bytes, and then call MD5Final, which
  will fill a supplied 16-byte array with the digest.

  Changed so as no longer to depend on Colin Plumb's `usual.h'
  header definitions; now uses stuff from dpkg's config.h
   - Ian Jackson <ian@chiark.greenend.org.uk>.
  Still in the public domain.

misc/shtool:

  GNU shtool -- The GNU Portable Shell Tool
  Copyright (c) 1994-2005 Ralf S. Engelschall <rse@engelschall.com>

  See http://www.gnu.org/software/shtool/ for more information.
  See ftp://ftp.gnu.org/gnu/shtool/ for latest version.

  Version:  2.0.2 (15-Jun-2005)
  Contents: 4/19 available modules

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA,
  or contact Ralf S. Engelschall <rse@engelschall.com>.

  NOTICE: Given that you include this file verbatim into your own
  source tree, you are justified in saying that it remains separate
  from your package, and that this way you are simply just using GNU
  shtool. So, in this situation, there is no requirement that your
  package itself is licensed under the GNU General Public License in
  order to take advantage of GNU shtool.

misc/install-sh:

  Copyright (C) 1994 X Consortium

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
  AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
  TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Except as contained in this notice, the name of the X Consortium shall not
  be used in advertising or otherwise to promote the sale, use or other deal-
  ings in this Software without prior written authorization from the X Consor-
  tium.

  FSF changes to this file are in the public domain.


MSVC/build/curl/curlbuild.h:

    Copyright (C) 1998 - 2009, Daniel Stenberg, <daniel@haxx.se>, et al.

    This software is licensed as described in the file COPYING, which
    you should have received as part of this distribution. The terms
    are also available at http://curl.haxx.se/docs/copyright.html.

    You may opt to use, copy, modify, merge, publish, distribute and/or sell
    copies of the Software, and permit persons to whom the Software is
    furnished to do so, under the terms of the COPYING file.

    This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
    KIND, either express or implied.

  The terms at the link and in the COPYING file are:

    COPYRIGHT AND PERMISSION NOTICE

    Copyright (c) 1996 - 2010, Daniel Stenberg, <daniel@haxx.se>.

    All rights reserved.

    Permission to use, copy, modify, and distribute this software for any purpose
    with or without fee is hereby granted, provided that the above copyright
    notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
    NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
    OR OTHER DEALINGS IN THE SOFTWARE.

    Except as contained in this notice, the name of a copyright holder shall not
    be used in advertising or otherwise to promote the sale, use or other dealings
    in this Software without prior written authorization of the copyright holder.

DejaVu derived bitmaps:
    The files data/fonts/ are derived from the DejaVu font. The original font
    files are located under misc/fonts/ and are distributed under the DejaVu
    License:

  DejaVu License

  From DejaVuWiki (http://dejavu.sourceforge.net/wiki/index.php/License)

     Fonts are (c) Bitstream (see below). DejaVu changes are in public domain.
     Explanation of copyright is on [3]Gnome page on Bitstream Vera fonts.
     Glyphs imported from [4]Arev fonts are (c) Tavmjung Bah (see below)

  Bitstream Vera Fonts Copyright

     Copyright (c) 2003 by Bitstream, Inc. All Rights Reserved. Bitstream
     Vera is a trademark of Bitstream, Inc.

     Permission is hereby granted, free of charge, to any person obtaining a
     copy of the fonts accompanying this license ("Fonts") and associated
     documentation files (the "Font Software"), to reproduce and distribute
     the Font Software, including without limitation the rights to use,
     copy, merge, publish, distribute, and/or sell copies of the Font
     Software, and to permit persons to whom the Font Software is furnished
     to do so, subject to the following conditions:

     The above copyright and trademark notices and this permission notice
     shall be included in all copies of one or more of the Font Software
     typefaces.

     The Font Software may be modified, altered, or added to, and in
     particular the designs of glyphs or characters in the Fonts may be
     modified and additional glyphs or characters may be added to the Fonts,
     only if the fonts are renamed to names not containing either the words
     "Bitstream" or the word "Vera".

     This License becomes null and void to the extent applicable to Fonts or
     Font Software that has been modified and is distributed under the
     "Bitstream Vera" names.

     The Font Software may be sold as part of a larger software package but
     no copy of one or more of the Font Software typefaces may be sold by
     itself.

     THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
     OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL
     BITSTREAM OR THE GNOME FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR
     OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL,
     OR CONSEQUENTIAL DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR
     OTHERWISE, ARISING FROM, OUT OF THE USE OR INABILITY TO USE THE FONT
     SOFTWARE OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.

     Except as contained in this notice, the names of Gnome, the Gnome
     Foundation, and Bitstream Inc., shall not be used in advertising or
     otherwise to promote the sale, use or other dealings in this Font
     Software without prior written authorization from the Gnome Foundation
     or Bitstream Inc., respectively. For further information, contact:
     fonts at gnome dot org.

  Arev Fonts Copyright

     Copyright (c) 2006 by Tavmjong Bah. All Rights Reserved.

     Permission is hereby granted, free of charge, to any person obtaining a
     copy of the fonts accompanying this license ("Fonts") and associated
     documentation files (the "Font Software"), to reproduce and distribute
     the modifications to the Bitstream Vera Font Software, including
     without limitation the rights to use, copy, merge, publish, distribute,
     and/or sell copies of the Font Software, and to permit persons to whom
     the Font Software is furnished to do so, subject to the following
     conditions:

     The above copyright and trademark notices and this permission notice
     shall be included in all copies of one or more of the Font Software
     typefaces.

     The Font Software may be modified, altered, or added to, and in
     particular the designs of glyphs or characters in the Fonts may be
     modified and additional glyphs or characters may be added to the Fonts,
     only if the fonts are renamed to names not containing either the words
     "Tavmjong Bah" or the word "Arev".

     This License becomes null and void to the extent applicable to Fonts or
     Font Software that has been modified and is distributed under the
     "Tavmjong Bah Arev" names.

     The Font Software may be sold as part of a larger software package but
     no copy of one or more of the Font Software typefaces may be sold by
     itself.

     THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
     MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
     OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL
     TAVMJONG BAH BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
     INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
     DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
     FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
     OTHER DEALINGS IN THE FONT SOFTWARE.

     Except as contained in this notice, the name of Tavmjong Bah shall not
     be used in advertising or otherwise to promote the sale, use or other
     dealings in this Font Software without prior written authorization from
     Tavmjong Bah. For further information, contact: tavmjong @ free . fr.
     Retrieved from
     "[8]http://dejavu.sourceforge.net/wiki/index.php/License"

  References

     Visible links
     1. http://dejavu.sourceforge.net/wiki/index.php/License#column-one
     2. http://dejavu.sourceforge.net/wiki/index.php/License#searchInput
     3. http://gnome.org/fonts/
     4. http://dejavu.sourceforge.net/wiki/index.php/Bitstream_Vera_derivatives#Arev_Fonts
     7. http://tavmjong.free.fr/FONTS/ArevCopyright.txt
     8. http://dejavu.sourceforge.net/wiki/index.php/License
     9. http://dejavu.sourceforge.net/wiki/index.php/License

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: basemap
Source: http://sourceforge.net/projects/matplotlib

Files: *
Copyright: 2006, 2007, 2011 Jeff Whitaker <jeffrey.s.whitaker@noaa.gov>
License: MIT
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notices appear in all copies and that
 both the copyright notices and this permission notice appear in
 supporting documentation.
 THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: 2011-2015 Sandro Tosi <morph@debian.org>
License: same as upstream

Files: docs/*
Copyright: 2011 Jeff Whitaker <jeffrey.s.whitaker@noaa.gov>

Files: examples/allskymap.py
Author: Tom Loredo (based on Jeff Whitaker's code in Basemap's __init__.py module)

Files: examples/allskymap_cr_example.py
Author: Tom Loredo

Files: lib/mpl_toolkits/basemap/data/*
License: GPL-2+
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: lib/mpl_toolkits/basemap/{accumulator.py, constants.py, geodesiccapability.py, geodesicline.py, geodesic.py, geomath.py, polygonarea.py}
Copyright: Copyright (c) Charles Karney (2011) <charles@karney.com>
License: MIT/X11 License

Files: src/*
Copyright: Copyright (c) 2000, Frank Warmerdam
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: src/c_numpy.pxd
Author: Travis Oliphant

Files: src/geocent.*
License: None apply to this component.

Files: src/{nad_init.c, pj_apply_gridshift.c, pj_datum_set.c, pj_datums.c, pj_geocent.c, pj_gridinfo.c, pj_gridlist.c, pj_initcache.c, pj_latlong.c, pj_mutex.c, pj_transform.c, pj_utils.c, proj_api.h, projects.h}
Copyright: Copyright (c) 2000-2002, 2009 Frank Warmerdam

Files: src/{PJ_aea.c, PJ_aeqd.c, PJ_airy.c, PJ_aitoff.c, PJ_nzmg.c}
Copyright: Copyright (c) 1995, Gerald Evenden

Files: src/{pj_gauss.c, PJ_sterea.c, proj_mdist.c, proj_rouss.c, PJ_omerc.c, proj_etmerc.c}
Copyright: Copyright (c) 2003, 2004, 2006, 2008 Gerald I. Evenden

Files: src/PJ_geos.c
Copyright:
 Copyright (c) 2004   Gerald I. Evenden
 Copyright (c) 2012   Martin Raspaud

Files: src/{pj_gc_reader.c, pj_gridcatalog.c}
Copyright: Copyright (c) 2012, Frank Warmerdam <warmerdam@pobox.com>

Files: src/{pj_init.c, pj_open_lib.c}
Copyright:
 Copyright (c) 1995, Gerald Evenden
 Copyright (c) 2002, Frank Warmerdam <warmerdam@pobox.com>

Files: src/{pj_apply_vgridshift.c, pj_ctx.c, pj_log.c}
Copyright: Copyright (c) 2010, Frank Warmerdam <warmerdam@pobox.com>

Files: src/{PJ_krovak.c, PJ_healpix.c}
Copyright: Copyright (c) 2001, Thomas Flemming, tf@ttqv.com

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: basemap
Source: http://sourceforge.net/projects/matplotlib
Files-Excluded: geos-3.3.3

Files: *
Copyright: 2006, 2007, 2011 Jeff Whitaker <jeffrey.s.whitaker@noaa.gov>
License: MIT
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notices appear in all copies and that
 both the copyright notices and this permission notice appear in
 supporting documentation.
 THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: 2011-2019 Sandro Tosi <morph@debian.org>
License: same as upstream

Files: doc/*
Copyright: 2011 Jeff Whitaker <jeffrey.s.whitaker@noaa.gov>, 2016 The matplotlib development team

Files: examples/allskymap.py
Author: Tom Loredo (based on Jeff Whitaker's code in Basemap's __init__.py module)

Files: examples/allskymap_cr_example.py
Author: Tom Loredo

Files: lib/mpl_toolkits/basemap/data/*
License: GPL-2+
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files:
 lib/mpl_toolkits/basemap/{accumulator.py
 constants.py
 geodesiccapability.py
 geodesicline.py
 geodesic.py
 geomath.py
 polygonarea.py}
Copyright: Copyright (c) Charles Karney (2011) <charles@karney.com>
License: MIT/X11 License

Files: src/c_numpy.pxd
Author: Travis Oliphant

Files: lib/mpl_toolkits/basemap/data/epsg
Copyright: Copyright (c) 2000, Frank Warmerdam
License:
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://search.cpan.org/CPAN/authors/id/C/CJ/CJFIELDS/BioPerl-1.6.923.tar.gz

Files: *
Copyright: The Bioperl developers.
 PRIMARY AUTHORS AND MAJOR CONTRIBUTORS TO BIOPERL
       Releases co-ordinated and submitted by bioperl core devs.
 .
 .
       * Ewan Birney <birney at ebi.ac.uk>
       * Chris Dagdigian <dag at sonsorol.org>
       * Hilmar Lapp <hlapp at gmx.net>
       * Heikki Lehväslaiho <heikki at ebi.ac.uk>
       * Jason Stajich <jason at bioperl.org>
       * Lincoln Stein <lstein at cshl.org>
 .
       Previous Bioperl Coordinators:
 .
 .
       * Steven Brenner <brenner at compbio.berkely.edu>
       * Georg Fuellen <fuellen at alum.mit.edu>
       * Steve Chervitz <sac at bioperl.org>
 .
       Major Contributors
 .
       * Richard Adams <Richard.Adams at ed.ac.uk>
       * Shuly Avraham <avraham at cshl.org> - Bio::Graphics::Glyph
       * Sendu Bala <bix at sendu.me.uk>
       * Peter Blaiklock <pblaiklo at restrictionmapper.org>
       * Benjamin Berman <benb at fruitfly.berkeley.edu>
       * Matthew Betts <Matthew.Betts at ii.uib.no>
       * David Block <dblock at gnf.org>
       * Kris Boulez <kris.boulez at algonomics.com>
       * Tim Bunce <Tim.Bunce at pobox.com> - code optimizations
       * Scott Cain <cain at cshl.org> - Bio::Graphics::Glyph
       * Yee Man Chan <ymc at yahoo.com> - Bio::Tools::dpAlign
       * Brad Chapman <chapmanb at arches.uga.edu>
       * Michele Clamp <michele at sanger.ac.uk>
       * Tony Cox <avc at sanger.ac.uk>
       * James Cuff <james at sanger.ac.uk>
       * Andrew Dalke <dalke at acm.org>
       * Allen Day <allenday at ucla.edu>
       * Jared Fox <jaredfox at ucla.edu> - Bio::SeqIO::interpro
       * Brian O’Connor <boconnor at ucla.edu> - Bio::TreeIO::svggraph
       * James Diggans <JDiggans at genelogic.com>
       * Peter Dimitrov <dimitrov at gnf.org> - Bio::Ontology
       * Rich Dobson <r.j.dobson at qmul.ac.uk> - Bio::Pop‐Gen::IO::hapmap,phase
       * Paul Edlefsen <pedlefsen at systemsbiology.org>
       * Rob Edwards <redwards at utmem.edu> - Bio::Restriction
       * Arne Elofsson <arne at sbc.su.se>
       * David Evans <David.Evans at vir.gla.ac.uk>
       * Christopher Fields <cjfields at uiuc.edu>
       * Mark Fiers <M.W.E.J.Fiers at plant.wag-ur.nl>
       * The Fugu Team <fuguteam at fugu-sg.org>
       * Luc Gauthier <lgauthie at hotmail.com>
       * James Gilbert <jgrg at sanger.ac.uk>
       * Nat Goodman
       * Ed Green <ed at compbio.berkeley.edu>
       * Matthew Hahn <matthew.hahn at duke.edu>
       * Roger Hall <roger at iosea.com>
       * Todd Harris <harris at cshl.org> - SVG support in Bio::Graphics
       * Mauricio Herrera Cuadra <arareko at campus.iztacala.unam.mx>
       * Ian Holmes <ihn at fruitfly.org>
       * Shawn Hoon <shawnh at fugu-sg.org>
       * Robert Hubley <rhubley at systemsbiology.org>
       * Joseph Insana <insana at ebi.ac.uk> - Bio::LiveSeq
       * Donald Jackson <donald.jackson at bms.com> - SiRNA
       * Keith James <kdj at sanger.ac.uk> - Bio::Tools::Geneid
       * Nicolas Joly <njoly at pasteur.fr>
       * Ian Korf <ikorf at sapiens.wustl.edu>
       * Dan Kortschak <kortschak at rsbs.anu.edu.au>
       * Arek Kasprzyk <arek at ebi.ac.uk>
       * Andreas Kähäri <andreas.kahari at ebi.ac.uk>
       * Charles C. Kim <cckim at stanford.edu>
       * Stefan Kirov <skirov at utk.edu> - Bio::Matrix::PSM
       * Balamurugan Kumarasamy <savikalpa at fugu-sg.org>
       * Josh Lauricha <laurichj at cs.ucr.edu> - Bio::SeqIO::tigr
       * Eckhard Lehmann <ecky at e-lehmann.de>
       * Catherine Letondal <letondal at pasteur.fr>
       * Philip Lijnzaad <p.lijnzaad at med.uu.nl>
       * Aaron Mackey <amackey at pcbi.upenn.edu>
       * Brad Marshall <bradmars at yahoo.com>
       * Chad Matsalla <chad at dieselwurks.com>
       * Andrew Macgregor <andrew at anatomy.otago.ac.nz>
       * Sheldon McKay <mckays at cshl.edu>
       * Juha Muilu <muilu at ebi.ac.uk>
       * Chris Mungall <cjm at fruitfly.org>
       * Giri Narasimhan <giri at cs.fiu.edu>
       * Brian Osborne <bosborne at alum.mit.edu>
       * Xiaokang Pan <pan at cshl.org> - Bio::Graphics::Glyph
       * Jong Park
       * Matthew Pocock <matthew_pocock at yahoo.co.uk>
       * Lorenz Pollack <lorenz at ist.org> -- BPlite porting
       * Richard Resnick -- original Bio::Seq
       * Todd Richmond <todd at andrew2.stanford.edu>
       * Peter Schattner <schattner at alum.mit.edu>
       * Torsten Seemann <torsten.seemann at infotech.monash.edu.au> -- Bio::Tools::Run::StandaloneBlast
       * Martin Senger <senger at ebi.ac.uk> -- Bio::Biblio
       * Nigam Shah <nigam at psu.edu>
       * Shengqiang Shu <sshu at bdgp.lbl.gov> - Bio::Graphics::Glyph
       * Allen Smith <allens at cpan.org> -- Bio::Matrix and Bio::SimpleAlign fixes
       * Marc Sohrmann <ms2 at sanger.ac.uk>
       * Robson Francisco de Souza <rfsouza at citri.iq.usp.br> - Bio::Assem‐bly
       * Mark Southern <mark_southern at merck.com>
       * Will Spooner <whs at sanger.ac.uk>
       * Arne Stabenau <stabenau at ebi.ac.uk>
       * Elia Stupka <elia at fugu-sg.org>
       * Gert Thijs <gert.thijs at esat.kuleuven.ac.be>
       * James Thompson <tex at biosysadmin.com> - Bio::Matrix::PSM protein-related modules.
       * Charles Tilford <tilfordc at bms.com>
       * Anthony Underwood <aunderwood at phls.org.uk>
       * Paul-Christophe Varoutas
       * Andrew G. Walsh <paeruginosa at hotmail.com>
       * Kai Wang <tumorimmunology at yahoo.com>
       * Gary Williams <G.Williams at hgmp.mrc.ac.uk>
       * Mark Wilkinson <mwilkinson at gene.pbi.nrc.ca>
       * Helge Weissig <helgew at sdsc.edu>
       * Juguang Xiao <juguang at tll.org.sg>
       * Alex Zelensky <alex_zelensky at mac.com> - Bioperl-DB
       * Peili Zhang <peili at morgan.harvard.edu>
       * Christian M. Zmasek <czmasek at gnf.org> - Bio::Phenotype & Bio::Ontology
License: Perl
Comment:
 Everyone can use it! We don't care if you are academic, corporate, or
 government. BioPerl is licensed under the same terms as Perl itself, which
 means it is dually-licensed under either the Artistic or GPL licenses. The Perl
 Artistic License, or the GNU GPL covers all the legalese and what you can and
 can't do with the source code.
 .
 We do appreciate:
 .
  * You letting us know you sell or use a product that uses BioPerl. This helps
    us show people how useful our toolkit is. It also helps us if we seek funding
    from a government source, to identify the utility of the code to many different
    groups of users. Add your project and institution to our BioPerl Users page.
 .
  * If you fix bugs, please let us know about them. Because Bioperl is
    dual-licensed under the GPL or Artistic licenses, you can choose the Artistic
    license, which means that you are not required to submit the code fixes, but in
    the spirit of making a better product we hope you'll contribute back to the
    community any insight or code improvements.
 . 
  * Please include the AUTHORS file and ascribe credit to the original BioPerl
    toolkit where appropriate.
 . 
  * If you are an academic and you use the software, please cite the article.
    See the BioPerl publications for a list of papers which describe components in
    the toolkit. 
URL: see http://www.bioperl.org/wiki/Licensing_BioPerl

Files: t/lib/Error.pm
Copyright: © 1997-8 Graham Barr <gbarr@ti.com>
License: Perl

Files: Bio/SeqIO/bsml.pm
Copyright: © 2001 Charles Tilford <tilfordc@bms.com>
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
Comment: On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in ‘/usr/share/common-licenses/LGPL-2.1’.

Files: Bio/SeqIO/msout.pm Bio/SeqIO/mbsout.pm
Copyright: Public Domain work
License: public-domain
 This software/database is ``United States Government Work'' under the
 terms of the United States Copyright Act. It was written as part of
 the authors' official duties for the United States Government and thus
 cannot be copyrighted. This software/database is freely available to
 the public for use without a copyright notice. Restrictions cannot
 be placed on its present or future use.
 .
 Although all reasonable efforts have been taken to ensure the accuracy
 and reliability of the software and data, the National Human Genome
 Research Institute (NHGRI) and the U.S. Government does not and cannot
 warrant the performance or results that may be obtained by using this
 software or data.  NHGRI and the U.S. Government disclaims all
 warranties as to performance, merchantability or fitness for any
 particular purpose.

License: Perl
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
 a) the GNU General Public License as published by the Free Software
    Foundation; either version 1, or (at your option) any later
    version, or
 .
 b) the "Artistic License" which comes with Perl.
Comment: On Debian systems, the complete text of the Artistic License can be
 found in ‘/usr/share/common-licenses/Artistic’, and the complete text of the
 GNU General Public License version 1 can be found in
 ‘/usr/share/common-licenses/GPL-1’.

Files: debian/*
Copyright: © 1999 Stéphane Bortzmeyer <bortzmeyer@debian.org>
           © 2001 Dr. Guenter Bechly <gbechly@debian.org>
	   © 2001 Adrian Bunk <bunk@fs.tum.de>
	   © 2001—2004 Matt Hope <dopey@debian.org>
	   © 2007—2011 Charles Plessy <charles-debian-nospam@plessy.org>
	   © 2008 David Paleino <d.paleino@gmail.com>
	   © 2008 Nelson A. de Oliveira <naoliv@debian.org>
License: unclear
 The license under which is placed the work of the earlier contributors is
 unknown. The work of Charles Plessy can be treated as if it were public
 domain.

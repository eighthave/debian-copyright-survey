Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: baresip
Upstream-Contact: https://github.com/alfredh/baresip/issues/
 Alfred E. Heggestad <aeh@db.org>
Source: https://github.com/alfredh/baresip

Files: *
Copyright: 2010-2016, Alfred E. Heggestad <aeh@db.org>
  2010-2016, Creytiv.com
  2010-2016, Richard Aas <richard@db.org>
License: BSD-3-clause

Files: modules/gtk/call_window.c
 modules/gtk/dial_dialog.c
 modules/gtk/gtk_mod.h
 modules/gtk/transfer_dialog.c
 modules/gtk/uri_entry.c
 modules/gtk/gtk_mod.c
 modules/gtk/module.mk
Copyright: 2010-2015, Creytiv.com
  2015, Charles E. Lehner
License: BSD-3-clause

Files: modules/gst_video/encode.c
 modules/gst_video/gst_video.c
 modules/gst_video/gst_video.h
 modules/gst_video1/gst_video.c
 modules/gst_video1/gst_video.h
Copyright: 2010-2014, Creytiv.com
  2014, Fadeev Alexander
License: BSD-3-clause

Files: modules/gst_video1/encode.c
Copyright: 2010-2013, Creytiv.com
  2014, Fadeev Alexander
  2015, Thomas Strobel
License: BSD-3-clause

Files: modules/directfb/directfb.c
 modules/directfb/module.mk
Copyright: 2010, Creytiv.com
  2013, Andreas Shimokawa <andi@fischlustig.de>
License: BSD-3-clause

Files: modules/dtmfio/dtmfio.c
Copyright: 2014, Aaron Herting 'qwertos' <aaron@herting.cc>
License: BSD-3-clause

Files: modules/dshow/dshow.cpp
Copyright: 2010, Creytiv.com
  2010, Dusan Stevanovic
License: BSD-3-clause

Files: modules/mpa/*
Copyright: 2016, Symonics GmbH
License: BSD-3-clause

Files: modules/presence/publisher.c
Copyright: 2010, Creytiv.com
  2014, Juha Heinanen
License: BSD-3-clause

Files: debian/patches/2001_drop_libre_so_check.patch
Copyright: 2015-2016, Vasudev Kamath <vasudev@copyninja.info>
License: BSD-3-clause

Files: debian/*
Copyright: 2008,2016-2017, Jonas Smedegaard <dr@jones.dk>
  2015, Ramakrishnan Muthukrishnan <ram@rkrsihnan.org>
  2015, Vasudev Kamath <vasudev@copyninja.info>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the Creytiv.com nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

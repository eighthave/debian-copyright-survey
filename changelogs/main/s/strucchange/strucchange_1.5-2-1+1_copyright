This is the Debian GNU/Linux r-cran-strucchange package of
strucchange, a package for estimating structural change in regression
models. Strucchange was written by Achim Zeileis, Friedrich Leisch,
Bruce Hansen, Kurt Hornik, Christian Kleiber, Andrea Peters, and is
maintained by Achim Zeileis <Achim.Zeileis@wu-wien.ac.at>.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'strucchange' to
'r-cran-strucchange' to fit the pattern of CRAN (and non-CRAN) packages
for R.  

Copyright (C) 2003 - 2008 Achim Zeileis, Friedrich Leisch, Bruce Hansen, Kurt Hornik, Christian Kleiber and Andrea Peters 

License: GPL

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

   Package: strucchange
   Version: 1.2-5
   Date: 2004-09-29
   Title: Testing for Structural Change
   Author: Achim Zeileis, Friedrich Leisch, Bruce Hansen,
           Kurt Hornik, Christian Kleiber, Andrea Peters
   Maintainer: Achim Zeileis <Achim.Zeileis@wu-wien.ac.at>
   Description: Testing, dating and monitoring of structural change in
                linear regression relationships.
                strucchange features tests/methods from the generalized
                fluctuation test framework as well as from the F test (Chow
                test) framework. This includes methods to fit, plot and test
                fluctuation processes (e.g., CUSUM, MOSUM, recursive/moving
                estimates) and F statistics, respectively.
                It is possible to monitor incoming data online using
                fluctuation processes.
                Finally, the breakpoints in regression models with structural
                changes can be estimated together with confidence intervals.
                Emphasis is always given to methods for visualizing the data.
   Depends: sandwich, zoo, R (>= 1.5.0)
   License: GPL
   Packaged: Wed Sep 29 12:39:57 2004; zeileis

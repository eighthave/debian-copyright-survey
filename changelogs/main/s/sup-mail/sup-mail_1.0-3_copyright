Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sup
Source: https://github.com/sup-heliotrope/sup

Files: *
Copyright: 2013-2020 Sup developers <supmua@googlegroups.com>
           2006-2009 William Morgan <wmorgan-sup@masanjin.net>
License: GPL-2+

Files: lib/sup/rfc2047.rb
Copyright: 2004 Sam Roberts <sroberts@uniserve.com>
License: Ruby or GPL-2+

Files: test/test_crypto.rb
Copyright: 2013 Sup developers <supmua@googlegroups.com>
           2011 Clint Byrum <clint@ubuntu.com>
License: GPL-2+

Files: debian/*
Copyright: 2020 Utkarsh Gupta <utkarsh@debian.org>
           2020 Iain Parris <ipv2.vcs@parris.org>
           2014 Caitlin Matos <caitlin.matos@zoho.com>
           2012-2014 Per Andersson <avtobiff@debian.org>
           2008-2012 Decklin Foster <decklin@red-bean.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: Ruby
 Ruby is copyrighted free software by Yukihiro Matsumoto <matz@netlab.jp>.
 You can redistribute it and/or modify it under either the terms of the GPL
 (see the file GPL), or the conditions below:
 .
   1. You may make and give away verbatim copies of the source form of the
      software without restriction, provided that you duplicate all of the
      original copyright notices and associated disclaimers.
 .
   2. You may modify your copy of the software in any way, provided that
      you do at least ONE of the following:
 .
        a) place your modifications in the Public Domain or otherwise
           make them Freely Available, such as by posting said
           modifications to Usenet or an equivalent medium, or by allowing
           the author to include your modifications in the software.
 .
        b) use the modified software only within your corporation or
           organization.
 .
        c) give non-standard binaries non-standard names, with
           instructions on where to get the original software distribution.
 .
        d) make other distribution arrangements with the author.
 .
   3. You may distribute the software in object code or binary form,
      provided that you do at least ONE of the following:
 .
        a) distribute the binaries and library files of the software,
           together with instructions (in the manual page or equivalent)
           on where to get the original distribution.
 .
        b) accompany the distribution with the machine-readable source of
           the software.
 .
        c) give non-standard binaries non-standard names, with
           instructions on where to get the original software distribution.
 .
        d) make other distribution arrangements with the author.
 .
   4. You may modify and include the part of the software into any other
      software (possibly commercial).  But some files in the distribution
      are not written by the author, so that they are not under these terms.
 .
      For the list of those files and their copying conditions, see the
      file LEGAL.
 .
   5. The scripts and library files supplied as input to or produced as
      output from the software do not automatically fall under the
      copyright of the software, but belong to whomever generated them,
      and may be sold commercially, and may be aggregated with this
      software.
 .
   6. THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
      IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
      PURPOSE.

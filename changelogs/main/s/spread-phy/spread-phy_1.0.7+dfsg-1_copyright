Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Spread
Upstream-Contact: Filip Bielejec <filip.bielejec@rega.kuleuven.be>
Source: https://github.com/phylogeography/SPREAD/downloads
Files-Excluded:
    *.jar
    release/Mac
    release/Windows
    release/tools
    bin
    classes
    .git

Files: *
Copyright: © 2004-2012 Filip Bielejec <filip.bielejec@rega.kuleuven.be>,
                     Andrew Rambaut <a.rambaut@ed.ac.uk>,
                     Marc A. Suchard <msuchard@gmail.com>,
                     Philippe Lemey
License: LGPL-3+

Files: src/math/* src/utils/*
Copyright: © 2002-2006 Alexei Drummond and Andrew Rambaut
License: LGPL-2+

Files: src/colorpicker/*
Copyright: © 2011, Jeremy Wood
License: Modified-BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: © 2012 Andreas Tille <tille@debian.org>
License: LGPL-3+

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On a Debian GNU/Linux system, the GNU Lesser GPL license version 3 is included
 in the file ‘/usr/share/common-licenses/LGPL-3’, and the GNU GPL license
 version 3 is included in the file ‘/usr/share/common-licenses/GPL-3’.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with BEAST; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301  USA
 .
 On a Debian GNU/Linux system, the GNU Lesser GPL license version 2 is
 included in the file ‘/usr/share/common-licenses/LGPL-2’.

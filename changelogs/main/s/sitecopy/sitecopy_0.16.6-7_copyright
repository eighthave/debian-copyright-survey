Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: sitecopy
Upstream-Contact: Joe Orton <joe@manyfish.co.uk>
Source: http://www.manyfish.co.uk/sitecopy/

Files: *
Copyright:
  1998-2008 Joe Orton <joe@manyfish.co.uk>
  1987-2004 Free Software Foundation, Inc
  2004 David A Knight <david@screem.org>
  2003 Nobuyuki Tsuchimura <tutimura@nn.iij4u.or.jp>
  1997 Janos Farkas (chexum@shadow.banki.hu)
X-Source-Comment:
 The bundled Expat XML parser is Copyright (C) 1998 James Clark
License: GPL-2+

Files: gnome/*
Copyright:
  1999 Lee Mallabone <lee@fonicmonkey.net>
License: GPL-2+

Files: debian/*
Copyright:
  2008-2012 Sandro Tosi <morph@debian.org>
  2006-2012 Kartik Mistry <kartik.mistry@gmail.com>
  2007-2012 Christian Marillat <marillat@debian.org>
  2012      Jari Aalto <jari.aalto@cante.net>
  2003-2006 Reed Snellenberger <rsnellenberger@houston.rr.com>
  2001-2003 Masayuki Hatta <mhatta@debian.org>
  1998-2000 Tom Lees <tom@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

Files: intl/*
Copyright:
  1995-1998, 2000-2003, Free Software Foundation, Inc
License: LGPL-2+

Files: lib/neon/*
X-File-Comment:
 Excluding following files:
 .
  ept
  ne_basic.h
  ne_dates.c
  ne_gnutls.c
  ne_md5.*
  ne_openssl.c
  ne_session.c
  ne_socket.c
  ne_sspi.c
Copyright:
  1999-2006 Joe Orton <joe@manyfish.co.uk>
License: LGPL-2+

Files: lib/neon/ne_sspi.c
Copyright:
  2004-2005 Vladimir Berezniker
License: LGPL-2+

Files: lib/neon/ne_gnutls.c
Copyright:
  2002-2006 Joe Orton <joe@manyfish.co.uk>
  2004 Aleix Conchillo Flaque <aleix@member.fsf.org>
License: LGPL-2+

Files: lib/neon/ne_openssl.c
Copyright:
  2002-2006, Joe Orton <joe@manyfish.co.uk>
  1999-2000 Tommi Komulainen <Tommi.Komulainen@iki.fi>
License: LGPL-2+

Files: lib/neon/ne_socket.c
Copyright:
  1998-2006 Joe Orton <joe@manyfish.co.uk>
  2004 Aleix Conchillo Flaque <aleix@member.fsf.org>
  1999-2000 Tommi Komulainen <Tommi.Komulainen@iki.fi>
License: LGPL-2+

Files: lib/neon/ne_md5.*
Copyright:
  1995, 1996, 1997 Free Software Foundation, Inc
License: LGPL-2+

Files: lib/neon/ne_basic.h
Copyright:
  1999-2006, Joe Orton <joe@manyfish.co.uk>
License: LGPL-2+

Files: lib/neon/ne_dates.c
Copyright:
  2004 Jiang Lei <tristone@deluxe.ocn.ne.jp>
  1999-2005 Joe Orton <joe@manyfish.co.uk>
License: LGPL-2+

License: LGPL-2+
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful,  but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

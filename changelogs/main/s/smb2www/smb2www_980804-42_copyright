Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Remco van Mook
Upstream-Contact: remco@samba.anu.edu.au
Source: http://samba.anu.edu.au/samba/smb2www/

Files: *
Copyright: © 1997-1998, Remco van Mook <remco@samba.anu.edu.au>
License: GPL-2+

Files: Base64.pm
Copyright: © 1995, 1996 Gisle Aas.
License: Artistic or GPL-1+

Files: cgi-bin/smbmsg.pl
Copyright: © 1998, Edward Rudd <eddie@omegaware.com>
License: GPL-2+

Files: debian/*
Copyright: © 1998-2000, Craig Small <csmall@debian.org>
           © 2000-2019 Robert Luberda <robert@debian.org>
License: GPL-2+

Files: debian/addons/po4a/po/cs.po
Copyright: © 2005, Miroslav Kure <kurem@debian.cz>
License: GPL-2+

Files: debian/addons/po4a/po/de.po
Copyright: © 2017, Chris Leick <c.leick@vollbio.de>
License: GPL-2+

Files: debian/addons/po4a/po/es.po
Copyright: © 2005, Gunnar Wolf <gwolf@gwolf.org>
License: GPL-2+

Files: debian/addons/po4a/po/fr.po
Copyright: © 2005, Nicolas Haller
License: GPL-2+

Files: debian/addons/po4a/po/nl.po
Copyright: © 2007, Bart Cornelis <cobaco@skolelinux.no>
License: GPL-2+

Files: debian/addons/po4a/po/pl.po
Copyright: © 2005, Robert Luberda <robert@debian.org>
License: GPL-2+

Files: debian/addons/po4a/po/sv.po
Copyright: © 2005, Daniel Nylander <yeager@lidkoping.net>
License: GPL-2+

Files: debian/addons/po4a/po/vi.po
Copyright: © 2005, Clytie Siddall <clytie@riverland.net.au>
License: GPL-2+

Files: debian/po/ca.po
Copyright: © 2004, Aleix Badia i Bosch <abadia@ica.es>
           © 2007, Matt Bonner <mattbonner@gmail.com>
License: GPL-2+

Files: debian/po/cs.po
Copyright: © 2007, Miroslav Kure <kurem@debian.cz>
License: GPL-2+

Files: debian/po/da.po
Copyright: © 2004, 2005, Claus Hindsgaul <claus_h@image.dk>
           © 2010, Joe Hansen <joedalton2@yahoo.dk>
License: GPL-2+

Files: debian/po/de.po
Copyright: © 2006, 2007 Helge Kreutzmann <debian@helgefjell.de>
License: GPL-2+

Files: debian/po/es.po
Copyright: © 2008, Enrique Monge <emonge@debian.org.sv>
License: GPL-2+

Files: debian/po/eu.po
Copyright: © 2007, Piarres Beobide <pi@beobide.net>
License: GPL-2+

Files: debian/po/fi.po
Copyright: © 2007, Esko Arajärvi <edu@iki.fi>
License: GPL-2+

Files: debian/po/fr.po
Copyright: © 2005-2007, Christian Perrier <bubulle@debian.org>
License: GPL-2+

Files: debian/po/gl.po
Copyright: © 2007, Jacobo Tarrio <jtarrio@debian.org>
License: GPL-2+

Files: debian/po/it.po
Copyright: © 2011, Fabrizio Regalli <fabreg@fabreg.it>
License: GPL-2+

Files: debian/po/ja.po
Copyright: © 2009, Hideki Yamane (Debian-JP) <henrich@debian.or.jp>
License: GPL-2+

Files: debian/po/nb.po
Copyright: © 2007, Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
License: GPL-2+

Files: debian/po/nl.po
Copyright: © 2007, Bart Cornelis <cobaco@skolelinux.no>
License: GPL-2+

Files: debian/po/pl.po
Copyright: © 2007i, Wojciech Zareba <wojtekz@comp.waw.pl>
License: GPL-2+

Files: debian/po/pt.po
Copyright: © 2005 - 2007, Rui Branco <ruipb@debianpt.org>
License: GPL-2+

Files: debian/po/pt_BR.po
Copyright: © 2003, André Luís Lopes <andrelop@ig.com.br>
License: GPL-2+

Files: debian/po/ru.po
Copyright: © 2007, Yuri Kozlov <kozlov.y@gmail.com>
License: GPL-2+

Files: debian/po/sv.po
Copyright: © 2008, Martin Ågren <martin.agren@gmail.com>
License: GPL-2+

Files: debian/po/vi.po
Copyright: © 2005-2007, Clytie Siddall <clytie@riverland.net.au>
License: GPL-2+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 On Debian systems, the complete text of the Artistic License can be
 found in '/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 1, February 1989, or (at
 your option) any later version
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'
 or `/usr/share/common-licenses/GPL-3'.

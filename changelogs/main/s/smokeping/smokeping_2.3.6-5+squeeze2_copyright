This package was debianized by Jose Carlos Garcia Sogo <jsogo@debian.org> on
Wed, 13 Feb 2002 23:11:07 +0100.

It was downloaded from http://people.ee.ethz.ch/~oetiker/webtools/smokeping/

Upstream authors: Tobias Oetiker <tobi@oetiker.ch> and Niko Tyni <ntyni@iki.fi>

Copyright:

  Copyright (c) 2001-2005 Tobias Oetiker <tobi@oetiker.ch>

The file bin/tSmoke.dist has the following copyright:

  Copyright (c) 2003 by Dan McGinn-Combs. All right reserved.

The files lib/Smokeping/Examples.pm and lib/Smokeping/RRDtools.pm
have the following copyright:

  Copyright 2005 by Niko Tyni.

The files Avgratio.pm, Median.pm and base.pm in lib/Smokeping/matchers/
have the following copyright:

  Copyright (c) 2004 by OETIKER+PARTNER AG. All rights reserved.

The files CheckLatency.pm and CheckLoss.pm in lib/Smokeping/matchers/ 
have the following copyright:

  Copyright (c) 2006 Dylan C Vanderhoof, Semaphore Corporation

All files expect those mentioned below share the GPL license:

  All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation; either version 2 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
	      
You can find a copy of GPL license at /usr/share/common-licenses/GPL

The files in the lib/Config directory belong the bundled Config::Grammar
Perl module and share the following copyright and license:

  Copyright (c) 2000-2005 by ETH Zurich. All rights reserved.
  Copyright (c) 2007 by David Schweikert. All rights reserved.

  This program is free software; you can redistribute it and/or modify it
  under the same terms as Perl itself.

Perl is distributed under the terms of either the Artistic license or the GPL
(version 1 or later). You can find a copy of the Artistic license at 
/usr/share/common-licenses/Artistic and the license of Perl itself at
/usr/share/doc/perl/copyright .

The files lib/BER.pm and lib/SNMP_Session.pm, which are not used in the
Debian version, have the following copyright and license:

  Copyright (c) 1995-2002, Simon Leinen.

  This program is free software; you can redistribute it under the
  "Artistic License" included in this distribution (file "Artistic").

The file lib/SNMP_util.pm, which is not used in the Debian version,
has the following copyright and license:
  Copyright (c) 1998-2002, Mike Mitchell.

  This program is free software; you can redistribute it under the
  "Artistic License" included in this distribution (file "Artistic").

You can find a copy of the Artistic license at /usr/share/common-licenses/Artistic

The files HMAC.pm, HMAC_MD5.pm and HMAC_SHA1.pm in lib/Digest are
copies from the Digest::HMAC module and are not used in the Debian
package.  They share the following copyright and license, included as
lib/Digest/HMAC.LICENSE:

  Copyright 1998-2001 Gisle Aas.
  Copyright 1998 Graham Barr.

  This library is free software; you can redistribute it and/or
  modify it under the same terms as Perl itself.

The files htdocs/cropper/cropper.js and htdocs/cropper/cropper.uncompressed.js 
have the following copyright and license:

 Copyright (c) 2006, David Spurr (http://www.defusion.org.uk/)
 All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:

     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright 
       notice, this list of conditions and the following disclaimer in the 
       documentation and/or other materials provided with the distribution.
     * Neither the name of the David Spurr nor the names of its contributors 
       may be used to endorse or promote products derived from this software 
       without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 http://www.opensource.org/licenses/bsd-license.php

The files in the htdocs/cropper/lib directory have the following copyright
and license:

 builder.js
 effects.js
 scriptaculous.js
  Copyright (c) 2005 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)

 controls.js
  Copyright (c) 2005 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
            (c) 2005 Ivan Krstic (http://blogs.law.harvard.edu/ivan
            (c) 2005 Jon Tirsen (http://www.tirsen.com)

 dragdrop.js
  Copyright (c) 2005 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
            (c) 2005 Sammi Williams (http://www.oriontransfer.co.nz, 
                     sammi@oriontransfer.co.nz)

 slider.js
  Copyright (c) 2005 Marty Haught, Thomas Fuchs 

 unittest.js
  Copyright (c) 2005 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
            (c) 2005 Jon Tirsen (http://www.tirsen.com)
            (c) 2005 Michael Schuerig (http://www.schuerig.de/michael/)

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:
  
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 prototype.js
  (c) 2005 Sam Stephenson <sam@conio.net>

  Prototype is freely distributable under the terms of an MIT-style license.
  For details, see the Prototype web site: http://prototype.conio.net/

This 'MIT-style' license is included in prototype.js.LICENSE:

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

-- Niko Tyni <ntyni@iki.fi> Mon, 17 Sep 2007 11:11:14 +0300


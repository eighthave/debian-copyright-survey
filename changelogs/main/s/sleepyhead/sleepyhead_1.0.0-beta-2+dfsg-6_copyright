Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SleepyHead
Source: http://sleepyhead.sourceforge.net
Files-Excluded:
 3rdparty/quazip/doc/html/jquery.js
 3rdparty/*
 Bundle3rdParty

Files: *
Copyright: 2011-2014 Mark Watkins <jedimark@users.sourceforge.net>
License: GPL-3

Files:
 sleepyhead/icons/svg/forward.svg
 sleepyhead/icons/svg/back.svg
 sleepyhead/icons/svg/media.svg
 sleepyhead/icons/svg/calendar.svg
Copyright: Jakub Steiner
License: public-domain

Files:
 sleepyhead/icons/svg/moon.svg
Copyright: Frank Solensky
License: GPL-2

Files: debian/*
Copyright: 2015-2017 Sergio Durigan Junior <sergiodj@sergiodj.net>
License: GPL-3

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License, v2, as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: public-domain
 The person or persons who have associated work with this document (the
 "Dedicator" or "Certifier") hereby either (a) certifies that, to the
 best of his knowledge, the work of authorship identified is in the
 public domain of the country from which the work is published, or (b)
 hereby dedicates whatever copyright the dedicators holds in the work
 of authorship identified below (the "Work") to the public domain. A
 certifier, moreover, dedicates any copyright interest he may have in
 the associated work, and for these purposes, is described as a
 "dedicator" below.
 .
 A certifier has taken reasonable steps to verify the copyright status
 of this work. Certifier recognizes that his good faith efforts may not
 shield him from liability if in fact the work certified is not in the
 public domain.
 .
 Dedicator makes this dedication for the benefit of the public at large
 and to the detriment of the Dedicator's heirs and
 successors. Dedicator intends this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights under
 copyright law, whether vested or contingent, in the Work. Dedicator
 understands that such relinquishment of all rights includes the
 relinquishment of all rights to enforce (by lawsuit or otherwise)
 those copyrights in the Work.
 .
 Dedicator recognizes that, once placed in the public domain, the Work
 may be freely reproduced, distributed, transmitted, used, modified,
 built upon, or otherwise exploited by anyone for any purpose,
 commercial or non-commercial, and in any way, including by methods
 that have not yet been invented or conceived.
 .
 This license text was taken from
 <http://creativecommons.org/licenses/publicdomain/>.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: stardict
Source: https://sourceforge.net/projects/stardict-4/
Files-Excluded:
 dict/help/stardict.omf.in
 tools/src/*.exe
 tools/src/dictgen.php
 tools/src/extractKangXi.py
 tools/src/makevietdict.py
Comment:
 dict/help/stardict.omf.in: needs to be removed for the build to work
                            without scrollkeeper and internet access.
 tools/src/*.exe: binary without source
 tools/src/dictgen.php: license not clear
 tools/src/extractKangXi.py: license not clear
 tools/src/makevietdict.py: license not clear

Files: *
Copyright: 2003, 2004, 2005, 2006, 2007 Hu Zheng
           Evgeniy Dushistov
           Alex Murygin
License: GPL-3+

Files: dict/help/*
Copyright: Hu Zheng
License: GFDL-NIV-1.2+

Files: dict/src/lib/ctype-uca.cpp
       dict/src/lib/ctype-utf8.cpp
Copyright: 2000, 2004 MySQL AB
           2011, kubtek
License: LGPL-2+

Files: dict/src/sigc++/*
Copyright: 2002, 2003, 2005 The libsigc++ Development Team
License: LGPL-2.1+

Files: dict/src/eggaccelerators.*
Copyright: 2002, 2003, 2005 The libsigc++ Development Team
License: LGPL-2+

Files: tools/src/libsd2foldoc.cpp
       tools/src/libsd2foldoc.h
       tools/src/sd2foldoc.cpp
Copyright: dazhiqian
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1) Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3) Neither the name of the ORGANIZATION nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GFDL-NIV-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License can be found in `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This file is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

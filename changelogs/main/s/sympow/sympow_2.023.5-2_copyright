Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: sympow
Upstream-Contact:
 Jerome G. M. Benoit <jgmbenoit@rezozer.net>
 Mark Watkins <watkins@maths.usyd.edu.au>
Source: https://gitlab.com/rezozer/forks/sympow

Files: *
Copyright:
 2018 Jerome G. M. Benoit <jgmbenoit@rezozer.net>
 2005-2018 Mark Watkins <watkins@maths.usyd.edu.au>
License: MW-adhoc

Files: debian/*
Copyright:
 2014-2018 Jerome Benoit <calculus@rezozer.net>
 2008 Tim Abbott <tabbott@mit.edu>
License: GPL-2+
Comment:
 This package was originally debianized by Tim Abbott <tabbott@mit.edu>.

License: MW-adhoc
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistribution of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistribution in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * If redistribution is done as a part of a compilation that has a more
   restrictive license (such as the GPL), then the fact that SYMPOW has
   a less restrictive license must be made clear to the recipient.
   For example, a line like (include bracketed text if SYMPOW is modified):
     "This compilation includes [a modification of] SYMPOW whose [original]
      code has a less-restrictive license than the entire compilation."
   should appear in a suitable place in the COPYING and/or LICENSE file.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

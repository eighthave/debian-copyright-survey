Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sonic Visualizer
Upstream-Contact: Chris Cannam <chris.cannam@breakfastquay.com>
Source: https://www.sonicvisualiser.org/download.html
Files-Excluded:
 */.git*
 */.hg*

Files: *
Copyright: 2000-2020, Chris Cannam, and Queen Mary University of London
License: GPL-2+

Files: checker/*
Copyright: 2008-2016, Queen Mary University of London
License: Expat

Files: dataquay/*
Copyright: 2009-2016, Chris Cannam, Particular Programs Ltd
 2005-2017, Chris Cannam, and Queen Mary University of London
License: Expat

Files: bqaudioio/*
 bqaudiostream/*
 bqfft/*
 bqresample/*
 bqthingfactory/*
 bqvec/*
Copyright: 2007-2019, Particular Programs Ltd.
License: Expat

Files: bqaudiostream/test/*.cpp
  bqaudiostream/test/*.h
Copyright: Chris Cannam
License: Expat


Files: svcore/plugin/RealTimePluginFactory.cpp
Copyright: 2000-2006 Guillaume Laurent <glaurent@telegraph-road.org>
 2000-2006, Chris Cannam <cannam@all-day-breakfast.com>
 2000-2006, Richard Bown <bownie@bownie.com>
License: GPL-2+

Files: svcore/plugin/LADSPAPlugin*
 svcore/data/fileio/MIDIFile*
 svcore/data/midi/MIDIEvent.h
Copyright: 2000-2006, Chris Cannam, Richard Bown, and Queen Mary University of London
License: GPL-2+

Files: svcore/base/Profiler.*
Copyright: 2000-2006, Chris Cannam, Guillaume Laurent, and Queen Mary University of London
License: GPL-2+

Files: svcore/base/ResourceFinder.*
Copyright: 2005-2011, Chris Cannam and the Rosegarden development team
License: GPL-2+

Files: svcore/base/MovingMedian.h
Copyright: 2007-2015, Particular Programs Ltd,
 2018, Queen Mary University of London.
License: GPL-2+

Files: svcore/data/midi/rtmidi/*
Copyright: 2003-2016, Gary P. Scavone
License: Expat

Files: svcore/plugin/api/alsa/*
Copyright: 1998-2001, Jaroslav Kysela <perex@suse.cz>
 1998-2001, Abramo Bagnara <abramo@alsa-project.org>
 1998-2001, Takashi Iwai <tiwai@suse.de>
License: LGPL-2.1+

Files: svcore/plugin/api/alsa/sound/asequencer.h
Copyright: 1998-1999, Frank van de Pol <fvdpol@coil.demon.nl>
  1998-1999, Jaroslav Kysela <perex@suse.cz>
License: GPL-2+

Files: svcore/plugin/api/dssi.h
 svcore/plugin/api/new_dssi_transport_patch
Copyright: 2004-2005, Chris Cannam, Steve Harris and Sean Bolton
License: LGPL-2.1+

Files: svcore/plugin/api/ladspa.h
Copyright: 2000-2002, Richard W.E. Furse, Paul Barton-Davis,
License: LGPL-2.1+

Files: svgui/widgets/Fader.*
Copyright: (c) 2002-2005 by Alex >Comix< Cominu [comix@users.sourceforge.net]
License: GPL-2+

Files: svgui/widgets/AudioDial.*
Copyright: 2003-2006, Chris Cannam
 2005, Pedro Lopez-Cabanillas
 2006, Queen Mary, University of London.
License: GPL-2+

Files: svgui/widgets/CommandHistory.*
Copyright: 2000, Werner Trobin and David Faure
  2000-2006, Chris Cannam
License: GPL-2+

Files: svgui/widgets/LEDButton.*
Copyright: 1998-2004, Jörg Habenicht, Richard J Moore, Chris Cannam and others (Queen Mary University, London)
License: GPL-2+

Files: bqvec/pommier/*
Copyright: 2007-2011, Julien Pommier
License: Zlib

Files: bqresample/speex/*
Copyright: 1992-1994, Jutta Degener, Carsten Bormann
  1993, 2002, 2006, David Rowe
  2002-2007, Jean-Marc Valin
  2002-2007, Xiph.org Foundation
  2003, EpicGames
  2005-2007, Analog Devices Inc.
  2005-2007, Commonwealth Scientific and Industrial Research
License: BSD-3-clause

Files: vamp-plugin-sdk/*
Copyright: 2009-2016, Chris Cannam, Particular Programs Ltd
 2005-2017, Chris Cannam, and Queen Mary University of London
License: Expat

Files: vamp-plugin-sdk/src/vamp-sdk/ext/*
Copyright: 2003-2010, Mark Borgerding
License: BSD-3-clause

Files: vamp-plugin-sdk/examples/AmplitudeFollower.*
Copyright: 2006, Dan Stowell.
License: Expat

Files: piper-vamp-cpp/*
Copyright: 2006-2019, Chris Cannam, and Queen Mary University of London
License: Expat

Files: piper-vamp-cpp/ext/base-n/*
Copyright: 2012, Andrzej Zawadzki <azawadzki@gmail.com>
License: Expat

Files: piper-vamp-cpp/ext/json11/*
Copyright: 2013, Dropbox, Inc.
License: Expat

Files: piper-vamp-cpp/ext/catch/catch.hpp
Copyright: 2012, Two Blue Cubes Ltd.
License: BSL-1.0

Files: piper-vamp-cpp/ext/serd/*
 piper-vamp-cpp/ext/sord/*
Copyright: 2011-2017, David Robillard <http://drobilla.net>
License: ISC

Files: i18n/sonic-visualiser_cs_CZ.*
Copyright: 2010-2018, Pavel Fric
License: GPL-2+

Files: i18n/sonic-visualiser_ru.*
Copyright: 2006-2018, Alexandre Prokoudine
License: GPL-2+

Files: debian/*
Copyright: 2010-2013, Alessio Treglia <alessio@debian.org>
 2012-2017, Jaromír Mikeš <mira.mikes@seznam.cz>
 2017, IOhannes m zmölnig <umlaeute@debian.org>
License: GPL-2+

Files: debian/missing-sources/piper.capnp
Copyright: 2006-2017, Chris Cannam, and Queen Mary University of London
License: Expat
Comment:
 piper.capnp was downloaded from https://github.com/piper-audio/piper/blob/dd42cc30c2e7d583b49e2d021746b0d5a295505e/capnp/piper.capnp


License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the names of Chris Cannam and
 Particular Programs Ltd shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this
 Software without prior written authorization.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. The name of the author may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: BSL-1.0
 Distributed under the Boost Software License, Version 1.0. (See accompanying
 file LICENSE_1_0.txt or copy at https://www.boost.org/LICENSE_1_0.txt)

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

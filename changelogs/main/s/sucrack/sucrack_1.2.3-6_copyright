Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sucrack
Source: https://labs.portcullis.co.uk/tools/sucrack/

Files: *
Copyright: 2006 Nico Leidecker <nfl@portcullis-security.com>
License: BSD-3-Clause

Files: debian/*
Copyright: 2007 Tim Brown <timb@nth-dimension.org.uk>
           2012 Eva Ramon Salinas <debian@empanadilla.net>
           2016 Marcos Fouces <mfouces@yahoo.es>
           2016 Sophie Brun <sophie@freexian.com>
           2016 Raphaël Hertzog <hertzog@debian.org>
           2018 SZ Lin (林上智) <szlin@debian.org>
License: GPL-3

License: BSD-3-Clause
 Redistribution and use of this software in source and binary forms,
 with or without modification, are permitted provided that the
 following conditions are met:
 .
 * Redistributions of source code must retain the above
   copyright notice, this list of conditions and the
   following disclaimer.
 .
 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the
   following disclaimer in the documentation and/or other
   materials provided with the distribution.
 .
 * Neither the names of the authors nor the names of their institutions
   may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in the /usr/share/common-licenses/GPL-3 file.

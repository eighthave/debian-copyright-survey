Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: The SWORD Project
Upstream-Contact: Members of the SWORD Project team <sword-feedback@crosswire.org>
  The CrossWire Bible Society
  P. O. Box 2528
  Tempe, AZ  85280-2528
Source: http://www.crosswire.org/sword/software/swordapi.jsp
Comment: Note that the original source tarball was repackaged to
 remove some non-free files, and other convenience copies of
 libraries.  There is a get-orig-source rule in debian/rules
 which does this repacking.  The files removed are:
 .
 src/utilfuns/zlib/[a-tv-z]* # an old copy of the zlib library
 src/utilfuns/zlib/uncompr.c # more of zlib
 include/zlib.h              # zlib header file
 src/utilfuns/regex.c # modified GNU C library
 include/regex.h # modified GNU C library header file
 src/utilfuns/win32/* # a non-free dirent for Windows 
 bindings/gsoap/include/stdsoap.h # non-free header file

Files: *
Copyright: 1996-2009 CrossWire Bible Society
License: GPL-2


Files: include/untgz.h src/utilfuns/zlib/untgz.c
Copyright: Pedro A. Aranda Guti\irrez <paag@tid.es>
           Jean-loup Gailly <jloup@gzip.org>
Comment: The author of zlib has made changes to untgz.c, it is shipped in the
 zlib Debian tarball, but is not installed/compiled. So I think we can assume the
 zlib/libpng license.
License: ZLIB

Files: include/sapphire.h src/modules/common/sapphire.cpp
Copyright: Michael Paul Johnson
License: public-domain
 Dedicated to the Public Domain the author and inventor
 (Michael Paul Johnson).  This code comes with no warranty.
 Use it at your own risk.

Files: bindings/corba/java/src/org/crosswire/util/Base64.java
Copyright: Robert Harder <rob@iharder.net>
License: public-domain
 I am placing this code in the Public Domain. Do with it as you will.
 This software comes with no guarantees or warranties but with
 plenty of well-wishing instead!

Files: include/ftplib.h src/utilfuns/ftplib.c
Copyright: 1996, 1997 Thomas Pfau, pfau@cnj.digex.net
           73 Catherine Street, South Bound Brook, NJ, 08880
License: LGPL-2+

Files: debian/*
Copyright: 2009 CrossWire Packaging Team <pkg-crosswire-devel@lists.alioth.debian.org>
            -2008 Daniel Glassey <wdg@debian.org>
License: GPL-2+

License: GPL-2+
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in /usr/share/common-licenses/GPL-2.

License: GPL-2
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in /usr/share/common-licenses/GPL-2.

License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General
 Public License, version 2, can be found in
 /usr/share/common-licenses/LGPL-2.

License: ZLIB
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

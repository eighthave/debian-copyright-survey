Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SYMPA - SYsteme de Multi-Postage Automatique
Upstream-Contact: CRU SYMPA Authors <sympa-authors@cru.fr>
Source: http://listes.cru.fr/sympa/distribution
Files-Excluded:
    .gitignore
    www/js/jquery.js
    www/js/jquery-migrate.js
    www/js/jquery-ui/*
    www/js/jquery-minicolors/*

Files: *
Copyright: 1997-1999, Christophe Wolfhugel
           1997-1999, Institut Pasteur
           1997-2011, Comité Réseau des Universités
           2007-2011, David Verdin <david.verdin@cru.fr>
           2001-2011, Olivier Salaün <olivier.salaun@cru.fr>
           2011-2011, Serge Aumont <serge.aumont@cru.fr>
           2013-2016, IKEDA Soji <hatuka@nezumi.nu>
           2011-2016, GIP RENATER
License: GPL-2+

Files: po/sympa/ar.po
Copyright: 2009, Ahmad Gharbeia <gharbeia@gmail.com>
           2015, acasado <acasado@ual.es>
License: GPL-2+

Files: po/sympa/bg.po
Copyright: 2011, Български
License: GPL-2+

Files: po/sympa/br.po
Copyright: 2015, Brezhoneg <LL@li.org>
License: GPL-2+

Files: po/sympa/ca.po
Copyright: 2009, Jordi Giralt Baldellou <projectek2upcnet.es>
           Català <projectek2@upcnet.es>
License: GPL-2+

Files: po/sympa/cs.po
Copyright: 2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/de.po
Copyright: Daniel <d.stoye@cms.hu-berlin.de>
License: GPL-2+

Files: po/sympa/el.po
Copyright: 2008, Santas Konstantinos <santas@eap.gr>
           2012, kopolyzo <kopolyzo@ccf.auth.gr>
License: GPL-2+

Files: po/sympa/en_US.po
Copyright: 2013, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/es.po
Copyright: 2007-2009, Free Software Foundation, Inc.
           2009, dani@belvil.net
           2010-2012, Alfonso Marin <amm44@alu.um.es>
           2016, Antonio Casado <acasado@ual.es>
License: GPL-2+

Files: po/sympa/et.po
Copyright: 2012-2016, Urmas <urmas@tamme.tartu.ee>
License: GPL-2+

Files: po/sympa/eu.po
Copyright: 2006, Rosetta Contributors and Canonical
           2006, Piarres Beobide <pi@beobide.net>
           2013, dabid <dabid@hackinbadakigu.net>
           2015, shagi <shagi@gisa-elkartea.org>
License: GPL-2+

Files: po/sympa/fi.po
Copyright: 2009-2013, Jyri-Petteri Paloposki <jyri-petteri.paloposki@iki.fi>
License: GPL-2+

Files: po/sympa/fr.po
Copyright: 2016, dverdin <david.verdin@renater.fr>
License: GPL-2+

Files: po/sympa/gl.po
Copyright: 2014-2016, Xesús M. Mosquera <xesusmosquera@hotmail.com>
License: GPL-2+

Files: po/sympa/hu.po
Copyright: 2012, Magyar <kde-i18n-doc@kde.org>
License: GPL-2+

Files: po/sympa/id.po
Copyright: 2010, Gian <wejick@gmail.com>*
           2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/it.po
Copyright: 2016, riccardo.zampolini <riccardo.zampolini@unipg.it>
License: GPL-2+

Files: po/sympa/ja.po
Copyright: 2005, moriwaka@valinux.co.jp
           2006-2014, IKEDA Soji <hatuka@nezumi.nu>
           2016, David Verdin <david.verdin@renater.fr>
License: GPL-2+

Files: po/sympa/ko.po
Copyright: 2008, Kang, Tae-Hee <tkang@hp.com>
License: GPL-2+

Files: po/sympa/ml.po
Copyright: 2009, jayasurian makkoth <jayasurian123@yahoo.com>
           2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/nb.po
Copyright: 2005, Anders Lund <anders.lund@uninett.no>
           2009, Ingar <iv+sympa@usit.uio.no>
           2012, IKEDA Soji <hatuka@nezumi.nu>
           2017, skh <sivert.hatteberg@usit.uio.no>
License: GPL-2+

Files: po/sympa/nl.po
Copyright: 2013, Kwadro Naut <kwadronaut@autistici.org>
License: GPL-2+

Files: po/sympa/oc.po
Copyright: 2014, Cedric31 <cvalmary@yahoo.fr>
           Occitan <asso.oc@abuledu.org>
License: GPL-2+

Files: po/sympa/pl.po
Copyright: 2009-2014, Marcin Woloszyn <marcin.woloszyn@us.edu.pl>
License: GPL-2+

Files: po/sympa/pt_BR.po
Copyright: 2005, Canonical Ltd, and Rosetta Contributors
           2010, Fernando H Rosa <pessoal@fernandohrosa.com.br>
           2014, rmanola <rmanola@gmail.com>
           vitalb <vitalb@riseup.net>
License: GPL-2+

Files: po/sympa/ro.po
Copyright: 2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/ru.po
Copyright: 2005, Canonical Ltd, and Rosetta Contributors
           2010-2011, Serguei S. Dukachev <iam@dukess.ru>
License: GPL-2+

Files: po/sympa/sv.po
Copyright: 2005, Canonical Ltd, and Rosetta Contributors
           2010-2011, Pontus Freyhult <pontus_sympa_pootle@soua.net>
License: GPL-2+

Files: po/sympa/tr.po
Copyright: 2005, Canonical Ltd, and Rosetta Contributors
           2008, Elfun K. <elfun@riseup.net>
License: GPL-2+

Files: po/sympa/vi.po
Copyright: 2010, Clytie Siddall <clytie@riverland.net.au>
           2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/sympa/zh_CN.po
Copyright: 2012, Zhao Yongming <zym@3721.com>
           2012, Hongquan <lhq.mickey@gmail.com>
License: GPL-2+

Files: po/sympa/zh_TW.po
Copyright: 2006, Xun Yan
           2012, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/web_help/br.po
Copyright: 2007, Tristan Loarer <tristanloarer@hotmail.com>
License: GPL-2+

Files: po/web_help/ca.po
Copyright: ana.ribas <ana.ribas@upcnet.es>
License: GPL-2+

Files: po/web_help/de.po
Copyright: 2016, Daniel <d.stoye@cms.hu-berlin.de>
License: GPL-2+

Files: po/web_help/el.po
Copyright: 2013, kopolyzo <kopolyzo@ccf.auth.gr>
License: GPL-2+

Files: po/web_help/es.po
Copyright: 2016, Antonio Casado <acasado@ual.es>
License: GPL-2+

Files: po/web_help/et.po
Copyright: 2016, Urmas Buhvestov <urmas@tamme.tartu.ee>
License: GPL-2+

Files: po/web_help/eu.po
Copyright: 2015, shagi <shagi@gisa-elkartea.org>
License: GPL-2+

Files: po/web_help/fi.po
Copyright: 2012, Jyri-Petteri <jyri-petteri.paloposki@iki.fi>
License: GPL-2+

Files: po/web_help/id.po
Copyright: 2007, Ferry Yanuar <ferry@cbn.net>
License: GPL-2+

Files: po/web_help/ja.po
Copyright: 2016, IKEDA Soji <hatuka@nezumi.nu>
License: GPL-2+

Files: po/web_help/nl.po
Copyright: 2012, ivoverdonck <ivoverdonck@gmail.com>
License: GPL-2+

Files: po/web_help/oc.po
Copyright: 2013, Cedric31 <cvalmary@yahoo.fr>
           2013, occitan <asso.oc@abuledu.org>
License: GPL-2+

Files: po/web_help/pt_BR.po
Copyright: 2014, rmanola <rmanola@gmail.com>
License: GPL-2+

Files: po/web_help/ru.po
Copyright: 2011, Serguei S. Dukachev <iam@dukess.ru>
License: GPL-2+

Files: po/web_help/sv.po
Copyright: 2011, Mattias <mattiasa@avm.se>
License: GPL-2+

Files: debian/*
Copyright: 1998-2000, Raphael Hertzog <hertzog@debian.org>
           2000-2002, Jerome Marant <jerome@debian.org>
           2003-2008, 2010, Stefan Hornburg (Racke) <racke@linuxia.de>
           2008-2010, Jonas Smedegaard <dr@jones.dk>
           2010-2016, Emmanuel Bouthenot <kolter@debian.org>
License: GPL-2+

Files: debian/doc/README.Debian-exim4
Copyright: 2011, Olivier Berger <obergix@debian.org>
License: GPL-2+

Files: debian/po/cs.po
Copyright: 2010, Miroslav Kure <kurem@debian.cz>
License: GPL-2+

Files: debian/po/da.po
Copyright: 2010, Joe Hansen <joedalton2@yahoo.dk>
License: GPL-2+

Files: debian/po/de.po
Copyright: 2003, Stefan Hornburg (Racke) <racke@linuxia.de>
           2007-2008, 2010, Helge Kreutzmann <debian@helgefjell.de>
License: GPL-2+

Files: debian/po/es.po
Copyright: 2010, Software in the Public Interest
License: GPL-2+

Files: debian/po/fr.po
Copyright: 2001-2010, Christian Perrier <bubulle@debian.org>
License: GPL-2+

Files: debian/po/gl.po
Copyright: 2007-2008, Jacobo Tarrio <jtarrio@debian.org>
License: GPL-2+

Files: debian/po/ja.po
Copyright: 2009-2010, Hideki Yamane <henrich@debian.org>
License: GPL-2+

Files: debian/po/nl.po
Copyright: 2007, Bart Cornelis <cobaco@skolelinux.no>
License: GPL-2+

Files: debian/po/pt_BR.po
Copyright: 2004, André Luís Lopes <andrelop@debian.org>
           2007, Felipe Augusto van de Wiel (faw) <faw@debian.org>
           2008-2011, Eder L. Marques <eder@edermarques.net>
License: GPL-2+

Files: debian/po/pt.po
Copyright: 2007-2010, Miguel Figueiredo <elmig@debianpt.org>
License: GPL-2+

Files: debian/po/ru.po
Copyright: 2007, Yuriy Talakan' <yt@drsk.ru>
           2008, 2010, Yuri Kozlov <yuray@komyakino.ru>
License: GPL-2+

Files: debian/po/sv.po
Copyright: 2010, Martin Bagge <brother@bsnet.se>
License: GPL-2+

Files: www/fonts/font-awesome/*
Copyright: 2012-2018, Dave Gandy - http://fontawesome.io
License: MIT

Files: www/js/foundation/*
Copyright: 2013-2018 ZURB, inc.
License: MIT

Files: www/js/respondjs/respond.min.js
Copyright: 2011-2015, Scott Jehl <scottjehl@gmail.com>
License: MIT

Files: www/js/jqplot/*
Copyright: 2009-2015 Chris Leonello
License: MIT

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU General Public
 License (GPL) version 2 or later can be found in
 '/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Word English Bible
Upstream-Contact: SWORD developers <sword-devel@crosswire.org>
Upstream-Source: http://www.crosswire.org

Files: *
Copyright: World English Bible contributors 1997-2015
License: public-domain
 The World English Bible is in the Public Domain. That means that it is not copyrighted.
 .
 However, "World English Bible" is a Trademark of eBible.org. You may copy, publish, proclaim, 
 distribute, redistribute, sell, give away, quote, memorize, read publicly, broadcast, transmit, share, back up, post 
 on the Internet, print, reproduce, preach, teach from, and use the World English Bible as much as you want, and 
 others may also do so. All we ask is that if you CHANGE the actual text of the World English Bible in any way, you 
 not call the result the World English Bible any more. This is to avoid confusion, not to limit your freedom. The Holy 
 Bible is God's Word. It belongs to God. He gave it to us freely, and we who have worked on this translation freely 
 give it to you by dedicating it to the Public Domain.

Files: debian/*
Copyright: Copyright 2015, Crosswire Packaging Team <pkg-crosswire-devel@lists.alioth.debian.org>
           Copyright 2000-2007, Daniel Glassey <wdg@debian.org>
License: GPL-2+

License: GPL-2+
 On Debian systems the full text of the GNU General Public License can be found
 in the `/usr/share/common-licenses/GPL-2' file.

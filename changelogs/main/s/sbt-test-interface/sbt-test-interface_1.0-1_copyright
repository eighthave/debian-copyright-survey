Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: test-interface
Source: https://github.com/sbt/test-interface

Files: *
Copyright: 2009-2010 Josh Cough
           2009-2010 Mark Harrah
License: BSD-3-clause

Files: debian/*
Copyright: 2017 Frederic Bonnard <frediz@linux.vnet.ibm.com>
License: BSD-3-clause

Files: bootstrapdeps/com.jsuereth/gpg-library_2.10/jars/gpg-library_2.10-0.8.jar
       bootstrapdeps/com.jsuereth/gpg-library_2.10/ivy-0.8.xml
Copyright: Josh Suereth
License: BSD-2-clause

Files: bootstrapdeps/commons-codec/commons-codec/jars/commons-codec-1.4.jar
       bootstrapdeps/commons-codec/commons-codec/ivy-1.4.xml
Copyright: Henri Yandell
           Tim OBrien
           Scott Sanders
           Rodney Waldhoff
           Daniel Rall
           Jon S. Stevens
           Gary D. Gregory
           David Graham
License: Apache-2.0

Files: bootstrapdeps/commons-logging/commons-logging/jars/commons-logging-1.1.1.jar
       bootstrapdeps/commons-logging/commons-logging/ivy-1.1.1.xml
Copyright: Morgan Delagrange
           Rodney Waldhoff
           Craig McClanahan
           Scott Sanders
           Robert Burrell Donkin
           Peter Donald
           Costin Manolache
           Richard Sitze
           Juozas Baliuka
           Simon Kitching
           Dennis Lundberg
           Brian Stansberry
License: Apache-2.0

Files: bootstrapdeps/net.databinder/dispatch-core_2.9.1/jars/dispatch-core_2.9.1-0.8.6.jar
       bootstrapdeps/net.databinder/dispatch-core_2.9.1/ivy-0.8.6.xml
       bootstrapdeps/net.databinder/dispatch-futures_2.9.1/jars/dispatch-futures_2.9.1-0.8.6.jar
       bootstrapdeps/net.databinder/dispatch-futures_2.9.1/ivy-0.8.6.xml
       bootstrapdeps/net.databinder/dispatch-http_2.9.1/jars/dispatch-http_2.9.1-0.8.6.jar
       bootstrapdeps/net.databinder/dispatch-http_2.9.1/ivy-0.8.6.xml
Copyright: Nathan Hamblen
License: LGPL-2.1

Files: bootstrapdeps/org.apache.httpcomponents/httpclient/jars/httpclient-4.1.2.jar
       bootstrapdeps/org.apache.httpcomponents/httpclient/ivy-4.1.2.xml
       bootstrapdeps/org.apache.httpcomponents/httpcore/jars/httpcore-4.1.2.jar
       bootstrapdeps/org.apache.httpcomponents/httpcore/ivy-4.1.2.xml
Copyright: Ortwin Glueck
           Oleg Kalnichevski
           Asankha C. Perera
           Sebastian Bazley
           Erik Abele
           Ant Elder
           Paul Fremantle
           Roland Weber
           Sam Berlin
           Sean C. Sullivan
License: Apache-2.0

Files: bootstrapdeps/org.bouncycastle/bcpg-jdk16/jars/bcpg-jdk16-1.46.jar
       bootstrapdeps/org.bouncycastle/bcpg-jdk16/ivy-1.46.xml
       bootstrapdeps/org.bouncycastle/bcprov-jdk16/jars/bcprov-jdk16-1.46.jar
       bootstrapdeps/org.bouncycastle/bcprov-jdk16/ivy-1.46.xml
Copyright: The Legion of the Bouncy Castle Inc.
License: Expat

Files: bootstrapdeps/scala_2.10/sbt_0.13/com.typesafe.sbt/sbt-pgp/jars/sbt-pgp-0.8.jar
       bootstrapdeps/scala_2.10/sbt_0.13/com.typesafe.sbt/sbt-pgp/ivy-0.8.xml
Copyright: Josh Suereth
License: BSD-3-clause

Files: bootstrapdeps/com.jcraft/jsch/jars/jsch-0.1.46.jar
       bootstrapdeps/com.jcraft/jsch/ivy-0.1.46.xml
Copyright: Atsuhiko Yamanaka
License: BSD-3-clause

Files: bootstrapdeps/com.thoughtworks.paranamer/paranamer/jars/paranamer-2.6.jar
       bootstrapdeps/com.thoughtworks.paranamer/paranamer/ivy-2.6.xml
Copyright: Paul Hammant
           Mauro Talevi
           Guilherme Silveira
License: BSD-3-clause

Files: bootstrapdeps/jline/jline/jars/jline-2.13.jar
       bootstrapdeps/jline/jline/ivy-2.13.xml
       bootstrapdeps/jline/jline/jars/jline-2.11.jar
       bootstrapdeps/jline/jline/ivy-2.11.xml
Copyright: Jason Dillon
           Guillaume Nodet
           Marc Prud'hommeaux
License: BSD-2-clause

Files: bootstrapdeps/org.fusesource.jansi/jansi/jars/jansi-1.11.jar
       bootstrapdeps/org.fusesource.jansi/jansi/ivy-1.11.xml
       bootstrapdeps/org.fusesource.jansi/jansi/jars/jansi-1.4.jar
       bootstrapdeps/org.fusesource.jansi/jansi/ivy-1.4.xml
Copyright: Hiram Chirino
License: Apache-2.0

Files: bootstrapdeps/org.json4s/json4s-ast_2.10/jars/json4s-ast_2.10-3.2.10.jar
       bootstrapdeps/org.json4s/json4s-ast_2.10/ivy-3.2.10.xml
       bootstrapdeps/org.json4s/json4s-core_2.10/jars/json4s-core_2.10-3.2.10.jar
       bootstrapdeps/org.json4s/json4s-core_2.10/ivy-3.2.10.xml
Copyright: Ivan Porto Carrero
License: Apache-2.0

Files: bootstrapdeps/org.scala-lang.modules/scala-parser-combinators_2.11/bundles/scala-parser-combinators_2.11-1.0.1.jar
       bootstrapdeps/org.scala-lang.modules/scala-parser-combinators_2.11/ivy-1.0.1.xml
       bootstrapdeps/org.scala-lang.modules/scala-xml_2.11/bundles/scala-xml_2.11-1.0.1.jar
       bootstrapdeps/org.scala-lang.modules/scala-xml_2.11/ivy-1.0.1.xml
Copyright: EPFL
           Typesafe, Inc.
License: BSD-3-clause

Files: bootstrapdeps/org.scala-lang.modules/scala-pickling_2.10/jars/scala-pickling_2.10-0.10.1.jar
       bootstrapdeps/org.scala-lang.modules/scala-pickling_2.10/ivy-0.10.1.xml
Copyright: Eugene Burmako
           Heather Miller
           Philipp Haller
           Havoc Pennington
           Eugene Yokota
           Josh Suereth
License: BSD-3-clause

Files: bootstrapdeps/org.scala-lang/jline/jars/jline-2.10.6.jar
       bootstrapdeps/org.scala-lang/jline/ivy-2.10.6.xml
Copyright: EPFL LAMP
           Typesafe, Inc.
License: BSD-2-clause

Files: bootstrapdeps/org.scala-lang/scala-compiler/jars/scala-compiler-2.11.0.jar
       bootstrapdeps/org.scala-lang/scala-compiler/ivy-2.11.0.xml
       bootstrapdeps/org.scala-lang/scala-library/jars/scala-library-2.11.0.jar
       bootstrapdeps/org.scala-lang/scala-library/ivy-2.11.0.xml
       bootstrapdeps/org.scala-lang/scala-reflect/jars/scala-reflect-2.11.0.jar
       bootstrapdeps/org.scala-lang/scala-reflect/ivy-2.11.0.xml
       bootstrapdeps/org.scala-lang/scala-compiler/jars/scala-compiler-2.10.6.jar
       bootstrapdeps/org.scala-lang/scala-compiler/ivy-2.10.6.xml
       bootstrapdeps/org.scala-lang/scala-library/jars/scala-library-2.10.6.jar
       bootstrapdeps/org.scala-lang/scala-library/ivy-2.10.6.xml
       bootstrapdeps/org.scala-lang/scala-reflect/jars/scala-reflect-2.10.6.jar
       bootstrapdeps/org.scala-lang/scala-reflect/ivy-2.10.6.xml
Copyright: EPFL LAMP
           Typesafe, Inc.
License: BSD-3-clause

Files: bootstrapdeps/org.scala-sbt.ivy/ivy/jars/ivy-2.3.0-sbt-2cc8d2761242b072cedb0a04cb39435c4fa24f9a.jar
       bootstrapdeps/org.scala-sbt.ivy/ivy/ivy-2.3.0-sbt-2cc8d2761242b072cedb0a04cb39435c4fa24f9a.xml
Copyright: Eugene Yokota
License: Apache-2.0

Files: bootstrapdeps/org.scala-sbt/actions/jars/actions-0.13.12.jar
       bootstrapdeps/org.scala-sbt/actions/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/api/jars/api-0.13.12.jar
       bootstrapdeps/org.scala-sbt/api/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/apply-macro/jars/apply-macro-0.13.12.jar
       bootstrapdeps/org.scala-sbt/apply-macro/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/cache/jars/cache-0.13.12.jar
       bootstrapdeps/org.scala-sbt/cache/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/classfile/jars/classfile-0.13.12.jar
       bootstrapdeps/org.scala-sbt/classfile/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/classpath/jars/classpath-0.13.12.jar
       bootstrapdeps/org.scala-sbt/classpath/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/collections/jars/collections-0.13.12.jar
       bootstrapdeps/org.scala-sbt/collections/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/command/jars/command-0.13.12.jar
       bootstrapdeps/org.scala-sbt/command/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/compile/jars/compile-0.13.12.jar
       bootstrapdeps/org.scala-sbt/compile/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/compiler-integration/jars/compiler-integration-0.13.12.jar
       bootstrapdeps/org.scala-sbt/compiler-integration/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/compiler-interface/jars/compiler-interface-0.13.12.jar
       bootstrapdeps/org.scala-sbt/compiler-interface/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/compiler-ivy-integration/jars/compiler-ivy-integration-0.13.12.jar
       bootstrapdeps/org.scala-sbt/compiler-ivy-integration/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/completion/jars/completion-0.13.12.jar
       bootstrapdeps/org.scala-sbt/completion/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/control/jars/control-0.13.12.jar
       bootstrapdeps/org.scala-sbt/control/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/cross/jars/cross-0.13.12.jar
       bootstrapdeps/org.scala-sbt/cross/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/incremental-compiler/jars/incremental-compiler-0.13.12.jar
       bootstrapdeps/org.scala-sbt/incremental-compiler/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/interface/jars/interface-0.13.12.jar
       bootstrapdeps/org.scala-sbt/interface/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/io/jars/io-0.13.12.jar
       bootstrapdeps/org.scala-sbt/io/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/ivy/jars/ivy-0.13.12.jar
       bootstrapdeps/org.scala-sbt/ivy/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/logging/jars/logging-0.13.12.jar
       bootstrapdeps/org.scala-sbt/logging/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/logic/jars/logic-0.13.12.jar
       bootstrapdeps/org.scala-sbt/logic/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/main-settings/jars/main-settings-0.13.12.jar
       bootstrapdeps/org.scala-sbt/main-settings/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/main/jars/main-0.13.12.jar
       bootstrapdeps/org.scala-sbt/main/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/persist/jars/persist-0.13.12.jar
       bootstrapdeps/org.scala-sbt/persist/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/process/jars/process-0.13.12.jar
       bootstrapdeps/org.scala-sbt/process/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/relation/jars/relation-0.13.12.jar
       bootstrapdeps/org.scala-sbt/relation/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/run/jars/run-0.13.12.jar
       bootstrapdeps/org.scala-sbt/run/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/sbt/jars/sbt-0.13.12.jar
       bootstrapdeps/org.scala-sbt/sbt/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/task-system/jars/task-system-0.13.12.jar
       bootstrapdeps/org.scala-sbt/task-system/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/tasks/jars/tasks-0.13.12.jar
       bootstrapdeps/org.scala-sbt/tasks/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/test-agent/jars/test-agent-0.13.12.jar
       bootstrapdeps/org.scala-sbt/test-agent/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/testing/jars/testing-0.13.12.jar
       bootstrapdeps/org.scala-sbt/testing/ivy-0.13.12.xml
       bootstrapdeps/org.scala-sbt/tracking/jars/tracking-0.13.12.jar
       bootstrapdeps/org.scala-sbt/tracking/ivy-0.13.12.xml
Copyright: Josh Suereth
           Eugene Yokota
License: BSD-3-clause

Files: bootstrapdeps/org.scala-tools.sbinary/sbinary_2.10/jars/sbinary_2.10-0.4.2.jar
       bootstrapdeps/org.scala-tools.sbinary/sbinary_2.10/ivy-0.4.2.xml
Copyright: David R. MacIver
License: Expat

Files: bootstrapdeps/org.scala-sbt/launcher-interface/jars/launcher-interface-1.0.0-M1.jar
       bootstrapdeps/org.scala-sbt/launcher-interface/ivy-1.0.0-M1.xml
Copyright: Josh Suereth
           Eugene Yokota
License: BSD-3-clause

Files: bootstrapdeps/org.scala-sbt/serialization_2.10/jars/serialization_2.10-0.1.2.jar
       bootstrapdeps/org.scala-sbt/serialization_2.10/ivy-0.1.2.xml
Copyright: Havoc Pennington
           Eugene Yokota
           Josh Suereth
License: Apache-2.0

Files: bootstrapdeps/org.scala-sbt/test-interface/jars/test-interface-1.0.jar
       bootstrapdeps/org.scala-sbt/test-interface/ivy-1.0.xml
Copyright: Mark Harrah
           Josh Cough
           Bill Venners
           Chua Chee Seng
License: BSD-3-clause

Files: bootstrapdeps/org.scalamacros/quasiquotes_2.10/jars/quasiquotes_2.10-2.0.1.jar
       bootstrapdeps/org.scalamacros/quasiquotes_2.10/ivy-2.0.1.xml
Copyright: Eugene Burmako
License: BSD-3-clause

Files: bootstrapdeps/org.scalatest/scalatest_2.11/jars/scalatest_2.11-2.1.3.jar
       bootstrapdeps/org.scalatest/scalatest_2.11/ivy-2.1.3.xml
Copyright: Bill Venners
License: Apache-2.0

Files: bootstrapdeps/org.spire-math/jawn-parser_2.10/jars/jawn-parser_2.10-0.6.0.jar
       bootstrapdeps/org.spire-math/jawn-parser_2.10/ivy-0.6.0.xml
       bootstrapdeps/org.spire-math/json4s-support_2.10/jars/json4s-support_2.10-0.6.0.jar
       bootstrapdeps/org.spire-math/json4s-support_2.10/ivy-0.6.0.xml
Copyright: Erik Osheim
License: Expat

License: BSD-2-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: LGPL-2.1
 On Debian systems, the full text of the LGPL-2.1 license
 can be found in the file '/usr/share/common-licenses/LGPL-2.1'

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

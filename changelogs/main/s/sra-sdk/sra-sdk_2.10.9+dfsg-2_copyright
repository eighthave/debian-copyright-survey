Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/ncbi/sra-tools/
Files-Excluded: */libs/ext
                */MSVC
                */Makefile.mac
                */Makefile.sun*
                */Makefile.vc++
                */Makefile.win
                */sun*.sh
                */ld.mac*
                */ld.win*
                */win-cc.sh
Upstream-Contact: sra-tools@ncbi.nlm.nih.gov

Files: *
Copyright: none claimed
License: public-domain
                           PUBLIC DOMAIN NOTICE
              National Center for Biotechnology Information
 .
 This software/database is a "United States Government Work" under the
 terms of the United States Copyright Act.  It was written as part of
 the author's official duties as a United States Government employee and
 thus cannot be copyrighted.  This software/database is freely available
 to the public for use. The National Library of Medicine and the U.S.
 Government have not placed any restriction on its use or reproduction.
 .
 Although all reasonable efforts have been taken to ensure the accuracy
 and reliability of the software and data, the NLM and the U.S.
 Government do not and cannot warrant the performance or results that
 may be obtained by using this software or data. The NLM and the U.S.
 Government disclaim all warranties, express or implied, including
 warranties of performance, merchantability or fitness for any particular
 purpose.
 .
 Please cite the author in any work or product based on this material.

Files: build/gprof2dot.py
Copyright: 2008-2009 Jose Fonseca
License: LGPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU Lesser General
 Public License at /usr/share/common-licenses/LGPL-3.

Files: debian/tests/test-data/test-data.tar.gz
Copyright: 1996-2018 NCBI
License: unrestricted
 Databases of molecular data on the NCBI Web site include such examples as
 nucleotide sequences (GenBank), protein sequences, macromolecular
 structures, molecular variation, gene expression, and mapping data. They
 are designed to provide and encourage access within the scientific
 community to sources of current and comprehensive information. Therefore,
 NCBI itself places no restrictions on the use or distribution of the data
 contained therein. Nor do we accept data when the submitter has requested
 restrictions on reuse or redistribution.
Comment:
 The test data files contained in the archive are a data set prepared from
 original SRAs with accession numbers SRR6650398, SRR2042184 and SRR7032226.

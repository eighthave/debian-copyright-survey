Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Starjava DPAC
Upstream-Contact: Mark Taylor <m.b.taylor@bristol.ac.uk>
Source: https://github.com/Starlink/starjava/tree/master/dpac

Files: *
Copyright: 2005-2018 Central Laboratory of the Research Councils
 2018 Mark Taylor
 2018 Ole Streicher <olebole@debian.org> (Debian files)
License: LGPL-2.1+
Comment: The license information is stored in the root of the upstream
 git repository.
 .
 This license is hereby asserted to apply to all the original
 (non-third-party) code and associated files in the packages
 from this starjava repository, specifically:
 .
   array      diva  jaiutil   ndx         splat     ttools
   astgui     fits  jniast    pal         srb       util
   astrogrid  frog  jnihds    plastic     startask  vo
   cdf        gbin  jnikappa  registry    table     votable
   coco       hds   jpcs      rv          task      xdoc
   connect    hdx   mirage    soapserver  topcat
   datanode   help  ndtools   sog         treeview
 .
 In particular this license applies to all the java classes in
 or under the uk.ac.starlink namespace.
 .
 If LGPL licensing is not acceptable for particular usage requirements,
 it may be possible to provide the code under other licensing arrangements.

Files: src/main/gaia/* src/main/uk/ac/starlink/dpac/epoch/*
Copyright:  2006-2018 Gaia Data Processing and Analysis Consortium
 2018 Mark Taylor
License: LGPL-2.1+
Comment: The code was copied from the DPAC subversion repository at
 https://gaia.esac.esa.int/dpacsvn and slightly modified by Mark Taylor
 to remove external dependencies.

Files: src/main/uk/ac/starlink/dpac/math/PolynomialSplineFunction.java
 src/main/uk/ac/starlink/dpac/math/SplineInterpolator.java
 src/main/uk/ac/starlink/dpac/math/PolynomialFunction.java
Copyright: 2001-2011 The Apache Software Foundation
License: Apache-2.0
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems you can find the complete text of the
 Apache-2.0 license in `/usr/share/common-licenses/Apache-2.0'.

License: LGPL-2.1+
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Symmetrica
Upstream-Contact: Lehrstuhl Mathematik II <symmetrica@symmetrica.de>
Source: http://www.algorithm.uni-bayreuth.de/en/research/SYMMETRICA/
Comment:
 The upstream source tarball is repacked mainly to contain the source material
 into a folder and eventually to substantially reduce size.

Files: *
Copyright:
 1987-2018 Lehrstuhl Mathematik II <symmetrica@symmetrica.de>
License: ISC
Comment:
 This package was originally set under a faulty license:
 confirmation was asked to the copyright holder.

Files: debian/*
Copyright:
 2014-2018 Jerome Benoit <calculus@rezozer.net>
 2008 Tim Abbott <tabbott@mit.edu>
License: ISC
Comment:
 This package was originally debianized by Tim Abbott <tabbott@mit.edu>
 under the same but then faulty upstream license: Debian material is now set
 under ISC licence which is the effective license of the upstream material.
 Thanks also to Dominique Ingoglia <Dominique.Ingoglia@gmail.com>,
 Lifeng Sun <lifongsun@gmail.com>, and Tobias Hansen <tobias.han@gmx.de>
 who had contributed to the Debian material as deposited at Alioth.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for
 any purpose with or without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sisu Guice
Source: https://github.com/sonatype/sisu-guice/
Comment:
 This project incorporates classes from the libasm4-java and libcglib3-java
 packages (versions listed in the Built-Using field in debian/control).
 under a different package to avoid classpath conflicts. 

Files: *
Copyright: 2006-2011, Google, Inc.
License: Apache-2.0

Files: src/proxy/net/sf/cglib/*
Copyright: 2002-2004 The Apache Software Foundation
License: Apache-2.0
Comment:
 Class files incorporated in sisu-guice via jarjar.
 The filespec is relative to the cglib source package.
 See the cglib3 source package for more information.

Files: src/org/objectweb/asm/*
Copyright: 2004-2008 Eric Bruneton, Eugene Kuleshov
Comment:
 Class files incorporated in sisu-guice via jarjar.
 The filespec is relative to the libasm4-java source package.
 See the libasm4-java source package for more information.
License: BSD-3-clause
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the copyright holders nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2011, Damien Raude-Morvan <drazzib@debian.org>
License: Apache-2.0

License: Apache-2.0
  On Debian GNU/Linux system you can find the complete text of the
  Apache-2.0 license in '/usr/share/common-licenses/Apache-2.0'


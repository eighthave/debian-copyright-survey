Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: StumpWM
Upstream-Contact: stumpwm-devel@nongnu.org
Source: https://github.com/stumpwm/stumpwm

Files: *
Copyright: 2003-2008 Shawn Betts <sabetts@gmail.com>
License: GPL-2+

Files: color.lisp
Copyright: 2007-2008 Jonathan Moore Liles <wantingwaiting@users.sourceforge.net>
           2014 Joram Schrijver
License: GPL-2+

Files: fdump.lisp
Copyright: 2007-2008 Shawn Betts <sabetts@gmail.com>
           2007-2008 Jonathan Moore Liles <wantingwaiting@users.sourceforge.net>
License: GPL-2+

Files: keysyms.lisp keytrans.lisp
Copyright: 2006-2008 Matthew Kennedy <mkennedy@gentoo.org>
License: GPL-2+

Files: module.lisp
Copyright: 2014 David Bjergaard
License: GPL-2+

Files: pathnames.lisp
Copyright: 2004 Peter Seibel
           2004-2009, Dr. Edmund Weitz
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials
     provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHORS 'AS IS' AND ANY EXPRESSED
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: stumpwm.texi.in
Copyright: 2000-2008 Shawn Betts <sabetts@gmail.com>
           2014 David Bjergaard
License: copyleft
 Permission is granted to make and distribute verbatim
 copies of this manual provided the copyright notice and
 this permission notice are preserved on all copies.
 .
 Permission is granted to copy and distribute modified
 versions of this manual under the conditions for
 verbatim copying, provided also that the sections
 entitled ``Copying'' and ``GNU General Public License''
 are included exactly as in the original, and provided
 that the entire resulting derived work is distributed
 under the terms of a permission notice identical to this
 one.
 .
 Permission is granted to copy and distribute
 translations of this manual into another language,
 under the above conditions for modified versions,
 except that this permission notice may be stated in a
 translation approved by the Free Software Foundation.

Files: time.lisp
Copyright: 2003-2008 Ivy Foster
License: GPL-2+

Files: version.lisp
Copyright: 2006-2008 Martin Bishop <martinbishop@bellsouth.net>
           2006-2008 Ivy Foster
License: GPL-2+

Files: debian/*
Copyright: 2005-2006 Trent Buck <trentbuck@gmail.com>
           2006-2010 Luca Capello <luca@pca.it>
           2010-2011 Desmond O. Chang <dochang@gmail.com>
           2014-2015 Milan Zamazal <pdm@debian.org>
License: GPL-2+

License: GPL-2+
 stumpwm is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 stumpwm is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this software; see the file COPYING.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License can
 be found in the `/usr/share/common-licenses/GPL' file.

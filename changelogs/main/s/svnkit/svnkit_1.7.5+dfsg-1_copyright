Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SVNKit
Upstream-Contact: TMate Software Ltd. <support@svnkit.com>
Source: http://svnkit.com/download.php

Files: *
Copyright: 2004-2011, TMate Software Ltd. <support@svnkit.com>
License: The TMate Open Source License

Files: debian/*
Copyright: 2010-2011, Miguel Landaeta <miguel@miguel.cc>
           2012, Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
License: Apache-2.0

Files: debian/build.xml
Copyright: 2004-2011, TMate Software Ltd. <support@svnkit.com>
           2012, Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
License: The TMate Open Source License

License: The TMate Open Source License
 The TMate Open Source License.
 .
 This license applies to all portions of TMate SVNKit library, which 
 are not externally-maintained libraries (e.g. Ganymed SSH library).
 .
 All the source code and compiled classes in package org.tigris.subversion.javahl
 except SvnClient class are covered by the license in JAVAHL-LICENSE file
 .
 Copyright (c) 2004-2011 TMate Software. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification, 
 are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice, 
       this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
 .
     * Redistributions in any form must be accompanied by information on how to 
       obtain complete source code for the software that uses SVNKit and any 
       accompanying software that uses the software that uses SVNKit. The source 
       code must either be included in the distribution or be available for no 
       more than the cost of distribution plus a nominal fee, and must be freely 
       redistributable under reasonable conditions. For an executable file, complete 
       source code means the source code for all modules it contains. It does not 
       include source code for modules or files that typically accompany the major 
       components of the operating system on which the executable file runs.
 .
     * Redistribution in any form without redistributing source code for software 
       that uses SVNKit is possible only when such redistribution is explicitly permitted 
       by TMate Software. Please, contact TMate Software at support@svnkit.com to 
       get such permission.
 .
 THIS SOFTWARE IS PROVIDED BY TMATE SOFTWARE ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE 
 DISCLAIMED. 
 .
 IN NO EVENT SHALL TMATE SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, 
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
     /usr/share/common-licenses/Apache-2.0 (on Debian systems)
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.


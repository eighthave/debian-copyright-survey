Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: the Staden Package
Upstream-Contact: http://sourceforge.net/p/staden/bugs/
                  Andrew Whitwham <aw7@sanger.ac.uk>
                  James Bonfield <jkb@sanger.ac.uk>
Source: http://sourceforge.net/projects/staden/files/

Files: *
Copyright: 1995-2001 Medical Research Council, Laboratory of Molecular Biology
           2005-2013, James Bonfield
           2005-2013, Andrew Whitwham
	   Rodger Staden
           Kathryn Beal
           Mark Jordan
           Yaping Cheng
           Simon Dear
           Matthew Betts
License: BSD-like

Files: primer3/*
Copyright: 1996,1997,1998 Whitehead Institute for Biomedical Research. All rights reserved.
           2001 James Bonfield
License: BSD-primer3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1.      Redistributions must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the  documentation
 and/or other materials provided with the distribution.  Redistributions of
 source code must also reproduce this information in the source code itself.
 .
 2.      If the program is modified, redistributions must include a notice
 (in the same places as above) indicating that the redistributed program is
 not identical to the version distributed by Whitehead Institute.
 .
 3.      All advertising materials mentioning features or use of this
 software  must display the following acknowledgment:
         This product includes software developed by the
         Whitehead Institute for Biomedical Research.
 .
 4.      The name of the Whitehead Institute may not be used to endorse or
 promote products derived from this software without specific prior written
 permission.
 .
 We also request that use of this software be cited in publications as
 .
 Steve Rozen, Helen J. Skaletsky (1996,1997,1998)
    Primer3. Code available at
    http://www-genome.wi.mit.edu/genome_software/other/primer3.html
 .
 THIS SOFTWARE IS PROVIDED BY THE WHITEHEAD INSTITUTE ``AS IS'' AND  ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  ARE
 DISCLAIMED. IN NO EVENT SHALL THE WHITEHEAD INSTITUTE BE LIABLE  FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL  DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS  OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)  HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: tk_utils/tclAppInit.c
Copyright: 1993 The Regents of the University of California.
           1994-1997 Sun Microsystems, Inc.
           1998-1999 by Scriptics Corporation.
License: UC_SUN_Scriptics

Files: tk_utils/tk_defs.h
Copyright: 1991-1994 The Regents of the University of California.
           1994-1995 Sun Microsystems, Inc
License: UC_SUN_Scriptics

Files: tracediff/*
Copyright: 2001 Medical Research Council
License: permission_granted
 Permission to use, copy, modify and distribute this software and its
 documentation for any purpose is hereby granted without fee, provided that
 this copyright and notice appears in all copies.
 .
 This file was written as part of the Staden Package at the MRC Laboratory
 of Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.
 .
 MRC disclaims all warranties with regard to this software.

Files: pregap4/widget.tcl
Copyright: 1997-1998 Jeffrey Hobbs <jeff.hobbs@acm.org>
License: TclTk
 This text was blatantly stolen from Tcl/Tk license and adapted - thus
 assume it falls under similar license terms.
 .
 The authors hereby grant permission to use, copy, modify, distribute, and
 license this software and its documentation for any purpose, provided that
 existing copyright notices are retained in all copies and that this notice
 is included verbatim in any distributions.  No written agreement, license,
 or royalty fee is required for any of the authorized uses.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF,
 EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE IS
 PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO
 OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 RESTRICTED RIGHTS: Use, duplication or disclosure by the U.S. government
 is subject to the restrictions as set forth in subparagraph (c) (1) (ii)
 of the Rights in Technical Data and Computer Software Clause as DFARS
 252.227-7013 and FAR 52.227-19.
 .
 SPECIAL NOTES:
 .
 This software is also falls under the bourbon_ware clause:
 .
   Should you find this software useful in your daily work, you should
   feel obliged to take the author out for a drink if the opportunity
   presents itself.  The user may feel exempt from this clause if they are
   under 21 or think the author has already partaken of too many drinks.

Files: debian/*
Copyright: 2013 Tim Booth <tbooth@ceh.ac.uk>
           2013-2014 Andreas Tille <tille@debian.org>
License: BSD-like

License: BSD-like
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name of the MEDICAL RESEARCH COUNCIL, THE LABORATORY OF
 MOLECULAR BIOLOGY nor the names of its contributors may be used to endorse or
 promote products derived from this software without specific prior written
 permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: UC_SUN_Scriptics
 This software is copyrighted by the Regents of the University of
 California, Sun Microsystems, Inc., Scriptics Corporation, ActiveState
 Corporation and other parties.  The following terms apply to all files
 associated with the software unless explicitly disclaimed in
 individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 GOVERNMENT USE: If you are acquiring this software on behalf of the
 U.S. government, the Government shall have only "Restricted Rights"
 in the software and related documentation as defined in the Federal
 Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
 are acquiring the software on behalf of the Department of Defense, the
 software shall be classified as "Commercial Computer Software" and the
 Government shall have only "Restricted Rights" as defined in Clause
 252.227-7013 (b) (3) of DFARs.  Notwithstanding the foregoing, the
 authors grant the U.S. Government and others acting in its behalf
 permission to use and distribute the software in accordance with the
 terms specified in this license.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: singular
Upstream-Contact: Singular Team <singular@mathematik.uni-kl.de>
Source: https://www.singular.uni-kl.de/
Comment:
 The upstream source tarball is repacked to drop off the regenerated material
 (mainly autotools related) and material provided by other packages (mainly
 autoconf-archive m4 files) to gain weight.
 (At the time of writing, the latest ax_prefix_config_h.m4 as distributed
 within the autoconf-archive is not compatible with the version distributed
 within Singular (serial 11); more precisely, not providing a prefix is an
 error in the lastest version (serial 15). Second, ax_python_with_version.m4
 does not belong to autoconf-archive.)
Files-Excluded:
 make_tar.sh
 redhat
 README.md
 m4/lt~obsolete.m4
 m4/ltoptions.m4
 m4/ltsugar.m4
 m4/ltversion.m4
 m4/libtool.m4
 build-aux/ylwrap
 build-aux/depcomp
 build-aux/compile
 build-aux/ar-lib
 build-aux/install-sh
 build-aux/ltmain.sh
 build-aux/missing
 build-aux/config.sub
 build-aux/config.guess
 build-aux/test-driver
 */aclocal.m4
 */Makefile.in
 gfanlib/_config.h.in
 gfanlib/configure
 libpolys/_config.h.in
 libpolys/configure
 factory/acinclude.m4
 factory/_config.h.in
 factory/configure
 omalloc/_config.h.in
 omalloc/configure
 resources/_config.h.in
 resources/configure
 xalloc/_config.h.in
 xalloc/configure
 _config.h.in
 configure
 doc/Singular.man
 doc/TSingular.man
 doc/ESingular.man
 m4/ax_append_compile_flags.m4
 m4/ax_append_flag.m4
 m4/ax_append_link_flags.m4
 m4/ax_check_compile_flag.m4
 m4/ax_check_link_flag.m4
 m4/ax_compute_relative_paths.m4
 m4/ax_cxx_gcc_abi_demangle.m4
 m4/ax_lib_socket_nsl.m4
 m4/ax_normalize_path.m4
 m4/ax_prog_python_version.m4
 m4/ax_pthread.m4
 m4/ax_python_config_var.m4
 m4/ax_python_embed.m4

Files: *
Copyright:
 1986-2018 Singular Team <singular@mathematik.uni-kl.de>
   Wolfram Decker
   Gert-Martin Greuel
   Gerhard Pfister
   Hans Schoeneman
License: GPL-2 or GPL-3
Comment:
 For information on how to cite Singular see
 `http://www.singular.uni-kl.de/index.php/how-to-cite-singular'.

Files: factory/*
Copyright:
 1991-2015
   Gert-Martin Greuel
   Rudiger Stobbe
   Jens Schmidt
   Gerhard Pfister
   Hans Schonemann
License: GPL-2 or GPL-3
Comment:
 Code from libfac or derived from code from libfac is contained in
 the following files:
  factory/cfCharSets.cc
  factory/cfCharSets.h
  factory/cfCharSetsUtil.cc
  factory/cfCharSetsUtil.h
  factory/facAlgFunc.cc
  factory/facAlgFunc.h
  factory/facAlgFuncUtil.cc
  factory/facAlgFuncUtil.h
 libfac is a library for `Characteristic sets and
 factorization over algebraic function fields' written by
 Michael Messollen <mmessollen@web.de>, Copyright 1996-2010.
 .
 Slightly modified code from Number Theory Library (NTL) is implemented
 in the following files:
  factory/cfNTLzzpEXGCD.cc
  factory/cfNTLzzpEXGCD.h
 NTL is a library for `Doing Number Theory' written under a GPL-2+ license
 by Victor Shoup <victor@shoup.net>, Copyright 1996-2009; NTL is available
 at `http://www.shoup.net'.

Files: omalloc/*
Copyright:
 1999-2010 Olaf Bachmann
License: GPL-2+

Files:
 kernel/*
 libpolys/*
 emacs/*
 Singular/*
Copyright:
 1986-2015
   Gert-Martin Greuel
   Gerhard Pfister
   Hans Schoeneman
License: GPL-2 or GPL-3

Files:
 factory/bin/folding.el
Copyright:
 1992-1993 Jamie Lokier
License: GPL-2+

Files:
 factory/aminclude.am
Copyright:
 2004 Oren Ben-Kiki
License: GPL-2+

Files:
 omalloc/omReturn.h
Copyright:
 2000 Gray Watson
License: other
 This file is part of the dmalloc package.
 .
 Permission to use, copy, modify, and distribute this software for
 any purpose and without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies, and that the name of Gray Watson not be used in advertising
 or publicity pertaining to distribution of the document or software
 without specific, written prior permission.
 .
 Gray Watson makes no representations about the suitability of the
 software described herein for any purpose.  It is provided "as is"
 without express or implied warranty.
 .
 The author may be contacted via `http://dmalloc.com/'.

Files:
 Singular/links/ndbm.cc
 Singular/links/ndbm.h
Copyright:
 1983 Regents of the University of California
License: BSD-3-clause

Files:
 Singular/fegetopt.c
Copyright:
 1987-1994 Free Software Foundation, Inc.
License: GPL-2+

Files:
 Singular/fegetopt.h
Copyright:
 1989-1993 Free Software Foundation, Inc.
License: GPL-2+

Files:
 m4/ax_prefix_config_h.m4
Copyright:
 2008 Guido U. Draheim <guidod@gmx.de>
 2008 Marten Svantesson
 2008 Gerald Point <Gerald.Point@labri.fr>
License: GPL-3+

Files:
 m4/google-perftools.m4
Copyright:
 2005-2006 Intel Corporation
License: Apache-2.0

Files:
 m4/p-procs.m4
Copyright:
 2011 Bradford Hovinen <hovinen@gmail.com>
License: public-domain
 This file is public domain and comes with NO WARRANTY of any kind.

Files: debian/*
Copyright:
 2014-2018 Jerome Benoit <calculus@rezozer.net>
 2008-2009 Tim Abbott <tabbott@mit.edu>
License: GPL-3+
Comment:
 This package was originally debianized by Tim Abbott <tabbott@mit.edu>.
 Thanks also to both Bernhard R. Link <brlink@debian.org> and
 Felix Salfelder <felix@salfelder.org> who had maintained the Debian
 material as deposited at Alioth from 2012 to 2014 (UNRELEASED).

License: GPL-2 or GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 or version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2', and the
 complete text of the GNU General Public License version 3 can be found
 in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.

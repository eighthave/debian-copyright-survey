Format: http://dep.debian.net/deps/dep5
Upstream-Name: starpu
Source: http://starpu.gforge.inria.fr/

Files: src/common/uthash.h
Copyright: 2003-2010, Troy D. Hanson
License: BSD-uthash
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: acinclude.m4
Copyright: Free Software Foundation, Inc.
License: GPL-2

Files: gcc-plugin/*
Copyright: 2011 Institut National de Recherche en Informatique et Automatique
License: GPL-3

Files: starpu-top/qwt/*
Copyright: 1997 Josef Wilgen
           2002-2003,2008 Uwe Rathmann
License: QWT

Files: starpu-top/qledindicator/*
Copyright: 2010 by Tn <thenobody@poczta.fm>
License: GPL-3

Files: socl/include/CL/*
Copyright: 2008-2010 The Khronos Group Inc.
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.

Files: doc/doxygen/refman.tex
       doc/doxygen/chapters/*
Copyright: 2009-2013  Université de Bordeaux
           2010, 2011, 2012, 2013, 2014  CNRS
           2011  Télécom-SudParis
	   2011, 2012  INRIA
License: GFDL-NIV

Files: *
Copyright: 2008-2012 Université de Bordeaux 1
           2010-2012 Centre National de la Recherche Scientifique
           2010-2012 Institut National de Recherche en Informatique et Automatique
	   2011 Télécom-SudParis
	   2010 Mehdi Juhoor <mjuhoor@gmail.com>
	   2010 Sylvain Gault
	   2011 William Braik, Yann Courtois, Jean-Marie Couteyen, Anthony Roy
License: LGPL-2.1

Files: debian/*
Copyright: 2011-2012 Samuel Thibault <sthibault@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 GCC-StarPU is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 GCC-StarPU is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with GCC-StarPU.  If not, see <http://www.gnu.org/licenses/>.  */
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-3".

License: QWT
 This library is free software; you can redistribute it and/or
 modify it under the terms of the Qwt License, Version 1.0
 .
                             Qwt License
                           Version 1.0, January 1, 2003
 .
 The Qwt library and included programs are provided under the terms
 of the GNU LESSER GENERAL PUBLIC LICENSE (LGPL) with the following
 exceptions:
 .
    1. Widgets that are subclassed from Qwt widgets do not
       constitute a derivative work.
 .
    2. Static linking of applications and widgets to the
       Qwt library does not constitute a derivative work
       and does not require the author to provide source
       code for the application or widget, use the shared
       Qwt libraries, or link their applications or
       widgets against a user-supplied version of Qwt.
 .
       If you link the application or widget to a modified
       version of Qwt, then the changes to Qwt must be
       provided under the terms of the LGPL in sections
       1, 2, and 4.
 .
    3. You do not have to provide a copy of the Qwt license
       with programs that are linked to the Qwt library, nor
       do you have to identify the Qwt license in your
       program or documentation as required by section 6
       of the LGPL.
 .
 .
       However, programs must still identify their use of Qwt.
       The following example statement can be included in user
       documentation to satisfy this requirement:
 .
           [program/widget] is based in part on the work of
           the Qwt project (http://qwt.sf.net).

License: GFDL-NIV
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A
 copy of the license is included in the section entitled “GNU Free
 Documentation License”.
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License can be found in "/usr/share/common-licenses/GFDL-1.3".

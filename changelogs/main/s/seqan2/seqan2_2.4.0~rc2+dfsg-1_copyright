Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SeqAn
Upstream-Contact: Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
                  Hannes Röst <roest@imsb.biol.ethz.ch>
Source: http://packages.seqan.de/
Files-Excluded: util/py_lib/seqan/dox
                util/py_lib/seqan/dddoc
                util/py_lib/clang
                util/py_lib/termcolor.py
                util/py_lib/threadpool
                util/py_lib/pyratemp.py
                util/mingw
                .*
                */*.bak
                */*HUGE.png
                */*LARGE.png

Files: *
Copyright: 2006-2016, Knut Reinert, FU Berlin
           Andreas Döring <doering@inf.fu-berlin.de>
           David Weese <weese@inf.fu-berlin.de>
           Tobias Rausch <rausch@inf.fu-berlin.de>
           Stephan Aiche <aiche@inf.fu-berlin.de>
           Anne-Katrin Emde <emde@inf.fu-berlin.de>
           Carsten Kemena <kemena@inf.fu-berlin.de>
           Konrad Ludwig Moritz Rudolph <krudolph@inf.fu-berlin.de>
           Marcel H. Schulz. <marcel.schulz@molgen.mpg.de>
           Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
           Hannes Röst <roest@imsb.biol.ethz.ch>
License: BSD-3-clause

Files: apps/dfi/*
Copyright: 2008 David Weese and Marcel H. Schulz
License: GPL-3+

Files: apps/insegt/*.cpp apps/insegt/*.h
Copyright: 2007 Knut Reinert
License: GPL-3+

Files: apps/pair_align/*
Copyright: 2007-2012 Knut Reinert
License: GPL-3+

Files: apps/param_chooser/*
       apps/tree_recon/*
Copyright: 2006-2016 Knut Reinert, FU Berlin
License: LGPL-3+

Files: apps/param_chooser/param_chooser.cpp
Copyright: 2008 Anne-Katrin Emde
License: LGPL-3+

Files: apps/rep_sep/*
Copyright: 2009 by Stephan Aiche
License: GPL-3+

Files: apps/rabema/*
       demos/unassigned_or_unused/arg_align.cpp
       demos/unassigned_or_unused/minimapper.cpp
       demos/howto/scores/load_score.cpp
       demos/howto/scores/init_score.cpp
Copyright: 2010-2012 Manuel Holtgrewe, FU Berlin
License: GPL-3+

Files: apps/razers/*
       apps/splazers/*
Copyright: 2008-2012 Anne-Katrin Emde, David Weese
License: LGPL-3+

Files: apps/seqan_tcoffee/*
Copyright: 2009 Tobias Rausch
License: LGPL-3+

Files: apps/sgip/*
Copyright: 2012 Jialu Hu
License: LGPL-3+

Files: apps/snp_store/*
Copyright: 2008 Anne-Katrin Emde
License: LGPL-3+

Files: apps/stellar/*
Copyright: 2010-2012  Birte Kehr
License: LGPL-3+

Files: include/seqan/stream/iostream_*.h
Copyright: 2003 Jonathan de Halleux
           2014 David Weese <dave.weese@gmail.com>
License: zlib
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a
     product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution

Files: include/seqan/basic/boost_preprocessor_subset.h
Copyright: 2002 Paul Mensonides
License: BSL-1.0

Files: include/seqan/basic/concept_checking.h
Copyright: 2002 John Maddock
License: BSL-1.0

Files: include/seqan/basic/fundamental_concepts.h
Copyright: 2000 Jeremy Siek
           2002 The Trustees of Indiana University
License: BSL-1.0

Files: debian/*
Copyright: 2016 Michael R. Crusoe <crusoe@ucdavis.edu>
                Kevin Murray <spam@kdmurray.id.au>
                Andreas Tille <tille@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Knut Reinert or the FU Berlin nor the names of
      its contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL KNUT REINERT OR THE FU BERLIN BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, you can find the GPL license version 3 in
 ‘/usr/share/common-licenses/GPL-3’.

License: LGPL-3+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian systems, you can find the LGPL license version 3 in
 ‘/usr/share/common-licenses/LGPL-3’.

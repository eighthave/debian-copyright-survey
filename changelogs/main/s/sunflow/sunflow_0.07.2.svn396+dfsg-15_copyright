Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sunflow - rendering system for photo-realistic image synthesis
Upstream-Contact: Christopher Kulla <ckulla@gmail.com>
Source: http://sunflow.sourceforge.net/

Files: *
Copyright: 2003-2008, Christopher Kulla <ckulla@gmail.com>
License: Expat

Files: exporters/blender/sunflow_export.py
Copyright: Robert "hayfever" Lindsay, Miguel "MADCello" Dominguez,
 Olivier "olivS" Saraja, Eugene Reilly, "Heavily Tessellated",
 "Humfred"
License: GPL-2+

Files: exporters/maya/sunflowExporter/src/skylight.cpp
 exporters/maya/sunflowExporter/src/sunskyConstants.h
 exporters/maya/sunflowExporter/src/skylight.h
Copyright: 2006, Mad Crew http://www.madcrew.se
License: GPL-2+

Files: debian/*
Copyright: 2007-2008, Cyril Brulebois <kibi@debian.org>
                2010, Gabriele Giacone <1o5g4r8o@gmail.com>
License: Expat

Files: debian/sunflow.svg
Copyright: 2008, Philipp Hagemeister <phihag@phihag.de>
License: Expat

License: GPL-2+
 On Debian systems the full text of the GNU General Public License can be found
 in the `/usr/share/common-licenses/GPL-2' file.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

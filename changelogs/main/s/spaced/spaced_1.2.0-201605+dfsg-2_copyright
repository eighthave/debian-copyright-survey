Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: spaced
Source: http://spaced.gobics.de/content/spaced.tar.gz
Files-Excluded: spaced

Files: *
Copyright: © 2015 Lars Hahn <lars.hahn@stud.uni-goettingen.de>
License: GPL-3+

Files: debian/*
Copyright: © 2016 Fabian Klötzl <kloetzl@evolbio.mpg.de>
License: GPL-3+

Files: debian/patches/0003-Add-autotools-buildsystem.patch
Copyright: 2008 Benjamin Kostnik <bkoz@redhat.com>,
           2012 Zack Weinberg <zackw@panix.com>,
           2013 Roy Stogner <roystgnr@ices.utexas.edu>,
           2014 Alexey Sokolov <sokolov@google.com>,
           2014, 2015 Google Inc.
License: all-permissive
Comment:
 These copyrights are for the file m4/ax_cxx_compile_stdcxx_11.m4 that is
 added through this quilt patch.

License: all-permissive
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

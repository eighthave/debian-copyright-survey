Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SeqAn
Upstream-Contact: Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
                  Hannes Röst <roest@imsb.biol.ethz.ch>
Source: http://packages.seqan.de/seqan-src/seqan-src-1.4.1.tar.gz
Files-Excluded: util/py_lib/seqan/dddoc/tpl/js
                util/py_lib/seqan/dddoc/tpl/panel
                util/py_lib/seqan/dox/tpl/js
                */extras
                */core/apps/C*
                */core/apps/d*
                */core/apps/f*
                */core/apps/m*
                */core/apps/r*
                */core/apps/p*
                */core/apps/t*
                */core/apps/sa*
                */core/apps/se*
                */core/apps/sn*
                */core/apps/sp*
                */core/apps/stellar/e*
                */core/apps/stellar/t*
                */core/apps/stellar/C*
                */core/apps/stellar/L*
                */core/apps/stellar/R*
                */mingw
                */.git*
                */.travis*
                */*.psd
                */*.ai
                */*HUGE.png
                */*LARGE.png
                */*.exe
                */py2exe
                */*.bak
                */docs
                */docs2
                */dox
                */manual
                */misc/seqan_instrumentation

Files: *
Copyright: 2006-2015
           Andreas Döring <doering@inf.fu-berlin.de>
           David Weese <weese@inf.fu-berlin.de>
           Tobias Rausch <rausch@inf.fu-berlin.de>
           Stephan Aiche <aiche@inf.fu-berlin.de>
           Anne-Katrin Emde <emde@inf.fu-berlin.de>
           Carsten Kemena <kemena@inf.fu-berlin.de>
           Konrad Ludwig Moritz Rudolph <krudolph@inf.fu-berlin.de>
           Marcel H. Schulz. <marcel.schulz@molgen.mpg.de>
           Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
           Hannes Röst <roest@imsb.biol.ethz.ch>
           Knut Reinert, FU Berlin
License: BSD-3-clause

Files: core/demos/arg_align.cpp
       core/demos/howto/scores/*
Copyright: 2010 Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
License: LGPL-3+

Files: core/apps/stellar/*
Copyright: 2010-2012 Birte Kehr
License: LGPL-3+

Files: core/demos/tutorial/read_mapping/minimapper.cpp
Copyright: 2010 Manuel Holtgrewe <manuel.holtgrewe@fu-berlin.de>
License: GPL-3+

Files: core/include/seqan/platform/windows_stdint.h
Copyright: 2010 Broad Institute
 2006-2008 Alexander Chemeris
License: BSD-3-clause

Files: util/py_lib/termcolor.py
Copyright: 2008-2011 Volvox Development Team
License: Expat

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of Knut Reinert or the FU Berlin nor the names of
       its contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL KNUT REINERT OR THE FU BERLIN BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

License: Expat
   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:
 .
   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.
 .
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
   BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
   ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, you can find the GPL license version 3 in
 ‘/usr/share/common-licenses/GPL-3’.

License: LGPL-3+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 . 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, you can find the LGPL license version 3 in
 ‘/usr/share/common-licenses/LGPL-3’.

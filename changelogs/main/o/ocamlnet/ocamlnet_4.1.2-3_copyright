Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Packaged-By: Stefano Zacchiroli <zack@debian.org>
Packaged-Date: Wed,  8 May 2002 00:27:44 +0200.
Source: http://projects.camlcity.org/projects/ocamlnet.html
Upstream-Contact: Gerd Stolpmann <gerd@gerd-stolpmann.de>

Files: *
Copyright: 2001-2013, Gerd Stolpmann and others
License: ocamlnet

Files: src/netcgi2-apache/*.c src/netcgi2-apache/*.h src/netcgi2-apache/*.in
Copyright: 2003-2007, Merjis Ltd. and Christophe Troestler
License: LGPL-2+

Files: src/nethttpd*
Copyright: 2005, Baretta s.r.l. and Gerd Stolpmann
License: GPL-2+

Files: src/netsys/netsys_c_win32.c src/netsys/unixsupport_w32.c
Copyright: 1996, Inria
License: LGPL-exception

Files: debian/*
Copyright: 2002-2009, Stefano Zacchiroli
           2009-2013, Stéphane Glondu
License: ocamlnet

License: ocamlnet
 This software is provided 'as-is', without any express or
 implied warranty. In no event will the authors be held liable
 for any damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you
 must not claim that you wrote the original software. If you use
 this software in a product, an acknowledgment in the product
 documentation would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

License: GPL-2+
 This software is distributed under the terms of the GNU General
 Public License (GPL), version 2 or above. On Debian systems a copy of
 the license is available at `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 This software is distributed under the terms of the GNU Library
 General Public License (LGPL), version 2 or above. On Debian systems a
 copy of the license is available at
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-exception
 This software is distributed under the terms of the GNU Library
 General Public License (LGPL), with the following special exception:
 .
 As a special exception to the GNU Library General Public License, you
 may link, statically or dynamically, a "work that uses the Library"
 with a publicly distributed version of the Library to produce an
 executable file containing portions of the Library, and distribute
 that executable file under terms of your choice, without any of the
 additional requirements listed in clause 6 of the GNU Library General
 Public License.  By "a publicly distributed version of the Library",
 we mean either the unmodified Library as distributed by INRIA, or a
 modified version of the Library that is distributed under the
 conditions defined in clause 3 of the GNU Library General Public
 License.  This exception does not however invalidate any other reasons
 why the executable file might be covered by the GNU Library General
 Public License.
 .
 On Debian systems, the full text of the GNU Library General Public
 License is available at `/usr/share/common-licenses/LGPL-2'.

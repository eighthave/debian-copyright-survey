Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Image package for Octave
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: http://octave.sourceforge.net/image/

Files: *
Copyright: 1999-2001 Kai Habel <kai.habel@gmx.de>
           1999, 2003 Andy Adler <adler@sce.carleton.ca>
           2000 Paul Kienzle <pkienzle@users.sf.net>
           2002 Jeff Orchard <jjo@cs.sfu.ca>
           2002 Jeffrey E. Boyd <boyd@cpsc.ucalgary.ca>
           2004 Josep Monés i Teixidor <jmones@puntbarra.com>
           2004 Antti Niemistö <antti.niemisto@tut.fi>
           2004 Stefan van der Walt <stefan@sun.ac.za>
           2004, 2005, Justus H. Piater <Justus.Piater@ULg.ac.be>
           2005 Carvalho-Mariel
           2005-2008, 2010 Søren Hauberg <soren@hauberg.org>
           2007 Alexander Barth <barth.alexander@gmail.com>
           2008 Jonas Wagner <j.b.w@gmx.ch>
           2006, 2009 Stefan Gustavson <stefan.gustavson@gmail.com>
           2009 Sergey Kirgizov
           2010 Alex Opie <lx_op@orcon.net.nz>
           2010 Andrew Kelly, IPS Radio & Space Services
           2010-2016 Carnë Draug <carandraug@octave.org>
           2011 Adrián del Pino <delpinonavarrete@gmail.com>
           2011 William Krekeler <WKrekeler@cleanearthtech.com>
           2011 Santiago Reyes González <mailalcuete@gmail.com>
           2011, 2012 Jordi Gutiérrez Hermoso <jordigh@octave.org>
           2012 Adam H Aitkenhead <adamaitkenhead@hotmail.com>
           2012 Rik Wehbring
           2012 Pablo Rossi <prossi@ing.unrc.edu.ar>
           2014 Benjamin Eltzner <b.eltzner@gmx.de>
           2015-2016 Hartmut Gimpel <hg_code@gmx.de>
           2015 Avinoam Kalma <a.kalma@gmail.com>
License: GPL-3+

Files: ./inst/immaximas.m
Copyright: 2003-2005, Peter Kovesi
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 . 
 The software is provided "as is", without warranty of any kind, express or
 implied, including but not limited to the warranties of merchantability,
 fitness for a particular purpose and noninfringement. In no event shall the
 authors or copyright holders be liable for any claim, damages or other
 liability, whether in an action of contract, tort or otherwise, arising from,
 out of or in connection with the software or the use or other dealings in the
 software.

Files: inst/colfilt.m inst/colorgradient.m
Copyright: none
License: public-domain
 This code is in the public domain.
 .
 Original author: Paul Kienzle <pkienzle@users.sf.net>

Files: inst/fftconv2.m src/graycomatrix.cc src/hough_line.cc
Copyright: 2004, Stefan van der Walt <stefan@sun.ac.za>
License: BSD-2
 This program is free software; redistribution and use in source and
 binary forms, with or without modification, are permitted provided that
 the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: debian/*
Copyright: 2008 Ólafur Jens Sigurðsson <ojsbug@gmail.com>
           2008-2009, 2012-2014 Rafael Laboissiere <rafael@debian.org>
           2008-2011 Thomas Weber <tweber@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

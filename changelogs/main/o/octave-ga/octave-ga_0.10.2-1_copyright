Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ga package for Octave
Upstream-Contact: Luca Favatella <slackydeb@gmail.com>
Source: https://octave.sourceforge.io/ga/

Files: *
Copyright: 2008-2019 Luca Favatella <slackydeb@gmail.com>
           2019-2020 John D <john.donoghue@ieee.org>
License: GPL-3+

Files: Makefile
Copyright: 2015-2016 Carnë Draug
           2015-2016 Oliver Heimlich
           2017 Julien Bect <jbect@users.sf.net>
           2017 Olaf Till <i7tiol@t-online.de>
           2019 John Donoghue <john.donoghue@ieee.org>
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: debian/*
Copyright: 2008 Olafur Jens Sigurdsson <ojsbug@gmail.com>
           2008-2009, 2014, 2017-2020 Rafael Laboissiere <rafael@debian.org>
           2008-2012 Thomas Weber <tweber@debian.org>
           2012-2019 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.


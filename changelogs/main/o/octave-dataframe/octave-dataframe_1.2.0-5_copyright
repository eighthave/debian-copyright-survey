Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Data manipulation toolbox similar to R data.frame
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: https://octave.sourceforge.io/dataframe/

Files: *
Copyright: 2009-2017 Pascal Dupuis <cdemills@gmail.com>
License: GPL-3+

Files: inst/@dataframe/private/*
Copyright: 2009-2017 Pascal Dupuis <cdemills@gmail.com>
License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: Makefile
Copyright: 2015-2016 Carnë Draug
           2015-2016 Oliver Heimlich
           2017 Julien Bect <jbect@users.sf.net>
           2017 Olaf Till <i7tiol@t-online.de>
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: debian/*
Copyright: 2012, 2014, 2015, 2017-2020 Rafael Laboissiere <rafael@debian.org>
           2014-2017, 2020 Sébastien Villemot <sebastien@debian.org>
           2018 Mike Miller <mtmiller@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OSSIM
Upstream-Contact: OSSIM Developers <ossim-developer@lists.sourceforge.net>
Source: https://github.com/ossimlabs/ossim

Files: *
Copyright: 1994-1996, Thomas G. Lane
                1997, TargetJr Consortium
                1999, Frank Warmerdam <warmerdam@pobox.com>
           2000-2002, ImageLinks, Inc
                2003, Storage Area Networks, Inc
          2000, 2004, Intelligence Data Systems
                2004, VIZRT Inc
           2004-2005, Garrett Potts
           2004-2005, David Burken
                2005, Oscar Kramer
                2005, SANZ Inc
                2007, Ball Aerspace & Technologies Corp
                2010, Radiant Blue Technologies Inc
                2014, RadiantBlue, Inc
License: Expat

Files: include/ossim/base/ossimFlexLexer.h
Copyright: 1993, The Regents of the University of California
Comment: This code is derived from software contributed to Berkeley by
 Kent Williams and Tom Epperly.
License: University-of-California
 Redistribution and use in source and binary forms with or without
 modification are permitted provided that: (1) source distributions retain
 this entire copyright notice and comment, and (2) distributions including
 binaries display the following acknowledgement:  ``This product includes
 software developed by the University of California, Berkeley and its
 contributors'' in the documentation or other materials provided with the
 distribution and in all advertising materials mentioning features or use
 of this software.  Neither the name of the University nor the names of
 its contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Files: include/ossim/base/ossimGzStream.h
 src/base/ossimGzStream.cpp
Copyright: 2001, Deepak Bandyopadhyay, Lutz Kettner
License: LGPL-2.1+

Files: include/ossim/base/ossimRegExp.h
 src/base/ossimRegExp.cpp
Copyright: 1991, Texas Instruments Incorporated
License: Texas-Instruments
 Permission is granted to any individual or institution to use, copy, modify,
 and distribute this software, provided that this complete copyright and
 permission notice is maintained, intact, in all copies and supporting
 documentation.
 .
 Texas Instruments Incorporated provides this software "as is" without
 express or implied warranty.

Files: include/ossim/matrix/*
 src/matrix/*
Copyright: 1991-2000, 2002-2003, R B Davies
Comment: See http://robertnz.net/nm10.htm for current license conditions
License: matrix
 I place no restrictions on the use of newmat except that I take no
 liability for any problems that may arise from its use, distribution or
 other dealings with it.
 .
 You can use it in your commercial projects.
 .
 You can make and distribute modified or merged versions. You can include
 parts of it in your own software.
 .
 If you distribute modified or merged versions, please make it clear
 which parts are mine and which parts are modified.
 .
 For a substantially modified version, simply note that it is, in part,
 derived from my software. A comment in the code will be sufficient.
 .
 The software is provided "as is", without warranty of any kind.
 .
 Please understand that there may still be bugs and errors. Use at your
 own risk. I (Robert Davies) take no responsibility for any errors or
 omissions in this package or for any misfortune that may befall you or
 others as a result of your use, distribution or other dealings with it.

Files: include/ossim/vec/ossimVpfExtent.h
 src/vec/ossimVpfExtent.cpp
Copyright: 1994-1995, Vectaport Inc., Cartoactive Systems
License: Vectaport
 Permission to use, copy, modify, distribute, and sell this software and
 its documentation for any purpose is hereby granted without fee, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the names of the copyright holders not be used in
 advertising or publicity pertaining to distribution of the software
 without specific, written prior permission.  The copyright holders make
 no representations about the suitability of this software for any purpose.
 It is provided "as is" without express or implied warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: include/ossim/vpfutil/values.h
Copyright: 1995-1997, Free Software Foundation, Inc
License: LGPL-2+

Files: scripts/git-prompt.sh
License: GPL-2+
Copyright: 2006-2007 Shawn O. Pearce <spearce@spearce.org>

Files: src/imaging/ossimFilter.cpp
Copyright: 2000, ImageMagick Studio
License: ImageMagick
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files ("ImageMagick"),
 to deal in ImageMagick without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of ImageMagick, and to permit persons to whom the
 ImageMagick is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of ImageMagick.
 .
 The software is provided "as is", without warranty of any kind, express or
 implied, including but not limited to the warranties of merchantability,
 fitness for a particular purpose and noninfringement.  In no event shall
 ImageMagick Studio be liable for any claim, damages or other liability,
 whether in an action of contract, tort or otherwise, arising from, out of
 or in connection with ImageMagick or the use or other dealings in
 ImageMagick.
 .
 Except as contained in this notice, the name of the ImageMagick Studio
 shall not be used in advertising or otherwise to promote the sale, use or
 other dealings in ImageMagick without prior written authorization from the
 ImageMagick Studio.

Files: src/imaging/ossimJpegStdIOSrc.cpp
Copyright: 1994-1996, Thomas G. Lane
                      Centre National d'Etudes Spatiales.
Comment: The OTB is distributed under the CeCILL license version 2. See files
 Licence_CeCILL_V2-en.txt (english version) or Licence_CeCILL_V2-fr.txt
 (french version) in 'Copyright' directory for details. This licenses are
 also available online:
 http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 http://www.cecill.info/licences/Licence_CeCILL_V2-fr.txt
License: CeCILL-2.0

Files: src/imaging/ossimRgbImage.cpp
Copyright: 1994-2001, Cold Spring Harbor Laboratory
           1996-2001, Boutell.Com, Inc
           1999-2000, Philip Warner
           1999-2000, Greg Roelofs
           1999-2000, John Ellson <ellson@lucent.com>
                2000, Doug Becker
           1994-1998, Thomas G. Lane
                2000, Maurice Szmurlo and Johan Van den Brande
Comment: File header documents the following:
 Credits:
        Much of the implementation within this file was taken from gd
        We can use it but we need to give credit to the people who wrote
        the algorithms.
 .
 COPYRIGHT STATEMENT FOLLOWS THIS LINE
 .
      Portions copyright 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001
      by Cold Spring Harbor Laboratory. Funded under Grant P41-RR02188 by
      the National Institutes of Health.
 .
      Portions copyright 1996, 1997, 1998, 1999, 2000, 2001 by
      Boutell.Com, Inc.
 .
      Portions relating to GD2 format copyright 1999, 2000 Philip Warner.
 .
      Portions relating to PNG copyright 1999, 2000 Greg Roelofs.
 .
      Portions relating to libttf copyright 1999, 2000 John Ellson
      (ellson@lucent.com).
 .
      Portions relating to JPEG and to color quantization copyright 2000,
      Doug Becker and copyright (C) 1994-1998, Thomas G. Lane. This
      software is based in part on the work of the Independent JPEG
      Group. See the file README-JPEG.TXT for more information.
 .
      Portions relating to WBMP copyright 2000 Maurice Szmurlo and Johan
      Van den Brande.
 .
      _Permission has been granted to copy, distribute and modify gd in
      any context without fee, including a commercial application,
      provided that this notice is present in user-accessible supporting
      documentation._
 .
      This does not affect your ownership of the derived work itself, and
      the intent is to assure proper credit for the authors of gd, not to
      interfere with your productive use of gd. If you have questions,
      ask. "Derived works" includes all programs that utilize the
      library. Credit must be given in user-accessible documentation.
 .
      _This software is provided "AS IS."_ The copyright holders disclaim
      all warranties, either express or implied, including but not
      limited to implied warranties of merchantability and fitness for a
      particular purpose, with respect to this code and accompanying
      documentation.
 .
      Although their code does not appear in gd 2.0.1, the authors wish
      to thank David Koblas, David Rowley, and Hutchison Avenue Software
      Corporation for their prior contributions.
License: Expat

Files: debian/*
Copyright: 2009, Francesco Paolo Lovergine <frankie@debian.org>
License: GPL-3+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: CeCILL-2.0
 CeCILL FREE SOFTWARE LICENSE AGREEMENT
 .
     Notice
 .
 This Agreement is a Free Software license agreement that is the result
 of discussions between its authors in order to ensure compliance with
 the two main principles guiding its drafting:
 .
     * firstly, compliance with the principles governing the distribution
       of Free Software: access to source code, broad rights granted to
       users,
     * secondly, the election of a governing law, French law, with which
       it is conformant, both as regards the law of torts and
       intellectual property law, and the protection that it offers to
       both authors and holders of the economic rights over software.
 .
 The authors of the CeCILL (for Ce[a] C[nrs] I[nria] L[ogiciel] L[ibre])
 license are:
 .
 Commissariat  l'Energie Atomique - CEA, a public scientific, technical
 and industrial research establishment, having its principal place of
 business at 25 rue Leblanc, immeuble Le Ponant D, 75015 Paris, France.
 .
 Centre National de la Recherche Scientifique - CNRS, a public scientific
 and technological establishment, having its principal place of business
 at 3 rue Michel-Ange, 75794 Paris cedex 16, France.
 .
 Institut National de Recherche en Informatique et en Automatique -
 INRIA, a public scientific and technological establishment, having its
 principal place of business at Domaine de Voluceau, Rocquencourt, BP
 105, 78153 Le Chesnay cedex, France.
 .
 .
     Preamble
 .
 The purpose of this Free Software license agreement is to grant users
 the right to modify and redistribute the software governed by this
 license within the framework of an open source distribution model.
 .
 The exercising of these rights is conditional upon certain obligations
 for users so as to preserve this status for all subsequent redistributions.
 .
 In consideration of access to the source code and the rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty and the software's author, the holder of the
 economic rights, and the successive licensors only have limited liability.
 .
 In this respect, the risks associated with loading, using, modifying
 and/or developing or reproducing the software by the user are brought to
 the user's attention, given its Free Software status, which may make it
 complicated to use, with the result that its use is reserved for
 developers and experienced professionals having in-depth computer
 knowledge. Users are therefore encouraged to load and test the
 suitability of the software as regards their requirements in conditions
 enabling the security of their systems and/or data to be ensured and,
 more generally, to use and operate it in the same conditions of
 security. This Agreement may be freely reproduced and published,
 provided it is not altered, and that no provisions are either added or
 removed herefrom.
 .
 This Agreement may apply to any or all software for which the holder of
 the economic rights decides to submit the use thereof to its provisions.
 .
 .
     Article 1 - DEFINITIONS
 .
 For the purpose of this Agreement, when the following expressions
 commence with a capital letter, they shall have the following meaning:
 .
 Agreement: means this license agreement, and its possible subsequent
 versions and annexes.
 .
 Software: means the software in its Object Code and/or Source Code form
 and, where applicable, its documentation, "as is" when the Licensee
 accepts the Agreement.
 .
 Initial Software: means the Software in its Source Code and possibly its
 Object Code form and, where applicable, its documentation, "as is" when
 it is first distributed under the terms and conditions of the Agreement.
 .
 Modified Software: means the Software modified by at least one
 Contribution.
 .
 Source Code: means all the Software's instructions and program lines to
 which access is required so as to modify the Software.
 .
 Object Code: means the binary files originating from the compilation of
 the Source Code.
 .
 Holder: means the holder(s) of the economic rights over the Initial
 Software.
 .
 Licensee: means the Software user(s) having accepted the Agreement.
 .
 Contributor: means a Licensee having made at least one Contribution.
 .
 Licensor: means the Holder, or any other individual or legal entity, who
 distributes the Software under the Agreement.
 .
 Contribution: means any or all modifications, corrections, translations,
 adaptations and/or new functions integrated into the Software by any or
 all Contributors, as well as any or all Internal Modules.
 .
 Module: means a set of sources files including their documentation that
 enables supplementary functions or services in addition to those offered
 by the Software.
 .
 External Module: means any or all Modules, not derived from the
 Software, so that this Module and the Software run in separate address
 spaces, with one calling the other when they are run.
 .
 Internal Module: means any or all Module, connected to the Software so
 that they both execute in the same address space.
 .
 GNU GPL: means the GNU General Public License version 2 or any
 subsequent version, as published by the Free Software Foundation Inc.
 .
 Parties: mean both the Licensee and the Licensor.
 .
 These expressions may be used both in singular and plural form.
 .
 .
     Article 2 - PURPOSE
 .
 The purpose of the Agreement is the grant by the Licensor to the
 Licensee of a non-exclusive, transferable and worldwide license for the
 Software as set forth in Article 5 hereinafter for the whole term of the
 protection granted by the rights over said Software.
 .
 .
     Article 3 - ACCEPTANCE
 .
 3.1 The Licensee shall be deemed as having accepted the terms and
 conditions of this Agreement upon the occurrence of the first of the
 following events:
 .
     * (i) loading the Software by any or all means, notably, by
       downloading from a remote server, or by loading from a physical
       medium;
     * (ii) the first time the Licensee exercises any of the rights
       granted hereunder.
 .
 3.2 One copy of the Agreement, containing a notice relating to the
 characteristics of the Software, to the limited warranty, and to the
 fact that its use is restricted to experienced users has been provided
 to the Licensee prior to its acceptance as set forth in Article 3.1
 hereinabove, and the Licensee hereby acknowledges that it has read and
 understood it.
 .
 .
     Article 4 - EFFECTIVE DATE AND TERM
 .
 .
       4.1 EFFECTIVE DATE
 .
 The Agreement shall become effective on the date when it is accepted by
 the Licensee as set forth in Article 3.1.
 .
 .
       4.2 TERM
 .
 The Agreement shall remain in force for the entire legal term of
 protection of the economic rights over the Software.
 .
 .
     Article 5 - SCOPE OF RIGHTS GRANTED
 .
 The Licensor hereby grants to the Licensee, who accepts, the following
 rights over the Software for any or all use, and for the term of the
 Agreement, on the basis of the terms and conditions set forth hereinafter.
 .
 Besides, if the Licensor owns or comes to own one or more patents
 protecting all or part of the functions of the Software or of its
 components, the Licensor undertakes not to enforce the rights granted by
 these patents against successive Licensees using, exploiting or
 modifying the Software. If these patents are transferred, the Licensor
 undertakes to have the transferees subscribe to the obligations set
 forth in this paragraph.
 .
 .
       5.1 RIGHT OF USE
 .
 The Licensee is authorized to use the Software, without any limitation
 as to its fields of application, with it being hereinafter specified
 that this comprises:
 .
    1. permanent or temporary reproduction of all or part of the Software
       by any or all means and in any or all form.
 .
    2. loading, displaying, running, or storing the Software on any or
       all medium.
 .
    3. entitlement to observe, study or test its operation so as to
       determine the ideas and principles behind any or all constituent
       elements of said Software. This shall apply when the Licensee
       carries out any or all loading, displaying, running, transmission
       or storage operation as regards the Software, that it is entitled
       to carry out hereunder.
 .
 .
       5.2 ENTITLEMENT TO MAKE CONTRIBUTIONS
 .
 The right to make Contributions includes the right to translate, adapt,
 arrange, or make any or all modifications to the Software, and the right
 to reproduce the resulting software.
 .
 The Licensee is authorized to make any or all Contributions to the
 Software provided that it includes an explicit notice that it is the
 author of said Contribution and indicates the date of the creation thereof.
 .
 .
       5.3 RIGHT OF DISTRIBUTION
 .
 In particular, the right of distribution includes the right to publish,
 transmit and communicate the Software to the general public on any or
 all medium, and by any or all means, and the right to market, either in
 consideration of a fee, or free of charge, one or more copies of the
 Software by any means.
 .
 The Licensee is further authorized to distribute copies of the modified
 or unmodified Software to third parties according to the terms and
 conditions set forth hereinafter.
 .
 .
         5.3.1 DISTRIBUTION OF SOFTWARE WITHOUT MODIFICATION
 .
 The Licensee is authorized to distribute true copies of the Software in
 Source Code or Object Code form, provided that said distribution
 complies with all the provisions of the Agreement and is accompanied by:
 .
    1. a copy of the Agreement,
 .
    2. a notice relating to the limitation of both the Licensor's
       warranty and liability as set forth in Articles 8 and 9,
 .
 and that, in the event that only the Object Code of the Software is
 redistributed, the Licensee allows future Licensees unhindered access to
 the full Source Code of the Software by indicating how to access it, it
 being understood that the additional cost of acquiring the Source Code
 shall not exceed the cost of transferring the data.
 .
 .
         5.3.2 DISTRIBUTION OF MODIFIED SOFTWARE
 .
 When the Licensee makes a Contribution to the Software, the terms and
 conditions for the distribution of the resulting Modified Software
 become subject to all the provisions of this Agreement.
 .
 The Licensee is authorized to distribute the Modified Software, in
 source code or object code form, provided that said distribution
 complies with all the provisions of the Agreement and is accompanied by:
 .
    1. a copy of the Agreement,
 .
    2. a notice relating to the limitation of both the Licensor's
       warranty and liability as set forth in Articles 8 and 9,
 .
 and that, in the event that only the object code of the Modified
 Software is redistributed, the Licensee allows future Licensees
 unhindered access to the full source code of the Modified Software by
 indicating how to access it, it being understood that the additional
 cost of acquiring the source code shall not exceed the cost of
 transferring the data.
 .
 .
         5.3.3 DISTRIBUTION OF EXTERNAL MODULES
 .
 When the Licensee has developed an External Module, the terms and
 conditions of this Agreement do not apply to said External Module, that
 may be distributed under a separate license agreement.
 .
 .
         5.3.4 COMPATIBILITY WITH THE GNU GPL
 .
 The Licensee can include a code that is subject to the provisions of one
 of the versions of the GNU GPL in the Modified or unmodified Software,
 and distribute that entire code under the terms of the same version of
 the GNU GPL.
 .
 The Licensee can include the Modified or unmodified Software in a code
 that is subject to the provisions of one of the versions of the GNU GPL,
 and distribute that entire code under the terms of the same version of
 the GNU GPL.
 .
 .
     Article 6 - INTELLECTUAL PROPERTY
 .
 .
       6.1 OVER THE INITIAL SOFTWARE
 .
 The Holder owns the economic rights over the Initial Software. Any or
 all use of the Initial Software is subject to compliance with the terms
 and conditions under which the Holder has elected to distribute its work
 and no one shall be entitled to modify the terms and conditions for the
 distribution of said Initial Software.
 .
 The Holder undertakes that the Initial Software will remain ruled at
 least by this Agreement, for the duration set forth in Article 4.2.
 .
 .
       6.2 OVER THE CONTRIBUTIONS
 .
 The Licensee who develops a Contribution is the owner of the
 intellectual property rights over this Contribution as defined by
 applicable law.
 .
 .
       6.3 OVER THE EXTERNAL MODULES
 .
 The Licensee who develops an External Module is the owner of the
 intellectual property rights over this External Module as defined by
 applicable law and is free to choose the type of agreement that shall
 govern its distribution.
 .
 .
       6.4 JOINT PROVISIONS
 .
 The Licensee expressly undertakes:
 .
    1. not to remove, or modify, in any manner, the intellectual property
       notices attached to the Software;
 .
    2. to reproduce said notices, in an identical manner, in the copies
       of the Software modified or not.
 .
 The Licensee undertakes not to directly or indirectly infringe the
 intellectual property rights of the Holder and/or Contributors on the
 Software and to take, where applicable, vis--vis its staff, any and all
 measures required to ensure respect of said intellectual property rights
 of the Holder and/or Contributors.
 .
 .
     Article 7 - RELATED SERVICES
 .
 7.1 Under no circumstances shall the Agreement oblige the Licensor to
 provide technical assistance or maintenance services for the Software.
 .
 However, the Licensor is entitled to offer this type of services. The
 terms and conditions of such technical assistance, and/or such
 maintenance, shall be set forth in a separate instrument. Only the
 Licensor offering said maintenance and/or technical assistance services
 shall incur liability therefore.
 .
 7.2 Similarly, any Licensor is entitled to offer to its licensees, under
 its sole responsibility, a warranty, that shall only be binding upon
 itself, for the redistribution of the Software and/or the Modified
 Software, under terms and conditions that it is free to decide. Said
 warranty, and the financial terms and conditions of its application,
 shall be subject of a separate instrument executed between the Licensor
 and the Licensee.
 .
 .
     Article 8 - LIABILITY
 .
 8.1 Subject to the provisions of Article 8.2, the Licensee shall be
 entitled to claim compensation for any direct loss it may have suffered
 from the Software as a result of a fault on the part of the relevant
 Licensor, subject to providing evidence thereof.
 .
 8.2 The Licensor's liability is limited to the commitments made under
 this Agreement and shall not be incurred as a result of in particular:
 (i) loss due the Licensee's total or partial failure to fulfill its
 obligations, (ii) direct or consequential loss that is suffered by the
 Licensee due to the use or performance of the Software, and (iii) more
 generally, any consequential loss. In particular the Parties expressly
 agree that any or all pecuniary or business loss (i.e. loss of data,
 loss of profits, operating loss, loss of customers or orders,
 opportunity cost, any disturbance to business activities) or any or all
 legal proceedings instituted against the Licensee by a third party,
 shall constitute consequential loss and shall not provide entitlement to
 any or all compensation from the Licensor.
 .
 .
     Article 9 - WARRANTY
 .
 9.1 The Licensee acknowledges that the scientific and technical
 state-of-the-art when the Software was distributed did not enable all
 possible uses to be tested and verified, nor for the presence of
 possible defects to be detected. In this respect, the Licensee's
 attention has been drawn to the risks associated with loading, using,
 modifying and/or developing and reproducing the Software which are
 reserved for experienced users.
 .
 The Licensee shall be responsible for verifying, by any or all means,
 the suitability of the product for its requirements, its good working
 order, and for ensuring that it shall not cause damage to either persons
 or properties.
 .
 9.2 The Licensor hereby represents, in good faith, that it is entitled
 to grant all the rights over the Software (including in particular the
 rights set forth in Article 5).
 .
 9.3 The Licensee acknowledges that the Software is supplied "as is" by
 the Licensor without any other express or tacit warranty, other than
 that provided for in Article 9.2 and, in particular, without any warranty
 as to its commercial value, its secured, safe, innovative or relevant
 nature.
 .
 Specifically, the Licensor does not warrant that the Software is free
 from any error, that it will operate without interruption, that it will
 be compatible with the Licensee's own equipment and software
 configuration, nor that it will meet the Licensee's requirements.
 .
 9.4 The Licensor does not either expressly or tacitly warrant that the
 Software does not infringe any third party intellectual property right
 relating to a patent, software or any other property right. Therefore,
 the Licensor disclaims any and all liability towards the Licensee
 arising out of any or all proceedings for infringement that may be
 instituted in respect of the use, modification and redistribution of the
 Software. Nevertheless, should such proceedings be instituted against
 the Licensee, the Licensor shall provide it with technical and legal
 assistance for its defense. Such technical and legal assistance shall be
 decided on a case-by-case basis between the relevant Licensor and the
 Licensee pursuant to a memorandum of understanding. The Licensor
 disclaims any and all liability as regards the Licensee's use of the
 name of the Software. No warranty is given as regards the existence of
 prior rights over the name of the Software or as regards the existence
 of a trademark.
 .
 .
     Article 10 - TERMINATION
 .
 10.1 In the event of a breach by the Licensee of its obligations
 hereunder, the Licensor may automatically terminate this Agreement
 thirty (30) days after notice has been sent to the Licensee and has
 remained ineffective.
 .
 10.2 A Licensee whose Agreement is terminated shall no longer be
 authorized to use, modify or distribute the Software. However, any
 licenses that it may have granted prior to termination of the Agreement
 shall remain valid subject to their having been granted in compliance
 with the terms and conditions hereof.
 .
 .
     Article 11 - MISCELLANEOUS
 .
 .
       11.1 EXCUSABLE EVENTS
 .
 Neither Party shall be liable for any or all delay, or failure to
 perform the Agreement, that may be attributable to an event of force
 majeure, an act of God or an outside cause, such as defective
 functioning or interruptions of the electricity or telecommunications
 networks, network paralysis following a virus attack, intervention by
 government authorities, natural disasters, water damage, earthquakes,
 fire, explosions, strikes and labor unrest, war, etc.
 .
 11.2 Any failure by either Party, on one or more occasions, to invoke
 one or more of the provisions hereof, shall under no circumstances be
 interpreted as being a waiver by the interested Party of its right to
 invoke said provision(s) subsequently.
 .
 11.3 The Agreement cancels and replaces any or all previous agreements,
 whether written or oral, between the Parties and having the same
 purpose, and constitutes the entirety of the agreement between said
 Parties concerning said purpose. No supplement or modification to the
 terms and conditions hereof shall be effective as between the Parties
 unless it is made in writing and signed by their duly authorized
 representatives.
 .
 11.4 In the event that one or more of the provisions hereof were to
 conflict with a current or future applicable act or legislative text,
 said act or legislative text shall prevail, and the Parties shall make
 the necessary amendments so as to comply with said act or legislative
 text. All other provisions shall remain effective. Similarly, invalidity
 of a provision of the Agreement, for any reason whatsoever, shall not
 cause the Agreement as a whole to be invalid.
 .
 .
       11.5 LANGUAGE
 .
 The Agreement is drafted in both French and English and both versions
 are deemed authentic.
 .
 .
     Article 12 - NEW VERSIONS OF THE AGREEMENT
 .
 12.1 Any person is authorized to duplicate and distribute copies of this
 Agreement.
 .
 12.2 So as to ensure coherence, the wording of this Agreement is
 protected and may only be modified by the authors of the License, who
 reserve the right to periodically publish updates or new versions of the
 Agreement, each with a separate number. These subsequent versions may
 address new issues encountered by Free Software.
 .
 12.3 Any Software distributed under a given version of the Agreement may
 only be subsequently distributed under the same version of the Agreement
 or a subsequent version, subject to the provisions of Article 5.3.4.
 .
 .
     Article 13 - GOVERNING LAW AND JURISDICTION
 .
 13.1 The Agreement is governed by French law. The Parties agree to
 endeavor to seek an amicable solution to any disagreements or disputes
 that may arise during the performance of the Agreement.
 .
 13.2 Failing an amicable solution within two (2) months as from their
 occurrence, and unless emergency proceedings are necessary, the
 disagreements or disputes shall be referred to the Paris Courts having
 jurisdiction, by the more diligent Party.
 .
 .
 Version 2.0 dated 2006-09-05.


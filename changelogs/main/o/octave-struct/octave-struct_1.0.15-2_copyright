Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Struct package for Octave
Upstream-Contact: Olaf Till <i7tiol@t-online.de>
Source: https://octave.sourceforge.io/struct/

Files: *
Copyright: 2000 Etienne Grossmann <etienne@egdn.net>
           2000 Paul Kienzle <pkienzle@users.sf.net>
           2010-2018 Olaf Till <i7tiol@t-online.de>
License: GPL-3+

Files: Makefile
       octave-struct.metainfo.xml
Copyright: 2015-2016 Carnë Draug
           2015-2016 Oliver Heimlich
           2017 Julien Bect <jbect@users.sf.net>
           2017 Olaf Till <i7tiol@t-online.de>
           2016 Colin B. Macdonald
License: permissive
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: debian/*
Copyright: 2008 Ólafur Jens Sigurðsson <ojsbug@gmail.com>
           2008-2009, 2012, 2014-2019 Rafael Laboissiere <rafael@debian.org>
           2009-2011 Thomas Weber <tweber@debian.org>
           2012-2018 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.


Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OsmoTRX
Source: http://cgit.osmocom.org/osmo-trx/
Files-Excluded: Transceiver52M/std_inband.rbf

Files: *
Copyright: 2008-2014 Free Software Foundation
           2010 Kestrel Signal Processing, Inc.
           2010-2012 Range Networks, Inc.
           2012-2014 Tom Tsou <tom@tsou.cc>
           2015-2016 Ettus Research LLC
           2018 sysmocom - s.f.m.c. GmbH
License: AGPL-3+

Files: Transceiver52M/Resampler.cpp
       Transceiver52M/Resampler.h
       Transceiver52M/osmo-trx.cpp
       Transceiver52M/ChannelizerBase.cpp
       Transceiver52M/Channelizer.cpp
       Transceiver52M/Synthesis.cpp
Copyright: 2012-2013 Thomas Tsou <tom@tsou.cc>
License: LGPL-2.1+

Files: config/ax_check_compile_flag.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2011 Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+

Files: CommonLibs/Makefile.am
       GSM/Makefile.am
       Transceiver52M/Makefile.am
       Transceiver52M/Transceiver.h
       Transceiver52M/Transceiver.cpp
Copyright: 2008-2010 Free Software Foundation
           2010-2012 Range Networks, Inc.
License: GPL-3+

Files: CommonLibs/trx_vty.c
Copyright: 2012-2017 sysmocom - s.f.m.c. GmbH
License: GPL-2+

Files: debian/*
Copyright: 2015,2017,2018 Ruben Undheim <ruben.undheim@gmail.com>
           2017,2018 Thorsten Alteholz <debian@alteholz.de>
License: GPL-3+


License: AGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".


License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see
 <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Linear-Algebra package for Octave
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: https://octave.sourceforge.io/linear-algebra/

Files: *
Copyright: 2010 Soren Hauberg
           2009-2010 VZLU Prague
           2006, 2007 Arno Onken <asnelt@asnelt.org>
           2008 Muthiah Annamalai <muthiah.annamalai@uta.edu>
           2001 Paul Kienzle <pkienzle@users.sf.net>
           2001 Rolf Fabian <fabian@tu-cottbus.de>
           2000-2019 P.R. Nienhuis <prnienhuis@users.sf.net>
           2011 Carnë Draug <carandraug+dev@gmail.com>
           2012 Nir Krakauer <nkrakauer@ccny.cuny.edu>
           200-2009, 2012 Jingu Kim and Haesun Park <jingu@cc.gatech.edu>
           2002 Etienne Grossmann <etienne@egdn.net>
           2013 Her Majesty The Queen In Right of Canada
License: GPL-3+

Files: inst/lobpcg.m
Copyright: 2000-2011 A.V. Knyazev <Andrew.Knyazev@ucdenver.edu>
License: LGPL-3+

Files: inst/nmf_pg.m
Copyright: 2005-2006 Chih-Jen Lin <cjlin@csie.ntu.edu.tw>
License: BSD-2-clause

Files: debian/*
Copyright: 2008 Ólafur Jens Sigurðsson <ojsbug@gmail.com>
           2008-2009, 2014-2020 Rafael Laboissiere <rafael@debian.org>
           2008-2011 Thomas Weber <tweber@debian.org>
           2011-2012, 2014, 2017, 2018 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     1 Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
     2 Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ''AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-3+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: opari

Files: *
Copyright: 2001 Forschungszentrum Juelich GmbH,
           Zentralinstitut fuer Angewandte Mathematik,
           Federal Republic of Germany.
License: misc
 All rights reserved. Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that the following conditions
 are met: 
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution. 
 .
 * Any publications that result from the use of this software shall
   reasonably refer to the Research Centre's development. 
 .
 * All advertising materials mentioning features or use of this software
   must display the following acknowledgement: 
 .
     This product includes software developed by Forschungszentrum
     Juelich GmbH, Federal Republic of Germany. 
 .
 * Forschungszentrum Juelich GmbH is not obligated to provide the user
   with any support, consulting, training or assistance of any kind with
   regard to the use, operation and performance of this software
   or to provide the user with any updates, revisions or new versions. 
 .
 THIS SOFTWARE IS PROVIDED BY FORSCHUNGSZENTRUM JUELICH GMBH "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL FORSCHUNGSZENTRUM JUELICH GMBH BE LIABLE FOR
 ANY SPECIAL, DIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
 ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE. 

Files: debian/*
Copyright: 2012 Samuel Thibault <sthibault@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

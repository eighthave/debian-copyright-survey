Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Ocsigen Server
Upstream-Contact: The Ocsigen Team <dev@ocsigen.org>
Source: http://ocsigen.org/ocsigenserver/install

License: LGPL-2.1+ and other
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 The complete text of the GNU Lesser General Public License can be
 found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 This program is released under the LGPL version 2.1 (see the text
 below) with the additional exemption that compiling, linking, and/or
 using OpenSSL is allowed.
 .
 As a special exception to the GNU Library General Public License,
 you may also link, statically or dynamically, a "work that uses the
 Library" with a publicly distributed version of the Library to
 produce an executable file containing portions of the Library, and
 distribute that executable file under terms of your choice, without
 any of the additional requirements listed in clause 6 of the GNU
 Library General Public License.  By "a publicly distributed version
 of the Library", we mean either the unmodified Library, or a
 modified version of the Library that is distributed under the
 conditions defined in clause 3 of the GNU Library General Public
 License.  This exception does not however invalidate any other
 reasons why the executable file might be covered by the GNU Library
 General Public License.

Files: *
Copyright: 2005-2012, Vincent Balat and many others
License: LGPL-2.1+ and other

Files: src/http/multipart.ml
Copyright: 2001, Patrick Doane and Gerd Stolpmann
License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software. If you use this
    software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.

Files: local/var/www/ocsigenstuff/*
Copyright: 2007, Nuno Pinheiro <nuno@oxygen-icons.org>
           2007, David Vignoni <david@icon-king.com>
           2007, David Miller <miller@oxygen-icons.org>
           2007, Johann Ollivier Lapeyre <johann@oxygen-icons.org>
           2007, Kenneth Wimer <kwwii@bootsplash.org>
           2007, Riccardo Iaconelli <riccardo@oxygen-icons.org>
License: LGPL-3+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 Clarification
 .
 The GNU Lesser General Public License or LGPL is written for software
 libraries in the first place. We expressly want the LGPL to be valid
 for this artwork library too.
 .
 KDE Oxygen theme icons is a special kind of software library, it is
 an artwork library, it's elements can be used in a Graphical User
 Interface, or GUI.
 .
 Source code, for this library means:
  - where they exist, SVG;
  - otherwise, if applicable, the multi-layered formats xcf or psd, or
    otherwise png.
 .
 The LGPL in some sections obliges you to make the files carry
 notices. With images this is in some cases impossible or hardly
 useful.
 .
 With this library a notice is placed at a prominent place in the
 directory containing the elements. You may follow this practice.
 .
 The exception in section 5 of the GNU Lesser General Public License
 covers the use of elements of this art library in a GUI.

Files: debian/*
Copyright: 2006-2007, Samuel Mimram <smimram@debian.org>
           2007-2012, Stéphane Glondu <steph@glondu.net>
License: LGPL-2.1+ and other

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Octave Fuzzy Logic Toolkit
Upstream-Contact: L. Markowsky <lmarkov@users.sourceforge.net>
Source: http://octave.sourceforge.net/fuzzy-logic-toolkit/

Files: *
Copyright: 2011-2021 L. Markowsky <lmarkov@users.sourceforge.net>
License: GPL-3+

Files: Makefile
Copyright: 2015-2016 Carnë Draug
           2015-2016 Oliver Heimlich
           2017 Julien Bect <jbect@users.sf.net>
           2017 Olaf Till <i7tiol@t-online.de>
           2019 John Donoghue <john.donoghue@ieee.org>
License: other
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

Files: debian/*
Copyright: 2018, 2019, 2021 Rafael Laboissiere <rafael@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.

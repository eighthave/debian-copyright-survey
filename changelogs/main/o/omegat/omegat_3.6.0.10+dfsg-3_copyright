Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/omegat-org/omegat
Files-Excluded:
 gradle/wrapper/*
 lib/*
 native/*
 release/*/*.exe
 release/*/*.app
 nbproject/*.jar

Files: *
Copyright:
 2000-2007 Keith Godfrey, Maxym Mykhalchuk,
           Henry Pijffers, Benjamin Siband,
           Sandra Jean Chua, Kim Bruning,
           Martin Wunderlich, Didier Briel and Thomas Huriaux.
 2008-2011 Didier Briel
 2007      Zoltan Bartko
 2007      Tiago Saboga
 2007-2011 Alex Buloichik
 2007-2010 Martin Fleurke
 2008      Fabian Mandelbaum
 2008      Josef Molnár (translation)
 2007-2011 Jean-Christophe Helary (translation)
 2008      Dick Groskamp (translation)
 2008      Dragomir Kovačević (translation)
 2008      Vito Smolej (translation)
 2006-2009 Martin Wunderlich
 2008      Andrzej Sawula
 2009      Arno Peters
 2009      Guido Leenders
 2010      Antonio Vilei
 2010      Wildrich Fourie
 2010      Volker Berlin
 1996,2005 The Free Software Foundation
 2010      Rashid Umarov
 2010      Ibai Lakunza Velasco
 2011      Valter Mura (italian translation)
 2011      Thelma L Sabim (brazilian portuguese translation)
 2011      John Moran
Comment:
 The user manual is the joint work of Jean-Christophe Helary, Samuel
 Murray, Maxym Mykhalchuk, Henry Pijffers and Vito Smolej.
License: GPL-3+

Files: debian/*
Copyright:
 2006-2011, Tiago Saboga <tiagosaboga@gmail.com>
License: GPL-3+

Files: debian/patches/02-add-bmsi.util.Diff.patch
Copyright:
 2000 Business Management Systems, Inc.
Comment:
 Author: Stuart D. Gathman, translated from GNU diff 1.15
 .
 Downloaded from: http://www.bmsi.com/java/Diff.java
License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian GNU/Linux systems, the complete text of the version 1
 of the GNU General Public License can be found in
 /usr/share/common-licenses/GPL-1.

Files:
 doc_src/en/*
 docs/en/*
Copyright:
 2013 Vito Smolej
 2014—2016 Vincent Bidaux
 2005—2012 Samuel Murray
License: GPL-3+
 The documentation is a free document; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as published
 by the Free Software Foundation; either version 3 of the License, or (if
 you prefer) any later version.
 .
 The documentation is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 .
 On Debian GNU/Linux systems, the complete text of the version 2
 of the GNU General Public License can be found in
 /usr/share/common-licenses/GPL-3.

Files: src/schemas/srx20.xsd
Copyright:
 The Localisation Industry Standards Association [LISA] 2006.
Comment:
 The file header states "All Rights Reserved", but according to
 the LISA website (as of 2011-06-07), all LISA Standards have been
 transferred to the public domain.
 .
 "As of 2011 February 28, the Localization Industry Standards
 Association (LISA) was declared insolvent. The results of the
 Extraordinary General Assembly meeting and ballot of 2011 April
 21 LISA has formallized the dissolution of the association and
 the transfer of the LISA Standards to the public domain.
 .
 The company Société fiduciaire Zemp & Associés, SàrL in Geneva
 is designated as the liquidator of the association’s assets."
License: public-domain

License: GPL-3+
 OmegaT is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 OmegaT is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public
 License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian GNU/Linux systems, the complete text of the version 3
 of the GNU General Public License can be found in
 /usr/share/common-licenses/GPL-3.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OmniDB
Source: https://github.com/OmniDB/OmniDB
Files-Excluded: OmniDB/deploy.old/lib
 OmniDB/OmniDB_app/tests.old

Files: *
Copyright:
 Portions Copyright (c) 2015-2018, The OmniDB Team
 Portions Copyright (c) 2017-2018, 2ndQuadrant Limited
License: Expat

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/css/explain/font-awesome.min.css
 OmniDB/OmniDB_app/static/OmniDB_app/fa/css/*
Copyright: (c) 2012-2018, Dave Gandy <drgandy@alum.mit.edu>
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/fa/webfonts/*
Copyright: (c) 2012-2018, Dave Gandy <drgandy@alum.mit.edu>
License: OFL-1.1
Comment:
 Upstream doesn't specify which version of OFL instead simply links to
 OFL page. Hence latest version of OFL is considered.

Files: OmniDB/OmniDB_app/static/OmniDB_app/fonts/RobotoMono/*
Copyright: Copyright 2015 Google Inc.
License: Apache-2.0

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/css/msdropdown/*
 OmniDB/OmniDB_app/static/OmniDB_app/images/msdropdown/*
 OmniDB/OmniDB_app/static/OmniDB_app/js/jquery.dd.min.js
Copyright: (c) Marghoob Suleman
License: msdropdown
 msDropDown is free jQuery Plugin: you can redistribute it and/or modify
 it under the terms of the either the MIT License or the Gnu General Public License (GPL) Version 2
 .
 On Debian systems, a copy of the GPL 2 is located in
 /usr/share/common-licenses/GPL-2.

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/lib/aimaraJS/*
 OmniDB/OmniDB_app/static/OmniDB_app/lib/tabs/lib/tabs.js
Copyright: Copyright 2015-2017 The OmniDB Team
License: GPL-3
 OmniDB is free software: you can redistribute it and/or modify it under the
 terms of the GNU General Public License as published by the Free Software
 Foundation, either version 3 of the License, or (at your option) any later
 version.
 .
 On Debian systems, a copy of the GPL 3 is located in
 /usr/share/common-licenses/GPL-3.

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/cytoscape/*
Copyright: Copyright (c) The Cytoscape Consortium
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/emojionearea/*
Copyright: Copyright Andrey Izman and other contributors
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/jquery/jquery.min.js
Copyright: Copyright JS Foundation and other contributors
License: Expat

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/css/jquery-ui*
 OmniDB/OmniDB_app/static/OmniDB_app/js/jquery-ui*
 OmniDB/OmniDB_app/static/OmniDB_app/lib/jquery-ui/*
Copyright: Copyright jQuery Foundation and other contributors
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/jqplot/*
Copyright: (c) 2009-2013 Chris Leonello
License: jqplot
 jqPlot is currently available for use in all personal or commercial projects
 under both the MIT and GPL version 2.0 licenses. This means that you can
 choose the license that best suits your project and use it accordingly.
 .
 The author would appreciate an email letting him know of any substantial
 use of jqPlot.  You can reach the author at: chris at jqplot dot com
 or see http://www.jqplot.com/info.php.  This is, of course, not required.
 .
 If you are feeling kind and generous, consider supporting the project by
 making a donation at: http://www.jqplot.com/donate.php.
 .
 sprintf functions contained in jqplot.sprintf.js by Ash Searle:
 .
     version 2007.04.27
     author Ash Searle
     http://hexmen.com/blog/2007/03/printf-sprintf/
     http://hexmen.com/js/sprintf.js
     The author (Ash Searle) has placed this code in the public domain:
     "This code is unrestricted: you are free to use it however you like."

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/moment/moment.min.js
Copyright: Tim Wood, Iskren Chernev, Moment.js contributors
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/popupJS/*
Copyright: Copyright (c) 2017 Israel Barth Rubio
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/ace/*
Copyright: Copyright (c) 2010, Ajax.org B.V.
License: bsd-ajax
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of Ajax.org B.V. nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL AJAX.ORG B.V. BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/Chart.*
Copyright:
 Copyright 2017 Nick Downie
 Copyright 2018 Chart.js Contributors
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/chartist*
Copyright: Copyright © 2017 Gion Kunz
License: WTFPL-or-Expat
 Free to use under either the WTFPL license or the MIT license.
 .
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004
 .
 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 .
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
  0. You just DO WHAT THE FUCK YOU WANT TO.

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/chartjs-plugin-annotation.min.js
Copyright:
 Copyright 2016 Evert Timberg
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/chartist-plugin-axistitle.min.js
Copyright: Copyright (c) 2019 Alex Stanbury
License: Expat

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/css/handsontable.full.css
 OmniDB/OmniDB_app/static/OmniDB_app/js/handsontable.full.js
Copyright:
 Copyright (c) 2012-2014 Marcin Warpechowski
 Copyright (c) 2015 Handsoncode sp. z o.o. <hello@handsoncode.net>
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/flatpickr*.js
Copyright: Copyright (c) 2017 Gregory Petrosyan
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/lib/bootstrap/bootstrap.min.js
Copyright: Copyright 2011-2019 The Bootstrap Authors
License: Expat

Files:
 OmniDB/OmniDB_app/static/OmniDB_app/js/popper*.js
 OmniDB/OmniDB_app/static/OmniDB_app/lib/popper/popper.min.js
 OmniDB/OmniDB_app/static/OmniDB_app/js/tooltip*.js
Copyright: Copyright (c) Federico Zivolo and contributors
License: Expat

Files: OmniDB/OmniDB_app/static/OmniDB_app/js/xterm.js
Copyright:
 Copyright (c) 2017-2019, The xterm.js authors (https://github.com/xtermjs/xterm.js)
 Copyright (c) 2014-2016, SourceLair Private Company (https://www.sourcelair.com)
 Copyright (c) 2012-2013, Christopher Jeffrey (https://github.com/chjj/)
License: Expat

Files: OmniDB/OmniDB_app/include/custom_paramiko_expect.py
Copyright: Copyright (c) 2013 Fotis Gimian
License: Expat

Files: OmniDB/OmniDB_app/include/Spartacus/Utils.py
Copyright: Copyright (c) 2014-2018 William Ivanski
License: Expat

Files: OmniDB/OmniDB_app/include/Spartacus/prettytable.py
Copyright: Copyright (c) 2009-2013, Luke Maurits <luke@maurits.id.au>
License: prettytable
 All rights reserved.
 With contributions from:
  * Chris Clark
  * Klein Stephane
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: Copyright (C) The Debian Project
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Apache-2.0
 On Debian systems, a copy of the Apache 2.0 license is located in
 /usr/share/common-licenses/Apache-2.0.

License: OFL-1.1
 PREAMBLE
  The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font creation
 efforts of academic and linguistic communities, and to provide a free and
 open framework in which fonts may be shared and improved in partnership
 with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded, 
 redistributed and/or sold with any software provided that any reserved
 names are not used by derivative works. The fonts and derivatives,
 however, cannot be released under any other type of license. The
 requirement for fonts to remain under this license does not apply
 to any document created using the fonts or their derivatives.
 .
 DEFINITIONS
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software components as
 distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to, deleting,
 or substituting -- in part or in whole -- any of the components of the
 Original Version, by changing formats or by porting the Font Software to a
 new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
  Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed, modify,
 redistribute, and sell modified and unmodified copies of the Font
 Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
    in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
    redistributed and/or sold with any software, provided that each copy
    contains the above copyright notice and this license. These can be
    included either as stand-alone text files, human-readable headers or
    in the appropriate machine-readable metadata fields within text or
    binary files as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
    Name(s) unless explicit written permission is granted by the corresponding
    Copyright Holder. This restriction only applies to the primary font
    name as presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
    Software shall not be used to promote, endorse or advertise any
    Modified Version, except to acknowledge the contribution(s) of the
    Copyright Holder(s) and the Author(s) or with their explicit written
    permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
    must be distributed entirely under this license, and must not be
    distributed under any other license. The requirement for fonts to
    remain under this license does not apply to any document created
    using the Font Software.
 .
 TERMINATION
  This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
  THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.

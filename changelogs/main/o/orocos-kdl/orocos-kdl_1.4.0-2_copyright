Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: orocos-kdl
Upstream-Contact: Orocos Developers <orocos-dev@lists.mech.kuleuven.be>
Source: https://github.com/orocos/orocos_kinematics_dynamics

Files: *
Copyright: 2007-2015 Ruben Smits <ruben@intermodalics.eu>
License: LGPL-2.1+

Files: debian/*
Copyright: 2015 Leopold Palomo-Avellaneda <leo@alaxarxa.net>
License: GPL-2+

Files: orocos_kdl/examples/chainiksolverpos_lma_demo.* 
      orocos_kdl/src/chainiksolverpos_lma.*
      orocos_kdl/src/frames.*
      orocos_kdl/src/frames_io.*
      orocos_kdl/src/path_circle.cpp
      orocos_kdl/src/path_composite.cpp
      orocos_kdl/src/path.*
      orocos_kdl/src/path_cyclic_closed.hpp
      orocos_kdl/src/path_line.*
      orocos_kdl/src/path_point.*
      orocos_kdl/src/path_roundedcomposite.*
      orocos_kdl/src/rotational_interpolation.*
      orocos_kdl/src/rotational_interpolation_sa.*
      orocos_kdl/src/trajectory*
      orocos_kdl/src/utilities/error*
      orocos_kdl/src/velocityprofile_*
Copyright: 2004-2012 Erwin Aertbelien 
License: LGPL-2.1+

Files: orocos_kdl/src/kdl.hpp
Copyright: 2007 Ruben Smits <ruben@intermodalics.eu>
           2011 Erwin Aertbelien <Erwin.Aertbelien@mech.kuleuven.be> 
License: LGPL-2.1+

Files: orocos_kdl/src/velocityprofile_dirac.*
Copyright: 2005 Peter Soetens <peter@thesourceworks.com>
License: LGPL-2.1+

Files: orocos_kdl/src/treefksolverpos_recursive.*
      orocos_kdl/src/treefksolver.*
Copyright: 2007 Ruben Smits <ruben@intermodalics.eu>
           2008 Julia Jesse
License: LGPL-2.1+

Files: orocos_kdl/src/chainfksolverpos_recursive.*
       orocos_kdl/src/chainfksolvervel_recursive.*
Copyright: 2007 Ruben Smits <ruben.smits@mech.kuleuven.be>
                Francois Cauwe <francois@cauwe.org>
License: LGPL-2.1+

Files: orocos_kdl/src/treeiksolverpos_nr_jl.*
Copyright: 2008 Mikael Mayer
           2008 Julia Jesse
           2007-2008 Ruben Smits <ruben.smits@mech.kuleuven.be>
License: LGPL-2.1+

Files: orocos_kdl/src/treeiksolverpos_online.*
Copyright: 2007-2008 Ruben Smits <ruben dot smits at mech dot kuleuven dot be>
           2011 PAL Robotics S.L. All rights reserved
           2008 Mikael Mayer
           2008 Julia Jesse
License: LGPL-2.1+

Files: orocos_kdl/src/chainidsolver_vereshchagin.*
Copyright: 2009-2011 Ruben Smits <ruben.smits@mech.kuleuven.be>
                     Herman Bruyninckx 
                     Azamat Shakhimardanov
License: LGPL-2.1+

Files: orocos_kdl/src/chaindynparam.*
      orocos_kdl/src/jntspaceinertiamatrix.*
Copyright: 2009 Dominick Vanthienen <dominick.vanthienen@mech.kuleuven.be>
License: LGPL-2.1+

Files: orocos_kdl/src/chainjnttojacdotsolver.*
Copyright: 2015  Antoine Hoarau <hoarau [at] isir.upmc.fr>
License: LGPL-2.1+

Files: orocos_kdl/src/solveri.hpp
Copyright: 2013 Stephen Roderick <kiwi.net@mac.com>
License: LGPL-2.1+

Files: orocos_kdl/tests/test-runner.cpp
Copyright: 2005 Peter Soetens 
           2007 Klaas Gadeyne
License: GPL-2+

Files: python_orocos_kdl/PyKDL/std_string.sip
Copyright: 2005  Torsten Marek <shlomme at gmx.net>
License: LGPL-2.1+

Files: python_orocos_kdl/cmake/*
Copyright: 2007 Simon Edwards <simon@simonzone.com>
License: BSD-3-clause

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or   
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 Orocos-kdl is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 Orocos-kdl is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with Orocos-kdl; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in "/usr/share/common-licenses/LGPL-2.1".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
  * Neither the name of copyright holder nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

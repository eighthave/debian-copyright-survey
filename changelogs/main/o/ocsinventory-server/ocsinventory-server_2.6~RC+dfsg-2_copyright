Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OCS Inventory NG Server
Upstream-Contact: Frank Bourdeau <frank.bourdeau@ocsinventory-ng.org>
Source: https://www.ocsinventory-ng.org
Files-Excluded:
	ocsreports/libraries/bootstrap
	ocsreports/libraries/charts.js
	ocsreports/libraries/jquery
	ocsreports/libraries/jquery-fileupload
	ocsreports/libraries/jquery-migrate-1
	ocsreports/libraries/select2
	ocsreports/files/oui.txt
	ocsreports/js/dataTables.bootstrap.js
	ocsreports/libraries/datatable
	ocsreports/libraries/phpcas
	ocsreports/libraries/tclib

Files: *
Copyright: 2005-2016 - OCS Inventory Development Team
License: GPL-2+

Files: ocsreports/js/dataTables.conditionalPaging.js
Copyright: 2015, Matthew Hasbach <https://github.com/mjhasbach>
License: Expat

Files: ocsreports/libraries/PHPMailer*
Copyright: 2010-2017, Marcus Bointon <phpmailer@synchromedia.co.uk>
 2010-2012, Jim Jagielski <jimjag@gmail.com>
 2004-2009, Andy Prevost <codeworxtech@users.sourceforge.net>
License: LGPL-2.1

Files: debian/*
Copyright: 2008-2015, Pierre Chifflier <pollux@debian.org>
           2014-2016, Jean-Michel Vourgère <nirgal@debian.org>
	   2018, Xavier Guimard <yadd@debian.org>
License: GPL-2+

Files: ocsreports/css/winclassic.css
Copyright: 2002, 2003, 2006 Erik Arvidsson
License: Apache-2.0

Files: ocsreports/js/datetimepicker.js
Copyright: 2003, TengYong Ng
License: Rainforestnet

Files: ocsreports/libraries/password_compat/*
Copyright: 2012, Anthony Ferrara
License: Expat

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian GNU/Linux systems, the complete text of the Apache
 License 2.0 can be found in `/usr/share/common-licenses/Apache-2.0'.

License: Rainforestnet
 Permission given to use this script in ANY kind of applications if
 header lines are left unchanged.
 .
 Website: https://www.rainforestnet.com
 Contact: contact@rainforestnet.com

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 The complete text of the GNU Lesser General Public License version 2.1
 can be found in `/usr/share/common-licenses/LGPL-2.1'.

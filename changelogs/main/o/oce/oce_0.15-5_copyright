Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OCE
Upstream-Contact: oce-dev@googlegroups.com
Source: https://github.com/tpaviot/oce/tags
Comment:
 This is based on OCE (OpenCASCADE Community Edition), fork
 of OpenCASCADE Technology (OCCT).
 .
 OCCT 6.7.0 (released on December 2013) has been relicensed to LGPL 2.1,
 with an exception clause to allow embedding header files in derived work.
Debianized-By: Adam C. Powell, IV <hazelsct@debian.org>
Debianized-Date: Wed, 02 Jan 2008 16:32:57 -0500

Files: *
Copyright: Copyright (C) 1991-2000 by Matra Datavision SA., 
 Copyright (C) 2001-2013 Open CASCADE S.A.S.
License: LGPL-2.1 with OpenCASCADE exception
 Open CASCADE Technology is free software; you can redistribute it and / or
 modify it under the terms of the GNU Lesser General Public version 2.1 as
 published by the Free Software Foundation, with special exception defined in
 the file OCCT_LGPL_EXCEPTION.txt (text reproduced below).
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
 .
 Alternatively, Open CASCADE Technology may be used under the terms of Open
 CASCADE commercial license or contractual agreement.
 .
 Text of OCCT_LGPL_EXCEPTION.txt follows:
 .
 Open CASCADE exception (version 1.0) to GNU LGPL version 2.1:
 .
 The object code (i.e. not a source) form of a "work that uses the Library"
 can incorporate material from a header file that is part of the Library.
 As a special exception to the GNU Lesser General Public License version 2.1,
 you may distribute such object code incorporating material from header files
 provided with the Open CASCADE Technology libraries (including code of CDL
 generic classes) under terms of your choice, provided that you give
 prominent notice in supporting documentation to this code that it makes use
 of or is based on facilities provided by the Open CASCADE Technology software.

Files: src/NCollection/NCollection_StdAllocator.hxx
Copyright: Copyright (C), Roman Lygin, 2012
License: public-domain
 This file is put into Public Domain and thus can freely be used for any purpose.
 The author disclaims any rights and liabilities.

Files: src/OSD/gettime_osx.h
Copyright: Copyright (C), by MM Weiss
License: BSD-3-clause

Files: src/OpenGl/OpenGl_glext.h
Copyright: Copyright (c) 2013 The Khronos Group Inc.
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and/or associated documentation files (the
 "Materials"), to deal in the Materials without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Materials, and to
 permit persons to whom the Materials are furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Materials.
 .
 THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
Comment: This is the text of the Expat license, except that "Software"
 had been replaced by "Materials"

Files: src/Standard/Standard_StdAllocator.hxx
Copyright: Copyright (C), Roman Lygin, 2011
License: public-domain
 This file is put into Public Domain and thus can freely be used for any purpose.
 The author disclaims any rights and liabilities.

Files: test/gtest-1.7.0/*
Copyright: Copyright 2008, Google Inc.
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
     * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ompl
Upstream-Contact: Mark Moll <mmoll@rice.edu>, Ioan Sucan <isucan@gmail.com>
Source: http://ompl.kavrakilab.org/
Files-Excluded: src/external/*

Files: *
Copyright: 2003-2006, Gino van den Bergen / Erwin Coumans http://continuousphysics.com/Bullet
           2008-2020, Rice University
           2008-2018, Willow Garage, Inc
           2013, Autonomous Systems Laboratory, Stanford University
           2013-2015, Rutgers the State University of New Jersey, New Brunswick
           2013-2016, University of Colorado, Boulder
           2014, JSK, The University of Tokyo
           2014-2015, University of Toronto
           2015, Caleb Voss and Wilson Beebe
           2015, Tel Aviv University
           2016, Georgia Institute of Technology
           2017, National Institute of Advanced Industrial Science
           2019, PickNik LLC
           2019, Robert Bosch GmbH
           2019-2020, University of Oxford
           2019, University of Stuttgart
License: BSD-3-clause

Files: debian/*
Copyright: 2013-2020, Leopold Palomo-Avellaneda <leo@alaxarxa.net>
License: BSD-3-clause
Comment: the Debian packaging is licensed under the same terms as the original package.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.


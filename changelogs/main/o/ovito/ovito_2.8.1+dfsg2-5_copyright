Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://ovito.svn.sourceforge.net/svnroot/ovito/tags/
Files-Excluded:
  src/3rdparty/botan/botan*
  src/3rdparty/botan/conf*
  src/3rdparty/botan/doc
  src/3rdparty/muparser/mu*
  src/3rdparty/muparser/Ch*
  src/3rdparty/muparser/L*
  src/3rdparty/pybind11/*
  src/3rdparty/qwt/qw*
  src/3rdparty/voro++/html/jquery.js
Upstream-Author: Alexander Stukowski

Files: *
Copyright: 2007-2016 Alexander Stukowski
License: GPL-2+

Files: doc/manual/*
Copyright: 2007-2010 Alexander Stukowski
License: GFDL-1.2

Files: src/3rdparty/qssh/*
Copyright: 2015 The Qt Company Ltd.
License: LGPL-2.1 or LGPL-3

Files: src/3rdparty/copr/qcprot/*
Copyright: 2009-2013, Pu Liu and Douglas L. Theobald
License: BSD-3-clause

Files: src/3rdparty/geogram/*
Copyright: 2012-2014, Bruno Levy
License: BSD-3-clause

Files: src/3rdparty/ptm/*
Copyright: 2016, PM Larsen
License: Expat

Files: src/3rdparty/tachyon/*
Copyright: 1994-2013 John E. Stone
License: BSD-3-clause

Files: src/3rdparty/voro++/*
Copyright: 2008 The Regents of the University of California, through
 Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy).
License: BSD-3-clause

License: BSD-3-clause
 The BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution. 
 .
   * Neither the name of foo nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
 .
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
 .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GFDL-1.2
  The documentation is licensed under the
  terms of GNU Free Documentation License, Version 1.2 or any later
  version published by Free Software Foundation.  On Debian systems, the
  complete text of the license can be found in
  "/usr/share/common-licenses/GFDL-1.2".

License: GPL-2+
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
 .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 .
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
  On Debian systems, the complete text of the GNU General
  Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1
  On Debian systems, the complete text of the GNU Lesser 
  General Public License version 2.1 can be found in 
  "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3
  On Debian systems, the complete text of the GNU Lesser 
  General Public License version 3 can be found in 
  "/usr/share/common-licenses/LGPL-3".

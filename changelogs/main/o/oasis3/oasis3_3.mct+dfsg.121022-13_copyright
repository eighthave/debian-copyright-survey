This version of OASIS was packaged for Debian by Alastair McKinstry,
<mckinstry@debian.org>, on 4 August 2009.

OASIS is available at https://oasistrac.cerfacs.fr/wiki

Copyright © 2006 Centre Européen de Recherche et Formation Avancée 
en Calcul Scientifiq(CERFACS).  This software and ancillary information 
called OASIS3 is free software. 
CERFACS has rights to use, reproduce, and distribute OASIS3. 

The public may copy, distribute, use, prepare derivative works and publicly 
display OASIS3 under the terms of the Lesser GNU General Public License (LGPL)
as published by the Free Software Foundation, provided that this notice and 
any statement of authorship are reproduced on all copies. If OASIS3 is modified 
to produce derivative works, such modified software should be clearly marked,
so as not to confuse it with the OASIS3 version available from CERFACS. 

The developers of the OASIS3 software are researchers attempting to build a modular 
and user-friendly coupler accessible to the climate modelling community. 
Although we use the tool ourselves and have made every effort to ensure its accuracy,
we can not make any guarantees. We provide the software to you for free. 
In return, you--the user--assume full responsibility for use of the software. 
The OASIS3 software comes without any warranties (implied or expressed) and 
is not guaranteed to work for you or on your computer. 
Specifically, CERFACS and the various individuals involved in development 
and maintenance of the OASIS3 software are not responsible for any damage 
that may result from correct or incorrect use of this software. 

A copy of the LGPL is available at /usr/share/common-licenses/LGPL-2

OASIS3 uses the SCRIP library, which is covered by the following license:

!     This software and ancillary information (herein called software) 
!     called SCRIP is made available under the terms described here.  
!     The software has been approved for release with associated 
!     LA-CC Number 98-45.
!
!     Unless otherwise indicated, this software has been authored
!     by an employee or employees of the University of California,
!     operator of the Los Alamos National Laboratory under Contract
!     No. W-7405-ENG-36 with the U.S. Department of Energy.  The U.S.
!     Government has rights to use, reproduce, and distribute this
!     software.  The public may copy and use this software without
!     charge, provided that this Notice and any statement of authorship
!     are reproduced on all copies.  Neither the Government nor the
!     University makes any warranty, express or implied, or assumes
!     any liability or responsibility for the use of this software.
!
!     If software is modified to produce derivative works, such modified
!     software should be clearly marked, so as not to confuse it with 
!     the version available from Los Alamos National Laboratory.


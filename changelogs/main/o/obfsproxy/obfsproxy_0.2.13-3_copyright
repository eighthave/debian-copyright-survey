Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: obfsproxy
Upstream-Contact: George Kadianakis <desnacked@riseup.net>
Source: https://git.torproject.org/pluggable-transports/obfsproxy.git

Files: *
Copyright: Copyright 2012      Brandon Wiley <brandon@blanu.net>
           Copyright 2012-2013 George Kadianakis <desnacked@riseup.net>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in
      the documentation and/or other materials provided with the
      distribution.
 .
    * Neither the names of the copyright owners nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: Copyright 2013-2014 Jérémy Bobbio <lunar@debian.org>
           Copyright 2014 Anonymity Tools Debian Maintainers <pkg-anonymity-tools@lists.alioth.debian.org>
License: permissive
 Copying and distribution of this package, with or without
 modification, are permitted in any medium without royalty
 provided the copyright notice and this notice are
 preserved.

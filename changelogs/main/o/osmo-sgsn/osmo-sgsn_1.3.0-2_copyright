Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: osmo-sgsn
Source: git://git.osmocom.org/osmo-sgsn


Files: *
Copyright: 2008-2015 Holger Hans Peter Freyther <zecke@selfish.org>
           2008-2016 Harald Welte <laforge@gnumonks.org>
           2010-2014 On-Waves
           2011-2017 sysmocom s.f.m.c. GmbH <info@sysmocom.de>
           2013 Jacob Erlbeck <jerlbeck@sysmocom.de>
License:   AGPL-3+

Files:     osmoappdesc.py
           tests/ctrl_test_runner.py
           tests/vty_test_runner.py
Copyright: 2013         Katerina Barone-Adesi <kat.obsc@gmail.com>
           2013         Jacob Erlbeck <jerlbeck@sysmocom.de>
           2013-2014    Holger Hans Peter Freyther <zecke@selfish.org>
License:   GPL-3+

Files:     git-version-gen
Copyright: 2007-2010 Free Software Foundation, Inc.
License:   GPL-3+

Files:     src/gprs/v42bis.c
           include/osmocom/sgsn/v42bis.h
           include/osmocom/sgsn/v42bis_private.h
Copyright: 2005-2011 Steve Underwood
License:   LGPL-2.1

Files:     src/gprs/slhc.c
           include/osmocom/sgsn/slhc.h
Copyright: 1989 Regents of the University of California.
License:   BSD-prior

Files:     m4/ax_check_compile_flag.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2011 Maarten Bosmans <mkbosmans@gmail.com>
License:   GPL-3+_with_autoconf_exception

Files:     debian/*
Copyright: 2017 Alexander Couzens <lynxis@fe80.eu>
           2017 Harald Welte <laforge@gnumonks.org>
           2018 Pau Espin Pedrol <pespin@sysmocom.de>
           2018 Ruben Undheim
License:   AGPL-3+


License: AGPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version 2.1,
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 The FSF address in the above text is the old one.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 Version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.


License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


License: GPL-3+_with_autoconf_exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.


License: BSD-prior
 Redistribution and use in source and binary forms are permitted
 provided that the above copyright notice and this paragraph are
 duplicated in all such forms and that any documentation,
 advertising materials, and other materials related to such
 distribution and use acknowledge that the software was developed
 by the University of California, Berkeley.  The name of the
 University may not be used to endorse or promote products derived
 from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

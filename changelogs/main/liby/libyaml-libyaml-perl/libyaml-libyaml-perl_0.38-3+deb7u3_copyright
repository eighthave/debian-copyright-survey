Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: YAML-LibYAML
Upstream-Contact: Ingy döt Net <ingy@cpan.org>
Source: http://search.cpan.org/dist/YAML-LibYAML/

Files: *
Copyright: 2007-2012, Ingy döt Net <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Spiffy.pm
Copyright: 2006, Ingy döt Net <ingy@cpan.org>
 2004, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/Base.pm inc/Test/Base/*
Copyright: 2005-2009, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/More.pm
Copyright: 2001-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: inc/Test/Builder.pm inc/Test/Builder/*
Copyright: 2002-2008, chromatic <chromatic@wgz.org>
 2002-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: LibYAML/ppport.h
Copyright: 2004-2009, Marcus Holland-Moritz <mhx-cpan@gmx.net>
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files:
 LibYAML/api.c
 LibYAML/config.h
 LibYAML/dumper.c
 LibYAML/emitter.c
 LibYAML/loader.c
 LibYAML/parser.c
 LibYAML/reader.c
 LibYAML/scanner.c
 LibYAML/writer.c
 LibYAML/yaml.h
 LibYAML/yaml_private.h
Copyright: 2006, Kirill Simonov
License:
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: debian/*
Copyright: 2009, Ryan Niebur <ryan@debian.org>
 2010, Jonathan Yu <jawnsy@cpan.org>
 2010, Krzysztof Krzyżaniak (eloy) <eloy@debian.org>
 2011-2012, gregor herrmann <gregoa@debian.org>
 2012, Julián Moreno Patiño <darkjunix@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.


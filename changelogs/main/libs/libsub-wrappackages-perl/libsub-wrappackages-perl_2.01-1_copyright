Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Sub-WrapPackages
Upstream-Contact: David Cantrell <pause@barnyard.co.uk>
Source: https://metacpan.org/release/Sub-WrapPackages

Files: *
Copyright: 2003-2009, David Cantrell <david@cantrell.org.uk>
License: Artistic or GPL-2+

Files: debian/*
Copyright: 2008, Jaldhar H. Vyas <jaldhar@debian.org>
 2009-2010, Jonathan Yu <jawnsy@cpan.org>
 2011-2017, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

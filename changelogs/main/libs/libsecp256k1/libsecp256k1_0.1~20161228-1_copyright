Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: https://github.com/bitcoin/secp256k1/issues
Upstream-Name: secp256k1
Source: https://github.com/bitcoin/secp256k1
 git://github.com/bitcoin/secp256k1

Files:
 contrib/*.c
 contrib/*.h
 src/*.c
 src/*.h
Copyright:
  2013-2014, Diederik Huys
  2013-2015, Pieter Wuille <pieter.wuille@gmail.com>
  2015, Andrew Poelstra
License-Grant:
 Distributed under the MIT software license, see the accompanying file
 COPYING or http://www.opensource.org/licenses/mit-license.php.
License: Expat

Files:
 autogen.sh
 build-aux/m4/bitcoin_secp.m4
 configure.ac
 COPYING
 */.gitignore
 include/*.h
 libsecp256k1.pc.in
 Makefile.am
 */Makefile.am.include
 README.md
 sage/*.sage
 src/java/*.c
 src/java/*.h
 src/java/*.java
 TODO
 .travis.yml
Copyright: None
License: None
Comment:
 These files seem authored by main upstream authors, and copyright and
 license is therefore assumed (to the extend required) to be same as
 generally for this project (see topmost Files section).

Files:
 src/java/org/bitcoin/NativeSecp256k1.java
 src/java/org/bitcoin/NativeSecp256k1Util.java
 src/java/org/bitcoin/Secp256k1Context.java
Copyright: 2013, Google Inc.
  2014-2016, the libsecp256k1 contributors
License-Grant:
 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License. You may obtain
 a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
License: Apache-2.0

Files: build-aux/m4/ax_*.m4
Copyright:
  2008, Don Anderson <dda@sleepycat.com>
  2008, Paolo Bonzini <bonzini@gnu.org>
License: FSFAP

Files: src/asm/field_10x26_arm.s
Copyright: 2014, Wladimir J. van der Laan
License: Expat

Files: debian/*
Copyright: 2015-2016, Jonas Smedegaard <js@debian.org>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+

Files: debian/patches/*
Copyright: 2016-2017, Jonas Smedegaard <js@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.

License: Apache-2.0
License-Reference: /usr/share/common-licenses/Apache-2.0

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libStatGen
Upstream-Contact: Mary Kate Wing <mktrost at umich.edu>
Source: https://genome.sph.umich.edu/wiki/C++_Library:_libStatGen

Files: *
Copyright: 2010-2018 Mary Kate Wing
License: GPL-3+

Files: bam/*
       fastq/*
       general/*
       glf/*
       vcf/*
Copyright: 2010-2013 Regents of the University of Michigan, Hyun Min Kang, Matthew Flickenger, Matthew Snyder and Goncalo Abecasis
License: GPL-3+

Files: general/Random.cpp
       general/Random.h
Copyright: 1997-2002 Makoto Matsumoto and Takuji Nishimura (MERSENNE TWISTER CODE)
           2010 Regents of the University of Michigan
License: BSD-3-clause and GPL-3+

Files: general/Performance.h
       general/test/gtest.cpp
Copyright: 2009 Regents of the University of Michigan
License: Expat

Files: general/IntHash.cpp
       general/IntHash.h
Copyright: 2000-2007 Goncalo Abecasis
License: GPL-3+

Files: general/CSG_MD5.h
Copyright: 1990-1992, RSA Data Security, Inc.
License: RSA-MD5

Files: samtools/*
Copyright: 2008 Broad Institute / Massachusetts Institute of Technology
           2008-2010 Genome Research Ltd (GRL)
           2008-2011 Attractive Chaos <attractor at live.co.uk>
           2008-2011 Heng Li <lh3 at sanger.ac.uk> / Bob Handsaker / Mary Kate Wing / SAMtools and htslib developers
License: Expat
Comment: Based on samtools version 981 (retrieved 7/26/11).

Files: debian/*
Copyright: 2018 Dylan Aïssi <bob.dybian@gmail.com>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and\/or other materials provided with the
    distribution.
 .
    Neither the name of the <ORGANIZATION> nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and\/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: RSA-MD5
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Statistics::Linefit
Upstream-Contact: Richard Anderson <cpan@richardanderson.org>
Source: https://metacpan.org/release/Statistics-LineFit
License: GPL-1+ or Artistic

Files: *
Copyright: 2004, Richard Anderson <cpan@richardanderson.org>
License: GPL-1+ or Artistic
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: debian/*
Copyright: 2012, Harlan Lieberman-Berg <H.LiebermanBerg@gmail.com>
License: GPL-1+ or Artistic

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

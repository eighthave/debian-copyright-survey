Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SOAP-Lite
Upstream-Contact: Paul Kulchenko <paulclinger@yahoo.com>,
 Byrne Reese <byrne@majordojo.com>,
 Martin Kutter <MKUTTER@cpan.org>
Source: https://metacpan.org/release/SOAP-Lite

Files: *
Copyright: 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, Paul Kulchenko
 2000, Lincoln D. Stein
 2007, 2008, 2012, Martin Kutter <MKUTTER@cpan.org>
 Randy J Ray
 Byrne Reese
 2013, Fred Moyer <fred@redhotpenguin.com>
License: Artistic or GPL-1+

Files: lib/SOAP/Transport/HTTP.pm
Copyright: 2000, 2004, Paul Kulchenko
 2001, <marko.asplund@kronodoc.fi>
 2008, Martin Kutter <MKUTTER@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2001, 2002, Dirk Eddelbuettel <edd@debian.org>
 2002, Stephen Zander <gibreel@debian.org>
 2004, Ezra Pagel <ezra@cpan.org>
 2005, 2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2005, Niko Tyni <ntyni@iki.fi>
 2008, 2011, Damyan Ivanov <dmn@debian.org>
 2008, Martín Ferrari <tincho@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2007-2014, gregor herrmann <gregoa@debian.org>
 2010, 2011, Nicholas Bamber <nicholas@periapt.co.uk>
 2010, Franck Joncourt <franck@debian.org>
 2010, Jonathan Yu <jawnsy@cpan.org>
 2011, Ansgar Burchardt <ansgar@debian.org>
 2012, Xavier Guimard <x.guimard@free.fr>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

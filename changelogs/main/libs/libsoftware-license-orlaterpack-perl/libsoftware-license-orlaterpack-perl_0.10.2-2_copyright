Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Software-License-OrLaterPack
Upstream-Contact: Van de Bugger <van.de.bugger@gmail.com>
Upstream-Name: Software-License-OrLaterPack
Comment: This project documents licenses, so tools which automatically
 detect licenses in source code will likely cause false positives on
 this project.

Files: *
Copyright: 2015, Van de Bugger <van.de.bugger@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: 2020, Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

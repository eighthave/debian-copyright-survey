Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Scalar-Defer
Upstream-Contact: Audrey Tang <cpan@audreyt.org>
Source: https://metacpan.org/release/Scalar-Defer

Files: *
Copyright: 2006-2009, Audrey Tang <cpan@audreyt.org>
License: MIT

Files: inc/Module/*
Copyright: 2002-2010, Adam Kennedy <adamk@cpan.org>
 2002-2010, Audrey Tang <autrijus@autrijus.org>
 2002-2010, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2010, Jonathan Yu <jawnsy@cpan.org>
 2009, Ansgar Burchardt <ansgar@debian.org>
 2008, AGOSTINI Yves <agostini@univ-metz.fr>
 2008-2009, Rene Mayorga <rmayorga@debian.org>
 2008-2009, gregor herrmann <gregoa@debian.org>
 2011, Fabrizio Regalli <fabreg@fabreg.it>	
License: MIT

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian GNU/Linux systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

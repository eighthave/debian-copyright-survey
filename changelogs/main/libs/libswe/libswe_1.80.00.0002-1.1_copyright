Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Swiss Ephemeris AutoTools
Source: http://download.gna.org/swissephauto/libswe-1.77.00.astrodocsrc.tar.bz2
Upstream-Contact: Paul Elliott (pelliott@blackpatchpanel.com>
Disclaimer: References to the Swiss Ephemeris
 Professional License, I ignore as I am a free software packager. I
 rely on the GPL.
Comment: On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
 Astrodienst is the author of the source files (*.h and *.c) and they deserve
 the main credit.

Files: *
Copyright: 2011 Paul Elliott <pelliott@blackpatchpanel.com>
License: GPL-2+
 All the files comming from the original Swiss Ephemeris distribution
 swe_unix_src_X.XX.XX.tar.gz are copyright Dieter Koch and Alois
 Treindl and are licensed under the GPL version 2 or later as described
 by the file src/LICENSE. References to the Swiss Ephemeris
 Professional License, I ignore as I am a free software packager. I
 rely on the GPL. All the other files, (mostly having to do with
 autotools building or packaging), Are Copyright Paul Elliott 2011 and
 are licensed under the GPL version 2 or later which is included below.
 .
 The files swephprg.doc swisseph.doc are also copyright Dieter Koch and
 Alois an licensed under the GPL version 2 or later as described by the
 file src/LICENSE as confirmed by confirm-copywrite.email.
 .
 If I ever find it necessary to edit one of the original Swiss
 Ephemeris distribution files, (has not happened yet as of Fri Apr 8
 22:03:26 CDT 2011), then that file will be copyright Dieter Koch and
 Alois Treindl and Paul Elliott and it will be licensed under the GPL
 version 2 or later.
 .
 Thus every file in this distribution is copyrighted by somebody. And
 every file is licensed under the GPL version 2 or later.

Files: debian/*
Copyright: 2011,2012 Paul Elliott <pelliott@blackpatchpanel.com>
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: astrodienst/*
Copyright: 1997-2008 Astrodienst AG
           1997-2008 Dieter Koch
           1997-2008 Alois Treindl
Comment: These files were copied from
 ftp://ftp.astro.com/pub/swisseph/swe_unix_src_1.77.00.tar.gz
 Known as the Swiss Ephemeris.
 These are the main source files for the package.
 Astrodienst deserves the main credit for this package.
 All bugs related to the program source will be refered to astrodienst
 Only bug caused by autotools building or packaging will be handled
 by Paul Elliott
License: GPL-2+
 /* Copyright (C) 1997 - 2008 Astrodienst AG, Switzerland.  All rights reserved.
 .
 License conditions
 ------------------
 .
 This file is part of Swiss Ephemeris.
 .
 Swiss Ephemeris is distributed with NO WARRANTY OF ANY KIND.  No author
 or distributor accepts any responsibility for the consequences of using it,
 or for whether it serves any particular purpose or works at all, unless he
 or she says so in writing.  
 .
 Swiss Ephemeris is made available by its authors under a dual licensing
 system. The software developer, who uses any part of Swiss Ephemeris
 in his or her software, must choose between one of the two license models,
 which are
 a) GNU public license version 2 or later
 b) Swiss Ephemeris Professional License
 .
 The choice must be made before the software developer distributes software
 containing parts of Swiss Ephemeris to others, and before any public
 service using the developed software is activated.
 .
 If the developer choses the GNU GPL software license, he or she must fulfill
 the conditions of that license, which includes the obligation to place his
 or her whole software project under the GNU GPL or a compatible license.
 See http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 .
 If the developer choses the Swiss Ephemeris Professional license,
 he must follow the instructions as found in http://www.astro.com/swisseph/ 
 and purchase the Swiss Ephemeris Professional Edition from Astrodienst
 and sign the corresponding license contract.
 .
 The License grants you the right to use, copy, modify and redistribute
 Swiss Ephemeris, but only under certain conditions described in the License.
 Among other things, the License requires that the copyright notices and
 this notice be preserved on all copies.
 .
 Authors of the Swiss Ephemeris: Dieter Koch and Alois Treindl
 .
 The authors of Swiss Ephemeris have no control or influence over any of
 the derived works, i.e. over software or services created by other
 programmers which use Swiss Ephemeris functions.
 .
 The names of the authors or of the copyright holder (Astrodienst) must not
 be used for promoting any software, product or service which uses or contains
 the Swiss Ephemeris. This copyright notice is the ONLY place where the
 names of the authors can legally appear, except in cases where they have
 given special permission in writing.
 .
 The trademarks 'Swiss Ephemeris' and 'Swiss Ephemeris inside' may be used
 for promoting such software, products or services.
 */

Files: astrodocsrc/*
Copyright: 1997-2008 Astrodienst AG
           1997-2008 Dieter Koch
           1997-2008 Alois Treindl
License: GPL-2+
 /* Copyright (C) 1997 - 2008 Astrodienst AG, Switzerland.  All rights reserved.
 .
 License conditions
 ------------------
 .
 This file is part of Swiss Ephemeris.
 .
 Swiss Ephemeris is distributed with NO WARRANTY OF ANY KIND.  No author
 or distributor accepts any responsibility for the consequences of using it,
 or for whether it serves any particular purpose or works at all, unless he
 or she says so in writing.  
 .
 Swiss Ephemeris is made available by its authors under a dual licensing
 system. The software developer, who uses any part of Swiss Ephemeris
 in his or her software, must choose between one of the two license models,
 which are
 a) GNU public license version 2 or later
 b) Swiss Ephemeris Professional License
 .
 The choice must be made before the software developer distributes software
 containing parts of Swiss Ephemeris to others, and before any public
 service using the developed software is activated.
 .
 If the developer choses the GNU GPL software license, he or she must fulfill
 the conditions of that license, which includes the obligation to place his
 or her whole software project under the GNU GPL or a compatible license.
 See http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 .
 If the developer choses the Swiss Ephemeris Professional license,
 he must follow the instructions as found in http://www.astro.com/swisseph/ 
 and purchase the Swiss Ephemeris Professional Edition from Astrodienst
 and sign the corresponding license contract.
 .
 The License grants you the right to use, copy, modify and redistribute
 Swiss Ephemeris, but only under certain conditions described in the License.
 Among other things, the License requires that the copyright notices and
 this notice be preserved on all copies.
 .
 Authors of the Swiss Ephemeris: Dieter Koch and Alois Treindl
 .
 The authors of Swiss Ephemeris have no control or influence over any of
 the derived works, i.e. over software or services created by other
 programmers which use Swiss Ephemeris functions.
 .
 The names of the authors or of the copyright holder (Astrodienst) must not
 be used for promoting any software, product or service which uses or contains
 the Swiss Ephemeris. This copyright notice is the ONLY place where the
 names of the authors can legally appear, except in cases where they have
 given special permission in writing.
 .
 The trademarks 'Swiss Ephemeris' and 'Swiss Ephemeris inside' may be used
 for promoting such software, products or services.
 */


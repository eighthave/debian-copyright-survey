Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: SNMP-Info
Upstream-Contact: Eric A. Miller <emiller@cpan.org>
                  Oliver Gorwits <oliver.gorwits@oucs.ox.ac.uk>
Source: https://metacpan.org/release/SNMP-Info

Files: *
Copyright: 2002-2003, Regents of the University of California
 2003-2016, Max Baker and SNMP::Info Developers
 2008-2016, Eric Miller
License: BSD-3-clause

Files: Info.pm
Copyright: 2002-2003, Regents of the University of California
           2003-2012, Max Baker and SNMP::Info Developers
License: BSD-3-clause

Files: Info/Aggregate.pm
 Info/CiscoAgg.pm
 Info/IEEE802dot3ad.pm
Copyright: 2014, SNMP::Info Developers
License: BSD-3-clause

Files: Info/RapidCity.pm
 Info/Layer3/Fortinet.pm
Copyright: 2014, Eric Miller
License: BSD-3-clause

Files: Info/Layer2/Aironet.pm Info/Layer2/HP.pm
Copyright: 2003 Regents of the University of California
           2008-2009 Max Baker
License: BSD-3-clause

Files: Info/Layer3/C6500.pm
Copyright: 2008-2009 Max Baker
License: BSD-3-clause

Files: Info/Layer3/CiscoSwitch.pm
 Info/Layer3/Nexus.pm
Copyright: 2014 Eric Miller
License: BSD-3-clause

Files: Info/Layer3/CiscoFWSM.pm
Copyright: 2010 Brian De Wolf
License: BSD-3-clause

Files: Info/Layer3/Pf.pm
Copyright: 2010 Max Baker
License: BSD-3-clause

Files: Info/Layer1/Bayhub.pm
Copyright: 2008 Eric Miller
           2008 Max Baker
License: BSD-3-clause

Files: Info/IPv6.pm
Copyright: 2010 Jeroen van Ingen
           2010 Carlos Vicente
License: BSD-3-clause

Files: Info/Layer3/Extreme.pm
Copyright: 2002, 2003 Regents of the University of California
           2012 Eric Miller
License: BSD-3-clause

Files: Info/Layer3/Altiga.pm
Copyright: 2008 Jeroen van Ingen Schenau
License: BSD-3-clause

Files: Info/Layer3/Arista.pm
Copyright: 2008 Arista Networks, Inc.
License: BSD-3-clause

Files: Info/CiscoPower.pm Info/Layer2/Netgear.pm Info/Layer3/AlcatelLucent.pm
 Info/Layer3/C4000.pm Info/Layer3/Juniper.pm Info/Layer3/NetSNMP.pm
 Info/Layer3/Timetra.pm Info/PowerEthernet.pm
Copyright: 2008 Bill Fenner
License: BSD-3-clause

Files: Info/CiscoQOS.pm Info/CiscoRTT.pm
Copyright: 2005 Alexander Hartmaier
License: BSD-3-clause

Files: Info/CiscoStack.pm Info/Layer2/Allied.pm Info/Layer2/Baystack.pm
 Info/Layer2/Cisco.pm Info/Layer2/ZyXEL_DSLAM.pm
 Info/Layer3/Cisco.pm
Copyright: 2008 Max Baker
License: BSD-3-clause

Files: Info/CiscoStats.pm
Copyright: 2003 Regents of the University of California
           2008-2009 Max Baker and SNMP::Info Developers
License: BSD-3-clause

Files: Info/CiscoStpExtensions.pm
Copyright: 2009 Carlos Vicente
License: BSD-3-clause

Files: Info/CiscoVTP.pm Info/Entity.pm Info/Layer2.pm
Copyright: 2003 Regents of the University of California,
           2008 Max Baker
License: BSD-3-clause

Files: Info/EtherLike.pm Info/Layer1/Allied.pm Info/Layer1/Asante.pm
 Info/Layer1.pm Info/Layer2/C1900.pm Info/Layer2/C2900.pm
 Info/Layer2/Catalyst.pm Info/Layer2/HP4000.pm Info/Layer3/Aironet.pm
 Info/Layer3/Foundry.pm Info/Layer3.pm Info/MAU.pm
Copyright: 2002, 2003 Regents of the University of California
           2008 Max Baker
License: BSD-3-clause

Files: Info/FDP.pm
Copyright: 2002, 2003 Regents of the University of California
           2008 Bruce Rodger, Max Baker
License: BSD-3-clause

Files: Info/Bridge.pm Info/CDP.pm
Copyright: 2002,2003 Regents of the University of California
           2004 Max Baker
License: BSD-3-clause

Files: Info/Layer3/C3550.pm
Copyright: 2004 Regents of the University of California,
           2008-2009 Max Baker
License: BSD-3-clause

Files: t/*
Copyright: 2003-2009, Max Baker and SNMP::Info Developers
           2002, 2003 Regents of the University of California
License: BSD-3-clause

Files: DeviceMatrix.txt
Copyright: 2004 Max Baker
License: BSD-3-clause

Files: Info/AdslLine.pm
Copyright: 2009 Alexander Hartmaier
License: BSD-3-clause

Files: Info/Layer2/HPVC.pm Info/Layer3/Mikrotik.pm Info/Layer3/PacketFront.pm
Copyright: 2011 Jeroen van Ingen
License: BSD-3-clause

Files: Info/Layer2/Kentrox.pm Info/Layer3/BlueCoatSG.pm
 Info/Layer3/SonicWALL.pm
Copyright: 2011 Netdisco Project
License: BSD-3-clause

Files: Info/Layer7.pm Info/Layer7/APC.pm
Copyright: 2011 Jeroen van Ingen
License: BSD-3-clause

Files: Info/Layer3/Force10.pm
Copyright: 2012 William Bulley
License: BSD-3-clause

Files: Info/Layer2/CiscoSB.pm
Copyright: 2013 Nic Bernstein
           2008-2009 Max Baker
           2003 Regents of the University of California
License: BSD-3-clause

Files: Info/Layer3/CiscoASA.pm
 Info/Layer7/CiscoIPS.pm
Copyright: 2013 Moe Kraus
License: BSD-3-clause

Files: Info/Layer3/H3C.pm
Copyright: 2012 Jeroen van Ingen
License: BSD-3-clause

Files: Info/Layer3/Huawei.pm
 Info/Layer3/Pica8.pm
Copyright: 2013 Jeroen van Ingen
License: BSD-3-clause

Files: Info/Layer3/Lantronix.pm
Copyright: 2012 J R Binks
License: BSD-3-clause

Files: Info/MRO.pm
Copyright: 2014 The SNMP::Info Project
License: BSD-3-clause

Files: Info/Layer3/PaloAlto.pm Info/Layer3/VMware.pm
Copyright: 2014-2016, Max Kosmach
License: BSD-3-clause

Files: debian/*
Copyright: 2007-2008, Christoph Martin <christoph.martin@uni-mainz.de>
           2007, Oliver Gorwits <oliver.gorwits@oucs.ox.ac.uk>
           2007, Damyan Ivanov <dmn@debian.org>
           2008, Gunnar Wolf <gwolf@debian.org>
           2009, Nathan Handler <nhandler@debian.org>
           2011, Jotam Jr. Trejo <jotamjr@debian.org.sv>
           2011-2016, gregor herrmann <gregoa@debian.org>
           2013-2015, Florian Schlichting <fsfs@debian.org>
License: GPL-2

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the University of California, Santa Cruz nor the 
   names of its contributors may be used to endorse or promote products 
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Syntax-Highlight-Perl-Improved
Upstream-Contact: David C.Y. Liu <davidcyl@cpan.org>
Upstream-Name: Syntax-Highlight-Perl-Improved

Files: *
Copyright: 2004 David C.Y. Liu <davidcyl@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2007      Jaldhar H. Vyas <jaldhar@debian.org>
           2008-2016 gregor herrmann <gregoa@debian.org>
           2017      Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+
Comment: Without explicit statement it is assumed that the packaging
 is licensed under the same terms as the upstream code.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: django-session-security
Upstream-Contact: James Pic <jamespic@gmail.com>
Source: https://github.com/yourlabs/django-session-security/
Files-Excluded: test_project/db.sqlite
Comment: test_project/db.sqlite is unused, even in tests, and contains binary
 data such as opened session.

Files: *
Copyright: 2012-2019 James Pic <jamespic@gmail.com>
           2013 Richard "autodidacticon" Moorhead <richard.moorhead@gmail.com>
           2013 Yat "yscumc" So <yat.so@dbmi.columbia.edu>
           2013 Fábio C. Barrioneuvo da Luz <bnafta@gmail.com>
           2014 John David Giese <johndgiese@gmail.com>
           2014 Jose Antonio Martin Prieto <jantonio.martin@gmail.com>
           2014 Scott Sexton <scott.sexton@gmail.com>
           2014 Jacoor <jacek@ivolution.pl>
           2014 Aaron Krill <aaron@krillr.com>
           2014 Matt Bosworth <matt@codetastic.com>
           2014 Michał Pasternak <michal.dtz@gmail.com>
           2014 Matthew Schettler <mschettler@gmail.com>
           2014-2016 Jean-Michel Vourgère <nirgal@debian.org>
           2014 Krzysztof Woś <krzysztof.wos@gmail.com>
           2014-2015 Andrei Coman <andrei.coman@3pillarglobal.com>
           2015 Marco Fucci <info@marcofucci.com>
           2015 Pēteris Caune <cuu508@gmail.com>
           2015 Matt Follett <matt.follett@gmail.com>
           2015 Nuno Khan <nunok7@gmail.com>
           2015 John Franey <johnfraney@gmail.com>
           2015 Erik Telepovský <erik.telepovsky@gmail.com>
           2016 Michael J. Schultz <mjschultz@gmail.com>
License: MIT
Comment: That list is from the AUTHORS file. Real names, dates and email were
 copied from upstream git repository. That repository seems to indicate
 this list is incomplete.

Files: debian/*
Copyright: 2014-2019 Jean-Michel Vourgère <jmv_deb@nirgal.com>
           2016-2018 Ondřej Nový <novy@ondrej.org>
License: MIT

Files: session_security/tests/project/static/jquery.js
Copyright: 2005, 2012 jQuery Foundation, Inc. and other contributors
License: MIT

Files: docs/source/_static/default.css
Copyright: 2007-2011 the Sphinx team
License: BSD-2-clause
Comment: That license was downloaded from
 https://pypi.python.org/packages/source/S/Sphinx/Sphinx-1.3b2.tar.gz

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

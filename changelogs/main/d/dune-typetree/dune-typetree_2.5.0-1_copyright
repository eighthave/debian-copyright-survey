Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dune-typetree
Source: http://www.dune-project.org/downloadsvn.html

Files: *
Copyright:
 2009          Peter Bastian
 2009--2014    Markus Blatt
 2013          Andreas Buhr
 2014--2016    Ansgar Burchardt
 2009--2016    Christian Engwer
 2009--2013    Jorrit Fahlke
 2015--2016    Carsten Gräser
 2015--2016    Felix Gruber
 2013--2016    Christoph Grüninger
 2013--2016    Dominic Kempf
 2010--2016    Steffen Müthing
 2012          Rebecca Neumann
 2015          Elias Pipping
 2015          Oliver Sander
 2013          Bård Skaflestad
License: LGPL-3+ or GPL-2 with runtime exception
 The TypeTree library, headers and test programs are copyrighted free software. You
 can use, modify and/or redistribute it under the terms of either one of the two
 following licenses:
 .
 * The GNU Lesser General Public License as published by the Free Software
   Foundation, either Version 3 of the license or (at your option) any later
   version. You can find a copy of the GNU Lesser General Public License, Version
   3, in the files GPL-3 and LGPL-3 or at <http://www.gnu.org/licenses/lgpl-3.0>.
 .
 * Version 2 of the GNU General Public License as published by the Free Software
   Foundation, with the following special exception for linking and compiling
   against the TypeTree library, the so-called "runtime exception":
 .
     As a special exception, you may use the PDELab source files as part of a
     software library or application without restriction.  Specifically, if other
     files instantiate templates or use macros or inline functions from one or
     more of the PDELab source files, or you compile one or more of the PDELab
     source files and link them with other files to produce an executable, this
     does not by itself cause the resulting executable to be covered by the GNU
     General Public License.  This exception does not however invalidate any
     other reasons why the executable file might be covered by the GNU General
     Public License.
 .
   This license is intended to be similar to the GNU Lesser General Public
   License, Version 2, which by itself isn't suitable for a template library. You
   can find a copy of the GNU General Public License, Version 2, in the file
   GPL-2 or at <http://www.gnu.org/licenses/gpl-2.0>.
Comment:
 On Debian systems, the full text of the GNU Lesser General Public License
 version 3 can be found in the file  `/usr/share/common-licenses/LGPL-3'.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file  `/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright: 2014, Ansgar Burchardt <ansgar@debian.org>
License: LGPL-3+ or GPL-2 with runtime exception
 The Debian packaging is released under the same terms as the upstream source.

This work was packaged for Debian by:

    Bernd Zeimetz <bzed@debian.org> on Wed, 26 May 2010 14:49:58 +0200

It was downloaded from:

    git://git.debian.org/pkg-phototools/darktable.git

Upstream Authors (at least 10 commits):

    johannes hanika, Henrik Andersson, Tobias Ellinghaus, Ulrich
    Pegelow, Pascal de Bruijn, Roman Lebedev, Pascal Obry, Jérémy
    Rosen, Pedro Côrte-Real, José Carlos García Sogo, Ger Siemerink,
    Robert Bieber, Edouard Gomez, parafin, Michel Leblond, Richard
    Levitte, Simon Spannagel, Aldric Renaudin, Jean-Sébastien Pédron,
    Olivier Tribout, Dan Torop, Bruce Guenter, Alexandre Prokoudine,
    Christian Tellefsen, bartokk, Rostyslav Pidgornyi, Moritz Lipp,
    Dennis Gnad, José Carlos Casimiro, tatica, Josep V. Moragues,
    Thomas Pryds, marcel, Kaminsky Andrey, Guilherme Brondani Torri,
    Michal Babej, Richard Wonka, Antony Dovgal, Victor Lamoine,
    Dimitrios Psychogios, Milan Knížek, Brian Teague, Frédéric
    Grollier, James C. McPherson, Andrew Toskin, Eckhart Pedersen,
    Mikko Ruohola, JohnnyRun, Kanstantsin Shautsou, Wyatt Olson

Files: *
Copyright: © 2009-2010 The Darktable Authors
Portions Copyright: Mark Probst,  Øyvind Kolås, Shawn Freeman, Udi Fuchs
License:
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

Files: src/common/pwstorage/backend_kwallet.c
License : BSD (3 clause) GPL (v3 or later) 
Copyright: © 2010 Tobias Ellinghaus <houz@gmx.de>
License: GPL3+
Portions copyright: 2010 The Chromium Authors. 

Files: src/iop/Permutohedral.h
Copyright: © 2010 Andrew Adams
License: BSD3
    Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of the Stanford Graphics Lab nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/external/rawspeed
Copyright: © 2009 Klaus Post  <klauspost (at) gmail (dot) com>,
           © 2018 Stefan Löffler, © 2018-2019 Roman Lebedev
Portions Copyright: Hubert Figuiere, Thomas G Lane, Kongji Huang,
	  Brian C. Smith, The Regents of the University of California.
License: LGPL 2+
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

Files: src/external/rawspeed/RawSpeed/TiffTag.h
Copyright: 2004 - 2006 Novell, Inc 
License: MIT/X11 (BSD like) 
    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/external/wb_preset.c
Copyright: © 2004-2013 Udi Fuchs
Licence: GPL2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

Files: src/external/libxcf
Copyright: © 2020 Tobias Ellinghaus
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


Files: src/external/CL/*
Copyright: © 2008-2010 The Khronos Group Inc.
License:
    The files in this directory constitute the interface specification of OpenCL revision 1.1.
    They are copyrighted by The Khronos Group Inc. and can be used, copied, modified, merged,
    published, distributed, sublicensed free of charge according to the following copyright notice:

    /*******************************************************************************
     * Copyright (c) 2008-2010 The Khronos Group Inc.
     *
     * Permission is hereby granted, free of charge, to any person obtaining a
     * copy of this software and/or associated documentation files (the
     * "Materials"), to deal in the Materials without restriction, including
     * without limitation the rights to use, copy, modify, merge, publish,
     * distribute, sublicense, and/or sell copies of the Materials, and to
     * permit persons to whom the Materials are furnished to do so, subject to
     * the following conditions:
     *
     * The above copyright notice and this permission notice shall be included
     * in all copies or substantial portions of the Materials.
     *
     * THE MATERIALS ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
     * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
     * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
     * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
     * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
     * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
     * MATERIALS OR THE USE OR OTHER DEALINGS IN THE MATERIALS.
     ******************************************************************************/ 

    From: Neil Trevett <ntrevett@nvidia.com>
    To: Ulrich Pegelow <ulrich.pegelow@tongareva.de>
    CC: James Riordon <webmaster@khronos.org>
    Date: Sat, 7 May 2011 04:32:47 -0700
    Subject: RE: Permission to use OpenCL header files in open source project
    Thread-Topic: Permission to use OpenCL header files in open source project
    Thread-Index: AcwDQf0xEnqgW9uUQjqGuglo7E4hlQImVv3A
    Message-ID: <12B40963ACC9884B96B321F99D87198416EFEE13D9@HQMAIL01.nvidia.com>
    References: <201104251410.09747.ulrich.pegelow@tongareva.de>
    In-Reply-To: <201104251410.09747.ulrich.pegelow@tongareva.de>
    Accept-Language: en-US
    X-MS-Has-Attach:
    X-MS-TNEF-Correlator:
    acceptlanguage: en-US
    MIME-Version: 1.0
    Content-Language: en-US
    Content-Type: text/plain;
      charsetso-8859-1"
    Content-Transfer-Encoding: quoted-printable
    Envelope-To: ulrich.pegelow@tongareva.de
    Status: R
    X-Status: N
    X-KMail-EncryptionState:  
    X-KMail-SignatureState:  
    X-KMail-MDN-Sent:  

    Hi Ulrich,

    Thank you for checking with us - I can confirm that the header
    files you list are indeed under a free to use license and you may
    integrate them to your project.

    Your application sounds intriguing.  The OpenCL working group is
    looking to outreach to selected developers on a private email
    feedback list.  I think the group would be interested in your
    input feedback on OpenCL functionality and the OpenCL ICD
    mechanism that is under construction.

    Would you be interested in providing feedback to the OpenCL group
    directly?  If so I can help in setting up the logistics to make
    that possible.

    Best regards,

    Neil

    Neil Trevett
    Vice President Mobile Content, NVIDIA | President, Khronos Group
    2701 San Tomas Expressway, Santa Clara, CA 95050 USA
    M: +1 (408) 464-7053 | O: +1 (408) 566-6512 | F: +1 (408) 986-8315
    ntrevett@nvidia.com | www.nvidia.com

    -----Original Message-----
    From: Ulrich Pegelow [mailto:ulrich.pegelow@tongareva.de] 
    Sent: Monday, April 25, 2011 5:10 AM
    To: info@khronos.org
    Subject: Permission to use OpenCL header files in open source project

    Dear Sir or Madam,

    I am seeking your advice and kind permission to use and distribute your OpenCL 
    header files with an open source project called "Darktable".

    Darktable is a photography workflow solution for Linux with a virtual 
    lighttable and darkroom. Its main focus and strength is in conversion and 
    development of photographic RAW-files. Details can be found at 
    http://darktable.sourceforge.net. Darktable is distributed in source code and 
    binary form under the GNU GENERAL PUBLIC LICENCE (GPL) Version 3 
    (http://www.gnu.org/licenses/gpl-3.0.html)

    Darktable already supports OpenCL functionality in various parts. As a 
    consequence it is running with blazing speed at many typical processing steps 
    making it the best open-source raw converter available. It could be a perfect 
    advocate for the benefits of GPU computing as provided by OpenCL and help 
    propagate the idea.

    Darktable's current limitation lies in the fact that a working OpenCL 
    development kit is required at compile time. The resulting binary will only 
    run on systems which have a working OpenCL run-time environment. As Linux 
    software distributions can not rely on this, they will have to ship darktable 
    binaries with OpenCL deactivated, thus preventing wide spread use of OpenCL.

    My goal is to modify Darktable in a way, that it is able to detect the 
    presence of OpenCL at run-time. I will use wrapper functions and dynamic 
    loading to accomplish this. At startup Darktable would search for an existing 
    OpenCL library and bind it if present. This way I would take advantage of the 
    fact that many system already have a working OpenCL runtime library provided 
    by a proprietary graphics card driver. That is the case e.g. for NVIDIA.

    In order to do so, I need to include OpenCL specifications in form of  header 
    files into Darktable's source code, namely the following files from your 
    OpenCL API registry (http://www.khronos.org/registry/cl):

	    opencl.h
	    cl_platform.h
	    cl.h
	    cl_ext.h
	    cl_d3d10.h
	    cl_gl.h
	    cl_gl_ext.h
	    cl.hpp

    All with Version 1.0 or Version 1.1. depending on final implementation.

    I understand the licensing terms in the preambles of your header files in the 
    way that Khronos Group grants all third parties the right to use, copy, 
    modify, merge, publish, distribute, sublicense, and/or sell copies of the 
    material as long as the copyright notice and permission notice in the preamble 
    is included.

    I would be indebted if you could confirm to me that my understanding in right 
    and that I am allowed to include the above mentioned files into Darktables 
    source tree, which is distributed under GPL.

    With best regards,

    Ulrich Pegelow


    -- 
    Ulrich Pegelow, Ph.D.
    Benrodestr. 76
    40597 Düsseldorf
    GERMANY
    Tel. +49-0211-719674

Files: src/external/lua
License: MIT
Copyright: © 1994–2013 Lua.org, PUC-Rio. 

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions: 

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software. 

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

Files: src/external/LuaAutoC
Copyright: (c) 2012, Daniel Holden
License: BSD-ish

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met: 

   1. Redistributions of source code must retain the above copyright notice, this
      list of conditions and the following disclaimer. 
   2. Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution. 

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
   ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

   The views and conclusions contained in the software and documentation are those
   of the authors and should not be interpreted as representing official policies, 
   either expressed or implied, of the FreeBSD Project.

Files: data/lua/darktable/external/pygy_require
License: MIT
Copyright: © 2014 Pierre-Yves Gérardy
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the “Software”), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.

Files: src/win/getrusage.[ch]
License: BSD
Portions Copyright: 1996-2010, PostgreSQL Global Development Group
	 1994, Regents of the University of California
License: (taken from PostgresSQL source)
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose, without fee, and without a written agreement
 is hereby granted, provided that the above copyright notice and this
 paragraph and the following two paragraphs appear in all copies.
 .
 IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING
 LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS
 DOCUMENTATION, EVEN IF THE UNIVERSITY OF CALIFORNIA HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATIONS TO
 PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

Files: cmake/modules/LibFindMacros.cmake
Copyright: © Lasse Kärkkäinen <tronic>
License: public domain
 Public Domain, originally written by Lasse Kärkkäinen <tronic>
 Maintained at https://github.com/Tronic/cmake-modules

Files: data/pswp/*
Copyright: Dmitry Semenov
License: MIT
 The MIT License (MIT)

 Copyright (c) 2014-2015 Dmitry Semenov, http://dimsemenov.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: Debian/*
Copyright: © 2010 Bernd Zeimetz <bzed@debian.org>, 
	   David Bremner <bremner@debian.org>
License: GPL3

The complete text of the GPL (version 3) and the LGPL (version 2.1 and
3) can be found in /usr/share/common-licenses on Debian systems.


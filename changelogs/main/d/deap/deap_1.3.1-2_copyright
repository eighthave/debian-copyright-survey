Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: deap
Upstream-Contact: DEAP Development Team <deap-users@googlegroups.com>
Source: https://github.com/DEAP/deap
Comment: Debianized by Miriam Ruiz on Thu, 09 Dec 2010 23:05:47 +0100

Files: *
Copyright: 2010-2019 DEAP Project,
           2010-2014 François-Michel De Rainville <francois-michel.de-rainville.1@ulaval.dot.ca>
                     Félix-Antoine Fortin <felix-antoine.fortin.1@ulaval.ca>
                     Christian Gagné <christian.gagne@gel.ulaval.ca>
License: LGPL-3+

Files: debian/*
Copyright: 2010-2014 Miriam Ruiz <miriam@debian.org>
           2014-2017 Daniel Stender <stender@debian.org>
License: LGPL-3+

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the latest version of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.

Files: doc/_static/sidebar.js
Copyright: 2007-2011 Sphinx Team
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: doc/_static/copybutton.js
Copyright: 2014 Python Software Foundation
License: Python-2.0
 PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
 --------------------------------------------
 1. This LICENSE AGREEMENT is between the Python Software Foundation ("PSF"),
 and the Individual or Organization ("Licensee") accessing and otherwise using
 this software ("Python") in source or binary form and its associated
 documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, PSF hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use Python alone or in any derivative version,
 provided, however, that PSF's License Agreement and PSF's notice of copyright,
 i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006 Python Software
 Foundation; All Rights Reserved" are retained in Python alone or in any
 derivative version prepared by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on or
 incorporates Python or any part thereof, and wants to make the derivative work
 available to others as provided herein, then Licensee hereby agrees to include
 in any such work a brief summary of the changes made to Python.
 .
 4. PSF is making Python available to Licensee on an "AS IS" basis. PSF MAKES
 NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF EXAMPLE, BUT
 NOT LIMITATION, PSF MAKES NO AND DISCLAIMS ANY REPRESENTATION OR WARRANTY OF
 MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF
 PYTHON WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON FOR ANY
 INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A RESULT OF
 MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON, OR ANY DERIVATIVE THEREOF,
 EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material breach
 of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote products
 or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Python, Licensee agrees to be
 bound by the terms and conditions of this License Agreement.

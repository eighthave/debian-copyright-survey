Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DPUSER
Source: https://www.mpe.mpg.de/~ott/dpuser/
Files-Excluded: utils/pngbook-20010630-src utils/pgplot
 external include lib dpuser/dpuserlib/primes.fnc
 dpuser/doc/pgplot* QFitsView/utils

Files: *
Copyright: 1999-2011 Thomas Ott,
 1994-2003, Research Systems, Inc.
 2002-2007 European Southern Observatory,
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2'.
Comment: According to the documentation, the Julian Date code was written by
 Christian Straubmeier. However he is not mentioned as a copyright holder.

Files: QFitsView/qt_mainwindow.* QFitsView/qt_mdichild.* QFitsView/highlighter.*
Copyright: 2005-2006 Trolltech ASA
License: GPL-2
 This file may be used under the terms of the GNU General Public
 License version 2.0 as published by the Free Software Foundation.
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.
 Please review the following information to ensure GNU General Public
 Licensing requirements will be met:
 https://www.trolltech.com/products/qt/opensource.html
 .
 If you are unsure which license is appropriate for your use, please
 review the following information:
 https://www.trolltech.com/products/qt/licensing.html or contact the
 sales department at sales@trolltech.com.
 .
 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Files: QFitsView/extra/*
Copyright: 2004-2008 Trolltech ASA,
 Martin R. Jones 1997
License: GPL-2/3+-TrollEx
 This file may be used under the terms of the GNU General Public
 License versions 2.0 or 3.0 as published by the Free Software
 Foundation. On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.
 The full text of the GNU General Public License version 3 can be found in the
 file `/usr/share/common-licenses/GPL-3'. Alternatively you may (at your
 option) use any later version of the GNU General Public License if such
 license has been publicly approved by Trolltech ASA (or its successors, if
 any) and the KDE Free Qt Foundation. In addition, as a special exception,
 Trolltech gives you certain additional rights. These rights are described in
 the Trolltech GPL Exception version 1.2, which can be found at
 `https://www.trolltech.com/products/qt/gplexception/`
 .
 Please review the following information to ensure GNU General
 Public Licensing requirements will be met:
 https://trolltech.com/products/qt/licenses/licensing/opensource/. If
 you are unsure which license is appropriate for your use, please
 review the following information:
 https://trolltech.com/products/qt/licenses/licensing/licensingoverview
 or contact the sales department at sales@trolltech.com.
 .
 In addition, as a special exception, Trolltech, as the sole
 copyright holder for Qt Designer, grants users of the Qt/Eclipse
 Integration plug-in the right for the Qt/Eclipse Integration to
 link to functionality provided by Qt Designer and its related
 libraries.
 .
 This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
 INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE. Trolltech reserves all rights not expressly
 granted herein.

Files: libfits/dpheader.cpp
Copyright: 1994 Associated Universities, Inc. Washington DC, USA.
License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by
 the Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
 License for more details.
Comment: This copyright applies only to the "worldpos_old" function.

Files: utils/regex/regex_sr.*
Copyright: 1986, 1993, 1995 by University of Toronto
License: RegEx
 Permission is granted to anyone to use this software for any
 purpose on any computer system, and to redistribute it in any way,
 subject to the following restrictions:
 .
 1. The author is not responsible for the consequences of use of
 this software, no matter how awful, even if they arise
 from defects in it.
 .
 2. The origin of this software must not be misrepresented, either
 by explicit claim or by omission.
 .
 3. Altered versions must be plainly marked as such, and must not
 be misrepresented (by explicit claim or omission) as being
 the original software.
 .
 4. This notice must not be removed or altered.

Files: utils/cmpfit/*
Copyright: 1999 University of Chicago,
 2003-2007 Craig B. Markwardt,
License: ISC-Alike+BSD-3-Clause
 This software is provided as is without any warranty whatsoever.
 Permission to use, copy, modify, and distribute modified or
 unmodified copies is granted, provided this copyright and disclaimer
 are included unchanged.
 .
 Source code derived from MINPACK must have the following disclaimer
 text provided:
 .
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the
 following conditions are met:
 .
 1. Redistributions of source code must retain the above
 copyright notice, this list of conditions and the following
 disclaimer.
 .
 2. Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials
 provided with the distribution.
 .
 3. The end-user documentation included with the
 redistribution, if any, must include the following
 acknowledgment:
 .
    "This product includes software developed by the
    University of Chicago, as Operator of Argonne National
    Laboratory.
 .
 Alternately, this acknowledgment may appear in the software
 itself, if and wherever such third-party acknowledgments
 normally appear.
 .
 4. WARRANTY DISCLAIMER. THE SOFTWARE IS SUPPLIED "AS IS"
 WITHOUT WARRANTY OF ANY KIND. THE COPYRIGHT HOLDER, THE
 UNITED STATES, THE UNITED STATES DEPARTMENT OF ENERGY, AND
 THEIR EMPLOYEES: (1) DISCLAIM ANY WARRANTIES, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE
 OR NON-INFRINGEMENT, (2) DO NOT ASSUME ANY LEGAL LIABILITY
 OR RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR
 USEFULNESS OF THE SOFTWARE, (3) DO NOT REPRESENT THAT USE OF
 THE SOFTWARE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS, (4)
 DO NOT WARRANT THAT THE SOFTWARE WILL FUNCTION
 UNINTERRUPTED, THAT IT IS ERROR-FREE OR THAT ANY ERRORS WILL
 BE CORRECTED.
 .
 5. LIMITATION OF LIABILITY. IN NO EVENT WILL THE COPYRIGHT
 HOLDER, THE UNITED STATES, THE UNITED STATES DEPARTMENT OF
 ENERGY, OR THEIR EMPLOYEES: BE LIABLE FOR ANY INDIRECT,
 INCIDENTAL, CONSEQUENTIAL, SPECIAL OR PUNITIVE DAMAGES OF
 ANY KIND OR NATURE, INCLUDING BUT NOT LIMITED TO LOSS OF
 PROFITS OR LOSS OF DATA, FOR ANY REASON WHATSOEVER, WHETHER
 SUCH LIABILITY IS ASSERTED ON THE BASIS OF CONTRACT, TORT
 (INCLUDING NEGLIGENCE OR STRICT LIABILITY), OR OTHERWISE,
 EVEN IF ANY OF SAID PARTIES HAS BEEN WARNED OF THE
 POSSIBILITY OF SUCH LOSS OR DAMAGES.
Comment: Original public domain version by B. Garbow, K. Hillstrom,
 J. More' (Argonne National Laboratory, MINPACK project, March 1980).
 Translation to C Language by S. Moshier (moshier.net)

Files: utils/osdir/* debian/*
Copyright: 2002 Bart Vanhauwaert
 2016 Ole Streicher <olebole@debian.org> (Debian files)
License: Expat
 Permission to use, copy, modify, distribute and sell this software
 for any purpose is hereby granted without fee. This license
 includes (but is not limited to) standalone compilation or as part
 of a larger project.
 .
 This software is provided "as is" without express or implied warranty.
 .
 For a full statement on warranty and terms and conditions for
 copying, distribution and modification, please see the comment block
 at the end of this file.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: daisy-player
Source: http://www.jlemmens.nl/
Upstream-Contact: Jos Lemmens <jos@jlemmens.nl>

Files: *
Copyright: © 2003-2018 Jos Lemmens <jos@jlemmens.nl>
License: GPL-2+

Files: ./debian/*
Copyright: © 2010-2018 Paul Gevers <elbrus@debian.org>
License: GPL-2+

Files: ./icons/*
Copyright: © 2002-2010
  Ulisse Perusin <uli.peru@gmail.com>
  Riccardo Buzzotta <raozuzu@yahoo.it>
  Josef Vybíral <cornelius@vybiral.info>
  Hylke Bons <h.bons@student.rug.nl>
  Ricardo González <rick@jinlabs.com>
  Lapo Calamandrei <calamandrei@gmail.com>
  Rodney Dawes <dobey@novell.com>
  Luca Ferretti <elle.uca@libero.it>
  Tuomas Kuosmanen <tigert@gimp.org>
  Andreas Nilsson <nisses.mail@home.se>
  Jakub Steiner <jimmac@novell.com>
  Ellen Rohaan <ellen.rohaan@esac.climbing.nl>
Comment: The daisy-player icon is based on the accessories-dictionary.svgz icon
  from the plasma-widgets-addons package (in Ubuntu version 4:4.5.1-0ubuntu4).
License: GPL-2

Files: ./src/gettext.h
Copyright: (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald
 Modified by the GLib Team and others 1997-2000.
License: LGPL-2+

Files: ./src/madplay/*
Copyright: 2000-2004 Robert Leslie <rob@mars.org>
License: GPL-2+

License: GPL-2
  This package is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; version 2 dated June, 1991.
  .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  On Debian GNU/Linux systems, the complete text of the GNU General
  Public License can be found in /usr/share/common-licenses/GPL-2.

License: GPL-2+
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.
  .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.
  .
  On Debian systems, the complete text of the GNU General Public
  License, version 2, can be found in /usr/share/common-licenses/GPL-2.

License: LGPL-2+
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
  .
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  .
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, see
  <http://www.gnu.org/licenses/>.
  .
  On Debian GNU/Linux systems, the complete text of the GNU Lesser
  General Public License, version 2, can be found in
  /usr/share/common-licenses/GPL-2.

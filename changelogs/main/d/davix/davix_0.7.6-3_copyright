Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: davix
Source: https://github.com/cern-fts/davix/releases/download/R_0_7_6/davix-0.7.6.tar.gz

Files: *
Copyright: 2013-2020 CERN
License: LGPL-2.1+

Files: deps/libneon
Copyright: 1999-2008 Joe Orton <joe@manyfish.co.uk>
           Aleix Conchillo Flaque <aleix@member.fsf.org>
           Arfrever Frehtes Taifersar Arahesis
           Arun Garg <arung@pspl.co.in>
           Daniel Stenberg <daniel@haxx.se>
           Free Software Foundation, Inc.
           Henrik Holst <henrik.holst2@gmail.com>
           Jiang Lei <tristone@deluxe.ocn.ne.jp>
           Kai Sommerfeld <kso@openoffice.org>
           Karl Ove Hufthammer.
           Michael Sobolev
           Nobuyuki Tsuchimura <tutimura@nn.iij4u.or.jp>
           Sylvain Glaize <mokona@puupuu.org>
           Thomas Schultz <tststs@gmx.de>
           Vladimir Berezniker @ http://public.xdi.org/=vmpn
           Yves Martin  <ymartin59@free.fr>
License: LGPL-2+
Comment:
 This is a fork of the neon library: http://www.webdav.org/neon/
 The modifications are substantial - it can no longer be considered a
 bundled copy of the original source. Unbundling in favour of using
 the libneon27 package is not possible.

Files: deps/libneon/doc deps/libneon/test
Copyright: 1999-2008 Joe Orton <joe@manyfish.co.uk>
License: GPL-2+

Files: genversion.py
Copyright: 2017 CERN/Switzerland
License: GPL-3+

Files: src/libs/alibxx
Copyright: Adrien Devresse
License: public-domain
 Alibxx is a simple library for CXX conveniences functions.
 This library is under public domain: feel free to reuse, include and change
 the license.

Files: src/libs/alibxx/str/format.cpp
Copyright: 2012-2014 Victor Zverovich
License: BSD-2-clause

Files: src/libs/rapidjson
Copyright: 2015 THL A29 Limited, a Tencent company, and Milo Yip.
License: MIT
Comment: Not used - removed in configure - uses rapidjson-dev instead

Files: src/libs/rapidjson/msinttypes
Copyright: 2006-2013 Alexander Chemeris
License: BSD-3-clause
Comment: Not used - removed in configure - uses rapidjson-dev instead

Files: deps/libneon/src/davix_logger_c.h deps/simple_getpass src/libs/datetime src/libs/alibxx/crypto
Copyright: 2013 Adrien Devresse <adrien.devresse@cern.ch>, CERN
License: LGPL-2.1+

Files: src/libs/IntervalTree.h
Copyright: 2011 Erik Garrison
License: MIT

Files: src/modules/copy/delegation/delegation-2.0.0.wsdl
Copyright: Members of the EGEE Collaboration. 2004.
 Members of the EGEE Collaboration
 ---------------------------------
 European Organization for Particle Physics (CERN)
 Institut für Graphische und Parallele Datenverarbeitung der
   Joh. Kepler Universität Linz (AT)
 Institut für Informatik der Universität Innsbruck (AT)
 CESNET, z.s.p.o. (CZ)
 Budapest University of Technology and Economics (HU)
 Eötvös Loránd University Budapest (HU)
 KFKI Research Institute for Particle and Nuclear Physics (HU)
 Magyar Tudományos Akadémia Számítástechnikai és Automatizálási
   Kutatóintézet (HU)
 Office for National Information and Infrastructure Development (HU)
 Akademickie Centrum Komputerowe CYFRONET akademii Górniczo-Hutniczej
   im. St. Staszica w Krakowie (PL)
 Warsaw University Interdisciplinary Centre for Mathematical and
   Computational Modelling (PL)
 Institute of Bioorganic Chemistry PAN, Poznan Supercomputing and
   Networking Center (PL)
 Ustav Informatiky, Slovenská Akadémia Vied (SK)
 Jožef Stefan Institute (SI)
 The Provost Fellows and Scholars of the College of the Holy and
   Undivided Trinity of Queen Elizabeth near Dublin (IE)
 Council for the Central Laboratory of the Research Councils (GB)
 The University of Edinburgh (GB)
 Particle Physics and Astronomy Research Council (GB)
 University College of London (GB)
 Commissariat l'Energie Atomique, Direction des Sciences de la Matière (FR)
 Compagnie Générale de Géophysique (FR)
 Centre National de la Recherche Scientifique (FR)
 CS Système d'Information Communication & Systèmes (FR)
 Centrale Recherche S.A. (FR)
 Deutsches Elektronen-Synchrotron (DE)
 Deutsches Klimarechenzentrum GmbH (DE)
 Fraunhofer-Gesellschaft zur Förderung der Angewandten Forschung e.V. (DE)
 Forschungszentrum Karlsruhe GmbH (DE)
 Gesellschaft für Schwerionenforschung GmbH (DE)
 DATAMAT S.p.A. (IT)
 Istituto Nazionale di Fisica Nucleare (IT)
 Trans-European Research and Networking Association (NL)
 Vrije Universiteit Brussel (BE)
 Faculty of Science University of Copenhagen (DK)
 University of Helsinki (FI)
 Foundation for Fundamental Research on Matter (NL)
 Stichting Academisch Rekencentrum Amsterdam (NL)
 Universiteit van Amsterdam (NL)
 University of Bergen (NO)
 Vetenskapsrådet, The Swedish Research Council (SE)
 Institute of High Energy Physics (RU)
 Institute of Mathematical Problems of Biology of Russian Academy of
   Sciences (RU)
 Institute of Theoretical and Experimental Physics (RU)
 Joint Institute for Nuclear Research (RU)
 Keldysh Institute of Applied Mathematics of Russian Academy of Sciences
   Moscow (RU)
 Petersburg Nuclear Physics Institute of Russian Academy of Sciences (RU)
 Russian Research Centre "Kurchatov Institute" (RU)
 Skobeltsyn Institute of Nuclear Physics of Moscow State University (RU)
 Central Lab. for Parallel Processing, Bulgarian Academy of Sciences (BG)
 University of Cyprus (CY)
 Greek Research and Technology Network (GR)
 Tel Aviv University (IL)
 National Institute for Research and Development in Informatics (RO)
 Laboratório de Instrumentação e Física Experimental de Partículas (PT)
 S.A.X. Centro de Supercomputación de Galicia (ES)
 Consejo Superior de Investigaciones Cientificas (ES)
 Institut de Física d'Altes Energies (ES)
 Instituto Nacional de Tecnica Aeroespacial (ES)
 Universidad Politécnica de Valencia (ES)
 University of Chicago (US)
 University of Southern California, Marina del Rey (US)
 The Board of Regents for the University of Wisconsin System (US)
 Royal Institute of Technology - Center for Parallel Computers (SE)
 Ente per le Nuove Tecnologie, l'Energia e l'Ambiente (IT)
 Università degli Studi della Calabria (IT)
 Università degli Studi di Lecce (IT)
 Università degli Studi di Napoli Federico II (IT)
 Delivery of Advanced Network Technology to Europe Limited (GB)
 Verein zur Förderung eines Deutschen Forschungsnetzes e.V. (DE)
 Consortium GARR (IT)
License: Apache-2.0

Files: src/modules/copy/delegation/GRSTx509MakeProxyCert.*
Copyright: 2002-2007 Andrew McNab, University of Manchester
License: BSD-2-clause

Files: deps/googletest
Copyright: Copyright 2008, Google Inc.
License: BSD-3-clause
Comment: Not used - removed in configure - use libgtest-dev instead

Files: deps/googletest/googlemock/scripts/upload.py deps/googletest/googletest/scripts/upload.py deps/googletest/googlemock/scripts/generator
Copyright: Copyright 2007 Google Inc.
License: Apache-2.0
Comment: Not used - removed in configure

Files: deps/strptime
Copyright: 1996, 1997, 1998, 1999, 2000 Free Software Foundation, Inc.
License: LGPL-2+
Comment: Not used - removed in configure - part of glibc

Files: doc/sphinx/_themes/sphinx_rtd_theme
Copyright: 2013 Dave Snider
License: Expat
Comment: Not used - removed in configure - uses python-sphinx-rtd-theme instead

Files: doc/sphinx/_themes/sphinx_rtd_theme/layout_old.html doc/sphinx/_themes/sphinx_rtd_theme/search.html
Copyright: 2007-2013 The Sphinx team
License: BSD-2-clause
Comment: Not used - removed in configure - uses python-sphinx-rtd-theme instead

Files: doc/sphinx/_themes/sphinx_rtd_theme/static/fonts/*
Copyright: 2012-2013 Dave Gandy <drgandy@alum.mit.edu>
           2010-2011 Łukasz Dziedzic
           2006 Raph Levien
License: OFL-1.1
Comment: Not used - removed in configure

Files: doc/sphinx/_themes/sphinx_rtd_theme/static/fonts/RobotoSlab-*.ttf
Copyright: Christian Robertson
License: Apache-2.0
Comment: Not used - removed in configure

Files: test/functional/optionparser.h
Copyright: 2012 Matthias S. Benkmann
License: MIT

Files: test/pywebdav
Copyright: 2009 Simon Pamies <s.pamies@banality.de>
           1999-2006 Christian Scholz <ruebe@aachen.heimat.de>
           2009 Vince Spicer <vince@vince.ca>
License: GPL-2+
Comment: Not used - removed in configure - could use python-webdav instead
 However, the functional tests that use this require valid credentials and
 network access and can therefore not be run as part of the package build.

Files: debian/*
Copyright: 2013-2021 Mattias Ellert <mattias.ellert@physics.uu.se>
License: LGPL-2.1+

Files: debian/missing-sources/modernizr.js
Copyright: 2009-2012 Faruk Ates, Paul Irish, Alex Sexton
License: Expat

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: GPL-2+
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Apache-2.0
 On Debian systems, the complete text of the Apache License, version 2 can
 be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
    3. Neither the name of the product nor the names of its contributors may
       be used to endorse or promote products derived from this software
       without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: OFL-1.1
 PREAMBLE
 .
 The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font creation
 efforts of academic and linguistic communities, and to provide a free and
 open framework in which fonts may be shared and improved in partnership
 with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded,
 redistributed and/or sold with any software provided that any reserved
 names are not used by derivative works. The fonts and derivatives,
 however, cannot be released under any other type of license. The
 requirement for fonts to remain under this license does not apply
 to any document created using the fonts or their derivatives.
 .
 DEFINITIONS
 .
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software components as
 distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to, deleting,
 or substituting -- in part or in whole -- any of the components of the
 Original Version, by changing formats or by porting the Font Software to a
 new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed, modify,
 redistribute, and sell modified and unmodified copies of the Font
 Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
    in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
    redistributed and/or sold with any software, provided that each copy
    contains the above copyright notice and this license. These can be
    included either as stand-alone text files, human-readable headers or
    in the appropriate machine-readable metadata fields within text or
    binary files as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
    Name(s) unless explicit written permission is granted by the corresponding
    Copyright Holder. This restriction only applies to the primary font
    name as presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
    Software shall not be used to promote, endorse or advertise any
    Modified Version, except to acknowledge the contribution(s) of the
    Copyright Holder(s) and the Author(s) or with their explicit written
    permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
    must be distributed entirely under this license, and must not be
    distributed under any other license. The requirement for fonts to
    remain under this license does not apply to any document created
    using the Font Software.
 .
 TERMINATION
 .
 This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.

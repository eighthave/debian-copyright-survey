Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dune-common
Source: https://www.dune-project.org/dev/downloadgit/

Files: *
Copyright:
 2015--2017    Marco Agnese
 2015          Martin Alkämper
 2003--2010    Peter Bastian
 2004--2017    Markus Blatt
 2013          Andreas Buhr
 2011--2017    Ansgar Burchardt
 2004--2005    Adrian Burri
 2014          Benjamin Bykowski
 2014          Marco Cecchetti
 2006--2018    Andreas Dedner
 2003          Marc Droske
 2003--2018    Christian Engwer
 2004--2018    Jorrit Fahlke
 2016          Thomas Fetzer
 2008--2013    Bernd Flemisch
 2013--2014    Christoph Gersbacher
 2015          Stefan Girke
 2015--2017    Felix Gruber
 2005--2017    Carsten Gräser
 2010--2018    Christoph Grüninger
 2006          Bernard Haasdonk
 2015--2016    René Heß
 2015          Claus-Justus Heine
 2012--2013    Olaf Ippisch
 2013--2017    Dominic Kempf
 2009          Leonard Kern
 2013          Torbjörn Klatt
 2003--2017    Robert Klöfkorn
 2005--2007    Sreejith Pulloor Kuttanikkad
 2012--2016    Arne Morten Kvarving
 2010--2014    Andreas Lauser
 2016--2017    Tobias Leibner
 2015          Lars Lubkoll
 2012--2017    Tobias Malkmus
 2007--2011    Sven Marnach
 2010--2017    René Milk
 2011--2018    Steffen Müthing
 2003--2006    Thimo Neubauer
 2011          Rebecca Neumann
 2008--2018    Martin Nolte
 2014          Andreas Nüßing
 2004--2005    Mario Ohlberger
 2014          Steffen Persvold
 2008--2017    Elias Pipping
 2011          Dan Popovic
 2017          Simon Praetorius
 2009          Atgeirr Rasmussen
 2006--2014    Uli Sack
 2003--2017    Oliver Sander
 2006          Klaus Schneider
 2004          Roland Schulz
 2015          Nicolas Schwenck
 2016          Linus Seelinger
 2009--2014    Bård Skaflestad
 2012          Matthias Wohlmuth
 2011--2016    Jonathan Youett
License: GPL-2 with DUNE exception

Files: cmake/modules/UseLATEX.cmake
Copyright: 2004-2015, Sandia Corporation
License: other
 Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
 license for use of this work by or on behalf of the
 U.S. Government. Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that this Notice and any
 statement of authorship are reproduced on all copies.

Files: doc/dunecontrol.1
Copyright: 2013-2015, Oliver Sander
License: GNU-All-Permissive-License

Files: debian/*
Copyright: 2011-2015, Ansgar Burchardt <ansgar@debian.org>
License: GPL-2 with DUNE exception

License: GPL-2 with DUNE exception
 The DUNE library and headers are licensed under version 2 of the GNU General
 Public License, with a special exception for linking and compiling against
 DUNE, the so-called "runtime exception." The license is intended to be
 similar to the GNU Lesser General Public License, which by itself isn't
 suitable for a template library.
 .
 The exact wording of the exception reads as follows:
 .
 As a special exception, you may use the DUNE library without
 restriction.  Specifically, if other files instantiate templates or
 use macros or inline functions from one or more of the DUNE source
 files, or you compile one or more of the DUNE source files and link
 them with other files to produce an executable, this does not by
 itself cause the resulting executable to be covered by the GNU
 General Public License.  This exception does not however invalidate
 any other reasons why the executable file might be covered by the
 GNU General Public License.
 .
 This license clones the one of the libstdc++ library.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file  `/usr/share/common-licenses/GPL-2'.

License: GNU-All-Permissive-License
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.

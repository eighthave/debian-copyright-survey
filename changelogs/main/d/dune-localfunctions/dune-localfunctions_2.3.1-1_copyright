Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dune-localfunctions
Source: http://www.dune-project.org/downloadsvn.html

Files: *
Copyright:
 2008--2010    Peter Bastian
 2009--2013    Markus Blatt
 2009--2013    Andreas Dedner
 2010          Martin Drohmann
 2008--2013    Christian Engwer
 2008--2012    Jorrit Fahlke
 2011--2013    Bernd Flemisch
 2012          Christoph Gersbacher
 2009--2013    Carsten Gräser
 2011--2013    Christoph Grüninger
 2013          Guillaume Jouvet
 2013          Tobias Malkmus
 2009          Sven Marnach
 2013          Steffen Müthing
 2012          Rebecca Neumann
 2008--2013    Martin Nolte
 2011--2013    Elias Pipping
 2012--2013    Human Rezaijafari
 2011--2012    Uli Sack
 2008--2013    Oliver Sander
 2011--2012    Matthias Wohlmuth
 2010--2013    Jonathan Youett
License: GPL-2 with DUNE exception

Files: debian/*
Copyright: 2011-2012, Ansgar Burchardt <ansgar@debian.org>
License: GPL-2 with DUNE exception

License: GPL-2 with DUNE exception
 The DUNE library and headers are licensed under version 2 of the GNU General
 Public License, with a special exception for linking and compiling against
 DUNE, the so-called "runtime exception." The license is intended to be
 similar to the GNU Lesser General Public License, which by itself isn't
 suitable for a template library.
 .
 The exact wording of the exception reads as follows:
 .
 As a special exception, you may use the DUNE library without
 restriction.  Specifically, if other files instantiate templates or
 use macros or inline functions from one or more of the DUNE source
 files, or you compile one or more of the DUNE source files and link
 them with other files to produce an executable, this does not by
 itself cause the resulting executable to be covered by the GNU
 General Public License.  This exception does not however invalidate
 any other reasons why the executable file might be covered by the
 GNU General Public License.
 .
 This license clones the one of the libstdc++ library.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file  `/usr/share/common-licenses/GPL-2'.

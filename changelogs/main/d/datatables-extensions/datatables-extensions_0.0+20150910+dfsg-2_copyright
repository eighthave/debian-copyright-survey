Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Datatables Extensions
Upstream-Contact: SpryMedia
                  https://datatables.net/contact
Source: http://www.datatables.net/license
Files-Excluded: */buttons.flash* */*.swf */vfs_fonts.js

Files: *
Copyright: 2008-2015 SpryMedia Limited
License: MIT

Files: JSZip/*
Copyright: 2009-2014 Stuart Knightley <stuart@stuartk.com>
License: MIT
Comment: The code is actually dual licenced under the MIT license or GPLv3
 but to match the other code of this package MIT is picked here.

Files: pdfmake/*
Copyright: 2014-2017 bpampuch
License: MIT
Comment: The file vfs_fonts.js was excluded (see above Files-Excluded) since
 it looked quite "binary without source".  I've checked also pdfmake source at
    https://github.com/bpampuch/pdfmake/
 where it looks like a TTF font file encapsulated in JS (which is not that
 visible in the code copy of the older version in datatables-extensions).
 Upstream README.md says:
  vfs_fonts.js: default font definition (it contains Roboto, you can however
  use custom fonts instead
     https://github.com/bpampuch/pdfmake/wiki/Custom-Fonts---client-side

Files: debian/*
Copyright: 2015 Sascha Steinbiss <sascha@steinbiss.name>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

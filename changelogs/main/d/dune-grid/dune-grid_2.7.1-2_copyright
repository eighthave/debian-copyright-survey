Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dune-grid
Source: https://www.dune-project.org/dev/downloadgit/

Files: *
Copyright:
 2015--2017    Marco Agnese
 2003--2012    Peter Bastian
 2013          Timo Betcke
 2004--2018    Markus Blatt
 2013          Andreas Buhr
 2012--2019    Ansgar Burchardt
 2004--2005    Adrian Burri
 2013          Benjamin Bykowski
 2006--2018    Andreas Dedner
 2010          Martin Drohmann
 2003          Marc Droske
 2003--2019    Christian Engwer
 2004--2019    Jorrit Fahlke
 2007--2018    Bernd Flemisch
 2006--2016    Christoph Gersbacher
 2005--2019    Carsten Gräser
 2015--2017    Felix Gruber
 2012--2019    Christoph Grüninger
 2006          Bernhard Haasdonk
 2016          René Heß
 2019          Stephan Hilb
 2013          Sven Kaulmann
 2019          Liam Keegan
 2013--2018    Dominic Kempf
 2010--2015    Ole Klein
 2015          Angela Klewinghaus
 2003--2019    Robert Klöfkorn
 2017          Marcel Koch
 2018--2019    Timo Koch
 2005--2007    Sreejith Pulloor Kuttanikkad
 2009--2013    Andreas Lauser
 2017--2018    Tobias Leibner
 2012--2017    Tobias Malkmus
 2007--2009    Sven Marnach
 2013          René Milk
 2012          Eike Müller
 2011          Thomas Müller
 2010--2019    Steffen Müthing
 2003--2005    Thimo Neubauer
 2011          Adrian Ngo
 2008--2016    Martin Nolte
 2015          Andreas Nüßing
 2004--2006    Mario Ohlberger
 2019          Santiago Ospina
 2014--2015    Marian Piatkowski
 2009--2016    Elias Pipping
 2018--2019    Simon Praetorius
 2010          Dan Popovic
 2009          Atgeirr Flø Rassmussen
 2009          Uli Sack
 2003--2019    Oliver Sander
 2011--2012    Alexander Schlaich
 2017          Jakob Schneck
 2004          Roland Schulz
 2012--2013    Nicolas Schwenck
 2015--2018    Linus Seelinger
 2016          Michaël Sghaïer
 2008          Nina Shokina
 2010          Bård Skaflestad
 2012          Markus Wolf
 2009--2018    Jonathan Youett
License: GPL-2 with DUNE exception

Files: debian/*
Copyright: 2011-2020, Ansgar <ansgar@debian.org>
License: GPL-2 with DUNE exception

License: GPL-2 with DUNE exception
 The DUNE library and headers are licensed under version 2 of the GNU General
 Public License, with a special exception for linking and compiling against
 DUNE, the so-called "runtime exception." The license is intended to be
 similar to the GNU Lesser General Public License, which by itself isn't
 suitable for a template library.
 .
 The exact wording of the exception reads as follows:
 .
 As a special exception, you may use the DUNE library without
 restriction.  Specifically, if other files instantiate templates or
 use macros or inline functions from one or more of the DUNE source
 files, or you compile one or more of the DUNE source files and link
 them with other files to produce an executable, this does not by
 itself cause the resulting executable to be covered by the GNU
 General Public License.  This exception does not however invalidate
 any other reasons why the executable file might be covered by the
 GNU General Public License.
 .
 This license clones the one of the libstdc++ library.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file  `/usr/share/common-licenses/GPL-2'.

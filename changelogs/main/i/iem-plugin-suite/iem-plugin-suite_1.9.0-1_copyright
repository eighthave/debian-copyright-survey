Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IEMPluginSuite
Source: https://git.iem.at/audioplugins/IEMPluginSuite
Files-Excluded:
 .git*

Files: *
Copyright: 2017-2018, Institute of Electronic Music and Acoustics, IEM/Graz
 2017-2018, Daniel Rudrich <rudrich@iem.at>
 2017-2018, Sebastian Grill
License: GPL-3+

Files: resources/Eigen/*
Copyright:
 2006-2011, Benoit Jacob <jacob.benoit.1@gmail.com>
 2008-2016, Gael Guennebaud <gael.guennebaud@inria.fr>
 2009-2013, Hauke Heibel <hauke.heibel@googlemail.com>
 2016, Eugene Brevdo <ebrevdo@gmail.com>
 2013, Gauthier Brun <brun.gauthier@gmail.com>
 2010, Thomas Capricelli <orzel@freehackers.org>
 2013, Nicolas Carre <nicolas.carre@ensimag.fr>
 2013, Jean Ceccato <jean.ceccato@ensimag.fr>
 2009, Rohit Garg <rpg.314@gmail.com>
 2009, Mathieu Gautier <mathieu.gautier@cea.fr>
 2014-2016, Pedro Gonnet <pedro.gonnet@gmail.com>
 2013, Pavel Holoborodko <pavel@holoborodko.com>
 2011, Timothy E. Holy <tim.holy@gmail.com>
 2001, Intel Corporation
 2012, Alexey Korepanov <kaikaikai@yandex.ru>
 2016, Rasmus Munk Larsen (rmlarsen@google.com)
 2010, Vincent Lejeune
 2010, Daniel Lowengrub <lowdanie@gmail.com>
 2008-2016, Konstantinos Margaritis <markos@freevec.org>
 2009, Ricard Marxer <email@ricardmarxer.com>
 2009, Keir Mierle <mierle@gmail.com>
 2010-2013, Jitse Niesen <jitse@maths.leeds.ac.uk>
 2012-2013, Désiré Nuentsa-Wakam <desire.nuentsa_wakam@inria.fr>
 2007, Michael Olbrich <michael.olbrich@gmx.net>
 2007, Julien Pommier
 2009, Kenneth Riddile <kfriddile@yahoo.com>
 2014-2016, Benoit Steiner <benoit.steiner.goog@gmail.com>
 2016, Tobias Wood <tobias@spinicist.org.uk>
 2013, Pierre Zoppitelli <pierre.zoppitelli@ensimag.fr>
License: MPL-2.0

Files:  resources/Eigen/src/Core/products/*_BLAS.h
 resources/Eigen/src/misc/lapacke.h
 resources/Eigen/src/*/*LAPACKE.h
 resources/Eigen/src/Core/util/MKL_support.h
 resources/Eigen/src/PardisoSupport/PardisoSupport.h
Copyright: 2010-2011, Intel Corporation.
License: BSD-3-clause

Files: resources/Eigen/src/Core/Assign_MKL.h
Copyright: 2011, Intel Corporation.
  2015, Gael Guennebaud <gael.guennebaud@inria.fr>
License: BSD-3-clause

Files: resources/Eigen/src/OrderingMethods/Amd.h
 resources/Eigen/src/SparseCholesky/SimplicialCholesky_impl.h
Copyright: 2008-2012, Gael Guennebaud <gael.guennebaud@inria.fr>
 2005-2006, Timothy A. Davis
License: LGPL-2.1+

Files: resources/Eigen/src/Core/arch/CUDA/Half.h
Copyright: Fabian Giesen, 2016
License: BSD-0-clause and MPL-2.0

Files: resources/WalshHadamard/*
Copyright: 2014, Jonathan Hadida <jonathan.hadida[at]dtc.ox.ac.uk>
License: Expat

Files: resources/NewtonApple/*
Copyright: 2017, Dr David Sinclair <david@newtonapples.net>
License: GPL-3

Files: debian/*
Copyright: 2017 IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>
License: GPL-3+

License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla Public License
 v. 2.0. If a copy of the MPL was not distributed with this file, You can
 obtain one at http://mozilla.org/MPL/2.0/.
Comment:
 On Debian systems, the complete text of the Mozilla Public License v2.0 can be
 found in "/usr/share/common-licenses/MPL-2.0".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.
 .
 This package is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, version 3 of the License.
 .
 This package is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in "/usr/share/common-licenses/GPL-3".



License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of Intel Corporation nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-0-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option)
 any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, see <https://www.gnu.org/licenses/>
 .
 Permission is hereby granted to use or copy this program under the terms of
 the GNU LGPL, provided that the Copyright, this License, and the Availability
 of the original version is retained on all copies. User documentation of any
 code that uses this code or any modified version of this code must cite the
 Copyright, this License, the Availability note, and "Used by permission."
 Permission to modify the code and to distribute modified code is granted,
 provided the Copyright, this License, and the Availability note are retained,
 and a notice that the code was modified is included.
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

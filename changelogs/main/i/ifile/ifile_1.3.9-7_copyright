Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ifile
Upstream-Contact: Jason Rennie <jrennie@gmail.com>
Source: http://people.csail.mit.edu/jrennie/ifile

Files: *
Copyright:
  1996-2002 Jason Rennie <jrennie@gmail.com>
License: LGPL-2+

Files:
 database.c
 error.c
 hash_table.c
 ifile.c
 include/ifile.h
 include/hash_table.h
 include/extendable_array.h
 lex-define.c
 opts.c
 primes.c
 stem.c
 test.sh
 util.c
Copyright:
  1996-2002 Jason Rennie <jrennie@gmail.com>
License: GPL-2+

Files: ifile.1
Copyright:
  1996-2002 Jason Rennie <jrennie@gmail.com>
License: Other
 ifile manpage section on examples:
 .
 Taken from http://www.nongnu.org/ifile/tutorial.html with
 permission from the Jason D. M. Rennie: "Yes, feel free to modify
 and redistribute that tutorial as you see fit for the Debian ifile
 package." as expressed in the log for bug#268332.

Files: debian/*
Copyright:
  2010-2016 Jari Aalto <jari.aalto@cante.net>
  2002-2009 Jens Peter Secher <jpsecher@diku.dk>
License: GPL-2+

Files: debian/ifile.procmail-0.2/*
Copyright:
 2001 Martin Maèok <martin.macok@underground.cz>
Comment:
 [From original debian/copyright, before converting to DEP 5]
 .
 All scripts written by Martin MaÃ¨ok <martin.macok@underground.cz>.
 Martin Maèok disclaims all copyright in these scripts and places them
 in the public domain. Originally from http://Xtrmntr.org/ORBman/ but
 URL no longer responds 2009-11-28.
License: Public-Domain
 [From debian/ifile.procmail-0.2/README]
 License:
 --------
 Public domain.

Files: debian/metamail/*
Copyright:
 2002-2009 Jens Peter Secher <jps@debian.org>
License: Custom

License: Custom
 All scripts written by Jens Peter Secher <jps@debian.org>, who
 disclaims all copyright in these scripts and places them in the
 public domain.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: idlastro
Source: https://idlastro.gsfc.nasa.gov/ftp/astron.tar.gz
Files-Excluded: pro/factor.pro pro/getwrd.pro pro/polrec.pro pro/prime.pro
 pro/recpol.pro pro/sphdist.pro pro/ymd2dn.pro
 pro/jplephinterp.pro pro/jplephread.pro pro/jplephtest.pro pro/tdb2tdt.pro

Files: *
Copyright: 2014 Wayne Landsman
 2015 Ole Streicher <olebole@debian.org> (Debian files)
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
 .
  Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in
  the documentation and/or other materials provided with the
  distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
Comment: There is no license file included in the tarball. However, the FAQ
 http://idlastro.gsfc.nasa.gov/idlfaq.html#A14 mentiones that it is BSD-2.
 On the download page, there is also a file with the actual BSD-2 license:
 http://idlastro.gsfc.nasa.gov/ftp/LICENSE
 The text above and the copyright owner is taken from there.

Files: pro/query_irsa_cat.pro pro/read_ipac_table.pro pro/read_ipac_var.pro
 pro/write_ipac_table.pro
Copyright: 2013 California Institute of Technology
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  *  Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
  *  Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
 .
  *  Neither the name of the California Institute of Technology
     (Caltech) nor the names of its contributors may be used to
     endorse or promote products derived from this software without
     specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: pro/eqpole_grid.pro pro/qdcb_grid.pro pro/wcs_demo.pro
Copyright: 1991 The Regents of the University of California
License: BSD-alike
 This software has been authored by an employee or employees of Los Alamos
 National Security, LLC, operator of the Los Alamos National Laboratory (LANL)
 under Contract No. DE-AC52-06NA25396 with the U.S. Department of Energy. The
 U.S. Government has rights to use, reproduce, and distribute this software.
 The public may copy, distribute, prepare derivative works and publicly
 display this software without charge, provided that this Notice and any
 statement of authorship are reproduced on all copies.  Neither the Government
 nor LANS makes any warranty, express or implied, or assumes any liability or
 responsibility for the use of this software.  If software is modified to
 produce derivative works, such modified software should be clearly marked, so
 as not to confuse it with the version available from LANL.

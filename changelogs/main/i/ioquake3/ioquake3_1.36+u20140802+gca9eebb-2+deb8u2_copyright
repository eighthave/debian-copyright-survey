Format: http://dep.debian.net/deps/dep5/
Upstream-Name: ioquake3
Upstream-Contact:
  http://ioquake3.org/
  mailto:ioquake3@lists.ioquake.org
  irc://irc.freenode.net/ioquake3
Source: https://github.com/ioquake/ioq3/
  .
  This package was debianized by Bruno "Fuddl" Kleinert <fuddl@debian.org> on
  Sat, 17 Jul 2010 22:49:30 +0200.
  .
  The "original" tarball is based on a git export, edited to leave out
  non-free tools, third-party libraries etc. as described by the
  get-orig-source target in debian/rules; you can use
  'make -f debian/rules get-orig-source' to repeat this process.
  .
  Some of the omitted libraries are DFSG-free themselves, but since we need
  to repack the tarball anyway, omitting them too means we don't need to
  comply with as many licenses.

Files: *
Copyright:
  © 1999-2005 id Software Inc.
  © 1994-2007 Free Software Foundation, Inc.
  © 1997-1998 Andrew Tridgell
  © 2005-2009 Zachary J. Slater
  © 2005-2008 Joerg Dietrich
  © 2005-2007 Ludwig Nussel / Novell Inc.
  © 2005-2006 Tim Angus
  © 2005 Stuart Dalton
  © 2006 Tony J. White
  © 2008 Przemyslaw Iskra
  © 2009 David S. Miller
License: GPL-2+

License: GPL-2+
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.
  .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  .
  You can find the GPL license text on a Debian system under
  /usr/share/common-licenses/GPL-2.

Files: debian/*
Copyright:
  © 2010 Bruno Kleinert
  © 2010-2013 Simon McVittie
License: GPL-2+

Files: code/qcommon/md5.c
Copyright: no copyright is claimed
License: not-applicable
  The algorithm is due to Ron Rivest.  This code was
  written by Colin Plumb in 1993, no copyright is claimed.

Files:
  code/qcommon/unzip.?
  code/qcommon/ioapi.?
  code/qcommon/puff.?
  code/client/libmumblelink.?
Copyright:
  © 1990-2000 Info-ZIP
  © 1995-2005 Mark Adler
  © 1995-2005 Jean-loup Gailly
  © 1998-2005 Gilles Vollant
  © 2006 Joerg Dietrich
  © 2008 Ludwig Nussel
License: Zlib
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
  .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
  .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

Files: code/game/bg_lib.c
Copyright: © 1992, 1993 The Regents of the University of California
License: BSD-3-clause
  All rights reserved.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

Files: code/client/snd_adpcm.c
Copyright: © 1992 Stichting Mathematisch Centrum, Amsterdam, The Netherlands
License: other-adpcm
                        All Rights Reserved
  .
  Permission to use, copy, modify, and distribute this software and its 
  documentation for any purpose and without fee is hereby granted, 
  provided that the above copyright notice appear in all copies and that
  both that copyright notice and this permission notice appear in 
  supporting documentation, and that the names of Stichting Mathematisch
  Centrum or CWI not be used in advertising or publicity pertaining to
  distribution of the software without specific, written prior permission.
  .
  STICHTING MATHEMATISCH CENTRUM DISCLAIMS ALL WARRANTIES WITH REGARD TO
  THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS, IN NO EVENT SHALL STICHTING MATHEMATISCH CENTRUM BE LIABLE
  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
  OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

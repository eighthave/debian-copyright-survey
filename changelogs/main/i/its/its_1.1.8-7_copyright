This is the Debian GNU/Linux r-cran-its package of its, a package for
handling irregularly spaced timeseries in GNU R. Its was written by
the Portfolio & Risk Advisory Group, Commerzbank Securities, was
initially maintained upstream by Giles Heywood
<giles.heywood@commerzbankib.com>, and is currently maintained by Whit
Armstrong <Whit.Armstrong@tudor.com>.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'its' to 'r-cran-its'
to fit the pattern of CRAN (and non-CRAN) packages for R.

Copyright (C) 2003 - 2004 Commerzbank Securities
Copyright (C) 2004 - 2008 Whit Armstrong

License: GPL

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

   Package: its
   Version: 1.0.0
   Date: 2004/02/13
   Title:  Irregular Time Series
   Author:  Portfolio & Risk Advisory Group, Commerzbank Securities
   Maintainer: Giles Heywood <giles.heywood@commerzbankib.com>
   Depends: R (>= 1.8.0), methods, Hmisc
   Description: The its package contains an S4 class for handling irregular 
       time series
   License: GPL Version 2

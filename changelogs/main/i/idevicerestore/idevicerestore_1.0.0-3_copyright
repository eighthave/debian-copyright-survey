Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: idevicerestore
Source: https://libimobiledevice.org

Files: *
Copyright: idevicerestore authors
License: LGPL-3

Files: src/asr.c
 src/asr.h
 src/common.h
 src/dfu.c
 src/dfu.h
 src/idevicerestore.c
 src/idevicerestore.h
 src/img3.c
 src/img3.h
 src/ipsw.c
 src/ipsw.h
 src/normal.c
 src/normal.h
 src/recovery.c
 src/recovery.h
 src/restore.c
 src/restore.h
 src/tss.c
 src/tss.h
Copyright: 2010, Joshua Hill.
  2010, Martin Szulecki.
  2010-2012, Martin Szulecki.
  2010-2013, Martin Szulecki.
  2010-2015, Martin Szulecki.
  2012, Martin Szulecki.
  2012, Nikias Bassen.
  2012-2013, Nikias Bassen.
  2012-2015, Nikias Bassen.
  2012-2019, Nikias Bassen.
  2013, Martin Szulecki.
License: LGPL-2.1+

Files: src/fls.c
 src/fls.h
 src/ftab.c
 src/ftab.h
 src/img4.c
 src/img4.h
 src/json_plist.c
 src/json_plist.h
 src/limera1n.h
 src/limera1n_payload.h
 src/locking.c
 src/locking.h
 src/mbn.h
Copyright: 2012, Nikias Bassen.
  2012-2013, Nikias Bassen.
  2013, Nikias Bassen.
  2013-2019, Nikias Bassen.
  2019, Nikias Bassen.
License: LGPL-2.1+

Files: src/socket.c
 src/socket.h
 src/thread.c
 src/thread.h
Copyright: 2012, Martin Szulecki
  2012, Nikias Bassen
License: LGPL-2.1+

Files: src/download.c
 src/download.h
 src/mbn.c
Copyright: 2012, Martin Szulecki.
  2012, Nikias Bassen.
  2012-2013, Martin Szulecki.
  2012-2019, Nikias Bassen.
License: LGPL-2.1+

Files: src/jsmn.c
 src/jsmn.h
Copyright: 2010, Serge A. Zaitsev
License: Expat

Files: src/fdr.c
 src/fdr.h
Copyright: 2014, BALATON Zoltan.
License: LGPL-2.1+

Files: debian/*
Copyright: 2020 Yves-Alexis Perez <corsac@debian.org>
License: GPL-3+

Files: m4/ax_pthread.m4
Copyright: 2008, Steven G. Johnson <stevenj@alum.mit.edu>
  2011, Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL-3+

Files: src/limera1n.c
Copyright: 2010, Chronic-Dev Team
  2012, Martin Szulecki.
  2012-2013, Nikias Bassen.
License: LGPL-2.1+

Files: src/common.c
Copyright: 1991-2018, Free Software Foundation, Inc.
  2010, Joshua Hill.
  2012, Martin Szulecki.
  2012-2019, Nikias Bassen.
License: LGPL-2.1+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation
 version 3 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/LGPL-3'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-3'.

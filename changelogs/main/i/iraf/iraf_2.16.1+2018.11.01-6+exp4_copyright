Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IRAF
Upstream-Author: IRAF group at the National Optical Astronomy Observatories
Source: https://github.com/olebole/iraf-v216

Files: *
Copyright: 1986-2014 Association of Universities for Research in Astronomy Inc.
 2006-2009 Chisato Yamauchi
 2014 David Kuehling <dvdkhlng AT posteo TOD de>
 2017 Anastasia Galkin
 2017 Ole Streicher
 2018 Peter Green
 2018 James Cowgill <jcowgill AT debian TOD org>
 2018 Gustavo Romero, Rogerio Cardoso, Breno Leitao, IBM Corporation
License: IRAF

Files: pkg/*cl/ytab.*
Copyright: 1984-2006 Free Software Foundation, Inc
License: GPL-2+
Comment: As a special exception, you may create a larger work that
 contains part or all of the Bison parser skeleton and distribute that
 work under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof as
 a parser skeleton.  Alternatively, if you modify or redistribute the
 parser skeleton itself, you may (at your option) remove this special
 exception, which will cause the skeleton and the resulting Bison
 output files to be licensed under the GNU General Public License
 without this special exception.

Files: unix/hlib/libc/stdarg-gcc.h
Copyright: 1989-2000 Free Software Foundation, Inc.
License: GPL-2+
Comment: As a special exception, if you include this header file into
 source files compiled by GCC, this header file does not by itself
 cause the resulting executable to be covered by the GNU General
 Public License.  This exception does not however invalidate any other
 reasons why the executable file might be covered by the GNU General
 Public License.

Files: sys/gio/ncarutil/*
Copyright: 1986-2011 University Corporation for Atmospheric Research (UCAR)
License: expat

Files: unix/boot/xyacc/*
Copyright: 1988 AT&T,  2008 Sun Microsystems, Inc.
License: CDDL

Files: math/slalib/*
Copyright: 1996 Rutherford Appleton Laboratory
License: GPL-2+

Files: math/llsq/*
Copyright: ACM Publications
License: BSD-2-Clause
Comment: License clarified for scipy; see
 http://permalink.gmane.org/gmane.comp.python.scientific.devel/6725
 .
 This code was taken from the book "Charles L. Lawson and Richard
 J. Hanso:Solving Least Squares Problems", 1974.

Files: math/bevington/*
Copyright: not applicable
License: public-domain
 These files were copied from this book: Bevington, Robinson: Data
 Reduction and Error Analysis for the Physical Sciences. 1st ed. from
 1969. This code is from before 1976 and does not contain a copyright
 statement, so according to the Copyright Act of 1976 it is public
 domain.

Files: math/deboor/*
Copyright: not applicable
License: public-domain
 Carl Deboor wrote these files 1978 as an U.S. government employee.
 Reference: Carl DeBoor: A Practical Guide to Splines, 1978.

Files: math/fftpack/*
Copyright: not applicable
License: public-domain
 FFTPACK was developed by Paul Swarztrauber of the National Center for
 Atmospheric Research as an US government employeee.

Files:  math/minpack/*
Copyright: not applicable
License: public-domain
 The original minpack code (where the code in IRAF derivates from; it
 went into IRAF ~1992) was written by B. Garbow, K. Hillstrom,
 J. More' (Argonne National Laboratory, MINPACK project, in March
 1980) and is public domain.

Files: noao/astutil/asttools/asttimes.x
Copyright: 2013-2017 NumFOCUS Foundation
License: BSD-3-Clause
Comment: This covers only ast_julday_to_date.
 Derived, with permission, from the SOFA library

Files: math/lapack/*
Copyright: 1992-2017 The University of Tennessee and The University of Tennessee Research Foundation
 2000-2017 The University of California Berkeley
 2006-2017 The University of Colorado Denverf
License: BSD-3-Clause

Files: vendor/cfitsio/zlib/*
Copyright: 1995-2010 Jean-loup Gailly and Mark Adler
License: BSD-3-Clause

Files: vendor/cfitsio/cfortran.*
Copyright: 1990-2003 Burkhard Burow
License: LGPL-2+ or CFORTRAN

Files: unix/f2c/*
Copyright: 1990-2001 by AT&T, Lucent Technologies and Bellcore.
License: ATT

Files: debian/*
Copyright: 2012 Ole Streicher <debian@liska.ath.cx>
License: expat
Note: This package was first debianized by Zed Pobre <zed@debian.org>
 on Monday, 10 March 1998. From him, I took mainly the manpages.

License: IRAF
 The IRAF software is publicly available, but is NOT in the public domain.
 The difference is that copyrights granting rights for unrestricted use and
 redistribution have been placed on all of the software to identify its authors.
 You are allowed and encouraged to take this software and use it as you wish,
 subject to the restrictions outlined below.
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation is hereby granted without fee, provided that the above copyright
 notice appear in all copies and that both that copyright notice and this
 permission notice appear in supporting documentation, and that references to
 the Association of Universities for Research in Astronomy Inc. (AURA),
 the National Optical Astronomy Observatories (NOAO), or the Image Reduction
 and Analysis Facility (IRAF) not be used in advertising or publicity
 pertaining to distribution of the software without specific, written prior
 permission from NOAO.  NOAO makes no representations about the suitability
 of this software for any purpose.  It is provided "as is" without express or
 implied warranty.
 .
 NOAO DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL NOAO
 BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: expat
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Neither the names of NCAR's Computational and Information Systems Laboratory,
 the University Corporation for Atmospheric Research, nor the names of its
 contributors may be used to endorse or promote products derived from this
 Software without specific prior written permission.
 Redistributions of source code must retain the above copyright notices, this
 list of conditions, and the disclaimer below.
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions, and the disclaimer below in the documentation and/or
 other materials provided with the distribution.
 THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING, BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
 SOFTWARE.

License: CDDL
 COMMON DEVELOPMENT AND DISTRIBUTION LICENSE (CDDL)
 .
 Version 1.0
 .
 1. Definitions.
 .
 1.1. “Contributor” means each individual or entity that creates or
 contributes to the creation of Modifications.
 .
 1.2. “Contributor Version” means the combination of the Original
 Software, prior Modifications used by a Contributor (if any), and the
 Modifications made by that particular Contributor.
 .
 1.3. “Covered Software” means (a) the Original Software, or (b)
 Modifications, or (c) the combination of files containing Original
 Software with files containing Modifications, in each case including
 portions thereof.
 .
 1.4. “Executable” means the Covered Software in any form other than
 Source Code.
 .
 1.5. “Initial Developer” means the individual or entity that first
 makes Original Software available under this License.
 .
 1.6. “Larger Work” means a work which combines Covered Software or
 portions thereof with code not governed by the terms of this
 License.
 1.7. “License” means this document.
 .
 1.8. “Licensable” means having the right to grant, to the maximum
 extent possible, whether at the time of the initial grant or
 subsequently acquired, any and all of the rights conveyed herein.
 .
 1.9. “Modifications” means the Source Code and Executable form of any
 of the following:
 .
  A. Any file that results from an addition to, deletion from or
     modification of the contents of a file containing Original
     Software or previous Modifications;
  B. Any new file that contains any part of the Original Software or
     previous Modification; or
  C. Any new file that is contributed or otherwise made available
     under the terms of this License.
 .
 1.10. “Original Software” means the Source Code and Executable form
 of computer software code that is originally released under this
 License.
 .
 1.11. “Patent Claims” means any patent claim(s), now owned or
 hereafter acquired, including without limitation, method, process,
 and apparatus claims, in any patent Licensable by grantor.
 .
 1.12. “Source Code” means (a) the common form of computer software
 code in which modifications are made and (b) associated documentation
 included in or with such code.
 .
 1.13. “You” (or “Your”) means an individual or a legal entity
 exercising rights under, and complying with all of the terms of, this
 License. For legal entities, “You” includes any entity which
 controls, is controlled by, or is under common control with You. For
 purposes of this definition, “control” means (a) the power, direct or
 indirect, to cause the direction or management of such entity,
 whether by contract or otherwise, or (b) ownership of more than fifty
 percent (50%) of the outstanding shares or beneficial ownership of
 such entity.
 .
 2. License Grants.
 .
 2.1. The Initial Developer Grant.
 .
 Conditioned upon Your compliance with Section 3.1 below and subject
 to third party intellectual property claims, the Initial Developer
 hereby grants You a world-wide, royalty-free, non-exclusive license:
 .
 (a) under intellectual property rights (other than patent or
 trademark) Licensable by Initial Developer, to use, reproduce,
 modify, display, perform, sublicense and distribute the Original
 Software (or portions thereof), with or without Modifications, and/or
 as part of a Larger Work; and
 .
 (b) under Patent Claims infringed by the making, using or selling of
 Original Software, to make, have made, use, practice, sell, and offer
 for sale, and/or otherwise dispose of the Original Software (or
 portions thereof).
 .
 (c) The licenses granted in Sections 2.1(a) and (b) are effective on
 the date Initial Developer first distributes or otherwise makes the
 Original Software available to a third party under the terms of this
 License.
 .
 (d) Notwithstanding Section 2.1(b) above, no patent license is
 granted: (1) for code that You delete from the Original Software, or
 (2) for infringements caused by: (i) the modification of the Original
 Software, or (ii) the combination of the Original Software with other
 software or devices.
 .
 2.2. Contributor Grant.
 .
 Conditioned upon Your compliance with Section 3.1 below and subject
 to third party intellectual property claims, each Contributor hereby
 grants You a world-wide, royalty-free, non-exclusive license:
 .
 (a) under intellectual property rights (other than patent or
 trademark) Licensable by Contributor to use, reproduce, modify,
 display, perform, sublicense and distribute the Modifications created
 by such Contributor (or portions thereof), either on an unmodified
 basis, with other Modifications, as Covered Software and/or as part
 of a Larger Work; and
 .
 (b) under Patent Claims infringed by the making, using, or selling of
 Modifications made by that Contributor either alone and/or in
 combination with its Contributor Version (or portions of such
 combination), to make, use, sell, offer for sale, have made, and/or
 otherwise dispose of: (1) Modifications made by that Contributor (or
 portions thereof); and (2) the combination of Modifications made by
 that Contributor with its Contributor Version (or portions of such
 combination).
 .
 (c) The licenses granted in Sections 2.2(a) and 2.2(b) are effective
 on the date Contributor first distributes or otherwise makes the
 Modifications available to a third party.
 .
 (d) Notwithstanding Section 2.2(b) above, no patent license is
 granted: (1) for any code that Contributor has deleted from the
 Contributor Version; (2) for infringements caused by: (i) third party
 modifications of Contributor Version, or (ii) the combination of
 Modifications made by that Contributor with other software (except as
 part of the Contributor Version) or other devices; or (3) under
 Patent Claims infringed by Covered Software in the absence of
 Modifications made by that Contributor.
 .
 3. Distribution Obligations.
 .
 3.1. Availability of Source Code.
 .
 Any Covered Software that You distribute or otherwise make available
 in Executable form must also be made available  in Source Code form
 and that Source Code form must be distributed only under the terms of
 this License. You must include a copy of this License with every copy
 of the Source Code form of the Covered Software You distribute or
 otherwise make available. You must inform recipients of any such
 Covered Software in Executable form as to how they can obtain such
 Covered Software in Source Code form in a reasonable manner on or
 through a medium customarily used for software exchange.
 .
 3.2. Modifications.
 .
 The Modifications that You create or to which You contribute are
 governed by the terms of this License. You represent that You believe
 Your Modifications are Your original creation(s) and/or You have
 sufficient rights to grant the rights conveyed by this License.
 .
 3.3. Required Notices.
 .
 You must include a notice in each of Your Modifications that
 identifies You as the Contributor of the Modification.  You may not
 remove or alter any copyright, patent or trademark notices contained
 within the Covered Software, or any notices of licensing or any
 descriptive text giving attribution to any Contributor or the Initial
 Developer.
 .
 3.4. Application of Additional Terms.
 .
 You may not offer or impose any terms on any Covered Software in
 Source Code form that alters or restricts the applicable version of
 this License or the recipients’ rights hereunder. You may choose to
 offer, and to charge a fee for, warranty, support, indemnity or
 liability obligations to one or more recipients of Covered Software.
 However, you may do so only on Your own behalf, and not on behalf of
 the Initial Developer or any Contributor.  You must make it
 absolutely clear that any such warranty, support, indemnity or
 liability obligation is offered by You alone, and You hereby agree to
 indemnify the Initial Developer and every Contributor for any
 liability incurred by the Initial Developer or such Contributor as a
 result of warranty, support, indemnity or liability terms You offer.
 .
 3.5. Distribution of Executable Versions.
 .
 You may distribute the Executable form of the Covered Software under
 the terms of this License or under the terms of a license of Your
 choice, which may contain terms different from this License, provided
 that You are in compliance with the terms of this License and that
 the license for the Executable form does not attempt to limit or
 alter the recipient’s rights in the Source Code form from the rights
 set forth in this License. If You distribute the Covered Software in
 Executable form under a different license, You must make it
 absolutely clear that any terms which differ from this License are
 offered by You alone, not by the Initial Developer or
 Contributor. You hereby agree to indemnify the Initial Developer and
 every Contributor for any liability incurred by the Initial Developer
 or such Contributor as a result of any such terms You offer.
 .
 3.6. Larger Works.
 .
 You may create a Larger Work by combining Covered Software with other
 code not governed by the terms of this License and distribute the
 Larger Work as a single product. In such a case, You must make sure
 the requirements of this License are fulfilled for the Covered
 Software.
 .
 Versions of the License.
 .
 4.1. New Versions.
 .
 Sun Microsystems, Inc. is the initial license steward and may publish
 revised and/or new versions of this License from time to time. Each
 version will be given a distinguishing version number. Except as
 provided in Section 4.3, no one other than the license steward has
 the right to modify this License.
 .
 4.2. Effect of New Versions.
 .
 You may always continue to use, distribute or otherwise make the
 Covered Software available under the terms of the version of the
 License under which You originally received the Covered Software. If
 the Initial Developer includes a notice in the Original Software
 prohibiting it from being distributed or otherwise made available
 under any subsequent version of the License, You must distribute and
 make the Covered Software available under the terms of the version of
 the License under which You originally received the Covered
 Software. Otherwise, You may also choose to use, distribute or
 otherwise make the Covered Software available under the terms of any
 subsequent version of the License published by the license steward.
 .
 4.3. Modified Versions.
 .
 When You are an Initial Developer and You want to create a new
 license for Your Original Software, You may create and use a modified
 version of this License if You: (a) rename the license and remove any
 references to the name of the license steward (except to note that
 the license differs from this License); and (b) otherwise make it
 clear that the license contains terms which differ from this License.
 .
 5. DISCLAIMER OF WARRANTY.
 .
 COVERED SOFTWARE IS PROVIDED UNDER THIS LICENSE ON AN “AS IS” BASIS,
 WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 WITHOUT LIMITATION, WARRANTIES THAT THE COVERED SOFTWARE IS FREE OF
 DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR
 NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF
 THE COVERED SOFTWARE IS WITH YOU.  SHOULD ANY COVERED SOFTWARE PROVE
 DEFECTIVE IN ANY RESPECT, YOU (NOT THE INITIAL DEVELOPER OR ANY OTHER
 CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR
 CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART
 OF THIS LICENSE.  NO USE OF ANY COVERED SOFTWARE IS AUTHORIZED
 HEREUNDER EXCEPT UNDER THIS DISCLAIMER.
 .
 6. TERMINATION.
 .
 6.1. This License and the rights granted hereunder will terminate
 automatically if You fail to comply with terms herein and fail to
 cure such breach within 30 days of becoming aware of the
 breach. Provisions which, by their nature, must remain in effect
 beyond the termination of this License shall survive.
 .
 6.2. If You assert a patent infringement claim (excluding declaratory
 judgment actions) against Initial Developer or a Contributor (the
 Initial Developer or Contributor against whom You assert such claim
 is referred to as “Participant”) alleging that the Participant
 Software (meaning the Contributor Version where the Participant is a
 Contributor or the Original Software where the Participant is the
 Initial Developer) directly or indirectly infringes any patent, then
 any and all rights granted directly or indirectly to You by such
 Participant, the Initial Developer (if the Initial Developer is not
 the Participant) and all Contributors under Sections 2.1 and/or 2.2
 of this License shall, upon 60 days notice from Participant terminate
 prospectively and automatically at the expiration of such 60 day
 notice period, unless if within such 60 day period You withdraw Your
 claim with respect to the Participant Software against such
 Participant either unilaterally or pursuant to a written agreement
 with Participant.
 .
 6.3. If You assert a patent infringement claim against Participant
 alleging that the Participant Software directly or indirectly
 infringes any patent where such claim is resolved (such as by license
 or settlement) prior to the initiation of patent infringement
 litigation, then the reasonable value of the licenses granted by such
 Participant under Sections 2.1 or 2.2 shall be taken into account in
 determining the amount or value of any payment or license.
 .
 6.4. In the event of termination under Sections 6.1 or 6.2 above, all
 end user licenses that have been validly granted by You or any
 distributor hereunder prior to termination (excluding licenses
 granted to You by any distributor) shall survive termination.
 .
 7. LIMITATION OF LIABILITY.
 .
 UNDER NO CIRCUMSTANCES AND UNDER NO LEGAL THEORY, WHETHER TORT
 (INCLUDING NEGLIGENCE), CONTRACT, OR OTHERWISE, SHALL YOU, THE
 INITIAL DEVELOPER, ANY OTHER CONTRIBUTOR, OR ANY DISTRIBUTOR OF
 COVERED SOFTWARE, OR ANY SUPPLIER OF ANY OF SUCH PARTIES, BE LIABLE
 TO ANY PERSON FOR ANY INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES OF ANY CHARACTER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR
 LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR
 ANY AND ALL OTHER COMMERCIAL DAMAGES OR LOSSES, EVEN IF SUCH PARTY
 SHALL HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGES. THIS
 LIMITATION OF LIABILITY SHALL NOT APPLY TO LIABILITY FOR DEATH OR
 PERSONAL INJURY RESULTING FROM SUCH PARTY’S NEGLIGENCE TO THE EXTENT
 APPLICABLE LAW PROHIBITS SUCH LIMITATION. SOME JURISDICTIONS DO NOT
 ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL
 DAMAGES, SO THIS EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU.
 .
 8. U.S. GOVERNMENT END USERS.
 .
 The Covered Software is a “commercial item,” as that term is defined
 in 48 C.F.R. 2.101 (Oct. 1995), consisting of “commercial computer
 software” (as that term is defined at 48 C.F.R. § 252.227-7014(a)(1))
 and “commercial computer software documentation” as such terms are
 used in 48 C.F.R. 12.212 Sept. 1995). Consistent with 48
 C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4 (June
 1995), all U.S. Government End Users acquire Covered Software with
 only those rights set forth herein. This U.S. Government Rights
 clause is in lieu of, and supersedes, any other FAR, DFAR, or other
 clause or provision that addresses Government rights in computer
 software under this License.
 .
 9. MISCELLANEOUS.
 .
 This License represents the complete agreement concerning subject
 matter hereof. If any provision of this License is held to be
 unenforceable, such provision shall be reformed only to the extent
 necessary to make it enforceable. This License shall be governed by
 the law of the jurisdiction specified in a notice contained within
 the Original Software (except to the extent applicable law, if any,
 provides otherwise), excluding such jurisdiction’s conflict-of-law
 provisions. Any litigation relating to this License shall be subject
 to the jurisdiction of the courts located in the jurisdiction and
 venue specified in a notice contained within the Original Software,
 with the losing party responsible for costs, including, without
 limitation, court costs and reasonable attorneys’ fees and
 expenses. The application of the United Nations Convention on
 Contracts for the International Sale of Goods is expressly
 excluded. Any law or regulation which provides that the language of a
 contract shall be construed against the drafter shall not apply to
 this License. You agree that You alone are responsible for compliance
 with the United States export administration regulations (and the
 export control laws and regulation of any other countries) when You
 use, distribute or otherwise make available any Covered Software.
 .
 10. RESPONSIBILITY FOR CLAIMS.
 .
 As between Initial Developer and the Contributors, each party is
 responsible for claims and damages arising, directly or indirectly,
 out of its utilization of rights under this License and You agree to
 work with Initial Developer and Contributors to distribute such
 responsibility on an equitable basis. Nothing herein is intended or
 shall be deemed to constitute any admission of liability.
 .
 NOTICE PURSUANT TO SECTION 9 OF THE COMMON DEVELOPMENT AND
 DISTRIBUTION LICENSE (CDDL)
 .
 The OpenSolaris code released under the CDDL shall be governed by the
 laws of the State of California (excluding conflict-of-law
 provisions). Any litigation relating to this License shall be subject
 to the jurisdiction of the Federal Courts of the Northern District of
 California and the state courts of the State of California, with
 venue lying in Santa Clara County, California.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1 Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 2 Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 3 Neither the name of the Standards Of Fundamental Astronomy Board, the
   International Astronomical Union nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: ATT
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies and that both that the copyright notice and this
 permission notice and warranty disclaimer appear in supporting
 documentation, and that the names of AT&T Bell Laboratories or
 Bellcore or any of their entities not be used in advertising or
 publicity pertaining to distribution of the software without
 specific, written prior permission.
 .
 AT&T and Bellcore disclaim all warranties with regard to this
 software, including all implied warranties of merchantability
 and fitness.  In no event shall AT&T or Bellcore be liable for
 any special, indirect or consequential damages or any damages
 whatsoever resulting from loss of use, data or profits, whether
 in an action of contract, negligence or other tortious action,
 arising out of or in connection with the use or performance of
 this software.

License: CFORTRAN
 THIS PACKAGE, I.E. CFORTRAN.H, THIS DOCUMENT, AND THE CFORTRAN.H EXAMPLE
 PROGRAMS ARE PROPERTY OF THE AUTHOR WHO RESERVES ALL RIGHTS. THIS PACKAGE AND
 THE CODE IT PRODUCES MAY BE FREELY DISTRIBUTED WITHOUT FEES, SUBJECT TO THE
 FOLLOWING RESTRICTIONS:
 - YOU MUST ACCOMPANY ANY COPIES OR DISTRIBUTION WITH THIS (UNALTERED) NOTICE.
 - YOU MAY NOT RECEIVE MONEY FOR THE DISTRIBUTION OR FOR ITS MEDIA
   (E.G. TAPE, DISK, COMPUTER, PAPER.)
 - YOU MAY NOT PREVENT OTHERS FROM COPYING IT FREELY.
 - YOU MAY NOT DISTRIBUTE MODIFIED VERSIONS WITHOUT CLEARLY DOCUMENTING YOUR
   CHANGES AND NOTIFYING THE AUTHOR.
 - YOU MAY NOT MISREPRESENTED THE ORIGIN OF THIS SOFTWARE, EITHER BY EXPLICIT
   CLAIM OR BY OMISSION.
 .
 THE INTENT OF THE ABOVE TERMS IS TO ENSURE THAT THE CFORTRAN.H PACKAGE NOT BE
 USED FOR PROFIT MAKING ACTIVITIES UNLESS SOME ROYALTY ARRANGEMENT IS ENTERED
 INTO WITH ITS AUTHOR.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
 EXPRESSED OR IMPLIED. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
 SOFTWARE IS WITH YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST
 OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. THE AUTHOR IS NOT RESPONSIBLE
 FOR ANY SUPPORT OR SERVICE OF THE CFORTRAN.H PACKAGE.

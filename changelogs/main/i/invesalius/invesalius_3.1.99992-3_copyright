Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: InVesalius
Upstream-Contact: InVesalius Team <invesalius@cti.gov.br>
                  Paulo Henrique Junqueira Amorim <paulojamorim@gmail.com>
                  Thiago Franco de Moraes <totonixsame@gmail.com>
                  Tatiana Al-Chueyr <tatiana.alchueyr@gmail.com>
		  Guilherme Cesar Soares Ruppert <guilherme.ruppert@cti.gov.br>
		  Fabio de Souza Azevedo <fabio.azevedo@cti.gov.br>
		  Jorge Vicente Lopes da Silva <jorge.silva@cti.gov.br>
Source: https://github.com/invesalius/invesalius3.git

Files: *
Copyright: © 2008-2017 Centro de Tecnologia da Informação Renato Archer <invesalius@cti.gov.br>
                       Paulo Henrique Junqueira Amorim <paulojamorim@gmail.com>,
                       Thiago Franco de Moraes <totonixsame@gmail.com>
                       Tatiana Al-Chueyr Pereira Martins <tatiana.alchueyr@gmail.com>,
License: GPL-2

Files: presets/raycasting/*
Copyright: © 2003-2010 Antoine Rosset
License: LGPL-3.0
 On Debian GNU/Linux systems the full text of LGPL-3 license can be
 found at /usr/share/common-licenses/LGPL-3
Comment: These files came from OsiriX (http://www.osirix-viewer.com/). OsiriX is
 licensed as LGPL-3 and these files seem to be as well, but it is not explicit,
 so I'm not sure.

Files: invesalius/gui/widgets/listctrl.py
Copyright: © 2001 by Total Control Software
License: WXwindows

Files: invesalius/data/transformations.py
Copyright: © 2006-2015, Christoph Gohlke
           © 2006-2015, The Regents of the University of California
           Produced at the Laboratory for Fluorescence Dynamics
License: BSD-3-Clause

Files: debian/*
Copyright: © 2009-2017 Centro de Tecnologia da Informação Renato Archer <invesalius@cti.gov.br>
           Andreas Tille <tille@debian.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, you can find the GPL license version 2 in:
 /usr/share/common-licenses/GPL-2


License: WXwindows
                wxWindows Library Licence, Version 3
                ====================================
  . 
  Copyright (c) 1998 Julian Smart, Robert Roebling et al
  .
  Everyone is permitted to copy and distribute verbatim copies
  of this licence document, but changing it is not allowed.
  .
                       WXWINDOWS LIBRARY LICENCE
     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
  .  
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public Licence as published by
  the Free Software Foundation; either version 2 of the Licence, or (at
  your option) any later version.
  .  
  This library is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
  General Public Licence for more details.
  .
  You should have received a copy of the GNU Library General Public Licence
  along with this software, usually in a file named COPYING.LIB.  If not,
  write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA 02110-1301, USA.
  .
  EXCEPTION NOTICE
  .
  1. As a special exception, the copyright holders of this library give
  permission for additional uses of the text contained in this release of
  the library as licenced under the wxWindows Library Licence, applying
  either version 3 of the Licence, or (at your option) any later version of
  the Licence as published by the copyright holders of version 3 of the
  Licence document.
  .
  2. The exception is that you may use, copy, link, modify and distribute
  under the user's own terms, binary object code versions of works based
  on the Library.
  .
  3. If you copy code from files distributed under the terms of the GNU
  General Public Licence or the GNU Library General Public Licence into a
  copy of this library, as this licence permits, the exception does not
  apply to the code that you add in this way.  To avoid misleading anyone as
  to the status of such modified files, you must delete this exception
  notice from such code and/or adjust the licensing conditions notice
  accordingly.
  .
  4. If you write modifications of your own for this library, it is your
  choice whether to permit this exception to apply to your modifications. 
  If you do not wish that, you must delete the exception notice from such
  code and/or adjust the licensing conditions notice accordingly.


License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of the copyright holders nor the names of any
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

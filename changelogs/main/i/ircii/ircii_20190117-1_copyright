Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ircii
Source: http://ircii.warped.com/

Files: *
Copyright: Copyright 1990 Michael Sandrof.
 1991, 1992 Troy Rollo.
 1991 Carl V. Loesch.
 1993 Jeremy Nelson.
 1995 Scott Reynolds.
 1998 glen mccready.
 1999,2003,2004 Joel Yliluoma.
 1992-2017 Matthew R. Green.
License: BSD-3-Clause

Files: source/sl.c
Copyright: 1994 Christos Zoulas
License: BSD-2-Clause

Files: ircbug.in
Copyright: 1993 Free Software Foundation, Inc.
 Brendan Kehoe <brendan@cygnus.com>
License: GPL-2.0+

Files: source/reg.c
Copyright: 2014 Douglas A. Lewis <dalewis@cs.Buffalo.EDU>
License: public-domain

Files: source/getaddrinfo.c
Copyright: 1995, 1996, 1997, and 1998 WIDE Project.
License: BSD-3-Clause

Files: source/snprintf.c
Copyright: 1995 Patrick Powell <papowell@astart.com>
 1996-1997 Brandon Long <blong@fiction.net>
 1998 Thomas Roessler <roessler@guug.de>
 1998 Michael Elkins <me@cs.hmc.edu>
 1998 Andrew Tridgell <tridge@samba.org>
 1999 Luke Mewburn <lukem@netbsd.org>
License: BSD-3-Clause

Files: source/inet_ntop.c source/inet_pton.c
Copyright: 1996 Internet Software Consortium
License: MIT-Old-Style-with-legal-disclaimer-2

Files: source/scandir.c
Copyright: 1983 Regents of the University of California
License: Custom

Files: debian/*
Copyright: 1995 Carl Streeter <streeter@cae.wisc.edu>
 1996-2006 Bernd Eckenfels <ecki@debian.org>
 2008 Tobias Klauser <tklauser@distanz.ch>
 2008 Peter Eisentraut <petere@debian.org>
 2011 Neil Williams <codehelp@debian.org>
 2011 Bernhard R. Link <brlink@debian.org>
 2016-2017 Daniel Echeverry <epsilon77@gmail.com>
License: BSD-3-Clause

License: public-domain
 This file is in the public domain.

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Custom
 Redistribution and use in source and binary forms are permitted provided that
 he above copyright notice and this paragraph are duplicated in all such
 forms and that any documentation, advertising materials, and other
 materials related to such distribution and use acknowledge that the
 software was developed by the University of California, Berkeley.  The
 name of the University may not be used to endorse or promote products
 derived from this software without specific prior written permission. THIS
 SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation and/or
    other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors may be
    used to endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: MIT-Old-Style-with-legal-disclaimer-2
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE CONSORTIUM DISCLAIMS
 ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET SOFTWARE
 CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

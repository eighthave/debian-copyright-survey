Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: igdiscover
Source: https://github.com/NBISweden/IgDiscover

Files: *
Copyright: 2016 Marcel Martin <marcel.martin@scilifelab.se>
 IgDiscover is the result of a collaboration between the
 `Gunilla Karlsson Hedestam group <http://ki.se/en/mtc/gunilla-karlsson-hedestam-group>`_
 at the
 `Department of Microbiology, Tumor and Cell Biology <http://ki.se/en/mtc/>`_
 at `Karolinska Institutet <http://ki.se/en/>`_, Sweden
 and the
 `Bioinformatics Long-Term Support <https://www.scilifelab.se/facilities/wabi/>`_
 facility at
 `Science for Life Laboratory (SciLifeLab) <https://www.scilifelab.se/>`_, Sweden.
License: MIT

Files: doc/fulltoc.py
Copyright: 2012 Doug Hellmann <doug.hellmann@dreamhost.com>
           New Dream Network, LLC (DreamHost)
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".


Files: versioneer.py
Copyright: Brian Warner
License: Public-Domain
 Available from https://github.com/warner/python-versioneer, the
 authors waives his copyrights for this work.


Files: debian/*
Copyright: 2019 Steffen Moeller <moeller@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

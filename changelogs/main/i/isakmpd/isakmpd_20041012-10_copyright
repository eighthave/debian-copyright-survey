This package have been packaged by Jean-Francois Dive <jef@debian.org> as 
isakmpd.  The upstream source of isakmpd can be found at www.openbsd.org

This package is now maintained by Jochen Friedrich <jochen@scram.de>

- This package links against openssl.
- This package include libdes from the openbsd tree which have the same 
  license as openssl, please refer to the following license statement for details.

 * Copyright (c) 1998 Niklas Hallqvist.  All rights reserved.
 * Copyright (c) 2004 Håkan Olsson.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This is isakmpd, a BSD-licensed ISAKMP/Oakley (a.k.a. IKE)
implementation.  It's written by Niklas Hallqvist and Niels Provos,
funded by Ericsson Radio Systems AB.  Isakmpd's home is in the
OpenBSD main source tree under src/sbin/isakmpd.  Look at
http://www.openbsd.org/ for details on how to get OpenBSD source.

The isakmpd license is the BSD license, please refer to 
/usr/share/common-license/BSD for details. The few code modification in isakmpd
(Linux support) are authored by Jean-Francois Dive and Jochen Friedrich 
and are release on the same license as isakmpd itself.

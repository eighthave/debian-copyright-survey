Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: irony-mode
Source: https://github.com/Sarcasm/irony-mode

Files: *
Copyright: 2011-2016 Guillaume Papin <guillaume.papin@epitech.eu>
License: GPL-3+

Files: irony-cdb-libclang.el
Copyright: 2015 Karl Hylén <karl.hylen@gmail.com>
License: GPL-3+

Files: server/build-aux/run-clang-tidy/run-clang-tidy.py
Copyright: 2014-2016 University of Illinois at Urbana-Champaign
           2014 Benjamin Kramer <benny.kra@googlemail.com>
           2017 Guillaume Papin <guillaume.papin@epitech.eu>
License: NCSA
Comment: Original commit by bkramer -> https://reviews.llvm.org/D5188

Files: debian/*
Copyright: 2014 Christoph Egger
           2017-2018 Nicholas D Steeves <nsteeves@gmail.com>
License: ISC

License: ISC
 Copyright (c) 2014 Christoph Egger
 .
 Permission to use, copy, modify, and/or distribute this software for
 any purpose with or without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux System you can find a copy of the GNU General
 Public License in "/usr/share/common-licenses/GPL-3".

License: NCSA
 ==============================================================================
 LLVM Release License
 ==============================================================================
 University of Illinois/NCSA
 Open Source License
 .
 Copyright (c) 2007-2016 University of Illinois at Urbana-Champaign.
 All rights reserved.
 .
 Developed by:
 .
     LLVM Team
 .
     University of Illinois at Urbana-Champaign
 .
     http://llvm.org
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal with
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimers.
 .
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimers in the
       documentation and/or other materials provided with the distribution.
 .
     * Neither the names of the LLVM Team, University of Illinois at
       Urbana-Champaign, nor the names of its contributors may be used to
       endorse or promote products derived from this Software without specific
       prior written permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
 SOFTWARE.

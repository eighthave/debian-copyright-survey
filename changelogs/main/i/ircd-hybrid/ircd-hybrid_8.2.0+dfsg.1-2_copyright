Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IRCD-Hybrid
Upstream-Contact: bugs@ircd-hybrid.org
Source: http://www.ircd-hybrid.org/downloads.html

Files: *
Copyright: 1988, 1989, 1990, 1991 University of Oulu, Computing Center
           1997-2014 ircd-hybrid development team
License: GPL-2+

Files: contrib/m_webirc.c
Copyright: 1990 Jarkko Oikarinen and University of Oulu, Co Center
           2002-2006 ircd-ratbox development team
           1996-2012 Hybrid Development Team
License: GPL-2+

Files: include/conf_db.h src/conf_db.c
Copyright: 1996-2009 Andrew Church <achurch@achurch.org>
           2012 Hybrid Development Team
License: GPL-2+

Files: include/mempool.h src/mempool.c
Copyright: 2007-2012 The Tor Project, Inc.
License: BSD-3-clause

Files: include/motd.h src/motd.c
Copyright: 2000 Kevin L. Mitchell <klmitch@mit.edu>
           2013 Hybrid Development Team
License: GPL-2+

Files: include/rng_mt.h
Copyright: 1997 - 2002, Makoto Matsumoto and Takuji Nishimura
License: BSD-3-clause

Files: include/watch.h
Copyright: 1997 Jukka Santala (Donwulff)
           2005 by the Hybrid Development Team
License: GPL-2+

Files: include/stdinc.h
Copyright: 2002 Aaron Sethman <androsyn@ratbox.org>
License: GPL-2+

Files: tools/mkpasswd.c
Copyright: 1991 Nelson Minar (minar@reed.edu)
License: Nelson-Minar
 You can use this code as long as my name stays with it.

Files: doc/technical/hostmask.txt
Copyright: 2001 by Andrew Miller(A1kmm)<a1kmm@mware.virtualave.net>
License: GPL-2+

Files: modules/m_svsmode.c modules/m_svsnick.c modules/m_services.c
Copyright: 1999 by the Bahamut Development Team.
License: GPL-2+

Files: modules/m_cap.c
Copyright: 2004 Kevin L. Mitchell <klmitch@mit.edu>
           2006-2014 ircd-hybrid development team
License: GPL-2+

Files: modules/m_watch.c src/watch.c
Copyright: 1997 Jukka Santala (Donwulff)
           2000-2014 ircd-hybrid development team
License: GPL-2+

Files: src/rng_mt.c
Copyright: 1997 - 2002, Makoto Matsumoto and Takuji Nishimura
License: BSD-3-clause

Files: src/reslib.c
Copyright: 1985, 1993 The Regents of the University of California
           1993 by Digital Equipment Corporation
           1996-1999 by Internet Software Consortium
License: BSD-4-clause
 Copyright (c) 1985, 1993
    The Regents of the University of California.  All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
      This product includes software developed by the University of
      California, Berkeley and its contributors.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 .
 Note: clause 3 has been rescinded by the copyright holder.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
   3. The names of its contributors may not be used to endorse or promote
      products derived from this software without specific prior written
      permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

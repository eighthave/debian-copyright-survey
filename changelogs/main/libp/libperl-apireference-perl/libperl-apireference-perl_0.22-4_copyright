Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Perl-APIReference
Upstream-Contact: Steffen Mueller <smueller@cpan.org>
Source: https://metacpan.org/release/Perl-APIReference

Files: *
Copyright: 2009-2015, Steffen Mueller <smueller@cpan.org>
License: Artistic or GPL-1+

Files: data/*
Copyright: Copyright 1989-2001, Larry Wall  All rights reserved.
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2010, Ryan Niebur <ryan@debian.org>
 2010, Damyan Ivanov <dmn@debian.org>
 2011, Jonathan Yu <jawnsy@cpan.org>
 2011-2016, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

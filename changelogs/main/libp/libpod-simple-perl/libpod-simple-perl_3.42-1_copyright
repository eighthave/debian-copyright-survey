Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Marc Green <marcgreen@cpan.org>
Source: https://metacpan.org/release/Pod-Simple
Upstream-Name: Pod-Simple

Files: *
Copyright: 2002, Sean M. Burke <sburke@cpan.org>
License: Artistic or GPL-1+

Files: lib/Pod/Simple/XHTML.pm
Copyright: 2003-2005, Allison Randal <allison@perl.org>
License: Artistic or GPL-1+

Files: lib/Pod/Simple/XMLOutStream.pm lib/Pod/Simple/HTML.pm
Copyright: 2002-2004, Sean M. Burke <sburke@cpan.org>
License: Artistic or GPL-1+

Files: t/perlfaq.pod t/perlfaqo.txt
Copyright: 1997-1999, Tom Christiansen <tchrist@mox.perl.com>
 1997-1999, Nathan Torkington <nathan@torkington.com>
License: Artistic or GPL-1+

Files: t/JustPod01.t
Copyright: 2009, Michael Schwern <mschwern@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2003-2006, Jay Bonci <jaybonci@debian.org>
 2007, Gunnar Wolf <gwolf@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
 2009-2011, Jonathan Yu <jawnsy@cpan.org>
 2010, Wen Heping <wenheping@gmail.com>
 2010-2020, gregor herrmann <gregoa@debian.org>
 2011, Ansgar Burchardt <ansgar@debian.org>
 2012-2013, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

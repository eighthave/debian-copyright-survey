Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Pod-Weaver-Section-Legal-Complicated
Upstream-Contact: Carnë Draug <cdraug@cpan.org>
Upstream-Name: Pod-Weaver-Section-Legal-Complicated

Files: *
Copyright: 2013-2017, Carnë Draug <cdraug@cpan.org>
License: GPL-3+
Comment: The distribution metadata refers to gpl_3 but the actual perl
 module itself has the "or (at your option) any later version" clause.

Files: debian/*
Copyright: 2017, Carnë Draug <carandraug+dev@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

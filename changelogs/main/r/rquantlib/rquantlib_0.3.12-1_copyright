This is the Debian GNU/Linux r-cran-rquantlib package of RQuantLib, an
interface package for GNU R providing access the QuantLib finance library.
RQuantLib was written by Dirk Eddelbuettel.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://dirk.eddelbuettel.com/code/rquantlib/
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'RQuantLib' to 
'r-cran-rquantlib' to highlight the fact that is it a CRAN package for R.

Copyright (C) 2003 - 2008 Dirk Eddelbuettel

License: GPL (>= 2)

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

    Package: RQuantLib
    Title: R interface to the QuantLib libraries
    Version: 0.1.4
    Date: $Date: 2003/03/25 23:41:15 $
    Maintainer: Dirk Eddelbuettel <edd@debian.org>
    Author: Dirk Eddelbuettel <edd@debian.org> for the R interface, the 
    	QuantLib group for QuantLib 
        (c.f. http://www.quantlib.org/html/group.html)
    Description: The RQuantLib packages provides access to (some) of the 
        QuantLib functions from within R. It is currently limited to some 
    	Option pricing and analysis functions.
        The QuantLib project aims to provide a comprehensive software 
        framework for quantitative finance. The goal is to provide a 
    	standard free/open source library to quantitative analysts and
    	developers for modeling, trading, and risk management of financial
    	assets.
        You need to have QuantLib installed in order to use RQuantLib.
    Depends: R (>= 1.5.0)
    License: GPL Version 2 or later for RQuantLib; QuantLib itself is released
            as Open Source as well
    URL: http://quantlib.org http://dirk.eddelbuettel.com/code/rquantlib.html

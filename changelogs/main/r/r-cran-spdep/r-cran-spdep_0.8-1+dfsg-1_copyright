Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: spdep
Upstream-Contact: Roger Bivand <Roger.Bivand@nhh.no>
Source: https://cran.r-project.org/package=spdep
Files-Excluded: */inst/doc/*.html

Files: *
Copyright: 2008-2016 Roger Bivand,
                     Micah Altman,
                     Luc Anselin,
                     Renato Assunção,
                     Olaf Berke,
                     Andrew Bernat,
                     Guillaume Blanchet,
                     Eric Blankmeyer,
                     Marilia Carvalho,
                     Bjarke Christensen,
                     Yongwan Chun,
                     Carsten Dormann,
                     Stéphane Dray,
                     Virgilio Gómez-Rubio,
                     Rein Halbersma,
                     Elias Krainski,
                     Pierre Legendre,
                     Nicholas Lewin-Koh,
                     Hongfei Li,
                     Jielai Ma,
                     Giovanni Millo,
                     Werner Mueller,
                     Hisaji Ono,
                     Pedro Peres-Neto,
                     Gianfranco Piras,
                     Markus Reder,
                     Michael Tiefelsdorf,
                     Danlin Yu
License: GPL-2+

Files: src/dnn.c src/knn.c
Copyright: 1994-1999 W. N. Venables and B. D. Ripley
           2001-2003 Roger Bivand
License: GPL-2+

Files: src/gabriel.c
       src/relative.c
       R/components.R
       R/gabrielneigh.R
       R/graph2nb.R
       R/nboperations.R
       R/relneigh.R
       R/soi.R
       man/compon.Rd
       man/graphneigh.Rd
       man/nboperations.Rd
Copyright: 2001 Nicholas Lewin-Koh
License: GPL-2+

Files: R/lag.spsarlm.R
Copyright: 1998-2002 Roger Bivand and Andrew Bernat
License: GPL-2+

Files: R/globalG.R
Copyright: 2002 Hisaji ONO and Roger Bivand
License: GPL-2+

Files: R/mtlocalmoran.R R/mtmoran.R
Copyright: 2002 by Roger Bivand and Michael Tiefelsdorf
License: GPL-2+

Files: R/EBI.R
Copyright: 2003 by Roger Bivand and Marilio Carvalho
License: GPL-2+

Files: R/p.adjustSP.R
Copyright: 2004 by Roger Bivand and Danlin Yu
License: GPL-2+

Files: R/autocov.R
Copyright: 2005 Carsten F. Dormann and Roger Bivand
License: GPL-2+

Files: R/kpgm_new.R
Copyright: 2005 Luc Anselin and Roger Bivand
License: GPL-2+

Files: R/SpatialFiltering.R
Copyright: 2005 Yongwan Chun, Michael Tiefelsdorf and Roger Bivand
License: GPL-2+

Files: R/bptest.sarlm.R
Copyright: 2004-2011 Roger Bivand
           original taken from bptest() in the lmtest package, 2001 Torsten Hothorn and Achim Zeileis
License: GPL-2+

Files: R/lextrB.R
Copyright: 2015 Roger S. Bivand, Yongwan Chun and Daniel A. Griffith
License: GPL-2+

Files: R/nb2mat.R
Copyright: 2001-2010 Roger Bivand, Markus Reder and Werner Mueller
           2015 Martin Gubri
License: GPL-2+

Files: debian/*
Copyright: 2014-2016 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

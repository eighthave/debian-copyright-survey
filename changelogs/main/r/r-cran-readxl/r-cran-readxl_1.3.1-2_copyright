This is the Debian GNU/Linux r-cran-readxl package of readxl.  The
readxl package let R read xls and xlsx files. It was written by Hadley
Wickham.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'readxl' to
'r-cran-readxl to fit the pattern of CRAN (and non-CRAN) packages
for R.

Files: src/*
Copyright: 2004 Komarov Valery
Copyright: 2006 Christophe Leitienne
Copyright: 2013 Bob Colbert
Copyright: 2008-2013 David Hoerl
License: BSD-2

Files: R/*
Files: src/*
Copyright: 2015 Hadley Wickham
License: GPL-3

Files: debian/*
Copyright: 2014  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2, and the GPL license
(version 3) is included in the file /usr/share/common-licenses/GPL-3.

The included BSD-2 license is:

  Redistribution and use in source and binary forms, with or without modification, are
  permitted provided that the following conditions are met:
  
     1. Redistributions of source code must retain the above copyright notice, this list of
        conditions and the following disclaimer.
  
     2. Redistributions in binary form must reproduce the above copyright notice, this list
        of conditions and the following disclaimer in the documentation and/or other materials
        provided with the distribution.
  
  THIS SOFTWARE IS PROVIDED BY David Hoerl ''AS IS'' AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL David Hoerl OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
  

For reference, the upstream DESCRIPTION file is included below:

  Package: readxl
  Title: Read Excel Files
  Version: 0.1.0
  Authors@R: c(
      person("Hadley", "Wickham", ,"hadley@rstudio.com", role = c("aut", "cre")),
      person("RStudio", role = "cph", comment = "Copyright holder of all R code
        and all C/C++ code without explicit copyright attribution"),
      person("Marcin", "Kalicinski", role = c("ctb", "cph"), comment = "Author of included RapidXML code"),
      person("Komarov Valery", role = c("ctb", "cph"), comment = "Author of included libxls code"),
      person("Christophe Leitienne", role = c("ctb", "cph"), comment = "Author of included libxls code"),
      person("Bob Colbert", role = c("ctb", "cph"), comment = "Author of included libxls code"),
      person("David Hoerl", role = c("ctb", "cph"), comment = "Author of included libxls code")
      )
  Description: Import excel files into R. Supports '.xls' via the embedded 'libxls'
      C library (http://sourceforge.net/projects/libxls/) and '.xlsx' via the
      embedded 'RapidXML' C++ library (http://rapidxml.sourceforge.net). Works
      on Windows, Mac and Linux without external dependencies.
  License: GPL-3 + file LICENSE
  LazyData: true
  LinkingTo: Rcpp
  Imports: Rcpp (>= 0.11.5)
  Suggests: testthat
  Packaged: 2015-04-14 13:24:38 UTC; hadley
  Author: Hadley Wickham [aut, cre],
    RStudio [cph] (Copyright holder of all R code and all C/C++ code
      without explicit copyright attribution),
    Marcin Kalicinski [ctb, cph] (Author of included RapidXML code),
    Komarov Valery [ctb, cph] (Author of included libxls code),
    Christophe Leitienne [ctb, cph] (Author of included libxls code),
    Bob Colbert [ctb, cph] (Author of included libxls code),
    David Hoerl [ctb, cph] (Author of included libxls code)
  Maintainer: Hadley Wickham <hadley@rstudio.com>
  NeedsCompilation: yes
  Repository: CRAN
  Date/Publication: 2015-04-14 16:28:42

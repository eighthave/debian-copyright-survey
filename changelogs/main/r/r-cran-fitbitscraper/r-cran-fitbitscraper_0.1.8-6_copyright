Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fitbitScraper
Upstream-Contact: Cory Nissen <corynissen@gmail.com>
Source: https://cran.r-project.org/package=fitbitScraper

Files: *
Copyright: 2014-2017 Cory Nissen
License: Expat
Comment: The CRAN policy is to encourage package authors to no include
 the license text "to save space". We as in Debian are aware that this
 is illegal strictly speaking but it seems we will not solve this issue
 package wise with every single package author. The better solution
 would be to form an Debian R team which should discuss this with CRAN
 maintainers in general.

Files: debian/*
Copyright: 2016-2017 Dylan Aïssi <bob.dybian at gmail.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

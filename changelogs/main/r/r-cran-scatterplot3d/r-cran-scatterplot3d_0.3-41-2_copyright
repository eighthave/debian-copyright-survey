Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: scatterplot3d
Upstream-Contact: Uwe Ligges <ligges@statistik.tu-dortmund.de>
Source: https://cran.r-project.org/package=scatterplot3d
Comment: The package was renamed to 'r-cran-scatterplot3d' to fit the
 pattern of CRAN (and non-CRAN) packages for GNU-R in Debian.

Files: *
Copyright: 2004-2018, Uwe Ligges <ligges@statistik.tu-dortmund.de>
License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: debian/*
Copyright: 2010-2018, Philip Rinn <rinni@inventati.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

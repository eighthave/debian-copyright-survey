This is the Debian GNU/Linux r-cran-openxlsx package of openxlsx.  The
openxlsx package provides read and write access to XLSX files. It
was written by Alexander Walker with contributions by Lica Braglia.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'openxlsx' to
'r-cran-openxlsx' to fit the pattern of CRAN (and non-CRAN) packages
for R.


Files: *
Copyright: 2014-2018 Alexander Walker
License: GPL-3

Files: debian/*
Copyright: 2014  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included in the file
/usr/share/common-licenses/GPL-2, and the GPL license (version 3) is included in the file
/usr/share/common-licenses/GPL-3.


For reference, the upstream DESCRIPTION file is included below:

    Package: openxlsx
    Type: Package
    Title: Read, Write and Edit XLSX Files
    Version: 4.0.17
    Date: 2017-03-09
    Authors@R: c(
        person("Alexander", "Walker",
        email = "Alexander.Walker1989@gmail.com", role = c("aut", "cre")),
        person("Luca", "Braglia", role = "ctb"))
    URL: https://github.com/awalker89/openxlsx
    BugReports: https://github.com/awalker89/openxlsx/issues
    Maintainer: Alexander Walker <Alexander.Walker1989@gmail.com>
    Description: Simplifies the creation of Excel .xlsx files by providing a high
        level interface to writing, styling and editing worksheets. Through the use of
        'Rcpp', read/write times are comparable to the 'xlsx' and 'XLConnect' packages
        with the added benefit of removing the dependency on Java.
    License: GPL-3
    LinkingTo: Rcpp
    Depends: R (>= 3.3.0)
    Imports: methods, Rcpp, grDevices, stats, utils
    VignetteBuilder: knitr
    Suggests: knitr, testthat
    RoxygenNote: 6.0.1.9000
    Collate: 'CommentClass.R' 'HyperlinkClass.R' 'RcppExports.R'
            'class_definitions.R' 'StyleClass.R' 'WorkbookClass.R'
            'baseXML.R' 'borderFunctions.R' 'chartsheet_class.R'
            'conditional_formatting.R' 'helperFunctions.R' 'loadWorkbook.R'
            'openXL.R' 'openxlsx.R' 'openxlsxCoerce.R' 'readWorkbook.R'
            'sheet_data_class.R' 'workbook_column_widths.R'
            'workbook_read_workbook.R' 'workbook_write_data.R'
            'worksheet_class.R' 'wrappers.R' 'writeData.R'
            'writeDataTable.R' 'writexlsx.R'
    NeedsCompilation: yes
    Packaged: 2017-03-23 09:21:13 UTC; Alex
    Author: Alexander Walker [aut, cre],
      Luca Braglia [ctb]
    Repository: CRAN
    Date/Publication: 2017-03-23 13:10:15 UTC

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: repsnapper
Upstream-Contact: Kulitorum <repsnapper@kulitorum.com>
Source: https://github.com/timschmidt/repsnapper

Files: *
Copyright: 2010 Kulitorum <repsnapper@kulitorum.com>
           2011 Michael Meeks <michael.meeks@novell.com>
           2011-2012 Martin Dieringer <martin.dieringer@gmx.de>
License: GPL-2+

Files: src/arcball.*
Copyright: 1999-2003 Tatewake.com
License: Expat

Files: src/connectview.* src/model.cpp src/platform.* src/progress.* src/render.* src/repsnapper.cpp src/settings.* src/settings-ui.* src/types.h src/view.*
Copyright: 2010-2011 Michael Meeks <michael.meeks@novell.com>
License: LGPL-2+

Files: src/convert.h
Copyright: 2007-2008 INRIA
License: LGPL-2+

Files: src/filechooser.*
Copyright: 2012 Martin Dieringer <martin.dieringer@gmx.de>
License: LGPL-3+

Files: src/gllight.*
Copyright: 2010 Evan Clinton (Palomides) <nave.notnilc@gmail.com>
License: GPL-2+

Files: src/Makefile.am
Copyright: 2009 Joachim Glauche
License: GPL-2+

Files: src/slicer/Makefile.am
Copyright: 2009 Joachim Glauche
           2011 Bas Wijnen <wijnen@debian.org>
License: GPL-2+

Files: src/miniball.h
Copyright: 1999-2006 Bernd Gaertner
License: GPL-2+

Files: src/printer.cpp src/printer_iochannel.cpp src/reprap_serial.*
Copyright: 2011-2012 Martin Dieringer <martin.dieringer@gmx.de>
License: LGPL-2+

Files: src/printer.h
Copyright: 2010 Kulitorum <repsnapper@kulitorum.com>
           2011-2012 Martin Dieringer <martin.dieringer@gmx.de>
License: LGPL-2+

Files: src/printer_libreprap.cpp
Copyright: 2011 Michael Meeks <michael.meeks@novell.com>
           2011-2012 Martin Dieringer <martin.dieringer@gmx.de>
License: LGPL-2+

Files: libraries/clipper/*
Copyright: 2010-2012 Angus Johnson
License: Expat

Files: libraries/lmfit/*
Copyright: 2004-2010 Joachim Wuttke <j.wuttke@fz-juelich.de>
License: public-domain
 lmfit is released under the LMFIT-BEER-WARE licence:
 .
 In writing this software, I borrowed heavily from the public domain,
 especially from work by Burton S. Garbow, Kenneth E. Hillstrom,
 Jorge J. Moré, Steve Moshier, and the authors of lapack. To avoid
 unneccessary complications, I put my additions and amendments also
 into the public domain. Please retain this notice. Otherwise feel
 free to do whatever you want with this stuff. If we meet some day,
 and you think this work is worth it, you can buy me a beer in return.

Files: libraries/poly2tri/*
Copyright: 2009-2010 Poly2Tri Contributors
License: BSD-3-clause-Poly2Tri
 Redistribution and use in source and binary forms, with or without
 modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of Poly2Tri nor the names of its contributors may be
   used to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: libraries/vmmlib/*
Copyright: 2006-2012 Visualization and Multimedia Lab, University of Zurich
           2010 Daniel Pfeifer
           2003 Jaakko Järvi, Jeremiah Willcock, Andrew Lumsdaine
License: BSD-3-clause-VMLab
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 Neither the name of the Visualization and Multimedia Lab, University of
 Zurich nor the names of its contributors may be used to endorse or
 promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2012 Ying-Chun Liu (PaulLiu) <paulliu@debian.org>
License: LGPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License along
 with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

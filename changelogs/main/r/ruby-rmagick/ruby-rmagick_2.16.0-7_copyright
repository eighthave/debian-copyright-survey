Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RMagick
Upstream-Contact: rmagick@rubyforge.org
Source: http://rmagick.rubyforge.org
Files-Excluded: test/srgb.icm

Files: *
Copyright: Copyright © 2002-2009 Timothy P. Hunter; Copyright © 2009-2011 Benjamin Thomas and Omer Bar-or
License: MIT-X

Files: debian/*
Copyright: Copyright © 2003-2011 Mike Williams <mdub@dogbiscuit.org>,
 Lucas Nussbaum <lucas@lucas-nussbaum.net>,
 Paul van Tilburg <paulvt@debian.org>,
 Antonio Terceiro <terceiro@debian.org>,
 Vincent Fourmond <fourmond@debian.org>,
 Gunnar Wolf <gwolf@debian.org>
License: GPL-3+

License: MIT-X
 Copyright © 2002-2009 by Timothy P. Hunter
 .
 Changes since Nov. 2009 copyright © by Benjamin Thomas and Omer Bar-or
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-3+
 The files hereby included are free software; you can distribute them
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

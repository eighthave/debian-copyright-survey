Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: © 2014 Hugo Heuzard
License: LGPL-2.1-exc

Files: pkg/*
Copyright: © 2014 Daniel C. Bünzli
License: BSD3

Files: debian/*
Copyright: © 2015 Stéphane Glondu <glondu@debian.org>
License: LGPL-2.1-exc

License: LGPL-2.1-exc
 This program is released under the LGPL version 2.1 (see the text below) with
 the additional exemption that compiling, linking, and/or using OpenSSL is
 allowed.
 .
 As a special exception to the GNU Library General Public License, you
 may also link, statically or dynamically, a "work that uses the Library"
 with a publicly distributed version of the Library to produce an
 executable file containing portions of the Library, and distribute
 that executable file under terms of your choice, without any of the
 additional requirements listed in clause 6 of the GNU Library General
 Public License.  By "a publicly distributed version of the Library",
 we mean either the unmodified Library, or a
 modified version of the Library that is distributed under the
 conditions defined in clause 3 of the GNU Library General Public
 License.  This exception does not however invalidate any other reasons
 why the executable file might be covered by the GNU Library General
 Public License.
 .
 On Debian systems, the full text of the LGPL version 2.1 is available
 in `/usr/share/common-licenses/LGPL-2.1'.

License: BSD3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
 3. Neither the name of Daniel C. Bünzli nor the names of
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

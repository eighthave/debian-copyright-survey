This is the Debian GNU/Linux r-cran-rmpi package of Rmpi, an interface
package for GNU R providing access to the MPI (Message-Passing
Interface) API with interactive R slave functionalities.  Rmpi was
written by Hao Yu.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'Rmpi' to 'r-cran-rmpi'
to highlight the fact that is it a CRAN package for R.

Copyright (C) 2003 - 2008 Hao Yu

License: GPL-2

On a Debian GNU/Linux system, the GPL license (v2) is included in the file
/usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION file is included below:

    Package: Rmpi
    Version: 0.4-8
    Date: 2004-04-21
    Title: Interface (Wrapper) to MPI (Message-Passing Interface)
    Author: Hao Yu
    Maintainer: Hao Yu <hyu@stats.uwo.ca>
    Depends: R (>= 1.8.1)
    Description: Rmpi provides an interface (wrapper) to MPI APIs. It also 
    	     provides interactive R slave environment in which 
    	     distributed statistical computing can be carried out.
    License: GPL version 2 or newer
    URL: http://www.stats.uwo.ca/faculty/yu/Rmpi
    Packaged: Thu Apr 22 10:03:22 2004; hyu

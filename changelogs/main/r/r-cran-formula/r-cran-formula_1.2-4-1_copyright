This is the Debian GNU/Linux r-cran-formula package of Formula.
The Formula package provides extended model formulas for R, and 
was written by Achim Zeileis and Yves Croissant.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'Formula' to
'r-cran-formula' to fit the pattern of CRAN (and non-CRAN) packages
for R.


Copyright (C) 2009 - 2013  Achim Zeileis and Yves Croissant

License: GPL-2

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

  Package: Formula
  Version: 1.1-1
  Date: 2013-04-04
  Title: Extended Model Formulas
  Description: Infrastructure for extended formulas with multiple parts
          on the right-hand side and/or multiple responses on the
          left-hand side.
  Authors@R: c(person(given = "Achim", family = "Zeileis", role =
          c("aut", "cre"), email = "Achim.Zeileis@R-project.org"),
          person(given = "Yves", family = "Croissant", role = "aut",
          email = "Yves.Croissant@univ-reunion.fr"))
  Depends: R (>= 2.0.0), stats
  License: GPL-2
  Packaged: 2013-04-04 17:32:01 UTC; zeileis
  Author: Achim Zeileis [aut, cre], Yves Croissant [aut]
  Maintainer: Achim Zeileis <Achim.Zeileis@R-project.org>
  NeedsCompilation: no
  Repository: CRAN
  Date/Publication: 2013-04-04 19:33:55
  
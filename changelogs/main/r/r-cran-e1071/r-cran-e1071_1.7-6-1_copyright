This is the Debian GNU/Linux r-cran-e1071 package of e1071.
The e1071 package regroups miscellaneous function of the Department
of Statistics (e1017) at the TU Wien. It was written by David Meyer,
Evgenia Dimitriadou, Kurt Hornik, Andreas Weingessel, and Friedrich 
Leisch.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'e1071' to
'r-cran-e1071' to fit the pattern of CRAN (and non-CRAN) packages 
for R.


Files: *
Copyright: 1999 - 2014  David Meyer, Evgenia Dimitriadou, Kurt Hornik, Andreas Weingessel, and Friedrich Leisch.
License: GPL-2+


Files: src/svm.*
Copyright: 2000 - 2014  Chih-Chung Chang and Chih-Chen Lin 
License: BSD
  Copyright (c) 2000-2014 Chih-Chung Chang and Chih-Jen Lin
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

  3. Neither name of copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.


  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Files: debian/*
Copyright: 2014  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included in the file
/usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: e1071
   Version: 1.6-3
   Date: 2014-02-13
   Title: Misc Functions of the Department of Statistics (e1071), TU Wien
   Imports: graphics, grDevices, class
   Suggests: cluster, mlbench, nnet, randomForest, rpart, SparseM, xtable,
           Matrix, MASS
   Authors@R: c(person(given = "David", family = "Meyer", role = c("aut", "cre"), 
                  email = "David.Meyer@R-project.org"),
                person(given = "Evgenia", family = "Dimitriadou", role = c("aut","cph")),
                person(given = "Kurt", family = "Hornik", role = "aut"),
                person(given = "Andreas", family = "Weingessel", role = "aut"),
                person(given = "Friedrich", family = "Leisch", role = "aut"),
                person(given = "Chih-Chung", family = "Chang", role = c("ctb","cph"), comment = "libsvm C++-code"),
                person(given = "Chih-Chen", family = "Lin", role = c("ctb","cph"), comment = "libsvm C++-code"))
   Description: Functions for latent class analysis, short time Fourier
   	     transform, fuzzy clustering, support vector machines,
   	     shortest path computation, bagged clustering, naive Bayes
   	     classifier, ...
   License: GPL-2
   LazyLoad: yes
   Packaged: 2014-03-16 22:45:51 UTC; david
   Author: David Meyer [aut, cre],
     Evgenia Dimitriadou [aut, cph],
     Kurt Hornik [aut],
     Andreas Weingessel [aut],
     Friedrich Leisch [aut],
     Chih-Chung Chang [ctb, cph] (libsvm C++-code),
     Chih-Chen Lin [ctb, cph] (libsvm C++-code)
   Maintainer: David Meyer <David.Meyer@R-project.org>
   NeedsCompilation: yes
   Repository: CRAN
   Date/Publication: 2014-03-17 12:12:07

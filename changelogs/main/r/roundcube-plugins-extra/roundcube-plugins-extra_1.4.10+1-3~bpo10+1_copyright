Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded-contextmenu:
  skins/elastic/contextmenu.min.css
Files-Excluded-sauserprefs:
  skins/elastic/sauserprefs.min.css
  skins/elastic/tabstyles.min.css
Files-Excluded-thunderbird-labels:
  tb_label.js

Files: authres-status/*
Copyright: Copyright (c) 20016-2020 pimlie <pimlie@hotmail.com>
License: GPL-3+

Files: compose-addressbook/*
Copyright: Copyright (c) 2009-2012 Cor Bosman <cor@roundcu.be>
License: GPL-2

Files: contextmenu/*
Copyright: Copyright (c) 2009-2020 Philip Weir <roundcube@tehinterweb.co.uk>
License: GPL-3+

Files: dovecot-impersonate/*
Copyright: Copyright (c) 2010-2011 Cor Bosman <roundcube@wa.ter.net>
License: GPL-2

Files: fail2ban/*
Copyright: Copyright (c) 2009-2011 Matt Rude <m@mattrude.com>
License: GPL-3

Files: html5-notifier/*
Copyright: Copyright (c) 2013-2020 Tilman Stremlau <tilman@stremlau.net>
License: ISC

Files: keyboard-shortcuts/*
Copyright:
 Copyright (c) 2010 Patrik Kullman
 Copyright (c) 2010 Roland 'rosali' Liebl
 Copyright (c) 2010-2013 Cor Bosman <cor@roundcu.be>
License: GPL-2

Files: message-highlight/*
Copyright: Copyright (c) 2010-2011 Cor Bosman <roundcube@wa.ter.net>
License: GPL-2

Files: message-highlight/colorpicker/*
Copyright: Copyright (c) 2010 Meta100 LLC.
License: Expat

Files: listcommands/*
Copyright: Copyright (c) 2009-2013 Cor Bosman <cor@roundcu.be>
License: GPL-2

Files: sauserprefs/*
Copyright: Copyright (c) 2009-2020 Philip Weir <roundcube@tehinterweb.co.uk>
License: GPL-3+

Files: thunderbird-labels/*
Copyright: Copyright (c) 2011-2020 Michael Kefeder
License: GPL-2

Files: debian/*
Copyright: Copyright 2011 Jérémy Bobbio <lunar@debian.org>
           2015 sandro Knauß <bugs@sandroknauss.de>
License: GPL-2+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software: you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation, either
 version 3 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE. See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software: you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation, either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE. See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

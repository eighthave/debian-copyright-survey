Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: shiny
Upstream-Contact: Winston Chang <winston@rstudio.com>
Source: https://cran.r-project.org/package=shiny
Files-Excluded: */bootstrap/css
                */bootstrap/fonts
                */bootstrap/js/bootstrap*
                */html5shiv.min.js
                */json2-min.js
                */highlight/highlight.pack.js
                */highlight/LICENSE
                */highlight/classref.txt
                */jquery.dataTables.min.js
                */es5-shim.min.js
                */jquery.min.*
                */jquery.js
                */jquery-ui.js
                */jquery-ui.min.js
                */jquery-ui.css
                */jquery-ui.min.css
                */jqueryui/images
                */font-awesome/css
                */datepicker/js
                */respond.min.js
                */selectize.min.js
                */strftime-min.js
                */babel-polyfill.min.js
                */normalize.css

Files: *
Copyright: 2013-2016 Winston Chang <winston@rstudio.com>
                     Joe Cheng, JJ Allaire, Yihui Xie, Jonathan McPherson,
                     RStudio, jQuery Foundation, jQuery contributors,
                     R Core Team
License: GPL-3

Files: debian/missing-sources/respond/respond.src.js
Copyright: 2012, Scott Jehl <scottjehl@gmail.com>
           2011-2015, Twitter, Inc.
License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
Comment: missing source for
         inst/www/shared/bootstrap/shim/respond.min.js
 downloaded from <https://github.com/scottjehl/Respond>
 using the version from the 1.4.2 tag.

Files: inst/www/shared/ionrangeslider/*
Copyright: 2015 Denis Ineshin
License: MIT
Comment: The source for this code can be found at
  https://github.com/IonDen/ion.rangeSlider

Files: inst/www/shared/jqueryui/*
Copyright: 2008-2015 jQuery Foundation and other contributors
License: MIT

Files: inst/www/shared/showdown/*
Copyright: 2007, John Fraser
License: BDS-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the name "Markdown" nor the names of its contributors may
   be used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 This software is provided by the copyright holders and contributors "as
 is" and any express or implied warranties, including, but not limited
 to, the implied warranties of merchantability and fitness for a
 particular purpose are disclaimed. In no event shall the copyright owner
 or contributors be liable for any direct, indirect, incidental, special,
 exemplary, or consequential damages (including, but not limited to,
 procurement of substitute goods or services; loss of use, data, or
 profits; or business interruption) however caused and on any theory of
 liability, whether in contract, strict liability, or tort (including
 negligence or otherwise) arising in any way out of the use of this
 software, even if advised of the possibility of such damage.

Files: debian/missing-sources/strftime/strftime.js
Copyright: 2010-2015 Sami Samhuri <sami@samhuri.net>
License: MIT

Files: R/tar.R
Copyright: 1995-2012 The R Core Team
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.

Files: inst/www/shared/selectize/css/selectize.bootstrap3.css
Copyright: 2013–2015 Brian Reavis & contributors
License: Apache-2.0

Files: debian/*
Copyright: 2016 Andreas Tille <tille@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation version 3.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-3'.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 file except in compliance with the License. You may obtain a copy of the License at:
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software distributed under
 the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 ANY KIND, either express or implied. See the License for the specific language
 governing permissions and limitations under the License.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/Apache-2.0'.


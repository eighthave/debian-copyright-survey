Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ttfunk
Source: https://github.com/prawnpdf/ttfunk

Files: *
Copyright: Copyright 2008, 2011 Gregory Brown <gregory.t.brown@gmail.com>
           Copyright 2008 Jia Wu <wujia123@yahoo.com>
           Copyright 2008, 2009 Jamis Buck  <jamis@37signals.com>
           Copyright 2010 Steve Sloan <steve@finagle.org>
           Copyright 2011 James Healy <jimmy@deefa.com>
           Copyright 2011 Daniel Nelson <dnelson+git@bluejade.com>
           Copyright 2011 Lucas Florio <lucasefe@gmail.com>
           Copyright 2011 Brad Ediger <brad.ediger@madriska.com>
License: Ruby or GPL-2 or GPL-3

Files: data/fonts/DejaVuSans.ttf
Copyright: Copyright 2003 Bitstream, Inc.
License: Bitstream
Comment: DejaVu changes are in public domain

Files: debian/*
Copyright: Copyright 2011-2014 Cédric Boutillier <boutil@debian.org>
License: Ruby or GPL-2 or GPL-3

License: Ruby
 You can redistribute Prawn and/or modify it under either the terms of the GPLv2
 or GPLv3 (see GPLv2 and GPLv3 files), or the conditions below:
 .
   1. You may make and give away verbatim copies of the source form of the
      software without restriction, provided that you duplicate all of the
      original copyright notices and associated disclaimers.
 .
   2. You may modify your copy of the software in any way, provided that
      you do at least ONE of the following:
 .
        a) place your modifications in the Public Domain or otherwise
           make them Freely Available, such as by posting said
     modifications to Usenet or an equivalent medium, or by allowing
     the author to include your modifications in the software.
 .
        b) use the modified software only within your corporation or
           organization.
 .
        c) rename any non-standard executables so the names do not conflict
     with standard executables, which must also be provided.
 .
        d) make other distribution arrangements with the author.
 .
   3. You may distribute the software in object code or executable
      form, provided that you do at least ONE of the following:
 .
        a) distribute the executables and library files of the software,
     together with instructions (in the manual page or equivalent)
     on where to get the original distribution.
 .
        b) accompany the distribution with the machine-readable source of
     the software.
 .
        c) give non-standard executables non-standard names, with
           instructions on where to get the original software distribution.
 .
        d) make other distribution arrangements with the author.
 .
   4. You may modify and include the part of the software into any other
      software (possibly commercial). 
 .
   5. The scripts and library files supplied as input to or produced as 
      output from the software do not automatically fall under the
      copyright of the software, but belong to whomever generated them, 
      and may be sold commercially, and may be aggregated with this
      software.
 .
   6. THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
      IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
      WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
      PURPOSE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License Version 2 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License Version 3 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, full text of the GPL-3 license can be found
 in `/usr/share/common-licenses/GPL-3'

License: Bitstream
 Copyright (c) 2003 by Bitstream, Inc. All Rights Reserved. Bitstream Vera is
 a trademark of Bitstream, Inc.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute the
 Font Software, including without limitation the rights to use, copy, merge,
 publish, distribute, and/or sell copies of the Font Software, and to permit
 persons to whom the Font Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright and trademark notices and this permission notice shall
 be included in all copies of one or more of the Font Software typefaces.
 .
 The Font Software may be modified, altered, or added to, and in particular
 the designs of glyphs or characters in the Fonts may be modified and
 additional glyphs or characters may be added to the Fonts, only if the fonts
 are renamed to names not containing either the words "Bitstream" or the word
 "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts or Font
 Software that has been modified and is distributed under the "Bitstream
 Vera" names.
 .
 The Font Software may be sold as part of a larger software package but no
 copy of one or more of the Font Software typefaces may be sold by itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT,
 TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL BITSTREAM OR THE GNOME
 FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING
 ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE
 FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font Software
 without prior written authorization from the Gnome Foundation or Bitstream
 Inc., respectively. For further information, contact: fonts at gnome dot
 org. 

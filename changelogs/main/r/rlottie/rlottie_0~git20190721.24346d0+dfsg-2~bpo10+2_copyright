Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rlottie
Upstream-Contact: Subhransu Mohanty <sub.mohanty@samsung.com>
                  Hermet Park <hermetpark@gmail.com>
Source: https://github.com/Samsung/rlottie
Files-Excluded: licenses/COPYING.RPD
                src/lottie/rapidjson/*
                src/vector/stb/stb_image.h
Comment: Remove bundled rapidjson completely and its evil license, as well as unused stb header

Files: *
License: LGPL-2.1+
Copyright: 2018 Samsung Electronics Co., Ltd. All rights reserved.

Files: debian/*
License: LGPL-2.1+
Copyright: 2019 Nicholas Guriev <guriev-ns@ya.ru>

Files: example/gif.h
License: Unlicense
Copyright: by Charlie Tangora <ctangora@gmail.com>
Comment: The code is in Public domain so Unlicense may be used.

Files: src/vector/freetype/*
License: FTL
Copyright: 2002-2006, 2008-2011, 2013 by David Turner, Robert Wilhelm, and Werner Lemberg.

Files: src/vector/pixman/*
License: Expat
Copyright: 1987, 1988, 1989, 1998  The Open Group
           1987, 1988, 1989 Digital Equipment Corporation
           1999, 2004, 2008 Keith Packard
           2000 SuSE, Inc.
           2000 Keith Packard, member of The XFree86 Project, Inc.
           2004, 2005, 2007, 2008, 2009, 2010 Red Hat, Inc.
           2004 Nicholas Miell
           2005 Lars Knoll & Zack Rusin, Trolltech
           2005 Trolltech AS
           2007 Luca Barbato
           2008 Aaron Plattner, NVIDIA Corporation
           2008 Rodrigo Kumpera
           2008 André Tupinambá
           2008 Mozilla Corporation
           2008 Frederic Plourde
           2009, Oracle and/or its affiliates. All rights reserved.
           2009, 2010 Nokia Corporation

Files: src/vector/pixman/pixman-arm-neon-asm.h
       src/vector/pixman/pixman-arm-neon-asm.S
License: Expat
Copyright: 2009 Nokia Corporation
Author: Siarhei Siamashka <siarhei.siamashka@nokia.com>

Files: src/vector/pixman/vregion.cpp
License: X11 and HPND-sell-variant
Copyright: 1987, 1988, 1989, 1998 by The Open Group
           1987, 1988, 1989 by Digital Equipment Corporation, Maynard, Massachusetts.
           1998 by Keith Packard

Files: src/vector/pixman/vregion.h
License: LGPL-2.1+
Copyright: 2018 Samsung Electronics Co., Ltd. All rights reserved.

Files: src/vector/stb/*
License: Expat
Copyright: 2017 Sean Barrett

Files: src/vector/vinterpolator.cpp
License: MPL-1.1 or GPL-2.0 or LGPL-2.1
Copyright: 2005 Brian Birtles <birtles@gmail.com>

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: FTL
 This file is part of the FreeType project, and may only be used, modified, and
 distributed under the terms of the FreeType project license, licenses/COPYING.FTL.
 By continuing to use, modify, or distribute this file you indicate that you
 have read the license and understand and accept it fully.
 .
                    The FreeType Project LICENSE
                    ----------------------------
                            2006-Jan-27
 .
                    Copyright 1996-2002, 2006 by
          David Turner, Robert Wilhelm, and Werner Lemberg
 .
 Introduction
 ============
 .
 The FreeType  Project is distributed in  several archive packages;
 some of them may contain, in addition to the FreeType font engine,
 various tools and  contributions which rely on, or  relate to, the
 FreeType Project.
 .
 This  license applies  to all  files found  in such  packages, and
 which do not  fall under their own explicit  license.  The license
 affects  thus  the  FreeType   font  engine,  the  test  programs,
 documentation and makefiles, at the very least.
 .
 This  license   was  inspired  by  the  BSD,   Artistic,  and  IJG
 (Independent JPEG  Group) licenses, which  all encourage inclusion
 and  use of  free  software in  commercial  and freeware  products
 alike.  As a consequence, its main points are that:
 .
   o We don't promise that this software works. However, we will be
     interested in any kind of bug reports. (`as is' distribution)
 .
   o You can  use this software for whatever you  want, in parts or
     full form, without having to pay us. (`royalty-free' usage)
 .
   o You may not pretend that  you wrote this software.  If you use
     it, or  only parts of it,  in a program,  you must acknowledge
     somewhere  in  your  documentation  that  you  have  used  the
     FreeType code. (`credits')
 .
 We  specifically  permit  and  encourage  the  inclusion  of  this
 software, with  or without modifications,  in commercial products.
 We  disclaim  all warranties  covering  The  FreeType Project  and
 assume no liability related to The FreeType Project.
 .
 Finally,  many  people  asked  us  for  a  preferred  form  for  a
 credit/disclaimer to use in compliance with this license.  We thus
 encourage you to use the following text:
 .
   """
    Portions of this software are copyright (c) <year> The FreeType
    Project (www.freetype.org).  All rights reserved.
   """
 .
 Please replace <year> with the value from the FreeType version you
 actually use.
 .
 Legal Terms
 ===========
 .
 0 Definitions
 -------------
 Throughout this license,  the terms `package', `FreeType Project',
 and  `FreeType  archive' refer  to  the  set  of files  originally
 distributed  by the  authors  (David Turner,  Robert Wilhelm,  and
 Werner Lemberg) as the `FreeType Project', be they named as alpha,
 beta or final release.
 .
 `You' refers to  the licensee, or person using  the project, where
 `using' is a generic term including compiling the project's source
 code as  well as linking it  to form a  `program' or `executable'.
 This  program is  referred to  as  `a program  using the  FreeType
 engine'.
 .
 This  license applies  to all  files distributed  in  the original
 FreeType  Project,   including  all  source   code,  binaries  and
 documentation,  unless  otherwise  stated   in  the  file  in  its
 original, unmodified form as  distributed in the original archive.
 If you are  unsure whether or not a particular  file is covered by
 this license, you must contact us to verify this.
 .
 The FreeType  Project is copyright (C) 1996-2000  by David Turner,
 Robert Wilhelm, and Werner Lemberg.  All rights reserved except as
 specified below.
 .
 1 No Warranty
 -------------
 THE FREETYPE PROJECT  IS PROVIDED `AS IS' WITHOUT  WARRANTY OF ANY
 KIND, EITHER  EXPRESS OR IMPLIED,  INCLUDING, BUT NOT  LIMITED TO,
 WARRANTIES  OF  MERCHANTABILITY   AND  FITNESS  FOR  A  PARTICULAR
 PURPOSE.  IN NO EVENT WILL ANY OF THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE  FOR ANY DAMAGES CAUSED  BY THE USE OR  THE INABILITY TO
 USE, OF THE FREETYPE PROJECT.
 .
 2 Redistribution
 ----------------
 This  license  grants  a  worldwide, royalty-free,  perpetual  and
 irrevocable right  and license to use,  execute, perform, compile,
 display,  copy,   create  derivative  works   of,  distribute  and
 sublicense the  FreeType Project (in  both source and  object code
 forms)  and  derivative works  thereof  for  any  purpose; and  to
 authorize others  to exercise  some or all  of the  rights granted
 herein, subject to the following conditions:
 .
   o Redistribution of  source code  must retain this  license file
     (`FTL.TXT') unaltered; any  additions, deletions or changes to
     the original  files must be clearly  indicated in accompanying
     documentation.   The  copyright   notices  of  the  unaltered,
     original  files must  be  preserved in  all  copies of  source
     files.
 .
   o Redistribution in binary form must provide a  disclaimer  that
     states  that  the software is based in part of the work of the
     FreeType Team,  in  the  distribution  documentation.  We also
     encourage you to put an URL to the FreeType web page  in  your
     documentation, though this isn't mandatory.
 .
 These conditions  apply to any  software derived from or  based on
 the FreeType Project,  not just the unmodified files.   If you use
 our work, you  must acknowledge us.  However, no  fee need be paid
 to us.
 .
 3 Advertising
 -------------
 Neither the  FreeType authors and  contributors nor you  shall use
 the name of the  other for commercial, advertising, or promotional
 purposes without specific prior written permission.
 .
 We suggest,  but do not require, that  you use one or  more of the
 following phrases to refer  to this software in your documentation
 or advertising  materials: `FreeType Project',  `FreeType Engine',
 `FreeType library', or `FreeType Distribution'.
 .
 As  you have  not signed  this license,  you are  not  required to
 accept  it.   However,  as  the FreeType  Project  is  copyrighted
 material, only  this license, or  another one contracted  with the
 authors, grants you  the right to use, distribute,  and modify it.
 Therefore,  by  using,  distributing,  or modifying  the  FreeType
 Project, you indicate that you understand and accept all the terms
 of this license.
 .
 4 Contacts
 ----------
 There are two mailing lists related to FreeType:
 .
   o freetype@nongnu.org
 .
     Discusses general use and applications of FreeType, as well as
     future and  wanted additions to the  library and distribution.
     If  you are looking  for support,  start in  this list  if you
     haven't found anything to help you in the documentation.
 .
   o freetype-devel@nongnu.org
 .
     Discusses bugs,  as well  as engine internals,  design issues,
     specific licenses, porting, etc.
 .
 Our home page can be found at http://www.freetype.org

License: MPL-1.1 or GPL-2.0 or LGPL-2.1
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/
 .
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
 .
 The Original Code is the Mozilla SMIL module.
 .
 The Initial Developer of the Original Code is Brian Birtles.
 Portions created by the Initial Developer are Copyright (C) 2005
 the Initial Developer. All Rights Reserved.
 .
 Alternatively, the contents of this file may be used under the terms of
 either of the GNU General Public License Version 2 or later (the "GPL"),
 or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the GPL or the LGPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: HPND-sell-variant
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that the
 above copyright notice appear in all copies and that both that copyright notice
 and this permission notice appear in supporting documentation, and that the
 name of the copyright holder not be used in advertising or publicity pertaining
 to distribution of the software without specific, written prior permission. The
 copyright holder makes no representations about the suitability of this software
 for any purpose. It is provided "as is" without express or implied warranty.
 .
 THE COPYRIGHT HOLDER DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT
 SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
 OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: X11
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation.
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of The Open Group shall not be
 used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization from The Open Group.

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
 software, either in source code form or as a compiled binary, for any purpose,
 commercial or non-commercial, and by any means.
 .
 In jurisdictions that recognize copyright laws, the author or authors of this
 software dedicate any and all copyright interest in the software to the public
 domain. We make this dedication for the benefit of the public at large and to
 the detriment of our heirs and successors. We intend this dedication to be an
 overt act of relinquishment in perpetuity of all present and future rights to
 this software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rope
Upstream-Contact: Ali Gholami Rudi <aligrudi@users.sourceforge.net>
Source: https://github.com/python-rope/rope

Files: *
Copyright: 2006-2012 Ali Gholami Rudi <aligrudi@users.sourceforge.net>
           2015-2016 Nicholas Smith
           2014-2015 Matej Cepl
           2009-2012 Anton Gritsay
License: LGPL-3+

Files: debian/*
Copyright: 2008-2014 David Spreen <netzwurm@debian.org>
           2014-2017 Arnaud Fontaine <arnau@debian.org>
           2016 Diane Trout <diane@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 may be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 may be found in '/usr/share/common-licenses/LGPL-3'.

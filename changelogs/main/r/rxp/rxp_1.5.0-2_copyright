This work was packaged for Debian by:

    NIIBE Yutaka <gniibe@fsij.org> on Wed, 12 May 2010 11:46:15 +0900

It was downloaded from:
    http://www.cogsci.ed.ac.uk/~richard/rxp.html
    When download, people need to agree license of GPL-2.

Upstream Author:
    Richard Tobin <richard@cogsci.ed.ac.uk>

Files: *
Copyright:
    Copyright (c) 1997-2008 Richard Tobin, Language Technology Group, HCRC,
    University of Edinburgh.
License: GPL-2 plus following disclaimer
    THE SOFTWARE IS PROVIDED ``AS IS'', WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHOR OR THE UNIVERSITY OF EDINBURGH BE LIABLE
    FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
    CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.

File: catalog/catalog_dtd.c
Copyright:
    Copyright (c) 2000, 2001, 2002 OASIS Open, Inc. All Rights Reserved.
License:
    This document and translations of it may be copied and furnished to
    others, and derivative works that comment on or otherwise explain
    it or assist in its implementation may be prepared, copied,
    published and distributed, in whole or in part, without restriction
    of any kind, provided that the above copyright notice and this
    paragraph are included on all such copies and derivative
    works. However, this document itself may not be modified in any
    way, such as by removing the copyright notice or references to
    OASIS, except as needed for the purpose of developing OASIS
    specifications, in which case the procedures for copyrights defined
    in the OASIS Intellectual Property Rights document must be
    followed, or as required to translate it into languages other than
    English.

Files: src/nf16check.c
       src/nf16data.c
       include/nf16check.h
       include/nf16data.h
Copyright:
    Copyright (c) 2003 Martin J. Du"rst, W3C
License:
    http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231
    By obtaining, using and/or copying this work, you (the licensee) agree that
    you have read, understood, and will comply with the following terms and
    conditions.

    Permission to copy, modify, and distribute this software and its
    documentation, with or without modification, for any purpose and without fee
    or royalty is hereby granted, provided that you include the following on ALL
    copies of the software and documentation or portions thereof, including
    modifications:

     * The full text of this NOTICE in a location viewable to users of the
        redistributed or derivative work.
     * Any pre-existing intellectual property disclaimers, notices, or terms and
        conditions. If none exist, the W3C Software Short Notice should be
        included (hypertext is preferred, text is permitted) within the body of
        any redistributed or derivative code.
     * Notice of any changes or modifications to the files, including the date
        changes were made. (We recommend you provide URIs to the location from
        which the code is derived.)

File: data in src/nf16data.c
    Copyright: Copyright (C) 1991-2010 Unicode, Inc. All rights reserved. 
License:
    COPYRIGHT AND PERMISSION NOTICE                       
                                                          
    Copyright (C) 1991-2010 Unicode, Inc. All rights       
    reserved. Distributed under the Terms of Use in http:/
    /www.unicode.org/copyright.html.                      
                                                          
    Permission is hereby granted, free of charge, to any  
    person obtaining a copy of the Unicode data files and 
    any associated documentation (the "Data Files") or    
    Unicode software and any associated documentation (the
    "Software") to deal in the Data Files or Software     
    without restriction, including without limitation the 
    rights to use, copy, modify, merge, publish,          
    distribute, and/or sell copies of the Data Files or   
    Software, and to permit persons to whom the Data Files
    or Software are furnished to do so, provided that (a) 
    the above copyright notice(s) and this permission     
    notice appear with all copies of the Data Files or    
    Software, (b) both the above copyright notice(s) and  
    this permission notice appear in associated           
    documentation, and (c) there is clear notice in each  
    modified Data File or in the Software as well as in   
    the documentation associated with the Data File(s) or 
    Software that the data or software has been modified. 
                                                          
    THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS",     
    WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,     
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN NO EVENT    
    SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS
    NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL        
    INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES     
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR        
    PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
    OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN        
    CONNECTION WITH THE USE OR PERFORMANCE OF THE DATA    
    FILES OR SOFTWARE.                                    
                                                          
    Except as contained in this notice, the name of a     
    copyright holder shall not be used in advertising or  
    otherwise to promote the sale, use or other dealings  
    in these Data Files or Software without prior written 
    authorization of the copyright holder.                

The Debian packaging is done by NIIBE Yutaka <gniibe@fsij.org>
and put into public domain.

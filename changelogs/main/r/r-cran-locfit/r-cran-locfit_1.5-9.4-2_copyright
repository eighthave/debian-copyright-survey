Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: locfit
Upstream-Contact: Andy Liaw <andy_liaw@merck.com>
Source: https://cran.r-project.org/package=locfit

Files: *
Copyright: © 1996-2013, Catherine Loader
           © 1998-2001, Jiayang Sun
           © 1996-2001, Lucent Technologies
           © 2005-2020, Andy Liaw
License: GPL-2+
Comment:
 Earlier versions of locfit had a license which restricted usage.
 The code was re-licensed by Prof. Loader in a version sent to
 Andy Liaw in 2005 from which this CRAN distribution is derived.

Files: debian/*
Copyright: © 2016 Michael R. Crusoe <michael.crusoe@gmail.com>
           © 2013 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.

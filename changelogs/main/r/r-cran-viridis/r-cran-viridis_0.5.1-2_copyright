This is the Debian GNU/Linux r-cran-viridis package of viridis.
The viridis package provides a port of the matplotlib color scales.
It was written by Simon Garnier.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'htmlTable' to
'r-cran-htmltable to fit the pattern of CRAN (and non-CRAN) packages
for R.

Files: *
Copyright: 2015 - 2016  Simon Garnier
Portions Copyright: 2015 - 2016  Noam RossL
Portions Copyright: 2015 - 2016  Bob Rudis
License: MIT

Files: debian/*
Copyright: 2016  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

    Package: viridis
    Type: Package
    Title: Default Color Maps from 'matplotlib'
    Version: 0.3.4
    Date: 2016-03-11
    Authors@R: c(
          person("Simon", "Garnier", email = "garnier@njit.edu", role = c("aut", "cre")),
          person("Noam", "Ross", email = "noam.ross@gmail.com", role = c("ctb", "cph"), comment = "Continuous scale"),
          person("Bob", "Rudis", email = "bob@rudis.net", role = c("ctb", "cph"), comment = "Combined scales")
      )
    Maintainer: Simon Garnier <garnier@njit.edu>
    Description: Port of the new 'matplotlib' color maps ('viridis' - the default
        -, 'magma', 'plasma' and 'inferno') to 'R'. 'matplotlib' <http://matplotlib.org/
        > is a popular plotting library for 'python'. These color maps are designed
        in such a way that they will analytically be perfectly perceptually-uniform,
        both in regular form and also when converted to black-and-white. They are
        also designed to be perceived by readers with the most common form of color
        blindness.
    License: MIT + file LICENSE
    LazyData: TRUE
    Encoding: UTF-8
    Depends: R (>= 2.10)
    Imports: stats, grDevices, ggplot2 (>= 1.0.1), gridExtra
    Suggests: hexbin (>= 1.27.0), scales, MASS, knitr, dichromat,
            colorspace, rasterVis, httr, mapproj
    VignetteBuilder: knitr
    URL: https://github.com/sjmgarnier/viridis
    BugReports: https://github.com/sjmgarnier/viridis/issues
    RoxygenNote: 5.0.1
    NeedsCompilation: no
    Packaged: 2016-03-12 00:32:35 UTC; simon
    Author: Simon Garnier [aut, cre],
      Noam Ross [ctb, cph] (Continuous scale),
      Bob Rudis [ctb, cph] (Combined scales)
    Repository: CRAN
    Date/Publication: 2016-03-12 06:17:29


The file LICENSE is included below:

    YEAR: 2015
    COPYRIGHT HOLDER: Simon Garnier

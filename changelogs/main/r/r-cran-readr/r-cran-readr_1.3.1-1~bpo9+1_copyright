This is the Debian GNU/Linux r-cran-readr package of readr.  The readr
package provides functions to read rectangular text data. It was
written by Hadley Wickham, Jim Hester, and Romain François.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'readr' to
'r-cran-readr' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Files: *
Copyright: 2015 - 2017  Hadley Wickham and Jim Hester and Romain François
Copyright: 2015 - 2017  RStudio
Portions Copyright: 2015   R Core Team
Portions Copyright: 2015   Jukka Jylänki and Mikkel Jørgensen
License: GPL-2

Files: debian/*
Copyright: 2017  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION file is included below:

   Package: readr
   Version: 1.1.1
   Title: Read Rectangular Text Data
   Description: The goal of 'readr' is to provide a fast and friendly way to read
       rectangular data (like 'csv', 'tsv', and 'fwf'). It is designed to flexibly
       parse many types of data found in the wild, while still cleanly failing when
       data unexpectedly changes.
   Authors@R: c(
       person("Hadley", "Wickham", , "hadley@rstudio.com", "aut"),
       person("Jim", "Hester", , "james.hester@rstudio.com", c("aut", "cre")),
       person("Romain", "Francois", role = "aut"),
       person("R Core Team", role = "ctb", comment = "Date time code adapted from R"),
       person("RStudio", role = c("cph", "fnd")),
       person("Jukka", "Jylänki", role = c("ctb", "cph"), comment = "grisu3 implementation"),
       person("Mikkel", "Jørgensen", role = c("ctb", "cph"), comment = "grisu3 implementation"))
   Encoding: UTF-8
   Depends: R (>= 3.0.2)
   LinkingTo: Rcpp, BH
   Imports: Rcpp (>= 0.12.0.5), tibble, hms, R6
   Suggests: curl, testthat, knitr, rmarkdown, stringi, covr
   License: GPL (>= 2) | file LICENSE
   BugReports: https://github.com/tidyverse/readr/issues
   URL: http://readr.tidyverse.org, https://github.com/tidyverse/readr
   VignetteBuilder: knitr
   RoxygenNote: 6.0.1
   NeedsCompilation: yes
   Packaged: 2017-05-16 16:03:56 UTC; jhester
   Author: Hadley Wickham [aut],
     Jim Hester [aut, cre],
     Romain Francois [aut],
     R Core Team [ctb] (Date time code adapted from R),
     RStudio [cph, fnd],
     Jukka Jylänki [ctb, cph] (grisu3 implementation),
     Mikkel Jørgensen [ctb, cph] (grisu3 implementation)
   Maintainer: Jim Hester <james.hester@rstudio.com>
   Repository: CRAN
   Date/Publication: 2017-05-16 19:03:57 UTC



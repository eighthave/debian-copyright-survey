Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rivet
Source: http://www.hepforge.org/archive/rivet/

Files: *
Copyright: Leif Lonnblad <Leif.Lonnblad@thep.lu.se>
           Andy Buckley <andy.buckley@durham.ac.uk>
           Jon Butterworth <J.Butterworth@ucl.ac.uk>
License: GPL-2.0

Files: doc/rivet-manual.tex
Copyright: Andy Buckley
           Jonathan Butterworth
           Leif Lonnblad
	   Hendrik Hoeth
	   James Monk
	   Holger Schulz
	   Jan Eike von Seggern
	   Frank Siegert
	   Lars Sonnenschein
License: GPL-2.0

Files: include/Rivet/Projections/Spherocity.hh
       src/Analyses/ALEPH_1996_S3486095.cc
Copyright: Holger Schulz
License: GPL-2.0

Files: include/Rivet/Tools/ParticleIdUtils.hh
Copyright: Lynn Garren
           Andy Buckley
License: GPL-2.0

Files: src/Analyses/ATLAS_2010_S8591806.cc
       src/Analyses/CDF_2008_S7540469.cc
       src/Analyses/D0_2007_S7075677.cc
       src/Analyses/D0_2008_S7554427.cc
       src/Analyses/JADE_OPAL_2000_S4300807.cc
Copyright: Frank Siegert
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_I926145.cc
Copyright: Paul Bell
           Holger Schulz
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_S8924791.cc
Copyright: Andy Buckley
	   Judith Katzy
	   Francesc Vives
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_S8994773.cc
Copyright: Jinlong Zhang
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_S9120807.cc
Copyright: Giovanni Marchiori
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_S9131140.cc
Copyright: Yatsenko
	   Judith Katzy
License: GPL-2.0

Files: src/Analyses/ATLAS_2011_S9212183.cc
Copyright: Chris Wymant
License: GPL-2.0

Files: src/Analyses/ATLAS_2012_I1084540.cc
Copyright: Tim Martin <tim.martin@cern.ch>
License: GPL-2.0

Files: src/Analyses/BELLE_2006_S6265367.cc
Copyright: Eike von Seggern
License: GPL-2.0

Files: src/Analyses/CDF_1994_S2952106.cc
       src/Analyses/CDF_2006_S6653332.cc
       src/Analyses/D0_2001_S4674421.cc
       src/Analyses/D0_2004_S5992206.cc
Copyright: Lars Sonnenschein
License: GPL-2.0

Files: src/Analyses/CDF_2000_S4155203.cc
       src/Analyses/CDF_2002_S4796047.cc
       src/Analyses/CDF_2008_LEADINGJETS.cc
       src/Analyses/CDF_2008_NOTE_9351.cc
       src/Analyses/CDF_2009_S8233977.cc
       src/Analyses/DELPHI_1995_S3137023.cc
       src/Analyses/CDF_2010_S8591881_DY.cc
       src/Analyses/CDF_2010_S8591881_QCD.cc
       src/Analyses/DELPHI_1995_S3137023.cc
       src/Analyses/DELPHI_1996_S3430090.cc
       src/Analyses/DELPHI_2002_069_CONF_603.cc
       src/Analyses/DELPHI_2003_WUD_03_11.cc
       src/Analyses/OPAL_1998_S3780481.cc
       src/Analyses/PDG_HADRON_MULTIPLICITIES.cc
       src/Analyses/STAR_2009_UE_HELEN.cc
Copyright: Hendrik Hoeth
License: GPL-2.0

Files: src/Analyses/D0_2006_S6438750.cc
       src/Analyses/D0_2007_S7075677.cc
       src/Analyses/D0_2008_S7554427.cc
       src/Analyses/D0_2008_S7662670.cc
       src/Analyses/D0_2008_S7719523.cc
       src/Analyses/D0_2008_S7837160.cc
Copyright: Gavin Hesketh
License: GPL-2.0

Files: src/Analyses/D0_2008_S7863608.cc
Copyright: Gavin Hesketh
	   Andy Buckley
	   Frank Siegert
License: GPL-2.0

Files: src/Analyses/D0_2010_S8671338.cc
Copyright: Flavia Dias
License: GPL-2.0

Files: src/Analyses/CMS_2011_S8978280.cc
Copyright: Kevin Stenson
License: GPL-2.0

Files: src/Analyses/H1_1994_S2919893.cc
       src/Analyses/H1_2000_S4129130.cc
       src/Analyses/OPAL_1993_S2692198.cc
Copyright: Peter Richardson
License: GPL-2.0

Files: src/Analyses/PDG_HADRON_MULTIPLICITIES_RATIOS.cc
Copyright: Holger Schulz
License: GPL-2.0

Files: src/Analyses/ZEUS_2001_S4815815.cc
Copyright: Jon Butterworth
License: GPL-2.0

Files: src/Tools/mt2_bisect.cc
       src/Tools/mt2_bisect.hh
Copyright: Hsin-Chia Cheng
	   Zhenyu Han
License: GPL-2.0

Files: src/Tools/TinyXML/tinyxmlparser.cpp
       src/Tools/TinyXML/tinyxml.cpp
       src/Tools/TinyXML/tinyxmlerror.cpp
Copyright: 2000-2002 Lee Thomason
License: custom
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

Files: src/Tools/binreloc.c
       src/Tools/binreloc.h
Copyright: Hongli Lai <h.lai@chello.nl>
License: public-domain
 This source code is public domain. You can relicense this code under
 whatever license you want.

Files: doc/compare-histos.html doc/make-plots.html
Copyright: 2002 Mihai Bazon
           2006 Troy D. Hanson
           2006, 2009 Stuart Rackham
License: GPL-2.0

Files: debian/*
Copyright: 2012 Lifeng Sun <lifongsun@gmail.com>
License: GPL-2.0

License: GPL-2.0
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

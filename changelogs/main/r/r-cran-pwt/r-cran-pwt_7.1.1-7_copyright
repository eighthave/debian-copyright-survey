Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Penn World Tables for R
Upstream-Contact: Achim Zeileis <achim.zeileis@r-project.org>
Source: http://cran.r-project.org/web/packages/pwt/index.html

Files: *
Copyright: 2009-2012 Achim Zeileis <achim.zeileis@r-project.org>
License: GPL-2
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with Dynare.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: data/pwt*.rda
Copyright: 2002-2012 Center for International Comparisons of Production,
 Income and Prices (CIC), University of Pennsylvania
License: permissive
 No restriction on redistribution, modification and usage.
Comment: These data files are repackaged versions of the files freely
 downloadable on https://pwt.sas.upenn.edu/.
 While this website is unclear about the license under which these files are
 distributed, Alan Heston made it clear that there is no restriction of any kind
 on the redistribution, modification and usage of the files (private email
 sent to Sébastien Villemot on July 1, 2013).

Files: debian/*
Copyright: 2013 Sébastien Villemot <sebastien@debian.org>
License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with Dynare.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

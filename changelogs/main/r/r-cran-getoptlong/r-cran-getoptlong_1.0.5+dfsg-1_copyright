Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GetoptLong
Upstream-Contact: Zuguang Gu <z.gu@dkfz.de>
Source: https://cran.r-project.org/package=GetoptLong
Files-Excluded: */inst/doc/*.html

Files: *
Copyright: 2017-2018 Zuguang Gu
License: MIT

Files: inst/extdata/perl_lib/*
Copyright: 2005-2016, Makamaka Hannyaharamitu <makamaka@cpan.org>
License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

Files: inst/extdata/perl_lib/JSON/backportPP.pm
       inst/extdata/perl_lib/JSON/backportPP/Compat5005.pm
       inst/extdata/perl_lib/JSON/backportPP/Compat5006.pm
       inst/extdata/perl_lib/JSON.pm
Copyright: 2007-2011 by Makamaka Hannyaharamitu
License: GPL-1+ or Artistic
Comment:
    This program is free software; you can redistribute it and/or modify
    it under the terms of either:
 .
    a) the GNU General Public License as published by the Free Software
       Foundation; either version 1, or (at your option) any later
       version, or
 .
    b) the "Artistic License" which comes with Perl.
 ----------------------------------------
 The directories ext/, dist/, and cpan/ contain separate distributions
 that have been bundled with the Perl core. The copyright and license
 status of these have been detailed separately below.
 .
 It is assumed that all the other files are part of Perl and share the
 above copyright and license information unless explicitly specified
 differently. Only the exceptions have been detailed below.
 .
 As a small portion of the files are indeed licensed differently from
 the above, all the other licenses have been collected and/or duplicated
 at the end of this file to facilitate review.

Files: debian/*
Copyright: 2019 Andreas Tille <tille@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Artistic
 On Debian GNU/Linux systems, the complete text of the
 Artistic Licence can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

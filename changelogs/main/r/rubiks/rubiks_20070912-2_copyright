Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rubiks
Upstream-Contact: sage-devel <sage-devel@googlegroups.com>
Source: http://files.sagemath.org/spkg/upstream/rubiks/index.html

Files: dietz/*
Copyright: 2003-2005 Eric Dietz <root@wrongway.org>
License: GPL
 The source code for my various rubik solvers are freely available.
 They are, for the moment, released GPL (e.g., free to distribute and
 modify but it and its derivitives must always remain free and open
 source).
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL'

Files: dik/*
Copyright: 1993 Dik T. Winter <Dik.Winter@cwi.nl>
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: reid/*
Copyright: 1997 Michael Reid <reid@math.ucf.edu>
License: GPL-2+

Files: Makefile
Copyright: 2007 Michal Abshoff
License: public-domain
 Too short and simple to warrant any copyright.

Files: debian/*
Copyright: 2015 Julien Puydt <julien.puydt@laposte.net>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'

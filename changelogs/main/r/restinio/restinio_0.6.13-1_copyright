Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: restinio
Upstream-Contact: info@stiffstream.com
Source: https://github.com/Stiffstream/restinio

Files: *
Copyright: (c) 2017-2020 Stiffstream <info at stiffstream dot com>. All rights reserved.
License: BSD-3-Clause

Files: dev/restinio/third_party/zlib/*
Copyright: 1995-2017 Jean-loup Gailly and Mark Adler
License: Zlib

Files: dev/restinio/third_party/zlib/contrib/masmx64/inffas8664.c
Copyright: 1995-2003 Mark Adler, 2003 Chris Anderson <christop@charm.net>
License: Zlib

Files: dev/restinio/third_party/zlib/contrib/masmx64/gvmat64.asm
Copyright: 1995-2010 Jean-loup Gailly, Brian Raiter and Gilles Vollant
License: Zlib

Files: dev/restinio/third_party/zlib/contrib/asm686/match.S
Copyright: 998, 2007 Brian Raiter <breadbox@muppetlabs.com>
License: Zlib

Files: dev/restinio/third_party/variant-lite/variant.hpp dev/restinio/third_party/optional-lite/optional.hpp dev/so_5/3rd_party/optional-lite/nonstd/optional.hpp dev/restinio/third_party/string-view-lite/string_view.hpp
Copyright: 2014-2018 Martin Moene
License: BSL-1.0

Files: dev/restinio/third_party/expected-lite/expected.hpp
Copyright: 2016-2018 Martin Moene
License: BSL-1.0

Files: dev/asio/*
Copyright: 2003-2019 Christopher M. Kohlhoff (chris at kohlhoff dot com)
License: BSL-1.0

Files: dev/asio/include/asio/serial_port_base.hpp dev/asio/include/asio/detail/win_iocp_serial_port_service.hpp dev/asio/include/asio/detail/win_iocp_handle_service.hpp dev/asio/include/asio/detail/reactive_serial_port_service.hpp dev/asio/include/asio/detail/win_iocp_handle_read_op.hpp dev/asio/include/asio/detail/impl/win_iocp_serial_port_service.ipp dev/asio/include/asio/detail/impl/reactive_serial_port_service.ipp dev/asio/include/asio/detail/impl/win_iocp_handle_service.ipp dev/asio/include/asio/detail/win_iocp_handle_write_op.hpp dev/asio/include/asio/serial_port.hpp dev/asio/include/asio/basic_serial_port.hpp dev/asio/include/asio/impl/serial_port_base.hpp dev/asio/include/asio/impl/serial_port_base.ipp
Copyright: 2008 Rep Invariant Systems, Inc. (info@repinvariant.com)
License: BSL-1.0

Files: dev/asio/include/asio/detail/eventfd_select_interrupter.hpp dev/asio/include/asio/detail/impl/eventfd_select_interrupter.ipp
Copyright: 2008 Roelof Naude (roelof.naude at gmail dot com)
License: BSL-1.0

Files: dev/asio/include/asio/detail/kqueue_reactor.hpp dev/asio/include/asio/detail/impl/kqueue_reactor.ipp dev/asio/include/asio/detail/impl/kqueue_reactor.hpp
Copyright: 2005 Stefan Arentz (stefan at soze dot com)
License: BSL-1.0

Files: dev/asio/include/asio/detail/impl/win_object_handle_service.ipp dev/asio/include/asio/detail/win_object_handle_service.hpp dev/asio/include/asio/windows/basic_object_handle.hpp dev/asio/include/asio/windows/object_handle.hpp
Copyright: 2011 Boris Schaeling (boris@highscore.de)
License: BSL-1.0

Files: dev/asio/include/asio/ssl/detail/impl/openssl_init.ipp dev/asio/include/asio/ssl/impl/context.hpp dev/asio/include/asio/ssl/impl/context.ipp
Copyright: 2005 Voipster / Indrek dot Juhani at voipster dot com
License: BSL-1.0

Files: dev/asio/include/asio/ip/network_v4.hpp dev/asio/include/asio/ip/impl/network_v4.ipp dev/asio/include/asio/ip/impl/network_v4.hpp dev/asio/include/asio/ip/impl/network_v6.ipp dev/asio/include/asio/ip/network_v6.hpp
Copyright: 2014 Oliver Kowalke (oliver dot kowalke at gmail dot com)
License: BSL-1.0

Files: dev/fmt/*
Copyright: 2012 - present, Victor Zverovich and fmt contributors
License: MIT

Files: dev/fmt/support/docopt.py
Copyright: 2013 Vladimir Keleshev, vladimir@keleshev.com
License: MIT

Files: dev/fmt/include/fmt/chrono.h
Copyright: Paul Dreik 2019
License: MIT

Files: dev/fmt/include/fmt/ranges.h
Copyright: 2018 - present, Remotion (Igor Schulz)
License: MIT

Files: dev/cmake/modules/FindPCRE.cmake
Copyright: 2007-2009 LuaDist
License: MIT

Files: dev/nodejs/*
Copyright: Joyent, Inc. and other Node contributors. All rights reserved.
License: MIT

Files: dev/rapidjson/*
Copyright: 2015 THL A29 Limited, a Tencent company, and Milo Yip
License: MIT

Files: dev/rapidjson/include/rapidjson/msinttypes/inttypes.h dev/rapidjson/include/rapidjson/msinttypes/stdint.h
Copyright: 2006-2013 Alexander Chemeris
License: BSL-1.0

Files: dev/catch2/* dev/clara/clara.hpp dev/catch2/catch.hpp
Copyright: 2014-2019 Two Blue Cubes Ltd
License: BSL-1.0

Files: dev/catch2/catch_reporter_sonarqube.hpp
Copyright: Social Point SL
License: BSL-1.0

Files: dev/catch2/catch_reporter_tap.hpp
Copyright: 2015 Martin Moene
License: BSL-1.0

Files: dev/catch2/catch_reporter_automake.hpp
Copyright: 2017 Justin R. Wilson
License: BSL-1.0

Files: debian/*
Copyright: 2020 Felix Salfelder <felix@salfelder.org>, 2020 Sebastien Delafond <seb@debian.org>
License: GPL-3
 See `/usr/share/common-licenses/GPL-3'

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or
 organization obtaining a copy of the software and accompanying
 documentation covered by this license (the "Software") to use,
 reproduce, display, distribute, execute, and transmit the
 Software, and to prepare derivative works of the Software, and
 to permit third-parties to whom the Software is furnished to do
 so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement,
 including the above license grant, this restriction and the
 following disclaimer, must be included in all copies of the
 Software, in whole or in part, and all derivative works of the
 Software, unless such copies or derivative works are solely in
 the form of machine-executable object code generated by a source
 language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
 NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
 ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE FOR ANY DAMAGES OR
 OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.
 .
 Jean-loup Gailly        Mark Adler
 jloup@gzip.org          madler@alumni.caltech.edu
 .
 If you use the zlib library in a product, we would appreciate *not* receiving
 lengthy legal documents to sign.  The sources are provided for free but without
 warranty of any kind.  The library has been entirely written by Jean-loup
 Gailly and Mark Adler; it does not include third-party code.
 .
 If you redistribute modified sources, we would appreciate that you include in
 the file ChangeLog history information documenting your changes.  Please read
 the FAQ for more information on the distribution of modified source versions.

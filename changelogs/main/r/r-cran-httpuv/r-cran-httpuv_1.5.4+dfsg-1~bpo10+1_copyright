Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: httpuv
Upstream-Contact: Joe Cheng <joe@rstudio.com>
Source: https://cran.r-project.org/package=httpuv
Files-Excluded: */libuv

Files: *
Copyright: RStudio, Inc.
License: GPL-3

Files: debian/*
Copyright: 2015 Tilburg University https://tilburguniversity.edu/
Comment: author: Joost van Baal-Ilić
License: GPL-3

Files: src/http-parser/http_parser.c
Copyright: Igor Sysoev, Joyent, Inc., other Node contributors
Comment: Based on src/http/ngx_http_parse.c from NGINX copyright Igor Sysoev
 Additional changes are licensed under the same terms as NGINX and
 copyright Joyent, Inc. and other Node contributors.
License: Node

License: GPL-3
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: Node
 All rights reserved.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RESTEasy
Source: http://rest-easy.org
Files-Excluded: travis-libs

Files: *
Copyright: 2013, Bill Burke
License: Apache-2.0

Files: jboss-modules/*
       resteasy-cdi/src/main/java/org/jboss/resteasy/cdi/CdiPropertyInjector.java
       security-legacy/keystone/keystone-as7-modules/*
Copyright: 2010-2012, Red Hat Inc.
           2010, Jozef Hartinger <jharting@redhat.com>
License: LGPL-2.1+

Files: eagledns/src/main/java/se/unlogic/standardutils/*
Copyright: 2010, Robert "Unlogic" Olofsson <unlogic@unlogic.se>
License: LGPL-3
 On Debian systems, the full text of the LGPL-3 license
 can be found in the file '/usr/share/common-licenses/LGPL-3'

Files: eagledns/src/main/java/se/unlogic/standardutils/crypto/Base64.java
Copyright: 2010, Robert Harder <rob@iharder.net>
License: public-domain
 The author places this code in the Public Domain. Do with it as you will.
 This software comes with no guarantees or warranties but with plenty of
 well-wishing instead!

Files: tjws/src/main/java/Acme/Serve/ThrottleItem.java
       tjws/src/main/java/Acme/Serve/ThrottledOutputStream.java
       tjws/src/main/java/Acme/Serve/CgiServlet.java
       tjws/src/main/java/Acme/WildcardDictionary.java
Copyright: 1996-1998, Jef Poskanzer <jef@acme.com>
License: BSD-2-clause

Files: tjws/src/main/java/Acme/Serve/Serve.java
       tjws/src/main/java/Acme/Serve/FileServlet.java
       tjws/src/main/java/Acme/Utils.java
Copyright: 1996-1998, Jef Poskanzer <jef@acme.com>
           1998-2010, Dmitriy Rogatkin
License: BSD-2-clause

Files: tjws/src/main/java/Acme/Serve/SimpleAcceptor.java
       tjws/src/main/java/Acme/Serve/WarDeployer.java
       tjws/src/main/java/Acme/Serve/SSLAcceptor.java
       tjws/src/main/java/Acme/Serve/SelectorAcceptor.java
Copyright: 1998-2010, Dmitriy Rogatkin
License: BSD-2-clause

Files: debian/*
Copyright: 2013, Ade Lee <alee@redhat.com>
License: Apache-2.0 or BSD-2-clause or LGPL-2.1+

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: LGPL-2.1+
 On Debian systems, the full text of the LGPL-2.1 license
 can be found in the file '/usr/share/common-licenses/LGPL-2.1'

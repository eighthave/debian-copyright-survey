This is the Debian GNU/Linux r-cran-fastica package of fastica.  The
fastica package provides tools to work with categorical variables. It
was written by J L Marchin, C Heaton and B D Ripley.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'fastICA' to
'r-cran-fastica' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Files: *
Copyright: 2001 - 2016  J L Marchini, C Heaton and B D Ripley
License: GPL-2+

Files: debian/*
Copyright: 2017  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2 and the GPL license
(version 3) is included in the file /usr/share/common-licenses/GPL-3

For reference, the upstream DESCRIPTION file is included below:

   Package: fastICA
   Version: 1.2-1
   Date: 2017-06-12
   Title: FastICA Algorithms to Perform ICA and Projection Pursuit
   Author: J L Marchini <marchini@stats.ox.ac.uk>, C Heaton
           <chrisheaton99@yahoo.com> and B D Ripley
           <ripley@stats.ox.ac.uk>
   Maintainer: Brian Ripley <ripley@stats.ox.ac.uk>
   Depends: R (>= 3.0.0)
   Suggests: MASS
   Description: Implementation of FastICA algorithm to perform Independent
           Component Analysis (ICA) and Projection Pursuit.
   License: GPL-2 | GPL-3
   Packaged: 2017-06-12 07:49:50 UTC; ripley
   NeedsCompilation: yes
   Repository: CRAN
   Date/Publication: 2017-06-12 10:10:37 UTC




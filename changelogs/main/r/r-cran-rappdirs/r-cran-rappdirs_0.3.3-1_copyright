Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rappdirs
Upstream-Contact: Hadley Wickham <hadley@rstudio.com>
Source: https://cran.r-project.org/package=rappdirs

Files: *
Copyright: 2010-2016 Hadley Wickham, RStudio, Sridhar Ratnakumar, Trent Mick
License: MIT

Files: R/appdir.r
       R/cache.r
       R/data.r
       R/log.r
Copyright: 2010 ActiveState, Eddy Petrisor, Trevor Davis,
                      Gabor Csardi, Gregory Jefferis
License: MIT

Files: debian/*
Copyright: 2018 Andreas Tille <tille@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

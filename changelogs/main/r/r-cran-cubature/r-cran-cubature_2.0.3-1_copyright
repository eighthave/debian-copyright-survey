This is the Debian GNU/Linux r-cran-cubature package of cubature.  The
cubature package provides adaptive multivariate integration over
hypercubes. It was written by Balasubramanian Narasimhan based on C
code by Steven G. Johnson.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'cubature' to
'r-cran-cubature' to fit the pattern of CRAN (and non-CRAN) packages
for R.


Files: src/cubature.*
Copyright: 2005-2009 Steven G. Johnson
Portions Copyright: 2002-2005 Rudolf Schuere
Portions Copyright: 1996-2000 Brian Gough
License: GPL-2+


Files: R/integrate.R
File: src/rcubature.c
Copyright: 2009-2014 Balasubramanian Narasimhan
License: GPL-2+


Files: debian/*
Copyright: 2014  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included in the file
/usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: cubature
   Type: Package
   Title: Adaptive multivariate integration over hypercubes
   Version: 1.1-2
   Date: 2013-02-24
   Author: C code by Steven G. Johnson, R by Balasubramanian Narasimhan
   Maintainer: Balasubramanian Narasimhan <naras@stat.stanford.edu>
   Description: Adaptive multivariate integration over hypercubes
   License: GPL (>= 2)
   Packaged: 2013-02-24 20:30:56 UTC; naras
   NeedsCompilation: yes
   Repository: CRAN
   Date/Publication: 2013-02-25 07:34:36

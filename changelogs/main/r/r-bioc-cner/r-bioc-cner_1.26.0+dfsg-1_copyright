Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CNEr
Upstream-Contact: Ge Tan <ge_tan@live.com>
Source: https://bioconductor.org/packages/CNEr/
Files-Excluded: */inst/doc/*.html

Files: *
Copyright: 2012-2018 Ge Tan <ge_tan@live.com>
License: GPL-2

Files: src/ucsc/*
Copyright: 2001-2018 Jim Kent, UC Regents
License: MIT

Files: src/ucsc/_*
Copyright: 2001-2018 Jim Kent, UC Regents
           Michael Lawrence <michafla@gene.com> (changes for r-bioc-rtracklayer)
License: MIT

Files: src/ucsc/os.c
Copyright: 2016-2018 Michael Lawrence <michafla@gene.com> (added for r-bioc-rtracklayer)
License: MIT

Files: debian/*
Copyright: 2018 Andreas Tille <tille@debian.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 2 at /usr/share/common-licenses/GPL-2.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


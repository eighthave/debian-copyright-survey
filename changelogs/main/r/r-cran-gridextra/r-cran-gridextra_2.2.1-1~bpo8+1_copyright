This is the Debian GNU/Linux r-cran-gridextra package of gridExtra.
The gridExtra package provides higher-level functions for grid
graphics in R, and was written by Baptiste Auguie.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'gridextra' to
'r-cran-gridextra' to fit the pattern of CRAN (and non-CRAN) packages
for R. 


Files: *
Copyright: 2010 - 2012  Baptiste Auguie
License: GPL (>= 2)

Files: debian/*
Copyright: 2015  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+


On a Debian GNU/Linux system, the GPL license (version 2) 
is included in the files /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: gridExtra
   Maintainer: baptiste <baptiste.auguie@gmail.com>
   License: GPL (>= 2)
   Title: functions in Grid graphics
   Type: Package
   Author: Baptiste Auguie
   Description: misc. high-level Grid functions
   Version: 0.9.1
   URL: https://github.com/baptiste/gridextra
   Date: 2012-09-08
   Depends: R(>= 2.5.0), grid
   Suggests: RGraphics, pixmap, EBImage, ggplot2, lattice
   Collate: 'arcText.r' 'arrange.r' 'barbedGrob.r' 'borderGrob.r'
           'colorstripGrob.r' 'ebimage.r' 'ellipseGrob.r'
           'gridExtra-package.r' 'grob-utils.r' 'polygon.regular.r'
           'ngonGrob.r' 'patternGrob.r' 'pixmap.r' 'rpattern.r'
           'stextGrob.r' 'tableGrob.r'
   Packaged: 2012-08-09 00:55:23 UTC; auguieba
   Repository: CRAN
   Date/Publication: 2012-08-09 05:38:06

NB In DESCRIPTION copy above URL updated to GitHub.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hashery
Source: https://github.com/rubyworks/hashery

Files: *
Copyright: 2010 Rubyworks
License: BSD-2-clause

Files: lib/hashery/association.rb
Copyright: 2005 Rubyworks, Thomas Sawer
License: BSD-2-clause

Files: lib/hashery/dictionary.rb
Copyright: 2005 Jan Molic
License: BSD-2-clause
Comment: derived work of http://codeforpeople.com/lib/ruby/orderedhash
  public domain

Files: lib/hashery/fuzzy_hash.rb
Copyright: 2009 Joshua Hull
License: BSD-2-clause

Files: lib/hashery/ini_hash.rb
Copyright: 2007 Jeena Paradies <info@jeenaparadies.net>
License: BSD-2-clause

Files: lib/hashery/linked_list.rb
Copyright: 2006 Kirk Haines <khaines@enigo.com>
License: BSD-2-clause

Files: lib/hashery/lru_hash.rb
Copyright: 2010 Robert Klemme
License: BSD-2-clause

Files: lib/hashery/path_hash.rb
Copyright: 2006 BehindLogic (http://hash_magic.rubyforge.org)
License: BSD-2-clause

Files: debian/*
Copyright: 2013 Cédric Boutillier <boutil@debian.org>
License: BSD-2-clause
Comment: the Debian packaging is licensed under the same terms as the original package.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR  SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

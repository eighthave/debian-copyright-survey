This is the Debian GNU/Linux r-cran-pbkrtest package of pbkrtest.  The
pbkrtest package provides tests for linear mixed effects models, and was 
written by Ulrich Halekoh and Søren Højsgaard.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'pbkrtest' to
'r-cran-pbkrtest' to fit the pattern of CRAN (and non-CRAN) packages
for R. 


Files: *
Copyright: 2011 - 2014  Ulrich Halekoh and Søren Højsgaard.
License: GPL (>= 2)

Files: debian/*
Copyright: 2015  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+


On a Debian GNU/Linux system, the GPL license (version 2) 
is included in the files /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: pbkrtest
   Version: 0.4-2
   Title: Parametric bootstrap and Kenward-Roger-based methods for mixed
           model comparison
   Author: Ulrich Halekoh <uhalekoh@health.sdu.dk> Søren Højsgaard <sorenh@math.aau.dk>
   Maintainer: Søren Højsgaard <sorenh@math.aau.dk>
   Description: Test in linear mixed effects models.
    .
      Attention is on linear mixed effects models as implemented in the
    lme4 package.
    .
      The package implements a parametric bootstrap test.
    .
      The package implements a Kenward-Roger modification of F-tests.
   URL: http://people.math.aau.dk/~sorenh/software/pbkrtest/
   Depends: R (>= 3.0.0), lme4
   Imports: Matrix, parallel, MASS
   Suggests: gplots
   Encoding: latin1
   ZipData: no
   License: GPL (>= 2)
   Packaged: 2014-11-13 08:33:37 UTC; sorenh
   NeedsCompilation: no
   Repository: CRAN
   Date/Publication: 2014-11-13 14:43:54


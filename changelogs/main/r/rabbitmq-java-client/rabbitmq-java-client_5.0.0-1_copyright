Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: rabbitmq-java-client
Source: https://github.com/rabbitmq/rabbitmq-java-client/
Files-Excluded: doc/channels/channels.pdf
 doc/channels/worktransition.pdf
 doc/channels/whiteboard.JPG
Comment: PDFs can be built from tex files
 doc/channels/whiteboard.JPG contains non-free ICC profile
 fixed upstream https://github.com/rabbitmq/rabbitmq-java-client/issues/322 

Files: *
Copyright: 2007-2017 Present Pivotal Software, Inc. <info@rabbitmq.com>
License: MPL-1.1 or GPL-2+ or Apache-2

Files: debian/*
Copyright: 2017 Christopher Hoskin <mans0954@debian.org>
License: MPL-1.1 or GPL-2+ or Apache-2

Files: codegen/*
Copyright: 2007-2017 Present Pivotal Software, Inc. <info@rabbitmq.com>
License: MPL-1.1

Files: codegen/amqp-1.0/*
Copyright: 2006-2011 Copyright Bank of America, N.A.
 2006-2011 Barclays Bank PLC
 2006-2011 Cisco Systems
 2006-2011 Credit Suisse
 2006-2011 Deutsche Boerse
 2006-2011 Envoy Technologies Inc.
 2006-2011 Goldman Sachs
 2006-2011 HCL Technologies Ltd
 2006-2011 IIT Software GmbH
 2006-2011 iMatix Corporation
 2006-2011 INETCO Systems Limited
 2006-2011 Informatica Corporation
 2006-2011 JPMorgan Chase & Co.
 2006-2011 Kaazing Corporation, N.A
 2006-2011 Microsoft Corporation
 2006-2011 my-Channels
 2006-2011 Novell
 2006-2011 Progress Software
 2006-2011 Red Hat Inc.
 2006-2011 Software AG
 2006-2011 Solace Systems Inc.
 2006-2011 StormMQ Ltd.
 2006-2011 Tervela Inc.
 2006-2011 TWIST Process
 2006-2011 Innovations Ltd
 2006-2011 GoPivotal, Inc.
 2006-2011 WS02 Inc.
License: BSD-3-clause

Files: ./codegen/amqp-rabbitmq-0.8.json
 ./codegen/amqp-rabbitmq-0.9.1.json
 ./codegen/credit_extension.json
Copyright: 2008-2016, Pivotal Software, Inc.
License: Expat

Files: ./codegen/amqp_codegen.py
Copyright: 2007-2014, GoPivotal, Inc.
License: MPL-1.1

Files: ./codegen/demo_extension.json
Copyright: 2009-2013, GoPivotal, Inc.
License: MPL-1.1

Files: ./doc/channels/zed-cm.sty
Copyright: 1997 Jim Davies <Jim.Davies@comlab.ox.ac.uk>
License: zed-cm
 You may copy and distribute this file freely.  Any queries and
 complaints can be forwarded to Jim.Davies@comlab.ox.ac.uk.  However,
 the author accepts no liability for the accuracy of these macros, or
 their fitness for any purpose.  If you make any changes to this
 file, please do not distribute the results under the name `zed-cm.sty'.  


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 .
 On Debian systems, the complete text of the Apache-2.0 license can be found in
 "/usr/share/common-licenses/Apache-2.0"
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

License: MPL-1.1
 The contents of this file are subject to the Mozilla Public License
 Version 1.1 (the "License"); you may not use this file except in
 compliance with the License.
 .
 On Debian systems, the complete text of the Mozilla Public License
 Version 1.1 can be found in "/usr/share/common-licenses/MPL-1.1"
 .
 Software distributed under the License is distributed on an "AS IS"
 basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 License for the specific language governing rights and limitations
 under the License.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: maptools
Upstream-Contact: Roger Bivand <Roger.Bivand@nhh.no>
Source: https://cran.r-project.org/package=maptools
Files-Excluded: data/wrld_simpl.rda
                data/gpcholes.rda
                data/state.vbm.rda
                inst/shapes/pointZ.*
                inst/shapes/baltim.*
                inst/shapes/columbus.*
                inst/shapes/sids.*

Files: *
Copyright: © 2001-2016 Nicholas J. Lewin-Koh and Roger Bivand
License: GPL-2+

Files: inst/shapes/fylk-val.*
Copyright: Statens Kartverk 2001
License: PD
 These files were released (with a collection of other shapefiles) to the
 public domain in July 2001. The link to the relevant page under
 http://www.statkart.no has now expired, this copy downloaded 13 July 2001.

Files: R/GE_png.R
Copyright: © 2007 by Duncan Golicher, David Forrest and Roger Bivand
License: GPL-2+

Files: R/point.in.polygon.R
Copyright: © 2004 Edzer Pebesma
License: GPL-2+
Comment: This file was copied from the R sp package, released under the GNU
 GPL version 2 or any later version.
 See http://downloads.sourceforge.net/r-spatial/sp_0.7-0.tar.gz for confirmation.

Files: R/readGPS.R
Copyright: © 2007 Patrick Giraudoux and Roger Bivand
License: GPL-2+

Files: R/sp2Mondrian.R
Copyright: © 2006-7 Patrick Hausmann and Roger Bivand
License: GPL-2+

Files: src/pip.c
Copyright: © 2004 Edzer Pebesma, with portion © 1998 by Joseph O'Rourke.
License: GPL-2+_with_portion
 Written by Joseph O'Rourke, contributions by Min Xu, June 1997.
 Questions to orourke@cs.smith.edu.
 .
 This code is Copyright 1998 by Joseph O'Rourke.  It may be freely 
 redistributed in its entirety provided that this copyright notice is 
 not removed.
Comment: This file was copied from the R sp package, released under the GNU
 GPL version 2 or any later version.
 See http://downloads.sourceforge.net/r-spatial/sp_0.7-0.tar.gz for confirmation.

Files: src/shapefil.h src/shpopen.c src/shptree.c
Copyright: © 1999, 2001, Frank Warmerdam <warmerdam@pobox.com>
License: Expat
 This software is available under the following "MIT Style" license,
 or at the option of the licensee under the LGPL (see LICENSE.LGPL).  This
 option is discussed in more detail in shapelib.html.
 .
 --
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
Comment: quoting from http://shapelib.maptools.org/license.html :
 I am pleased to receive bug fixes, and improvements for Shapelib. Unless the
 submissions indicate otherwise I will assume that changes submitted to me
 remain under the above "dual license" terms. If changes are made to the
 library with the intention that those changes should be protected by the LGPL
 then I should be informed upon submission. Note that I will not generally
 incorporate changes into the core of Shapelib that are protected under the LGPL
 as this would effectively limit the whole file and distribution to LGPL terms.

Files: src/Rcentroid.c
Copyright: © Joseph O'Rourke, Carl Anderson, Frank Warmerdam, Nicholas Lewin-Koh, Roger S. Bivand
License: GPL-2+

Files: src/Rgshhs.*
Copyright: © 1996-2008 P. Wessel and W. H. F. Smith
           © 2005-7 Roger Bivand
License: GPL-2+

Files: debian/*
Copyright: 2009-2016 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL'.

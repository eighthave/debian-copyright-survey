Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pracma
Upstream-Contact: Hans W. Borchers <hwborchers@googlemail.com>
Source: https://cran.r-project.org/package=pracma

Files: *
Copyright: 2010-2018 Hans W. Borchers
License: GPL-3+

Files: R/fractalcurve.R
Copyright: 2011 Jonas Lundgren
License: GPL-3+

Files: R/quadgr.R
Copyright: 2009 Jonas Lundgren
License: GPL-3+

Files: R/triquad.R
       R/cotes.R
       R/barycentric.R
Copyright: 2004-2005 Greg von Winckel
License: GPL-3+

Files: R/rationalfit.R
       R/gamma.R
       R/zeta.R
Copyright: 2001-2006 Paul Godfrey
License: GPL-3+
Comment: man/eta.Rd is mentioning Eta function with copyright by
 Paul Godfrey but this is not used in code

Files: R/integral2.R
Copyright: 2008 Lawrence F. Shampine
License: GPL-3+

Files: R/quadl.R
       R/quad.R
Copyright: 1998 Walter Gautschi
License: GPL-3+

Files: R/ppfit.R
Copyright: 2012 Ben Abbott, Martin Helm
License: GPL-3+

Files: R/ode*.R
       R/pchip.R
Copyright: 2004 Cleve Moler
License: GPL-3+

Files: R/anms.R
Copyright: 2012 F. Gao, L. Han
License: GPL-3+

Files: R/distmat.R
Copyright: 1999 Roland Bunschoten
License: GPL-3+

Files: R/fresnel.R
Copyright: 1996 Zhang and Jin
License: GPL-3+

Files: R/dblquad.R
Copyright: 2008 W. Padden and Ch. Macaskill
License: GPL-3+

Files: debian/*
Copyright: 2018 Oliver Dechant <dechant@dal.ca>,
                Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.

This is the Debian GNU/Linux r-cran-sparsem package of SparseM.
The SparseM package provides basic linear algebra for sparse matrices
and was written by Roger Koenker and Pin Ng.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'SparseM' to
'r-cran-sparsem' to fit the pattern of CRAN (and non-CRAN) packages
for R.

Files: *
Copyright: 2002 - 2013  Roger Koenker and Pin Ng
License: GPL-2

Files: debian/*
Copyright: 2013  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2


On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

   Package: SparseM
   Version: 1.03
   Author: Roger Koenker <rkoenker@uiuc.edu> and Pin Ng <Pin.Ng@NAU.EDU>
   Maintainer: Roger Koenker <rkoenker@uiuc.edu>
   Depends: R (>= 2.4.1), methods, stats, utils
   Description: Basic linear algebra for sparse matrices
   License: GPL (>= 2)
   Title: Sparse Linear Algebra
   URL: http://www.econ.uiuc.edu/~roger/research/sparse/sparse.html
   Packaged: 2013-08-01 15:28:43 UTC; root
   NeedsCompilation: yes
   Repository: CRAN
   Date/Publication: 2013-08-01 18:50:17

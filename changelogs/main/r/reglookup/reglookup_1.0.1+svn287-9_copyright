Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RegLookup
Upstream-Contact:  Timothy D. Morgan <tim-debian@sentinelchicken.org, get.secure@blindspotsecurity.com>
Source: http://projects.sentinelchicken.org/reglookup

Files: *
Copyright: 1992-1998 Andrew Tridgell
           1992-2005 Samba development team
           2002      Richard Sharpe <rsharpe@richardsharpe.com>
           2005      Gerald (Jerry) Carter
           2005-2011 Timothy D. Morgan <tim-debian@sentinelchicken.org>
           2010      Michael Cohen <scudette@users.sourceforge.net>
           2010      Tobias Mueller
License: GPL-3

Files: bin/reglookup-timeline
Copyright: 2005-2010 Timothy D. Morgan
License: GPL-2

Files: python/experimental/include/class.h
       python/experimental/regfi/class.c
Copyright: 2004 Commonwealth of Australia
           2004 Michael Cohen <scudette@users.sourceforge.net>
License: GPL-2+

Files: debian/*
Copyright: 2008      David Paleino <d.paleino@gmail.com>
           2008-2010 Christophe Monniez <christophe.monniez@fccu.be>
           2008-2010 Daniel Baumann <daniel@debian.org>
           2010      Michael Prokop <mika@debian.org>
           2014-2015 Joao Eriberto Mota Filho <eriberto@debian.org>
           2016-2021 Giovani Augusto Ferreira <giovani@debian.org>
           2019-2020 Samuel Henrique <samueloph@debian.org>
License: GPL-2+

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

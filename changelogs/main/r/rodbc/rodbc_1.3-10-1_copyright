This is the Debian GNU/Linux r-cran-rodbc package of rodbc, an ODBC
database access package for GNU R. RODBC was originally written by
Michael Lapsley <mlapsley@sthelier.sghms.ac.u> and is now maintained
by B. D. Ripley <ripley@stats.ox.ac.uk>.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'RODBC' to 'r-cran-rodbc'
to highlight the fact that is it a CRAN package for R, and to match
Debian's convention of lowercase package names.

Copyright (C) 1997 - 2008 Michael Lapsley and Brian D. Ripley 

License: GPL-2

On a Debian GNU/Linux system, the GPL (v2) license is included in the
file /usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION file is included below:

    Package: RODBC
    Version: 1.0-1
    Date: 2003-01-20
    Author: Originally Michael Lapsley <mlapsley@sthelier.sghms.ac.uk>,
    	later B. D. Ripley <ripley@stats.ox.ac.uk>
    Maintainer: B. D. Ripley <ripley@stats.ox.ac.uk>
    Title: ODBC database access
    Description: An ODBC database interface
    Depends: R (>= 1.6.0)
    License: GPL2

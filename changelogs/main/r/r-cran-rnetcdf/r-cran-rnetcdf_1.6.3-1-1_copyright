Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: RNetCDF
Upstream-Contact: Pavel Michna <michna@giub.unibe.ch>
Source: http://cran.r-project.org/web/packages/RNetCDF/index.html

Files: *
Copyright: 2004-2012 Pavel Michna <michna@giub.unibe.ch>
License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the
 GNU General Public License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: man/*
Copyright: 1993-1999 University Corporation for Atmospheric Research/Unidata
License: NetCDF
 .
 Because large parts of the documentation are taken from the "NetCDF
 User's Guide for C" and from the udunits(3) manpage, it is
 appropriate to cite Ucar's copyright notice for the NetCDF library:
 .
 Portions of this software were developed by the Unidata Program at the
 University Corporation for Atmospheric Research.
 .
 Access and use of this software shall impose the following obligations
 and understandings on the user. The user is granted the right, without
 any fee or cost, to use, copy, modify, alter, enhance and distribute
 this software, and any derivative works thereof, and its supporting
 documentation for any purpose whatsoever, provided that this entire
 notice appears in all copies of the software, derivative works and
 supporting documentation.  Further, UCAR requests that the user credit
 UCAR/Unidata in any publications that result from the use of this
 software or in any product that includes this software. The names UCAR
 and/or Unidata, however, may not be used in any advertising or publicity
 to endorse or promote any products or commercial entity unless specific
 written permission is obtained from UCAR/Unidata. The user also
 understands that UCAR/Unidata is not obligated to provide the user with
 any support, consulting, training or assistance of any kind with regard
 to the use, operation and performance of this software nor to provide
 the user with any updates, revisions, new versions or "bug fixes."
 .
 THIS SOFTWARE IS PROVIDED BY UCAR/UNIDATA "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL UCAR/UNIDATA BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE.

Files: inst/HDF5_COPYING
Copyright: 2006-2012 by The HDF Group
License: NCSA-HDF5
 Although this package does not itself use HDF5, if statically linked
 to a build of netcdf with the netcdf4 feature included it will link in
 code from HDF5, in which case the following licence applies to such code.
 .
 =======================================================================
 .
 Copyright Notice and License Terms for
 HDF5 (Hierarchical Data Format 5) Software Library and Utilities
 -----------------------------------------------------------------------------
 .
 HDF5 (Hierarchical Data Format 5) Software Library and Utilities
 Copyright 2006-2012 by The HDF Group.
 .
 NCSA HDF5 (Hierarchical Data Format 5) Software Library and Utilities
 Copyright 1998-2006 by the Board of Trustees of the University of Illinois.
 .
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted for any purpose (including commercial purposes)
 provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions, and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions, and the following disclaimer in the documentation
    and/or materials provided with the distribution.
 .
 3. In addition, redistributions of modified forms of the source or binary
    code must carry prominent notices stating that the original code was
    changed and the date of the change.
 .
 4. All publications or advertising materials mentioning features or use of
    this software are asked, but not required, to acknowledge that it was
    developed by The HDF Group and by the National Center for Supercomputing
    Applications at the University of Illinois at Urbana-Champaign and
    credit the contributors.
 .
 5. Neither the name of The HDF Group, the name of the University, nor the
    name of any Contributor may be used to endorse or promote products derived
    from this software without specific prior written permission from
    The HDF Group, the University, or the Contributor, respectively.
 .
 DISCLAIMER:
 THIS SOFTWARE IS PROVIDED BY THE HDF GROUP AND THE CONTRIBUTORS
 "AS IS" WITH NO WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED.  In no
 event shall The HDF Group or the Contributors be liable for any damages
 suffered by the users arising out of the use of this software, even if
 advised of the possibility of such damage.
 .
 -----------------------------------------------------------------------------
 -----------------------------------------------------------------------------
 .
 Contributors:   National Center for Supercomputing Applications (NCSA) at
 the University of Illinois, Fortner Software, Unidata Program Center (netCDF),
 The Independent JPEG Group (JPEG), Jean-loup Gailly and Mark Adler (gzip),
 and Digital Equipment Corporation (DEC).
 .
 -----------------------------------------------------------------------------
 .
 Portions of HDF5 were developed with support from the Lawrence Berkeley
 National Laboratory (LBNL) and the United States Department of Energy
 under Prime Contract No. DE-AC02-05CH11231.
 .
 -----------------------------------------------------------------------------
 .
 Portions of HDF5 were developed with support from the University of
 California, Lawrence Livermore National Laboratory (UC LLNL).
 The following statement applies to those portions of the product and must
 be retained in any redistribution of source code, binaries, documentation,
 and/or accompanying materials:
 .
    This work was partially produced at the University of California,
    Lawrence Livermore National Laboratory (UC LLNL) under contract
    no. W-7405-ENG-48 (Contract 48) between the U.S. Department of Energy
    (DOE) and The Regents of the University of California (University)
    for the operation of UC LLNL.
 .
    DISCLAIMER:
    This work was prepared as an account of work sponsored by an agency of
    the United States Government. Neither the United States Government nor
    the University of California nor any of their employees, makes any
    warranty, express or implied, or assumes any liability or responsibility
    for the accuracy, completeness, or usefulness of any information,
    apparatus, product, or process disclosed, or represents that its use
    would not infringe privately- owned rights. Reference herein to any
    specific commercial products, process, or service by trade name,
    trademark, manufacturer, or otherwise, does not necessarily constitute
    or imply its endorsement, recommendation, or favoring by the United
    States Government or the University of California. The views and
    opinions of authors expressed herein do not necessarily state or reflect
    those of the United States Government or the University of California,
    and shall not be used for advertising or product endorsement purposes.
 -----------------------------------------------------------------------------

Files: debian/*
Copyright: 2014 Sebastian Gibb <sgibb.debian@gmail.com>
License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the
 GNU General Public License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.


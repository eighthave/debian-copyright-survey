Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: effectsize
Upstream-Contact: Dominique Makowski <dom.makowski@gmail.com>
Source: https://cran.r-project.org/package=effectsize

Files: *
Copyright: 2019 Dominique Makowski,
 Daniel Lüdecke,
 Mattan S. Ben-Shachar,
 Ken Kelley,
 David Stanley
License: GPL-3

Files: vignettes/apa.csl
Copyright: 2016 Sebastian Karcher
License: CC-BY-SA-3.0
 This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 License
 http://creativecommons.org/licenses/by-sa/3.0/
 .
 This is a human-readable summary of (and not a substitute for) the license.
 Disclaimer.
 .
 You are free to:
 .
  • Share — copy and redistribute the material in any medium or format
  • Adapt — remix, transform, and build upon the material
  • for any purpose, even commercially.
  •
 .
 This license is acceptable for Free Cultural Works.
 .
  • The licensor cannot revoke these freedoms as long as you follow the license
    terms.
 .
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 .
 Under the following terms:
 .
  • Attribution — You must give appropriate credit, provide a link to the
    license, and indicate if changes were made. You may do so in any reasonable
    manner, but not in any way that suggests the licensor endorses you or your
    use.
 .
    Attribute this work:
 .
  • ShareAlike — If you remix, transform, or build upon the material, you must
    distribute your contributions under the same license as the original.
 .
  • No additional restrictions — You may not apply legal terms or technological
    measures that legally restrict others from doing anything the license
    permits.

Files: debian/*
Copyright: 2019 Andreas Tille <tille@debian.org>
License: GPL-3

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the full text of the GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.



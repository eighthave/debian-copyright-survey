Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: raster
Upstream-Contact: Robert J. Hijmans <r.hijmans@gmail.com>
Source: https://cran.r-project.org/package=raster

Files: *
Copyright: 2006-2016 Robert J. Hijmans, Jacob van Etten, Joe Cheng,
                     Matteo Mattiuzzi, Michael Sumner, Jonathan A. Greenberg,
                     Oscar Perpinan Lamigueiro, Andrew Bevan,
                     Etienne B. Racine, Ashton Shortridge
License: GPL-3+

Files: src/geodesic.*
Copyright: 2008-2016 Charles Karney <charles@karney.com>
License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
Comment: This is a C implementation of the geodesic algorithms described in
 *   C. F. F. Karney,
 *   Algorithms for geodesics,
 *   J. Geodesy <b>87</b>, 43--55 (2013);
 *   https://dx.doi.org/10.1007/s00190-012-0578-z
 *   Addenda: http://geographiclib.sf.net/geod-addenda.html
 *
 * http://geographiclib.sourceforge.net/

Files: debian/*
Copyright: 2016-2017 Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-3+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-3'.

This is the Debian GNU/Linux r-cran-rcppgsl package of RcppGSL.  The
RcppGSL package provides GNU R integration with the GNU GSL and was
written by Romain Francois and Dirk Eddelbuettel.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'RcppGSL' to
'r-cran-rcppgsl' to fit the pattern of CRAN (and non-CRAN) packages
for R.


Files: *
Copyright: 2011 - 2015  Romain Francois and Dirk Eddelbuettel
License: GPL-2

Files: debian/*
Copyright: 2013  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2


On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2.


For reference, the upstream DESCRIPTION file is included below:

  Package: RcppGSL
  Type: Package
  Title: 'Rcpp' Integration for 'GNU GSL' Vectors and Matrices
  Version: 0.3.0
  Date: 2015-08-30
  Author: Dirk Eddelbuettel and Romain Francois
  Maintainer: Dirk Eddelbuettel <edd@debian.org>
  Description: 'Rcpp' integration for 'GNU GSL' vectors and matrices
   The 'GNU Scientific Library' (or 'GSL') is a collection of numerical routines for
   scientific computing. It is particularly useful for C and C++ programs as it
   provides a standard C interface to a wide range of mathematical routines
   such as special functions, permutations, combinations, fast fourier
   transforms, eigensystems, random numbers, quadrature, random distributions,
   quasi-random sequences, Monte Carlo integration, N-tuples, differential
   equations, simulated annealing, numerical differentiation, interpolation,
   series acceleration, Chebyshev approximations, root-finding, discrete
   Hankel transforms physical constants, basis splines and wavelets.  There
   are over 1000 functions in total with an extensive test suite.
   .
     The 'RcppGSL' package provides an easy-to-use interface between 'GSL' data
   structures and R using concepts from 'Rcpp' which is itself a package that
   eases the interfaces between R and C++.
   .
     This package also serves as a prime example of how to build a package
   that uses 'Rcpp' to connect to another third-party library. The 'autoconf'
   script, 'inline' plugin and example package can all be used as a stanza to
   write a similar package against another library.
  License: GPL (>= 2)
  LazyLoad: yes
  LinkingTo: Rcpp
  Imports: Rcpp (>= 0.11.0), stats
  Suggests: RUnit, inline, highlight
  SystemRequirements: GNU GSL
  VignetteBuilder: highlight
  NeedsCompilation: yes
  Packaged: 2015-08-30 14:24:20.393471 UTC; edd
  Repository: CRAN
  Date/Publication: 2015-08-30 16:25:04

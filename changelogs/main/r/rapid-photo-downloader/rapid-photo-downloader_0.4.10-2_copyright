Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Rapid Photo Downloader
Upstream-Contact: Damon Lynch <damonlynch@gmail.com>
Source: https://launchpad.net/rapid

Files: *
Copyright: 2007-2014 Damon Lynch <damonlynch@gmail.com>
           2002-2006 Stephen Kennedy <stevek@gnome.org>
License: GPL-2+

Files: debian/*
Copyright: 2009      Damon Lynch <damonlynch@gmail.com>
           2009-2014 Julien Valroff <julien@debian.org>
           2014      Jörg Frings-Fürst <debian@jff-webhosting.net>
License: GPL-2+

Files: rapid/ValidatedEntry.py
Copyright: 2006      Daniel J. Popowich <dpopowich@astro.umass.edu>
License: Expat

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in the file `/usr/share/common-licenses/GPL-2'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

This is rakarrack, maintained by Tiago Bortoletto Vaz <tiago@debian-ba.org> on
Qua Mar 11 20:55:42 EDT 2009

The original source can always be found at:
	http://sourceforge.net/projects/rakarrack/files/

Copyright (C) 2009: Josep Andreu <holborn@telefonica.net>,
                    José Luis Di Biase <josx@interorganic.com.ar>
                    Hernán Ordiales <h@ordia.com.ar>

Contributors: Daniel Vidal <hdanielvidalchornet@gmail.com>
              Carlos Pino
              Eugenio Rubio
              Alberto Mendez

Rakarrack License:
==================

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this package; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

Other's GPL sources used in Rakarrack:
======================================

* src/Alienwah.* src/AnalogFilter.* src/Chorus.* src/Distorsion.*
  src/DynamicFilter.* src/Echo.* src/EffectLFO.* src/EQ.* src/Filter.*
  src/FilterParams.* src/FormantFilter.* src/Phaser.* src/Reverb.*
  src/SVFilter.*
   Copyright holder: Nasca Octavian Paul

* src/MIDIConverter.* src/Recognizer.* src/Tuner.*
   Copyright holder: Mario Lang <mlang@delysid.org>

Sources licensed under other licenses:
======================================

* src/smbPitchShift.*
   Copyright Holder: Stephan M. Bernsee <smb [AT] dspdimension [DOT] com>
   License: The Wide Open License (WOL)
	    Permission to use, copy, modify, distribute and sell this software
            and its documentation for any purpose is hereby granted without fee,
            provided that the above copyright notice and this license appear in
            all source copies. THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS
            OR IMPLIED WARRANTY OF ANY KIND. See http://www.dspguru.com/wol.htm
            for more information.

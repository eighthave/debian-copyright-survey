Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: renderdoc
Upstream-Contact: Baldur Karlsson <baldurk@baldurk.org>
Source: https://github.com/baldurk/renderdoc
Files-Excluded:
    docs/sphinx_rtd_theme_chm_friendly
    qrenderdoc/3rdparty/pyside
    qrenderdoc/3rdparty/python
    qrenderdoc/3rdparty/qt
    qrenderdoc/3rdparty/swig
    qrenderdoc/Resources/qrenderdoc.rc
    renderdoc/3rdparty/breakpad
    renderdoc/3rdparty/dbghelp
    renderdoc/3rdparty/glslang
    renderdoc/3rdparty/interceptor-lib
    renderdoc/3rdparty/stb
    renderdoc/data/renderdoc.rc
    renderdoc/data/resource.h
Comment: The following files were removed because
    docs/sphinx_rtd_theme_chm_friendly: debian source
    qrenderdoc/3rdparty/pyside: only used on windows
    qrenderdoc/3rdparty/python: debian source
    qrenderdoc/3rdparty/qt: debian source
    qrenderdoc/3rdparty/swig: debian source
    qrenderdoc/Resources/qrenderdoc.rc: only used on windows
    renderdoc/3rdparty/breakpad: only used on windows
    renderdoc/3rdparty/dbghelp: only used on windows
    renderdoc/3rdparty/glslang: debian source
    renderdoc/3rdparty/interceptor-lib: only used on android
    renderdoc/3rdparty/stb: debian source
    renderdoc/data/renderdoc.rc: only used on windows
    renderdoc/data/resource.h: only used on windows

Files: *
Copyright: 2015-2017 Baldur Karlsson
           2014 Crytek
License: MIT

Files: docs/sphinx_exts/sphinx_paramlinks/*
Copyright: Michael Bayer
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2017-2018 Intel Corporation
License: MIT

Files: qrenderdoc/3rdparty/flowlayout/*
Copyright: 2015 The Qt Company Ltd.
License: BSD-3-clause

License: BSD-3-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: qrenderdoc/3rdparty/scintilla/*
Copyright: 1998-2003 by Neil Hodgson <neilh@scintilla.org>
License: scintilla-HPND

License: scintilla-HPND
 All Rights Reserved
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation.
 .
 NEIL HODGSON DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS, IN NO EVENT SHALL NEIL HODGSON BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 OR PERFORMANCE OF THIS SOFTWARE.

Files: qrenderdoc/3rdparty/toolwindowmanager/*
Copyright: 2014 Pavel Strakhov
License: MIT

Files: renderdoc/3rdparty/aosp/*
Copyright: 2005 The Android Open Source Project
License: Apache2

Files: docs/pycharm_helpers/*
Copyright: 2000-2018 JetBrains s.r.o.
License: Apache2

License: Apache2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 On Debian systems, the full text of the Apache 2.0 License can be found
 in the file
 `/usr/share/common-licenses/Apache-2.0'.

Files: renderdoc/3rdparty/catch/*
Copyright: 2017-2018 Baldur Karlsson
License: Boost

License: Boost
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: renderdoc/3rdparty/include-bin/*
Copyright: 2016 Hubert Jarosz
License: zlib

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgement in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: renderdoc/3rdparty/jpeg-compressor/*
Copyright: public domain
License: public-domain

License: public-domain
 jpeg-compressor - C++ class for JPEG compression.
 Public domain, Rich Geldreich <richgel99@gmail.com>

Files: renderdoc/3rdparty/lz4/*
Copyright: 2011-2017, Yann Collet.
License: BSD-2-clause

License: BSD-2-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: renderdoc/3rdparty/miniz/*
Copyright: public domain
License: unlicense

License: unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

Files: renderdoc/3rdparty/plthook/*
Copyright: 2013-2016 Kubo Takehiro <kubo@jiubao.org>
License: BSD-2-clause

Files: renderdoc/3rdparty/pugixml/*
Copyright: 2006-2016 Arseny Kapoulkine
License: MIT

Files: renderdoc/3rdparty/tinyexr/*
Copyright: 2014 - 2015, Syoyo Fujita
License: BSD-3-clause

Files: renderdoc/3rdparty/tinyfiledialogs/*
Copyright: 2014 - 2016 Guillaume Vareille http://ysengrin.com
License: zlib

Files: renderdoc/3rdparty/zstd/*
Copyright: 2016-present, Facebook, Inc. All rights reserved.
License: BSD-3-clause

Files: swig/*
Copyright: 1995-2011 The SWIG Developers
           2005-2006 Arizona Board of Regents (University of Arizona).
           1998-2005 University of Chicago.
           1995-1998 The University of Utah and the Regents of the University of California
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

Files: swig/vs2015/pcre/*
Copyright: 1997-2014 University of Cambridge
License: PCRE

License: PCRE
 -----------------------------------------------------------------------------
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
     * Neither the name of the University of Cambridge nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 -----------------------------------------------------------------------------

Files: swig/Examples/*
Copyright: 1995-2011 The SWIG Developers
           2005-2006 Arizona Board of Regents (University of Arizona).
           1998-2005 University of Chicago.
           1995-1998 The University of Utah and the Regents of the University of California
License: swig_library_and_examples

Files: swig/Lib/*
Copyright: 1995-2011 The SWIG Developers
           2005-2006 Arizona Board of Regents (University of Arizona).
           1998-2005 University of Chicago.
           1995-1998 The University of Utah and the Regents of the University of California
License: swig_library_and_examples

License: swig_library_and_examples
 The SWIG library and examples, under the Lib and Examples top level
 directories, are distributed under the following terms:
 .
 You may copy, modify, distribute, and make derivative works based on
 this software, in source code or object code form, without
 restriction. If you distribute the software to others, you may do
 so according to the terms of your choice. This software is offered as
 is, without warranty of any kind.

Files: swig/CCache/*
Copyright: Andrew Tridgell 2002-2005
           Martin Pool 2003
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: swig/Tools/config/ac_compile_warnings.m4
Copyright: Loic Dachary <loic@senga.org>
           William Fulton <wsf@fultondesigns.co.uk>
License: GPL-3.0-with-autoconf-exception

License: GPL-3.0-with-autoconf-exception
 Version 3.0, 18 August 2009
 .
 Copyright © 2009 Free Software Foundation, Inc. >http://fsf.org/<
 .
 Everyone is permitted to copy and distribute verbatim copies of this
 license document, but changing it is not allowed.
 .
 This Exception is an additional permission under section 7 of the GNU
 General Public License, version 3 ("GPLv3"). It applies to a given
 file that bears a notice placed by the copyright holder of the file
 stating that the file is governed by GPLv3 along with this Exception.
 .
 The purpose of this Exception is to allow distribution of Autoconf's
 typical output under terms of the recipient's choice (including
 proprietary).
 .
 0. Definitions.
 .
 "Covered Code" is the source or object code of a version of Autoconf that is a covered work under this License.
 .
 "Normally Copied Code" for a version of Autoconf means all parts
 of its Covered Code which that version can copy from its code
 (i.e., not from its input file) into its minimally verbose,
 non-debugging and non-tracing output.
 .
 "Ineligible Code" is Covered Code that is not Normally Copied Code.
 .
 1. Grant of Additional Permission.
 .
 You have permission to propagate output of Autoconf, even if such
 propagation would otherwise violate the terms of GPLv3. However,
 if by modifying Autoconf you cause any Ineligible Code of the
 version you received to become Normally Copied Code of your
 modified version, then you void this Exception for the resulting
 covered work. If you convey that resulting covered work, you must
 remove this Exception in accordance with the second paragraph of
 Section 7 of GPLv3.
 .
 2. No Weakening of Autoconf Copyleft.
 .
 The availability of this Exception does not imply any general
 presumption that third-party software is unaffected by the copyleft
 requirements of the license of Autoconf.

Files: swig/Tools/config/ac_define_dir.m4
Copyright: Stepan Kasal <kasal@ucw.cz>
           Andreas Schwab <schwab@suse.de>
           Guido Draheim <guidod@gmx.de>
           Alexandre Oliva
License: FSFAP

Files: swig/Tools/config/ax_cxx_compile_stdcxx_11.m4
Copyright: 2008, Benjamin Kosnik <bkoz@redhat.com>
  2012, William Fulton <wsf@fultondesigns.co.uk>
  2012, Zack Weinberg <zackw@panix.com>
License: FSFAP

Files: swig/Tools/config/ax_path_generic.m4
Copyright: 2009, Francesco Salvestrini <salvestrini@users.sourceforge.net>
License: FSFAP

Files: swig/Tools/config/ax_boost_base.m4
Copyright: 2008, Thomas Porschberg <thomas@randspringer.de>
  2009, Peter Adolphs
License: FSFAP

Files: swig/Tools/config/ax_compare_version.m4
Copyright: 2008, Tim Toolan <toolan@ele.uri.edu>
License: FSFAP

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

This is the Debian GNU/Linux r-cran-rms package of rms, a collection
of GNU R functions for regression modeling strategies, written by
Frank Harrell and supporting the book (Springer, 2001) of the same
title.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
    http://cran.r-project.org/web/packages/rms/index.html
and are also available at all CRAN mirrors.

The package was renamed from its upstream name 'rms' to 'r-cran-rms'
to fit the pattern of CRAN and non-CRAN packages for R. 

Copyright (C) 2001 Frank Harrell

License: GPL-2

On a Debian GNU/Linux system, the GPL (v2) license is included in the
file /usr/share/common-licenses/GPL-2.

For reference, the upstream DESCRIPTION file is included below:

   Package: rms
   Version: 3.3-2
   Date: 2011-11-09
   Title: Regression Modeling Strategies
   Author: Frank E Harrell Jr <f.harrell@vanderbilt.edu>
   Maintainer: Frank E Harrell Jr <f.harrell@vanderbilt.edu>
   Depends: Hmisc (>= 3.7), survival (>= 2.36-3)
   Imports: survival
   Suggests: lattice, quantreg, nlme, rpart, polspline
   Description: Regression modeling, testing, estimation, validation,
   	graphics, prediction, and typesetting by storing enhanced model design
   	attributes in the fit.  rms is a collection of 229 functions that
   	assist with and streamline modeling.  It also contains functions for
   	binary and ordinal logistic regression models and the Buckley-James
   	multiple regression model for right-censored responses, and implements
   	penalized maximum likelihood estimation for logistic and ordinary
   	linear models.  rms works with almost any regression model, but it
   	was especially written to work with binary or ordinal logistic
	regression, Cox regression, accelerated failure time models,
	ordinary linear models,	the Buckley-James model, generalized least
	squares for serially or spatially correlated observations, generalized
	linear models, and quantile regression.
   License: GPL (>= 2)
   URL: http://biostat.mc.vanderbilt.edu/rms
   LazyLoad: yes
   Packaged: 2011-11-10 03:58:34 UTC; harrelfe
   Repository: CRAN
   Date/Publication: 2011-11-10 12:59:11



Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: znc
Source: https://www.znc.in/

Files: *
Copyright: 2004-2018 ZNC
 Prozac <prozac@rottenboy.com>,
 Jim Hull <imaginos@imaginos.net>,
 Uli Schlachter <tobespammed@web.de>.
License: Apache-2.0

Files: modules/autoreply.cpp
Copyright: 2004-2018 ZNC
 2008 Michael "Svedrin" Ziegler <diese-addy@funzt-halt.net>
License: Apache-2.0

Files: modules/controlpanel.cpp
Copyright: 2004-2018 ZNC
 2008 by Stefan Rado
License: Apache-2.0

Files: modules/cyrusauth.cpp
Copyright: 2004-2018 ZNC
 2008 Heiko Hund <heiko@ist.eigentlich.net>
License: Apache-2.0

Files: modules/log.cpp
Copyright: 2004-2018 ZNC
 2006-2007, CNU <bshalm@broadpark.no>
License: Apache-2.0

Files: src/MD5.*
Copyright: 2001-2003  Christophe Devine
License: GPL-2+

Files: src/SHA256.*
Copyright: 2005, 2007 Olivier Gay <olivier.gay@a3.epfl.ch>
License: BSD-C3

Files: third_party/Csocket/*
Copyright: 1999-2012 Jim Hull <csocket@jimloco.com>
License: Sleepycat

Files: webskins/_default_/pub/jquery-*
Copyright: 2005-2015 jQuery Foundation, Inc. and other contributors
License: Expat

Files: webskins/_default_/pub/selectize-*
Copyright: 2013-2015 Brian Reavis & contributors
License: Apache-2.0

Files: debian/*
Copyright: Patrick Matthäi <pmatthaei@debian.org>
License: GPL-2

Files: debian/contrib/znc-backlog/*
Copyright: 2013-2017 Rasmus Eskola
License: Apache-2.0

Files: debian/contrib/znc-push/*
Copyright: 2011-2014 John Reese <john@noswap.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: Apache-2.0
 On Debian systems, the complete text of the Apache-2.0 can be found in the
 file /usr/share/common-licenses/Apache-2.0.

License: GPL-2
 On Debian systems, the complete text of the GPL can be found in the file
 /usr/share/common-licenses/GPL-2

License: GPL-2+
  On Debian systems, the complete text of the GPL can be found in the file
  /usr/share/common-licenses/GPL-2.
  .
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  .
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

License: BSD-C3
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  3. Neither the name of the project nor the names of its contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

License: Sleepycat
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 - Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 - Redistributions in any form must be accompanied by information on how to
   obtain complete source code for the DB software and any accompanying
   software that uses the DB software. The source code must either be included
   in the distribution or be available for no more than the cost of
   distribution plus a nominal fee, and must be freely redistributable under
   reasonable conditions. For an executable file, complete source code means
   the source code for all modules it contains. It does not include source code
   for modules or files that typically accompany the major components of the
   operating system on which the executable file runs.
 .
 THIS SOFTWARE IS PROVIDED BY SLEEPYCAT SOFTWARE ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE
 DISCLAIMED. IN NO EVENT SHALL SLEEPYCAT SOFTWARE BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

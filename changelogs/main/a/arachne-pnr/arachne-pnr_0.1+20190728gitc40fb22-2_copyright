Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: arachne-pnr
Source: https://github.com/YosysHQ/arachne-pnr

Files: *
Copyright: 2015-2018 Cotton Seed <cotton@alum.mit.edu>
License: MIT

Files:  src/arachne-pnr.cc
        src/bitvector.hh
        src/blif.cc
        src/blif.hh
        src/bstream.hh
        src/carry.hh
        src/casting.hh
        src/chipdb.cc
        src/chipdb.hh
        src/configuration.cc
        src/configuration.hh
        src/constant.cc
        src/constant.hh
        src/designstate.cc
        src/designstate.hh
        src/global.cc
        src/global.hh
        src/hashmap.hh
        src/hashset.hh
        src/io.cc
        src/io.hh
        src/line_parser.cc
        src/line_parser.hh
        src/location.cc
        src/location.hh
        src/netlist.cc
        src/netlist.hh
        src/pack.cc
        src/pack.hh
        src/pcf.cc
        src/pcf.hh
        src/place.cc
        src/place.hh
        src/priorityq.hh
        src/route.cc
        src/route.hh
        src/util.cc
        src/util.hh
        src/vector.hh
Copyright: 2015 Cotton Seed <cotton@alum.mit.edu>
License: GPL-2


Files: debian/*
Copyright: 2015-2018 Ruben Undheim <ruben.undheim@gmail.com>
License: GPL-2+


License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

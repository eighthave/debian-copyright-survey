Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: arpwatch
Source: ftp://ftp.ee.lbl.gov/arpwatch.tar.gz
Upstream-Contact: arpwatch@ee.lbl.gov
Comment: Upstream author is Craig Leres, Network Research Group,
 Lawrence Berkeley Laboratory

Files: *
Copyright: 1990, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 2000 The
 Regents of the University of California. All rights reserved.
License: BSD-4-Clause-Shortened
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that: (1) source code distributions
 retain the above copyright notice and this paragraph in its entirety, (2)
 distributions including binary code include the above copyright notice and
 this paragraph in its entirety in the documentation or other materials
 provided with the distribution, and (3) all advertising materials mentioning
 features or use of this software display the following acknowledgement:
 ``This product includes software developed by the University of California,
 Lawrence Berkeley Laboratory and its contributors.'' Neither the name of
 the University nor the names of its contributors may be used to endorse
 or promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Files: debian/*
Copyright: 2009 Craig Sanders <cas@taz.net.au>
           2014 Florian Schlichting <fsfs@debian.org>
           2015 Axel Beckert <abe@debian.org>
           2017 Lukas Schwaighofer <lukas@schwaighofer.name>
License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3 can be found in "/usr/share/common-licenses/GPL-3".
Comment: Most of the debian/* control files were re-used from the
 earlier package of arpwatch 2.1a13 by KELEMEN Péter <fuji@debian.org>

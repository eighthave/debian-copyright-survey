Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ap51-flash
Upstream-Contact: Marek Lindner <mareklindner@neomailbox.ch>
Source: https://github.com/ap51-flash/ap51-flash/

Files: *
Copyright: 2009-2019, Marek Lindner <mareklindner@neomailbox.ch>
License: GPL-3+

Files: .github/pull_request_template
Copyright: 2017-2019, Sven Eckelmann <sven@narfation.org>
  2009-2019, Marek Lindner <mareklindner@neomailbox.ch>
License: CC0-1.0

Files: docs/flash-station/*
Copyright: 2013, Linus Lüssing <linus.luessing@c0d3.blue>
License: GPL-3+

Files: docs/supported-devices/index.rst
Copyright: 2013, Saverio Proto <zioproto@gmail.com>
  2013-2019, Marek Lindner <mareklindner@neomailbox.ch>
  2018, Antonio Quartulli <a@unstable.cc>
  2017-2019, Sven Eckelmann <sven@narfation.org>
License: GPL-3+

Files: docs/conf.py
  docs/index.rst
  docs/Makefile
  man/ap51-flash.8
  fwcfg.*
Copyright: 2017-2019, Sven Eckelmann <sven@narfation.org>
License: GPL-3+

Files: router_netconsole.*
Copyright: Antonio Quartulli <a@unstable.cc>
License: GPL-3+

Files: embed_image.mk
  router_images.c
  router_tftp_client.c
Copyright: 2009-2019, Marek Lindner <mareklindner@neomailbox.ch>
  2014-2019, Sven Eckelmann <sven@narfation.org>
License: GPL-3+

Files: CHANGELOG.rst
  README.rst
  .gitattributes
  .gitignore
  .travis.yml
Copyright: 2017-2019, Sven Eckelmann <sven@narfation.org>
License: CC0-1.0

Files: debian/*
Copyright: 2019, Sven Eckelmann <sven@narfation.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GPL v3 can be found in
 `/usr/share/common-licenses/GPL-3'.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in ‘/usr/share/common-licenses/CC0-1.0’.

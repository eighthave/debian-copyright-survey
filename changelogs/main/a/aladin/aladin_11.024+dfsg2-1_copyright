Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Aladin
Upstream-Contact:
 Thomas Boch <thomas.boch@astro.unistra.fr>
 Francois Bonnarel <bonnarel@astro.u-strasbg.fr>
 Pierre Fernique <fernique@astro.u-strasbg.fr>
 Anais Oberto <oberto@astro.u-strasbg.fr>
Contributors:
 Francois Ochsenbein <francois@astro.u-strasbg.fr>
 Andre Schaaff <andre.schaaff@astro.unistra.fr>
Source: https://aladin.u-strasbg.fr/java/download/AladinSrc.jar

Files: * debian/aladin.svg
Copyright: 1999-2012 Universite de Strasbourg /
 Centre de Donnees astronomiques de Strasbourg (CDS/CNRS)
 2001-2016 Pierre Fernique
 1993 Association of Universities for Research in Astronomy
 1994-1995 European Southern Observatory (ESO)
License: GPL-3

Files: debian/*
Copyright: 2012 Florian Rothmaier <gavo@ari.uni-heidelberg.de>
 2016 Paul Sladen <debian@paul.sladen.org>
 2016-2020 Ole Streicher <olebole@debian.org>
License: GPL-3

License: GPL-3
 Aladin is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 3 of the License.
 .
 Aladin is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

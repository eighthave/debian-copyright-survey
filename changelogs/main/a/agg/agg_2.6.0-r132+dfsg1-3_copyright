Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: agg
Upstream-Contact: Jim Barry <jim@chez-jim.net>
Comment: This source tarball is the agg-2.4 directory of the upstream repository.
  Documentation files and the AGG web site files have been excluded.
Source: http://www.antigrain.com

Files: *
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2012-2016 Jim Barry <jim@chez-jim.net>
  2009-2017 John Horigan <john@glyphic.com>
  2008-2009 Klaas Holwerda <ngi@klaasholwerda.nl>
License: BSD-3-clause

Files: examples/svg_viewer/agg_svg_parser.cpp
  examples/svg_viewer/agg_svg_path_renderer.*
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2008 Rene Rebe <rene@exactcode.de>
License: BSD-3-clause

Files: include/platform/mac/agg_mac_pmap.h
  src/platform/mac/agg_platform_support.cpp
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2004 2003 Hansruedi Baer <baer@karto.baug.eth.ch>
License: BSD-3-clause

Files: src/platform/sdl/agg_platform_support.cpp
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2004 Mauricio Piacentini
License: BSD-3-clause

Files: src/platform/BeOS/agg_platform_support.cpp
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2004 Stephan Assmus <superstippi@gmx.de>
License: BSD-3-clause

Files: src/platform/AmigaOS/agg_platform_support.cpp
Copyright:
  2002-2006 Maxim Shemanarev <mcseem@antigrain.com>
  2004 Steven Solie
License: BSD-3-clause

Files: debian/*
Copyright:
  2012 Andrea Veri <and@debian.org>
  2017 John Horigan <john@glyphic.com>
License: GPL-2+

License: BSD-3-clause
 Anti-Grain Geometry - Version 2.4 
 Copyright (C) 2002-2005 Maxim Shemanarev (McSeem)
 .
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions 
 are met:
  1. Redistributions of source code must retain the above copyright 
     notice, this list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in 
     the documentation and/or other materials provided with the 
     distribution. 
  3. The name of the author may not be used to endorse or promote 
     products derived from this software without specific prior 
     written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Aytana Indicator Application
Upstream-Contact: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
Source: https://github.com/AyatanaIndicators/ayatana-indicator-appliction

Files: AUTHORS
 ChangeLog
 Makefile.am
 Makefile.am.coverage
 Makefile.am.marshal
 NEWS
 README
 autogen.sh
 data/Makefile.am
 data/ayatana-indicator-application.conf.in
 data/ayatana-indicator-application.desktop.in
 data/ayatana-indicator-application.override
 data/ayatana-indicator-application.service.in
 data/ordering-override.keyfile
 data/upstart/Makefile.am
 data/upstart/ayatana-indicator-application.desktop.in
 m4/gcov.m4
 src/Makefile.am
 src/ayatana-appindicator3-0.1.pc.in
 src/ayatana-notification-watcher.xml
 tests/Makefile.am
 tests/run-xvfb.sh
Copyright: 2009-2010, Canonical Ltd.
License: GPL-3
Comment:
 Assuming copyright holdership and license from rest of the
 code files.

Files: src/application-service-appstore.c
 src/application-service-appstore.h
 src/application-service-watcher.c
 src/application-service-watcher.h
 src/application-service.c
 src/ayatana-application-service-marshal.list
 src/dbus-shared.h
 src/generate-id.c
 src/generate-id.h
 src/indicator-application.c
Copyright: 2009, Canonical Ltd.
  2010, Canonical Ltd.
License: GPL-3

Files: src/app-indicator-enum-types.c.in
 src/app-indicator-enum-types.h.in
Copyright: 2009, Canonical Ltd.
License: LGPL-2.1 or LGPL-3

Files: acinclude.m4
Copyright: 1999-2004, Free Software Foundation, Inc.
License: GPL-2+

Files: src/ayatana-application-service.xml
Copyright: 2009, Canonical Ltd.
  2015, Arctica Project
License: GPL-3

Files: configure.ac
Copyright: 2009-2010, Canonical Ltd.
  2015, Arctica Project
License: GPL-3

Files: debian/*
Copyright: 2009-2012, Canonical Ltd.
  2015-2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: debian/patches/*
Copyright: 2015-2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3 or LGPL-2.1 or LGPL-3

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 See /usr/share/common-licenses/GPL-2 for the full license text.

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; version 2.1
 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 See /usr/share/common-licenses/LGPL-2.1 for the full license text.

License: LGPL-3
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; version 3
 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 See /usr/share/common-licenses/LGPL-3 for the full license text.

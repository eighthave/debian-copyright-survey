Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://apertium.org/
Comment: The official stance of the Apertium project is that when one of our
 repositories don't clarify the license beyond putting the GPL COPYING file in
 the repo, then it should be interpreted as the "or any later" version of it to
 maximize reusability.
 .
 The data is often mixed with GPLv3 data at compile time, often by the same
 authors, but where the GPLv2 stuff just predates wider GPLv3 adoption.
 .
 For more details, see email:
 https://www.mail-archive.com/apertium-stuff@lists.sourceforge.net/msg06931.html
 by Francis Tyers for reference Apertium Project Management Committee.

Files: *
Copyright: 2011-2019, Hèctor Alòs i Font <hectoralos@gmail.com>
           2009-2018, Francis M. Tyers <ftyers@prompsit.com>
                2018, Marc Riera Irigoyen <marc.riera.irigoyen@gmail.com>
                2018, Sushain Cherivirala <sushain@skc.name>
           2009-2014, Jim O'Regan <joregan@gmail.com>
                2013, Trond Trosterud <trond.trosterud@uit.no>
                2012, Anthony J. Bentley <anthony@anjbe.name>
           2010-2011, Carme Armentano-Oller <carme.armentano@gmail.com>
                2010, Mireia Ginestí Rosell <mireia.ginesti@gmail.com>
           2009-2010, Antonio Toral <atoral@computing.dcu.ie>
License: GPL-2+

Files: apertium-cat-2.8.0/*
Copyright: 2015-2018, Xavi Ivars <xavi.ivars@gmail.com>
           2017-2018, Hèctor Alòs i Font <hectoralos@gmail.com>
           2017-2018, Marc Riera Irigoyen <marc.riera.irigoyen@gmail.com>
           2017-2018, Donís Seguí <donissegui@users.sourceforge.net>
           2017-2018, Jaume Ortolà i Font <jaumeortola@gmail.com>
                2017, Gema Ramírez Sánchez <gramirez@prompsit.com>
                2017, Sergio Ortiz Rojas <sergio.ortiz@gmail.com>
           2016-2017, Juan Pablo Martínez Cortés <jpmart@unizar.es>
           2015-2017, Francis M. Tyers <ftyers@prompsit.com>
                2016, Kevin Brubeck Unhammer <unhammer@fsfe.org>
                2016, Frankie Robertson <frankie@robertson.name>
License: GPL-2+

Files: apertium-ita-0.10.5/*
Copyright: 2016-2019, Hèctor Alòs i Font <hectoralos@gmail.com>
           2013-2018, Francis M. Tyers <ftyers@prompsit.com>
           2018-2019, Lorenza Russo <lorenzarusso@hotmail.com>
           2016-2017, Gema Ramírez Sánchez <gramirez@prompsit.com>
           2016-2017, Gianfranco Fronteddu <gfro3d@gmail.com>
           2014-2017, Gianluca Grossi <gianluca.grossi@me.com>
                2016, Xavi Ivars <xavi.ivars@gmail.com>
                2016, Marina Loffredo <loffredomarina@gmail.com>
                2016, Frankie Robertson <frankie@robertson.name>
License: GPL-2+

Files: debian/*
Copyright: 2014, Apertium Project Management Committee <apertium-pmc@dlsi.ua.es>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

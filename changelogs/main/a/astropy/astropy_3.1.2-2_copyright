Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: astropy
Upstream-Author: The Astropy Developers
Source: https://www.astropy.org/

Files: * astropy/io/fits/*
 astropy/extern/configobj/*
 astropy/extern/js/jquery.dataTables.js debian/sphinx_automodapi/*
Copyright: 2011-2017 Astropy Developers,
 2004-2005 Association of Universities for Research in Astronomy,
 2006 Enthought, Inc.,
 2010-2011 Smithsonian Astrophysical Observatory,
 2003-2010 Michael Foord, Mark Andrews, Nicola Larosa,
 1995-2010 Jean-loup Gailly and Mark Adler,
 2001-2011 David M. Beazley (Dabeaz LLC),
 2008-2013 Allan Jardine,
 2015 Curtis McCully,
 2012-2018 Ole Streicher <olebole@debian.org>
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided
 with the distribution.
 .
 3. Neither the name of AURA and its representatives nor the names
 of the Smithsonian Astrophysical Observatory, Enthought Inc., Michael Foord,
 the name of Voidspace, the names of its contributors, David Beazley, or
 Dabeaz LLC, Allan Jardine nor SpryMedia, may not be used to endorse or
 promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY ITS AUTHORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL AURA BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

Files: cextern/expat/*
Copyright: 2001-2007 Expat maintainers,
 1998-2003 Thai Open Source Software Center Ltd and Clark Cooper
 2014 jQuery Foundation and other contributors
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, includingwithout limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: cextern/wcslib/*
Copyright: Copyright (C) 1995-2012, Mark Calabretta
License: LGPL-3
 WCSLIB is free software: you can redistribute it and/or modify it under the
 terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 WCSLIB is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
 .
 Correspondence concerning WCSLIB may be directed to:
   Internet email: mcalabre@atnf.csiro.au
   Postal address: Dr. Mark Calabretta
                   Australia Telescope National Facility, CSIRO
                   PO Box 76
                   Epping NSW 1710
                   AUSTRALIA
Comment: Note that these files are not used in the package build process, so
 the copyright information is included here only as a reference.

Files: cextern/cfitsio/*
Copyright: (Unpublished--all rights reserved under the copyright laws of
 the United States), U.S. Government as represented by the Administrator
 of the National Aeronautics and Space Administration.  No copyright is
 claimed in the United States under Title 17, U.S. Code.
 .
 Copyright files present in the sources, but not used:
 .
  * 1995-2010 Mark Adler
  * 1995-2010 Jean-loup Gailly
  * 1990-2003 Burkhard Burow
License: MIT
 Permission to freely use, copy, modify, and distribute this software
 and its documentation without fee is hereby granted, provided that this
 copyright notice and disclaimer of warranty appears in all copies.
 .
 DISCLAIMER:
 .
 THE SOFTWARE IS PROVIDED 'AS IS' WITHOUT ANY WARRANTY OF ANY KIND,
 EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED TO,
 ANY WARRANTY THAT THE SOFTWARE WILL CONFORM TO SPECIFICATIONS, ANY
 IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE, AND FREEDOM FROM INFRINGEMENT, AND ANY WARRANTY THAT THE
 DOCUMENTATION WILL CONFORM TO THE SOFTWARE, OR ANY WARRANTY THAT THE
 SOFTWARE WILL BE ERROR FREE.  IN NO EVENT SHALL NASA BE LIABLE FOR ANY
 DAMAGES, INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES, ARISING OUT OF, RESULTING FROM, OR IN ANY WAY
 CONNECTED WITH THIS SOFTWARE, WHETHER OR NOT BASED UPON WARRANTY,
 CONTRACT, TORT , OR OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY
 PERSONS OR PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED
 FROM, OR AROSE OUT OF THE RESULTS OF, OR USE OF, THE SOFTWARE OR
 SERVICES PROVIDED HEREUNDER.
Comment: Note that these files are not used in the package build process, so
 the copyright information is included here only as a reference.

Files: cextern/erfa/*
Copyright: 2013, NumFOCUS Foundation.
License: BSD-3-Clause-ERFA
 This library is derived, with permission, from the International Astronomical
 Union's "Standards of Fundamental Astronomy" library, available from
 http://www.iausofa.org.
 .
 The ERFA version is intended to retain identical functionality to the SOFA
 library, but made distinct through different function and file names, as set
 out in the SOFA license conditions. The SOFA original has a role as a
 reference standard for the IAU and IERS, and consequently redistribution is
 permitted only in its unaltered state. The ERFA version is not subject to
 this restriction and therefore can be included in distributions which do not
 support the concept of "read only" software.
 .
 Although the intent is to replicate the SOFA API (other than replacement of
 prefix names) and results (with the exception of bugs; any that are
 discovered will be fixed), SOFA is not responsible for any errors found in
 this version of the library.
 .
 If you wish to acknowledge the SOFA heritage, please acknowledge that you are
 using a library derived from SOFA, rather than SOFA itself.
 .
 TERMS AND CONDITIONS
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1 Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 2 Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 3 Neither the name of the Standards Of Fundamental Astronomy Board, the
   International Astronomical Union nor the names of its contributors may be
   used to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
Comment: ERFA has received explicit permission from the IAU SOFA Board to
 re-license and re-copyright ERFA from SOFA. The email providing permission is
 shown here:
 .
 > The IAU Standards Of Fundamental Astronomy Board approves the
 > relicensing of a changed SOFA library by the NumFOCUS Foundation to use
 > a "Three Clause BSD" license.  The changed, relicensed version shall
 > differ from the SOFA version in that all function names shall change to
 > use "era" as a prefix in place of "iau", and that the SOFA Board shall
 > be removed as a copyright holder in the relicensed version.
 .
 > Catherine
 > Chair, IAU SOFA Board
 > ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 > HM Nautical Almanac Office
 > United Kingdom Hydrographic Office
 > Admiralty Way
 > Taunton TA1 2DN
 > Catherine.Hohenkerk@UKHO.gov.uk
 > ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 .
 Note that these files are not used in the package build process, so
 the copyright information is included here only as a reference.

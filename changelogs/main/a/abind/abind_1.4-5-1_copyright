This is the Debian GNU/Linux r-cran-abind package of abind, an array-combining
utility function for GNU R, which was written by Tony Plate and Richard Heiberger.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'abind' to 'r-cran-abind'
to fit the pattern of CRAN (and non-CRAN) packages for R.

Copyright 2004 Tony Plate and Richard Heiberger

License: LGPL

On a Debian GNU/Linux system, the LGPL license is included in the file
/usr/share/common-licenses/LGPL.

For reference, the upstream DESCRIPTION [with lines broken to 80 cols] file
is included below:

   Package: abind
   Version: 1.0-1
   Date: 2003-06-10
   Title: Combine multi-dimensional arrays
   Author: Tony Plate <tplate@attglobal.net> and Richard Heiberger
   Maintainer: Tony Plate <tplate@attglobal.net>
   Description: Combine multi-dimensional arrays.  This is a
    generalization of cbind and rbind.  Takes a sequence of
    vectors, matrices, or arrays and produces a single array of
    the same or higher dimension.
   Depends: R (>= 1.5.0)
   License: LGPL Version 2 or later.



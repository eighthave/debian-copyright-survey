Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ASIS
Source: https://libre.adacore.com/libre/download/
Comment:
 ASIS-for-GNAT was originally developed  by the ASIS-for-GNAT team at the
 Software  Engineering  Laboratory  of  the Swiss  Federal  Institute  of
 Technology (LGL-EPFL) in Lausanne,  Switzerland, in cooperation with the
 Scientific  Research  Computer  Center of  Moscow State University (SRCC
 MSU), Russia,  with funding partially provided  by grants from the Swiss
 National  Science  Foundation  and  the  Swiss  Academy  of  Engineering
 Sciences.     ASIS-for-GNAT    is     now    maintained    by    AdaCore
 (http://www.adacore.com).
 .
 Although
 most licensing boilerplates have not been updated yet from GPL-2+,
 Avatox.pdf and README.avatox mention the GNAT exception to the GPL,
 the license of this whole package is GPL-3+, as for everything
 available on libre.adacore.com under the "GPL 2014 edition".
 The GPL is stated for example at
 http://lists.adacore.com/pipermail/gtkada/2009-June/003789.html,
 and the version makes no doubt because AdaCore has replaced many
 years ago a file named COPYING containing the GPL-2 with a file named
 COPYING3 containing the GPL-3.
 Please contact them at libre@adacore.com for any clarification.
 .
 However, the header at https://libre.adacore.com/libre/download/
 seems to imply that the documentation is now licensed under the GNU
 Free Documentation License with invariant sections, and cannot be
 distributed by Debian.
Files-Excluded:
 documentation
 tools/gnatcheck/gnatcheck_rm.texi

Files: *
Copyright: 1995-2013, Free Software Foundation, Inc.
           1995-2013, Adacore, Inc.
License: GPL-3+
Comment:
 In contrast to previous versions of ASIS, this version is licensed under
 the pure GPL.  This means  that if you produce binary executables linked
 with ASIS,  and if  you distribute these  binary executables  to anyone,
 then you *must* do  so under the terms of the GPL.   This means that you
 must give  them (your customers) the  sources of your  program, and that
 they  (your customers)  have a  right  to modify  and redistribute  your
 program under the terms of the GPL.
 .
 If you do not wish to license  your software under the terms of the GPL,
 then you have three choices:
 .
 - do not distribute your software at all (i.e. only  use  it  internally
   for your own purposes)
 .
 - distribute your software in source form only, under licensing terms of
   your choosing
 .
 - contact AdaCore (sales@adacore.com) and acquire a  GNAT  Pro  license,
   which will allow you to distribute binaries  linked  with  ASIS  under
   licensing terms of your choosing.

Files: debian/*
Copyright: 2000-2014 Ludovic Brenta <lbrenta@debian.org>
           2013-2014 Nicolas Boulenguez <nicolas@debian.org>
License: GPL-3+

License: GPL-3+
 ASIS-for-GNAT is free software; you can redistribute it and/or modify it
 under terms of  the GNU General Public License as  published by the Free
 Software Foundation;  either version  3, or (at  your option)  any later
 version.  ASIS-for-GNAT is  distributed  in  the hope  that  it will  be
 useful, but WITHOUT  ANY WARRANTY; without even the  implied warranty of
 MERCHANTABILITY  or  FITNESS FOR  A  PARTICULAR  PURPOSE.   See the  GNU
 General Public License for more details. You should have received a copy
 of the  GNU General Public  License distributed with  ASIS-for-GNAT; see
 file COPYING. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian GNU/Linux systems, the  full terms of the GNU General Public
 License are in /usr/share/common-licenses/GPL-3.

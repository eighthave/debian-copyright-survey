Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: apophenia
Upstream-Contact: Ben Klemens <fluffmail@f-m.fm>
Source: http://apophenia.info/
X-Source-Downloaded-From: https://github.com/b-k/apophenia/releases
X-Upstream-Vcs-Git: https://github.com/b-k/apophenia.git
X-Upstream-Vcs-Browser: https://github.com/b-k/apophenia/
X-Upstream-Bugs: https://github.com/b-k/apophenia/issues
Comment:
 The upstream source tarball is repacked to drop off the regenerated
 material, mainly autotools related, to gain substantial weight.
Files-Excluded:
 m4/lt~obsolete.m4
 m4/ltsugar.m4
 m4/ltoptions.m4
 m4/ltversion.m4
 m4/libtool.m4
 build-aux/compile
 build-aux/depcomp
 build-aux/install-sh
 build-aux/ltmain.sh
 build-aux/test-driver
 build-aux/missing
 build-aux/config.sub
 build-aux/config.guess
 transform/Makefile.in
 model/Makefile.in
 Makefile.in
 cmd/Makefile.in
 tests/Makefile.in
 docs/Makefile.in
 eg/Makefile.in
 acinclude.m4
 aclocal.m4
 config.h.in
 configure
 apop.h

Files: *
Copyright: 2005-2019 Ben Klemens <fluffmail@f-m.fm>
License: GPL-2

Files: apop_fexact.c
Copyright: 2006-2009 Ben Klemens <fluffmail@f-m.fm>
License: GPL-2
Comment:
 Implementation of Algorithm TOMS643 (1993) from ACM imported and adapted
 by hand from the R Project (<https://www.r-project.org/>) C source file
 <R-base>/src/library/stats/src/fexact.c which is distributed under the
 GPL-2+ License by The R Core Team; see the source file for further details.

Files: apop_hist.c
Copyright: 2006, 2007, 2010, 2013 Ben Klemens <fluffmail@f-m.fm>
License: GPL-2
Comment:
 Contains the function psmirnov2x imported and adapted by hand from the R Project
 (<https://www.r-project.org/>) C source file <R-base>/src/library/stats/src/ks.c
 which is distributed under the GPL-2+ License by The R Core Team; see the source
 file for further details.

Files: apop_stats.c
Copyright: 2006, 2007, 2013 Ben Klemens <fluffmail@f-m.fm>
License: GPL-2
Comment:
 Implements an adpated C version of the R function nearPD as implemented and
 copyrighted (2007) by Jens Oehlschlage under the GPL License in the R source
 file <R-matrix>/R/nearPD.R ; the <R-matrix> material may be available at the
 R Project site (<https://www.r-project.org/>).

Files: model/apop_multinomial.c
Copyright: 2006, 2007, 2010, 2011 Ben Klemens <fluffmail@f-m.fm>
License: GPL-2
Comment:
 Contains some mathematical oriented code inspired by the GNU Scientific
 Library (GSL) (<https://www.gnu.org/software/gsl/>) which is distributed
 under the GPL license.

Files: asprintf.c
Copyright: 1999, 2002 Free Software Foundation, Inc.
License: GPL-2+

Files: debian/*
Copyright:
 2014-2019 Jerome Benoit <calculus@rezozer.net>
License: GPL-2+

License: GPL-2
 This is free software; you can redistribute it and/or modify
 it under the terms of version 2 of the GNU General Public
 License version 2 as published by the Free Software Foundation.
 .
 This is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

Format-Specification: http://dep.debian.net/deps/dep5/
Name: Antelope
Maintainer: Dale Anson <danson@tigris.org>
Source: <http://antelope.tigris.org/servlets/ProjectDocumentList>

Files: *
Copyright: 2002-2009 Dale Anson.  All rights reserved
Licence: Apache-1.1

Files: src/ise/library/ascii/MessageBox.java,
 src/ise/antelope/tasks/password/*,
 src/ise/antelope/tasks/PasswordHandler.java
Copyright: 2004, Dale Anson
Licence: BSD

Files: src/ise/antelope/launcher/Locator.java,
 src/ise/antelope/launcher/LaunchException.java,
 src/ise/antelope/launcher/Launcher.java,
 src/ise/antelope/common/AntelopeProjectHelper2.java,
 src/ise/antelope/tasks/Unset.java,
 src/ise/antelope/tasks/condition/Or.java,
 src/ise/antelope/tasks/condition/And.java,
 src/ise/antelope/tasks/condition/Not.java,
 src/ise/antelope/tasks/Variable.java,
 src/ise/antelope/tasks/Limit.java,
 src/ise/antelope/tasks/Or.java,
 src/ise/antelope/tasks/And.java,
 src/ise/antelope/tasks/Repeat.java
Copyright: 2001-2004 The Apache Software Foundation
Licence: Apache-2.0

Files: src/ise/library/KappaLayout.java,
 src/ise/library/LambdaLayout.java,
 src/ise/antelope/launcher/KappaLayout.java,
 src/ise/antelope/launcher/LambdaLayout.java
Copyright: 2000, Dale Anson
Licence: LGPL-2.1+

Files: src/ise/antelope/app/jedit/*
Copyright: 1998, 1999 Slava Pestov
Copyright: 1999, Mike Dillon
Copyright: 2002, Dale Anson
Copyright: 2001, Tom Bradford
Licence: other
  You may use and modify this package for any purpose. Redistribution is
  permitted, in both source and binary form, provided that this notice
  remains intact in all source distributions of this package.

Files: src/ise/antelope/tasks/Base64.java,
 src/ise/antelope/tasks/util/Base64.java
Copyright: 2000, Robert Harder <rob@iharder.net>
Licence: PD
  I am placing this code in the Public Domain. Do with it as you will.
  This software comes with no guarantees or warranties but with
  plenty of well-wishing instead!

Files: debian/*
Copyright: 2010, Damien Raude-Morvan <drazzib@debian.org>
Licence: BSD


Licence: BSD
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  .
     1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
     2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
     3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Licence: Apache-2.0
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  .
     http://www.apache.org/licenses/LICENSE-2.0
  .
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  .
  The full text of the Apache-2.0 license can be found in
  `/usr/share/common-licenses/Apache-2.0' on Debian systems.

Licence: Apache-1.1
  [Debian: from src/docs/license.txt]
  .
  This license pertains to all files in the Antelope distribution except the images,
  which are from Sun's Java Look and Feel Design Guidelines and are licensed according
  to those guidelines.
  .
  [Debian: those images have been removed from upstream tarball]
  .
  Based on the Apache Software License, Version 1.1
  .
  Copyright (c) 2002 Dale Anson.  All rights reserved.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  .
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
  .
  3. The end-user documentation included with the redistribution, if
     any, must include the following acknowlegement:
        "This product includes software developed by Dale Anson,
         danson@users.sourceforge.net."
     Alternately, this acknowlegement may appear in the software itself,
     if and wherever such third-party acknowlegements normally appear.
  .
  4. The name "Antelope" must not be used to endorse or promote products derived
     from this software without prior written permission. For written
     permission, please contact danson@users.sourceforge.net.
  .
  5. Products derived from this software may not be called "Antelope"
     nor may "Antelope" appear in their names without prior written
     permission of Dale Anson.
  .
  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED.  IN NO EVENT SHALL DALE ANSON OR ANY PROJECT
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

Licence: LGPL-2.1+
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  .
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU<
  Lesser General Public License for more details.
  .
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
  .
  The full text of the LGPL-2.1 license can be found in
  `/usr/share/common-licenses/LGPL-2.1' on Debian systems.

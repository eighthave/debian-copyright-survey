This package was debianized by Camm Maguire <camm@debian.org> on
Sat, 26 Oct 2002 11:58:58 -0400.

It was downloaded from ftp://ftp.cs.utexas.edu:/pub/moore/acl2/v2-8/

Upstream Authors: 

  Matt Kaufmann,kaufmann@cs.utexas.edu			                   (main program)
  J Strother Moore,moore@cs.utexas.edu                                     (main program)

  University of Texas at Austin                                            (books, partial)
  Computational Logic, Inc.,mksmith@acm.org,msmith17@austin.rr.com         (books, partial)
  John R. Cowles, University of Wyoming                                    (books, partial)
  Bishop Brock and J Strother Moore                                        (books, partial)
  Panagiotis Manolios and J Strother Moore                                 (books, partial)
  Georgia Institute of Technology                                          (books, partial)
  Jared Davis,jared@cs.utexas.edu                                          (books, partial)
  Panagiotis Manolios,manolios@cc.gatech.edu                               (books, partial)
  Daron Vroon,vroon@cc.gatech.edu                                          (books, partial)
  Matt Kaufmann,kaufmann@cs.utexas.edu                                     (books, partial)

Copyright:

All files in acl2_6.0.orig.tar.gz, (e.g. all sub-directories outside of "books/"):

Copyright (c) 2012, Regents of the University of Texas
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

o Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

o Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

o Neither the name of the University of Texas, Austin nor the names of
  its contributors may be used to endorse or promote products derived
  from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


All files in acl2_6.0.orig-books.tar.gz, (e.g. the contents of the "books/" sub-directory):


		    GNU GENERAL PUBLIC LICENSE
		       Version 2, June 1991

     
 Copyright (C) 1989, 1991 Free Software Foundation, Inc., 
     51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

			    Preamble

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation's software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.)  You can apply it to
your programs, too.

The ACL2 XDOC FANCY VIEWER files are licensed as follows:


                         ACL2 XDOC FANCY VIEWER
                           License Information


Scope of this LICENSE file:

   An XDOC manual has both Viewer Files for displaying topics and Content Files
   that store the actual topic data.  This LICENSE file describes the copyright
   and licensing of the Viewer Files, only.

   The topics in Content Files, such as xindex.js, xdata.js, and (in some
   cases) xdata.db, are usually derived from ACL2 books and/or other source
   code files.  These topics are typically subject to their own copyright and
   licensing terms, which are beyond the scope of this LICENSE.


Copyright for main source files:

   XDOC Documentation System for ACL2
   Copyright (C) 2009-2013 Centaur Technology

   Contact:
     Centaur Technology Formal Verification Group
     7600-C N. Capital of Texas Highway, Suite 300, Austin, TX 78731, USA.
     http://www.centtech.com/

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 2 of the License, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program; if not, write to the Free Software Foundation, Inc., 51
   Franklin Street, Suite 500, Boston, MA 02110-1335, USA.

   Original author: Jared Davis <jared@centtech.com>

Copyrights for Supporting Libraries: (within the "lib" directory)

   Hogan:
     Copyright 2011 Twitter, Inc.
     Released under Apache License
     http://twitter.github.io/hogan.js/

   jQuery:
     Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors
     Released under the MIT license.
     http://jquery.com/

   jquery.base64.js:
     Copyright 2012 Yannick Albert
     Released under MIT license.
     https://github.com/yckart/jquery.base64.js

   PowerTip:
     Copyright 2013 Steven Benner
     Released under MIT license.
     http://stevenbenner.github.com/jquery-powertip/

   LazyLoad:
     Copyright 2011 Ryan Grove
     Released under MIT license.
     https://github.com/rgrove/lazyload

   Typeahead:
     Copyright 2013 Twitter, Inc. and other contributors
     Released under MIT license.
     https://github.com/twitter/typeahead


Copyrights for other supporting files:

   Original XDOC Images
     Copyright 2009-2013 Centaur Technology
     License: GNU General Public License

     - leaf.png
     - plus.png
     - minus.png
     - expand_subtopics.png
     - collapse_subtopics.png
     - favicon.png
     - xdoc-home.png
     - xdoc-logo.png

   Oxygen Icon Set Images
     http://www.oxygen-icons.org/
     Released under GNU General Public License
     Accessed 2012-08-13 from:
       http://kde-look.org/content/show.php/Oxygen+Icons?content=74184

     - printer.png
     - download.png
     - view_flat.png   (adapted)
     - view_tree.png   (adapted)

   External Link Image:
     Released under GNU General Public License
     Accessed 2013-07-26 from
       http://commons.wikimedia.org/wiki/File:Icon_External_Link.png

     - Icon_External_Link.png


   ACL2 Tour Images (.gif files within the images/ directory)
     Copyright 2012 Regents of the University of Texas
     Released under a BSD-style license (distributed with ACL2)



On Debian GNU/Linux systems, the complete text of the Apache license can
be found in /usr/share/common-licenses/Apache-2.0.

On Debian GNU/Linux systems, the complete text of the GNU General
Public License Version 2 can be found in
/usr/share/common-licenses/GPL-2.


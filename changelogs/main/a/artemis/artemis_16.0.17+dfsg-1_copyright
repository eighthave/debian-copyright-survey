Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: artemis
Upstream-Contact: <artemis@sanger.ac.uk>
Source: https://github.com/sanger-pathogens/Artemis
Files-Excluded:
	lib/*
	test/lib/*

Files: *
Copyright: 1998-2013 Genome Research Limited
License: GPL-2+

Files:
	org/gmod/schema/dao/*
	uk/ac/sanger/artemis/components/SwingWorker.java
	uk/ac/sanger/artemis/components/TextFieldSink.java
	uk/ac/sanger/artemis/components/filetree/FileManager.java
	uk/ac/sanger/artemis/components/filetree/FileNode.java
	uk/ac/sanger/artemis/components/filetree/LocalAndRemoteFileManager.java
	uk/ac/sanger/artemis/components/filetree/DatabaseEntryFilterPanel.java
Copyright: 2006 Genome Research Limited
License: LGPL-2+

Files: uk/ac/sanger/artemis/components/variant/TabixReader.java
Copyright: 2010 Broad Institute
License: MIT

Files:
	uk/ac/sanger/artemis/components/filetree/FileList.java
	uk/ac/sanger/artemis/components/filetree/RemoteFileNode.java
	uk/ac/sanger/artemis/components/filetree/TransferableFileNodeList.java
Copyright: Tim Carver
License: LGPL-2+

Files: debian/*
Copyright:
	2015-2016 Afif Elghraoui <afif@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as published
 by  the Free Software Foundation; either version 2 of the License or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Library General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: amule-emc
Source: https://github.com/palopezv/amule-emc

Files: *
Copyright: 2007-2008 Johannes Krampf <wuischke@amule.org>
License: GPL-2+

Files: debian/*
Copyright: 2008-2019, Sandro Tosi <morph@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA
 .
 On a Debian system the complete text of the GNU General Public License
 v2 can be found in the file `/usr/share/common-licenses/GPL-2'

Files: src/MuleCollection.cpp
Comments:
 Angel Vidal Veiga aka Kry <kry@amule.org>
 * changed class names
 .
 Marcelo Malheiros <mgmalheiros@gmail.com>
 * fixed error with FT_FILEHASH
 * added inital 5 tag/file support
 .
 Any parts of this program derived from the xMule, lMule or eMule project,
 or contributed by third-party developers are copyrighted by their
 respective authors.

Files: src/Types.h
Copyright: Copyright (c) 2003-2008 aMule Team ( admin@amule.org / http://www.amule.org )
 Copyright (c) 2002 Merkur ( devs@emule-project.net / http://www.emule-project.net )
Comments:
 Any parts of this program derived from the xMule, lMule or eMule
 project, or contributed by third-party developers are copyrighted
 by their respective authors.

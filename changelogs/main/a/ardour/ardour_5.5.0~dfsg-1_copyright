Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Ardour
Upstream-Contact: Paul Davis <paul@linuxaudiosystems.com>
Source: http://ardour.org/download
Copyright: 1998-2016, Paul Davis
Files-Excluded:
 waf
 .gitignore
 msvc*/*
 MSVC*/*
 icons/win32/*
 gtk2_ardour/win32/*

Files: *
Copyright:
 1998-2017 Paul Davis
License: GPL-2+

Files: debian/*
Copyright:
 2013-2016 Adrian Knoth <adi@drcomp.erfurt.thur.de>
 2013-2016 Jaromír Mikeš <mira.mikes@seznam.cz>
 2015-2016 IOhannes m zmölnig <umlaeute@debian.org>
License: GPL-2+

Files: libs/vamp-*
Copyright:
 2000-2008 Chris Cannam
 2006-2008 Queen Mary, University of London
License: Expat and other-nopromo-Chris

Files:
 libs/vamp-plugins/Onset.* libs/vamp-plugins/PercussionOnsetDetector.cpp
Copyright:
 2006-2008 Chris Cannam
License: GPL-2+

Files:
 libs/appleutility/*
Copyright:
 2005-2014 Apple Computer, Inc.
License: other-Apple

Files: libs/surfaces/tranzport/*
Copyright:
 2006 Paul Davis <pbd@op.net>
 2007 Michael Taht
License: GPL-2+

Files: libs/surfaces/mackie/*
Copyright:
 2006-2008 John Anderson
License: GPL-2+

Files:
 libs/surfaces/mackie/mcp_buttons.cc
 libs/surfaces/mackie/led.cc
 libs/surfaces/mackie/led.h
 libs/surfaces/mackie/controls.h
 libs/surfaces/mackie/jog.cc
 libs/surfaces/mackie/meter.cc
 libs/surfaces/mackie/button.h
 libs/surfaces/mackie/fader.cc
 libs/surfaces/mackie/meter.h
 libs/surfaces/mackie/device_profile.cc
 libs/surfaces/mackie/device_info.h
 libs/surfaces/mackie/pot.cc
 libs/surfaces/mackie/device_info.cc
 libs/surfaces/mackie/button.cc
 libs/surfaces/mackie/strip.cc
 libs/surfaces/mackie/jog.h
 libs/surfaces/mackie/pot.h
 libs/surfaces/mackie/mackie_control_protocol.cc
Copyright:
 2006-2008 John Anderson
 2012 Paul Davis
License: GPL-2+

Files:
 libs/ardour/ardour/ticker.h
 libs/ardour/ardour/midi_patch_manager.h
 libs/ardour/ticker.cc
 libs/ardour/midi_patch_manager.cc
 libs/midi++2/midnam_patch.cc
Copyright:
 2008 Hans Baier
License: GPL-2+

Files:
 libs/ardour/vumeterdsp.cc
 libs/ardour/kmeterdsp.cc
 libs/ardour/iec2ppmdsp.cc
 libs/ardour/ardour/iec1ppmdsp.h
 libs/ardour/ardour/kmeterdsp.h
 libs/ardour/ardour/vumeterdsp.h
 libs/ardour/ardour/iec2ppmdsp.h
Copyright:
 2008-2012 Fons Adriaensen <fons@linuxaudio.org>
License: GPL-2+

Files:
 libs/surfaces/control_protocol/*
 gtk2_ardour/canvas-waveview.h
Copyright:
 2006 Paul Davis <pbd@op.net>
License: LGPL-2+

Files:
 libs/ardour/midiport_manager.cc
 libs/ardour/async_midi_port.cc
 libs/ardour/ardour/midiport_manager.h
 libs/ardour/ardour/async_midi_port.h
 libs/gtkmm2ext/auto_spin.cc
 libs/gtkmm2ext/choice.cc
 libs/gtkmm2ext/click_box.cc
 libs/gtkmm2ext/gtk_ui.cc
 libs/gtkmm2ext/gtkmm2ext/auto_spin.h
 libs/gtkmm2ext/gtkmm2ext/click_box.h
 libs/gtkmm2ext/gtkmm2ext/gtk_ui.h
 libs/gtkmm2ext/gtkmm2ext/gtkutils.h
 libs/gtkmm2ext/gtkmm2ext/popup.h
 libs/gtkmm2ext/gtkmm2ext/prompter.h
 libs/gtkmm2ext/gtkmm2ext/selector.h
 libs/gtkmm2ext/gtkmm2ext/tearoff.h
 libs/gtkmm2ext/gtkmm2ext/textviewer.h
 libs/gtkmm2ext/gtkmm2ext/utils.h
 libs/gtkmm2ext/popup.cc
 libs/gtkmm2ext/prompter.cc
 libs/gtkmm2ext/selector.cc
 libs/gtkmm2ext/tearoff.cc
 libs/gtkmm2ext/textviewer.cc
 libs/gtkmm2ext/utils.cc
 libs/midi++2/midi++/channel.h
 libs/midi++2/midi++/mmc.h
 libs/midi++2/midi++/parser.h
 libs/midi++2/midi++/port.h
 libs/midi++2/midi.cc
 libs/midi++2/mmc.cc
 libs/midi++2/mtc.cc
 libs/midi++2/midi++/ipmidi_port.h
 libs/midi++2/port.cc
 libs/midi++2/parser.cc
 libs/midi++2/channel.cc
 libs/pbd/pbd/abstract_ui.h
 libs/pbd/pbd/mountpoint.h
 libs/pbd/pbd/pool.h
 libs/pbd/pbd/receiver.h
 libs/pbd/pbd/selectable.h
 libs/pbd/pbd/stl_delete.h
 libs/pbd/pbd/stl_functors.h
 libs/pbd/pbd/textreceiver.h
 libs/pbd/pbd/thrown_error.h
 libs/pbd/pbd/touchable.h
 libs/pbd/pbd/transmitter.h
 libs/pbd/pool.cc
 libs/pbd/receiver.cc
 libs/pbd/textreceiver.cc
 libs/pbd/transmitter.cc
 libs/pbd/receiver.cc
Copyright:
 1998-2005 Paul Barton-Davis
License: GPL-2+

Files:
 gtk2_ardour/bundle_env_msvc.cc
 libs/ardour/ardour/msvc_libardour.h
 libs/pbd/pbd/msvc_pbd.h
 libs/pbd/pbd/windows_special_dirs.h
 libs/pbd/windows_special_dirs.cc
Copyright:
 2008-2014 John Emmas
License: GPL-2+

Files:
 gtk2_ardour/nsmclient.cc
 gtk2_ardour/nsm.h
 gtk2_ardour/nsmclient.h
 gtk2_ardour/nsm.cc
Copyright:
 2012 Jonathan Moore Liles
License: GPL-2+

Files:
 libs/gtkmm2ext/paths_dialog.cc
 libs/gtkmm2ext/gtkmm2ext/paths_dialog.h
 libs/plugins/reasonablesynth.lv2/rsynth.c
 libs/plugins/reasonablesynth.lv2/lv2.c
 libs/ardouralsautil/request_device.c
 libs/ardour/ardour/mididm.h
 libs/ardour/ardour/ebur128_analysis.h
 libs/ardour/ebur128_analysis.cc
 libs/ardour/mididm.cc
 libs/vfork/exec_wrapper.c
Copyright:
 2013-2015 Robin Gareus <robin@gareus.org>
License: GPL-2+

Files:
 libs/canvas/xfade_curve.cc
 libs/canvas/canvas/xfade_curve.h
 libs/ardouralsautil/devicelist.cc
 libs/ardouralsautil/ardouralsautil/devicelist.h
 libs/ardour/ardour/system_exec.h
 libs/ardour/ardour/delayline.h
 libs/ardour/delayline.cc
 libs/pbd/pbd/system_exec.h
 session_utils/fix_bbtppq.cc
Copyright:
 2013-2015 Robin Gareus <robin@gareus.org>
 2006-2013 Paul Davis
License: GPL-2+

Files:
 libs/gtkmm2ext/emscale.cc
 libs/gtkmm2ext/gtkmm2ext/emscale.h
Copyright:
 2014 Paul Davis, Robin Gareus
License: GPL-2+

Files:
 libs/gtkmm2ext/gtkmm2ext/rgb_macros.h
Copyright:
 2000 EMC Capital Management, Inc
License: GPL-2+

Files:
 gtk2_ardour/timers.cc
 gtk2_ardour/timers.h
 libs/ardour/session_state_utils.cc
 libs/ardour/ardour/session_state_utils.h
 libs/ardour/ardour/jack_utils.h
 libs/ardour/ardour/session_directory.h
 libs/ardour/ardour/tape_file_matcher.h
 libs/ardour/ardour/filesystem_paths.h
 libs/ardour/test/test_ui.h
 libs/ardour/test/test_ui.cc
 libs/ardour/session_directory.cc
 libs/ardour/filesystem_paths.cc
 libs/ardour/test/test_util.cc
 libs/pbd/search_path.cc
 libs/pbd/timer.cc
 libs/pbd/resource.cc
 libs/pbd/ffs.cc
 libs/pbd/test/test_common.h
 libs/pbd/test/test_common.cc
 libs/pbd/pbd/timing.h
 libs/pbd/pbd/file_utils.h
 libs/pbd/pbd/timer.h
 libs/pbd/pbd/ffs.h
 libs/pbd/pbd/resource.h
 libs/pbd/pbd/search_path.h
 libs/pbd/pbd/atomic_counter.h
 libs/pbd/timing.cc
Copyright:
 2007-2015 Tim Mayberry
License: GPL-2+

Files:
 libs/ardour/ardour/search_paths.h
 libs/ardour/test/test_util.h
 libs/ardour/test/test_receiver.h
 libs/pbd/file_utils.cc
Copyright:
 2007-2014 Tim Mayberry
 1998-2014 Paul Davis
License: GPL-2+

Files:
 libs/clearlooks-newer/clearlooks_rc_style.c
 libs/clearlooks-newer/clearlooks_rc_style.h
 libs/clearlooks-older/clearlooks_rc_style.c
 libs/clearlooks-older/clearlooks_rc_style.h
 libs/clearlooks-older/clearlooks_style.h
Copyright:
 2005 Richard Stellingwerff
License: LGPL-2+

Files:
 gtk2_ardour/gettext.h
 libs/ardour/gettext.h
 libs/gtkmm2ext/gettext.h
 libs/pbd/gettext.h
Copyright:
 1995-1998, 2000-2002 Free Software Foundation, Inc
License: LGPL-2+

Files:
 libs/pbd/pbd/timersub.h
Copyright:	
 1991-1994,1996-2003,2005,2006,2009 Free Software Foundation, Inc.
License: LGPL-2.1+

Files:
 gtk2_ardour/export_range_markers_dialog.h
Copyright:
 2006 Andre Raue
License: GPL-2+

Files:
 libs/pbd/pbd/undo.h
 libs/pbd/undo.cc
Copyright:
 2001-2002 Brett Viren & Paul Davis
License: GPL-2+

Files:
 libs/ardour/ardour/mtdm.h
 libs/ardour/mtdm.cc
Copyright:
 2003-2008 Fons Adriaensen <fons@kokkinizita.net>
License: GPL-2+

Files:
 libs/pbd/pbd/command.h
 libs/pbd/pbd/memento_command.h
Copyright:
 2006 Hans Fugal
 2006 Paul Davis
License: GPL-2+

Files:
 libs/ardour/ardour/vestige/aeffectx.h
Copyright:
 2006 Javier Serrano Polo <jasp00/at/users.sourceforge.net>
License: GPL-2+

Files:
 libs/pbd/pbd/ringbuffer.h
 libs/pbd/pbd/ringbufferNPT.h
Copyright:
 2000 Benno Senoner
 2000 Paul Davis
License: GPL-2+

Files:
 libs/clearlooks-newer/clearlooks_draw_gummy.c
 libs/clearlooks-newer/clearlooks_draw_inverted.c
Copyright:
 2007 Andrea Cimitan
License: LGPL-2+

Files:
 libs/clearlooks-newer/clearlooks_style.c
 libs/clearlooks-newer/support.c
Copyright:
 2005 Richard Stellingwerff
 2007, Benjamin Berg <benjamin@sipsolutions.net>
License: LGPL-2+

Files:
 libs/pbd/pbd/xml++.h
 libs/pbd/xml++.cc
Copyright:
 2000 Ari Johnson, and
License: GPL-2+

Files:
 libs/ardour/cycle_timer.cc
Copyright:
 2002 Andrew Morton
License: GPL-2+

Files: ./gtk2_ardour/rgb_macros.h
Copyright: 2000, EMC Capital Management, Inc
License: GPL-2+

Files:
 libs/ardour/pcm_utils.cc
Copyright:
 2006 Paul Davis
 Erik de Castro Lopo
License: GPL-2+

Files:
 gtk2_ardour/utils.cc
Copyright:
 2000 Greg Ercolano <erco@3dsite.com>
 2003 Paul Davis
License: GPL-2+

Files:
 libs/ardour/audio_unit.cc
Copyright:
 2006-2009, Paul Davis
 Sophia Poirier
License: GPL-2+

Files:
 libs/ardour/ardour/pcm_utils.h
Copyright:
 2006 Paul Davis
 Erik de Castro Lopo
License: GPL-2+

Files:
 libs/ardour/sse_functions_64bit.s
Copyright:
 2005-2006 John Rigg
 2005-2006 Paul Davis
License: GPL-2+

Files:
 libs/ardour/ardour/mix.h
Copyright:
 2005 Sampo Savolainen
License: GPL-2+

Files:
 libs/ardour/ardour/logcurve.h
Copyright:
 2001 Paul Davis
 2001 Steve Harris
License: GPL-2+

Files:
 libs/audiographer/private/gdither/gdither_types.h
 libs/audiographer/private/gdither/gdither.cc
 libs/audiographer/private/gdither/gdither.h
 libs/audiographer/private/gdither/gdither_types_internal.h
Copyright:
 2002 Steve Harris
License: GPL-2+

Files:
 libs/gtkmm2ext/gtkmm2ext/application.h
Copyright:
 2009 Paul Davis
License: LGPL-2.1

Files:
 libs/clearlooks-newer/clearlooks_draw_glossy.c
Copyright:
 2006 Benjamin Berg
 2007 Andrea Cimitan
License: LGPL-2+

Files:
 libs/clearlooks-newer/clearlooks_draw.c
Copyright:
 2006 Daniel Borgman
 2006 Richard Stellingwerff
 2007 Andrea Cimitan
 2007 Benjamin Berg
License: LGPL-2+

Files:
 libs/clearlooks-newer/clearlooks_style.h
Copyright:
 2005 Richard Stellingwerff
 2006 Benjamin Berg
License: LGPL-2+

Files:
 libs/clearlooks-newer/animation.c
Copyright:
 2006 Benjamin Berg <benjamin@sipsolutions.net>
 2006 Kulyk Nazar <schamane@myeburg.net>
License: LGPL-2+

Files:
 libs/ardour/ardour/spline.h
Copyright:
 1997 David Mosberger
License: LGPL-2+

Files:
 libs/surfaces/mackie/timer.h
Copyright:
 1998-2000, 2007 John Anderson
License: LGPL-2+

Files:
 libs/pbd/pbd/compose.h
Copyright:
 2002 Ole Laursen <olau@hardworking.dk>
License: LGPL-2.1+

Files:
 libs/ardour/ardour/ladspa.h
Copyright:
 2000-2002 Richard W.E. Furse, Paul Barton-Davis
License: LGPL-2.1+

Files:
 libs/vamp-plugins/AmplitudeFollower.cpp
 libs/vamp-plugins/AmplitudeFollower.h
Copyright:
 2006 Dan Stowell
License: Expat and other-nopromo-Chris

Files:
 libs/vamp-plugins/ebu_r128_proc.cc
 libs/vamp-plugins/ebu_r128_proc.h
Copyright:
 2010-2011 Fons Adriaensen <fons@linuxaudio.org>
 2015 Robin Gareus <robin@gareus.org>
License: GPL-2+

Files:
 tools/session_exchange.py
Copyright:
 2004-2005 Taybin Rutkin
License: GPL-2+
Comment: 
 From: Paul Davis <paul@linuxaudiosystems.com> 
 To: Jaromír Mikeš <mira.mikes@gmail.com>
 Copy: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org> 
 Subject: Re: Ardour3 in debian
 Date: Wed Sep 18 13:32:49 UTC 2013
 .
 GPL2+ is taybin's answer (actually, his real answer is that he doesn't care :)
 .
 --p
 .
 On Wed, Sep 18, 2013 at 2:53 AM, Jaromír Mikeš <mira.mikes at gmail.com> wrote:
 .
 >
 > 2013/9/17 Paul Davis <paul at linuxaudiosystems.com>
 >
 >> Since that script (session_exchange.py) no longer works, I suggest you
 >> remove it from your packaging.
 >>
 >> The licensing intent ... I will ask Taybin.
 >>
 >
 > Thank you Paul for prompt answer,
 >
 > removing session_exchange.py would be solution, clarifying from Taybin
 > would be great.
 >
 > mira

Files:
 libs/pbd/pbd/fastlog.h
Copyright:
 unknown. Code by Laurent de Soras <laurent@ohmforce.com>
License: WTFPL-2

Files:
 tools/omf/loader.cc
 tools/omf/omftool.cc
Copyright:
 2009 Hannes Breul
License: zlib

Files:
 libs/libltc/ltc/ltc.h
 libs/libltc/ltc/decoder.h
 libs/libltc/ltc/encoder.h
 libs/libltc/timecode.c
 libs/libltc/ltc.c
 libs/libltc/encoder.c
 libs/libltc/decoder.c
Copyright:
 2006-2012 Robin Gareus <robin@gareus.org>
License: LGPL-3+

Files:
 libs/timecode/timecode/time.h
 libs/timecode/timecode/bbt_time.h
 libs/timecode/src/time.cc
 libs/timecode/src/bbt_time.cc
Copyright:
 2002-2010 Paul Davis
License: LGPL-2+

Files:
 gtk2_ardour/gtk_pianokeyboard.c
Copyright:
 2007, 2008 Edward Tomasz Napiera <trasz@FreeBSD.org>
License: BSD-2-clause

Files:
 libs/pbd/boost-debug/shared_ptr.hpp
Copyright:
 2001, 2002, 2003 Peter Dimov
 1998, 1999 Greg Colvin and Beman Dawes
License: BSL-1.0

Files:
 libs/gtkmm2ext/sync-menu.c
 libs/gtkmm2ext/gtkmm2ext/sync-menu.h
 libs/gtkmm2ext/gtkmm2ext/gtkapplication.h
Copyright:
 2007 Pioneer Research Center USA, Inc / 2007 Imendio AB
License: LGPL-2.1

Files:
 libs/gtkmm2ext/gtkapplication.c
 libs/gtkmm2ext/application.cc
 libs/gtkmm2ext/gtkapplication_x11.c
 libs/gtkmm2ext/gtkmm2ext/gtkapplication-private.h
 libs/gtkmm2ext/gtkapplication_win32.c
Copyright:
 2009 Paul Davis / 2007 Imendio AB / 2007 Pioneer Research Center USA, Inc
License: LGPL-2.1

Files:
 libs/evoral/evoral/Control.hpp
 libs/evoral/evoral/Note.hpp
 libs/evoral/evoral/Event.hpp
 libs/evoral/evoral/Curve.hpp
 libs/evoral/evoral/EventSink.hpp
 libs/evoral/evoral/types.hpp
 libs/evoral/evoral/Range.hpp
 libs/evoral/evoral/Parameter.hpp
 libs/evoral/evoral/ControlList.hpp
 libs/evoral/evoral/Sequence.hpp
 libs/evoral/evoral/MIDIEvent.hpp
 libs/evoral/evoral/TypeMap.hpp
 libs/evoral/evoral/ControlSet.hpp
 libs/evoral/evoral/EventList.hpp
 libs/evoral/evoral/PatchChange.hpp
 libs/evoral/evoral/TimeConverter.hpp
 libs/evoral/evoral/midi_util.h
 libs/evoral/evoral/SMF.hpp
 libs/evoral/evoral/OldSMF.hpp
 libs/evoral/evoral/SMFReader.hpp
 libs/evoral/test/SMFTest.hpp
 libs/evoral/src/Curve.cpp
 libs/evoral/src/SMF.cpp
 libs/evoral/src/MIDIEvent.cpp
 libs/evoral/src/Sequence.cpp
 libs/evoral/src/Note.cpp
 libs/evoral/src/ControlList.cpp
 libs/evoral/src/OldSMF.cpp
 libs/evoral/src/ControlSet.cpp
 libs/evoral/src/Control.cpp
 libs/evoral/src/SMFReader.cpp
 libs/evoral/src/Event.cpp
Copyright:
 2000-2008 Paul Davis / 2008 David Robillard <http://drobilla.net>
License: GPL-2+

Files:
 libs/evoral/evoral/midi_events.h
 libs/midi++2/midi++/events.h
Copyright:
 *No copyright*
License: LGPL-2.1+

Files:
 libs/surfaces/frontier/kernel_drivers/tranzport.c	
Copyright:
 2004 Greg Kroah-Hartman (greg@kroah.com)
 2005 Michael Hund <mhund@ld-didactic.de>
 2007 Michael Taht (m@taht.net)
 2003 David Glance <advidgsf@sourceforge.net>
License: GPL-2+

Files:
 libs/ardouralsautil/ardouralsautil/reserve.h
 libs/ardouralsautil/reserve.c
Copyright:
 2009 Lennart Poettering
License: Expat

Files:
 libs/pbd/system_exec.cc
Copyright:
 2005-2008 Lennart Poettering
 2010-2014 Robin Gareus <robin@gareus.org>
 2010 Paul Davis
License: GPL-2+

Files:
 libs/qm-dsp/*
Copyright:
 2006 Chris Cannam
 2006-2010, Queen Mary, University of London
License: GPL-2+

Files:
 libs/qm-dsp/maths/CosineDistance.cpp
 libs/qm-dsp/maths/CosineDistance.h
Copyright:
 2008 Kurt Jacobson
License: GPL-2+

Files: libs/qm-dsp/maths/Polyfit.h
Copyright: 1981-2001 Allen Miller, David J Taylor and others
License: MPL-1.1

Files:
 libs/qm-dsp/dsp/rhythm/BeatSpectrum.cpp
 libs/qm-dsp/dsp/rhythm/BeatSpectrum.h
Copyright:
 2008 Kurt Jacobson and QMUL
License: GPL-2+

Files:
 libs/qm-dsp/maths/pca/pca.h
Copyright:
 2006 Centre for Digital Music, Queen Mary, University of London
License: GPL-2+

Files:
 libs/qm-dsp/maths/KLDivergence.cpp
 libs/qm-dsp/maths/KLDivergence.h
Copyright:
 2008 QMUL
License: GPL-2+

Files:
 libs/qm-dsp/dsp/mfcc/MFCC.h
 libs/qm-dsp/dsp/mfcc/MFCC.cpp
Copyright:
 2005 Nicolas Chetry, 2008 QMUL
License: GPL-2+

Files:
 libs/qm-dsp/dsp/tonal/TCSgram.h
 libs/qm-dsp/dsp/tonal/TCSgram.cpp
 libs/qm-dsp/dsp/tonal/ChangeDetectionFunction.h
 libs/qm-dsp/dsp/tonal/ChangeDetectionFunction.cpp
 libs/qm-dsp/dsp/tonal/TonalEstimator.cpp
 libs/qm-dsp/dsp/tonal/TonalEstimator.h
Copyright:
 2006 Martin Gasser
License: GPL-2+

Files:
 libs/qm-dsp/dsp/wavelet/Wavelet.cpp
 libs/qm-dsp/dsp/wavelet/Wavelet.h
Copyright:
 2009 Thomas Wilmering
License: GPL-2+

Files:
 libs/qm-dsp/dsp/tempotracking/TempoTrackV2.h
 libs/qm-dsp/dsp/tempotracking/DownBeat.h
 libs/qm-dsp/dsp/tempotracking/DownBeat.cpp
 libs/qm-dsp/dsp/tempotracking/TempoTrackV2.cpp
Copyright:
 2008-2009 Matthew Davies and QMUL
License: GPL-2+

Files:
 libs/qm-dsp/dsp/tempotracking/TempoTrack.cpp	
Copyright:
 2005-2006 Christian Landone and Matthew Davies
License: GPL-2+

Files:
 libs/ardour/rdff.c
 libs/ardour/lv2_evbuf.h
 libs/ardour/lv2_evbuf.c
 libs/ardour/lv2_evbuf.h
Copyright:
 2008-2012 David Robillard <http://drobilla.net>
License: ISC

Files:
 libs/audiographer/private/sndfile.hh
Copyright:
 2005-2007 Erik de Castro Lopo <erikd@mega-nerd.com>
License: BSD-3-clause
 
Files: libs/lua/LuaBridge/*
Copyright: 2001
  2001, Andrei Alexandrescu
  2007, Nathan Reed
  2008, Nigel Atkinson
  2012, Vinnie Falco <vinnie.falco@gmail.com>
License: Expat
 
Files: libs/lua/lua-5.3.3/*
Copyright: 1994-2016, Lua.org, PUC-Rio.
License: Expat

Files: libs/vamp-plugins/BarBeatTrack.cpp
 libs/vamp-plugins/BeatTrack.cpp
 libs/vamp-plugins/ChromagramPlugin.cpp
 libs/vamp-plugins/KeyDetect.cpp
 libs/vamp-plugins/OnsetDetect.cpp
 libs/vamp-plugins/TonalChangeDetect.cpp
 libs/vamp-plugins/Transcription.cpp
Copyright: 2006-2009, QMUL -";
  2006-2013, QMUL -";
  2008-2009, QMUL -";
License: GPL-2+

Files: libs/qm-dsp/ext/kissfft/COPYING
 libs/qm-dsp/ext/kissfft/_kiss_fft_guts.h
 libs/qm-dsp/ext/kissfft/kiss_fft.c
 libs/qm-dsp/ext/kissfft/tools/kiss_fftnd.c
 libs/qm-dsp/ext/kissfft/tools/kiss_fftndr.c
 libs/qm-dsp/ext/kissfft/tools/kiss_fftr.c
Copyright: 2003-2004, Mark Borgerding
  2003-2010, Mark Borgerding
License: BSD-3-clause

Files: libs/qm-dsp/base/Pitch.cpp
 libs/qm-dsp/base/Pitch.h
 libs/qm-dsp/base/Window.h
 libs/qm-dsp/maths/MedianFilter.h
 libs/vamp-plugins/Onset.cpp
 libs/vamp-plugins/Onset.h
Copyright: 2006, Chris Cannam.
  2010, Chris Cannam.
License: GPL-2+

Files: libs/surfaces/cc121/cc121.cc
 libs/surfaces/cc121/cc121.h
 libs/surfaces/cc121/cc121_interface.cc
 libs/surfaces/cc121/gui.cc
 libs/surfaces/cc121/gui.h
 libs/surfaces/cc121/operations.cc
Copyright: 2006, Paul Davis
  2012, Paul Davis
  2015, Paul Davis
  2016, W.P. van Paassen
License: GPL-2+

Files: libs/hidapi/hidapi/hidapi.h
 libs/hidapi/linux/hid.c
 libs/hidapi/mac/hid.c
 libs/hidapi/windows/hid.c
Copyright: 2009-2010
License: GPL-3

Files: libs/qm-dsp/dsp/phasevocoder/PhaseVocoder.cpp
 libs/qm-dsp/dsp/phasevocoder/PhaseVocoder.h
 libs/qm-dsp/maths/KLDivergence.h
Copyright: 2008-2013, QMUL.
License: GPL-2+

Files: libs/qm-dsp/dsp/keydetection/GetKeyMode.cpp
 libs/qm-dsp/dsp/keydetection/GetKeyMode.h
Copyright: 2005, Centre for Digital Music ( C4DM )
License: GPL-2+

Files: libs/qm-dsp/maths/pca/pca.h
 libs/vamp-plugins/SimilarityPlugin.h
Copyright: 2006, Centre for Digital Music, Queen Mary, University of London.
  2008, Centre for Digital Music, Queen Mary, University of …
License: GPL-2+

Files: libs/qm-dsp/dsp/chromagram/ConstantQ.cpp
 libs/qm-dsp/dsp/chromagram/ConstantQ.h
Copyright: NONE
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 Some files differ from above by replacing "this program" with "this
 file".
 .
 On Debian GNU systems, the complete text of the GNU General Public
 License (GPL) version 2 or later can be found in
 '/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment: You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU Lesser General
 Public License (LGPL) version 2.1 or later can be found in
 '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/>.

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published
 by the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU Library General
 Public License (LGPL - later renamed GNU Lesser General Public License)
 version 2 or later can be found in '/usr/share/common-licenses/LGPL-2'.
 .
 You should have received a copy of the GNU Library General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/>.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 Lesser General Public License for more details.
 .
 On Debian GNU systems, the complete text of the GNU Lesser General
 Public License (LGPL) version 2.1 can be found in
 '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/>.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: other-nopromo-Chris
 Except as contained in this notice, the names of the Centre for Digital
 Music; Queen Mary, University of London; and Chris Cannam shall not be
 used in advertising or otherwise to promote the sale, use or other
 dealings in this Software without prior written authorization.

License: other-Apple
 IMPORTANT:  This Apple software is supplied to you by Apple Computer,
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 .
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple’s copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety
 and without modifications, you must retain this notice and the
 following text and disclaimers in all such redistributions of the Apple
 Software.  Neither the name, trademarks, service marks or logos of
 Apple Computer, Inc. may be used to endorse or promote products derived
 from the Apple Software without specific prior written permission from
 Apple.  Except as expressly stated in this notice, no other rights or
 licenses, express or implied, are granted by Apple herein, including
 but not limited to any patent rights that may be infringed by your
 derivative works or by other works in which the Apple Software may be
 incorporated.
 .
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 .
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: WTFPL-2
 This work is free. You can redistribute it and/or modify it under the
 terms of the Do What The Fuck You Want To Public License, Version 2,
 as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
X-Comment: On Debian systems, the complete text of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSL-1.0
 Distributed under the Boost Software License, Version 1.0. (See
 accompanying file LICENSE_1_0.txt or copy at
 http://www.boost.org/LICENSE_1_0.txt)

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 * Neither the author nor the names of any contributors may be used
   to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MPL-1.1
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
 .
 The Original Code is JEDI math.
 .
 You may obtain a copy of the License at http://www.mozilla.org/MPL/

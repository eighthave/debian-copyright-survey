Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: antrigrav
Source: http://www.luolamies.org/software/antigrav/

Files: *
Copyright: 2006 Calle Laakkonen <calle@luolamies.org>
           2006 Salminen <RichardoX@gmail.com>
License: GPL-2

Files: src/tinyxml/*
Copyright: 2000-2002 Lee Thomason <leethomason@gmail.com>
License: Zlib

Files: debian/*
Copyright: 2006-2007 Gürkan Sengün <gurkan@linuks.mine.nu>
           2007      Ana Beatriz Guerrero Lopez <ana@debian.org>
           2007      Barry deFreese <bddebian@comcast.net>
           2007      Gürkan Sengün <gurkan@linuks.mine.nu>
           2007-2014 Vincent Fourmond <fourmond@debian.org>
           2014      Evgeni Golov
           2016      Markus Koschany <apo@debian.org>
           2016      Tobias Frost <tobi@debian.org>
           2018      Matheus Faria <matheus.sousa.faria@gmail.com>
           2019      Daniel Pimentel <d4n1@d4n1.org>
License: GPL-2+

License: GPL-2 or GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems the full text of the GNU General Public License can
 be found in the "/usr/share/common-licenses/GPL-2" file.

License: Zlib
 This software is provided 'as-is', without any express or implied warranty. 
 In no event will the authors be held liable for any damages arising from 
 the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose, 
 including commercial applications, and to alter it and redistribute 
 it freely, subject to the following restrictions:
 1. The origin of this software must not be misrepresented; you must 
    not claim that you wrote the original software. If you use this 
    software in a product, an acknowledgment in the product documentation 
    would be appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not 
    be misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Miro Hrončok <miro@hroncok.cz>
Source: https://github.com/admesh/admesh

Files: *
Copyright: Copyright (C) 1995, 1996 Anthony D. Martin <amartin@varlog.com>
           Copyright (C) 2013 Andy Doucette <andy@printathing.com>
           Copyright (C) 2013 Anton Gladky <gladk@debian.org>
           Copyright (C) 2013, 2014 Alessandro Ranellucci <aar@cpan.org>
           Copyright (C) 2013, 2014 Guillaume Seguin <guillaume@segu.in>
           Copyright (C) 2013, 2014 Miro Hrončok <miro@hroncok.cz>
           Copyright (C) 2013, 2014 Tomas Chvatal <tomas.chvatal@gmail.com>
License: GPL-2+

Files: debian/*
Copyright: 1998 John Lapeyre <lapeyre@physics.arizona.edu>
           2000 Martin Michlmayr <tbm@cyrius.com>
           2001,2002 Adrian Bunk <bunk@fs.tum.de>
           2003-2005 Kevin M. Rosenberg <kmr@debian.org>
           2006-2010 Víctor Pérez Pereira <vperez@debianvenezuela.org>
           2011-2013 Anton Gladky <gladk@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Authentication Headers
Upstream-Contact: Valimail Inc <info@valimail.com>
Source: https://pypi.org/project/authheaders/

Files: *
Copyright: Copyright (c) 2017 Valimail Inc
License: BSD-like

Files: authheaders/dmarc_lookup.py
Copyright: Copyright © 2014, 2015, 2016 OnlineGroups.net and Contributors.
License: ZPL-2.1

Files: authheaders/public_suffix_list.txt
Copyright: Not specified
License: MPL-2.0

Files: debian/*
Copyright: 2018 Scott Kitterman <scott@kitterman.com>
License: BSD-like

License: BSD-like
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: ZPL-2.1
 Zope Public License (ZPL) Version 2.1
 .
 A copyright notice accompanies this license document that identifies the
 copyright holders.
 .
 This license has been certified as open source. It has also been designated as
 GPL compatible by the Free Software Foundation (FSF).
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions in source code must retain the accompanying copyright
 notice, this list of conditions, and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the accompanying copyright
 notice, this list of conditions, and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Names of the copyright holders must not be used to endorse or promote
 products derived from this software without prior written permission from the
 copyright holders.
 .
 4. The right to distribute this software or to use it for any purpose does not
 give you the right to use Servicemarks (sm) or Trademarks (tm) of the
 copyright
 holders. Use of them is covered by separate agreement with the copyright
 holders.
 .
 5. If any files are modified, you must cause the modified files to carry
 prominent notices stating that you changed the files and the date of any
 change.
 .
 Disclaimer
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESSED
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
 .
 On Debian systems, the full text of the Mozilla Public License 2.0 can be
 found in the file `/usr/share/common-licenses/MPL-2.0'.

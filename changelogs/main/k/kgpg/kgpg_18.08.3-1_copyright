Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2016, Andrius Štikonas <andrius@stikonas.eu>
           2016, David Zaslavsky <diazona@ellipsix.net>
           2002, Jean-Baptiste Mardelle
           2002-2004, Jean-Baptiste Mardelle <bj@altern.org>
           2006, Jimmy Gilles
           2006-2007, Jimmy Gilles <jimmygilles@gmail.com>
           2005, KDE RUssian translation team
           2011, Luis Ángel Fernández Fernández <laffdez@gmail.com>
           2007, Lukas Kropatschek
           2011, Philip Greggory Lee <rocketman768@gmail.com>
           2007-2018, Rolf Eike Beer <kde@opensource.sf-tec.de>
           2007, Rosetta Contributors and Canonical Ltd 2007
           2003-2017, This_file_is_part_of_KDE
           2003, Waldo Bastian
License: GPL-2+

Files: debian/*
Copyright: 2012, Eshat Cakar <info@eshat.de>
License: GPL-2+

Files: core/KGpgExpandableNode.cpp
       core/KGpgExpandableNode.h
       core/KGpgGroupMemberNode.cpp
       core/KGpgGroupMemberNode.h
       core/KGpgGroupNode.cpp
       core/KGpgGroupNode.h
       core/KGpgKeyNode.cpp
       core/KGpgKeyNode.h
       core/KGpgNode.cpp
       core/KGpgNode.h
       core/KGpgOrphanNode.cpp
       core/KGpgOrphanNode.h
       core/KGpgRefNode.cpp
       core/KGpgRefNode.h
       core/KGpgRootNode.cpp
       core/KGpgRootNode.h
       core/KGpgSignNode.cpp
       core/KGpgSignNode.h
       core/KGpgSignableNode.cpp
       core/KGpgSignableNode.h
       core/KGpgSubkeyNode.cpp
       core/KGpgSubkeyNode.h
       core/KGpgUatNode.cpp
       core/KGpgUatNode.h
       core/KGpgUidNode.cpp
       core/KGpgUidNode.h
       keytreeview.cpp
       keytreeview.h
       kgpgfirstassistant.cpp
       kgpgfirstassistant.h
       model/gpgservermodel.cpp
       model/gpgservermodel.h
       model/groupeditproxymodel.cpp
       model/groupeditproxymodel.h
       model/keylistproxymodel.cpp
       model/keylistproxymodel.h
       model/kgpgitemmodel.cpp
       model/kgpgitemmodel.h
       model/kgpgitemnode.h
       model/kgpgsearchresultmodel.h
       model/selectkeyproxymodel.cpp
       model/selectkeyproxymodel.h
Copyright: 2016, David Zaslavsky <diazona@ellipsix.net>
           2008-2017, Rolf Eike Beer <kde@opensource.sf-tec.de>
           2013, Thomas Fischer <fischer@unix-ag.uni-kl.de>
License: GPL-2+3+KDEeV

Files: doc/index.docbook
       po/ca/docs/kgpg/index.docbook
       po/de/docs/kgpg/index.docbook
       po/es/docs/kgpg/index.docbook
       po/et/docs/*
       po/fr/docs/*
       po/gl/docs/*
       po/it/docs/kgpg/index.docbook
       po/lt/docs/*
       po/nl/docs/*
       po/pt/docs/*
       po/pt_BR/docs/kgpg/index.docbook
       po/sr/docs/kgpg/index.docbook
       po/sv/docs/kgpg/index.docbook
       po/uk/docs/kgpg/index.docbook
Copyright: 2002-2003, <personname
           2002-2003, Jean-Baptiste Mardelle
           2002-2003, Jean-Baptiste Mardelle <email
           2002-2003, depara Jean-Baptiste Mardelle <email
License: GFDL-1.2+

Files: kgpg_debug.cpp
       kgpg_debug.h
Copyright: 2015, Laurent Montel <montel@kde.org>
License: LGPL-2+

Files: org.kde.kgpg.appdata.xml
Copyright: KDE e.V
License: CC0 or GPL-2+

Files: po/uk/kgpg.po
Copyright: 2004-2017, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

License: CC0
 CC0 1.0 Universal
 .
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
 LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
 ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
 INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
 REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
 THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
 HEREUNDER.
 .
 The complete text of the Creative Commons 0 1.0 Universal
 can be found in `/usr/share/common-licenses/CC0-1.0'.

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 The complete text of the GNU Free Documentation License 1.2
 can be found in `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 The complete text of the GNU General Public License version 2
 can be found in `/usr/share/common-licenses/GPL-2', likewise, the
 complete text of the GNU General Public License version 3 can be
 found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 The complete text of the GNU Lesser General Public License version 2
 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 The complete text of the GNU Lesser General Public License version 2.1
 can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 The complete text of the GNU Lesser General Public License version 3
 can be found in `/usr/share/common-licenses/LGPL-3'.


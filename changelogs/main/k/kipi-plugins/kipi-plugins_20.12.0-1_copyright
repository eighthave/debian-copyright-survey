Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kipi-plugins
Upstream-Contact: kde-devel@kde.org
Source: https://invent.kde.org/graphics/kipi-plugins

Files: *
Copyright: 2011-2012, A Janardhan Reddy <annapareddyjanardhanreddy@gmail.com>
           2011-2015, Alexander Potashev <aspotashev@gmail.com>
           2011, Alexandre Mendes <alex.mendes1988@gmail.com>
           2008-2012, Andi Clemens <andi.clemens@googlemail.com>
           2008, Andrea Diamantini <adjam7@gmail.com>
           2008, Andreas Trink <atrink@nociaro.org>
           2013-2017, Andrew Goodbody <ajg zero two@elfringham.co.uk>
           2006-2013, Angelo Naselli <anaselli@linux.it>
           2008-2016, Caulier Gilles <caulier.gilles@gmail.com>
           2006-2007, Colin Guthrie <kde@colin.guthr.ie>
           2011, Dirk Tilger <dirk.kde@miriup.de>
           2012, Dodon Victor <dodonvictor@gmail.com>
           2016, Fabian Vogt <fabian@ritter dash vogt.de>
           2010-2014, Frederic Coiffier <frederic.coiffier@free.com>
           2003-2018, Gilles Caulier <caulier.gilles@gmail.com>
           2012, Iliya Ivanov <ilko2002@abv.bg>
           2010, Jens Mueller <tschenser@gmx.de>
           2005-2006, Joern Ahrens <joern.ahrens@kdemail.net>
           2006-2009, Johannes Wienke <languitar@semipol.de>
           2003-2016, KDE developers team
           2008-2010, Luka Renko <lure@kubuntu.org>
           2011-2013, Lukas Krejci <krejci.l@centrum.cz>
           2017-2018, Maik Qualmann <metzpinguin@gmail.com>
           2007-2010, Marcel Wiesweg <marcel.wiesweg@gmx.de>
           2010-2013, Marius Orcsik <marius@habarnam.ro>
           2010, Michael G. Hansen <mike@mghansen.de>
           2006, Michael Hoechstetter <michael.hoechstetter@gmx.de>
           2012, Nathan Damie <nathan.damie@gmail.com>
           2013, Pankaj Kumar <me@panks.me>
           2012, Parthasarathy Gopavarapu <gparthasarathy93@gmail.com>
           2012-2016, Peter Potrowl <peter.potrowl@gmail.com>
           2009, Pieter Edelman <pieter.edelman@gmx.net>
           2003-2005, Renchi Raju <renchi.raju@gmail.com>
           2010-2013, Roman Tsisyk <roman@tsisyk.com>
           2013, Saurabh Patel
           2015, Shourya Singh Gupta <shouryasgupta@gmail.com>
           2006-2007, Stephane Pontier <shadow.walker@free.fr>
           2002-2004, Todd Shoemaker <todd@theshoemakers.net>
           2006, Tom Albers <tomalbers@kde.nl>
           2005-2009, Vardhman Jain <vardhman@gmail.com>
           2011-2014, Veaceslav Munteanu <veaceslav.munteanu90@gmail.com>
License: GPL-2+

Files: CMakeLists.txt
       */CMakeLists.txt
       cmake/templates/gitscript.cmake.in
       common/data/libkipiplugins.qrc
Copyright: 2010-2018, Gilles Caulier <caulier dot gilles at gmail dot com>
           2015, Alexander Potashev <aspotashev@gmail.com>
License: BSD-3-clause

Files: common/libkipiplugins/o2/*
Copyright: 2011, Andre Somers
           2012, Akos Polster
License: BSD-2-clause-o2

Files: common/libkipiplugins/o2/acknowledgements.md
       common/libkipiplugins/o2/src/o0simplecrypt.h
       common/libkipiplugins/o2/src/o2simplecrypt.cpp
Copyright: 2011, Andre Somers
License: BSD-3-clause

Files: common/libkipiplugins/tools/kipiplugins_debug.cpp
       common/libkipiplugins/tools/kipiplugins_debug.h
Copyright: 2014, Laurent Montel <montel at kde dot org>
License: LGPL-2+

Files: org.kde.kipi_plugins.metainfo.xml
Copyright: 2019, Jonathan Riddell <jr@jriddell.org>
License: FSFAP

Files: po/ca/*.po
       po/ca@valencia/*.po
       po/uk/*.po
Copyright: 2004-2020 This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV-translations

Files: yandexfotki/yandexrsa.cpp
       yandexfotki/yandexrsa.h
Copyright:  2010, George Barwood
            2010, Yandex
License: public-domain
 This code is in the public domain; do with it what you wish.

Files: debian/*
Copyright: 2020, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2019, Jonathan Riddell <jr@jriddell.org>
           2020, Pino Toscano <pino@debian.org>
License: GPL-2+

License: BSD-2-clause-o2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this  
 list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 --
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2’.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 --
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1+3+KDEeV-translations
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 --
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1’,
 likewise, the complete text of the GNU Lesser General Public License version
 3 can be found in `/usr/share/common-licenses/LGPL-3’.

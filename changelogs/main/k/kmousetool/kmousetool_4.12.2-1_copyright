Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kmousetool
Source: http://www.kde.org

Files: *
Copyright: 2002-2003 Jeff Roush <jeff@mousetool.com>
           2003 Olaf Schmidt <ojschmidt@kde.org>
           2003 Gunnar Schmi Dt <gunnar@schmi-dt.de>
License: GPL-2.0+

Files: doc/*
Copyright: 2002 Jeff Roush <jeff@kmousetool.com>
           2010 Lauri.Watts
License: GFDL-NIV-1.2+
Comment: index.docbook contains the calls &underFDL, &underGPL and &FDLNotice.
 This serve the purpose to expand the licenses explaining that the doc is under
 the FDL with no Invariant Sections, with no Front-Cover Texts, and with no
 Back-Cover Texts and that the program itself is under the GPL.

Files: kmousetool/Xmd_kmousetool.h
Copyright: 1987-1998  The Open Group
           1987 Digital Equipment Corporation, Maynard, Massachusetts.
License: MIT

Files: debian/*
Copyright: 2012 Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GFDL-NIV-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 can be found in "/usr/share/common-licenses/GFDL-1.2".

License: MIT
 Copyright 1987, 1998  The Open Group
 .
 All Rights Reserved.
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 OPEN GROUP BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of The Open Group shall not be
 used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization from The Open Group.
 .
 Copyright 1987 by Digital Equipment Corporation, Maynard, Massachusetts.
 .
 All Rights Reserved
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the name of Digital not be
 used in advertising or publicity pertaining to distribution of the
 software without specific, written prior permission.
 .
 DIGITAL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 DIGITAL BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KiNG
Upstream-Contact: Vincent B. Chen <vincent.chen@duke.edu>
Source: http://kinemage.biochem.duke.edu/software/king.php
Files-Excluded: *.jar
                *.class
                javadocs
                lib
                king*/installer/king
                king*/installer/linux*
                king*/installer/win*
                king*/installer/*.gif
                king*/installer/KiNG*
                king*/installer/*.indigo
                king*/installer/.xvpics
                extratools*/doc/work/tools-manual.pdf
                king*/doc/work/king-manual.pdf
                king*/doc/work/format-kinemage.pdf
                king*/1.x_src

Files: *
Copyright: 2002-2011 Ian W. Davis <ian.w.davis@gmail.com>,
                     Vincent B. Chen <vincent.chen@duke.edu>
License: KiNG-License
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 3. The end-user documentation included with the redistribution, if any, must
    include the following acknowledgment:
 .
    "This product includes software developed in the Richardon lab at Duke
    University (http://kinemage.biochem.duke.edu)."
 .
    Alternately, this acknowledgment may appear in the software itself, if and
    wherever such third-party acknowledgments normally appear.
 .
 4. The name of the copyright holder, the names of the contributors, and the
    names "KiNG", "Kinemage, Next Generation", and "Mage" must not be used to
    endorse or promote products derived from this software without prior
    written permission.
 5. Products derived from this software may not be called "KiNG", nor may
    "KiNG" appear in their name, without prior written permission.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 This license is based on the BSD and Apache licenses.
 See http://www.opensource.org/ for more details on these and other licenses.
Comment:  The Files driftwood-*-src/* are released under "The Driftwood License"
 but this is just another name for the completely identical text as the "King
 License" and copyright holders are identical as well.

Files: debian/*
Copyright: 2012 Andreas Tille <tille@debian.org>
License: Apache-2.0
 On Debian systems the complete text of the Apache-2.0 license can be found at
 `/usr/share/common-licenses/Apache-2.0`.


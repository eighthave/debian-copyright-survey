Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kservice
Source: https://projects.kde.org/projects/frameworks/kservice

Files: *
Copyright: 2006, Aaron Seigo <aseigo@kde.org>
           2014, Alex Richardson <arichardson.kde@gmail.com>
           1999-2013, David Faure <faure@kde.org>
           2008, Hamish Rodda <rodda@kde.org>
           2004-2008, Jarosław Staniek <staniek@kde.org>
           2003-2007, Matthias Kretz <kretz@kde.org>
           2006, Ralf Habacker <ralf.habacker@freenet.de>
           2013, Sebastian Kügler <sebas@kde.org>
           1999-2000, Torben Weis <weis@kde.org>
           1999-2003, Waldo Bastian <bastian@kde.org>
           2009, Adriaan de Groot, Mustapha Abubakar, Ibrahim Dasuna
           2006, Canonical Ltd, and Rosetta Contributors 2006
           1998, Erez Nir <erez-n@actcom.co.il>
           1997-2009, Free Software Foundation, Inc
           2008-2009, K Desktop Environment
           2005, KDE Armenian translation team
           1999-2002, KDE Developers
           2005, KDE Russian translation team
           2000-2003, KDE e.v
           2007, KDE i18n Project for Vietnamese
           1999-2003, Meni Livne <livne@kde.org>
           2008, This_file_is_part_of_KDE
           2001, translate.org.za
           1999-2002, กลุ่มผู้พัฒนา KDE
           1999, David Faure <faure@kde.org>
           2003-2007, Matthias Kretz <kretz@kde.org>
           2002-2003, Waldo Bastian <bastian@kde.org>
License: LGPL-2

Files: autotests/ksycocadicttest.cpp
       autotests/pluginlocatortest.cpp
       autotests/pluginlocatortest.h
       src/kbuildsycoca/kbuildmimetypefactory.cpp
       src/kbuildsycoca/kbuildmimetypefactory.h
       src/kbuildsycoca/kbuildservicefactory.h
       src/kbuildsycoca/kbuildservicetypefactory.h
       src/kdeinit/ktoolinvocation.cpp
       src/kdeinit/ktoolinvocation.h
       src/kdeinit/ktoolinvocation_x11.cpp
       src/services/kautostart.cpp
       src/services/kautostart.h
       src/services/kservice.h
       src/services/kservice_p.h
       src/services/kserviceaction.cpp
       src/services/kserviceaction.h
       src/services/kservicefactory.h
       src/services/kservicetype.h
       src/services/kservicetype_p.h
       src/services/kservicetypefactory.h
       src/services/kservicetypeprofile.h
       src/services/ktraderparse.cpp
       src/services/ktraderparse_p.h
       src/services/ktraderparsetree.cpp
       src/services/ktraderparsetree_p.h
       src/sycoca/kmemfile.cpp
       src/sycoca/kmemfile_p.h
       src/sycoca/ksycocatype.h
       tests/kdbusservicestartertest.cpp
       tests/pluginlocator/plugintest.cpp
       tests/pluginlocator/plugintest.h
Copyright: 2006-2008, Aaron Seigo <aseigo@kde.org>
           2005, Brad Hards <bradh@frogmouth.net>
           2008, Christian Ehrlicher <ch.ehrlicher@gmx.de>
           1999-2007, David Faure <faure@kde.org>
           1999, Espen Sand <espen@kde.org>
           2000-2004, Frerich Raabe <raabe@kde.org>
           2001-2005, Lubos Lunak <l.lunak@kde.org>
           1997-2000, Matthias Ettrich <ettrich@troll.no>
           1997-1999, Matthias Kalle Dalheimer <kalle@kde.org>
           2003-2004, Oswald Buddenhagen <ossi@kde.org>
           2012-2013, Sebastian Kügler <sebas@kde.org>
           1998-2005, Stephan Kulow <coolo@kde.org>
           2006, Thiago Macieira <thiago@kde.org>
           1998-1999, Torben Weis <weis@kde.org>
           1999-2004, Waldo Bastian <bastian@kde.org>
License: LGPL-2+

Files: autotests/kmimeassociationstest.cpp
       autotests/ksycocathreadtest.cpp
       src/kbuildsycoca/kbuildsycocainterface.h
       src/kbuildsycoca/kmimeassociations.cpp
       src/kbuildsycoca/kmimeassociations.h
       src/sycoca/ksycocadevices_p.h
Copyright: 2008-2009, David Faure <faure@kde.org>
License: LGPL-2+3+KDEeV

Files: po/ca/kservice5.po
       po/ca@valencia/kservice5.po
       po/uk/kservice5.po
       tests/findservice.cpp
Copyright: 2014, Alex Merry <alex.merry@kde.org>
           1998-2014, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

Files: COPYING.LIB
       src/sycoca/sycocadebug.cpp
       src/sycoca/sycocadebug.h
Copyright: 1991-1999, Free Software Foundation, Inc
License: LGPL-2.1+

Files: autotests/fakeplugin.desktop
       autotests/fakeplugin.json
       autotests/noplugin.desktop
Copyright: No copyright claimed
License: LGPL

Files: COPYING
       tests/pluginlocator/main.cpp
Copyright: 1989-1991, Free Software Foundation, Inc
           2013, Sebastian Kügler <sebas@kde.org>
License: GPL-2+

Files: autotests/nsaplugin.cpp
       autotests/nsaplugin.h
Copyright: 2013, Sebastian Kügler <sebas@kde.org>
License: GPL-2+3+KDEeV

Files: src/services/yacc.c
       src/services/yacc.h
Copyright: 1984-2006, Free Software Foundation, Inc.
License: GPL-3+_BisonException

Files: COPYING.GPL3
Copyright: 2007, Free Software Foundation, Inc. <http://fsf.org/>
License: GPL-3+

Files: po/bg/kservice5.po
Copyright: 2009-2013, Yasen Pramatarov <yasen@lindeas.com>
           2006-2009, Zlatko Popov <zlatkopopov@fsa-bg.org>
License: GPL

Files: debian/*
Copyright: 2014, Scarlett Clark <scarlett@scarlettgatelyclark.com>
License: LGPL-2+

License: GPL
 This file is licensed under the GPL.
 .
 On Debian systems, the complete text of the GNU General Public License can be
 found in `/usr/share/common-licenses/GPL'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2,
 or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2", likewise, the
 complete text of the GNU General Public License version 3 can be found in
 "/usr/share/common-licenses/GPL-3".

License: GPL-3+_BisonException
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.
 .
 This special exception was added by the Free Software Foundation in
 version 2.2 of Bison.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL
 X-KDE-PluginInfo-License=LGPL
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL'.

License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License version 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2+3+KDEeV
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2 of the License or ( at
 your option ) version 3 or, at the discretion of KDE e.V. ( which shall
 act as a proxy as in section 14 of the GPLv3 ), any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General Public License
 version 2 can be found in "/usr/share/common-licenses/LGPL-2", likewise,
 the complete text of the GNU Lesser General Public License version 3 can be
 found in "/usr/share/common-licenses/LGPL-3".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-2.1+3+KDEeV
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) version 3, or any
 later version accepted by the membership of KDE e.V. (or its
 successor approved by the membership of KDE e.V.), which shall
 act as a proxy defined in Section 6 of version 3 of the license.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1", likewise,
 the complete text of the GNU Lesser General Public License version 3 can be
 found in "/usr/share/common-licenses/LGPL-3".

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kst
Source: http://kst.kde.org/

Files: *
Copyright: 2003-2010 The University of Toronto
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public License 
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: debian/*
Copyright: 2011 Ruben Molina <rmolina@udea.edu.co>
           2010 Steven Benton <stevebenton@rogers.com>
           2010 Serafeim Zanikolas <sez@debian.org>
           2005 LaMont Jones <lamont@debian.org>
           2004-2008 Mark Hymers <mhy@debian.org>
License: GPL-2.0+

Files: src/libkst/editablevector.*
       src/libkst/editablematrix.*
       src/libkstmath/emailthread.*
       src/libkst/datamatrix.*
       src/libkst/generatedmatrix.*
       src/libkst/matrix.*
       src/libkstmath/csd.*
       src/libkstmath/eventmonitorentry.*
       src/libkstmath/image.*
       src/libkstmath/linestyle.*
       src/libkstmath/plotdefines.*
       src/plugins/*
Copyright: 2007 The University of Toronto <netterfield@astro.utoronto.ca>
           2004-2005 The University of British Columbia <dscott@phas.ubc.ca>
License: GPL-2.0+

Files: src/libkstapp/gridlayouthelper.*
Copyright: 1992-2007 Trolltech ASA
           2007 The University of Toronto
License: GPL-2.0+

Files: src/libkstapp/cartesianrenderitem.*
       src/libkstapp/plotrenderitem.cpp
       src/libkstapp/axis.cpp
       old_tests/healpix/testhealpix.cpp
Copyright: 2005-2007 Theodore Kisner <tsk@humanityforward.org>
License: GPL-2.0+

Files: src/datasources/healpix/healpix_tools*
Copyright: 2005 Ted Kisner <tskisner.public@gmail.com>
License: GPL-2.0+

Files: src/datasources/netcdf/*
Copyright: 2004 Nicolas Brisset <nicodev@users.sourceforge.net>
License: GPL-2.0+

Files: src/libkst/psversion.*
Copyright: 2002-2003 Albert Cahalan <albert@users.sf.net>
           1996 Charles Blake <cblake@bbn.com>
           1995 Martin Schulze <joey@infodrom.north.de>
License: GPL-2.0+

Files: src/libkst/procps.*
Copyright: 1998-2003 Albert Cahalan <albert@users.sf.net>
License: GPL-2.0+

Files: src/libkst/kst_export.h
Copyright: 2005 Dirk Mueller <mueller@kde.org>
License: GPL-2.0+

Files: src/plugins/dataobject/genericfilter/*
Copyright: 2005 Jean-Paul Petillon
License: GPL-2.0+

Files: src/libkstmath/eparse.*
Copyright: 1984-2006 Free Software Foundation, Inc.
License: GPL-2.0+

Files: src/libkstmath/psdcalculator.*
Copyright: 2006 Kst
License: GPL-2.0+

Files: src/libkst/dataprimitive.*
       src/libkst/datascalar.*
       src/libkst/datastring.*
       src/libkst/datavector.*
       src/libkst/generatedvector.*
       src/libkst/scalar.*
       src/libkst/vector.*
       src/libkst/vscalar*
       src/libkstmath/curve.*
       src/libkstmath/curvepointsymbol.*
Copyright: © 2000-2009, cbn
License: GPL-2.0+

Files: src/libkst/namedobject.*
       src/libkst/shortnameindex.*
       src/libkstmath/curve.*
       src/libkstmath/curvepointsymbol.*
       src/libkstmath/dataobject.*
       src/libkstmath/histogram.*
       src/libkstmath/relation.*
       src/libkstmath/equation.*
       src/libkstmath/palette.*
       src/libkstmath/psd.*
       src/libkstapp/aboutdialog.*
       src/libkstapp/commandlineparser.*
       src/libkstapp/dimensionstab.*
       src/libkstapp/logdialog.*
       src/libkstapp/rangetab.*
       src/datasources/qimagesource/qimagesource.*
       src/widgets/dialogdefaults.*
       src/d2asc/d2asc.*
       old_tests/kstXbench/*
       old_tests/dirfile_replay/getdata_struct.*
       old_tests/dirfile_replay/getdata.*
       old_tests/dirfile_replay/replay.*
Copyright: 2000-2010 C. Barth Netterfield <netterfield@astro.utoronto.ca>
License: GPL-2.0+

License: LGPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as 
 published by the Free Software Foundation; either version 2 of the 
 License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General Public 
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".


Files: src/libkst/timezones.*
Copyright: 2005 S.R.Haque <srhaque@iee.org>
License: LGPL-2.0+

Files: src/libkst/sharedptr.h
Copyright: 2004 The University of Toronto <netterfield@astro.utoronto.ca>
           1999 Waldo Bastian <bastian@kde.org>
License: LGPL-2.0+

Files: src/libkst/sysinfo.*
Copyright: 1998-2003 Albert Cahalan <albert@users.sf.net>
           1992-1998 Michael K. Johnson <johnsonm@redhat.com>
License: LGPL-2.0+

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: svn2cl.xsl
Copyright: 2004 Arthur de Jong.
License: BSD-3-Clause

Files: misc/getdata-windows/*
       misc/netcdf-windows/* 
Copyright: 2006-2008 Alexander Chemeris
License: BSD-3-Clause

Files: src/libkstmath/fftsg_h.*
Copyright: 1996-2001 Takuya OOURA <ooura@mmm.t.u-tokyo.ac.jp>
License: fftsg
 You may use, copy, modify and distribute this code for any purpose 
 (include commercial use) and without fee. Please refer to this package 
 when you modify this code.
Comment: http://www.kurims.kyoto-u.ac.jp/~ooura/fft.html

Files: old_tests/planck/toigen.c
Copyright: No Copyright
License: public-domain
 Written By Staikos Computing Services Inc
 Released to the public domain without warranty.

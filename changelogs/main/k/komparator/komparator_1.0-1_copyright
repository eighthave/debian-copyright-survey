This work was packaged for Debian by:

    Fathi Boudra <fboudra@free.fr> on Tue, 1 Aug 2006 17:42:25 +0200.

It was downloaded from:

    http://kde-apps.org/content/show.php/komparator4?content=116039

Upstream Author:

    Georg Hennig <georg.hennig@web.de>

Copyright:

    Copyright (C) 2005-2010 Georg Hennig <georg.hennig@web.de>

License:

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>

On Debian systems, the complete text of the GNU General
Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

The Debian packaging is:

    Copyright (C) 2006-2010 Fathi Boudra <fabo@debian.org>

you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Copyright for src/komparatorcomparejob.*:

    Copyright (C) 2005-2010 Georg Hennig <georg.hennig@web.de>

    This file uses main parts of "fdupes.c" from fdupes-1.40.
    FDUPES Copyright (C) 1999 Adrian Lopez <adrian2@caribe.net>

    Permission is hereby granted, free of charge, to any person
    obtaining a copy of this software and associated documentation files
    (the "Software"), to deal in the Software without restriction,
    including without limitation the rights to use, copy, modify, merge,
    publish, distribute, sublicense, and/or sell copies of the Software,
    and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

Copyright for src/kmdcodec.*:

    Copyright (C) 2000-2001 Dawit Alemayehu <adawit@kde.org>
    Copyright (C) 2001 Rik Hemsley (rikkus) <rik@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

    RFC 1321 "MD5 Message-Digest Algorithm" Copyright (C) 1991-1992.
    RSA Data Security, Inc. Created 1991. All rights reserved.

    The KMD5 class is based on a C++ implementation of
    "RSA Data Security, Inc. MD5 Message-Digest Algorithm" by
    Mordechai T. Abzug,  Copyright (c) 1995.  This implementation
    passes the test-suite as defined in RFC 1321.

    The encoding and decoding utilities in KCodecs with the exception of
    quoted-printable are based on the java implementation in HTTPClient
    package by Ronald Tschal�r Copyright (C) 1996-1999.

    The quoted-printable codec as described in RFC 2045, section 6.7. is by
    Rik Hemsley (C) 2001.

    KMD4 class based on the LGPL code of Copyright (C) 2001 Nikos Mavroyanopoulos
    The algorithm is due to Ron Rivest.  This code is based on code
    written by Colin Plumb in 1993.

Copyright for src/kdatecombo.*:

    Copyright (C) 2008 Daniel Caleb Jones <danielcjones@gmail.com> 

The documentations provided with komparator is distributed under
the GNU Free Documentation License (FDL). They are considered free with
regards to the Debian Free Software Guidelines (DFSG) because they don't
contain any unmodifiable parts (invariant sections).

On Debian systems, the complete text of the GNU Free Documentation
License can be found in `/usr/share/common-licenses/GFDL'.

Komparator uses kquery.cpp, kquery.h, kdatecombo.cpp and kdatecombo.h taken
without changes from kfind which is © Stephan Kulow <coolo@kde.org> and
is licensed under the GPL, see above.

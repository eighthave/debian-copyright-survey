Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kio-gdrive
Source: https://invent.kde.org/network/kio-gdrive
Upstream-Contact: Elvis Angelaccio <elvis.angelaccio@kde.org>

Files: *
Copyright: 2013, 2014  Daniel Vrátil <dvratil@redhat.com>
           2016, 2017  Elvis Angelaccio <elvis.angelaccio@kde.org>
           2019, 2020  David Barchiesi <david@barchie.si>
License: GPL-2+

Files: cmake/*
Copyright: 2013  Valama development team
License: GPL-3+

Files: desktop/org.kde.kio_gdrive.metainfo.xml
Copyright: 2017-2018  Elvis Angelaccio <elvis.angelaccio@kde.org>
License: CC0-1.0
Comment:
      Primary copyright holder was established by examining upstream git
      history.  The file was added in commit:032c7f773baa.  Translations
      are injected into git by "l10n daemon script <scripty@kde.org>".
      Please contact upstream for further details about potential copyright
      holders for the po files that this script uses as its sources.  CC0-1.0
      does not require either a full-text license or header to be distributed
      with the associated work, thus one has not been included here.

Files: src/gdrive_udsentry.h
       src/integration/propertiesplugin/gdrivepropertiesplugin.cpp
       src/integration/propertiesplugin/gdrivepropertiesplugin.h
Copyright: 2019, 2020  David Barchiesi <david@barchie.si>
License: KDE-Accepted-GPL
Comment:
 These files are technically "GPL-2.0-only OR GPL-3.0-only OR
 LicenseRef-KDE-Accepted-GPL" Given that the text of
 LicenseRef-KDE-Accepted-GPL.txt defines a GPL-3-only state, it is
 unclear whether the work as a whole is GPL-2.0-only OR GPL-3.0-only,
 but given that the license of the bulk of the files is GPL-2+, it is
 likely that KDE-Accepted-GPL best represents the intent of upstream.
 That intent seems to be GPL-2+, gated/proxied by the KDE e.V, which
 GPL-3.0-only satisfies moreso than GPL-2.0-only

Files: debian/*
Copyright: 2016  Clive Johnston <clivejo@kubuntu.org>
           2016  Scarlett Clark <sgclark@kubuntu.org>
           2017  Jonathan Riddell <jr@jriddell.org>
           2018-2021  Nicholas D Steeves <sten@debian.org>
License: GPL-2+

License: CC0-1.0
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 Copyright (C) 2013, Valama development team
 .
 Valama is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 Valama is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: KDE-Accepted-GPL
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of
 the license or (at your option) at any later version that is
 accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

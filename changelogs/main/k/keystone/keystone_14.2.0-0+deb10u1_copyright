Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: keystone
Source: https://github.com/openstack/keystone

Files: *
Copyright: 2010 United States Government as represented by the Administrator of
            the National Aeronautics and Space Administration.
	   (c) 2010-2017, OpenStack Foundation
           (c) 2013-2015, IBM Corp.
           (c) 2013-2015, Hewlett-Packard Development Company, L.P.
           (c) 2011, Piston Cloud Computing, Inc.
           (c) 2015, UnitedStack, Inc
           (c) 2017-2018, Huawei
           (c) 2012, Canonical Ltd.
           (c) 2013, Metacloud, Inc.
           (c) 2011-2012, Justin Santa Barbara
           (c) 2013-2017, Red Hat, Inc.
           (c) 2017-2018, SUSE Linux Gmbh
           (c) 2016, Massachusetts Open Cloud
           (c) 2015, CERN
           (c) 2016, Intel Corporation
           (c) 2015, Mirantis Inc.
License: Apache-2

Files: debian/*
Copyright: (c) 2011-2016, Thomas Goirand <zigo@debian.org>
           (c) 2012, Ghe Rivero <ghe@debian.org>
           (c) 2012, Mehdi Abaakouk <sileht@sileht.net>
           (c) 2011-2012, Julien Danjou <acid@debian.org>
           (c) 2017-2018, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy of
 the License at:
 .
  http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations under
 the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license can be
 found in /usr/share/common-licenses/Apache-2.0.

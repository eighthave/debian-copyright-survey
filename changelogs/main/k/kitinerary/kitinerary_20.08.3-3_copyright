Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kitinerary
Source: https://invent.kde.org/pim/kitinerary
Upstream-Contact: kde-devel@kde.org

Files: *
Copyright: 2018, Daniel Vrátil <dvratil@kde.org>
           2018, Luca Beltrame <lbeltrame@kde.org>
           2018, Sune Vuorela <sune@kde.org>
           2017-2018, Volker Krause <vkrause@kde.org>
License: LGPL-2+

Files: debian/*
Copyright: 2018 Jonathan Riddell <jr@jriddell.org>
License: LGPL-2+

Files: src/cli/org.kde.kitinerary-extractor.appdata.xml
Copyright: KDE e.V.
License: CC0-1.0

Files: po/ca/*
       po/ca@valencia/*
       po/uk/*
Copyright: 2018, This_file_is_part_of_KDE
License: LGPL-2.1+3+KDEeV

License: CC0-1.0
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 The complete text of the GNU Lesser General Public License version 2.1
 can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 The complete text of the GNU Lesser General Public License version 3
 can be found in `/usr/share/common-licenses/LGPL-3'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+3+KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2.1".
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

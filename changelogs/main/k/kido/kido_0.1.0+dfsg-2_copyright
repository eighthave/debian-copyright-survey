Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KIDO
Source: http://github.com/dartsim/dart
File-Excluded: .travis.yml .gitignore docs/

Files: *
Copyright: 2008-2015 Georgia Tech Research Corporation
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: kido/lcp*
Copyright: 2001,2002 Russell L. Smith
License: LGPL-2.1
  See `/usr/share/common-licenses/LGPL-2.1`

Files: kido/planning/RRT.cpp
Copyright: 2010, Georgia Tech Research Corporation
License: BSD-3-clause

Files: kido/gui/lodepng.hpp
Copyright: 2005-2012 Lode Vandevenne
License: Zlib or Libpng

Files: tools/cpplint.py
Copyright: 2009 Google Inc. All rights reserved
License: AS-IS

Files: kido/utils/ParserSkel.cpp_
Copyright: 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.
License: GPL-2+
  See `/usr/share/common-licenses/GPL-2`

Files: kido/utils/urdf/urdf_world_parser.hpp
Copyright: 2013, Humanoid Robotics Lab
           2013, Willow Garage, Inc
License: BSD-3-clause

Files: unittests/gtest/include/gtest/gtest.h
Copyright: 2005, Google Inc
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer. 
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE  
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS   
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)     
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF    
 SUCH DAMAGE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

License: libpng
 The PNG Reference Library is supplied "AS IS".  The Contributing Authors
 and Group 42, Inc. disclaim all warranties, expressed or implied,
 including, without limitation, the warranties of merchantability and of
 fitness for any purpose.  The Contributing Authors and Group 42, Inc.
 assume no liability for direct, indirect, incidental, special, exemplary,
 or consequential damages, which may result from the use of the PNG
 Reference Library, even if advised of the possibility of such damage.
 .
 Permission is hereby granted to use, copy, modify, and distribute this
 source code, or portions hereof, for any purpose, without fee, subject
 to the following restrictions:
 .
 1. The origin of this source code must not be misrepresented.
 .
 2. Altered versions must be plainly marked as such and must not
 be misrepresented as being the original source.
 .
 3. This Copyright notice may not be removed or altered from any
 source or altered source distribution.
 .
 The Contributing Authors and Group 42, Inc. specifically permit, without
 fee, and encourage the use of this source code as a component to
 supporting the PNG file format in commercial products.  If you use this
 source code in a product, acknowledgment is not required but would be
 appreciated.

License: AS-IS
 This software is provided 'as-is', without any express or implied 
 warranty. In no event will the authors be held liable for any 
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any 
 purpose, including commercial applications, and to alter it and 
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must 
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and 
 must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source 
 distribution.

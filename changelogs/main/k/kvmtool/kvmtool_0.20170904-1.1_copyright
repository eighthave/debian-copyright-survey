Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://git.kernel.org/cgit/linux/kernel/git/will/kvmtool.git/
Upstream-Name: kvmtool
Upstream-Contact: kvm@vger.kernel.org

Files: *
Copyright: 2010-2015, Pekka Enberg <penberg@kernel.org>
 2010-2013, Asias He <asias.hejun@gmail.com>
 2011-2014, Sasha Levin
License: GPL-2+

Files: debian/*
Copyright: 2015, Riku Voipio <riku.voipio@linaro.org>
License: GPL-2+

Files: arm/*
Copyright: 2012, - Virtual Open Systems and Columbia University
License: GPL-2

Files: arm/aarch64/*
Copyright: 2012,2013, - ARM Ltd / 2012, - Virtual Open Systems and Columbia University
License: GPL-2

Files: include/*
Copyright: 2010, Pekka Enberg <penberg@kernel.org>
License: BSD-3-clause

Files: include/linux/9p.h
Copyright: 2005, Latchesar Ionkov <lucho@ionkov.net>
  2004, Eric Van Hensbergen <ericvh@gmail.com>
  2002, Ron Minnich <rminnich@lanl.gov>
License: GPL-2

Files: include/linux/rbtree.h
Copyright: 1999, Andrea Arcangeli <andrea@suse.de>
License: GPL-2+

Files: include/linux/rbtree_augmented.h
Copyright: 2012, Michel Lespinasse <walken@google.com>
  2002, David Woodhouse <dwmw2@infradead.org>
  1999, Andrea Arcangeli <andrea@suse.de>
License: GPL-2+

Files: include/linux/sizes.h
Copyright: Copyright (C) ARM Limited 1998.
License: GPL-2

Files: include/linux/virtio_mmio.h
Copyright: IBM Corp. 2007, / 2011, ARM Ltd
License: BSD-3-clause

Files: include/linux/virtio_scsi.h
Copyright: IBM Corp. 2010
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

Files: powerpc/*
Copyright: 2011, 2012, Matt Evans <matt@ozlabs.org>
 2007-2011, IBM Corporation
 2010,2011, David Gibson
 2011, Alexey Kardashevski
License: GPL-2

Files: util/*
Copyright: 2011, Pekka Enberg <penberg@kernel.org>
License: GPL-2+

Files: util/rbtree.c
Copyright: 2012, Michel Lespinasse <walken@google.com>
  2002, David Woodhouse <dwmw2@infradead.org>
  1999, Andrea Arcangeli <andrea@suse.de>
License: GPL-2+

Files: x86/*
Copyright: 1999-2007, H. Peter Anvin
License: GPL-2+

License: BSD-3-clause
 This program is free software; you can redistribute it and/or modify
 it under the terms of the BSD license.
 .
 On Debian systems, the complete text of the BSD license in
 '/usr/share/common-licenses/BSD'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ksnip
Source: https://github.com/ksnip/ksnip

Files: *
Copyright: 2016-2020 Damir Porobic <damir.porobic@gmx.com>
License: GPL-2.0+

Files: debian/*
Copyright: 2020 Boyuan Yang <byang@debian.org>
License: public-domain
 These files are released to the public domain.

Files: desktop/org.ksnip.ksnip.appdata.xml
Copyright:  2017-2020 Damir Porobic <damir.porobic@gmx.com>
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.

Files:
 src/backend/ipc/*
 src/backend/saver/WildcardResolver.h
 src/backend/saver/WildcardResolver.cpp
 src/backend/saver/UniqueNameProvider.h
 src/backend/saver/UniqueNameProvider.cpp
 src/gui/settingsDialog/SaverSettings.h
 src/gui/settingsDialog/SaverSettings.cpp
 src/gui/settingsDialog/StickerSettings.h
 src/gui/settingsDialog/StickerSettings.cpp
 src/bootstrapper/*.cpp
 src/bootstrapper/*.h
 src/bootstrapper/singleInstance/InstanceLock.*
 src/bootstrapper/singleInstance/SingleInstanceClientBootstrapper.*
 src/bootstrapper/singleInstance/SingleInstanceConstants.h
 src/bootstrapper/singleInstance/SingleInstanceParameterTranslator.cpp
 src/bootstrapper/singleInstance/SingleInstanceParameterTranslator.h
 src/bootstrapper/singleInstance/SingleInstanceServerBootstrapper.cpp
 src/bootstrapper/singleInstance/SingleInstanceServerBootstrapper.h
 src/widgets/CustomLineEdit.cpp
 src/widgets/CustomLineEdit.h
Copyright: 2020 Damir Porobic <damir.porobic@gmx.com>
License: LGPL-2.0+

License: GPL-2.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

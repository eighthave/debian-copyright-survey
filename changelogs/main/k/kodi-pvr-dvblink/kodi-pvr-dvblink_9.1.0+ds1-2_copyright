Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pvr.dvblink
Source: https://github.com/kodi-pvr/pvr.dvblink
Files-Excluded: depends

Files: *
Copyright: 2005-2021 Team Kodi
           2012 Palle Ehmsen
           2010-2011 Marcel Groothuis, Fred Hoogduin
License: GPL-2+

Files: src/base64.cpp
 src/base64.h
Copyright: 2004-2008 René Nyffenegger <rene.nyffenegger@adp-gmbh.ch>
 2012 Palle Ehmsen <Palle@barcodemadness.com>
License: BSD-3-clause
Comment: License retrieved from src/Base64.cpp

Files: lib/libdvblinkremote/*
Copyright: 2012 Marcus Efraimsson
 2013-2021 Team Kodi
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
Comment: License retrieved from lib/libdvblinkremote/COPYING

Files: debian/*
Copyright: 2020-2021 Vasyl Gello <vasek.gello@gmail.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-clause
 This source code is provided 'as-is', without any express or implied
 warranty. In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this source code must not be misrepresented; you must not
    claim that you wrote the original source code. If you use this source code
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original source code.
 .
 3. This notice may not be removed or altered from any source distribution.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: KDb
Source: https://community.kde.org/KDb

Files: *
Copyright: 1997, Matthias Kalle Dalheimer <kalle@kde.org>
           1999, Preston Brown <pbrown@kde.org>
           1998-1999, Torben Weis <weis@kde.org>
           1999, 2007, Stephan Kulow <coolo@kde.org>
           1999, Sirtaj Singh Kang <taj@kde.org>
           1999, Waldo Bastian <bastian@kde.org>
           2001, 2003-2016, Jarosław Staniek <staniek@kde.org>
           2002-2003, Joseph Wenninger <jowenn@kde.org>
           2002-2004, Lucijan Busch <lucijan@gmx.at>
           2003, 2005, 2011, Adam Pigg <adam@piggz.co.uk>
           2003, 2007, Oswald Buddenhagen <ossi@kde.org>
           2003, Daniel Molkentin <molkentin@kde.org>
           2004-2005, Martin Ellis <martin.ellis@kdemail.net>
           2006-2008, Sharan Rao <sharanrao@gmail.com>
           2006-2007, Thomas Braxton <kde.braxton@gmail.com>
           2007, 2009, David Faure <faure@kde.org>
           2012, Dimitrios T. Tanis <dimitrios.tanis@kdemail.net>
           2014, Michał Poteralski <michalpoteralskikde@gmail.com>
           2014, Radoslaw Wicik <radoslaw@wicik.pl>
           2015, Laurent Montel <montel@kde.org>
License: LGPL-2+

Files: cmake/modules/*
Copyright: 2004-2009, Kitware, Inc.
           2003-2017, Jarosław Staniek <staniek@kde.org>
           2008, Gilles Caulier <caulier.gilles@gmail.com>
           2009, Sebastian Trueg <trueg@kde.org>
           2013, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2014, Alex Merry <alex.merry@kdemail.net>
License: BSD-3-clause

Files: cmake/modules/GetGitRevisionDescription.cmake
       cmake/modules/GetGitRevisionDescription.cmake.in
Copyright: 2009-2010, Iowa State University
License: BSL-1.0

Files: src/drivers/sqlite/dump/shell.c
       src/drivers/sqlite/icu/*
Copyright: none
License: public-domain
 The files listed have been put on the public domain by the sqlite3
 contributors.

Files: src/parser/generate_parser_code.sh
       src/tools/transliteration/generate_transliteration_table.sh
Copyright: 2006-2015, Jarosław Staniek <staniek@kde.org>
License: GPL-2+

Files: src/parser/generated/sqlparser.cpp
       src/parser/generated/sqlparser.h
Copyright: 1984, 1989-1990, 2000-2015, Free Software Foundation, Inc.
License: GPL-3+-with-exception
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.

Files: tools/sdc.py
Copyright: 2010-2015, Jarosław Staniek <staniek@kde.org>
License: GPL-2+-with-exception
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program; see the file COPYING.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 As a special exception, you may create a larger work that contains
 code generated by the Shared Data Compiler and distribute that work
 under terms of the GNU Lesser General Public License (LGPL) as published
 by the Free Software Foundation; either version 2.1 of the License,
 or (at your option) any later version or under terms that are fully
 compatible with these licenses.
 .
 Alternatively, if you modify or redistribute the Shared Data Compiler tool
 itself, you may (at your option) remove this special exception, which will
 cause the resulting generted source code files to be licensed under
 the GNU General Public License (either version 2 of the License, or
 at your option under any later version) without this special exception.
 .
 This special exception was added by Jarosław Staniek.
 Contact him for more licensing options, e.g. using in non-Open Source projects.

Files: debian/*
Copyright: 2016-2017, Pino Toscano <pino@debian.org>
License: LGPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 On Debian systems, the complete texts of the GNU General Public License
 Version 2, and 3 can be found in "/usr/share/common-licenses/GPL-2", and
 "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 On Debian systems, the complete texts of the GNU Library General Public
 License Version 2, 2.1, and 3 can be found in
 "/usr/share/common-licenses/LGPL-2", "/usr/share/common-licenses/LGPL-2.1",
 and "/usr/share/common-licenses/LGPL-3".

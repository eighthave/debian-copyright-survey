Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mail-IMAPClient
Upstream-Contact: Phil Pearl (Lobbes) <phil@zimbra.com>
Source: https://metacpan.org/release/Mail-IMAPClient/

Files: *
Copyright: 2010-2013, Phil Pearl (Lobbes) <phil@zimbra.com>
 2007-2009, Mark Overmeer <mark@overmeer.net>
 1999-2003, The Kernen Group, Inc.
License: Artistic or GPL-1+

Files: examples/*
Copyright: 2003, The Kernen Group, Inc.
License: Artistic or GPL-1+
 This example is distributed with Mail::IMAPClient and subject to the same
 licensing requirements as Mail::IMAPClient.

Files: examples/imap_to_mbox.pl
Copyright: 1999, Thomas Stromberg <tstromberg@rtci.com>
License: BSD

Files: debian/*
Copyright: 2000-2004, Michael Alan Dorman <mdorman@debian.org>
 2004, Joachim Breitner <nomeata@debian.org>
 2004-2008, Gunnar Wolf <gwolf@debian.org>
 2006, Russ Allbery <rra@debian.org>
 2006-2008, Niko Tyni <ntyni@debian.org>
 2007-2008, Damyan Ivanov <dmn@debian.org>
 2008, Ansgar Burchardt <ansgar@debian.org>
 2008, Peter Pentchev <roam@ringlet.net>
 2008, Rene Mayorga <rmayorga@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2008-2013, gregor herrmann <gregoa@debian.org>
 2009, Ryan Niebur <ryan@debian.org>
 2009-2011, Jonathan Yu <jawnsy@cpan.org>
 2009-2012, Salvatore Bonaccorso <carnil@debian.org>
License: Artistic or GPL-1+
Comment: It is assumed that package maintainers have licensed their work
 under terms compatible with upstream licensing terms.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'

License: BSD
 This software is protected by the BSD License. No rights reserved anyhow. 
 <tstromberg@rtci.com>
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 . 
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-1'

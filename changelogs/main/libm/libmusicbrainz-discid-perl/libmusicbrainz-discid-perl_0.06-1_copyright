Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/MusicBrainz-DiscID
Upstream-Contact: Nicholas J. Humfrey <njh@aelius.com>
Upstream-Name: MusicBrainz-DiscID

Files: *
Copyright: 2009-2019, Nicholas J. Humfrey <njh@aelius.com>
License: Expat

Files: debian/*
Copyright: 2010, Jonathan Yu <jawnsy@cpan.org>
 2013-2018, gregor herrmann <gregoa@debian.org>
 2019, Nick Morrott <nickm@debian.org>
License: Expat
Comment:
 Debian packaging was relicensed to Expat on 2019/12/10  after consultation
 with Jonathan and gregor via email:
 .
 On Mon, 9 Dec 2019 at 20:31, Jonathan Yu <jonathan.i.yu@gmail.com> wrote:
 .
  If gregor is happy, then I'm happy :)
 .
 I concur with relicensing as needed, especially to align things with the Perl
 group's standard practices.
 .
 On Mon, Dec 9, 2019, 09:13 gregor herrmann <gregoa@debian.org> wrote:
 >
 > On Mon, 09 Dec 2019 01:35:06 +0000, Nick Morrott wrote:
 >
 > > You are both listed in debian/copyright for this package as having
 > > contributed previous packaging work under the Artistic/GPL-1+ licences.
 >
 > Which is a bit untypical, as usually we would use the superset of
 > $upstreamlicense and $perllicense, so "GPL-2+ or Artistic or GPL-1+".
 > Ok, GPL-2+ is already included in GPL-1+ :)
 >
 > > I want to confirm if you are happy to relicense your previous work on
 > > this specific package under the MIT/Expat licence to match upstream going
 > > forwards?
 >
 > Sure, I'm always happy use any DFSG-compatible license for my
 > contributions.
 > (And I'm fine with "Expat" or "Expat or Artistic or GPL-1+" or
 > whatever you choose in the end.)

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

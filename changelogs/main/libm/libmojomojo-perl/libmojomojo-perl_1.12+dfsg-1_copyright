Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MojoMojo
Upstream-Contact: Marcus Ramberg <marcus@nordaaker.com>
Source: https://metacpan.org/release/MojoMojo
Files-Excluded: root/static/flash/swfupload_f8.swf
 root/static/flash/swfupload_f9.swf
 root/static/flash/swfupload.swf
 root/static/js/jquery.js
 root/static/js/jquery.livequery.js
Comment: The upstream sources were repackaged in order to remove
 swfupload, which contains ActionScript that cannot be compiled
 using free tools. Additionally two minified javascript files are removed.

Files: *
Copyright: 2005-2010, Marcus Ramberg <marcus@nordaaker.com>
License: Artistic or GPL-1+

Files: lib/MojoMojo/Declaw.pm
Copyright: 2003-2009, The FastMail Partnership <http://www.fastmail.fm/>
License: Artistic or GPL-1+

Files: lib/Text/SmartyPants.pm
Copyright: 2003, John Gruber <gruber@fedora.net>
License: BSD-3-clause

Files: root/static/js/jquery.autocomplete.js
Copyright: 2007, Dylan Verheul
 2007, Dan G. Switzer
 2007, Anjesh Tuladhar
 2007, Jörn Zaefferer
License: MIT or GPL-2+
Comment: this file mentions it is dual-licensed under MIT
 and GPL licenses, but does not specify a version. therefore,
 the same-as-upstream terms are assumed.

Files: root/static/js/jquery.cookies.2.0.1.min.js
 debian/missing-sources/jquery.cookies.2.0.1.js
Copyright: 2005-2009, James Auldridge
License: BSD-3-clause or MIT or GPL-2+
Comment: This file's web site provides links to version 2 of
 the GNU GPL, the standard MIT license, and the above BSD
 license.
 .
 The unminified debian/missing-sources/jquery.cookies.2.0.1.js is from
 upstream svn:
  svn cat http://cookies.googlecode.com/svn/branches/2.0.1/jquery.cookies.js@14

Files: root/static/js/jquery.editinplace.js 
Copyright: 2009, Dave Hauenstein <davehauenstein@gmail.com>
License: BSD-3-clause

Files: root/static/js/prism.js root/static/css/prism.css
Copyright: 2012, Lea Verou
License: MIT
Comment: Information taken from https://github.com/PrismJS/prism/blob/gh-pages/LICENSE

Files: root/static/js/swfupload.js root/static/js/swfupload.queue.js
Copyright: 2007-2008, Jake Roberts
 2006-2007, Lars Huring
 2006-2007, Olov Nilzén
 2006-2007, Mammon Media
License: MIT

Files: script/util/dump_content.pl script/util/import_content.pl script/util/delete_inactive_users.pl
Copyright: 2010, Dan Dascalescu
License: Artistic or GPL-1+

Files: inc/File/Copy/Recursive.pm
Copyright: 2004, Daniel Muey <dmuey@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/Install/Catalyst.pm
Copyright: 2006-2009, various contributors to the Catalyst project
License: Artistic or GPL-1+
Comment: this file is included with the libcatalyst-devel-perl
 package. For a full list of copyright holders, please see copyright
 information for the libcatalyst-perl package.

Files: debian/*
Copyright: 2009-2010, Jonathan Yu <jawnsy@cpan.org>
 2011-2018, gregor herrmann <gregoa@debian.org>
 2012-2013, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
  * Neither the name "SmartyPants" nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 This software is provided by the copyright holders and contributors
 "as is" and any express or implied warranties, including, but not
 limited to, the implied warranties of merchantability and fitness
 for a particular purpose are disclaimed. In no event shall the
 copyright owner or contributors be liable for any direct, indirect,
 incidental, special, exemplary, or consequential damages (including,
 but not limited to, procurement of substitute goods or services;
 loss of use, data, or profits; or business interruption) however
 caused and on any theory of liability, whether in contract, strict
 liability, or tort (including negligence or otherwise) arising in
 any way out of the use of this software, even if advised of the
 possibility of such damage.

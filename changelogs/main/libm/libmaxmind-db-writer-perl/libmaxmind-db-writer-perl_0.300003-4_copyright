Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/MaxMind-DB-Writer
Upstream-Contact: Dave Rolsky <drolsky@maxmind.com> and others
Upstream-Name: MaxMind-DB-Writer

Files: *
Copyright: 2018 Olaf Alders <oalders@maxmind.com>, Greg Oschwald <goschwald@maxmind.com>, Dave Rolsky <drolsky@maxmind.com>, Mark Fowler <mfowler@maxmind.com>
License: Artistic or GPL-1+

Files: c/perl_math_int64.c c/perl_math_int128.c
Copyright: 2020 Salvador Fandino <sfandino@yahoo.com>, Dave Rolsky <autarch@urth.org>
License: public-domain
 This file is in the public domain

Files: c/windows_mman.c
Copyright: Steven Lee and others, https://github.com/witwall/mman-win32
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

Files: c/ppport.h
Copyright: 2004-2013, Marcus Holland-Moritz <mhx-cpan@gmx.net>
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files: inc/Capture/Tiny.pm
Copyright: 2009, David A. Golden <dagolden@cpan.org>
License: Apache-2.0

Files: inc/Config/AutoConf.pm
Copyright: 2004-2011, Alberto Simões, <ambs@cpan.org>
 2004-2011, Jens Rehsack, <rehsack@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2020, Federico Ceratto <federico@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Apache-2.0
 Licensed under Apache License, Version 2.0 (the "License"). You may not
 use this file except in compliance with the License. A copy of the
 License was distributed with this file or you may obtain a copy of the
 License from https://www.apache.org/licenses/LICENSE-2.0
 .
 Files produced as output though the use of this software, shall not be
 considered Derivative Works, but shall be considered the original work
 of the Licensor.
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License, Version 2.0
 can be found in `/usr/share/common-licenses/Apache-2.0'.

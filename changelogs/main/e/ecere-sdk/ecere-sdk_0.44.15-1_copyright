Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ecere-sdk
Upstream-Contact: Jerome St-Louis <jerome@ecere.com>
Source: http://ecere.org/

Files: *
Copyright: 1996-2016 Jérôme Jacovella-St-Louis
           2005-2016 Ecere Corporation
License: BSD-3-clause

Files: extras/md5.ec
Copyright: Copyright 2001 Alexander Peslyak
License: public-domain
 An eC adaptation of Alexander Peslyak's public domain MD5 implementation
 .
 Author:
 Alexander Peslyak, better known as Solar Designer <solar at openwall.com>
 .
 This software was written by Alexander Peslyak in 2001.  No copyright is
 claimed, and the software is hereby placed in the public domain.
 In case this attempt to disclaim copyright and place the software in the
 public domain is deemed null and void, then the software is
 Copyright (c) 2001 Alexander Peslyak and it is hereby released to the
 general public under the following terms:
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted.
 .
 There's ABSOLUTELY NO WARRANTY, express or implied.
 .
 (This is a heavily cut-down "BSD license".)

Files: extras/sha256.ec
Copyright: Copyright 2009  Gabriel A. Petursson
License: BSD-3-clause

Files: ecere/src/gfx/drivers/harfbuzz/*
Copyright: Harfbuzz Project & Contributors
License: MIT
 HarfBuzz is licensed under the so-called "Old MIT" license.  Details follow.
 .
 Copyright © 2011 Codethink Limited
 Copyright © 2010,2011  Google, Inc.
 Copyright © 2006  Behdad Esfahbod
 Copyright © 2009  Keith Stribley
 Copyright © 2009  Martin Hosken and SIL International
 Copyright © 2007  Chris Wilson
 Copyright © 2004,2007,2008,2009,2010  Red Hat, Inc.
 Copyright © 1998-2004  David Turner and Werner Lemberg
 .
 For full copyright notices consult the individual files in the package.
 .
 .
 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the
 above copyright notice and the following two paragraphs appear in
 all copies of this software.
 .
 IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.
 .
 THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 ON AN "AS IS" BASIS, AND THE COPYRIGHT HOLDER HAS NO OBLIGATION TO
 PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

Files: ecere/res/*/*.png
Copyright: (some) Tango Project
License: public-domain
 Terms Of Use
 .
 The Tango base icon theme is released to the Public Domain. The palette is in public domain.
 Developers, feel free to ship it along with your application.
 .
 Though the tango-icon-theme package is released to the Public Domain, we ask that you still
 please attribute the Tango Desktop Project, for all the hard work we've done. Thanks.

Files: eda/drivers/sqlite/sqlite3.c eda/drivers/sqlite/sqlite3.h eda/drivers/sqlite/shell.c
Copyright: SQLite Project
License: public-domain
 /************** Begin file sqliteInt.h ***************************************/
 /*
 ** 2001 September 15
 **
 ** The author disclaims copyright to this source code.  In place of
 ** a legal notice, here is a blessing:
 **
 **    May you do good and not evil.
 **    May you find forgiveness for yourself and forgive others.
 **    May you share freely, never taking more than you give.
 **
 *************************************************************************

Files: eda/drivers/sqliteCipher/sqlite3.c eda/drivers/sqliteCipher/sqlite3.h
Copyright: 2008-2010 Zetetic LLC
License: BSD-3-clause

License: BSD-3-clause
    All rights reserved.
 .
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
     * Neither the name of Ecere Corporation nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.
 .
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

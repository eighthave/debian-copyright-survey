Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: esorex
Source: https://www.eso.org/sci/software/cpl/download.html
Upstream-Author: Derek McKay <dmckay@eso.org>,
 Michael Kiesgen <mkiesgen@eso.org>,
 Neil Ferguson <nferguso@eso.org>,
 Ralf Palsa <rpalsa@eso.org

Files: *
Copyright: Copyright (C) 2001-2016 European Southern Observatory
 2011-2016 Ole Streicher <olebole@debian.org> (Debian files)
License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: */Makefile.in aclocal.m4 configure src/getopt*
Copyright: Copyright (C) 1994-2009 Free Software Foundation, Inc.
License: Free
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

Files: libltdl/*
Copyright: Copyright (C) 1994-2009 Free Software Foundation, Inc.
License: LGPL-2+
 GNU Libltdl is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 As a special exception to the GNU Lesser General Public License,
 if you distribute this file as part of a program or library that
 is built using GNU Libtool, you may include this file under the
 same distribution terms that you use for the rest of that program.
 .
 GNU Libltdl is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: src/getopt*
Copyright: Copyright (C) 1994-2009 Free Software Foundation, Inc.
License: LGPL-2.1
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

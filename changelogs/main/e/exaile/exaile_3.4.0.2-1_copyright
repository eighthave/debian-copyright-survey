Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: exaile
Source: http://www.exaile.org/

Files: *
Copyright: 2008-2010 Adam Olsen <arolsen@gmail.com>
License: GPL-2.0+

Files: plugins/alarmclock/*
Copyright: 2009 Adam Olsen <arolsen@gmail.com>
           2009 François Martorello <francois.martorello@gmail.com>
License: GPL-1.0+

Files: plugins/bookmarks/*
Copyright: 2009-2010 Brian Parma
License: GPL-2.0+

Files: plugins/contextinfo/*
Copyright: 2010 Guillaume Lecomte <guillaume86@gmail.com>
License: GPL-1.0+

Files: plugins/contextinfo/pylast.py
Copyright: 2008-2009 Amr Hassan
License: GPL-2.0+

Files: plugins/ipconsole/*
Copyright: 2009-2010 Brian Parma <execrable@gmail.com>
License: GPL-2.0+

Files: plugins/ipconsole/ipython_view.py
Copyright: 2007 IBM Corporation
License: BSD-3-clause

Files: plugins/ipod/*
Copyright: 2009 Andrew Stormont <astormont@svn.gnome.org>
License: GPL-3.0+

Files: plugins/librivox/*
Copyright: 2009 Arunas Radzvilavicius <arunas.rv@gmail.com>
License: GPL-3.0+

Files: plugins/moodbar/*
Copyright: 2009-2010 Solyianov Michael <crantisz@gmail.com>
License: GPL-3.0+

Files: plugins/mpris/*
Copyright: 2009-2010 Abhishek Mukherjee <abhishek.mukher.g@gmail.com>
License: GPL-3.0+

Files: plugins/multialarmclock/*
Copyright: 2006 Adam Olsen <arolsen@gmail.com>
           2009 Brian Parma <execrable@gmail.com>
License: GPL-1.0+

Files: plugins/notify/*
Copyright: 2009-2010 Abhishek Mukherjee <abhishek.mukher.g@gmail.com>
License: GPL-1.0+

Files: plugins/notifyosd/*
Copyright: 2009-2010 Adam Olsen <arolsen@gmail.com>
           2009-2010 Abhishek Mukherjee <abhishek.mukher.g@gmail.com>
           2009-2010 Steve Dodier <sidnioulzg@gmail.com>
License: GPL-1.0+

Files: plugins/playlistanalyzer/ext/d3.min.js
Copyright: 2010-2014 Michael Bostock
License: BSD-3-clause

Files: plugins/podcasts/_feedparser.py
Copyright: 2002-2006 Mark Pilgrim
License: BSD-2-clause

Files: debian/*
Copyright: 2006 François Févotte <francois.fevotte@ensta.org>
           2006-2009 Adam Cécile (Le_Vert) <gandalf@le-vert.net>
           2011 Andrew Starr-Bochicchio <a.starr.b@gmail.com>
           2011 Vincent Cheng <vcheng@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-1.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 1 can be found in "/usr/share/common-licenses/GPL-1".

License: GPL-3.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: BSD-2-clause
 Copyright (c) 2002-2006, Mark Pilgrim
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS'
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Copyright (c) 2007, IBM Corporation
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of IBM Corporation nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS'
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Eigenbase Farago
Source: http://sourceforge.net/projects/farrago/files/

Files: *
Copyright: 2002-2009, The Eigenbase Project
           2002-2009, SQLstream, Inc.
           2002-2009, LucidEra, Inc.
           2002-2009, John V. Sichi
           2005-2009, Xiaoyang Luo
License: GPL-2+ with linking GPL-incompatible libraries exception

Files: debian/*
Copyright: 2008-2009, Damien Raude-Morvan <drazzib@debian.org>
License: GPL-2+
 On Debian systems you can find the complete text of the
 GPL v2 license in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+ with linking GPL-incompatible libraries exception
 This software is released under the GNU General Public License,
 which is reproduced below.
 .
 In addition, as a special exception, The Eigenbase Project gives
 permission to link the code of this program with the following
 GPL-incompatible libraries, and distribute linked combinations
 including all of these:
 .
   - MDR library from Netbeans (or modified versions of MDR that use
     the same license as MDR--the Sun Public License), including JMI
     interfaces from Sun Microsystems
 .
   - All standard Java runtime libraries distributed by Sun Microsystems
     as part of any release of J2SE or J2EE
 .
   - MOF, UML, and CWM model definitions from Object Management Group
 .
   - Apache Commons from the Apache Software Foundation
 .
   - Hibernate and supporting libraries from Red Hat Middleware
 .
 You must obey the GNU General Public License in all respects for all
 of the code used other than the libraries listed above.  If you modify
 this program, you may extend this exception to your version of the
 program, but you are not obligated to do so.  If you do not wish to do
 so, delete this exception statement from your version.
 .
 ======================================================
 .
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2 of the License, or (at your option)
 any later version approved by The Eigenbase Project.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the complete text of the
 GPL v2 license in `/usr/share/common-licenses/GPL-2'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ESO-MIDAS
Upstream-Author: Ralf Palsa <rpalsa@eso.org>, Lars Lundin <llundin@eso.org>
Source: http://www.eso.org/sci/software/esomidas/midas-distrib.html

Files: * debian/*
Copyright:
 1985-2011 European Southern Observatory,
 1990 David E. Smyth,
 1990 by Auto-trol Technology Corporation, Denver, Colorado,
 1990 Rodney J. Whitby,
 1990 Richard Hesketh / rlh2@ukc.ac.uk,
 1990-1991 GROUPE BULL,
 1994 Landessternwarte Heidelberg,
 1995 Mark Calabretta,
 1987-1992 Free Software Foundation, Inc,
 1992 European Southern Observatory and Warsaw Observatory,
 1990 Andrew T. Young,
 2014 Ole Streicher <debian@liska.ath.cx> (Debian packaging)
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
Notice: The web page claims that ESO-MIDAS
 <http://www.eso.org/sci/software/esomidas/> is a registered trademark.
 However, this is outdated, as the trademark ist cancelled since 1998.

Files: /gui/GraphLib/libsrc/uimxR5/*
Copyright: 1988-1992, Visual Edge Software Ltd,
 1993 Daniel Boulet and RTMX Inc.
License: MIT
 ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 distribute  this  software  and its documentation for any purpose
 and  without  fee  is  hereby  granted,  provided  that the above
 copyright  notice  appear  in  all  copies  and  that  both  that
 copyright  notice and this permission notice appear in supporting
 documentation,  and that  the name of Visual Edge Software not be
 used  in advertising  or publicity  pertaining to distribution of
 the software without specific, written prior permission. The year
 included in the notice is the year of the creation of the work.

Files: gui/GraphLib/libsrc/uimxR5/include/msg.h
Copyright: 1993 Daniel Boulet and RTMX Inc.
License: BSD
 This system call was implemented by Daniel Boulet under contract from RTMX.
 .
 Redistribution and use in source forms, with and without modification,
 are permitted provided that this entire comment appears intact.
 .
 Redistribution in binary form may occur without any restrictions.
 Obviously, it would be nice if you gave credit where credit is due
 but requiring it would be too onerous.
 .
 This software is provided ``AS IS'' without any warranties of any kind.

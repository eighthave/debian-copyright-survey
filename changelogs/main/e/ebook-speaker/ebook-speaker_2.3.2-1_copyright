Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: eBook-speaker
Source: http://web.inter.nl.net/users/lemmensj/
Upstream-Contact: Jos Lemmens <jos@jlemmens.nl>

Files: *
Copyright: © 2011-2013 Jos Lemmens <jos@jlemmens.nl>
License: GPL-2+

Files: ./error.wav
Copyright: © 2002 Damon Chaplin <damon@ximian.com>
Comment: The file error.wav is a copy of the file default_alarm.wav and
 comes from the evolution package.
License: LGPL-2 or LGPL-3

Files: ./ebook-speaker*.svg
Copyright: The Oxygen Icon Theme
    Copyright (C) 2007 Nuno Pinheiro <nuno@oxygen-icons.org>
    Copyright (C) 2007 David Vignoni <david@icon-king.com>
    Copyright (C) 2007 David Miller <miller@oxygen-icons.org>
    Copyright (C) 2007 Johann Ollivier Lapeyre <johann@oxygen-icons.org>
    Copyright (C) 2007 Kenneth Wimer <kwwii@bootsplash.org>
    Copyright (C) 2007 Riccardo Iaconelli <riccardo@oxygen-icons.org>
    and others
    copyright © 2012-2013 Paul Gevers <elbrus@debian.org>
Comment: These icons are based on the accessories-text-editor.svgz icon from
 the oxygen-icons-4.8-4 package.
License: LGPL-3+

Files: ./debian/*
Copyright: © 2011-2013 Paul Gevers <elbrus@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in /usr/share/common-licenses/GPL-2.

License: LGPL-2 or LGPL-3
 Evolution is licensed under the LGPLv2 or LGPLv3 license using
 following text:
 .
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) version 3.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Less General Public
 License, version 2, can be found in /usr/share/common-licenses/LGPL-2.

License: LGPL-3+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Less General Public
 License, version 3, can be found in /usr/share/common-licenses/LGPL-3.
Comment:
 The GNU Lesser General Public License or LGPL is written for
 software libraries in the first place. We expressly want the LGPL to
 be valid for this artwork library too.
 .
 KDE Oxygen theme icons is a special kind of software library, it is an
 artwork library, it's elements can be used in a Graphical User Interface, or
 GUI.
 .
 Source code, for this library means:
  - where they exist, SVG;
  - otherwise, if applicable, the multi-layered formats xcf or psd, or
    otherwise png.
 .
 The LGPL in some sections obliges you to make the files carry
 notices. With images this is in some cases impossible or hardly useful.
 .
 With this library a notice is placed at a prominent place in the directory
 containing the elements. You may follow this practice.
 .
 The exception in section 5 of the GNU Lesser General Public License covers
 the use of elements of this art library in a GUI.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: WWW-Search
Upstream-Contact: Martin Thurn <mthurn@cpan.org>
Source: https://metacpan.org/release/WWW-Search

Files: *
Copyright: 1996-1997, University of Southern California
License: USC

Files: inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 1996, 1999, Michael Alan Dorman <mdorman@debian.org>
 1999, Richard Braakman <dark@xs4all.nl>
 2001, 2002, 2004, Scott M. Dier <sdier@debian.org>
 2002, Don Armstrong <don@donarmstrong.com>
 2009, Ryan Niebur <ryan@debian.org>
 2011-2012, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

Files: altavista/*
Copyright: 1996-1998, University of Southern California
License: USC

Files: altavista/inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: altavista/lib/WWW/Search/AltaVista/AdvancedWeb.pm
Copyright:
 1999, Jim Smyser & USC/ISI
 1996, University of Southern California.
License: USC

Files: altavista/lib/WWW/Search/AltaVista/NL.pm
Copyright:
 2001, Different Soft
 1996-1998 University of Southern California.
License: USC

Files: pagesjaunes/*
Copyright: 2002, Briac Pilpré
License: Artistic or GPL-1+

Files: yahoo/*
Copyright: 1998-2009, Martin 'Kingpin' Thurn
License: Artistic or GPL-1+

Files: yahoo/inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: ebay/*
Copyright: 1998-2015, Martin 'Kingpin' Thurn
License: Artistic or GPL-1+

Files: ebay/inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: googlism/*
Copyright: 2002, xern <xern@cpan.org>
License: Artistic

Files: jobserve/*
Copyright: 2003, Andy Pritchard
License: Artistic or GPL-1+

Files: news/*
Copyright: 1998-2009, Martin 'Kingpin' Thurn
License: Artistic or GPL-1+

Files: news/inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: firstgov/*
Copyright: Dennis Sutch, <dsutch@doc.gov>
License: Artistic or GPL-1+

Files: lycos/*
Copyright: 1996-1998, USC/ISI
License: Artistic or GPL-1+

Files: lycos/inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

License: USC
 Redistribution and use in source and binary forms are permitted
 provided that the above copyright notice and this paragraph are
 duplicated in all such forms and that any documentation, advertising
 materials, and other materials related to such distribution and use
 acknowledge that the software was developed by the University of
 Southern California, Information Sciences Institute.  The name of the
 University may not be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

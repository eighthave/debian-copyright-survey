Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Web-Simple
Upstream-Contact: Matt S. Trout <mst@shadowcat.co.uk>
Source: https://metacpan.org/release/Web-Simple

Files: *
Copyright: 2010-2011, Matt S. Trout <mst@shadowcat.co.uk>
 2011, Devin Austin (dhoss) <dhoss@cpan.org>
 2011, Arthur Axel 'fREW' Schmidt <frioux@gmail.com>
 2011, gregor herrmann (gregoa) <gregoa@debian.org>
 2011, John Napiorkowski (jnap) <jjn1056@yahoo.com>
 2011, Josh McMichael <jmcmicha@linus222.gsc.wustl.edu>
 2011, Justin Hunter (arcanez) <justin.d.hunter@gmail.com>
 2011, Kjetil Kjernsmo <kjetil@kjernsmo.net>
 2011, markie <markie@nulletch64.dreamhost.com>
 2011, Christian Walde (Mithaldu) <walde.christian@googlemail.com>
 2011, nperez <nperez@cpan.org>
 2011, Robin Edwards <robin.ge@gmail.com>
 2011, Andrew Rodland (hobbs) <andrew@cleverdomain.org>
 2011, Robert Sedlacek (phaylon) <r.sedlacek@shadowcat.co.uk>
 2011, Hakim Cassimally (osfameron) <osfameron@cpan.org>
 2011, Karen Etheridge (ether) <ether@cpan.org>
License: Artistic or GPL-1+

Files: lib/Web/Simple/AntiquatedPerl.pod
Copyright: 2010-2011, Matt S. Trout <mst@shadowcat.co.uk>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009-2017, gregor herrmann <gregoa@debian.org>
 2011-2012, Fabrizio Regalli <fabreg@fabreg.it>
 2012, Nuno Carvalho <smash@cpan.org>
 2016, Angel Abad <angel@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

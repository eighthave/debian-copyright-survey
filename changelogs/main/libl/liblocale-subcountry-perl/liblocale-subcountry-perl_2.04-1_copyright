Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Locale-SubCountry
Upstream-Contact: Kim Ryan <kimryan@cpan.org>
Source: https://metacpan.org/release/Locale-SubCountry

Files: *
Copyright: 2011-2018, Kim Ryan <kimryan@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2002-2004, Ardo van Rangelrooij <ardo@debian.org>
 2004-2005, Gunnar Wolf <gwolf@debian.org>
 2004, Marc 'HE' Brockschmidt <he@debian.org>
 2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2006, Niko Tyni <ntyni@debian.org>
 2007, Joey Hess <joeyh@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2008-2018, gregor herrmann <gregoa@debian.org>
 2011, Jonathan Yu <jawnsy@cpan.org>
 2016, Lucas Kanashiro <kanashiro.duarte@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/samtools/htsjdk/releases
Comment: Convenience binary jar files are removed.
Files-Excluded: gradle/wrapper/gradle-wrapper.jar

Files: *
Copyright: 2009-2018, The Broad Institute
License: MIT

Files: debian/*
Copyright: 2010 Shaun Jackman <sjackman@debian.org>
           2013-2018 Olivier Sallou <osallou@debian.org>
           2015 Vincent Danjean <vdanjean@debian.org>
License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

Files: scripts/*
  src/main/*
Copyright: 2009-2018, The Broad Institute
License: Expat

Files: src/main/java/htsjdk/samtools/AsyncSAMFileWriter.java
  src/main/java/htsjdk/samtools/BAMIteratorFilter.java
  src/main/java/htsjdk/samtools/BAMQueryMultipleIntervalsIteratorFilter.java
  src/main/java/htsjdk/samtools/BamFileIoUtils.java
  src/main/java/htsjdk/samtools/BrowseableBAMIndex.java
  src/main/java/htsjdk/samtools/CRAMCRAIIndexer.java
  src/main/java/htsjdk/samtools/CRAMContainerStreamWriter.java
  src/main/java/htsjdk/samtools/Chunk.java
  src/main/java/htsjdk/samtools/DefaultSAMRecordFactory.java
  src/main/java/htsjdk/samtools/Defaults.java
  src/main/java/htsjdk/samtools/QueryInterval.java
  src/main/java/htsjdk/samtools/SAMRecordFactory.java
  src/main/java/htsjdk/samtools/SamFiles.java
  src/main/java/htsjdk/samtools/SamIndexes.java
  src/main/java/htsjdk/samtools/SamInputResource.java
  src/main/java/htsjdk/samtools/SamReader.java
  src/main/java/htsjdk/samtools/SamReaderFactory.java
  src/main/java/htsjdk/samtools/SamStreams.java
  src/main/java/htsjdk/samtools/SecondaryOrSupplementarySkippingIterator.java
  src/main/java/htsjdk/samtools/StreamInflatingIndexingOutputStream.java
  src/main/java/htsjdk/samtools/ValidationStringency.java
  src/main/java/htsjdk/samtools/util/AbstractAsyncWriter.java
  src/main/java/htsjdk/samtools/util/AbstractProgressLogger.java
  src/main/java/htsjdk/samtools/util/CodeUtil.java
  src/main/java/htsjdk/samtools/util/ComparableTuple.java
  src/main/java/htsjdk/samtools/util/CoordSpanInputSteam.java
  src/main/java/htsjdk/samtools/util/DelegatingIterator.java
  src/main/java/htsjdk/samtools/util/HttpUtils.java
  src/main/java/htsjdk/samtools/util/IterableAdapter.java
  src/main/java/htsjdk/samtools/util/IterableOnceIterator.java
  src/main/java/htsjdk/samtools/util/Iterables.java
  src/main/java/htsjdk/samtools/util/Lazy.java
  src/main/java/htsjdk/samtools/util/Locatable.java
  src/main/java/htsjdk/samtools/util/LocationAware.java
  src/main/java/htsjdk/samtools/util/PositionalOutputStream.java
  src/main/java/htsjdk/samtools/util/ProgressLogger.java
  src/main/java/htsjdk/samtools/util/QualityEncodingDetector.java
  src/main/java/htsjdk/samtools/util/RelativeIso8601Date.java
  src/main/java/htsjdk/samtools/util/Tuple.java
  src/main/java/htsjdk/samtools/sra/ReferenceCache.java
  src/main/java/htsjdk/samtools/sra/SRAIndexedSequenceFile.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/samtools/CRAMBAIIndexer.java
Copyright: 2013, EMBL-EBI
License: Apache-2.0 or Expat

Files: src/main/java/htsjdk/samtools/CRAMFileReader.java
  src/main/java/htsjdk/samtools/CRAMFileWriter.java
  src/main/java/htsjdk/samtools/CRAMIterator.java
Copyright: 2013, EMBL-EBI
License: Apache-2.0

Files: src/main/java/htsjdk/samtools/ChainedDownsamplingIterator.java
  src/main/java/htsjdk/samtools/ConstantMemoryDownsamplingIterator.java
  src/main/java/htsjdk/samtools/DownsamplingIteratorFactory.java
  src/main/java/htsjdk/samtools/HighAccuracyDownsamplingIterator.java
  src/main/java/htsjdk/samtools/SAMRecordQueryHashComparator.java
Copyright: 2015, 2016, Tim Fennell
License: Expat

Files: src/main/java/htsjdk/samtools/SAMFlag.java
Copyright: 2015 Pierre Lindenbaum
License: Expat

Files: src/main/java/htsjdk/samtools/SRAFileReader.java
  src/main/java/htsjdk/samtools/SRAIndex.java
  src/main/java/htsjdk/samtools/SRAIterator.java
  src/main/java/htsjdk/samtools/sra/*
Copyright: Public Domain
License: Public-Domain

Files: src/main/java/htsjdk/samtools/SamFlagField.java
Copyright: 2016, Nils Homer
License: Expat

Files: src/main/java/htsjdk/samtools/cram/*
Copyright: 2013, EMBL-EBI
License: Apache-2.0

Files: src/main/java/htsjdk/samtools/fastq/AsyncFastqWriter.java
  src/main/java/htsjdk/samtools/fastq/FastqWriter.java
  src/main/java/htsjdk/samtools/fastq/FastqWriterFactory.java
  src/main/java/htsjdk/samtools/filter/InsertSizeFilter.java
  src/main/java/htsjdk/samtools/filter/MappingQualityFilter.java
  src/main/java/htsjdk/samtools/filter/SecondaryAlignmentFilter.java
  src/main/java/htsjdk/samtools/filter/SecondaryOrSupplementaryFilter.java
  src/main/java/htsjdk/samtools/seekablestream/ByteArraySeekableStream.java
  src/main/java/htsjdk/samtools/seekablestream/ISeekableStreamFactory.java
  src/main/java/htsjdk/samtools/seekablestream/SeekableMemoryStream.java
  src/main/java/htsjdk/samtools/seekablestream/SeekablePathStream.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/samtools/filter/AbstractJavascriptFilter.java
  src/main/java/htsjdk/samtools/filter/JavascriptSamRecordFilter.java
Copyright: 2015, Pierre Lindenbaum @yokofakun Institut du Thorax - Nantes - France
License: Expat

Files: src/main/java/htsjdk/samtools/seekablestream/SeekableFTPStream.java
  src/main/java/htsjdk/samtools/seekablestream/SeekableFTPStreamHelper.java
  src/main/java/htsjdk/samtools/seekablestream/SeekableFileStream.java
Copyright: 2007-2011, The Broad Institute of MIT and Harvard.
License: LGPL-2.1

Files: src/main/java/htsjdk/samtools/util/AsyncBufferedIterator.java
Copyright: 2016, Daniel Cameron
License: Expat

Files: src/main/java/htsjdk/samtools/util/DateParser.java
Copyright: MIT, INRIA and Keio, 2000, W3C IPR SOFTWARE NOTICE / 1995-1998, World Wide Web Consortium, (Massachusetts Institute of
License: W3C-Software

Files: src/main/java/htsjdk/samtools/util/Murmur3.java
Copyright: 2011, The Guava Authors
License: Apache-2.0

Files: src/main/java/htsjdk/samtools/util/RuntimeScriptException.java
Copyright: 2009-2015, The Broad Institute
License: Expat

Files: src/main/java/htsjdk/samtools/util/TrimmingUtil.java
Copyright: 2015, 2016, Tim Fennell
License: Expat

Files: src/main/java/htsjdk/samtools/util/ftp/*
Copyright: 2007-2011, The Broad Institute of MIT and Harvard.
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/AbstractFeatureReader.java
  src/main/java/htsjdk/tribble/AsciiFeatureCodec.java
  src/main/java/htsjdk/tribble/FeatureCodec.java
  src/main/java/htsjdk/tribble/FeatureReader.java
Copyright: 2007-2010, The Broad Institute, Inc. and the Massachusetts Institute of Technology
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/BinaryFeatureCodec.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/tribble/CloseableTribbleIterator.java
Copyright: 2009, 2010, The Broad Institute, Inc
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/index/AbstractIndex.java
Copyright: 2007-2010, The Broad Institute, Inc. and the Massachusetts Institute of Technology
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/index/interval/*
Copyright: 2007-2010, The Broad Institute, Inc. and the Massachusetts Institute of Technology
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/index/linear/*
Copyright: 2009, 2010, The Broad Institute, Inc
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/index/linear/LinearIndexCreator.java
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/main/java/htsjdk/tribble/readers/AsciiLineReader.java
  src/main/java/htsjdk/tribble/readers/PositionalBufferedStream.java
Copyright: 2007-2010, The Broad Institute, Inc. and the Massachusetts Institute of Technology
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/readers/AsciiLineReaderIterator.java
  src/main/java/htsjdk/tribble/readers/LineIterator.java
  src/main/java/htsjdk/tribble/readers/LineIteratorImpl.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/tribble/util/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/tribble/util/HTTPHelper.java
  src/main/java/htsjdk/tribble/util/URLHelper.java
Copyright: 2007-2011, The Broad Institute of MIT and Harvard.
License: LGPL-2.1

Files: src/main/java/htsjdk/tribble/util/MathUtils.java
  src/main/java/htsjdk/tribble/util/ParsingUtils.java
  src/main/java/htsjdk/tribble/util/TabixUtils.java
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/main/java/htsjdk/tribble/util/popgen/*
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/main/java/htsjdk/variant/variantcontext/GenotypeJEXLContext.java
  src/main/java/htsjdk/variant/variantcontext/JEXLMap.java
  src/main/java/htsjdk/variant/variantcontext/VariantContextComparator.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/variant/variantcontext/filter/JavascriptVariantFilter.java
Copyright: 2015, Pierre Lindenbaum @yokofakun Institut du Thorax - Nantes - France
License: Expat

Files: src/main/java/htsjdk/variant/variantcontext/writer/AsyncVariantContextWriter.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/main/java/htsjdk/variant/vcf/VCFEncoder.java
  src/main/java/htsjdk/variant/vcf/VCFFileReader.java
  src/main/java/htsjdk/variant/vcf/VCFRecordCodec.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/*
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/samtools/AbstractBAMFileIndexTest.java
  src/test/java/htsjdk/samtools/BAMCigarOverflowTest.java
  src/test/java/htsjdk/samtools/BAMQueryMultipleIntervalsIteratorFilterTest.java
  src/test/java/htsjdk/samtools/CRAMBAIIndexerTest.java
  src/test/java/htsjdk/samtools/CRAMCRAIIndexerTest.java
  src/test/java/htsjdk/samtools/CRAMComplianceTest.java
  src/test/java/htsjdk/samtools/CRAMContainerStreamWriterTest.java
  src/test/java/htsjdk/samtools/CRAMEdgeCasesTest.java
  src/test/java/htsjdk/samtools/CRAMFileBAIIndexTest.java
  src/test/java/htsjdk/samtools/CRAMFileCRAIIndexTest.java
  src/test/java/htsjdk/samtools/CRAMFileWriterWithIndexTest.java
  src/test/java/htsjdk/samtools/DownsamplingIteratorTests.java
  src/test/java/htsjdk/samtools/DuplicateSetIteratorTest.java
  src/test/java/htsjdk/samtools/GenomicIndexUtilTest.java
  src/test/java/htsjdk/samtools/SAMBinaryTagAndValueUnitTest.java
  src/test/java/htsjdk/samtools/SamFilesTest.java
  src/test/java/htsjdk/samtools/SamFlagFieldTest.java
  src/test/java/htsjdk/samtools/SamIndexesTest.java
  src/test/java/htsjdk/samtools/SamReaderFactoryTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/SAMFlagTest.java
  src/test/java/htsjdk/samtools/SAMSequenceDictionaryTest.java
Copyright: 2015 Pierre Lindenbaum
License: Expat

Files: src/test/java/htsjdk/samtools/cram/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/cram/structure/ReadTagTest.java
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/samtools/fastq/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/filter/InsertSizeFilterTest.java
  src/test/java/htsjdk/samtools/filter/MappingQualityFilterTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/filter/JavascriptSamRecordFilterTest.java
Copyright: 2015, Pierre Lindenbaum @yokofakun Institut du Thorax - Nantes - France
License: Expat

Files: src/test/java/htsjdk/samtools/metrics/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/metrics/MetricsFileTest.java
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/samtools/reference/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/reference/FastaSequenceFileTest.java
  src/test/java/htsjdk/samtools/reference/FastaSequenceIndexTest.java
  src/test/java/htsjdk/samtools/reference/ReferenceSequenceTests.java
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/samtools/seekablestream/SeekableStreamFactoryTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/sra/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/util/AsyncBufferedIteratorTest.java
Copyright: 2016, Daniel Cameron
License: Expat

Files: src/test/java/htsjdk/samtools/util/CloseableIteratorTest.java
  src/test/java/htsjdk/samtools/util/CodeUtilTest.java
  src/test/java/htsjdk/samtools/util/ComparableTupleTest.java
  src/test/java/htsjdk/samtools/util/CoordSpanInputSteamTest.java
  src/test/java/htsjdk/samtools/util/HistogramTest.java
  src/test/java/htsjdk/samtools/util/OverlapDetectorTest.java
  src/test/java/htsjdk/samtools/util/QualityEncodingDetectorTest.java
  src/test/java/htsjdk/samtools/util/RelativeIso8601DateTest.java
  src/test/java/htsjdk/samtools/util/SolexaQualityConverterTest.java
  src/test/java/htsjdk/samtools/util/TupleTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/samtools/util/TrimmingUtilTest.java
Copyright: 2015, 2016, Tim Fennell
License: Expat

Files: src/test/java/htsjdk/tribble/bed/*
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/tribble/index/*
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/tribble/index/IndexTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/tribble/index/interval/*
Copyright: 2007-2010, The Broad Institute, Inc. and the Massachusetts Institute of Technology
License: LGPL-2.1

Files: src/test/java/htsjdk/tribble/index/linear/*
Copyright: 2009, 2010, The Broad Institute, Inc
License: LGPL-2.1

Files: src/test/java/htsjdk/variant/*
Copyright: 2009-2016, The Broad Institute
License: Expat

Files: src/test/java/htsjdk/variant/variantcontext/VariantContextBuilderTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/variant/variantcontext/filter/CompoundFilterTest.java
  src/test/java/htsjdk/variant/variantcontext/filter/PassingVariantFilterTest.java
  src/test/java/htsjdk/variant/variantcontext/filter/SnpFilterTest.java
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/variant/variantcontext/filter/JavascriptVariantFilterTest.java
Copyright: 2015, Pierre Lindenbaum @yokofakun Institut du Thorax - Nantes - France
License: Expat

Files: src/test/java/htsjdk/variant/vcf/*
Copyright: 2009-2015, The Broad Institute
License: MIT

Files: src/test/java/htsjdk/variant/vcf/IndexFactoryUnitTest.java
  src/test/java/htsjdk/variant/vcf/VCFCompoundHeaderLineUnitTest.java
  src/test/java/htsjdk/variant/vcf/VCFHeaderUnitTest.java
  src/test/java/htsjdk/variant/vcf/VCFStandardHeaderLinesUnitTest.java
Copyright: 2009-2016, The Broad Institute
License: Expat

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment: On Debian systems, the complete text of the Apache License version 2.0
 is available in the file ‘/usr/share/common-licenses/Apache-2.0’.

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: LGPL-2.1
 Quoted from ‘/usr/share/common-licenses/LGPL-2.1’:
 .
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: W3C-Software
 W3C IPR SOFTWARE NOTICE
 .
 Copyright 1995-1998 World Wide Web Consortium, (Massachusetts Institute of
 Technology, Institut National de Recherche en Informatique et en
 Automatique, Keio University). All Rights Reserved.
 http://www.w3.org/Consortium/Legal/
 .
 This W3C work (including software, documents, or other related items) is
 being provided by the copyright holders under the following license. By
 obtaining, using and/or copying this work, you (the licensee) agree that you
 have read, understood, and will comply with the following terms and
 conditions:
 .
 Permission to use, copy, and modify this software and its documentation,
 with or without modification, for any purpose and without fee or royalty is
 hereby granted, provided that you include the following on ALL copies of the
 software and documentation or portions thereof, including modifications,
 that you make:
 .
   1. The full text of this NOTICE in a location viewable to users of the
      redistributed or derivative work.
   2. Any pre-existing intellectual property disclaimers, notices, or terms
      and conditions. If none exist, a short notice of the following form
      (hypertext is preferred, text is permitted) should be used within the
      body of any redistributed or derivative code: "Copyright World Wide
      Web Consortium, (Massachusetts Institute of Technology, Institut
      National de Recherche en Informatique et en Automatique, Keio
      University). All Rights Reserved. http://www.w3.org/Consortium/Legal/"
   3. Notice of any changes or modifications to the W3C files, including the
      date changes were made. (We recommend you provide URIs to the location
      from which the code is derived).
 .
 In addition, creators of derivative works must include the full text of this
 NOTICE in a location viewable to users of the derivative work.
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS
 MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR
 PURPOSE OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE
 ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR
 DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in advertising
 or publicity pertaining to the software without specific, written prior
 permission. Title to copyright in this software and any associated
 documentation will at all times remain with copyright holders.

License: Public-Domain
 This software/database is a "United States Government Work" under the
 terms of the United States Copyright Act.  It was written as part of
 the author's official duties as a United States Government employee and
 thus cannot be copyrighted.  This software/database is freely available
 to the public for use. The National Library of Medicine and the U.S.
 Government have not placed any restriction on its use or reproduction.
 .
 Although all reasonable efforts have been taken to ensure the accuracy
 and reliability of the software and data, the NLM and the U.S.
 Government do not and cannot warrant the performance or results that
 may be obtained by using this software or data. The NLM and the U.S.
 Government disclaim all warranties, express or implied, including
 warranties of performance, merchantability or fitness for any particular
 purpose.
 .
 Please cite the author in any work or product based on this material.

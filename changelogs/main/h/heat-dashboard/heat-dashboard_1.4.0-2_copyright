Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: heat-dashboard
Source: https://github.com/openstack/heat-dashboard.git

Files: *
Copyright: (c) 2013-2015, Hewlett-Packard Development Company, L.P.
           (c) 2017, OpenStack Foundation
           (c) 2012, Nebula, Inc.
           (c) 2013, Big Switch Networks
           (c) 2012, US Gov. as represented by the NASA admin
           (c) 2014, Rackspace, US, Inc.
           (c) 2017, NTT Communications.
License: Apache-2.0

Files: heat_dashboard/static/dashboard/project/heat_dashboard/template_generator/js/vendors/vis.js
Copyright: (c) 2011-2014, Almende B.V, http://almende.com
           (c) 2014, Jorik Tangelder
           (c) 2010-2012 Robert Kieffer
           (c) 2011, Andrei Mackenzie
License: Apache-2.0+Expat

Files: debian/*
Copyright: (c) 2018, Thomas Goirand <zigo@debian.org>
           (c) 2018-2019, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Apache-2.0+Expat
 Vis.js is dual licensed under both
 .
  The Apache 2.0 License
  http://www.apache.org/licenses/LICENSE-2.0
 .
 and
 .
  The MIT License
  http://opensource.org/licenses/MIT

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

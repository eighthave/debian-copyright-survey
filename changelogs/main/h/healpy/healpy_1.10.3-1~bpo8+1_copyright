Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Healpy
Upstream-Contact: A. Zonca <zonca@deepspace.ucsb.edu>
Upstream-Source: https://github.com/healpy/healpy

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: *
Copyright: 2014 A. Zonca <zonca@deepspace.ucsb.edu>
           2014 C. Rosset <cyrille.rosset@apc.univ-paris-diderot.fr>
License: GPL-2+

Files: healpixsubmodule/src/cxx/autotools/*
Copyright: 1997-2013 HEALPix collaboration
           1998-2013 Anthony J. Banday <banday@mpa-garching.mpg.de>
           1998-2003 Matthias Bartelmann <mbartelmann@ita.uni-heidelberg.de>
           2005-2008 Hans Kristian Eriksen <h.k.k.eriksen@astro.uio.no>
           1997-2013 Krzysztof M. Gorski <krzysztof.m.gorski@jpl.nasa.gov>
           1999-2000 Frode K. Hansen <fhansen@mpa-garching.mpg.de>
           1997-2013 Eric Hivon <hivon@iap.fr>
           2005-2013 William O'Mullane <womullan@skysrv.pha.jhu.edu>
           2003-2013 Martin Reinecke <martin@mpa-garching.mpg.de> and
           1998-2003 Benjamin D. Wandelt <bwandelt@iap.fr>.
License: GPL-2+

Files: debian/*
Copyright: 2012 Leo Singer <leo.singer@ligo.org>
License: GPL-2+

Files: cfitsio/*
Copyright: None
 Copyright (Unpublished--all rights reserved under the copyright laws of
 the United States), U.S. Government as represented by the Administrator
 of the National Aeronautics and Space Administration.  No copyright is
 claimed in the United States under Title 17, U.S. Code.
License: public-domain
 Permission to freely use, copy, modify, and distribute this software
 and its documentation without fee is hereby granted, provided that this
 copyright notice and disclaimer of warranty appears in all copies.
 .
 DISCLAIMER:
 .
 THE SOFTWARE IS PROVIDED 'AS IS' WITHOUT ANY WARRANTY OF ANY KIND,
 EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED TO,
 ANY WARRANTY THAT THE SOFTWARE WILL CONFORM TO SPECIFICATIONS, ANY
 IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE, AND FREEDOM FROM INFRINGEMENT, AND ANY WARRANTY THAT THE
 DOCUMENTATION WILL CONFORM TO THE SOFTWARE, OR ANY WARRANTY THAT THE
 SOFTWARE WILL BE ERROR FREE.  IN NO EVENT SHALL NASA BE LIABLE FOR ANY
 DAMAGES, INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, SPECIAL OR
 CONSEQUENTIAL DAMAGES, ARISING OUT OF, RESULTING FROM, OR IN ANY WAY
 CONNECTED WITH THIS SOFTWARE, WHETHER OR NOT BASED UPON WARRANTY,
 CONTRACT, TORT , OR OTHERWISE, WHETHER OR NOT INJURY WAS SUSTAINED BY
 PERSONS OR PROPERTY OR OTHERWISE, AND WHETHER OR NOT LOSS WAS SUSTAINED
 FROM, OR AROSE OUT OF THE RESULTS OF, OR USE OF, THE SOFTWARE OR
 SERVICES PROVIDED HEREUNDER.

Files: cfitsio/zlib/*
Copyright: 1995-2010 Jean-loup Gailly <jloup@gzip.org>
           1995-2010 Mark Adler <madler@alumni.caltech.edu>
License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

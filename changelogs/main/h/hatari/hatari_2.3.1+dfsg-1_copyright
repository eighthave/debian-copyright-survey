Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://hatari.tuxfamily.org/
Comment: While src/tos.img is licensed under the GPL, source code for
 it is not included with hatari
Files-Excluded: src/tos.img src/hatari_21.msa

Files: *
Copyright: 1995, Alessandro Bissacco
           1996, Herman ten Brugge
           1999, Sam Jordan
           1995-2002, Bernd Schmidt
           1997-2008, Christian Bauer
           1998, Gilles Vollant
           1999-2003, Ian Piumarta
           2000-2002, Bernd Meyer
           2000-2005, Gwenole Beauchesne
           2003, Fabrice Bellard
           2004-2005, Richard Drummond
           2005, Peter Keunecke
           2006 Openedhand Ltd.
           2001-2008, ARAnyM developer team
           2008-2009, Jean-Baptiste Berlioz <jb.berlioz@freemind-tobe.com>
           2011, Markus Heiden <markus@markusheiden.de>
           2003-2011, Matthias Arndt <marndt@asmsoftware.de>
           2014-2015, Frode Solheim <frode@fs-uae.net>
           2006-2016, Sébastien Molines <clafou@gmail.com>
           2017-2020, Thorsten Otto admin@tho-otto.de
           2018-2019, Miro Kropacek <miro.kropacek@gmail.com>
           2018-2020, Uwe Seimet <Uwe.Seimet@seimet.de>
           1999-2020, Toni Wilen <twilen@winuae.net>
           1998-2020, Nicolas Pomarede <npomarede at corp.free.fr>
           1998-2020, Thomas Huth <huth at tuxfamily.org>
           1998-2020, Laurent Sallafranque <laurent.sallafranque@free.fr>
           1998-2020, Eero Tamminen <oak at helsinkinet fi>
License: GPL-2+

Files: src/includes/unzip.h
Copyright: 1998, Gilles Vollant
License: zlib

Files: cmake/FindSDL2.cmake
Copyright: 2003-2009, Kitware, Inc
License: BSD-3-clause-cmake
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * The names of Kitware, Inc., the Insight Consortium, or the names of
    any consortium members, or of any contributors, may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/cfgopts.c
Copyright: Jeffry J. Brickley, public domain
License: public-domain-cfgopts
 Like the original code from which this was taken, this is released
 to the Public Domain.  I cannot make any guarantees other than these
 work for me and I find them useful.  Feel free to pass these on to
 a friend, but please do not charge him....

Files: src/debug/a.out.h
Copyright: Guido Flohr (gufl0000@stud.uni-sb.de)
License: public-domain
 This file is in the public domain.

Files: debian/*
Copyright: 2004-2020, Teemu Hukkanen <tjhukkan@iki.fi>
License: GPL-2+

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Soft- ware Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in "/usr/share/common-licenses/GPL-2".

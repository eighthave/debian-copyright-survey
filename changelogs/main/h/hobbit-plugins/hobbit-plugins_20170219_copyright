Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Comment: This is a collection of plugins for the Xymon monitoring
 system, formerly known as Hobbit

Files: * debian/*
Copyright: 2007-2015 Christoph Berg <myon@debian.org>
           2010-2016 Axel Beckert <abe@debian.org>
           2005      Peter Palfrader <peter@palfrader.org>
           2008      Peter Eisentraut <peter_e@gmx.net>
           2009-2010 Emil Larsson <emil.larsson@qbranch.se>
           2012      David Bremner <bremner@debian.org>
           2013-2014 Elmar Heeb <elmar@heebs.ch>
License: MIT

Files: src/usr/lib/xymon/client/ext/backuppc
       src/usr/lib/xymon/client/ext/cciss
       src/usr/lib/xymon/server/ext/conn6
       src/usr/lib/xymon/server/ext/tftp
Copyright: 2006-2007 Seneca Cunningham <tetragon@users.sourceforge.net>
           2011 Roland Rosenfeld <roland@spinnaker.de>
           2011-2014 Axel Beckert <abe@debian.org>
License: GPL-2+

Files: t/fake-lib/BackupPC/Lib.pm
Copyright: 2015 Axel Beckert <abe@debian.org>
License: WTFPL

Files: t/perl-syntax.t
Copyright: 2007-2008 Steve Kemp <steve@steve.org.uk>
           2010-2015 Axel Beckert <abe@debian.org>
           2010 Stéphane Jourdois <stephane@jourdois.fr>
License: GPL-1+ or Artistic-1.0-Perl

License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems the full text of the GNU General Public License
 version 2 can be found under /usr/share/common-licenses/GPL-2.

License: WTFPL
 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 0. You just DO WHAT THE FUCK YOU WANT TO.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems the full text of the GNU General Public License
 version 1 can be found under /usr/share/common-licenses/GPL-1.

License: Artistic-1.0-Perl
 The "Artistic License" which comes with Perl.
 .
 On Debian systems, the full text of the Artistic License 1.0 (Perl)
 can be found under /usr/share/common-licenses/Artistic.

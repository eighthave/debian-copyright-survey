Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hashdeep
Source: http://md5deep.sf.net

Files: *
Copyright: ?         US Government work: public domain
           2007-2014 Jesse Kornblum <research@jessekornblum.com>
           2011-2012 Simson Garfinkel
License: public-domain


Files: m4/ax_pthread.m4
Copyright: 2008 Steven G. Johnson <stevenj@alum.mit.edu>
           2011 Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL-3+


Files: src/sha1.c
       src/sha1.h
Copyright: 2006-2010, Brainspark B.V.
License: GPL-2+
Comment: This file is part of PolarSSL (http://www.polarssl.org).
         Lead Maintainer: Paul Bakker <polarssl_maintainer at polarssl.org>.


Files: src/tiger.c
Copyright: 1998, 2001, 2002, 2003, 2010 Free Software Foundation, Inc.
License: LGPL-2.1+


Files: src/utf8.h
       src/utf8/checked.h
       src/utf8/core.h
       src/utf8/unchecked.h
Copyright: 2006 Nemanja Trifunovic
License: boost-software-license


Files: debian/*
Copyright: 2008-2009 Daniel Baumann <daniel@debian.org>
           2008-2012 Christophe Monniez <christophe.monniez@fccu.be>
           2009-2012 Michael Prokop <mika@debian.org>
           2011-2012 Julien Valroff <julien@debian.org>
           2015      Joao Eriberto Mota Filho <eriberto@debian.org>
           2016-2018 Giovani Augusto Ferreira <giovani@debian.org>
License: GPL-2+


License: public-domain
 This program is a work of the US Government. In accordance with 17 USC 105,
 copyright protection is not available for any work of the US Government.
 This program is PUBLIC DOMAIN. Portions of this program contain code that
 is licensed under the terms of the General Public License (GPL). Those
 portions retain their original copyright and license. See the file COPYING
 for more details.
 .
 There is NO warranty for this program; not even for MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.


License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".


License: boost-software-license
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

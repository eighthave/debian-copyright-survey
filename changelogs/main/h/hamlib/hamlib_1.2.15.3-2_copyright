Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Hamlib
Upstream-Contact: Hamlib Developers <hamlib-developer@lists.sourceforge.net>
Source: https://sourceforge.net/projects/hamlib/

Files: *
Copyright: 2000-2003 Frank Singleton
           2000-2012 Stephane Fillod
           2000-2012 The Hamlib Group
License: LGPL-2.1+ and GPL-2+
Comment: The libraries in this package are copyrighted by the LGPL Version 2.1
 (or later version), utilities are copyrighted by the GPL Version 2 (or later
 version).  See the files LICENSE and AUTHORS for additional information.

Files: debian/*
Copyright: 2001 Terry Dawson <tjd@animats.net>
           2006 Joop Stakenborg <pa3aba@debian.org>
           2010-2012 Kamal Mostafa <kamal@whence.com>
License: GPL-2+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

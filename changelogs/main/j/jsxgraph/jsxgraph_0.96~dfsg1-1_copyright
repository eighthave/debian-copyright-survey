This package was debianized by Georges Khaznadar <georgesk@ofset.org> on
Tue, 23 Sep 2008 08:08:51 +0200.

It was downloaded from http://downloads.sourceforge.net/jsxgraph/jsxgraph0.50.zip

Upstream Authors: 

        Matthias Ehmann <matthias.ehmann@uni-bayreuth.de>
        Michael Gerhaeuser <michael.gerhaeuser@uni-bayreuth.de>
        Carsten Miller <carsten.miller@uni-bayreuth.de>
        Bianca Valentin <bianca.valentin@uni-bayreuth.de>
        Alfred Wassermann <alfred.wassermann@uni-bayreuth.de>
        Peter Wilfahrt <operator@techi.de>

Copyright: 

    Copyright (C) 2008-2010 Matthias Ehmann
    Copyright (C) 2008-2010 Michael Gerhaeuser
    Copyright (C) 2008-2010 Carsten Miller
    Copyright (C) 2008-2010 Bianca Valentin
    Copyright (C) 2008-2010 Alfred Wassermann
    Copyright (C) 2008-2010 Peter Wilfahrt

License:

    LGPL - Lesser GNU General Public License, version 3 or any 
    later version
see `/usr/share/common-licenses/LGPL-3'.

Some files in the package are licensed under MIT license. As the text of 
this license is not provided with the upstream package, a template for 
this license, provided by the OSI has been copied from 
http://www.opensource.org/licenses/mit-license.php:

--------------8<------------------------
The MIT License

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
--------------8<------------------------

Some files have mixed licenses:
==============================

* file src/jsxgraph.js:
* file examples/wahldaten_test02.html:
* file ./examples/dataplotGer8.html:

these file bear the LGPL-3+ license, except for the code of one
function included inside: the functions JXG.timedChunk, respectively 
timedChunk and timedChunk are (c) 2009 Nicholas C. Zakas
(http://www.nczonline.net/contact/) and are distributed under the MIT
license.

* file src/Utils.js:

this file bears the LGPL-3+ license, except for the code of one
function included inside: the function JXG.Util.genUUID is 
Copyright (c) 2010 Robert Kieffer, and dual licensed under the
MIT and the GPL licenses. Based on the date of the copyright, the
GPL is presumabily GPL-2, see `/usr/share/common-licenses/GPL-2'.

Some other files have a single MIT license:
==========================================

* file src/xanvas.js:

this file is Copyright (c) 2010 Michael Gerhaeuser
<michael.gerhaeuser@uni-bayreuth.de>, and is licensed under the MIT
license.

* file examples/env/env.js:

this file is Copyright (c) 2007 John Resig (http://ejohn.org), and is 
licensed under  the MIT license.


Some other files have a double license, MIT and GPL-2:
=====================================================

* file examples/templates/js/shBrushJScript.js:
* file examples/templates/js/shBrushXml.js:
* file examples/templates/js/shCore.js:
* file examples/templates/css/shCore.css:
* file examples/templates/css/shThemeDefault.css:

these files are Copyright (C) 2004-2010 Alex Gorbatchev 
(http://alexgorbatchev.com/SyntaxHighlighter), and are dual
licensed under the MIT and GPL licenses. The GPL is presumabily
GPL-2.

The Debian packaging is (C) 2008-2010, Georges Khaznadar <georgesk@ofset.org> 
and is licensed under the GPL3, see `/usr/share/common-licenses/GPL-3'.

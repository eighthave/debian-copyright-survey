Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jabber-muc

Files: *
Copyright: Copyright © 1999-2002 Jabber.com, Inc.
    	   Copyright © 1998-1999 Jeremie Miller
    	   Copyright © 2003 Paul Curtis <pfc@terrapin.com>
    	   Copyright © 2002-2005 David Sutton jid: peregrine@legend.net.uk
    	   Copyright © 2007 Grégoire Menuel jid: mega@im.apinc.org
License: GPL-2+

Files: src/jabberd/sha.c
Copyright: 1995-1999 Cryptography Research, Inc.
License: MPL

Files: src/jabberd/snprintf.c
Copyright: 1995-1998 The Apache Group
License: Apache

Files: debian/*
Copyright: 2002-2007 Jamin W. Collins <jcollins@asgardsrealm.net>
	   2009-2013 Jorge Salamero Sanz <bencer@debian.org>
	   2013 Willem van den Akker <wvdakker@wilsoft.nl>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 
License: MPL
 The contents of this file are subject to the Mozilla Public
 License Version 1.1 (the "License"); you may not use this file
 except in compliance with the License. You may obtain a copy of
 the License at http://www.mozilla.org/MPL/
 . 
 Software distributed under the License is distributed on an "AS
 IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 implied. See the License for the specific language governing
 rights and limitations under the License.
 
License: Apache
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer. 
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. All advertising materials mentioning features or use of this
    software must display the following acknowledgment:
    "This product includes software developed by the Apache Group
    for use in the Apache HTTP server project (http://www.apache.org/)."
 .
 4. The names "Apache Server" and "Apache Group" must not be used to
    endorse or promote products derived from this software without
    prior written permission.
 .
 5. Redistributions of any form whatsoever must retain the following
    acknowledgment:
    "This product includes software developed by the Apache Group
    for use in the Apache HTTP server project (http://www.apache.org/)."
 .
 THIS SOFTWARE IS PROVIDED BY THE APACHE GROUP ``AS IS'' AND ANY
 EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE APACHE GROUP OR
 ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 ====================================================================
 .
 This software consists of voluntary contributions made by many
 individuals on behalf of the Apache Group and was originally based
 on public domain software written at the National Center for
 Supercomputing Applications, University of Illinois, Urbana-Champaign.
 For more information on the Apache Group and the Apache HTTP server
 project, please see <http://www.apache.org/>.
 .
 This code is based on, and used with the permission of, the
 SIO stdio-replacement strx_* functions by Panos Tsirigotis
 <panos@alumni.cs.colorado.edu> for xinetd.
 
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: JetS3t
Upstream-Contact: James Murty <jmurty@dev.java.net>
Source: https://jets3t.s3.amazonaws.com/downloads.html

Files: *
Copyright: 2006-2010, James Murty <jmurty@dev.java.net>
           2008, Zmanda Inc.
           2009, Doug MacEachern
           2010, Bennett Hiles
License: Apache-2.0

Files: src/org/jets3t/gui/TableSorter.java
Copyright: 1995-2008, Sun Microsystems, Inc
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
   - Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   - Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
   - Neither the name of Sun Microsystems nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/com/centerkey/utils/BareBonesBrowserLaunch.java
Copyright: 2005, Dem Pilafian
License: public-domain
 Free to Use as You Like.

Files: resources/images/nuvola/*
Copyright: 2003-2004, David Vignoni <david@icon-king.com>
License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 The full text of the LGPL 2.1 license is distributed in
 /usr/share/common-licenses/LGPL-2.1 on Debian systems.

Files: debian/*
Copyright: 2010-2011, Miguel Landaeta <nomadium@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
     /usr/share/common-licenses/Apache-2.0 (on Debian systems)
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 The full text of the Apache 2.0 license is distributed in
 /usr/share/common-licenses/Apache-2.0 on Debian systems.

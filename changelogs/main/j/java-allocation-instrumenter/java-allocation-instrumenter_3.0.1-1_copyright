Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: java-allocation-instrumenter
Source: https://github.com/google/allocation-instrumenter

Files: *
Copyright: 2009, Jeremy Manson
License: Apache-2.0

Files: src/main/java/com/google/monitoring/runtime/instrumentation/ConstructorCallback.java
       src/main/java/com/google/monitoring/runtime/instrumentation/VerifyingClassAdapter.java
Copyright: 2008, 2011, Google, Inc.
License: Apache-2.0

Files: src/main/java/com/google/monitoring/runtime/instrumentation/AllocationClassAdapter.java
Copyright: 2009, Google, Inc.
License: Apache-2.0
Comment: This file states "Copyright 2009 Google Inc. All Rights Reserved."
 After contacting upstream, the license has been clarified to be Apache-2.0.
 See: https://github.com/google/allocation-instrumenter/commit/6d60c64b79617b8517cf4156d93a7993a2897448

Files: src/main/java/com/google/monitoring/runtime/instrumentation/StaticClassWriter.java
Copyright: 2002-2005, France Telecom
           2011, Google, Inc. (originally Eric Bruneton)
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the copyright holders nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2014, Tim Potter <tpot@hp.com>
License: Apache-2.0

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

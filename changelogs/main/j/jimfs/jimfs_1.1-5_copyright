Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jimfs
Source: https://github.com/google/jimfs

Files: *
Copyright: 2013-2016, Google, Inc.
License: Apache-2.0

Files: jimfs/src/main/java/com/google/common/jimfs/Util.java
Copyright: 2011, Austin Appleby
License: MurmurHash3

Files: debian/*
Copyright: 2016, Hans-Christoph Steiner <hans@eds.org>
           2016, Kai-Chung Yan <seamlikok@gmail.com>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: MurmurHash3
 MurmurHash3 was written by Austin Appleby, and is placed in the
 public domain. The author hereby disclaims copyright to this source
 code.
 .
 Note: this was included in an official Google project, which means it
 passed Google legal review and their Contributors' License Agreement
 requirement.
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jsamp
Upstream-Contact: Mark Taylor <m.b.taylor@bristol.ac.uk>
Source: http://software.astrogrid.org/doc/p/jsamp/
Contributors: Thomas Boch, Laurent Bourges, Markus Demleitner,
 Jonathan Fay, Borislav Iordanov, Luigi Paioro, John Taylor,
 Ivan Zolotukhin

Files: *
Copyright: 2008-2012, Mark Taylor <m.b.taylor@bristol.ac.uk>
License: BSD-3-clause
 JSAMP is open source software; it is available under the Academic
 Free Licence (see http://www.opensource.org/licenses/afl-3.0.php )
 and the BSD Licence.
 .
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: src/java/org/astrogrid/samp/gui/BrowserLauncher.java
Copyright: 1999-2001, Eric Albert <ejalbert@cs.stanford.edu>
License: LGPL-2
 This code is Copyright 1999-2001 by Eric Albert (ejalbert@cs.stanford.edu) and may be
 redistributed or modified in any form without restrictions as long as the portion of this
 comment from this paragraph through the end of the comment is not removed.  The author
 requests that he be notified of any application, applet, or other binary that makes use of
 this code, but that's more out of curiosity than anything and is not required.  This software
 includes no warranty.  The author is not repsonsible for any loss of data or functionality
 or any adverse or unexpected effects of using this software.
 .
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2 can be found in the file `/usr/share/common-licenses/LGPL-2

Files: src/java/org/astrogrid/samp/JsonReader.java
Copyright: 2011, Miami-Dade County
License: Apache
 You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

Files: debian/*
Copyright: 2013, Florian Rothmaier <frothmai@ari.uni-heidelberg.de>
 2016, Paul Sladen <debian@paul.sladen.org>
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

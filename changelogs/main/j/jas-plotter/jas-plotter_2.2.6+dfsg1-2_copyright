Format-Specification: http://dep.debian.net/deps/dep5/
Name: JAS(2) Plotter
Maintainer: The FreeHEP team
Source: http://java.freehep.org/jas-plotter

Files: *
Copyright: 2000-2007, CERN, Geneva, Switzerland
           2000-2007, SLAC, Stanford, California, U.S.A.
           2000-2007, University of California Santa Cruz, U.S.A.
License: LGPL-2.1+
  On Debian GNU/Linux system you can find the complete text of the
  LGPL-2.1 license in '/usr/share/common-licenses/LGPL-2.1'
  No license files are included in upstream tarball but copyright holders
  list is available at
  <http://java.freehep.org/svn/repos/freehep/show/freehep/trunk/vectorgraphics/LICENSE.txt>
  and adopted license at <http://java.freehep.org/license.html>

Files: src/main/java/jas/util/encoder/GifEncoder.java, \
 src/main/java/jas/util/encoder/ImageEncoder.java
Copyright: 1996-1998, Jef Poskanzer <jef@acme.com>
License: other
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer. 
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

Files: debian/*
Copyright: 2010, Giovanni Mascellani <gio@debian.org>
	   2010, Gabriele Giacone <1o5g4r8o@gmail.com>
License: GPL-3+
  On Debian GNU/Linux system you can find the complete text of the
  GPL-3 license in '/usr/share/common-licenses/GPL-3'

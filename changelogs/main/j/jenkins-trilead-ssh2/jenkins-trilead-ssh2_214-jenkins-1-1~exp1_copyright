Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: Trilead SSH2 for Java (Jenkins Variant)
Source: https://github.com/jenkinsci/trilead-ssh2

Files: *
Copyright: 2007-2008 Trilead AG (http://www.trilead.com)
           2005 - 2006 Swiss Federal Institute of Technology (ETH Zurich),
           Department of Computer Science (http://www.inf.ethz.ch),
           Christian Plattner. All rights reserved.
License: BSD

Files: src/com/trilead/ssh2/crypto/cipher/BlowFish.java
       src/com/trilead/ssh2/crypto/cipher/DES.java
       src/com/trilead/ssh2/crypto/cipher/DESede.java
       src/com/trilead/ssh2/crypto/cipher/AES.java
Copyright: 2000 - 2004 The Legion Of The Bouncy Castle
           (http://www.bouncycastle.org)
License: MIT

Files: src/com/trilead/ssh2/crypto/digest/MD5.java
Copyright: 1991-2, RSA Data Security, Inc. Created 1991.  All rights reserved.
           2007 Christian Plattner
License: MD5 license
 License to copy and use this software is granted provided that it is
 identified as the "RSA Data Security, Inc. MD5 Message-Digest Algorithm" in
 all material mentioning or referencing this software or this function.
 .
 License is also granted to make and use derivative works provided that such
 works are identified as "derived from the RSA Data Security, Inc. MD5
 Message-Digest Algorithm" in all material mentioning or referencing the
 derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either the
 merchantability of this software or the suitability of this software for any
 particular purpose. It is provided "as is" without express or implied
 warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.
 
Files: debian/*
Copyright: 2010, Canonical Ltd (http://www.canonical.com)
License: BSD

License: BSD
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  .
  a.) Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
  b.) Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
  c.) Neither the name of Trilead nor the names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

License: MIT
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

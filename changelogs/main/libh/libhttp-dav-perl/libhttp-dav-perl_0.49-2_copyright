Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/HTTP-DAV
Upstream-Contact: Cosimo Streppone <cosimo@cpan.org>
Upstream-Name: HTTP-DAV

Files: *
Copyright: 2000-2008, Patrick Collins <pcollins@cpan.org>
 2008-2017, Cosimo Streppone for Opera Software ASA <cosimo@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2002, Stephen Zander <gibreel@debian.org>
 2004-2013, Steinar H. Gunderson <sesse@debian.org>
 2017-2018, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mininet
Source: http://mininet.org

Files: *
Copyright: 2013 Open Networking Laboratory
           2009-2012 Bob Lantz and The Board of Trustees of The Leland Stanford Junior University
License: mit-mininet-license
Comment: original authors of Mininet are Bob Lantz and Brandon Heller

Files: util/sch_htb-ofbuf/sch_htb.c
Copyright: Martin Devera <devik@cdi.cz>
License: GPL-2+

Files: debian/*
Copyright: 2014-2015 Tomasz Buchert <tomasz@debian.org>
           2014 Dariusz Dwornikowski <dariusz.dwornikowski@cs.put.poznan.pl>
License: mit-mininet-license


License: mit-mininet-license
 We are making Mininet available for public use and benefit with the
 expectation that others will use, modify and enhance the Software and
 contribute those enhancements back to the community. However, since we
 would like to make the Software available for broadest use, with as few
 restrictions as possible permission is hereby granted, free of charge, to
 any person obtaining a copy of this Software to deal in the Software
 under the copyrights without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 The name and trademarks of copyright holder(s) may NOT be used in
 advertising or publicity pertaining to the Software or any derivatives
 without specific, written prior permission.
Comment: original authors of Mininet are Bob Lantz and Brandon Heller

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

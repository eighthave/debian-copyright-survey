Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mongo-tools
Source: https://github.com/mongodb/mongo-tools
Files-Excluded: vendor/src/github.com/smartystreets/goconvey/web/client/resources

Files: *
Copyright: 2014 MongoDB, Inc.
License: Apache-2.0

Files:
 common/json/bench_test.go
 common/json/decode.go
 common/json/decode_test.go
 common/json/encode.go
 common/json/encode_test.go
 common/json/example_test.go
 common/json/fold.go
 common/json/fold_test.go
 common/json/indent.go
 common/json/scanner.go
 common/json/scanner_test.go
 common/json/stream.go
 common/json/stream_test.go
 common/json/tagkey_test.go
 common/json/tags.go
 common/json/tags_test.go
 vendor/src/golang.org/*
 vendor/src/github.com/3rf/*
Copyright: 2010 - 2013 The Go Authors.
License: BSD-3-clause

Files: vendor/src/github.com/10gen/escaper/*
Copyright: 2016 Lucas Morales
License: MIT/X11

Files: vendor/src/github.com/golang/snappy/*
Copyright: 2011 The Snappy-Go Authors
License: BSD-3-clause
Comment: Not used for the Debian package build

Files: vendor/src/github.com/google/gopacket/*
Copyright: 2012 Google, Inc
           2009-2011 Andreas Krennmair
License: BSD-3-clause
Comment: Not used for the Debian package build

Files: vendor/src/github.com/nsf/termbox-go/*
Copyright: 2012 termbox-go authors
License: MIT/X11
Comment: Not used for the Debian package build

Files: vendor/src/github.com/patrickmn/go-cache/*
Copyright: 2012-2015 Patrick Mylund Nielsen and the go-cache contributors
License: MIT/X11

Files: vendor/src/golang.org/x/crypto/*
Copyright: 2009 The Go Authors
License: BSD-3-clause
Comment: Not used for the Debian package build

Files: vendor/src/github.com/spacemonkeygo/*
Copyright: 2014 Space Monkey, Inc
License: Apache-2.0

Files: vendor/src/github.com/spacemonkeygo/openssl/hostname.c
Copyright: 1999-2003 The OpenSSL Project
License: OpenSSL

Files: vendor/src/github.com/spacemonkeygo/openssl/cert_test.go
       vendor/src/github.com/spacemonkeygo/openssl/ctx_test.go
       vendor/src/github.com/spacemonkeygo/openssl/nid.go
       vendor/src/github.com/spacemonkeygo/openssl/pem.go
Copyright: 2014 Ryan Hileman
License: Apache-2.0

Files: vendor/src/github.com/smartystreets/*
Copyright: 2013-2015 SmartyStreets, LLC
License: MIT/X11

Files: vendor/src/github.com/smartystreets/assertions/internal/oglemock/*
       vendor/src/github.com/smartystreets/assertions/internal/ogletest/*
       vendor/src/github.com/smartystreets/assertions/internal/oglematchers/*
Copyright: 2011-2015 Aaron Jacobs
License: Apache-2.0

Files: vendor/src/github.com/smartystreets/assertions/internal/go-render/*
Copyright: 2015 The Chromium Authors
License: BSD-3-clause

Files: vendor/src/github.com/smartystreets/assertions/internal/reqtrace/*
Copyright: 2015 Google Inc.
License: Apache-2.0

Files: vendor/src/github.com/jacobsa/*
Copyright: 2011-2012 Aaron Jacobs
License: Apache-2.0

Files: vendor/src/github.com/jessevdk/*
Copyright: 2012 Jesse van den Kieboom
License: BSD-3-clause

Files: vendor/src/github.com/jtolds/*
Copyright: 2013, Space Monkey, Inc.
License: MIT/X11

Files: vendor/src/github.com/howeyc/*
Copyright: 2012 Chris Howe
License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: vendor/src/gopkg.in/mgo.v2/*
       vendor/src/github.com/10gen/llmgo/*
Copyright: 2010-2014 Gustavo Niemeyer <gustavo@niemeyer.net>
License: BSD-2-clause

Files: vendor/src/gopkg.in/tomb.v2/*
Copyright: 2010-2011 Gustavo Niemeyer <gustavo@niemeyer.net>
License: BSD-3-clause

Files: debian/*
Copyright: 2016-2018 Apollon Oikonomopoulos <apoikos@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache 2.0 license can be found in
 "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
    * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: OpenSSL
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. All advertising materials mentioning features or use of this
    software must display the following acknowledgment:
    "This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit. (http://www.OpenSSL.org/)"
 .
 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
    endorse or promote products derived from this software without
    prior written permission. For written permission, please contact
    licensing@OpenSSL.org.
 .
 5. Products derived from this software may not be called "OpenSSL"
    nor may "OpenSSL" appear in their names without prior written
    permission of the OpenSSL Project.
 .
 6. Redistributions of any form whatsoever must retain the following
    acknowledgment:
    "This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit (http://www.OpenSSL.org/)"
 .
 THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
 EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
 ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT/X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

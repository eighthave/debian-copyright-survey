Format: http://anonscm.debian.org/viewvc/dep/web/deps/dep5.mdwn?revision=202
Upstream-Name: djExtDirect
Source: http://bitbucket.org/Svedrin/mumble-django

Files: *
Copyright: 2009-2012, Michael Ziegler
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full license can be found at
 "/usr/share/common-licenses/GPL-2".

Files: debian/*
Copyright: 2010-2012, Michael Ziegler
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full license can be found at
 "/usr/share/common-licenses/GPL-2".

Files: htdocs/ext-3.2.0/*
Copyright: 2010-2012, Michael Ziegler
License: GPL-3
 Ext JS - JavaScript Library
 Copyright © 2006-2010, Ext JS, LLC
 All rights reserved.
 licensing@extjs.com
 .
 http://extjs.com/license
 .
 Open Source License
 -------------------------------------------------------------------
 Ext is licensed under the terms of the Open Source GPL 3.0 license.
 .
 http://www.gnu.org/licenses/gpl.html
 .
 There are several FLOSS exceptions available for use with this
 release for open source applications that are distributed under a
 license other than the GPL.
 .
 * Open Source License Exception for Applications
 .
   http://extjs.com/products/floss-exception.php
 .
 * Open Source License Exception for Development
 .
   http://extjs.com/products/ux-exception.php
 .
 Commercial License
 -------------------------------------------------------------------
 This is the appropriate option if you are creating proprietary
 applications and you are not prepared to distribute and share the
 source code of your application under the GPL v3 license. Please
 visit http://extjs.com/license for more details.
 .
 OEM / Reseller License
 -------------------------------------------------------------------
 For more details, please visit: http://extjs.com/license.
 .
 --
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

Files: pyweb/mumble/media/img/*
Copyright: 2005-2009, Martin Skilnand
License: BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 - Neither the name of the Mumble Developers nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 `AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: pyweb/mumble/locale/hr/LC_MESSAGES/django.po
Copyright: 2009-2012, Michael Ziegler,
 2009, Vid "Night" Marić <nightcooki3@gmail.com>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full license can be found at
 "/usr/share/common-licenses/GPL-2".

Files: pyweb/mumble/locale/it/LC_MESSAGES/django.po
Copyright: 2009-2012, Michael Ziegler,
 2009, satinez <info@satinez.net>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the full license can be found at
 "/usr/share/common-licenses/GPL-2".

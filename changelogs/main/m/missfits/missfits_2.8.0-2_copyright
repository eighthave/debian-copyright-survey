Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: missfits
Source: https://www.astromatic.net/download/missfits
Files-Excluded: doc/missfits.pdf

Files: *
Copyright: 1995-2012 Emmanuel Bertin -- IAP/CNRS/UPMC,
 2006-2010 Chiara Marmo -- IAP/CNRS
 1995-1999 Mark Calabretta
 2015 Gijs Molenaar <Gijs Molenaar <gijs@pythonic.nl>,
 2015 Ole Streicher <olebole@debian.org>
License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
Comment: The files in src/wcs/ were originally released under GPL-2.0+ and
 LGPL-2.0+, but changed and relicensed under GPLv3+ by the upstream author.
 .
 This was approved by the original author (Mark Calabretta):
 .
 Emmanuel Bertin was an early adopter of wcslib and I'm not worried if
 he has applied a stricter (GPL) license to a modified version of 2.2.
 If necessary, I will sprinkle holy water on missfits if the Debian
 package manager requires it.

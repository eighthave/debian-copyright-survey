Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: marionnet
Source: https://launchpad.net/marionnet

Files: *
Copyright: 2007-2013 Luca Saiu <saiu@lipn.univ-paris13.fr>
           2007-2013 Jean-Vincent Loddo <loddo@lipn.univ-paris13.fr>
           2007-2013 Université Paris 13
License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

Files: po/*
Copyright: 2010, the Marionnet translators
License: BSD-3-clause
 The following license applies to translation files (the ones with
 a .po extension), and as a consequence to the .mo files which are
 automatically generated from them.
 .
 Translation contributions coming from the Launchpad interface are
 automatically licensed under these conditions, as the Launchpad
 terms of use (at https://help.launchpad.net/TermsofUse as of
 27 September 2010) stipulate.
 The translation copyright belongs to the original submitters.
 .
 Please notice that Marionnet and Ocamlbricks are distributed
 under the GNU GPL rather than this three-clause BSD license.
 .
 The following text is the complete three-clause BSD license.
 .
 --------------------
 .
 Copyright (c) 2010, the Marionnet translators
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: share/images/leds/*
Copyright: made by Luca Saiu, but public domain
License: public-domain
 Made by Luca Saiu with the GIMP. Public domain.

Files: share/images/treeview-icons/*
Copyright: made by Luca Saiu, but public domain
License: public-domain
 Images in this directory were made by Luca Saiu with the GIMP.
 The author releases them into the public domain.

Files: share/images/ico.info.orig.png
       share/images/ico.warning.orig.png
       share/images/ico.error.orig.png
Copyright: same as GNOME Amaranth theme
License: Artistic
 On Debian systems, the complete text of the Artistic license
 can be found in "/usr/share/common-licenses/Artistic".
Comment: see share/images/README
 taken from GNOME Amaranth theme
 
Files: share/images/ico.question-2.orig.png
Copyright: same as GNOME icons
Comment: see share/images/README
 ico.question-2.orig.png is taken from the Gnome icons, released
 by its authors under the GNU GPL version 2; converted by Luca Saiu
 in 2008.
License: GPL-2
 The license paragraph was not included in share/images/README.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: share/images/ico.splines.png
Copyright: the Oxygen Team
Comment: see share/images/README
 "Actions-format-stroke-color-icon" taken from the Oxygen Icons and
 released by its authors, the "Oxygen Team", under the GNU LGPL v3
 (a copy of which is included in this directory); the icon was modified
 by Jean-Vincent Loddo in 2013.
 http://www.oxygen-icons.org/
 Original file (24x24) downloaded from:
 http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-format-stroke-color-icon.html
License: LGPL-3
 The license paragraph was not included in share/images/README.
 .
 On Debian systems, the complete text of the GNU lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

Files: share/images/ico.invert-arcs.png
Copyright: the Oxygen Team
Comment: see share/images/README
 "Actions-transform-rotate-icon" taken from the Oxygen Icons and
 released by its authors, the "Oxygen Team", under the GNU LGPL v3;
 the icon was modified by Jean-Vincent Loddo in 2013 (simply desatured).
 http://www.oxygen-icons.org/
 Original file (24x24) downloaded from:
 http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org/Actions-transform-rotate-icon.html
License: LGPL-3
 The license paragraph was not included in share/images/README.
 .
 On Debian systems, the complete text of the GNU lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

Files: share/images/ico.action.zoom.in.png
       share/images/ico.action.zoom.out.png
Copyright: Saki
Comment: see share/images/README
 "Actions-zoom-in-icon" taken from the "Snowish Icons" and
 released by its author "Saki" under the GNU LGPL v3.
 http://www.oxygen-icons.org/
 Original file (24x24) downloaded from:
 http://www.iconarchive.com/show/snowish-icons-by-saki/Actions-zoom-in-icon.html
License: LGPL-3
 The license paragraph was not included in share/images/README.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-3".

Files: share/vde_switch.lang
Copyright: 2012  Jean-Vincent Loddo
           2012  Université Paris 13
License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>

Files: debian/*
Copyright: 2013 Lucas Nussbaum <lucas@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

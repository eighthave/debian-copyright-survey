Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: montage
Source: http://montage.ipac.caltech.edu/docs/download.html
Files-Excluded: lib/src/wcstools-* lib/src/cfitsio-* lib/src/jpeg-8b
 lib/src/freetype-* data/fonts

Files: *
Copyright: 2001-2014 California Institute of Technology, Pasadena, California.
License: BSD-3-Clause
 Based on Cooperative Agreement Number NCC5-626 between NASA and the
 California Institute of Technology. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions of this
 BSD 3-clause license are met:
 .
  1. Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
 .
  3. Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 This software was developed at the Infrared Processing and Analysis Center
 (IPAC) and the Jet Propulsion Laboratory (JPL) by Bruce Berriman, John Good,
 Joseph Jacob, Daniel S. Katz, and Anastasia Laity.

Files: util/QuickSearch/rtree/*
Copyright: 2001 GRASS Development Team
License: GPL-2+

Files: lib/src/lodepng*
Copyright: 2005-2014 Lode Vandevenne
License: libpng
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
  claim that you wrote the original software. If you use this software
  in a product, an acknowledgment in the product documentation would be
  appreciated but is not required.
 .
  2. Altered source versions must be plainly marked as such, and must not be
  misrepresented as being the original software.
 .
  3. This notice may not be removed or altered from any source
  distribution.

Files: debian/*
Copyright: 2014 Gijs Molenaar <gijs@pythonic.nl>
 2015 Ole Streicher <olebole@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

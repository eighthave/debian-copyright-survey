Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Merkaartor
Upstream-Contact: Merkaartor Developers <merkaartor@openstreetmap.org>
Source: https://github.com/openstreetmap/merkaartor

Files: *
Copyright: 2005, Robin Gingras
           2007, Kai Winter
           2008, Łukasz Jernaś
           2008, Travers Carter <tcarter@noggin.com.au>
           2010, Christian Vetter <veaac.fdirct@gmail.com>
           2010, James Hogan <james@albanarts.com>
      2006-2016, Bart Vanhauwaert <bvh-osm@irule.be>
      2006-2016, Chris Browet <cbro@semperpax.com>
      2006-2016, Ladislav Láska
Comment: AUTHORS
 .
 Bart Vanhauwaert bvh-osm@irule.be
 Chris "Koying" Browet
 Ladislav Láska <krakonos@krakonos.org>
 Elrond elrond+openstreetmap.org@samba-tng.org
 Oliver Heesakkers
 Patric Cameron
 Geoff Richards
 Raphael Studer
 Matt Williams
 Daniel van Gerpen
 Colin Marquardt
 Andreas Laas
 Vincent Meurisse
 Dirk Stöcker
 Tomás Mika
 Yves "LonelyPixel" Goergen
 Toby Speight
 James Hogan
 Richard Brinkman
 Balgofil
License: GPL-2+

Files: macos/mklocversion.sh
Copyright: 2010, Albrecht Dreß <albrecht.dress@arcor.de>
License: GPL-2

Files: 3rdparty/qtsingleapplication-2.6_1-opensource/*
 src/QToolBarDialog/qttoolbardialog.cpp
 src/QToolBarDialog/qttoolbardialog.h
Copyright: 2009, Nokia Corporation and/or its subsidiary(-ies)
License: QT-Commercial or LGPL-2.1 with Digia Qt LGPL Exception 1.1 or GPL-3

Files: 3rdparty/quazip-0.7/*
Copyright: 2005-2012, Sergey A. Tachenov
License: LGPL-2+

Files: 3rdparty/quazip-0.7/quazip/*
 3rdparty/quazip-0.7/qztest/*
Copyright: 2010, Adam Walczak
           2010, Roberto Pompermaier
      2005-2014, Sergey A. Tachenov
License: LGPL-3+

Files: 3rdparty/quazip-0.7/quazip/crypt.h
 3rdparty/quazip-0.7/quazip/ioapi.h
 3rdparty/quazip-0.7/quazip/qioapi.cpp
 3rdparty/quazip-0.7/quazip/unzip.c
 3rdparty/quazip-0.7/quazip/unzip.h
 3rdparty/quazip-0.7/quazip/zip.c
 3rdparty/quazip-0.7/quazip/zip.h
Copyright: 1990-2000, Info-ZIP.
           2007-2008, Even Rouault
           1998-2010, Gilles Vollant (minizip) ( http://www.winimage.com/zLibDll/minizip.html )
           2009-2010, Mathias Svensson ( http://result42.com )
           2005-2014, Sergey A. Tachenov
License: zlib

Files: mobilemerk/thirdparty/MouseMachine/*
Copyright: 2009, Chris Browet <cbro@semperpax.com>
License: LGPL-2.1+

Files: plugins/background/MCadastreFranceBackground/qadastre/*
Copyright: 2010, Pierre Ducroquet <pinaraf@pinaraf.info>
License: GPL-3+

Files: plugins/background/MNavitBackground/attr_def.h
 plugins/background/MNavitBackground/item_def.h
Copyright: 2005-2009, Navit Team
License: LGPL-2

Files: debian/*
Copyright: 2006, bart <bvh-osm@irule.be>
           2008, Christoph Berg <myon@debian.org>
      2008-2009, Bernd Zeimetz <bzed@debian.org>
License: GPL-2+

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 or
 version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public License
 version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: QT-Commercial
 Commercial License Usage
 Licensees holding valid commercial Qt licenses may use this file in
 accordance with the commercial license agreement provided with the
 Software or, alternatively, in accordance with the terms contained in
 a written agreement between you and Digia. For licensing terms and
 conditions see http://qt.digia.com/licensing. For further information
 use the contact form at http://qt.digia.com/contact-us.

License: LGPL-2.1 with Digia Qt LGPL Exception 1.1
 Alternatively, this file may be used under the terms of the GNU Lesser
 General Public License version 2.1 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPL included in the
 packaging of this file. Please review the following information to
 ensure the GNU Lesser General Public License version 2.1 requirements
 will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Digia gives you certain additional
 rights. These rights are described in the Digia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 Digia Qt LGPL Exception version 1.1
 As an additional permission to the GNU Lesser General Public License version
 2.1, the object code form of a "work that uses the Library" may incorporate
 material from a header file that is part of the Library. You may distribute
 such object code under terms of your choice, provided that:
 (i) the header files of the Library have not been modified; and
 (ii) the incorporated material is limited to numerical parameters, data
 structure layouts, accessors, macros, inline functions and
 templates; and
 (iii) you comply with the terms of Section 6 of the GNU Lesser General
 Public License version 2.1.
 .
 Moreover, you may apply this exception to a modified version of the Library,
 provided that such modification does not involve copying material from the
 Library into the modified Library's header files unless such material is
 limited to (i) numerical parameters; (ii) data structure layouts;
 (iii) accessors; and (iv) small macros, templates and inline functions of
 five lines or less in length.
 .
 Furthermore, you are not required to apply this additional permission to a
 modified version of the Library.


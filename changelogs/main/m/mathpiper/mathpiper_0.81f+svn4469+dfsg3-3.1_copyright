Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mathpiper
Source: http://code.google.com/p/mathpiper/

Files: *
Copyright: 2008-2011 Ted Kosan <tkosan@dev.java.net>
           2008-2011, Sherm Ostrowsky
           1998-2009, Ayal Pinkus <apinkus@xs4all.nl>
License: GPL-2+

Files: src/org/mathpiper/ui/gui/controlpanel/HaltButton.java
       src/org/mathpiper/ui/gui/MultiSplitLayout.java
       src/org/mathpiper/ui/gui/MultiSplitPane.java
Copyright: 2004, Sun Microsystems, Inc., 4150 Network Circle, Santa Clara, California 95054, U.S.A.
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the LGPL-2.1 license text can be found in:
 /usr/share/common-licenses/LGPL-2.1

Files: src/org/mathpiper/ui/gui/hoteqn/*
Copyright: 2006, Stefan Müller
           2006, Christian Schmid
License: GPL
 HotEqn is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; 
 HotEqn is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

Files: src/org/mathpiper/mpreduce/*
       lib/build_scripts/*
       src/packages/*
Copyright: 1998-2011, Codemist Ltd.
           1998-2011, A. C. Norman
           2002, Vijay Chauhan
           2011, Ted Kosan <tkosan@dev.java.net>
	   1987-1996, The RAND Corporation
	   1987-2005, Anthony C. Hearn
	   2008-2010, Thomas Sturm
	   1996, Neil Langmead
	   1988, David Harper
	   1990-1997, Zuse-Institut Berlin (ZIB)
	   1997, Wolfram Koepf
	   1997, Harald Boeing
	   1987, James W Eastwood
	   1987-1995, Stanley L. Kameny <stan_kameny@rand.org>
	   1988-1990, Institute of Nuclear Physics, Moscow State University
	   1990-1992, Mathias Warns
	   1990-1996, A. Kryukov <kryukov@theory.npi.msu.su>
	   1989-2010, Rainer M. Schoepf
	   1999-2009, Andreas Dolzmann
	   2003-2009, Lorenz Gilch
	   2002-2009, A. Seidl
	   J.A. van Hulzen
	   Alexey Yu. Zharkov
	   Yuri A. Blinkov
License: BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the relevant
       copyright notice, this list of conditions and the following
       disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
 THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.
Comment: Portions attributed to the RAND Corporation do not carry an
 explicit license notice. Anyway, they come from the Reduce project,
 which is now wholly available under the reported license (see
 http://www.reduce-algebra.com/license.htm). I believe it is safe to
 consider such code snippets under the same license.

Files: src/org/mathpiper/builtin/javareflection/JavaField.java
       src/org/mathpiper/builtin/javareflection/JavaMethod.java
       src/org/mathpiper/builtin/javareflection/JavaConstructor.java
       src/org/mathpiper/builtin/javareflection/StaticReflector.java
       src/org/mathpiper/builtin/javareflection/Invoke.java
       src/org/mathpiper/builtin/javareflection/Import.java
Copyright: 1998, Peter Norvig <peter@norvig.com>
           2000, Ken R. Anderson <kanderso@bbn.com>
License: JScheme
 This system is licensed under the following 
 zlib/libpng open-source license.
 .
 This software is provided 'as-is', without any express or
 implied warranty.
 .
 In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it
 and redistribute it freely, subject to the following
 restrictions:
 .
 1. The origin of this software must not be misrepresented; you
    must not claim that you wrote the original software. If you
    use this software in a product, an acknowledgment in the
    product documentation would be appreciated but is not
    required.
 .
 2. Altered source versions must be plainly marked as such, and
    must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.
Comment: These files come from the JScheme project. Their license is
 not specified in Mathpiper, but I consider safe to assume the JScheme
 license is valid here.

Files: src/org/mathpiper/builtin/library/cern/*
Copyright: © 1999, CERN - European Organization for Nuclear Research
License: CERN
 Permission to use, copy, modify, distribute and sell this software
 and its documentation for any purpose is hereby granted without fee,
 provided that the above copyright notice appear in all copies and
 that both that copyright notice and this permission notice appear in
 supporting documentation.  CERN makes no representations about the
 suitability of this software for any purpose.  It is provided "as is"
 without expressed or implied warranty.

Files: src/org/mathpiper/builtin/library/jscistats/SpecialMath.java
Copyright: © 1993, Sun Microsystems, Inc.
License: SUN
 Permission to use, copy, modify, and distribute this software is
 freely granted, provided that this notice is preserved.

Files: src/org/mathpiper/builtin/library/statdistlib/*
Copyright: 1995-1998, Ross Ihaka
           1995-1996, Robert Gentleman
           1998, R Core Team
           Mark Hale
           Jaco van Kooten
License: GPL-2+

Files: debian/*
Copyright: 2009-2013, Giovanni Mascellani <gio@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the GPL-2 license text can be found in:
 /usr/share/common-licenses/GPL-2

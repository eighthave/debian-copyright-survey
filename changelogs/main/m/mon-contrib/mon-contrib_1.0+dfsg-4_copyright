Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mon-contrib
Source: https://sourceforge.net/projects/mon/files/mon-contrib


Files: alerts/aim/aim.alert
Copyright: 2002, Don Harper <duck@duckland.org>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/bugzilla/bugzilla.alert
Copyright: 2000, Michael S. Fischer, <michael@auctionwatch.com>, AuctionWatch.com.
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/gnats/gnats.alert
Copyright: 2002, Ted Serreyn <ted@serreyn.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/hpov/hpov.alert
Copyright: 2001, Scott Prater <sprater@servicom2000.com>
           1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/lvs/ipvs.alert
Copyright: 2004, Christopher DeMarco <cdemarco@md.com.my>
           2008, Richard Hartmann <richih@net.in.tun.de>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/sms/sms.alert
Copyright: Robert Sander <robert.sander@epigenomics.com>
License: Public domain
 See: /usr/share/doc/mon-contrib/alerts/sms.alert.copyright.README


Files: alerts/sms/sms/sms.alert
Copyright: 1998, Peter Holzleitner <P.Holzleitner@computer.org>
           1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/snapshot/snapdelete.alert
Copyright: 1998-2001, Theo Van Dinter <tvd@colltech.com> <felicity@kluge.net>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: alerts/winpopup/winpopup.alert
Copyright: 2001, Matthew Rechs - rechsm@hotmail.com
           1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/bgp/bgp/bgp.monitor
Copyright: 2002, Marc Hauswirth, Safe Host SA <marc@safehostnet.com>
           2005, Ed Ravin <eravin@panix.com>
License: GPL-2
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/cisco/cisco-env.monitor
Copyright: 2004, Carnegie Mellon University
License: BSD-4-clause 
 Copyright (c) 2002 Carnegie Mellon University. All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name "Carnegie Mellon University" must not be used to endorse or
    promote products derived from this software without prior written
    permission. For permission or any legal details, please contact:
      Office of Technology Transfer
      Carnegie Mellon University
      5000 Forbes Avenue
      Pittsburgh, PA 15213-3890
      (412) 268-4387, fax: (412) 268-7395
      tech-transfer@andrew.cmu.edu
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment: "This product includes software developed by Computing
    Services at Carnegie Mellon University (http://www.cmu.edu/computing/)."
 .
 CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.


Files: monitors/cisco/snmp_interface/snmp_interface.monitor
Copyright: 1998, Brian Moore <bem@cmc.net>
           2000, Ed Ravin <eravin@panix.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/citrix/citrix.monitor
Copyright: 2005, Jeroen Moors <Jeroen.Moors@Cegeka.be>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/cyrus/imsp.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/cyrus/lmtp.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/cyrus/mupdate.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/dhcp/dhcp.monitor
Copyright: 2002, Carnegie Mellon University
License: BSD-4-clause 
 Copyright (c) 2002 Carnegie Mellon University. All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name "Carnegie Mellon University" must not be used to endorse or
    promote products derived from this software without prior written
    permission. For permission or any legal details, please contact:
      Office of Technology Transfer
      Carnegie Mellon University
      5000 Forbes Avenue
      Pittsburgh, PA 15213-3890
      (412) 268-4387, fax: (412) 268-7395
      tech-transfer@andrew.cmu.edu
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment: "This product includes software developed by Computing
    Services at Carnegie Mellon University (http://www.cmu.edu/computing/)."
 .
 CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.


Files: monitors/diskspace/netsnmp-freespace/netsnmp-freespace.monitor
Copyright: 2001, SATOH Fumiyasu <fumiya@samba.gr.jp>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/diskspace/snmpdiskspace/snmpdiskspace.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
           2000, jens persson <jens.persson@btj.se>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/diskspace/storage.monitor-1.0.0/storage.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
           2000, Peter Holzleitner <P.Holzleitner@computer.org>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/dnsbl/rbl.monitor
Copyright: 2007-2008, Ed Ravin <eravin@panix.com>
License: GPL-2
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/file/dir_file_age/dir_file_age.monitor
Copyright: 2000-2011, Jon Meek <meekj@ieee.org>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/flexlm/flexlm.monitor
Copyright: 1999, Juha Ylitalo  <jylitalo@iki.fi>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/ftp/ftps/ftps.monitor
Copyright: 2008, Pierre-Emmanuel Andre
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/http/http_integrity/http_integrity.monitor
Copyright: 2000, Andrew Ryan <andrewr@nam-shub.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/http/phttp/phttp.monitor
Copyright: 2006, Gilles LAMIRAL <lamiral@mail.dotcom.fr>
License: GPL-1 
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-1".


Files: monitors/https/https/https.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: Artistic | GPL-1
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.


Files: monitors/icecast/icecast/icecast.monitor
Copyright: 2001, Mark Rushing <rushing@orbislumen.net>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/imap/imap-ptp.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/imap/imap-ssl.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/imap/imap-starttls.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/informix/informix/informix.monitor
Copyright: 1999, SKECHERS USA, Inc.
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/informix/informixdbspace/informixdbspace.monitor
Copyright: 1999, SKECHERS USA, Inc.
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/ipsec/ipsec/ipsec.monitor
Copyright: 2001, Wiktor Wodecki
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/kerberos/krb5.monitor
Copyright: 2002, Carnegie Mellon University <net-dev@andrew.cmu.edu>
License: BSD-4-clause 
 Copyright (c) 2002 Carnegie Mellon University. All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name "Carnegie Mellon University" must not be used to endorse or
    promote products derived from this software without prior written
    permission. For permission or any legal details, please contact:
      Office of Technology Transfer
      Carnegie Mellon University
      5000 Forbes Avenue
      Pittsburgh, PA 15213-3890
      (412) 268-4387, fax: (412) 268-7395
      tech-transfer@andrew.cmu.edu
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment: "This product includes software developed by Computing
    Services at Carnegie Mellon University (http://www.cmu.edu/computing/)."
 .
 CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.


Files: monitors/lvs/ipvs.monitor
Copyright: 2004, Christopher DeMarco <cdemarco@md.com.my>
           2008, Richard Hartmann <richih@net.in.tun.de>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/mon/mon/mon.monitor
Copyright: 2000, Andrew Ryan <andrewr@nam-shub.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/mon/umn_mon/umn_mon.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/netsnmp/netsnmp-exec/netsnmp-exec.monitor
Copyright: 2001 Daniel J. Urist <durist@world.std.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/netsnmp/netsnmp-freespace/netsnmp-freespace.monitor
Copyright: 2001 SATOH Fumiyasu <fumiya@samba.gr.jp>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/netsnmp/netsnmp-proc/netsnmp-proc.monitor
Copyright: 2001 Daniel J. Urist <durist@world.std.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/netsnmp/process-full-command-line/process-full-command-line.monitor
Copyright: 1998, Stephane Bortzmeyer <bortzmeyer@generic-nic.net>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/netware/netwarefree/netwarefree.monitor
Copyright: 2000-2001, Peter Holzleitner <peter@holzleitner.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/oracle/sqlconn/sqlconn.monitor
Copyright: 1999, Duncan Lawie <duncanl@demon.net>
License: GPL-1+ 
 On Debian systems, the complete text of the GNU General
 Public License version 1 can be found in "/usr/share/common-licenses/GPL-1".



Files: monitors/ospf/ospf.monitor
Copyright: 2002, Marc Hauswirth, Safe Host SA <marc@safehostnet.com>
           2005, Ed Ravin <eravin@panix.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/postgres/postgresql/postgresql.monitor
Copyright: 2001, CubIT IT Solutions, 
                 Severin Luftensteiner <severin.luftensteiner@cubit.at>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/printers/printmib/printmib.monitor
Copyright: 2000, Seth Vidal <skvidal@phy.duke.edu>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/ps/ps.monitor
Copyright: 2005, Allan Wind
License: 
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


Files: monitors/pubcookie/pubcookielogin.monitor
Copyright: 1999, Juha Ylitalo <jylitalo@iki.fi>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/pubcookie/webapp.monitor
Copyright: 1999, Juha Ylitalo <jylitalo@iki.fi>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/radius/radius/radius.monitor
Copyright: 1998, ACC TelEnterprises,
                 James FitzGibbon <james@ican.net>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/raid/softraid.monitor
Copyright: 2004-2011, Kevin Ivory <Ivory@SerNet.de>
License: GPL-3
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-3".


Files: monitors/remote/remote/remote.monitor
Copyright: 1999, Gilles LAMIRAL <lamiral@mail.dotcom.fr>
           1999, Laurent COMBE <laurent.combe@free.fr>
           1999, Thomas MORIN <thomas.morin@webmotion.com>
License: GPL-1
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-1".


Files: monitors/remote/startremote/startremote.monitor
Copyright: 2001, CubIT IT Solutions,
                 Severin Luftensteiner <severin.luftensteiner@cubit.at>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/repeater/rptr/rptr.monitor
Copyright: 2000, Phil Gregory
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/samba/samba/samba.monitor
Copyright: 1998, Jean Le Moigne <jean.lemoigne@francetelecom.fr>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/smtp/mailloop/mailloop.monitor
Copyright: 2000, Shared Medical Systems, Inc,
                 Bill Smargiassi <william.smargiassi@smed.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/smtp/smtp-ssl.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/smtp/smtp_rt/smtp_rt.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
           1999, Tom Bates <ctbates@tva.gov>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/ssh/ssh/ssh.monitor
Copyright: 2000, Regents of the University of Minnesota,
                 Steven F. Siirila <sfs@umn.edu>
License: 
 Permission to use, copy, modify, and distribute this software and
 documentation without fee is hereby granted, provided that the
 University of Minnesota copyright notices and this permission notice
 appear in all copies, and that the name University of Minnesota not
 be used in advertising or publicity pertaining to this software and
 documentation without specific, written prior permission.
 .
 The University of Minnesota makes no representations about the
 suitability of this software and documentation for any purpose.
 It is provided ``as is'' without express or implied warranty.


Files: monitors/sybase/sybase/sybase.monitor
Copyright: 1999, Peter Holzleitner <P.Holzleitner@computer.org>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/tcp/tcpch/tcpch.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
           2000, Ed Ravin <eravin@panix.com>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: monitors/tftp/tftp.monitor
Copyright: 2002, Carnegie Mellon University,
                 David Nolan <net-dev@andrew.cmu.edu>
License: BSD-4-clause 
 Copyright (c) 2002 Carnegie Mellon University. All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name "Carnegie Mellon University" must not be used to endorse or
    promote products derived from this software without prior written
    permission. For permission or any legal details, please contact:
      Office of Technology Transfer
      Carnegie Mellon University
      5000 Forbes Avenue
      Pittsburgh, PA 15213-3890
      (412) 268-4387, fax: (412) 268-7395
      tech-transfer@andrew.cmu.edu
 .
 4. Redistributions of any form whatsoever must retain the following
    acknowledgment: "This product includes software developed by Computing
    Services at Carnegie Mellon University (http://www.cmu.edu/computing/)."
 .
 CARNEGIE MELLON UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.


Files: monitors/udp/udp.monitor
Copyright: 1998, Jim Trocki <trockij@transmeta.com>
           2005, David Nolan <vitroth@cmu.edu>
License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


Files: debian/*
Copyright: 2011-2014, Dario Minnucci <midget@debian.org>
License: GPL-2
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


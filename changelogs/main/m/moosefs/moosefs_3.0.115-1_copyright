Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MooseFS
Upstream-Contact: support@moosefs.com
Source: https://github.com/moosefs/moosefs
Files-Excluded:
Comment:

Files: *
Copyright:
    2005-2020 Jakub Kruszona-Zawadzki
              Core Technology Sp. z o.o.
License: GPL-2

Files:
    m4/ax_check_compile_flag.m4
Copyright:
    2008      Guido U. Draheim <guidod@gmx.de>
    2011      Maarten Bosmans <mkbosmans@gmail.com>
License: FSF-Install

Files:
    m4/ax_pthread.m4
Copyright:
    2008      Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3+_with_exception-Autoconf

Files:
    m4/libtool.m4
    m4/ltoptions.m4
    m4/ltsugar.m4
    m4/ltversion.m4
    m4/lt~obsolete.m4
Copyright:
    2004-2015 Free Software Foundation, Inc
License: FSFULLR

Files: debian/*
Copyright:
    2014-2020 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-3+

Files: debian/patches/*
Copyright:
    2014-2019 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-2+

License: FSF-Install
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice and
 this notice are preserved. This file is offered as-is, without warranty of
 any kind.

License: FSFULLR
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

License: GPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, version 2 of the License.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 The complete text of the GNU General Public License version 3
 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3+_with_exception-Autoconf
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 The complete text of the GNU General Public License version 3
 can be found in "/usr/share/common-licenses/GPL-3".
 ․
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 ․
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

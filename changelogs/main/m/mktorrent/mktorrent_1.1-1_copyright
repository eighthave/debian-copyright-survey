Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mktorrent
Upstream-Contact: Kai Nilsen <mktorrent@rudde.org>
Source: https://github.com/Rudde/mktorrent
Comment:
 Emil Renner Berthing, the upstream author, transferred the maintainership of
 mktorrent to Kai Nilsen (Rudde) on May 26, 2016. See upstream commit 887351f:
 https://github.com/Rudde/mktorrent/commit/887351ffda449e4dee2f.

Files: *
Copyright:
 2009-2017, Emil Renner Berthing <esmil@mailme.dk>
License: GPL-2+ with OpenSSL exception

Files: debian/*
Copyright:
 2009-2018, Nico Golde <nion@debian.org>
 2018, Paride Legovini <pl@ninthfloor.org>
License: GPL-2+

License: GPL-2+ with OpenSSL exception
 This package is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 In addition:
 Permission is granted to link the code in this release with the
 OpenSSL project's 'OpenSSL' library and to distribute the linked
 executables.  Works derived from mktorrent may, at their authors'
 discretion, keep or delete this exception.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

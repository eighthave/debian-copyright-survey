Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mlpack
Upstream-Contact: Ryan Curtin <ryan@ratml.org>
Source: http://www.mlpack.org/
	git://github.com/mlpack/mlpack.git

Files: *
Copyright:
  2008-2015, Ryan Curtin <ryan@ratml.org>
  2008-2013, Bill March <march@gatech.edu>
  2008-2012, Dongryeol Lee <dongryel@cc.gatech.edu>
  2008-2013, Nishant Mehta <niche@cc.gatech.edu>
  2008-2013, Parikshit Ram <p.ram@gatech.edu>
  2010-2012, James Cline <james.cline@gatech.edu>
  2010-2013, Sterling Peet <sterling.peet@gatech.edu>
  2011-2012, Matthew Amidon <mamidon@gatech.edu>
  2011-2012, Neil Slagle <npslagle@gmail.com>
  2011, Ajinkya Kale <kaleajinkya@gmail.com>
  2011, Vlad Grantcharov <vlad321@gatech.edu>
  2011, Noah Kauffman <notoriousnoah@gmail.com>
  2012, Rajendran Mohan <rmohan88@gatech.edu>
  2012, Trironk Kiatkungwanglai <trironk@gmail.com>
  2012, Patrick Mason <patrick.s.mason@gmail.com>
  2013-2015, Marcus Edel <marcus.edel@fu-berlin.de>
  2013, Mudit Raj Gupta <mudit.raaj.gupta@gmail.com>
  2013, Sumedh Ghaisas <sumedhghaisas@gmail.com>
  2014, Michael Fox <michaelfox99@gmail.com>
  2014, Ryan Birmingham <birm@gatech.edu>
  2014, Siddharth Agrawal <siddharth.950@gmail.com>
  2014, Saheb Motiani <saheb210692@gmail.com>
  2014, Yash Vadalia <yashdv@gmail.com>
  2014, Abhishek Laddha <laddhaabhishek11@gmail.com>
  2014, Vahab Akbarzadeh <v.akbarzadeh@gmail.com>
  2014, Andrew Wells <andrewmw94@gmail.com>
  2014, Zhihao Lou <lzh1984@gmail.com>
  2014, Udit Saxena <saxenda.udit@gmail.com>
  2014-2015, Stephen Tu <tu.stephenl@gmail.com>
  2014-2015, Jaskaran Singh <jaskaranvirdi@ymail.com>
  2015, Shangtong Zhang <zhangshangtong.cpp@icloud.com>
  2015, Hritik Jain <hritik.jain.cse13@itbhu.ac.in>
  2015, Vladimir Glazachev <glazachev.vladimir@gmail.com>
  2015, QiaoAn Chen <kazenoyumechen@gmail.com>
  2015, Janzen Brewer <jahabrewer@gmail.com>
  2015, Trung Dinh <dinhanhtrung@gmail.com>
  2015, Tham Ngap Wei <thamngapwei@gmail.com>
  2015, Grzegorz Krajewski <krajekg@gmail.com>
  2015, Joseph Mariadassou <joe.mariadassou@gmail.com>
  2015, Pavel Zhigulin <pashaworking@gmail.com>
License: BSD-3-clause
  mlpack is provided without any warranty of fitness for any purpose.  You
  can redistribute the library and/or modify it under the terms of the 3-clause
  BSD license.  The text of the 3-clause BSD license is contained below.
  .
  ----
  Copyright (c) 2007-2014, mlpack contributors (see COPYRIGHT.txt)
  All rights reserved.
  .
  Redistribution and use of mlpack in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  .
  1. Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
  .
  2. Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
  .
  3. Neither the name of the copyright holder nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: Barak A. Pearlmutter <bap@debian.org>
License: Public-Domain
  This material is released to the public domain.

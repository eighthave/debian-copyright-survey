Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MUSTANG
Upstream-Contact: Arun S Konagurthu <arun@bx.psu.edu>
Source: http://www.csse.monash.edu.au/~karun/Site/mustang.html

Files: *
Copyright: 2005-2009 Arun S Konagurthu <arun@bx.psu.edu>
                     The University of Melbourne.
License: BSD-3-clause

Files: debian/*
Copyright: 2007-2009 Morten Kjeldgaard <mok@bioxray.dk>
           2008 Steffen Möller <moeller@debian.org>
           2010 Charles Plessy <plessy@debian.org>
           2008-2016 Andreas Tille <tille@debian.org>
License: BSD-3-clause

Files: data/test/pdbs/*
Copyright: PD
License: PD
 ADVISORY NOTICE FOR USE OF THE PDB ARCHIVE
 .
 By using the materials available in this archive, the user agrees to abide
 by the following conditions:
 .
 * The archival data files in the PDB archive are made freely available
 to all users. Data files within the archive may be redistributed in original
 form without restriction. Redistribution of modified data files using the same
 file name as is on the FTP server is prohibited. The rules for file names are
 detailed at http://www.wwpdb.org/wwpdb_charter.html.
 .
 * Data files containing PDB content may incorporate the PDB 4-letter entry
 name (e.g. 1ABC) in standard PDB records only if they are exactly the same
 as what is residing in the PDB archive. This does not prevent databases
 from including PDB entry_id's as cross-references where it is clear that
 they refer to the PDB archive. PDB records refer to the standard PDB format.
 The distribution of modified PDB format data including the records: HEADER,
 CAVEAT, REVDAT, SPRSDE, DBREF, SEQADV, and MODRES are not allowed.
 .
 * The user assumes all responsibility for insuring that intellectual property
 claims associated with any data set deposited in the PDB archive are honored.
 It should be understood that the PDB data files do not contain any information
 on intellectual property claims with the exception in some cases of a reference
 for a patent involving the structure.
 .
 * Any opinion, findings, and conclusions expressed in the PDB archive by the
 authors/contributors do not necessarily reflect the views of the wwPDB.
 .
 * The data in the PDB archive are provided on an "as is" basis.  The wwPDB
 nor its comprising institutions cannot be held liable to any party for direct,
 indirect, special, incidental, or consequential damages, including lost
 profits, arising from the use of PDB materials.
 .
 * Resources on this site are provided without warranty of any kind, either
 expressed or implied. This includes but is not limited to merchantability or
 fitness for a particular purpose. The institutions managing this site make
 no representation that these resources will not infringe any patent or other
 proprietary right.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * Neither the name of the University of Melbourne nor the names of its
   contributors may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.


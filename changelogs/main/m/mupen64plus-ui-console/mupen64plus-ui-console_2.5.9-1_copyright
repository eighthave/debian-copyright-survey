Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mupen64plus-ui-console
Upstream-Contact: Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
Source: https://github.com/mupen64plus/mupen64plus-ui-console/

Files: *
Copyright: 2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: data/icons/*
Copyright: 2008, Scott 'Tillin9' Knauert
License: GPL-2+

Files: debian/*
Copyright: 2009-2019, Sven Eckelmann <sven@narfation.org>
  2009-2019, Tobias Loose <TobiasLoose@gmx.de>
License: GPL-2+

Files: src/cheat.c
Copyright: 2010, Rhett 'spinout' Osborne
  2009, 2010, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: src/compare_core.c
Copyright: 2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
  2002, Hacktarux
License: GPL-2+

Files: src/main.c
Copyright: 2008, Ebenblues
  2008, John 'NMN' Chadwick
  2008, Scott 'okaygo' Gorman
  2008, Scott 'Tillin9' Knauert
  2007-2018, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
  2002, Hacktarux
License: GPL-2+

Files: src/main.h
  src/plugin.c
  src/plugin.h
Copyright: 2009, Richard 'Richard42' Goedeken <Richard@fascinationsoftware.com>
License: GPL-2+

Files: src/debugger.*
Copyright: 2014, Will Nayes
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: monopd
Source: http://gtkatlantic.gradator.net/downloads.html

Files: *
Copyright: 2001-2002, Rob Kaper <cap@capsi.com>
           2001,      Erik Bourget <ebourg@cs.mcgill.ca>
           2014-2015, Sylvain Rochet <gradator@gradator.net>
License: GPL-2

Files: src/listener.h
       src/listenport.cpp
       src/listener.cpp
       src/socket.h
       src/socket.cpp
       src/listenport.h
Copyright: 2002, Rob Kaper <cap@capsi.com>
License: BSD-2-clause

Files: debian/*
Copyright: 2003-2011, Daniel Schepler <schepler@debian.org>
           2011,      Paul Wise <pabs@debian.org>
           2015,      Peter Pentchev <roam@ringlet.net>
           2013-2015, Markus Koschany <apo@gambaru.de>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; see the file COPYING.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 The text of the GPL is available at /usr/share/common-licenses/GPL-2.

License: BSD-2-clause
 Copyright (c) 2002 Rob Kaper <rob@unixcode.org>. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS `AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

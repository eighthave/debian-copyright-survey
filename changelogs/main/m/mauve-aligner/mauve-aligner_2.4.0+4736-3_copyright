Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mauve
Upstream-Contact: Aaron Darling <darling@cs.wisc.edu>
Source: http://sourceforge.net/p/mauve/code/HEAD/tree/mauve/trunk/

Files: *
Copyright: 2003 - 2014 Aaron Darling -- darling@cs.wisc.edu
License: GPL-2+

Files: src/org/gel/mauve/gui/dnd/TransferableObject.java
Copyright: 2001 Robert Harder
License: CPL
 This code is licensed for public use under the Common Public License version 0.5.
 .
 The Common Public License, developed by IBM and modeled after their
 industry-friendly IBM Public License, differs from other common open source
 licenses in several important ways:
 .
  * You may include this software with other software that uses a different
    (even non-open source) license.
  * You may use this software to make for-profit software.
  * Your patent rights, should you generate patents, are protected.

Files: src/org/gel/mauve/gui/sequence/ZiggyRectangularBeadRenderer.java
Copyright: 2003-2014 Keith James, Aaron Darling
License: LGPL-3+

Files: src/org/gel/mauve/BrowserLauncher.java
Copyright: 1999-2002 Eric Albert <ejalbert@cs.stanford.edu>
License: BrowserLauncher
 This code may be redistributed or modified in any form without restrictions as long
 as the portion of this comment from this paragraph through the end of the
 comment is not removed. The author requests that he be notified of any
 application, applet, or other binary that makes use of this code, but that's
 more out of curiosity than anything and is not required. This software
 includes no warranty. The author is not repsonsible for any loss of data or
 functionality or any adverse or unexpected effects of using this software.
 .
 Credits:
 Steven Spencer, JavaWorld magazine
  http://www.javaworld.com/javaworld/javatips/jw-javatip66.html - Java Tip 66
 .
 Thanks also to Ron B. Yeh, Eric Shapiro, Ben Engber, Paul Teitlebaum, Andrea
 Cantatore, Larry Barowski, Trevor Bedzek, Frank Miedrich, Ron Rabakukk, and
 Glenn Vanderburg

Files: debian/*
Copyright: 2015 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the licenses can be found in:
 GPL-2 - `/usr/share/common-licenses/GPL-2'

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the license can be found in:
 `/usr/share/common-licenses/LGPL-3'


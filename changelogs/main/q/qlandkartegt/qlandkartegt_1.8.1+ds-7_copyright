Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: QLandkarteGT
Upstream-Contact: Oliver Eichler <oliver.eichler@gmx.de>
Source: http://sourceforge.net/projects/qlandkartegt/files/qlandkartegt/
Files-Excluded: src/*.cpp.orig

Files: *
Copyright: 2006-2014, Oliver Eichler <oliver.eichler@gmx.de>
           2008-2011, Marc Feld
                2011, Michael Klein <michael.klein@puffin.lb.shuttle.de>
           2009-2010, Joerg Wunsch <j@uriah.heep.sax.de>
          2008, 2010, Albrecht Dreß <albrecht.dress@arcor.de>
                2010, Christian Treffs <ctreffs@gmail.com>
                2008, Andrew Vagin <avagin@gmail.com>
License: GPL-3+

Files: calculate_completion.sh
Copyright: 2008, Klaas van Gend
License: public-domain
 This script is donated to the public domain

Files: licensecheck.pl
Copyright: 2007-2008, Adam D. Barratt
                2012, Francesco Poli
License: GPL-2+

Files: 3rdparty/getopt/*
Copyright: 2003-2004, froglogic Porten & Stadlbauer GbR
License: BSD-3-Clause

Files: 3rdparty/qtsoap/*
Copyright: 2010, Nokia Corporation and/or its subsidiary(-ies)
License: BSD-3-Clause

Files: 3rdparty/QZip/qzip*
 3rdparty/QTextHtmlExporter/QTextHtmlExporter.*
Copyright: 2010-2011, Nokia Corporation and/or its subsidiary(-ies)
License: QT-Commercial or LGPL-2.1 with Digia Qt LGPL Exception 1.1 or GPL-3

Files: 3rdparty/SerialPort/ManageSerialPort.*
Copyright: 2008, IANNEY-LIAUD Philippe <philippe.vianney.liaud gmail.com>
License: GPL-1+

Files: 3rdparty/SerialPort/qextserialport/*
Copyright: 2000-2003, Wayne Roth
           2004-2007, Stefan Sander
                2007, Michal Policht
                2008, Brandon Fosdick
           2009-2010, Liam Staskawicz
                2011, Debao Zhang
License: MIT

Files: 3rdparty/tzone/*
Copyright: 2010-2012, Konrad Rosenbaum
License: GPL-3+

Files: 3rdparty/tzone/db/yearistype.sh
Copyright: disclaimed
License: public-domain
 This file is in the public domain, so clarified as of
 2006-07-17 by Arthur David Olson.

Files: include/libexif/*
Copyright: 2001-2005, Lutz Mueller <lutz@users.sourceforge.net>
License: LGPL-2+

Files: include/libexif/exif-ifd.h
Copyright: 2002, Lutz Mueller <lutz@users.sourceforge.net>
License: LGPL-2.1+

Files: MacOSX/mklocversion.sh
Copyright: 2010, Albrecht Dreß <albrecht.dress@arcor.de>
License: GPL-2

Files: src/CTextEditWidget.*
Copyright: 2012, Digia Plc and/or its subsidiary(-ies)
License: QT-Commercial or LGPL-2.1 with Digia Qt LGPL Exception 1.1 or GPL-3

Files: Win32/stdint.h
Copyright: 2006-2008, Alexander Chemeris
License: BSD-3-Clause

Files: debian/*
Copyright: 2009-2012, Michael Hanke <michael.hanke@gmail.com>
           2012-2013, Jaromír Mikeš <mira.mikes@seznam.cz>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as published
 by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 or
 version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: QT-Commercial
 Commercial License Usage
 Licensees holding valid commercial Qt licenses may use this file in
 accordance with the commercial license agreement provided with the
 Software or, alternatively, in accordance with the terms contained in
 a written agreement between you and Digia. For licensing terms and
 conditions see http://qt.digia.com/licensing. For further information
 use the contact form at http://qt.digia.com/contact-us.

License: LGPL-2.1 with Digia Qt LGPL Exception 1.1
 Alternatively, this file may be used under the terms of the GNU Lesser
 General Public License version 2.1 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPL included in the
 packaging of this file. Please review the following information to
 ensure the GNU Lesser General Public License version 2.1 requirements
 will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Digia gives you certain additional
 rights. These rights are described in the Digia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 Digia Qt LGPL Exception version 1.1
 As an additional permission to the GNU Lesser General Public License version
 2.1, the object code form of a "work that uses the Library" may incorporate
 material from a header file that is part of the Library. You may distribute
 such object code under terms of your choice, provided that:
 (i) the header files of the Library have not been modified; and
 (ii) the incorporated material is limited to numerical parameters, data
 structure layouts, accessors, macros, inline functions and
 templates; and
 (iii) you comply with the terms of Section 6 of the GNU Lesser General
 Public License version 2.1.
 .
 Moreover, you may apply this exception to a modified version of the Library,
 provided that such modification does not involve copying material from the
 Library into the modified Library's header files unless such material is
 limited to (i) numerical parameters; (ii) data structure layouts;
 (iii) accessors; and (iv) small macros, templates and inline functions of
 five lines or less in length.
 .
 Furthermore, you are not required to apply this additional permission to a
 modified version of the Library.


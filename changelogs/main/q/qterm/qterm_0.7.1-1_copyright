Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qterm
Source: https://github.com/qterm/qterm

Files: *
Copyright: kingson <xiaokiangwang@yahoo.com.cn>
           fiasco <yinshouyi@peoplemail.com.cn>
           hooey <hephooey@hotmail.com>
           2001-2004, The QTerm Developers
           2003-2016 Xiaoqiang Wang <xiaoqiangwang@gmail.com>
License: GPL-2+ with OpenSSL exception

Files: debian/*
Copyright: 2002-2003 Anthony Fok <foka@debian.org>
           2003-2004 Xiaoqiang Wang <xiaokiangwang@yahoo.com.cn>
           2004 SU Yong <yoyosu@ustc.edu.cn>
           2004-2005 Yu Guanghui <ygh@debian.org>
           2005 Rob Bradford <robster@debian.org>
           2005 Alexander Schmehl <tolimar@debian.org>
           2005-2008 LI Daobing <lidaobing@gmail.com>
           2016 Boyuan Yang <073plan@gmail.com>
License: GPL-2+

Files: src/overlayWidget.* src/popupMessage.* src/progressBar.*
Copyright: 2005 Max Howell <max.howell@methylblue.com>
License: GPL-2+  

Files: src/osdmessage.*
Copyright: 2004 by Enrico Ros <eros.kde@email.it>
License: GPL-2+  

Files: src/ssh/crc32.*
Copyright: 2003 Markus Friedl
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License may be found in /usr/share/common-licenses/GPL-2.

License: GPL-2+ with OpenSSL exception
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 In addition, as a special exception, the author of this
 program gives permission to link the code of its
 release with the OpenSSL project's "OpenSSL" library (or
 with modified versions of it that use the same license as
 the "OpenSSL" library), and distribute the linked
 executables. You must obey the GNU General Public
 License in all respects for all of the code used other
 than "OpenSSL".  If you modify this file, you may extend
 this exception to your version of the file, but you are
 not obligated to do so.  If you do not wish to do so,
 delete this exception statement from your version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

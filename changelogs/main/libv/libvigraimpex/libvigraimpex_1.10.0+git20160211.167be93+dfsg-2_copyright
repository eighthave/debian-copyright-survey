Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: VIGRA
Upstream-Contact: Ullrich Köthe <ullrich.koethe@iwr.uni-heidelberg.de>
Source: https://github.com/ukoethe/vigra/
Files-Excluded: .gitignore
                .hgignore
                .hgtags
                .travis.yml
                appveyor.yml
                docsrc/documents/viff.ps
                docsrc/documents/DataAccessors.ps
                docsrc/documents/GenericProg2D.ps
                docsrc/documents/FunctorFactory.ps
                vigranumpy/docsrc/_static
                src/images/lenna_color.gif
                src/images/lenna_color_small.gif
                src/images/lenna_composite_color.gif
                src/images/lenna_gray.gif
                src/images/lenna_inverted.gif
                src/images/lenna_mirror_horizontal.gif
                src/images/lenna_mirror_vertical.gif
                src/images/lenna_small.gif
                src/images/lenna_smoothed.gif
                src/images/lenna_stripes.gif
                src/images/lenna_sub.gif
                src/images/lenna_transposed_major.gif
                src/images/lenna_transposed_minor.gif
                src/examples/lenna.bmp
                src/examples/lenna_color.gif
                src/examples/lenna_color_small.gif
                src/examples/lenna_gray.gif
                src/examples/lenna_small.gif
                src/examples/lenna_stripes.gif
                src/examples/lenna_sub.gif
                src/examples/lenna_transposed.gif
                src/examples/tutorial/lenna_color.gif
                src/examples/tutorial/lenna_color_small.gif
                src/examples/tutorial/lenna_composite_color.gif
                src/examples/tutorial/lenna_gray.gif
                src/examples/tutorial/lenna_inverted_color.gif
                src/examples/tutorial/lenna_small.gif
                src/examples/tutorial/lenna_smoothed.gif
                test/convolution/lenna128nonlinear.xv
                test/convolution/lenna128recgrad.xv
                test/convolution/lenna128rgbsepgrad.xv
                test/convolution/lenna128rgb.xv
                test/convolution/lenna128sepgrad.xv
                test/convolution/lenna128.xv
                test/convolution/lenna_gaussian_sharpening_orig.xv
                test/convolution/lennahessxx.xv
                test/convolution/lennahessxy.xv
                test/convolution/lennahessyy.xv
                test/convolution/lenna_level-1.xv
                test/convolution/lenna_level1.xv
                test/convolution/lenna_level-2.xv
                test/convolution/lenna_level2.xv
                test/convolution/lenna_levellap0.xv
                test/convolution/lenna_levellap1.xv
                test/convolution/lenna_levellap2.xv
                test/convolution/lennargbst.xv
                test/convolution/lenna_simple_sharpening_orig.xv
                test/convolution/lennastxx.xv
                test/convolution/lennastxy.xv
                test/convolution/lennastyy.xv
                test/imgproc/lenna128rgb.xv
                test/imgproc/lenna128.xv
                test/imgproc/lenna288neu.xv
                test/imgproc/lenna288rgbneu.xv
                test/imgproc/lenna288rgb.xv
                test/imgproc/lenna288.xv
                test/imgproc/lenna367FIR.xv
                test/imgproc/lenna367IIR.xv
                test/imgproc/lenna42FIR.xv
                test/imgproc/lenna42IIR.xv
                test/imgproc/lenna42linrgb.xv
                test/imgproc/lenna42lin.xv
                test/imgproc/lenna42neu.xv
                test/imgproc/lenna42rgbneu.xv
                test/imgproc/lenna42rgb.xv
                test/imgproc/lenna42.xv
                test/imgproc/lennargb42FIR.xv
                test/imgproc/lennargb42IIR.xv
                test/imgproc/lenna_rotate.xv
                test/slic2d/lenna.xv
                test/simpleanalysis/lenna128.xv
                test/image/lenna.gif
                test/image/lenna.xv
                test/impex/lenna_0.tif
                test/impex/lenna_1.tif
                test/impex/lenna_2.tif
                test/impex/lennafloatrgb.xv
                test/impex/lennafloat.xv
                test/impex/lenna_gifref.xv
                test/impex/lenna_masked_color.tif
                test/impex/lenna_masked_gray.tif
                test/impex/lennargb.xv
                test/impex/lenna.xv

Files: *
Comment:
  PostScript documents without source has been stripped from the Debian
  version of this package.
Copyright: 1998-2013            Ullrich Köthe
           1998-2002, 2007      Pablo d'Angelo
           2012-2013            Thorsten Beier
           2009-2011            Janis Fehr
           2008-2011            Michael Hanselmann
           2003-2004, 2006-2007 F. Heinrich
           2011                 Lukas Jirkovsky
           1998-2013            Gunnar Kedenburg
           2009-2013            Thorben Kroeger
           2012                 Frank Lenzen
           1998-2010            Hans Meine
           1999                 Paul Moore
           2003-2009            Rahul Nair
           2011-2012            Markus Nullmeier
           2003-2007            Christian-Dennis Rahn
           2010                 Joachim Schleicher
           2011-2012            Stefan Schmidt
           2003-2004, 2006-2007 B. Seppke
           2012                 Christoph Spiel
           2009                 Nico Splitthoff
           1993                 Sun Microsystems, Inc.
           2003-2007            Kasim Terzic
           2011                 Michael Tesch
License: MIT

Files: debian/*
Copyright: 1995-1996 Alejandro Sierra
           2009-2011 Jakub Wilk
           2011      Ralf Treinen
           2012-2013 Matthias Klose
           2013-2016 Andreas Metzler <ametzler@bebt.de>
           2015-2016 Daniel Stender <stender@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the “Software”), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

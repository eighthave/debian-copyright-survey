This is Debian GNU/Linux's prepackaged version of Finance::Quote.  This
is a set of perl modules which provide access to securities prices.

This package was put together by Ross Peachey <rcp@debian.org>, from
the original sources, available from:

     http://sourceforge.net/projects/finance-quote/

The library is also available from the Comprehensive Perl Archive
Network (CPAN). Visit <URL:http://www.perl.com/CPAN/> to find a CPAN
site near you.

Copyright 1998, Dj Padzensky
Copyright 1998, 1999 Linas Vepstas
Copyright 2000, Yannick LE NY (update for Yahoo Europe and YahooQuote)
Copyright 2000-2001, Paul Fenwick (updates for ASX, maintenance and release)
Copyright 2000-2001, Brent Neal (update for TIAA-CREF)
Copyright 2000 Volker Stuerzl (DWS and VWD support)
Copyright 2000 Keith Refson (Trustnet support)
Copyright 2001 Rob Sessink (AEX support)
Copyright 2001 Leigh Wedding (ASX updates)
Copyright 2001 Tobias Vancura (Fool support)
Copyright 2001 James Treacy (TD Waterhouse support)
Copyright (C) 1989, 1991 Free Software Foundation, Inc.
Copyright (C) 1998, 1999 Linas Vepstas <linas@linas.org>
Copyright (C) 1998, Dj Padzensky <djpadz@padz.net>
Copyright (C) 2000-2004, Paul Fenwick <pjf@cpan.org>
Copyright (C) 2000-2001, Brent Neal <brentn@users.sourceforge.net>
Copyright (C) 2000, Keith Refson <Keith.Refson@earth.ox.ac.uk>
Copyright (C) 2000, Volker Stuerzl <volker.stuerzl@gmx.de>
Copyright (C) 2000, Yannick LE NY <y-le-ny@ifrance.com>
Copyright (C) 2001, James Treacy <treacy@debian.org>
Copyright (C) 2001, Leigh Wedding <leigh.wedding@telstra.com>
Copyright (C) 2001, M.R.Muthu Kumar <m_muthukumar@users.sourceforge.net>
Copyright (C) 2001, Rob Sessink <rob_ses@users.sourceforge.net>
Copyright (C) 2001, Tobias Vancura <tvancura@altavista.net>
Copyright (C) 2002, Rainer Dorsch <rainer.dorsch@informatik.uni-stuttgart.de>
Copyright (C) 2003,2005,2006 Jörg Sommer <joerg@alea.gnuu.de>
Copyright (C) 2003, Ian Dall <ian@beware.dropbear.id.au>
Copyright (C) 2003, Pawel Konieczny <konieczp@users.sourceforge.net>
Copyright (C) 2003, Rob Clark <finiteautomaton@users.sourceforge.net>
Copyright (C) 2003, Tomas Carlsson <tc@tompa.nu>
Copyright (C) 2004, Frank Mori Hess <fmhess@users.sourceforge.net>
Copyright (C) 2004, Johan van Oostrum
Copyright (C) 2004, Michael Curtis 
Copyright (C) 2005  Knut Franke <Knut.Franke@gmx.de>
Copyright (C) 2005, Morten Cools <morten@cools.no>
Copyright (C) 2006, Dominique Corbex <domcox@sourceforge.net>
Copyright (C) 2006, Klaus Dahlke <klaus.dahlke@gmx.de>
Copyright (C) 2006, Mika Laari <mika.laari@iki.fi>
Copyright (C) 2008, Stephan Ebelt <stephan.ebelt@gmx.de>
Copyright (C) 2008, Bernard Fuentes <bernard.fuentes@gmail.com>
Copyright (C) 2008-2009 Adam Kennedy
Copyright (C) 2009, Herman van Rink
Copyright (C) 2008-2009, Erik Colson <eco@ecocode.net>
Copyright (C) 2009, Bradley Dean <bjdean@bjdean.id.au>

License:

   This package is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This package is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this package; if not, write to the Free Software
   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

The Debian packaging is
   Copyright (C) 2001-2003 Ross Peachey <rcp@debian.org>
   Copyright (C) 2006 Don Armstrong <don@debian.org>
   Copyright (C) 2006-2009 Bart Martens <bartm@knars.be>
and is licensed under the GPL version 2, see above.


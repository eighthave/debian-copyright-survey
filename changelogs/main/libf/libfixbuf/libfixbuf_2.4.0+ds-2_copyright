Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libfixbuf
Source: https://tools.netsa.cert.org/fixbuf/download.html
Files-Excluded:
 configure
 aclocal.m4
 m4/libtool.m4
 autoconf
 doc/html

Files: *
Copyright: 2005-2018 Carnegie Mellon University
License: LGPL-3.0
 NO WARRANTY. THIS CARNEGIE MELLON UNIVERSITY AND SOFTWARE
 ENGINEERING INSTITUTE MATERIAL IS FURNISHED ON AN "AS-IS"
 BASIS. CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND,
 EITHER EXPRESSED OR IMPLIED, AS TO ANY MATTER INCLUDING, BUT NOT
 LIMITED TO, WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY,
 EXCLUSIVITY, OR RESULTS OBTAINED FROM USE OF THE
 MATERIAL. CARNEGIE MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF
 ANY KIND WITH RESPECT TO FREEDOM FROM PATENT, TRADEMARK, OR
 COPYRIGHT INFRINGEMENT.
 .
 Released under a GNU-Lesser GPL 3.0-style license, please see
 LICENSE.txt or contact permission@sei.cmu.edu for full terms.
 .
 [DISTRIBUTION STATEMENT A] This material has been approved for
 public release and unlimited distribution.  Please see Copyright
 notice for non-US Government use and distribution.
 .
 Carnegie Mellon(R) and CERT(R) are registered in the U.S. Patent
 and Trademark Office by Carnegie Mellon University.

Files: m4/lt~obsolete.m4
       m4/ltoptions.m4
       m4/ltsugar.m4
       m4/ltversion.m4
Copyright: 2004-2005, 2007, 2009, 2011-2015 Free Software Foundation, Inc.
License: permissive1
 This file is free software; the Free Software Foundation gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.

Files: Makefile.in
       src/infomodel/Makefile.in
       src/Makefile.in
Copyright: 1994-2015 Free Software Foundation, Inc.
License: permissive2
 This Makefile.in is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: debian/*
Copyright: 2013-2018 Johannes Schauer <j.schauer@email.de>
License: LGPL-2.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

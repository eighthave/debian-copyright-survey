Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Encode-Base58
Upstream-Contact: Tatsuhiko Miyagawa <miyagawa@bulknews.net>
Upstream-Name: Encode-Base58

Files: *
Copyright: 2009, Tatsuhiko Miyagawa <miyagawa@bulknews.net>
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: inc/Module/*
Copyright: 2002-2009, Adam Kennedy <adamk@cpan.org>
 2002-2009, Audrey Tang <autrijus@autrijus.org>
 2002-2009, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Spiffy.pm
Copyright: 2004-2006, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/Base.pm inc/Test/Base/*
Copyright: 2005-2008, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Test/More.pm
Copyright: 2001-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: inc/Test/Builder.pm inc/Test/Builder/*
Copyright: 2002-2008, chromatic <chromatic@wgz.org>
 2002-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2018, Laurent Baillet <laurent.baillet@gmail.com>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

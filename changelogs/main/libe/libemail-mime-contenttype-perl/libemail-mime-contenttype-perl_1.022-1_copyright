Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Email-MIME-ContentType
Upstream-Contact: Ricardo Signes, rjbs@cpan.org
Source: https://metacpan.org/release/Email-MIME-ContentType

Files: *
Copyright: 2004, Simon Cozens <simon@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 2005-2008, Ernesto Hernández-Novich (USB) <mhn@telcel.net.ve>
 2007-2008, other members of the Debian Perl Group, cf. debian/changelog
 2013-2017, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Embperl
Upstream-Contact: Gerald Richter <richter@embperl.org>
Source: https://metacpan.org/release/Embperl/

Files: *
Copyright: 1997-2008, Gerald Richter / ECOS Technology GmbH <http://www.ecos.de>
 2008-2014, Gerald Richter <richter@embperl.org>
License: Artistic or GPL-1+

Files: eg/forms/js/prototype.js
Copyright: 2005-2007, Sam Stephenson
License: MIT-style

Files: debian/*
Copyright: 2003-2007, Angus Lees <gus@debian.org>
 2005-2008, Marc 'HE' Brockschmidt <he@debian.org>
 2007-2009, Gunnar Wolf <gwolf@debian.org>
 2009, Ryan Niebur <ryan@debian.org>
 2009-2014, gregor herrmann <gregoa@debian.org>
 2011, Dominic Hargreaves <dom@earth.li>
 2011, Jonathan Yu <jawnsy@cpan.org>
 2012-2014, Florian Schlichting <fsfs@debian.org>
 2014, Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: MIT-style
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

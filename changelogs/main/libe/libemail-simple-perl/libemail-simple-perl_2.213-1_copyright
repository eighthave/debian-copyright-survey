Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Email-Simple
Upstream-Contact: Ricardo Signes <rjbs@cpan.org>
Source: https://metacpan.org/release/Email-Simple

Files: *
Copyright: 2003, Simon Cozens <simon@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2004, S. Zachariah Sprackett <zac@sprackett.com>
           2005, Paul Wise <pabs3@bonedaddy.net>
           2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
           2006, Niko Tyni <ntyni@iki.fi>
           2006-2017, gregor herrmann <gregoa@debian.org>
           2007, Jeremiah Foster <jeremiah@jeremiahfoster.com>
           2008, Ansgar Burchardt <ansgar@debian.org>
           2008, Rene Mayorga <rmayorga@debian.org.sv>
           2009, Brian Cassidy <brian.cassidy@gmail.com>
           2009, Jonathan Yu <jawnsy@cpan.org>
           2009-2012, Nathan Handler <nhandler@ubuntu.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kiwix-lib
Upstream-Contact: https://github.com/kiwix/kiwix-lib/issues
Source: https://github.com/kiwix/kiwix-lib

Files: *
Copyright: 2011-2014 Emmanuel Engelhart <kelson@kiwix.org>
           2016-2018 Matthieu Gautier <mgautier@kymeria.fr>
License: GPL-3+

Files: include/ctpp2/CTPP2VMStringLoader.hpp
Copyright: 2013 Renaud Gaudin <reg@kiwix.org>
License: GPL-3+

Files: src/common/base64.cpp
Copyright: 2004-2008 René Nyffenegger
License: base64-license

Files: src/xapian/htmlparse.cc
Copyright: 1999-2001 BrightStation PLC
           2001 Ananova Ltd
           2002, 2006-2008 Olly Betts
License: GPL-2+

Files: src/xapian/htmlparse.h
Copyright: 1999-2001 BrightStation PLC
           2002, 2006, 2008 Olly Betts
License: GPL-2+

Files: src/xapian/myhtmlparse.cc src/xapian/myhtmlparse.h
Copyright: 1999-2001 BrightStation PLC
           2002-2004, 2006-2008 Olly Betts
License: GPL-2+

Files: src/xapian/namedentities.h
Copyright: 2006-2007 Olly Betts
License: GPL-2+

Files: subprojects/gtest/*
Copyright: 2008, Google Inc.
License: BSD-3-Clause

Files: subprojects/gtest/googlemock/scripts/generator/*
Copyright: 2007, Neal Norwitz
           2007-2008, Google Inc.
License: Apache-2.0

Files: subprojects/gtest/googletest/scripts/upload.py
       subprojects/gtest/googlemock/scripts/upload.py
Copyright: 2007, Google Inc.
License: Apache-2.0

Files: test/parseUrl.cpp
Copyright: 2013 Tommi Maekitalo
License: GPL-2+

Files: debian/*
Copyright: 2017-2019 Kunal Mehta <legoktm@debian.org>
License: GPL-3+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: base64-license
 This source code is provided 'as-is', without any express or implied
 warranty. In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this source code must not be misrepresented; you must not
   claim that you wrote the original source code. If you use this source code
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original source code.
 .
 3. This notice may not be removed or altered from any source distribution.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file
 `/usr/share/common-licenses/Apache-2.0`.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Imager
Upstream-Contact: Tony Cook <tonyc@cpan.org>
Source: https://metacpan.org/release/Imager
Comment: The +dfsg version is created by removing the non-free file
 FT2/fontfiles/MMOne.pfb. debian/repack.sh automates this process.
 Also the BaKoMa fonts collection appears to be non-free so removing
 T1/fontfiles/dcr10.afm

Files: *
Copyright:
 1999-2004, Arnar M. Hrafnkelsson
 2004-2013, Tony Cook <tonyc@cpan.org>
License: Artistic or GPL-1+

Files: ppport.h
Copyright:
 2004-2013, Marcus Holland-Moritz <mhx-cpan@gmx.net>
 2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
 1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files: lib/Imager/Color/Table.pm
Copyright: 2004, X Consortium
License: other
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 X Window System is a trademark of X Consortium, Inc.

Files: scale.im
Copyright: 1989, 1991, by Jef Poskanzer
License: other
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation.  This software is provided "as is" without express or
 implied warranty.

Files: inc/Devel/CheckLib.pm
Copyright: 2007, David Cantrell
 Portions Copyright 2007, David Golden
License: Artistic or GPL-1+

Files: debian/*
Copyright:
 2004-2007, Jay Bonci <jaybonci@debian.org>
 2004, Michael K. Edwards <medwards-debian@sane.net>
 2004, Steve Langasek <vorlon@debian.org>
 2007, Gunnar Wolf <gwolf@debian.org>
 2008-2014, gregor herrmann <gregoa@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
 2008, Rene Mayorga <rmayorga@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2009, Jonathan Yu <jawnsy@cpan.org>
 2009, Krzysztof Krzyżaniak (eloy) <eloy@debian.org>
 2010, Chris Butler <chrisb@debian.org>
 2010-2011, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

Files: debian/repack.stub
Copyright: 2009, Ryan Niebur <ryan@debian.org>
License: Artistic or GPL-1+

Files: debian/dh_perl_imager.in debian/perl_imager.pm
Copyright: 2010, Ansgar Burchardt <ansgar@debian.org>
 2012, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.

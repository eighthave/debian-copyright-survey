This package was debianized by Christian Bayle 
<bayle@debian.org>, Mon,  7 Jul 2003 00:47:01 +0200

It was downloaded from http://itextpdf.com/download.php


 * Copyright (c) 1998-2011 1T3XT BVBA
 * Authors: Bruno Lowagie, Paulo Soares, et al.

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3
 * as published by the Free Software Foundation with the addition of the
 * following permission added to Section 15 as permitted in Section 7(a):
 * FOR ANY PART OF THE COVERED WORK IN WHICH THE COPYRIGHT IS OWNED BY 1T3XT,
 * 1T3XT DISCLAIMS THE WARRANTY OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses or write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA, 02110-1301 USA, or download the license from the following URL:
 * http://itextpdf.com/terms-of-use/
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License,
 * a covered work must retain the producer line in every PDF that is created
 * or manipulated using iText.
 *
 * You can be released from the requirements of the license by purchasing
 * a commercial license. Buying such a license is mandatory as soon as you
 * develop commercial activities involving the iText software without
 * disclosing the source code of your own applications.
 * These activities include: offering paid services to customers as an ASP,
 * serving PDFs on the fly in a web application, shipping iText with a closed
 * source product.
 *
 * For more information, please contact iText Software Corp. at this
 * address: sales@itextpdf.com


-----------------------------------------------------------------------

The following are under different licenses:

(1)

ExceptionConverter:
The original version of this class was published in an article by Heinz Kabutz.
Read http://www.javaspecialists.co.za/archive/newsletter.do?issue=033&print=yes&locale=en_US
"This material from The Java(tm) Specialists' Newsletter by Maximum Solutions
(South Africa). Please contact Maximum Solutions  for more information.


(2)

SimpleXMLParser:
The original version of this class was published in a JavaWorld article by Steven Brandt:
http://www.javaworld.com/javaworld/javatips/jw-javatip128.html
Jennifer Orr (JavaWorld) wrote: "You have permission to use the code appearing in
Steven Brandt's JavaWorld article, 'Java Tip 128: Create a quick-and-dirty XML parser.'
We ask that you reference the author as the creator and JavaWorld as the original publisher
of the code." Steven Brandt also agreed with the use of this class.

 * The code to recognize the encoding in this class and in the convenience 
 * class IanaEncodings was taken from Apache Xerces published under the 
 * following license:
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

On Debian systems, the complete text of the Apache License can be found
in `/usr/share/common-licenses/Apache-2.0'.


(3)

The following files contain material that was copyrighted by SUN:

com/itextpdf/text/pdf/LZWDecoder.java
com/itextpdf/text/pdf/codec/LZWStringTable.java
com/itextpdf/text/pdf/codec/TIFFFaxDecompressor.java
com/itextpdf/text/pdf/codec/LZWCompressor.java
com/itextpdf/text/pdf/codec/TIFFField.java
com/itextpdf/text/pdf/codec/TIFFLZWDecoder.java
com/itextpdf/text/pdf/codec/BitFile.java
com/itextpdf/text/pdf/codec/TIFFDirectory.java
com/itextpdf/text/pdf/codec/TIFFFaxDecoder.java

The original code was released under the BSD license, and contained the following
extra restriction: "You acknowledge that Software is not designed, licensed or intended
for use in the design, construction, operation or maintenance of any nuclear facility."

In a mail sent to Bruno Lowagie on January 23, 2008, Brian Burkhalter (@sun.com)
writes: "This code is under a BSD license and supersedes the older codec packages
on which your code is based. It also includes numerous fixes among them being the
ability to handle a lot of 'broken' TIFFs."

Note that numerous fixes were applied to the code used in iText by Paulo Soares,
but apart from the fixes there were no essential changes between the code that
was originally adapted and the code that is now available under the following
license:

 Copyright (c) 2005 Sun Microsystems, Inc. All  Rights Reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met: 
 
 - Redistribution of source code must retain the above copyright 
   notice, this  list of conditions and the following disclaimer.
 
 - Redistribution in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in 
   the documentation and/or other materials provided with the
   distribution.
 
 Neither the name of Sun Microsystems, Inc. or the names of 
 contributors may be used to endorse or promote products derived 
 from this software without specific prior written permission.
 
 This software is provided "AS IS," without a warranty of any 
 kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND 
 WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, 
 FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
 EXCLUDED. SUN MIDROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL 
 NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF 
 USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR 
 ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL,
 CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND
 REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR
 INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGES. 
 
 You acknowledge that this software is not designed or intended for 
 use in the design, construction, operation or maintenance of any 
 nuclear facility.

The main difference can be found in the final paragraph: the restriction
that the source code is not "licensed" in this particular situation has
been removed.

FYI: Brian also added: "A bit of history might be in order.
The codec classes that you used originally were based on some
classes included with JAI but not strictly part of JAI.
As of Java SE 1.4 an official Image I/O framework was
added in javax.imageio.... This frameork supports these formats:

Java 1.4: GIF (read only), JPEG, PNG
Java 1.5: Added support for BMP and WBMP
Java 1.6: Added support for writing GIF

The JAI Image I/O Tools packages (jai-imageio-core) were created
to support formats handled by JAI but not included in Java SE
as well as some new things like JPEG2000."


(4) the file com/itextpdf/text/pdf/codec/TIFFConstants
and some other TIFF related code is derived from LIBTIFF:

 Copyright (c) 1988-1997 Sam Leffler
 Copyright (c) 1991-1997 Silicon Graphics, Inc.

 Permission to use, copy, modify, distribute, and sell this software and 
 its documentation for any purpose is hereby granted without fee, provided
 that (i) the above copyright notices and this permission notice appear in
 all copies of the software and related documentation, and (ii) the names of
 Sam Leffler and Silicon Graphics may not be used in any advertising or
 publicity relating to the software without the specific, prior written
 permission of Sam Leffler and Silicon Graphics.
 
 THE SOFTWARE IS PROVIDED "AS-IS" AND WITHOUT WARRANTY OF ANY KIND, 
 EXPRESS, IMPLIED OR OTHERWISE, INCLUDING WITHOUT LIMITATION, ANY 
 WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  
 
 IN NO EVENT SHALL SAM LEFFLER OR SILICON GRAPHICS BE LIABLE FOR
 ANY SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY KIND,
 OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER OR NOT ADVISED OF THE POSSIBILITY OF DAMAGE, AND ON ANY THEORY OF 
 LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
 OF THIS SOFTWARE.


(5)

BidiOrder:
As stated in the Javadoc comments, materials from Unicode.org
are used in the class com/itextpdf/text/pdf/BidiOrder.java
The following license applies to these materials:
 http://www.unicode.org/copyright.html#Exhibit1
  
 EXHIBIT 1
 UNICODE, INC. LICENSE AGREEMENT - DATA FILES AND SOFTWARE
 
 Unicode Data Files include all data files under the directories
 http://www.unicode.org/Public/, http://www.unicode.org/reports/,
 and http://www.unicode.org/cldr/data/ .
 Unicode Software includes any source code published in the Unicode Standard
 or under the directories http://www.unicode.org/Public/, http://www.unicode.org/reports/,
 and http://www.unicode.org/cldr/data/.
 
 NOTICE TO USER: Carefully read the following legal agreement. BY DOWNLOADING,
 INSTALLING, COPYING OR OTHERWISE USING UNICODE INC.'S DATA FILES ("DATA FILES"),
 AND/OR SOFTWARE ("SOFTWARE"), YOU UNEQUIVOCALLY ACCEPT, AND AGREE TO BE BOUND BY,
 ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT. IF YOU DO NOT AGREE, DO NOT
 DOWNLOAD, INSTALL, COPY, DISTRIBUTE OR USE THE DATA FILES OR SOFTWARE.
 
 COPYRIGHT AND PERMISSION NOTICE
 Copyright (C) 1991-2007 Unicode, Inc. All rights reserved. Distributed under
 the Terms of Use in http://www.unicode.org/copyright.html.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of the Unicode data files and any associated documentation (the "Data Files")
 or Unicode software and any associated documentation (the "Software") to deal
 in the Data Files or Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, and/or sell copies
 of the Data Files or Software, and to permit persons to whom the Data Files
 or Software are furnished to do so, provided that (a) the above copyright
 notice(s) and this permission notice appear with all copies of the Data Files
 or Software, (b) both the above copyright notice(s) and this permission notice
 appear in associated documentation, and (c) there is clear notice in each
 modified Data File or in the Software as well as in the documentation associated
 with the Data File(s) or Software that the data or software has been modified.
 
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE
 LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
 DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THE DATA FILES OR SOFTWARE.
 
 Except as contained in this notice, the name of a copyright holder shall not
 be used in advertising or otherwise to promote the sale, use or other dealings
 in these Data Files or Software without prior written authorization of the
 copyright holder.


(6)
Some files use code from different Apache projects:
com/itextpdf/text/pdf/IntHashtable.java
com/itextpdf/text/pdf/qrcode/*
com/itextpdf/text/pdf/hyphenation/HyphenationException.java
com/itextpdf/text/pdf/hyphenation/PatternConsumer.java
com/itextpdf/text/pdf/hyphenation/Hyphenator.java
com/itextpdf/text/pdf/hyphenation/Hyphen.java
com/itextpdf/text/pdf/hyphenation/CharVector.java
com/itextpdf/text/pdf/hyphenation/TernaryTree.java
com/itextpdf/text/pdf/hyphenation/Hyphenation.java
com/itextpdf/text/pdf/hyphenation/HyphenationTree.java
com/itextpdf/text/pdf/hyphenation/ByteVector.java
com/itextpdf/text/xml/XmlDomWriter.java

The source code of these files contains the appropriate copyright notices
as described in the Appendix of http://www.apache.org/licenses/LICENSE-2.0

They are

  Copyright 1999-2004 The Apache Software Foundation.

and

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

On Debian systems, the complete text of the Apache License can be found
in `/usr/share/common-licenses/Apache-2.0'.


(7)
Files:
com/itextpdf/text/pdf/fonts/cmaps/

 * Copyright (c) 2005, www.fontbox.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of fontbox; nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * http://www.fontbox.org


(8)
src/main/resources/com/itextpdf/text/pdf/fonts/*.afm
src/main/resources/com/itextpdf/text/pdf/fonts/mustRead.html

Are subjec to the following, where "This file" refers to the html
file:

This file and the 14 PostScript(R) AFM files it accompanies may be
used, copied, and distributed for any purpose and without charge, with
or without modification, provided that all copyright notices are
retained; that the AFM files are not distributed without this file;
that all modifications to this file or any of the AFM files are
prominently noted in the modified file(s); and that this paragraph is
not modified. Adobe Systems has no responsibility or obligation to
support the use of the AFM files.


(9)
src/main/resources/com/itextpdf/text/pdf/fonts/*.cmap
src/main/resources/com/itextpdf/text/pdf/fonts/*.properties
src/main/resources/com/itextpdf/text/pdf/fonts/cmaps/*

The *.cmap and *.properties files in this jar are necessary
to produce PDF files with iText that use CJK fonts.

Note that whatever value you pass for the 'embedded' parameter
with the method BaseFont.createFont, the font WILL NOT BE embedded.
To understand why, please read the following information.

The fonts that are used in Acrobat Reader when viewing a PDF file
that uses CJK fonts will be downloaded in one or more font packs.
You can download these fontpacks yourself from this URL:
http://www.adobe.com/products/acrobat/acrrasianfontpack.html

On this page, you can find the following information:
"If the author of an Adobe (R) Portable Document Format (PDF) file
embeds CJK and Central European (CE) fonts in a PDF document, then
any language version of Adobe Reader (R) software will be able to
display the CJK and CE text on any system without additional software.

If the author of the PDF document uses CJK or CE fonts but does not
embed them in the document, then the correct fonts will need to be
installed in order to view the Adobe PDF file on non-native systems."

When you download one of the font packs, for instance the font
pack for "Chinese Simplified", you will see that the fonts are
licensed for use in Adobe Reader only:

"Note: The font software contained in this package is being licensed
to you solely for use with Adobe (R) Acrobat (R) Reader (R) software
("Acrobat Reader") and is subject to the terms and conditions of
the electronic End-User License Agreement accompanying Acrobat Reader."

This explains why iText doesn't ever embed a CJK font in the PDF file.
These fonts have to be downloaded and used in the context of Adobe
Reader; you can not use them with iText to produce a PDF document
that has these fonts embedded (as you would do with other fonts) so
that they can be viewed in other readers; unless you have a license
from Adobe to use these fonts.

The *.cmap and *.properties files in this jar, do not contain
any font program. They contain information (mappings, metrics,...)
that is based on font information distributed on Adobe's site:
http://partners.adobe.com/public/developer/acrobat/index_advanced.html#pci

The original copyright notice of the mappings is as follows:
"Copyright 1990-2000 Adobe Systems Incorporated.
 All Rights Reserved.

 Patents Pending

 NOTICE: All information contained herein is the property
 of Adobe Systems Incorporated.

 Permission is granted for redistribution of this file
 provided this copyright notice is maintained intact and
 that the contents of this file are not altered in any
 way from its original form.

 PostScript and Display PostScript are trademarks of
 Adobe Systems Incorporated which may be registered in
 certain jurisdictions."

The original files with the mappings are plain text files,
and therefore not optimized for being read by a computer
software program. That's why they were pre-processed to map
directly the Unicode value with the CID value using a 64k
char array. No data was changed in this process.

Additionally, the iTextAsian.jar contains some properties files
with font metrics. These are included for the same reason AFM
files are needed (see also the file mustRead.html shipped with
the iText.jar). As defined in the PDF reference: "The width
information for each glyph is stored both in the font dictionary
and in the font program itself. (The two sets of widths must be
identical; storing this information in the font dictionary, although
redundant, enables a consumer application to determine glyph
positioning without having to look inside the font program.)"
See PDF Reference sixth edition section 5.1.3 (p393-394).

Whereas in the case of CJK fonts, the font program is subject
to the Adobe Reader EULA, the font metrics aren't. Page 396:
"Glyph metric information is also available separately in the
form of Adobe font metrics (AFM) and Adobe composite font metrics
(ACFM) files. These files are for use by application programs
that generate PDF page descriptions and must make formatting
decisions based on the widths and other metrics of glyphs. (...)
Specifications for the AFM and ACFM file formats are available
in Adobe Technical Note #5004, Adobe Font Metrics File Format
Specification; the files can be obtained from the Adobe Solutions
Network Web site."

Unfortuntately the URL of these files has changed over time, and
some metrics files seem to have been removed. However, you'll
find sufficient information in the Technical Notes to build your
own AFM and/or ACFM files if you ever need font metrics in the
Adobe Font Metrics format.

Note that the properties files in the iTextAsian.jar contain
font metrics, but they are not stored in the AFM or ACFM format.
For reasons of performance, the font metrics were stored as
key-value pairs. Compare the keys in the properties files with
the keys mentioned in Table 5.19 on p456 of the PDF Reference.
This way, the necessary key-value pairs can be imported directly
into a Font Dictionary that is part of a PDF file created by iText.

These specific metrics files were created by Paulo Soares and
may be used, copied, and distributed for any purpose and without
charge, with or without modification.


(10)
src/main/resources/com/itextpdf/text/pdf/fonts/glyphlist.txt

The information used to create this file is Adobe documentation
covered by the following license:

# ###################################################################################
# Copyright (c) 1997,1998,2002,2007 Adobe Systems Incorporated
# 
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this documentation file to use, copy, publish, distribute,
# sublicense, and/or sell copies of the documentation, and to permit
# others to do the same, provided that:
# - No modification, editing or other alteration of this document is
# allowed; and
# - The above copyright notice and this permission notice shall be
# included in all copies of the documentation.
# 
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this documentation file, to create their own derivative works
# from the content of this document to use, copy, publish, distribute,
# sublicense, and/or sell the derivative works, and to permit others to do
# the same, provided that the derived work is not represented as being a
# copy or version of this document.
# 
# Adobe shall not be liable to any party for any loss of revenue or profit
# or for indirect, incidental, special, consequential, or other similar
# damages, whether based on tort (including without limitation negligence
# or strict liability), contract or other legal or equitable grounds even
# if Adobe has been advised or had reason to know of the possibility of
# such damages.Ê The Adobe materials are provided on an "AS IS" basis.Ê
# Adobe specifically disclaims all express, statutory, or implied
# warranties relating to the Adobe materials, including but not limited to
# those concerning merchantability or fitness for a particular purpose or
# non-infringement of any third party rights regarding the Adobe
# materials.
# ###################################################################################
# Name:          Adobe Glyph List
# Table version: 2.0
# Date:          September 20, 2002
#
# See http://partners.adobe.com/asn/developer/typeforum/unicodegn.html
#
# Format: Semicolon-delimited fields:
#            (1) glyph name
#            (2) Unicode scalar value

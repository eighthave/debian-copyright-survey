Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IMAP-Admin
Upstream-Contact: Eric Estabrooks <eric@urbanrage.com>
Source: https://metacpan.org/release/IMAP-Admin

Files: *
Copyright: 1998-2009, Eric Estabrooks <eric@urbanrage.com>
License: Artistic
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: debian/*
Copyright: 2012, Florian Schlichting <fschlich@zedat.fu-berlin.de>
 2009, Jonathan Yu <jawnsy@cpan.org>
 2008, Rene Mayorga <rmayorga@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2004, Joachim Breitner <nomeata@debian.org>
 2003, Daniel Schepler <schepler@debian.org>
 2000, Alan Dorman <mdorman@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

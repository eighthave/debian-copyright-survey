Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mermaid
Upstream-Contact: Knut Sveidqvist
Source: https://github.com/knsv/mermaid
 https://registry.npmjs.org/webpack-node-externals
 https://registry.npmjs.org/scope-css
 https://github.com/sindresorhus/crypto-random-string/tags
 https://github.com/simov/slugify/tags
 https://github.com/kobezzza/Escaper/tags
 https://github.com/sindresorhus/strip-css-comments/tags
 https://github.com/sindresorhus/is-regexp/tags
 https://github.com/braintree/sanitize-url/
 https://registry.npmjs.org/khroma
 https://github.com/thysultan/stylis.js/
 https://github.com/shrpne/entity-decode/
Files-Excluded: dist/*.js dist/*.map  dist_dev
Files-Excluded-escaper: dist/escaper.min.js

Files: *
Copyright: 2014-2018, Knut Sveidqvist
License: Expat

Files: crypto-random-string/*
 is-regexp/*
 strip-css-comments/*
Copyright: Sindre Sorhus <sindresorhus@gmail.com>
License: Expat

Files: escaper/*
Copyright: 2014-2019, Andrey Kobets
License: Expat

Files: escaper/predefs/src/ws/jasmine.js
Copyright: 2013, The Closure Compiler Authors
License: Apache-2.0

Files: sanitize-url/*
Copyright: 2017, Braintree
License: Expat

Files: slugify/*
Copyright: Simeon Velichkov <simeonvelichkov@gmail.com>
License: Expat

Files: strip-css-comments/fixture.css
Copyright: 2011-2014, Twitter, Inc.
License: Expat

Files: webpack-node-externals/*
Copyright: 2016, Liad Yosef
License: Expat

Files: khroma/*
Copyright: 2019-present Fabio Spampinato, Andrew Maney
License: Expat

Files: stylis/*
Copyright: 2016-present, Sultan Tarimo
License: Expat

Files: entity-decode/*
Copyright: 2018, shrpne <shrpne@gmail.com>
License: Expat

Files: scope-css/*
Copyright: 2020 Dmitry Ivanov
License: Expat
Comment: License has been added to git-repo: https://github.com/dy/scope-css/blob/master/LICENSE but not yet released at npmjs

Files: khroma/src/methods/mix.ts
 khroma/dist/methods/mix.js
Copyright: 2016, Google Inc.
License: Expat
Comment: "SOURCE" points to this file: https://github.com/sass/dart-sass/blob/master/LICENSE

Files: khroma/dist/utils/channel.js
 khroma/src/utils/channel.ts
Copyright: Fabio Spampinato, Andrew Maney, Michael Jackson
License: Expat
Comment: Michael Jackson is the owner of https://gist.github.com/mjackson/5311256 mentioned in "SOURCE". Code for https://planetcalc.com/7779/ mentioned in "SOURCE" seems to have been taken up from the webpage's public frontend code - hence not mentioning this.

Files: debian/*
Copyright: 2020, Nilesh Patra <npatra974@gmail.com>
License: Expat

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

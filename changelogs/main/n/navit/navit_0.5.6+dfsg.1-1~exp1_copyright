Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded: navit/support/win32/ConvertUTF.c
 gradle/wrapper/gradle-wrapper.jar
Upstream-Name: navit
Upstream-Contact: https://github.com/navit-gps/navit
Source: https://github.com/navit-gps/navit

Files: *
Copyright: © 2005-2015 Navit Team
License: GPL-2

Files: navit/*.h
Copyright: © 2005-2015 Navit Team
License: LGPL-2

Files: navit/atom.h navit/cache.h navit/command.h navit/event_glib.h
 navit/keys.h navit/navit_nls.h navit/window.h
Copyright: © 2005-2015 Navit Team
License: GPL-2

Files: navit/graphics/sdl/graphics_sdl.c
Copyright: © 2005-2008 Bryan Rittmeyer <bryanr@bryanr.org>
           © 2008-2015 Navit Team
License: GPL-2

Files: navit/graphics/sdl/raster.*
Copyright: © 2005-2008 Bryan Rittmeyer <bryanr@bryanr.org>
           © 2008-2015 Navit Team
License: LGPL-2

Files: navit/map/garmin/*
Copyright: © 2007 Alexander Atanasov <aatanasov@gmail.com>
           © 2008-2011 Navit Team
License: GPL-2

Files: navit/vehicle/webos/cJSON.*
Copyright: © 2009 Dave Gamble
License: Expat

Files: contrib/sjjb_import.pl
Copyright: © 2012 Lutz Möhrmann
License: GPL-2

Files: navit/icons/archaeological_site.svg
 navit/icons/bahai.svg
 navit/icons/bicycle_rental.svg
 navit/icons/bicycle_shop.svg
 navit/icons/biergarten.svg
 navit/icons/buddhist.svg
 navit/icons/bus_stop.svg
 navit/icons/car_dealer.svg
 navit/icons/car_rent.svg
 navit/icons/car_sharing.svg
 navit/icons/casino.svg
 navit/icons/castle.svg
 navit/icons/cave.svg
 navit/icons/dam.svg
 navit/icons/daymark.svg
 navit/icons/drinking_water.svg
 navit/icons/emergency.svg
 navit/icons/hindu.svg
 navit/icons/islamic.svg
 navit/icons/jain.svg
 navit/icons/jewish.svg
 navit/icons/library.svg
 navit/icons/memorial.svg
 navit/icons/picnic.svg
 navit/icons/rail_station.svg
 navit/icons/shelter.svg
 navit/icons/shinto.svg
 navit/icons/shop_computer.svg
 navit/icons/shop_department.svg
 navit/icons/sikh.svg
 navit/icons/sport.svg
 navit/icons/theater.svg
 navit/icons/townhall.svg
 navit/icons/worship.svg
 navit/textures/cemetery.svg
 navit/textures/diagonal-stripes-gray.svg
 navit/textures/diagonal-stripes.svg
 navit/textures/quarry.svg
 navit/textures/scrub.svg
 navit/textures/wetland.svg
 navit/textures/wood.svg
Copyright: Public Domain
License: CC0-1.0
Comment:
 Map icons CC-0 from SJJB Management:
 http://www.sjjb.co.uk/mapicons/

Files: navit/support/shapefile/*
Copyright: © 1999-2008, Frank Warmerdam
License: Expat or LGPL
Comment:
 This software is available under the following "MIT Style" [0] license,
 or at the option of the licensee under the LGPL (see LICENSE.LGPL).  This
 option is discussed in more detail in shapelib.html.
 .
 [0] Read "Expat".

Files: navit/fib-1.1/*
Copyright: © 1997-2003 John-Mark Gurney <gurney_j@resnet.uoregon.edu>
           © 2008-2011 Navit Team
License: FreeBSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: navit/sunriset.c
Copyright: © 1989, 1992, Paul Schlyter <pausch@saaf.se>
           © 2009-2013 Navit Team
License: PD
 Released to the public domain by Paul Schlyter, December 1992.

Files: navit/browserplugin.c
Copyright: © 1998 Netscape Communications Corporation
License: MPL-1.1 or GPL-2+ or LGPL-2.1+

Files: debian/*
Copyright: © 2008 Carsten Wolff <carsten@wolffcarsten.de>
           © 2008-2009 Bernd Zeimetz <bzed@debian.org>
           © 2008-2015 Gilles Filippini <gilles.filippini@free.fr>
License: GPL-2+


License: GPL-2
 On Debian systems the full text of the GNU General Public License version 2
 can be found in the `/usr/share/common-licenses/GPL-2' file.

License: GPL-2+
 On Debian systems the full text of the GNU General Public License version 2
 can be found in the `/usr/share/common-licenses/GPL-2' file.

License: LGPL
 On Debian systems the full text of the GNU Lesser General Public License
 version 2 can be found in the `/usr/share/common-licenses/LGPL-2' file.

License: LGPL-2
 On Debian systems the full text of the GNU Lesser General Public License
 version 2 can be found in the `/usr/share/common-licenses/LGPL-2' file.

License: LGPL-2.1+
 On Debian systems the full text of the GNU Lesser General Public License
 version 2.1 can be found in the `/usr/share/common-licenses/LGPL-2.1' file.

License: MPL-1.1
 On Debian systems the full text of the MPL 1.1 can be found in
 the `/usr/share/common-licenses/MPL-1.1' file.

License: CC0-1.0
 On Debian systems the full text of the Creative Commons CC0 1.0 Universal
 license can be found in the `/usr/share/common-licenses/CC0-1.0` file.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

This work was packaged for Debian by:

    Eloy Paris <peloy@debian.org> on Wed, 16 Jun 2010 21:10:46 -0400

The current Debian maintainer is Eloy Paris <peloy@debian.org>

The software was downloaded from:

    http://www.netexpect.org

Upstream Author(s):

    Eloy Paris <peloy@netexpect.org>

Copyright:

    2007, 2008, 2009, 2010 Eloy Paris

License:

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.


This version of Network Expect relies heavily on libwireshark for
all packet dissection tasks. libwireshark is part of the Wireshark
(http://www.wireshark.org) package. Wireshark is licensed under the
GNU General Public License and is Copyright (C) 1998 Gerald Combs and
others.

Some of the IPv6 Neighbor Discovery code is Copyright (C) 2007 by Darrin
Miller <dmiller@cisco.com> and Eloy Paris <eloy@cisco.com>.

The following files contain code that comes from Don Libes' Expect, and
therefore is Copyright (C) 1987-2002 by Don Libes, National Institute of
Standards and Technology (http://www.nist.gov/):

    src/nexp_commands.c, src/expect_network.c, src/util.c,
    src/nexp_log.c,
    src/include/expect_network.h, src/include/netexpect_comm.h,
    src/include/nexp_log.h, src/include/nexp_log.c

    This code is in the public domain:

    Design and implementation of this program was paid for by U.S. tax
    dollars. Therefore it is public domain. However, the author and NIST
    would appreciate credit if this program or parts of it are used.

The following files are public-domain: src/md5c.c, src/include/md5.h.

The following files are copyright 2000 by Gary V. Vaughan, and released
under the GPL version 2: src/xmalloc.c and src/xstrdup.c.

The following files are copyright (c) 1998 Todd C. Miller
<Todd.Miller@courtesan.com>, and released under a BSD license:
missing/strlcat.c, and missing/strlcpy.c.

The functions copy_argv() and gmt2local() in src/util.c come from the
tcpdump program, which is released under a BSD license. These functions
are copyright (C) 1990, 1991, 1993, 1994, 1995, 1996, 1997 by The
Regents of the University of California.

The file src/include/byteorder.h is copyright (C) Andrew Tridgell
1992-1995 and is under GPL version 2.


The Debian packaging is:

    Copyright (C) 2010 Eloy Paris <peloy@debian.org>

you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation;
either version 2 of the License, or (at your option) any later version.

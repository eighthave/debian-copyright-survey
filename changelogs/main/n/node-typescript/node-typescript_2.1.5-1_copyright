Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: typescript
Upstream-Contact: https://github.com/Microsoft/TypeScript/issues
Source: http://typescriptlang.org/

Files: *
Copyright: 2016 Microsoft Corp.
License: Apache-2.0

Files: tests/baselines/reference/parserS12.11_A3_T4.errors.txt
       tests/baselines/reference/parserS12.11_A3_T4.js
       tests/baselines/reference/parserS7.2_A1.5_T2.errors.txt
       tests/baselines/reference/parserS7.2_A1.5_T2.js
       tests/baselines/reference/parserS7.3_A1.1_T2.errors.txt
       tests/baselines/reference/parserS7.3_A1.1_T2.js
       tests/baselines/reference/parserS7.6.1.1_A1.10.js
       tests/baselines/reference/parserS7.6.1.1_A1.10.symbols
       tests/baselines/reference/parserS7.6.1.1_A1.10.types
       tests/baselines/reference/parserS7.6_A4.2_T1.errors.txt
       tests/baselines/reference/parserS7.6_A4.2_T1.js
       tests/baselines/reference/parserS7.9_A5.7_T1.errors.txt
       tests/baselines/reference/parserS7.9_A5.7_T1.js
       tests/baselines/reference/parserSbp_7.9_A9_T3.js
       tests/baselines/reference/parserSbp_7.9_A9_T3.symbols
       tests/baselines/reference/parserSbp_7.9_A9_T3.types
       tests/baselines/reference/scannerS7.2_A1.5_T2.errors.txt
       tests/baselines/reference/scannerS7.2_A1.5_T2.js
       tests/baselines/reference/scannerS7.3_A1.1_T2.errors.txt
       tests/baselines/reference/scannerS7.3_A1.1_T2.js
       tests/baselines/reference/scannerS7.4_A2_T2.errors.txt
       tests/baselines/reference/scannerS7.4_A2_T2.js
       tests/baselines/reference/scannerS7.6_A4.2_T1.errors.txt
       tests/baselines/reference/scannerS7.6_A4.2_T1.js
       tests/baselines/reference/scannerS7.8.3_A6.1_T1.errors.txt
       tests/baselines/reference/scannerS7.8.3_A6.1_T1.js
       tests/baselines/reference/scannerS7.8.4_A7.1_T4.errors.txt
       tests/baselines/reference/scannerS7.8.4_A7.1_T4.js
       tests/cases/conformance/parser/ecmascript5/parserS7.6_A4.2_T1.ts
       tests/cases/conformance/parser/ecmascript5/parserS7.2_A1.5_T2.ts
       tests/cases/conformance/parser/ecmascript5/parserS7.3_A1.1_T2.ts
       tests/cases/conformance/parser/ecmascript5/parserS7.6.1.1_A1.10.ts
       tests/cases/conformance/parser/ecmascript5/parserSbp_7.9_A9_T3.ts
       tests/cases/conformance/parser/ecmascript5/parserS7.9_A5.7_T1.ts
       tests/cases/conformance/parser/ecmascript5/parserS12.11_A3_T4.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.8.4_A7.1_T4.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.2_A1.5_T2.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.4_A2_T2.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.3_A1.1_T2.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.8.3_A6.1_T1.ts
       tests/cases/conformance/scanner/ecmascript5/scannerS7.6_A4.2_T1.ts
Copyright: 2009 the Sputnik authors
License: BSD-3-clause

Files: tests/baselines/reference/parser10.1.1-8gs.errors.txt
       tests/baselines/reference/parser10.1.1-8gs.js
       tests/baselines/reference/parser15.4.4.14-9-2.errors.txt
       tests/baselines/reference/parser15.4.4.14-9-2.js
       tests/baselines/reference/scanner10.1.1-8gs.errors.txt
       tests/baselines/reference/scanner10.1.1-8gs.js
       tests/cases/conformance/parser/ecmascript5/parser10.1.1-8gs.ts
       tests/cases/conformance/parser/ecmascript5/parser15.4.4.14-9-2.ts
       tests/cases/conformance/scanner/ecmascript5/scanner10.1.1-8gs.ts
Copyright: 2012 Ecma International
License: BSD-3-clause

Files: debian/*
Copyright: 2016 Julien Puydt <julien.puydt@laposte.net>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 3. Neither the name of the authors nor the copyright holders may be used to
 endorse or promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
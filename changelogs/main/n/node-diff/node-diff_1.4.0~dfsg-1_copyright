Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jsdiff
Upstream-Contact: Tim Savery <tim.savery@gmail.com>
 https://github.com/kpdecker/jsdiff/issues
Source: https://github.com/kpdecker/jsdiff/downloads
 git://github.com/kpdecker/jsdiff
 Repackaged, excluding images with non-DFSG licensed color calibration.
Files-Excluded: images/

Files: *
Copyright:
  © 2009-2011, Kevin Decker kpdecker@gmail.com
License: BSD-3-clause

Files: debian/*
Copyright:
  © 2012-2013,2015, Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
License: GPL-3+

License: BSD-3-clause
 Redistribution and use of this software in source and binary forms,
 with or without modification, are permitted provided that the following
 conditions are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
  * Neither the name of Kevin Decker nor the names of its contributors
    may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
License-Reference: /usr/share/common-licenses/GPL-3

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Nagstamon
Upstream-Contact: Henri Wahl <h.wahl@ifw-dresden.de>
Source: https://github.com/HenriWahl/Nagstamon
License: GPL-2
Copyright: 2008-2016 Henry Wahl

Files: *
Copyright: 2008-2016 Henry Wahl <h.wahl@ifw-dresden.de>
License: GPL-2+

Files: Nagstamon/Servers/Multisite.py
Copyright: 2010 Mathias Kettner
License: GPL-2

Files: Nagstamon/thirdparty/Xlib/*
Copyright: 2000 Peter Liljenberg <petli@ctrl-c.liu.se>
           2002 Jeffrey Boser <verin@lvcm.com>
           2006 Mike Meyer <mwm@mired.org>
           2006 Alex Badea <vamposdecampos@gmail.com>
           2013 LiuLang <gsushzhsosgsu@gmail.com>
License: GPL-2

Files: Nagstamon/thirdparty/zabbix_api.py
Copyright: 2009 Brett Lentz brett.lentz(at)gmail(dot)com
           2009 Andrew Nelson nelsonab(at)red-tux(dot)net
License: LGPL-2.1+

Files: Nagstamon/thirdparty/zenoss_api.py Nagstamon/Servers/Zenoss.py
Copyright: 2016 Jake Murphy <jake.murphy@faredge.com.au> Far Edge Technology
License: GPL-2

Files: debian/*
Copyright: 2012 Carl Chenet <chaica@debian.org>
           2017 Moritz Schlarb <moschlar+deb@metalabs.de>
License: GPL-2

License: GPL-2
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2.
    .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    .
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    .
    On Debian systems, the complete text of the GNU General Public
    License, version 2, can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    .
    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    .
    On Debian systems, the complete text of the GNU General Public
    License, version 2, can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    .
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    .
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
    .
    On Debian systems, the complete text of the GNU Lesser General Public
    License, version 2.1, can be found in `/usr/share/common-licenses/LGPL-2.1'.

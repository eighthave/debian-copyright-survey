Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nwipe
Source: https://github.com/martijnvanbrummelen/nwipe

Files: *
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
License: GPL-2+

Files: src/context.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/device.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
              Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/device.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/gui.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/gui.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/logging.c 
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
License: GPL-2

Files: src/logging.h 
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
License: GPL-2

Files: src/method.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/method.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/mt19937ar-cok.c
Copyright: 1997 - 2002 Makoto Matsumoto <matumoto@math.keio.ac.jp> 
           1997 - 2002 Takuji Nishimura
License: BSD-3-clause
   All rights reserved.                          
   .
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   .
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
   .
     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
   .
     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.
   .
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/nwipe.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/nwipe.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>.
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/options.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/options.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/pass.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/pass.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
           Andy Beverley <andy@andybev.com>
License: GPL-2

Files: src/prng.c
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
License: GPL-2

Files: src/prng.h
Copyright: Darik Horn <dajhorn-dban@vanadac.com>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, version 2.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: debian/*
Copyright: 2012 Martijn van Brummelen <mvb@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Files: install-sh
Copyright: 1994 X Consortium
License: X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 FSF changes to this file are in the public domain.

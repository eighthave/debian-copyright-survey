Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: NVIDIA Texture Tools
Source: http://code.google.com/p/nvidia-texture-tools/

Files: *
Copyright: Copyright (c) 2007 NVIDIA Corporation
License: Expat

Files: debian/*
Copyright: Copyright 2012 Lennart Weller <lhw@ring0.de>
License: Expat

Files: src/nvtt/squish/* src/nvtt/SingleColorLookup.h
Copyright: Copyright (c) 2006 Simon Brown <si@sjbrown.co.uk>
License: Expat

Files: src/nvcore/poshlib/posh.*
Copyright: Copyright (c) 2004, Brian Hook
License: BSD-2-clause

Files:
 src/nvimage/Filter.h
 src/nvimage/ColorBlock.cpp
 src/nvimage/Image.h
 src/nvimage/NormalMipmap.cpp
 src/nvimage/Quantize.h
 src/nvimage/PsdFile.h
 src/nvimage/FloatImage.h
 src/nvimage/ImageIO.h
 src/nvimage/Image.cpp
 src/nvimage/ImageIO.cpp
 src/nvimage/Filter.cpp
 src/nvimage/nvimage.h
 src/nvimage/ColorBlock.h
 src/nvimage/TgaFile.h
 src/nvimage/NormalMipmap.h
 src/nvimage/HoleFilling.h
 src/nvimage/HoleFilling.cpp
 src/nvimage/FloatImage.cpp
 src/nvimage/Quantize.cpp
 src/nvmath/Triangle.cpp
 src/nvmath/Montecarlo.h
 src/nvmath/SphericalHarmonic.cpp
 src/nvmath/Plane.cpp
 src/nvmath/Plane.h
 src/nvmath/Random.cpp
 src/nvmath/Basis.h
 src/nvmath/Random.h
 src/nvmath/Box.h
 src/nvmath/Basis.cpp
 src/nvmath/Triangle.h
 src/nvmath/Color.h
 src/nvmath/Quaternion.h
 src/nvmath/nvmath.h
 src/nvmath/Vector.h
 src/nvmath/Montecarlo.cpp
 src/nvmath/SphericalHarmonic.h
 src/nvmath/Matrix.h
 src/nvcore/Debug.cpp
 src/nvcore/Memory.h
 src/nvcore/TextReader.h
 src/nvcore/Tokenizer.cpp
 src/nvcore/Stream.h
 src/nvcore/TextWriter.h
 src/nvcore/TextReader.cpp
 src/nvcore/Debug.h
 src/nvcore/StrLib.cpp
 src/nvcore/StrLib.h
 src/nvcore/TextWriter.cpp
 src/nvcore/Prefetch.h
 src/nvcore/Library.h
 src/nvcore/nvcore.h
 src/nvcore/Ptr.h
 src/nvcore/Tokenizer.h
 src/nvcore/Containers.h
 src/nvcore/BitArray.h
 src/nvcore/StdStream.h
 src/nvcore/Memory.cpp
 src/nvcore/Radix.h
 src/nvcore/Radix.cpp
 src/nvmath/TriBox.cpp
Copyright: Ignacio Castano <castanyo@yahoo.es>
License: public-domain
 The license is not specified beyond public-domain.
 The line from the source code is the following:
 "This code is in the public domain -- castanyo@yahoo.es"

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

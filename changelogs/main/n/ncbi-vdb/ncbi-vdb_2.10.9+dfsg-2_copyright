Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ngs
Source: https://github.com/ncbi/ncbi-vdb/releases
Files-Excluded: */ext
                */MSVC
                */Makefile.mac
                */Makefile.sun*
                */Makefile.vc++
                */Makefile.win
                */sun*.sh
                */win-cc.sh
                */mac/*
                */sun/*
                */win/*
                */jni.h
                */jni_md.h

Files: *
Copyright: © 2010-2016 National Center for Biotechnology Information
License: public_domain
                            PUBLIC DOMAIN NOTICE
               National Center for Biotechnology Information
 .
  This software/database is a "United States Government Work" under the
  terms of the United States Copyright Act.  It was written as part of
  the author's official duties as a United States Government employee and
  thus cannot be copyrighted.  This software/database is freely available
  to the public for use. The National Library of Medicine and the U.S.
  Government have not placed any restriction on its use or reproduction.
 .
  Although all reasonable efforts have been taken to ensure the accuracy
  and reliability of the software and data, the NLM and the U.S.
  Government do not and cannot warrant the performance or results that
  may be obtained by using this software or data. The NLM and the U.S.
  Government disclaim all warranties, express or implied, including
  warranties of performance, merchantability or fitness for any particular
  purpose.
 .
  Please cite the author in any work or product based on this material.

Files: build/gprof2dot.py
Copyright: 2008-2009 Jose Fonseca
License: LGPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems you can find a copy of the  GNU Lesser General Public
 License version 3 at /usr/share/common-licenses/LGPL-3.

Files: interfaces/klib/ksort-macro.h
       libs/klib/qsort.c
Copyright: 1991-2004 Douglas C. Schmidt <schmidt@ics.uci.edu>
License: LGPL-2.1+

Files: libs/klib/bsearch.c
Copyright: 1991-2002 Free Software Foundation, Inc.
           This file is part of the GNU C Library.
License: LGPL-2.1+

Files: libs/klib/md5.c
Copyright: 1999 Aladdin Enterprises
                 L. Peter Deutsch <ghost@aladdin.com>
License: Aladdin
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
 .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

Files: libs/kfg/config-tokens.h
       libs/kfg/config-grammar.c
Copyright: 1984-2011 Free Software Foundation, Inc.
License: GPL-3+
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
 .
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 .
 On Debian systems you can find a copy of the  GNU General Public
 License version 3 at /usr/share/common-licenses/GPL-3.

Files: debian/*
Copyright: 2016 Andreas Tille <tille@debian.org>
License: LGPL-2.1+

License: LGPL-2.1+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems you can find a copy of the  GNU Lesser General Public
 License version 2.1 at /usr/share/common-licenses/LGPL-2.1.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: newlisp
Source: http://www.newlisp.org

Files: *
Copyright: Lutz Mueller <lutz@nuevatec.com>
License: GPL-3+

Files: doc/*
Copyright: Lutz Mueller <lutz@nuevatec.com>
License: GFDL-NIV-1.2

Files: debian/*
Copyright: 2016 Sergio Durigan Junior <sergiodj@sergiodj.net>
License: GPL-3+

Files:
 guiserver/images/*.png
 guiserver/images/*.jpg
Copyright: 2007-2016 Michael Michaels
License: GPL-3+

Files: pcre*
Copyright: 1997-2016 University of Cambridge
License: BSD-3-clause

Files: win-path.c
Copyright: 2007,2008,2010 Michael Sabin
License: zlib

Files: nl-web.c
Copyright:
 2016 Lutz Mueller <lutz@nuevatec.com>
 1998 - 2004, Daniel Stenberg, <daniel@haxx.se>, et al.
License: GPL-3+ and curl

Files: nl-utf8.c
Copyright:
 2016 Lutz Mueller <lutz@nuevatec.com>
 1997-2003 University of Cambridge
License: GPL-3+ and BSD-3-clause

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GFDL-NIV-1.2
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
 copy of the license is included in the section entitled "GNU Free
 Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License version 1.2 can be found in
 "/usr/share/common-licenses/GFDL-1.2".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
 .
     * Neither the name of the University of Cambridge nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: curl
 This software is licensed as described in the file COPYING, which
 you should have received as part of this distribution. The terms
 are also available at http://curl.haxx.se/docs/copyright.html.
 .
 You may opt to use, copy, modify, merge, publish, distribute and/or sell
 copies of the Software, and permit persons to whom the Software is
 furnished to do so, under the terms of the COPYING file.
 .
 This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 KIND, either express or implied.

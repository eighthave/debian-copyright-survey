This package was created by Mike Gabriel <mike.gabriel@das-netzwerkteam.de> on Thu Oct  10 22:53:56 CEST 2011

This software (NX-redistributed) was downloaded from http://code.x2go.org/releases/source/nx-libs/

Copyright (c) 2012-2014 X2Go Project, http://wiki.x2go.org/.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 2 of the License, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program; if not, write to the Free Software Foundation, Inc., 51
   Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Patches contributed by the X2Go Project and the FreeNX Project have been applied
on top of the original NX upstream sources (see /doc/applied-patches):

  The Arctica Project:

    Author: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
        9900-dxpc-license-history.full+lite.patch

  X2Go Project:

    Author: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
        0000_add-NX-redistribution-README.full+lite.patch
        0009_nxproxy-add-man-page.patch
        0015_nxproxy_makefile-uninstall.patch
        0024_fix-make-clean.full+lite.patch
        0027_nxcomp_abstract-X11-socket.full+lite.patch
        0220_nxproxy_bind-loopback-only.full+lite.patch
        0607_nxcomp_macosx-X11-launcher-in-private-tmp.full+lite.patch
        0991_fix-hr-typos.full+lite.patch

    Author: Jan Engelhardt <jengelh@medozas.de>
        0012_nxcomp_makefile-uninstall.patch

    Author: Orion Poplawski <orion@cora.nwra.com>
        0026_nxcomp_honour-optflags.full+lite.patch

    Author: Mihai Moldovan <ionic@ionic.de>
        0029_nxcomp_ppc64.full+lite.patch
        0051_nxcomp_macos105-fdisset.full+lite.patch
        0052_nxcomp_macos10-nxauth-location.full+lite.patch

    Author: Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
        0210_nxcomp_save_session_state.full+lite.patch
        0211_nxcomp_set_default_options.full+lite.patch

    Author: Fedora NX package maintainers
        0006_nxcomp_libpng15.full+lite.patch

    Author: Clemens Lang <cal@macports.org>
        0410_nxcomp_use-MAKEDEPEND-in-path.full+lite.patch
        0420_nxcomp_use-correct-library-naming-scheme-on-OS-X.full+lite.patch
        0605_nxcomp_Types.h-dont-use-STL-internals-on-libc++.full+lite.patch 
        0612_nxproxy_use-MAKEDEPEND-in-path.full+lite.patch
        0613_nxcomp_use-dynamiclib-flag-on-OS-X.full+lite.patch

  The QVD Project:

    Author: Nito Martinez <Nito@Qindel.ES>
        0212_nxcomp_build-on-Android.full+lite.patch
        0400_nxcomp-version.full+lite.patch
        0992_fix-DEBUG-TEST-DUMP-FLUSH-TOKEN-PING-et-al-builds.full+lite.patch

    Author: Vadim Troshchinskiy <vtroshchinskiy@qindel.com>
        0660_nxcomp_fix-negotiation-in-stage-10-error.full+lite.patch

  FreeNX Project:

    Author: Marcelo Boveto Shima <marceloshima@gmail.com>
        0005_nxcomp_gcc43.full+lite.patch

    Author: Alexander Morozov
        0008_nxcomp_sa-restorer.full+lite.patch

  Others:

    Author: Gabriel Marcano <gabemarcano@yahoo.com>
        0025_nxcomp-fix-ftbfs-against-jpeg9a.full+lite.patch 

Copyright (c) 2001-2011 NoMachine, http://www.nomachine.com/.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 2 of the License, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License along with
   this program; if not, write to the Free Software Foundation, Inc., 51
   Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Parts of this software are derived from DXPC project. These copyright
notices apply to original DXPC code:

Copyright (c) 1995,1996 Brian Pane
Copyright (c) 1996,1997 Zachary Vonler and Brian Pane
Copyright (c) 1999-2002 Kevin Vigor and Brian Pane
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

The files MD5.[ch] are copyright (C) 1999, 2000, 2002 Aladdin Enterprises.

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


The packaging for Debian is copyright 2011 Mike Gabriel and is released
under the GPL version 2. On Debian systems, the complete text of the GNU
General Public License can be found in `/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: normaliz
Upstream-Contact: normaliz team <normaliz@uos.de>
Source: https://www.normaliz.uni-osnabrueck.de/
X-Upstream-Vcs: https://github.com/Normaliz/Normaliz.git
Files-Excluded:
 Dockerfile
 Docker
 install_scripts_opt
 install_normaliz.sh
 install_normaliz_*.sh
 install_pynormaliz.sh
 download.sh
 PyNormaliz/.mailmap
 PyNormaliz/.gitignore
 PyNormaliz/.clang-format
 PyNormaliz/.travis-install-normaliz.sh
 PyNormaliz/.travis.yml
 PyNormaliz/.coveragerc
 PyNormaliz/.codecov.yml
 PyNormaliz/.github
 .travis-build.sh
 .travis.yml
 source/libnormaliz/version.h
 test/sortdiff
 test/tools
 */Makefile.in
 PyNormaliz/doc/PyNormaliz_Tutorial.pdf
 doc/Normaliz.pdf
 m4
 cnf
 bootstrap.sh
 aclocal.m4
 configure


Files: *
Copyright:
 1997-2020 the normaliz team <normaliz@uos.de>
   Winfried Bruns
   Bogdan Ichim
   Tim Romer
   Christof Soeger
License: GPL-3+

Files: debian/*
Copyright:
 2015-2020 Jerome Benoit <calculus@rezozer.net>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

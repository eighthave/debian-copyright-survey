Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: babel-types
Upstream-Contact: Sebastian McKenzie <sebmck@gmail.com>
Source: https://babeljs.io/
Files-Excluded: lib/types.js

Files: *
Copyright: 2014-2017, Sebastian McKenzie <sebmck@gmail.com>
License: Expat

Files: core-js/*
Copyright: 2014-2019, Denis Pushkarev
License: Expat

Files: debian/*
Copyright: 2016, Pirate Praveen <praveen@debian.org>
 2019, Xavier Guimard <yadd@debian.org>
License: Expat

Files: debian/build_modules/gulp-babel/*
Copyright: Sindre Sorhus <sindresorhus@gmail.com> (sindresorhus.com)
License: Expat

Files: packages/babel-preset-es2015/test/fixtures/traceur/*
Copyright: 2012-2016, Google Inc
License: Apache-2.0
Comment: The upstream repository (that of traceur, not babel) contains a file package.json with a license-line (Apache-2) and author-line (The Traceur authors). The file AUTHORS contains all persons who contributed and such hold the copyright. Strictly speaking this list should be added to debian/copyright, but it is fine to just add the main contributor: Google (most contributors have a gmail.com address). The github repository started 5 years ago and ended a year ago, so years would be 2012-2016.

Files: packages/babel-plugin-transform-object-assign/*
Copyright: 2015, Jed Watson
License: Expat

Files: packages/babel-generator/test/fixtures/comments/simple-a-lot-of-line-comment/*
Copyright: 2012, Yusuke Suzuki <utatane.tea@gmail.com>
License: BSD-2-clause

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER “AS IS” AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

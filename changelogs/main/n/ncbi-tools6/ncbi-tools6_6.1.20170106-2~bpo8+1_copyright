Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: toolbox@ncbi.nlm.nih.gov
Upstream-Name: ncbi
Source: http://ftp.ncbi.nih.gov/toolbox/ncbi_tools/old/

Files: *
Copyright: 1996-2017 NCBI
License: public_domain

Files: debian/*
Copyright: 1998-1999 Stephane Bortzmeyer <bortzmeyer@pasteur.fr>
           2001      Dr. Guenter Bechly <gbechly@debian.org>
           2001-2017 Aaron M. Ucko <ucko@debian.org>
License: public_domain

License: public_domain
 The NCBI toolkit has been put into the public domain, completely unfettered:
 .
                           PUBLIC DOMAIN NOTICE
              National Center for Biotechnology Information
 .
 This software/database is a "United States Government Work" under the
 terms of the United States Copyright Act.  It was written as part of
 the author's official duties as a United States Government employee and
 thus cannot be copyrighted.  This software/database is freely available
 to the public for use. The National Library of Medicine and the U.S.
 Government have not placed any restriction on its use or reproduction.
 .
 Although all reasonable efforts have been taken to ensure the accuracy
 and reliability of the software and data, the NLM and the U.S.
 Government do not and cannot warrant the performance or results that
 may be obtained by using this software or data. The NLM and the U.S.
 Government disclaim all warranties, express or implied, including
 warranties of performance, merchantability or fitness for any particular
 purpose.
 .
 Please cite the author in any work or product based on this material.

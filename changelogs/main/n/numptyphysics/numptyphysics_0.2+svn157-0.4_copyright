Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: numptyphysics
Upstream-Contact: Tim Edmonds <numptyphysics@gmail.com>
Source: http://numptyphysics.garage.maemo.org/

Files: *
Copyright: 2008-2010, Tim Edmonds <numptyphysics@gmail.com>
License: GPL-3+

Files: happyhttp.cpp happyhttp.h
Copyright: 2006, Ben Campbell <ben@scumways.com>
License: ZLIB

Files: zoomer.cpp
Copyright: 2008, Andreas Schiffler <aschiffler@ferzkopp.net>
License: LGPL-2.1
 On Debian systems the full text of the GNU Lesser General Public
 License can be found in the `/usr/share/common-licenses/LGPL-2.1'
 file.

Files: Box2d/*
Copyright: 2006-2007 Erin Catto
License: ZLIB

Files: Box2D/Source/Common/jtypes.h
Copyright: 2005  Michael Noland (joat),
                 Jason Rogers (dovoto),
                 Dave Murphy (WinterMute),
                 Chris Double (doublec)
License: ZLIB

Files: Box2D/Source/Common/Fixed.h
Copyright: 2006  Henry Strickland & Ryan Seto,
           2007-2008  Tobias Weyand
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2009-2010, Leo 'costela' Antunes <costela@debian.org>
License: GPL-3+

License: GPL-3+
 On Debian systems the full text of the GNU General Public License
 can be found in the `/usr/share/common-licenses/GPL-3' file.

License: ZLIB
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

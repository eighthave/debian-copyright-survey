Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: VSEARCH
Upstream-Contact: Torbjørn Rognes <torognes@ifi.uio.no>
Source: https://github.com/torognes/vsearch/

Files: *
Copyright: © 2014-2015 Torbjorn Rognes <torognes@ifi.uio.no>,
             Tomas Flouri, Frederic Mahe
License: BSD-2-clause

Files: debian/*
Copyright: © 2014-2015 Tim Booth <tbooth@ceh.ac.uk>
           © 2015 Andreas Tille <tille@debian.org>
License: BSD-2-clause

License: BSD-2-clause
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
 .
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
 .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
Comment: The source code is dual licensed GPL-3+ or BSD-2-clause.
 For the packaging we pick BSD-2-clause since otherwise lintian throws
 error possible-gpl-code-linked-with-openssl.

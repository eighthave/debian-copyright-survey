This package was debianized by ViTE developpers <vite-developpeurs@lists.gforge.inria.fr> on
Thu, 16 Apr 2009 14:30:59 +0200 and maintained by Samuel Thibault <sthibault@debian.org>

It was downloaded from <http://vite.gforge.inria.fr>

externals/otf part:
===================

Upstream Authors:

    Andreas Knuepfer <andreas.knuepfer AT tu-dresden.de>
    Ronny Brendel <ronny.brendel AT tu-dresden.de>
    Matthias Jurenz <matthias.jurenz AT tu-dresden.de>
    Johannes Spazier <johannes.spazier AT tu-dresden.de>
    Michael Heyde <michael.heyde AT tu-dresden.de>
    Michael Kluge <michael.kluge AT tu-dresden.de>
    Holger Mickler <holger.mickler AT tu-dresden.de>
    Holger Brunst <holger.brunst AT tu-dresden.de>
    Hartmut Mix <hartmut.mix AT tu-dresden.de>
    
Copyright:

    Copyright by ZIH, TU Dresden 2005-2009.

License:

    Copyright (c) 2005-2010, ZIH, Technische Universitaet Dresden,
    Federal Republic of Germany
    
     All rights reserved.
    
    Redistribution and use in source and binary forms, with or without modification, 
    are permitted provided that the following conditions are met:
    
    - Redistributions of source code must retain the above copyright notice, 
    this list of conditions and the following disclaimer.
    
    - Redistributions in binary form must reproduce the above copyright notice, 
    this list of conditions and the following disclaimer in the documentation and/or 
    other materials provided with the distribution.
    
    - Neither the name of ZIH, TU Dresden nor the names of its contributors 
    may be used to endorse or promote products derived from this software without 
    specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR 
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

externals/qtcolorpicker part:
=============================

Copyright:

    Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).

License:

    Commercial Usage  
    Licensees holding valid Qt Commercial licenses may use this file in
    accordance with the Qt Solutions Commercial License Agreement provided
    with the Software or, alternatively, in accordance with the terms
    contained in a written agreement between you and Nokia.
    
    GNU Lesser General Public License Usage
    Alternatively, this file may be used under the terms of the GNU Lesser
    General Public License version 2.1 as published by the Free Software
    Foundation and appearing in the file LICENSE.LGPL included in the
    packaging of this file.  Please review the following information to
    ensure the GNU Lesser General Public License version 2.1 requirements
    will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
    
    In addition, as a special exception, Nokia gives you certain
    additional rights. These rights are described in the Nokia Qt LGPL
    Exception version 1.1, included in the file LGPL_EXCEPTION.txt in this
    package.
    
    GNU General Public License Usage 
    Alternatively, this file may be used under the terms of the GNU
    General Public License version 3.0 as published by the Free Software
    Foundation and appearing in the file LICENSE.GPL included in the
    packaging of this file.  Please review the following information to
    ensure the GNU General Public License version 3.0 requirements will be
    met: http://www.gnu.org/copyleft/gpl.html.
    
    Please note Third Party Software included with Qt Solutions may impose
    additional restrictions and it is the user's responsibility to ensure
    that they have met the licensing requirements of the GPL, LGPL, or Qt
    Solutions Commercial license and the relevant license of the Third
    Party Software they are using.
    
    If you are unsure which license is appropriate for your use, please
    contact Nokia at qt-info@nokia.com.

The complete text of the LGPL licence version 2.1 can be found in
/usr/share/common-licenses/LGPL-2.1

The complete text of the GPL licence version 3.0 can be found in
/usr/share/common-licenses/GPL-3

The aforementioned exception (LGPL_EXCEPTION.txt) is the following:

    Nokia Qt LGPL Exception version 1.1
    
    As an additional permission to the GNU Lesser General Public License
    version 2.1, the object code form of a "work that uses the Library"
    may incorporate material from a header file that is part of the
    Library.  You may distribute such object code under terms of your
    choice, provided that:
    (i) the header files of the Library have not been modified; and
    (ii) the incorporated material is limited to numerical parameters,
    data structure layouts, accessors, macros, inline functions and
    templates; and 
    (iii) you comply with the terms of Section 6 of the GNU
    Lesser General Public License version 2.1.
    
    Moreover, you may apply this exception to a modified version of the
    Library, provided that such modification does not involve copying
    material from the Library into the modified Library?s header files
    unless such material is limited to (i) numerical parameters; (ii) data
    structure layouts; (iii) accessors; and (iv) small macros, templates
    and inline functions of five lines or less in length.
    
    Furthermore, you are not required to apply this additional permission
    to a modified version of the Library.

externals/tau part:
===================

Copyright:

    Copyright 1997-2007
    Department of Computer and Information Science, University of Oregon
    Advanced Computing Laboratory, Los Alamos National Laboratory
    Research Center Juelich, ZAM Germany

License:

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted,
    provided that the above copyright notice appear in all copies and that
    both that copyright notice and this permission notice appear in
    supporting documentation, and that the name of University of Oregon (UO)
    Research Center Juelich, (ZAM) and Los Alamos National Laboratory (LANL) 
    not be used in advertising or publicity pertaining to distribution of
    the software without specific, written prior permission.  The
    University of Oregon, ZAM and LANL make no representations about the
    suitability of this software for any purpose.  It is provided "as is"
    without express or implied warranty.
    
    UO, ZAM AND LANL DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, 
    INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. 
    IN NO EVENT SHALL THE UNIVERSITY OF OREGON, ZAM OR LANL BE LIABLE FOR
    ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
    RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
    CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
    CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

src/core/getopt.h:
==================

Copyright:

    (C) 2005 by fizzgig

License:

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

The complete text of the GPL licence version 2 can be found in
/usr/share/common-licenses/GPL-2


src/interface/qxt*.*:
=====================

Copyright:

    (C) Adam Higerd <coda@bobandgeorge.com>

License:

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or any later version.
    
    This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
    WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
    
    There is aditional information in the LICENSE file of libqxt.
    If you did not receive a copy of the file try to download it or
    contact the libqxt Management

The complete text of the LGPL licence version 2.1 can be found in
/usr/share/common-licenses/LGPL-2.1

src/trace/portable_iarchive.*pp:
================================

Copyright:

    christian.pfligersdorffer@eos.info

License:

    The boost software license applies.

    
    	Boost Software License - Version 1.0
    	------------------------------------
    
    Boost Software License - Version 1.0 - August 17th, 2003
    
    Permission is hereby granted, free of charge, to any person or organization
    obtaining a copy of the software and accompanying documentation covered by
    this license (the "Software") to use, reproduce, display, distribute,
    execute, and transmit the Software, and to prepare derivative works of the
    Software, and to permit third-parties to whom the Software is furnished to
    do so, all subject to the following:
    
    The copyright notices in the Software and this entire statement, including
    the above license grant, this restriction and the following disclaimer,
    must be included in all copies of the Software, in whole or in part, and
    all derivative works of the Software, unless such copies or derivative
    works are solely in the form of machine-executable object code generated by
    a source language processor.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
    SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
    FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

The rest (ViTE)
===============

Upstream Authors:

    COULOMB Kevin
    FAVERGE Mathieu
    JAZEIX Johnny
    LAGRASSE Olivier
    MARCOUEILLE Jule
    NOISETTE Pascal
    REDONDY Arthur
    VUCHENER Clément 
    
Copyright:

    (c) 2009-2012 ViTE developers
    
License:

    This software is governed by the CeCILL-A license under French law
    and abiding by the rules of distribution of free software. You can
    use, modify and/or redistribute the software under the terms of the
    CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
    URL: "http://www.cecill.info".
    
    As a counterpart to the access to the source code and rights to copy,
    modify and redistribute granted by the license, users are provided
    only with a limited warranty and the software's author, the holder of
    the economic rights, and the successive licensors have only limited
    liability.
    
    In this respect, the user's attention is drawn to the risks associated
    with loading, using, modifying and/or developing or reproducing the
    software by the user in light of its specific status of free software,
    that may mean that it is complicated to manipulate, and that also
    therefore means that it is reserved for developers and experienced
    professionals having in-depth computer knowledge. Users are therefore
    encouraged to load and test the software's suitability as regards
    their requirements in conditions enabling the security of their
    systems and/or data to be ensured and, more generally, to use and
    operate it in the same conditions as regards security.
    
    The fact that you are presently reading this means that you have had
    knowledge of the CeCILL-A license and that you accept its terms.


The Debian packaging is © 2009-2012, ViTE developpers <vite-developpeurs@lists.gforge.inria.fr> and is licensed under the CeCILL-A

The complete text of the CeCILL-A licence version 2 can be found in
Licence-CeCILL_V2-en.txt (english version)
Licence-CeCILL_V2-fr.txt (french version)

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vinetto
Source: https://github.com/AtesComp/Vinetto

Files: *
Copyright: 2019 Keven L. Ates <atescomp@gmail.com>
License: GPL-3

Files: bin/vinetto
       vinetto/vinreport.py
       vinetto/vinutils.py
Copyright: 2005-2007 Michel Roukine <rukin@users.sf.net>
License: GPL-2+

Files: debian/*
Copyright: 2006      Danny van der Meeren <danny@illogic.nl>
           2014-2015 Joao Eriberto Mota Filho <eriberto@debian.org>
           2016-2021 Giovani Augusto Ferreira <giovani@debian.org>
           2019-2020 Samuel Henrique <giovani@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

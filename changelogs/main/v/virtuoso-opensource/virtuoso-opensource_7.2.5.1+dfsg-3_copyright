Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Virtuoso
Source: https://github.com/openlink/virtuoso-opensource/releases
Files-Excluded: binsrc/oat/toolkit/webclip.js
                *.jar
                *.dll
                *.chm
                */bootstrap.min.js
                */jquery.min.js
                */ckeditor
                libsrc/zlib
                libsrc/util/pcrelib
                docsrc/stylesheets/docbook
                debian
                autom4te.cache
                .gitignore
                .cvsrc
                */libtool.macosx
                windows

Files: *
Copyright: 2009-2017 OpenLink Software's Virtuoso Open-Source Edition project
                     OpenLink Virtuoso Maintainer <vos.admin@openlinksw.com>
                     OpenLink Software
License: GPL-2+openssl

Files: libsrc/Tidy/*
Copyright: 1998-2000 World Wide Web Consortium
License: Libpng
 *  This software and documentation is provided "as is," and
 *  the copyright holders and contributing author(s) make no
 *  representations or warranties, express or implied, including
 *  but not limited to, warranties of merchantability or fitness
 *  for any particular purpose or that the use of the software or
 *  documentation will not infringe any third party patents,
 *  copyrights, trademarks or other rights.
 *
 *  The copyright holders and contributing author(s) will not be
 *  liable for any direct, indirect, special or consequential damages
 *  arising out of any use of the software or documentation, even if
 *  advised of the possibility of such damage.
 *
 *  Permission is hereby granted to use, copy, modify, and distribute
 *  this source code, or portions hereof, documentation and executables,
 *  for any purpose, without fee, subject to the following restrictions:
 *
 *  1. The origin of this source code must not be misrepresented.
 *  2. Altered versions must be plainly marked as such and must
 *     not be misrepresented as being the original source.
 *  3. This Copyright notice may not be removed or altered from any
 *     source or altered source distribution.
 *
 *  The copyright holders and contributing author(s) specifically
 *  permit, without fee, and encourage the use of this source code
 *  as a component for supporting the Hypertext Markup Language in
 *  commercial products. If you use this source code in a product,
 *  acknowledgment is not required but would be appreciated.

Files: binsrc/redland/*
Copyright: 2000-2008 Openlink Software
License: GPL-2+
Comment: For simplicity reasons we just pick GPL-2+ which is specified below.
 * This package is Free Software and part of Redland http://librdf.org/
 *
 * It is licensed under the following three licenses as alternatives:
 *   1. GNU Lesser General Public License (LGPL) V2.1 or any newer version
 *   2. GNU General Public License (GPL) V2 or any newer version
 *   3. Apache License, V2.0 or any newer version
 *
 * You may not use this file except in compliance with at least one of
 * the above three licenses.
 *
 * See LICENSE.html or LICENSE.txt at the top of this package for the
 * complete terms and further detail along with the license texts for
 * the licenses in COPYING.LIB, COPYING and LICENSE-2.0.txt respectively.

Files: binsrc/oat/*
Copyright:  2005-2012 OpenLink Software
License: GPL-2+openssl
Comment:
        Virtuoso uses the OpenLink AJAX Toolkit (OAT) in the Data Space
        application suite, distributable under the terms of the GNU General
        Public License v2: <http://www.gnu.org/copyleft/gpl.html>

Files: binsrc/maildrop/sysexits.h
Copyright: 1987, 1993 The Regents of the University of California.
License: BSD-3-clause

Files: binsrc/tutorial/hosting/ho_s_17/*.jsp
Copyright: 1999, 2004 The Apache Software Foundation
License: Apache-2.0
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
 .
          http://www.apache.org/licenses/LICENSE-2.0
 .
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
 .
 On Debian systems you can find the full text of the Apache license version
 2.0 at /usr/share/common-licenses/Apache-2.0

Files: binsrc/yacutia/syntax/shCore.js
Copyright: 2004 Dream Projections Inc.
License: GPL-2+

Files: libsrc/odbcsdk/include/*
Copyright: 1999-2005 OpenLink Software <iodbc@openlinksw.com>
License: GPL-2+ or BSD-3-clause
Comment:
       While not mandated by the BSD license, any patches you make to the
       iODBC source code may be contributed back into the iODBC project
       at your discretion. Contributions will benefit the Open Source and
       Data Access community as a whole. Submissions may be made at:
           http://www.iodbc.org

Files: libsrc/util/uuid*
Copyright: 1996, 1997, 1998 Theodore Ts'o.
License: BSD-3-clause

Files: appsrc/ODS-Gallery/www-root/js/ajax.js
Copyright: 2005-2009 by Matthias Hertel, http://www.mathertel.de/
License: BSD-3-clause

Files: appsrc/ODS-Gallery/www-root/js/timer.js
       appsrc/ODS-Gallery/www-root/js/slider.js
       appsrc/ODS-Gallery/www-root/js/range.js
Copyright: 1999 - 2005 Erik Arvidsson
License: WebFX
       This software is provided "as is", without warranty of any kind, express or
       implied, including  but not limited  to the warranties of  merchantability,
       fitness for a particular purpose and noninfringement. In no event shall the
       authors or  copyright  holders be  liable for any claim,  damages or  other
       liability, whether  in an  action of  contract, tort  or otherwise, arising
       from,  out of  or in  connection with  the software or  the  use  or  other
       dealings in the software.
 .
       This  software is  available under the  three different licenses  mentioned
       below.  To use this software you must chose, and qualify, for one of those.
 .
       The WebFX Non-Commercial License          http://webfx.eae.net/license.html
       Permits  anyone the right to use the  software in a  non-commercial context
       free of charge.
 .
         The WebFX Non Commercial license only applies if all of the following
         statements are true and not broken.
 .
            1. The header in the P/C (1) may not be removed or modified.
            2. The P/C is used in a non-commercial or non-profit environment (2).
                  1. No money is put into the environment except the cost of the
                     web space and domain name.
                     You may of course also put money into hardware and software
                     that run the environment.
                     This means that no one can get paid to maintain the
                     environment.
                     No company, organization or institute can back up the
                     environment financially. Non-profit organizations (3) are
                     excluded.
                  2. The environment that the P/C is used in cannot make any
                     profit.
                     This means that you cannot sell anything, directly or
                     indirectly using the environment.
                     You cannot use the P/C in an environment that act as a
                     promotion/commercial for a commercial product.
                     Commercial banners can be used as long as the income from
                     these does not exceed the cost to maintain the environment.
                     Donations are allowed.
            3. The P/C may be freely modified as long as 1 - 2 are not broken.
               Any work based on the P/C also fall under this license.
               This means that if you base any work on the P/C then the new
               product cannot be used commercially.
            4. The P/C may be freely distributed as long as 1 - 3 are not
               broken.
               This means that the P/C cannot be redistributed to any third
               commercial part because this would break 2.
               This also means that if the P/C is included in a widget pack
               (component library) then the widget pack (or parts of it that
               uses the P/C) may not be used in a commercial environment.
 .
         (1) P/C - product/component provided by WebFX.
         (2) Environment - Program that uses the product/component. This
             includes web sites, intranets and offline programs.
         (3) Non-profit organization - a non-profit organization is a
             organization that has no intention of making money in any way and
             all work is done for charity.
 .
       The WebFX Commercial license           http://webfx.eae.net/commercial.html
       Permits the  license holder the right to use  the software in a  commercial
       context. Such license must be specifically obtained, however it's valid for
       any number of  implementations of the licensed software.
 .
         The WebFX Commercial License permits the inclusion of the products for
         which a license has been obtained (hereby referred to as the products)
         in any current or future product produced by the license holding
         corporation (hereby referred to as the corporation) with the exception
         of the following product-types:
 .
             * Individual or combined sale of the products as a stand-alone
               offer.
             * Individual or combined sale of the products as part of a
               components package or web development aid.
 .
         The products may be included simply as enhancements to another product.
         The products may be included simply as a part of another product. The
         corporation may never profit directly from sale of the products, only
         from the application they are included in. All intellectual property
         rights and source code right will remain in the possession of WebFX and
         the affected author.
 .
         The products may not be resold. Neither may products deviated from the
         original code, or products produced by reverse engineering the original
         code, be sold or in any other way be profitable for the corporation.
         If The products are included in a service offered by the corporation a
         license must be obtained for each company the service is sold to.
 .
         The license is valid only for the products and only for the purchased
         version of those, with the following exceptions.
 .
             * Minor updates.
             * Major updates released within one (1) month of the purchase.
             * The product has been discontinued and replaced by another product
               within one (1) month of the purchase.
 .
         A minor update consists of bug fixes and patches that may include some
         enhancements and small, added features to the product. Minor updates
         are signified by minor version number changes. For example, a minor
         update from version 3.0 would be 3.1 or 3.01).
 .
         A major upgrade consists of major enhancements and new features added
         to the product. Major upgrades are signified by a major version number
         change. For example, a major upgrade from version 3.0 would be 4.0.
 .
         The corporation is entitled to a free update, if one or more of the
         above criteria are met. Updates not addressed above requires a new
         license, which can be purchased at rebated price if the old one is
         revoked.
 .
       GPL - The GNU General Public License    http://www.gnu.org/licenses/gpl.txt
       Permits anyone the right to use and modify the software without limitations
       as long as proper  credits are given  and the original  and modified source
       code are included. Requires  that the final product, software derivate from
       the original  source or any  software  utilizing a GPL  component, such  as
       this, is also licensed under the GPL license.

Files: binsrc/tests/bsbm/src/*
Copyright: 2000-2008 Hewlett-Packard Development Company, LP
           2008 Andreas Schultz
License: GPL-3+

Files: binsrc/oat/toolkit/crypto.js
       binsrc/yacutia/md5.js
Copyright: 1999-2002 Paul Johnston
License: BSD-3-clause
Comment:
    JavaScript RSA MD5 and SHA routines
 .
       A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
       Digest Algorithm, as defined in RFC 1321.
       Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
       Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
       Distributed under the BSD License
 .
       License to copy and use this software is granted provided that it is
       identified as the "RSA Data Security, Inc. MD5 Message-Digest Algorithm"
       in all material mentioning or referencing this software or this function.
 .
       License is also granted to make and use derivative works provided that
       such works are identified as "derived from the RSA Data Security, Inc.
       MD5 Message-Digest Algorithm" in all material mentioning or referencing
       the derived work.
 .
       RSA Data Security, Inc. makes no representations concerning either the
       merchantability of this software or the suitability of this software for
       any particular purpose. It is provided "as is" without express or
       implied warranty of any kind.
 .
       These notices must be retained in any copies of any part of this
       documentation and/or software.

Files: appsrc/ODS-Framework/mm_menu.js
       binsrc/yacutia/mm_menu.js
Copyright: 2000-2002 Andy Finnell, Macromedia, Inc.
           1997-1999 Gary Smith, Netscape Communications Corp.
License: Netscape
       Netscape grants you a royalty free license to use or modify this
       software provided that this copyright notice appears on all copies.
       This software is provided "AS IS," without a warranty of any kind.

Files: libsrc/JDBCDriverType4/openlink/util/MD5.java
Copyright: 1996 Santeri Paavolainen, Helsinki Finland
License: GPL-2+
Comment:
       See http://www.cs.hut.fi/~santtu/java/ for more information on this
       class.
 .
       This is rather straight re-implementation of the reference implementation
       given in RFC1321 by RSA.
 .
       Passes MD5 test suite as defined in RFC1321.
 .
       This Java class has been derived from the RSA Data Security, Inc. MD5
       Message-Digest Algorithm and its reference implementation.

Files: libsrc/util/strtok_r.c
       libsrc/langfunc/latin1ctype.h
       libsrc/langfunc/latin1ctype.c
       libsrc/util/getopt1.c
       libsrc/util/getopt.c
       libsrc/util/fnmatch.c
Copyright: 1987-1999 Free Software Foundation, Inc.
License: GPL-2+
Comment: This file is part of the GNU C Library.

Files: binsrc/oat/toolkit/OpenLayers.js
Copyright: 2005-2008 MetaCarta, Inc.
License: OpenLayers
 released under the Clear BSD license.
       Please see http://svn.openlayers.org/trunk/openlayers/license.txt
       for the full text of the license.
 .
       Includes compressed code under the following licenses:
 .
       (For uncompressed versions of the code used please see the
       OpenLayers SVN repository: <http://openlayers.org/>)
 .
       Contains portions of Prototype.js:
 .
       Prototype JavaScript framework, version 1.4.0
        (c) 2005 Sam Stephenson <sam@conio.net>
 .
        Prototype is freely distributable under the terms of an MIT-style license.
        For details, see the Prototype web site: http://prototype.conio.net/
 .
 .
       Contains portions of Rico <http://openrico.org/>
 .
       Copyright 2005 Sabre Airline Solutions
 .
       Licensed under the Apache License, Version 2.0 (the "License"); you
       may not use this file except in compliance with the License. You
       may obtain a copy of the License at
 .
              http://www.apache.org/licenses/LICENSE-2.0
 .
       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
       implied. See the License for the specific language governing
       permissions and limitations under the License.

Files: docsrc/xmlsource/DocBook/docbookx.dtd
       docsrc/xmlsource/DocBook/calstblx.dtd
Copyright: 1992-2000 HaL Computer Systems, Inc.,
           O'Reilly & Associates, Inc., ArborText, Inc., Fujitsu Software
           Corporation, Norman Walsh and the Organization for the Advancement
           of Structured Information Standards (OASIS).
License: DocBook
       Permission to use, copy, modify and distribute the DocBook XML DTD
       and its accompanying documentation for any purpose and without fee
       is hereby granted in perpetuity, provided that the above copyright
       notice and this paragraph appear in all copies.  The copyright
       holders make no representation about the suitability of the DTD for
       any purpose.  It is provided "as is" without expressed or implied
       warranty.
 .
       If you modify the DocBook XML DTD in any way, except for declaring and
       referencing additional sets of general entities and declaring
       additional notations, label your DTD as a variant of DocBook.  See
       the maintenance documentation for more information.
 .
       Please direct all questions, bug reports, or suggestions for
       changes to the docbook@lists.oasis-open.org mailing list. For more
       information, see http://www.oasis-open.org/docbook/.

Files: debian/*
Copyright: 2012 José Manuel Santamaría Lema <panfaust@gmail.com>
           2009-2011 Obey Arthur Liu <arthur@milliways.fr>
           2009-2010 Will Daniels <mail@willdaniels.co.uk>
           2009 Olivier Berger <olivier.berger@it-sudparis.eu>
           2008 Miriam Ruiz <little_miry@yahoo.es>
           2018 Andreas Tille <tille@debian.org>
License: GPL-3+

License: GPL-2+
       This program is free software; you can redistribute it and/or
       modify it under the terms of the GNU General Public License
       as published by the Free Software Foundation; either version 2
       of the License, or (at your option) any later version.
 .
       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License:GPL-2+openssl
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 2 dated June 1991.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    OpenSSL exemption
    -----------------
 .
    This project may be compiled/linked with the OpenSSL library. If so, the
    following exemption is added to the above license:
 .
        In addition, as a special exemption, OpenLink Software gives
        permission to link the code of its release of Virtuoso with the
        OpenSSL project's "OpenSSL" library (or with modified versions
        of it that use the same license as the "OpenSSL" library), and
        distribute the linked executables. You must obey the GNU General
        Public License in all respects for all of the code used other
        than "OpenSSL".
 .
    Client Protocol Driver exemptions
    ---------------------------------
 .
        In addition, as a special exemption, OpenLink Software gives
        permission to use the unmodified client libraries (ODBC, JDBC,
        ADO.NET, OleDB, Jena, Sesame and Redland providers) in your own
        application whether open-source or not, with no obligation to use
        the GPL on the resulting application. In all other respects you
        must abide by the terms of the GPL.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
       Redistribution and use in source and binary forms, with or without
       modification, are permitted provided that the following conditions
       are met:
       1. Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
       2. Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.
       3. Neither the name of the University nor the names of its contributors
          may be used to endorse or promote products derived from this software
          without specific prior written permission.
 .
       THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
       ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
       IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
       ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
       FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
       DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
       OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
       HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
       LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
       OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
       SUCH DAMAGE.

License: GPL-3+
       This program is free software: you can redistribute it and/or modify
       it under the terms of the GNU General Public License as published by
       the Free Software Foundation, either version 3 of the License, or
       any later version.
 .
       This program is distributed in the hope that it will be useful,
       but WITHOUT ANY WARRANTY; without even the implied warranty of
       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
       GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

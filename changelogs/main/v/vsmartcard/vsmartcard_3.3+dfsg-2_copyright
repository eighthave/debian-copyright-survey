Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vsmartcard
Source: <https://github.com/frankmorgner/vsmartcard.git>
Files-Excluded: ACardEmulator/app/libs/sdk-v1.0.0.jar doc/bilder/Firewall.pdf doc/bilder/computer-tango.pdf doc/bilder/network-wireless.pdf doc/bilder/smartphone.pdf

Files: *
Copyright: Copyright: 2009-2020 Dominik Oepen and Frank Morgner.
License: GPL-2+

Files: debian/*
Copyright:  2020, Philippe Thierry <philou@debian.org>
License: GPL-2+

Files: ACardEmulator/app/src/fdroid/java/com/vsmartcard/acardemulator/*.java
Copyright: Copyright: 2019 Frank Morgner
License: GPL-3+

Files: virtualsmartcard/doc/generate_modules.py
Copyright: 2010 Thomas Waldmann <tw AT waldmann-edv DOT de>
           2008 SociÃ©tÃ© des arts technologiques (SAT), http:www.sat.qc.ca/
License: GPL-2+

Files: ccid/src/*
Copyright: Copyright: 2009 Frank Morgner
           Copyright: 2009-2012 Frank Morgner <frankmorgner@gmail.com>
License: GPL-3+

Files: pcsc-relay/src/*
Copyright: 2010-2012 Frank Morgner <frankmorgner@gmail.com>.
           2009 Dominik Oepen <oepen@informatik.hu-berlin.de>.
License: GPL-3+

Files: remote-reader/app/src/main/java/com/vsmartcard/remotesmartcardreader/app/*.java
Copyright: Copyright: 2014 Frank Morgner
License: GPL-3+

Files: virtualsmartcard/src/ifd-vpcd/ifd-vpcd.h
Copyright: Copyright: 2013 Frank Morgner
License: GPL-3+

Files: virtualsmartcard/src/pcsclite-vpcd/winscard.c
       virtualsmartcard/src/vpcd-config/vpcd-config.c
       virtualsmartcard/src/vpcd/lock.h
       virtualsmartcard/src/vpcd/vpcd.c
       virtualsmartcard/src/vpcd/vpcd.h
Copyright: Copyright: 2009-2014 Frank Morgner
License: GPL-3+

Files: virtualsmartcard/src/vpicc/virtualsmartcard/*.py
Copyright: Copyright: 2011-2015 Dominik Oepen, Frank Morgner, Henryk Ploetz
License: GPL-3+

Files: ACardEmulator/app/src/full/java/com/vsmartcard/acardemulator/SmartcardProviderService.java
Copyright: Copyright: 2015 Samsung Electronics Co., Ltd.
License: BSD-3-clause

Files: docs/_static/basic.css
       docs/_static/doctools.js
       docs/_static/websupport.js
Copyright: Copyright: Copyright 2007-2014 by the Sphinx team, see AUTHORS.
License: BSD-3-Clause

Files: virtualsmartcard/src/pcsclite-vpcd/error.c
Copyright: 2006-2009 Ludovic Rousseau <ludovic.rousseau@free.fr>
           1999-2002 David Corcoran <corcoran@musclecard.com>
License: BSD-3-clause

Files: docs/_static/bootstrap-*/css/bootstrap*.css
       docs/_static/bootstrap-*/js/bootstrap*.js
       docs/_static/bootswatch-*/*/bootstrap*.css
Copyright: Copyright: 2011-2015 Twitter, Inc
License: Apache-2.0

Files: remote-reader/app/src/main/java/com/example/android/common/logger/*.java
Copyright: Copyright: 2012-2013 The Android Open Source Project
License: Apache-2.0


License: Apache-2.0
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache Software License version 2 can
 be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder
 nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 THE COPYRIGHT HOLDER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

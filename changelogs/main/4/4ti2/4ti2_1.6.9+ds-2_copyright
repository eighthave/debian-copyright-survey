Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: 4ti2
Upstream-Contact: 4ti2 team
Source: https://github.com/4ti2/4ti2
Files-Excluded:
 snippet
 lib
 m4/00gnulib.m4
 m4/gnulib-common.m4
 m4/gnulib-comp.m4
 m4/gnulib-tool.m4
 m4/extensions.m4
 m4/extern-inline.m4
 m4/getopt.m4
 m4/include_next.m4
 m4/nocrash.m4
 m4/off_t.m4
 m4/onceonly.m4
 m4/ssize_t.m4
 m4/stddef_h.m4
 m4/sys_types_h.m4
 m4/trapv-check.m4
 m4/unistd_h.m4
 m4/warn-on-use.m4
 m4/wchar_t.m4
 m4/lt~obsolete.m4
 m4/ltsugar.m4
 m4/ltoptions.m4
 m4/ltversion.m4
 m4/libtool.m4
 test-driver
 depcomp
 compile
 install-sh
 ltmain.sh
 missing
 config.sub
 config.guess
 swig/allegrocommonlisp/4ti2.cl
 swig/allegrocommonlisp/allegrocl-4ti2.c
 src/groebner/script.template
 src/4ti2/4ti2_config.h
 src/config.h.in
 doc/3by3magic.eps
 doc/cubepicture.eps
 doc/magicsqs.eps
 doc/4ti2_manual.pdf
 */Makefile.in
 swig/aclocal.m4
 swig/configure
 aclocal.m4
 configure
 m4/ax_cc_maxopt.m4
 m4/ax_cflags_warn_all.m4
 m4/ax_check_compile_flag.m4
 m4/ax_check_compiler_flags.m4
 m4/ax_compiler_vendor.m4
 m4/ax_cxx_maxopt.m4
 m4/ax_gcc_archflag.m4
 m4/ax_gcc_x86_cpuid.m4

Files: *
Copyright:
 2006-2020 the 4ti2 team
   Ralf Hemmecke
   Raymond Hemmecke
   Matthias Koeppe
   Peter Malkin
   Matthias Walter
License: GPL-2+

Files: debian/*
Copyright:
 2014-2020 Jerome Benoit <calculus@rezozer.net>
License: GPL-2+

Files: debian/adhoc/examples/tutorial/*
Copyright:
 2014 Jerome Benoit <calculus@rezozer.net>
License: public-domain
 These files are public domain and are not licensed under any copyright.

Files: debian/adhoc/examples/www/*
Copyright:
 2006-2013 the 4ti2 team
License: public-domain
 These files are public domain and are not licensed under any copyright.
Comment:
 These data files belong to the `example files' collection available at
 http://www.4ti2.de/; they were grabbed as-is by hand and eventually renamed
 to reflect the naming scheme used in ``User's Guide for 4ti2''.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: 4ti2
Upstream-Contact: 4ti2 team
Source: http://www.4ti2.de/
Comment:
 The upstream source tarball is repacked to drop off the regenerated material
 (mainly gnulib, autotools and documentation related) to gain substantial weight.

Files: *
Copyright:
 2006-2015 the 4ti2 team
   Ralf Hemmecke
   Raymond Hemmecke
   Matthias Koeppe
   Peter Malkin
   Matthias Walter
License: GPL-2+

Files: debian/*
Copyright:
 2014-2015 Jerome Benoit <calculus@rezozer.net>
License: GPL-2+

Files: debian/adhoc/examples/tutorial/*
Copyright:
 2014 Jerome Benoit <calculus@rezozer.net>
License: public-domain
 These files are public domain and are not licensed under any copyright.

Files: debian/adhoc/examples/www/*
Copyright:
 2006-2013 the 4ti2 team
License: public-domain
 These files are public domain and are not licensed under any copyright.
Comment:
 These data files belong to the `example files' collection available at
 http://www.4ti2.de/; they were grabbed as-is by hand and eventually renamed
 to reflect the naming scheme used in ``User's Guide for 4ti2''.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

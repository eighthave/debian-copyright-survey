Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libucimf
Upstream-Contact: Chun-Yu Lee (Mat) <MatLinuxer2@gmail.com>
Source: https://github.com/matlinuxer2/ucimf/tree/master/libucimf

Files: *
Copyright: 2006-2010, Chun-Yu Lee (Mat) <MatLinuxer2@gmail.com> and
                      Open RazzmatazZ Laboratory (OrzLab)
License: GPL-2

Files: debian/*
Copyright: 2010, Aron Xu <happyaron.xu@gmail.com>
           2016, ChangZhuo Chen (陳昌倬) <czchen@debian.org>
License: GPL-2+

Files: font/font.cpp
       font/font.h
Copyright: 2008-2009 dragchan <zgchan317@gmail.com>
           2010 Chun-Yu Lee (Mat) <matlinuxer2@gmail.com>
License: GPL-2+

Files: display/*
Copyright: 2007 Chun-Yu Lee (Mat) and Open RazzmatazZ Laboratory (OrzLab)
          Parts taken from zhcon project:
           2001 ejoy <ejoy@users.sourceforge.net>, huyong <ccpaging@online.sh.cn>
License: GPL-2+

Files: display/fblinear8.cpp
       display/fblinear8.h
Copyright: 2001 ejoy <ejoy@users.sourceforge.net>, huyong <ccpaging@online.sh.cn>
License: GPL-2+

Files: include/imf/imf.h
       sample/imf/*
Copyright: 2006-2007 Chun-Yu Lee (Mat) and Open RazzmatazZ Laboratory (OrzLab)
License: BSD-3-clause

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as published by
 the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
 .
     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
 .
     3. Neither the name of the University nor the names of its contributors
        may be used to endorse or promote products derived from this software
        without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libgig
Upstream-Contact: Christian Schoenebeck <cuse@users.sourceforge.net>
Source: http://download.linuxsampler.org/packages/
Copyright: 2003-2009, Christian Schoenebeck <cuse@users.sourceforge.net>
Files-Excluded:
 */.git*
 */autom4te.cache/*
 */debian/*

Files: *
Copyright:
 2003-2015, Christian Schoenebeck <cuse@users.sourceforge.net>
License: GPL-2+

Files: src/tools/sf2extract.cpp
Copyright: 2009-2010, Grigor Iliev
  2015, Christian Schoenebeck
License: GPL-2+

Files: src/tools/sf2dump.cpp
Copyright: 2003-2014, Christian Schoenebeck
  2009, Grigor Iliev <grigor@grigoriliev.com>
License: GPL-2+

Files: src/SF.h
 src/SF.cpp
Copyright: 2009-2010, Grigor Iliev <grigor@grigoriliev.com>
License: GPL-2+

Files: src/Akai.cpp
 src/Akai.h
 src/tools/akaidump.cpp
 src/tools/akaiextract.cpp
Copyright: 2002-2003, Sébastien Métrot
License: LGPL-2.1+

Files: osx/autoconf_builder.sh
Copyright: 2007, Toshi Nagata.
License: BSD-2-clause

Files:
 debian/*
Copyright:
 2009, Alessio Treglia <quadrispro@ubuntu.com>
 2007-2008, Free Ekanayaka <freee@debian.org>
 2005, Paul Brossier <piem@debian.org>
 2005, Matt Flax <flatmax@pgb.unsw.edu.au>
 2003-2007, Christian Schoenebeck <cuse@users.sourceforge.net>
 2016-2017, Jaromír Mikeš <mira.mikes@seznam.cz>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
Comment:
 On Debian GNU systems the GNU General Public License (GPL) version 2 is
 located in '/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
Comment:
 On Debian GNU systems the GNU Lesser General Public License (LGPL)
 version 2.1 is located in '/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/>.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification, are permitted 
 provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this list of 
 conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Genome
Upstream-Contact: genome-dev@genome.wustl.edu
Source: https://metacpan.org/release/Genome-Model-Tools-Music

Files: *
Copyright: 2007-2011 Washington University in St. Louis
License: LGPL-3
Comment: README says "same terms as Perl itself"; we are assuming
 that this is boilerplate text from the build tools, since the code
 has explicit statements about the LGPL-3. A bug has been created
 upstream to clarify the situation but upstream bug system does not
 seem to be followed anymore since 2012.
 Bug ref: https://rt.cpan.org/Public/Bug/Display.html?id=85079

Files: lib/Genome/Model/Tools/Music/PathScan/*
Copyright: 2009 Washington University in St. Louis
License: GPL-2+

Files: debian/*
Copyright: 2013, Olivier Sallou <osallou@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.


License: LGPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.

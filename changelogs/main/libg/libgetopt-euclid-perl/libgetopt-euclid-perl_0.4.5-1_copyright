Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Getopt-Euclid
Upstream-Contact: Damian Conway <DCONWAY@cpan.org>, Kevin Galinksy <kgalinksy@gmail.com>
Source: https://metacpan.org/release/Getopt-Euclid

Files: *
Copyright: 2002-2005, Damian Conway <DCONWAY@cpan.org>
License: Artistic or GPL-1+

Files: lib/Getopt/Euclid/PodExtract.pm
Copyright: 2011, Florent Angly <florent.angly@gmail.com>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2006-2013, gregor herrmann <gregoa@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2009, Nathan Handler <nhandler@debian.org>
 2010-2011, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Geo-Coordinates-OSGB
Source: https://metacpan.org/release/Geo-Coordinates-OSGB
Upstream-Contact: Toby Thurston <toby@cpan.org>

Files: *
Copyright: 2002-2017, Toby Thurston <toby@cpan.org>
License: GPL-2+
Comment: various files in the source tarball seem to include OSTN02/OSGM02 data
  Copyright: 2002, Crown Copyright
  License: other
   Copyright and any database right or other intellectual property right in the
   OSTN02 data available from this site, however it is made available, belongs to
   Ordnance Survey (and is therefore Crown copyright). Copyright and any database
   right or other intellectual property right in OSGM02 data available from this
   site, however it is made available, belongs jointly to Ordnance Survey,
   Ordnance Survey Northern Ireland (and is protected by Crown copyright) and
   Ordnance Survey Ireland.
   .
   Ordnance Survey and (where relevant) Ordnance Survey Northern Ireland and
   Ordnance Survey Ireland permit users to copy or incorporate copyrighted
   material from both OSTN02, the OSi/OSNI polynomial transformation, or OSGM02
   either onto their own PCs or into their software. However, this is dependant
   on the appropriate copyright notice being displayed in the software. The
   appropriate notices are '© Ordnance Survey Ireland, 2002' or '© Crown
   copyright 2002. All rights reserved.' as applicable. The appropriate mapping
   agency logo must also appear next to the copyright notice. Details of mapping
   agency logos are found in the style guides available on the web site.
   .
   The following amendments to the licence have been provided by the
   Ordnance Survey, as recorded in the email exchange at
   <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=694621>:
   .
   i. The requirement for the logo to be displayed is relaxed for this
   library package.
   .
   ii. The OSTN02 data may be modified, as long as it is no longer referred
   to as OSTN02.
   .
   The full email correspondence is also included in the Debian source
   package at debian/os-licence-email.txt.

Files: debian/*
Copyright: 2012-2016, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: units
Source: https://www.gnu.org/software/units/

Files: *
Copyright: 1984-2017 Free Software Foundation, Inc.
License: GPL-3+

Files: getopt.h
Copyright: 1989-1994 Free Software Foundation, Inc.
License: GPL-2+

Files: units.info units.pdf units.texinfo units.txt
Copyright: 1996-2017 Free Software Foundation, Inc.
License: GFDL-1.3+

Files: debian/*
Copyright: 1997-1998 James Troup <james@nocrew.org>
           1999-2012 John G. Hasler <jhasler@debian.org>
           2013-2017 Stephen Kitt <skitt@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02111 USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License, version 2, can be found in
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301 USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License, version 3, can be found in
 `/usr/share/common-licenses/GPL-3'.

License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
 Texts.  A copy of the license is included in the section entitled
 "GNU Free Documentation License".
 .
 On Debian GNU/Linux systems, the complete text of the GNU Free
 Documentation License, version 1.3, can be found in
 `/usr/share/common-licenses/GFDL-1.3'.

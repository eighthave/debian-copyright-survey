This package was debianized by:

    Uwe Hermann <uwe@debian.org> on Wed, 19 Nov 2008 13:18:38 +0100

It was downloaded from:

    http://www.urjtag.org

Upstream Authors:

    Marcel Telka <marcel@telka.sk>
    Arnim Läuger <arniml@users.sourceforge.net>
    Kolja Waschk <urjtag@ixo.de>
    and many, many others (see AUTHORS)

-------------------------------------------------------------------------------

Files: *
Copyright: © 2002-2003 ETC s.r.o.
License: GPL-2+

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

-------------------------------------------------------------------------------

Files: Makefile.rules
Copyright: © 2003 ETC s.r.o.
           © 2004 Marcel Telka
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bsdl/bsdl.c,
       src/bsdl/bsdl_msg.h,
       src/bsdl/bsdl_parser.h,
       src/bsdl/bsdl_sem.c,
       src/bsdl/bsdl_sysdep.h,
       src/bsdl/bsdl_types.h,
       src/bsdl/vhdl_parser.h,
       src/bus/fjmem.c,
       src/bus/jopcyc.c,
       src/bus/zefant-xs3.c,
       src/cmd/bsdl.c,
       src/cmd/svf.c,
       src/svf/svf.[ch],
       src/tap/cable/ft2232.c,
Copyright: © 2004-2008 Arnim Laeuger
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/au1500.c
Copyright: © 2003 BLXCPU co. Ltd.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/avr32.c
Copyright: © 2008 Gabor Juhos <juhosg@openwrt.org>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/bf537_stamp.c,
       src/bus/bf548_ezkit.c,
       src/bus/bf561_ezkit.c,
Copyright: © 2008 Analog Devices, Inc.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/ejtag.c
Copyright: © 2005 Marek Michalkiewicz
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/generic_bus.[ch],
       src/tap/cable/vision_ep9307.c,
Copyright: © 2008 H. Hartley Sweeten <hsweeten@visionengravers.com>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/h7202.c
Copyright: © 2005 Raphael Mack <mail@raphael-mack.de>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/lh7a400.c
Copyright: © 2002-2003 Marcel Telka <marcel@telka.sk>
           © 2004 Marko Roessler <marko.roessler@indakom.de>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/mpc5200.c,
       src/bus/mpc824x.c,
       src/cmd/reset.c,
       src/cmd/salias.c,
Copyright: © 2003-2004 Marcel Telka <marcel@telka.sk>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/pxa2x0_mc.h
Copyright: © 2002-2003 ETC s.r.o.
License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the ETC s.r.o. nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Written by Marcel Telka <marcel@telka.sk>, 2002, 2003.

-------------------------------------------------------------------------------

Files: src/bus/s3c4510x.c
Copyright: © 2003 Jiun-Shian Ho <asky@syncom.com.tw>
           © 2002-2003 Marcel Telka <marcel@telka.sk>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/sharc21065l.c,
       src/flash/amd_flash.c,
Copyright: © 2006 Kila Medical Systems
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/slsup3.c,
       src/bus/writemem.c,
       src/cmd/writemem.c,
Copyright: © 2005 Kent Palmkvist
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/bus/tx4925.c
Copyright: © 2003 RightHand Technologies, Inc.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/cmd/debug.c,
       src/cmd/test.c,
Copyright: © 2005 Protoparts
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/cmd/eraseflash.c
Copyright: © 2003 Thomas Froehlich <t.froehlich@gmx.at>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/flash/amd.c
Copyright: © 2003 August Hörandl <august.hoerandl@gmx.at>
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/jim/intel_28f800b3.c,
       src/jim/some_cpu.c,
       src/jim/tap.c,
Copyright: © 2008 Kolja Waschk <kawk>
License: other
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
  
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
  
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.

-------------------------------------------------------------------------------

Files: src/lib/ansidecl.h,
       src/lib/filenames.h,
       src/lib/libiberty.h,
       src/lib/lrealpath.c,
       src/lib/make-relative-prefix.c,
       src/svf/svf_bison.[ch],
Copyright: © 1987-2007 Free Software Foundation, Inc.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/lib/fclock.c,
Copyright: © 2005 Hein Roehrig
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/lib/lbasename.c,
       src/lib/safe-ctype.[ch],
Copyright: © 2001-2002 Free Software Foundation, Inc.
License: LGPL-2+

On Debian systems, the complete text of the GNU Lesser General
Public License can be found in `/usr/share/common-licenses/LGPL'.

-------------------------------------------------------------------------------

Files: src/tap/cable.c
Copyright: © 2003 ETC s.r.o.
           © 2005 Hein Roehrig
           © 2008 Kolja Waschk
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/tap/usbconn.c,
       src/tap/cable/jim.c,
       src/tap/cable/usbblaster.c,
       src/tap/usbconn/libusb.c,
Copyright: © 2006-2008 Kolja Waschk
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/tap/cable/jlink.c
Copyright: © 2007 Juergen Stuber
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/tap/cable/wiggler2.c
Copyright: © 2003 Ultra d.o.o.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/tap/cable/xpc.c
Copyright: © 2008 Kolja Waschk
           © 2002-2003 ETC s.r.o.
License: GPL-2+

-------------------------------------------------------------------------------

Files: src/tap/parport/ppi.c
Copyright: © 2005 Daniel O'Connor
License: GPL-2+

-------------------------------------------------------------------------------

Files: data/*
Copyright: © 200x Various authors
License: GPL-2+

-------------------------------------------------------------------------------

Files: doc/UrJTAG.txt
Copyright: © 2007, 2008 Kolja Waschk (and others)
License: GFDL-1.2+
   Permission is granted to copy, distribute and/or modify this document under
   the terms of the GNU Free Documentation License, Version 1.2 or any later
   version published by the Free Software Foundation. A copy of the license
   is included in the section entitled "GNU Free Documentation License".

On Debian systems, the complete text of the GNU Free Documentation License
can be found in `/usr/share/common-licenses/GFDL'.

-------------------------------------------------------------------------------

Files: debian/*
Copyright: © 2008-2011 Uwe Hermann <uwe@debian.org>
License: PD
   The packaging done by Uwe Hermann <uwe@debian.org> is hereby
   released as public domain.


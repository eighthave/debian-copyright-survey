Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2002-2011, Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de>
           2006-2012, Daniel Hokka Zakrisson <daniel@hozac.com>
License: GPL-2

Files: lib/vserver.h scripts/vservers.grabinfo.sh src/capchroot.c
 src/chbind.c src/chcontext.c src/fakerunlevel.c src/ifspec.c
 src/listdevip.c src/parserpmdump.c src/rebootmgr.c src/reducecap.c
 src/showattr.c src/showperm.c src/vlimit.c src/vreboot.c
 src/vserver-stat.c tests/escaperoot.c tests/forkbomb.c
 tests/testipc.c tests/testlimit.c tests/testopenf.c
Copyright: 2003-2004, Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de>
License: GPL-2+

Files: lib_internal/crypto-wrapper-nss.h
Copyright: 2008, Enrico Scholz <enrico.scholz@informatik.tu-chemnitz.de>
License: GPL-3

Files: debian/*
Copyright: 2004, Noèl Köthe <noel@debian.org>
           2005, Lars Wirzenius <liw@iki.fi>
           2004-2006, Ola Lundqvist <opal@debian.org>
           2006-2010, Micah Anderson <micah@debian.org>
           2012, Christian Perrier <bubulle@debian.org>
           2013, Carlos Alberto Lopez Perez <clopez@igalia.com>
License: GPL-2+

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; under
 version 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

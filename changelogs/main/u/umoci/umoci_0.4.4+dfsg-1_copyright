Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: umoci
Source: https://github.com/openSUSE/umoci
Files-Excluded:
    .gitignore */.gitignore */*/.gitignore
    Godeps/_workspace
    vendor/github.com/apex/log
    vendor/github.com/blang/semver
    vendor/github.com/cyphar/filepath-securejoin
    vendor/github.com/docker/go-units
    vendor/github.com/fatih/color
    vendor/github.com/golang/protobuf
    vendor/github.com/hashicorp/errwrap
    vendor/github.com/hashicorp/go-multierror
    vendor/github.com/klauspost/compress
    vendor/github.com/klauspost/cpuid
    vendor/github.com/klauspost/crc32
    vendor/github.com/klauspost/pgzip
    vendor/github.com/mattn/go-colorable
    vendor/github.com/mattn/go-isatty
  ~~vendor/github.com/mohae/deepcopy
    vendor/github.com/opencontainers/go-digest
    vendor/github.com/opencontainers/image-spec
    vendor/github.com/opencontainers/image-tools
    vendor/github.com/opencontainers/runtime-spec
    vendor/github.com/opencontainers/runtime-tools
      vendor/github.com/opencontainers/runtime-tools/cmd
    vendor/github.com/pkg/errors
  ~~vendor/github.com/rootless-containers/proto
    vendor/github.com/sirupsen/logrus
    vendor/github.com/Sirupsen/logrus
    vendor/github.com/syndtr/gocapability
    vendor/github.com/urfave/cli
    vendor/github.com/vbatts/go-mtree
    vendor/github.com/xeipuuv/gojsonpointer
    vendor/github.com/xeipuuv/gojsonreference
    vendor/github.com/xeipuuv/gojsonschema
    vendor/golang.org/x

Files: *
Copyright:
    2016-2018 SUSE LLC.
    2018      Rootless Containers authors
    2013-2016 Docker, Inc.
License: Apache-2.0

Files:
    contrib/logo/*
Copyright:
    2018 Max Bailey <maxbailey@flywheel.io>
License: Apache-2.0

Files: vendor/github.com/mohae/deepcopy/*
Copyright: 2014-2016 Joel Scoble <github.com/mohae>
License: Expat

Files: vendor/github.com/rootless-containers/proto/*
Copyright: 2018 Rootless Containers Authors
License: Apache-2.0

Files: vendor/github.com/opencontainers/runtime-tools/*
Copyright: 2015 The Linux Foundation
License: Apache-2.0

Files: third_party/user/*
Copyright:
    2014 Docker, Inc.
    The Linux Foundation
License: Apache-2.0

Files: debian/*
Copyright: 2018 Dmitry Smirnov <onlyjob@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

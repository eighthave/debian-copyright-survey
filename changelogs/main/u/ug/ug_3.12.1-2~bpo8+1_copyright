Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ug
Source: http://www.iwr.uni-heidelberg.de/wiki-link-pages/wiki-software

Files: *
Copyright:
 1994-1998            Universität Stuttgart
 1992-1994, 1998-2014 Universität Heidelberg
License: LGPL-2.1+

Files: m4/ax_mpi.m4
Copyright:
 2008, Steven G. Johnson <stevenj@alum.mit.edu>
 2008, Julian C. Cummings <cummings@cacr.caltech.edu>
License: GPL-3+ with Autoconf Macro exception

Files: ui/mmio.*
Copyright:
                      National Institute of Standards and Technology
 1994-1998            Universität Stuttgart
 1992-1994, 1998-2014 Universität Heidelberg
License: LGPL-2.1+ and other
 These files are modified versions based on mmio.* from
 http://math.nist.gov/MatrixMarket. The original version was released
 under the terms below:
 .
 Fri Aug 15 16:29:47 EDT 1997
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     Matrix Market I/O library for ANSI C
     Roldan Pozo, NIST (pozo@nist.gov)
 .
     See http://math.nist.gov/MatrixMarket for details and sample
     calling programs.
 .
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                 NOTICE
 .
  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted
  provided that the above copyright notice appear in all copies and
  that both the copyright notice and this permission notice appear in
  supporting documentation.
 .
  Neither the Author nor the Institution (National Institute of Standards
  and Technology) make any representations about the suitability of this 
  software for any purpose. This software is provided "as is" without 
  expressed or implied warranty.
 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Comment:
 Copyright information for the original version was taken from
 the file inst/Copyrights in the rmatrix (1.1-3-1) source package.

Files: debian/*
Copyright: 2014, Ansgar Burchardt <ansgar@debian.org>
License: LGPL-2.1+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file /usr/share/common-licenses/LGPL-2.1.

License: GPL-3+ with Autoconf Macro exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 On Debian systems, the full text of the GNU General Public License version 3
 can be found in the file  `/usr/share/common-licenses/GPL-3'.

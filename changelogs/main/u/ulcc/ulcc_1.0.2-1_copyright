Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ulcc
Upstream-Contact: https://bitbucket.org/admsasha/ulcc/issues/new
Source: https://bitbucket.org/admsasha/ulcc/src/master/

Files: *
Copyright: 2018-2019 DanSoft <dik@inbox.ru>
License: GPL-3+

Files: dictionaries/african_animals/images/*
       dictionaries/different_animals/images/Bear-Plot.png
       dictionaries/piece_of_furniture/images/Brown-bed.png
       dictionaries/piece_of_furniture/images/Machovka-Bookcase.png
       dictionaries/piece_of_furniture/images/Rfc1394-Wooden-table.png
       dictionaries/piece_of_furniture/images/Stellaris-Cupboard.png
       dictionaries/piece_of_furniture/images/sofa.png
       dictionaries/piece_of_furniture/images/stool.png
       dictionaries/piece_of_furniture/images/wooden-chair-viyana.png
       dictionaries/professions/images/firefight03.png
       dictionaries/professions/images/policeman.png
Copyright: none
License: CC0-1.0

Files: debian/*
Copyright: 2020 Joao Eriberto Mota Filho <eriberto@debian.org>
License: GPL-3+

Files: debian/upstream/org.bitbucket.ulcc.metainfo.xml
Copyright: 2020 Joao Eriberto Mota Filho <eriberto@debian.org>
License: MIT

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along with
 this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license can be
 found in "/usr/share/common-licenses/CC0-1.0".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

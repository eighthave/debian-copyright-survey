Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Business-ISBN-Data
Source: https://metacpan.org/release/Business-ISBN-Data
Upstream-Author: brian d foy <bdfoy@cpan.org>

Files: *
Copyright: 2002-2012, brian d foy <bdfoy@cpan.org>
License: GPL-1+ or Artistic

Files: lib/Business/ISBN/RangeMessage.xml
Copyright: 2012, International ISBN Agency
License: custom-permissive-attribution
 From: "Stella Griffiths (ISBN)" <stella@isbn-international.org>
 To: fschlich@ZEDAT.FU-Berlin.DE
 Date: Wed, 26 Sep 2012 13:18:17 +0100
 Subject: RE: distributing a copy of RangeMessage.xml
 .
 Dear Florian
 .
 Thank you for contacting us about this.
 .
 The range message is made available publicly, without restriction, on our
 website since the intention is that it is a resource that can be used to
 provide a computer system with all the necessary data to correctly split 13
 digit ISBNs issued by any agency into the ISO defined segments of: [EAN.UCC
 prefix]-[Group Prefix]-[Registrant]-[book]-[check-digit].
 .
 It is also the intention that the file should be accessed each time it is
 necessary to carry out these tasks, or at least to have access to a file
 that is quite up to date (i.e. so the file should be downloaded regularly
 from our site).
 .
 We would not recommend that you download a copy and then make that
 particular instance only available within your package, for the simple
 reason that it will be quickly out of date. We regularly introduce new
 ranges or make revisions to existing ranges. None of these changes or
 adjustments would invalidate any pre-existing ISBN assignments, but they
 would affect and control future ones.
 .
 We would advise that you include a link to our range message
 (http://www.isbn-international.org/page/ranges) within your package. We
 would also ask that you include suitable attribution references making clear
 that the range message is provided by the International ISBN Agency Ltd.
 .
 Please let me know if you have further questions.
 .
 Kind Regards
 .
 Stella
 .
 Stella Griffiths
 Executive Director, International ISBN Agency

Files: debian/*
Copyright: 2007, Vincent Danjean <vdanjean@debian.org>
 2012, Florian Schlichting <fschlich@zedat.fu-berlin.de>
License: GPL-1+ or Artistic

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

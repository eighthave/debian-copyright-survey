Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: X2Go Desktop Sharing
Upstream-Contact: Oleksandr Shneyder <o.schneydr@phoca-gmbh.de>
Source: https://code.x2go.org/releases/source/x2godesktopsharing/

Files: ChangeLog
 Doxyfile
 VERSION.x2godesktopsharing
 dlg.ui
 icons/svg/black-list.svg
 icons/svg/dialog-question.svg
 icons/svg/display-allowed.svg
 icons/svg/display-protected.svg
 icons/svg/eye.svg
 icons/svg/quit.svg
 icons/svg/reconnect.svg
 icons/svg/stop.svg
 icons/svg/white-list.svg
 icons/svg/x2go-plug.svg
 icons/svg/x2godesktopsharing.svg
 icons/x2godesktopsharing.xpm
 icons/128x128/x2godesktopsharing.png
 icons/16x16/x2godesktopsharing.png
 icons/22x22/accept.png
 icons/22x22/discard.png
 icons/22x22/exit.png
 icons/22x22/share.png
 icons/22x22/stop.png
 icons/22x22/view.png
 icons/22x22/wlist.png
 icons/22x22/x2godesktopsharing.png
 icons/32x32/blist.png
 icons/32x32/exit.png
 icons/32x32/share.png
 icons/32x32/stop.png
 icons/32x32/view.png
 icons/32x32/wlist.png
 icons/32x32/x2godesktopsharing.png
 icons/64x64/x2godesktopsharing.png
 man/man1/x2godesktopsharing.1
 resources.qrc
 rpm/x2godesktopsharing-rpmlintrc
 x2godesktopsharing.desktop
 x2godesktopsharing.pro
 x2godesktopsharing.kdevelop.pcs
 x2godesktopsharing_cs.ts
 x2godesktopsharing_da.ts
 x2godesktopsharing_de.ts
 x2godesktopsharing_es.ts
 x2godesktopsharing_et.ts
 x2godesktopsharing_fr.ts
 x2godesktopsharing_fi.ts
 x2godesktopsharing_nb_no.ts
 x2godesktopsharing_nl.ts
 x2godesktopsharing_pt.ts
 x2godesktopsharing_ru.ts
 x2godesktopsharing_sv.ts
 x2godesktopsharing_tr.ts
 x2godesktopsharing_zh_tw.ts
Copyright: 2007-2015, Oleksandr Shneyder
  2017-2015, X2Go Project
License: GPL-2+
Comment:
 Assuming license as found in COPYING file.
 Assuming copyright holder from other code file.

Files: qt_da.qm
 qt_de.qm
 qt_es.qm
 qt_fr.qm
 qt_ru.qm
 qt_sv.qm
 qt_zh_tw.qm
Copyright: 2012 Nokia Corporation
  2015 The Qt Company Ltd.
License: LGPL-2.1-or-3 with Qt-1.1 exception
Comment:
 The qt_*.qm files have been copied over from Debian package
 qttranslations5-l10n by the upstream maintainers.

Files: accessdialog.cpp
 accessdialog.h
Copyright: 2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: main.cpp
 accessaction.cpp
 accessaction.h
 listdialog.cpp
 simplelocalsocket.cpp
 simplelocalsocket.h
Copyright: 2006-2015, Oleksandr Shneyder <o.schneydr@phoca-gmbh.de>
License: GPL-2+

Files: sharetray.cpp
 sharetray.h
Copyright: 2006-2015, Oleksandr Shneyder <o.schneydr@phoca-gmbh.de>
  2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: share/x2gofeature.d/x2godesktopsharing.features
Copyright: 2007-2018, X2Go Project - https://wiki.x2go.org
  2011-2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: debian/*
Copyright: 2007-2015, Oleksandr Shneyder <o.schneydr@phoca-gmbh.de>
  2011-2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1-or-3 with Qt-1.1 exception
 GNU Lesser General Public License:
 This file may be used under the terms of the GNU Lesser General Public
 License version 2.1 or version 3 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPLv21 and LICENSE.LGPLv3
 included in the packaging of this file. Please review the following
 information to ensure the GNU Lesser General Public License requirements
 will be met: https://www.gnu.org/licenses/lgpl.html.
 .
 In addition, as a special exception, The Qt Company gives you certain
 additional rights. These rights are described in The Qt Company Qt LGPL
 Exception version 1.1, included in the file LGPL_EXCEPTION.txt in this
 package.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1`.
 The complete text of the GNU Lesser General Public License version 3 can be
 found in `/usr/share/common-licenses/LGPL-3`.
 .
 The Qt Company LGPL Exception version 1.1:
 As an additional permission to the GNU Lesser General Public License version
 2.1, the object code form of a "work that uses the Library" may incorporate
 material from a header file that is part of the Library.  You may distribute
 such object code under terms of your choice, provided that:
     (i)   the header files of the Library have not been modified; and
     (ii)  the incorporated material is limited to numerical parameters, data
           structure layouts, accessors, macros, inline functions and
           templates; and
     (iii) you comply with the terms of Section 6 of the GNU Lesser General
           Public License version 2.1.
 .
 Moreover, you may apply this exception to a modified version of the Library,
 provided that such modification does not involve copying material from the
 Library into the modified Library's header files unless such material is
 limited to (i) numerical parameters; (ii) data structure layouts;
 (iii) accessors; and (iv) small macros, templates and inline functions of
 five lines or less in length.
 .
 Furthermore, you are not required to apply this additional permission to a
 modified version of the Library.

This is Debian GNU/Linux prepackaged version of the font collection
`Xcyr' ver. 2.3.7 by Serge Winitzki.  `Xcyr' is a descendant of the
package Xrus-2.3 by Andrey Chernov.

It was debianised by Anton Zinoviev <anton@lml.bas.bg>, and build from
sources obtained from:
  http://sawsoft.newmail.ru/LS/xcyr-2.3.tar.gz

Changes:
 * added Debian GNU/Linux package maintenance system files
 * fonts are converted by the `trbdf' script from KOI8-C to other
encodings
 * the fonts.alias is used to generate fonts.alias-files for all
encodings. 

The fonts are distributed with the following copyright:

 Copyright (C) 1994-1995 Cronyx Ltd.
 Changes Copyright (C) 1996-1997 by Andrey A. Chernov, Moscow, Russia.
 Changes Copyright (C) 1996 by Sergey Vovk
 Changes made in 1999 by Serge Winitzki

 This software may be used, modified, copied, distributed, and sold,
 in both source and binary form provided that the above copyright
 and these terms are retained. Under no circumstances is the author
 responsible for the proper functioning of this software, nor does
 the author assume any responsibility for damages incurred with its use.

The script `trbdf' used for conversion to other encodings is written
by me -- Anton Zinoviev, and is covered by GPL.  It is included only
in the source package and doesn't affect the binary packages in any
way.  On Debian GNU/Linux systems, the complete text of the GNU
General Public License can be found in `/usr/share/common-licenses/GPL'.

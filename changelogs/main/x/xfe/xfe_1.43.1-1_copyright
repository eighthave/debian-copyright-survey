Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xfe
Upstream-Contact: Roland Baudin <roland65@free.fr>
Source: http://sourceforge.net/projects/xfe/


Files: *
Copyright: 2002-2016, Roland Baudin <roland65@free.fr>
License: GPL-2+

Files: intl/*
Copyright: 1995-2004 Free Software Foundation, Inc.
License: LGPL
   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
   .
   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
   .
   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

Files: libsn/*
Copyright: 2009 Julien Danjou <julien at danjou dot info>
           2002 Red Hat, Inc.
License: MIT/X11

Files: st/*
Copyright: 2012-2015 Roberto E. Vargas Caballero <k0ga at shike2 dot com>
           2012-2015 Christoph Lohmann <20h at r-36 dot net>
           2014 Laslo Hunhold <dev at frign dot de>
           2013 Eon S. Jeon <esjeon at hyunmu dot am>
           2013 Alexander Sedov <alex0player at gmail dot com>
           2013 Mark Edgar <medgar123 at gmail dot com>
           2013 Eric Pruitt <eric.pruitt at gmail dot com>
           2013 Michael Forney <mforney at mforney dot org>
           2013-2014 Markus Teich <markus dot teich at stusta dot mhn dot de>
           2009-2012 Aurélien APTEL <aurelien dot aptel at gmail dot com>
           2009 Anselm R Garbe <garbeam at gmail dot com>
License: MIT/X11

Files: debian/*
Copyright: 2009-2018, Joachim Wiedorn <ad_debian@joonet.de>
License: GPL-2+


License: MIT/X11
  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  DEALINGS IN THE SOFTWARE.

License: GPL-2+
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by 
  the Free Software Foundation; either version 2 of the License, or 
  (at your option) any later version.
  .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  .
  On Debian systems, the complete text of the GNU General Public License
  version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

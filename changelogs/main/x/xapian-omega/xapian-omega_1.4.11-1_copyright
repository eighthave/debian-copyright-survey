This is the Debian package of the Xapian Omega search system.  This
package was originally created and maintained by Richard Boulton
<richard@tartarus.org>.  It is currently maintained by Olly Betts
<olly@survex.com>.

It was downloaded from http://xapian.org/download

Upstream Authors:

        Olly Betts <olly@survex.com>
        James Aylett <tartarus@users.sourceforge.net>
        Sam Liddicott <samjam@users.sourceforge.net>
	Ananova Ltd
	BrightStation PLC
	Intercede 1749 Ltd
	Lemur Consulting Ltd
	Thomas Viehmann
	Frank J Bruzzaniti
	Mihai Bivol
	Aarsh Shah

Copyright:

  Copyright (C) 2002-2017 Olly Betts
  Copyright (C) 2001,2005 James Aylett
  Copyright (C) 2001 Sam Liddicott
  Copyright (C) 2001,2002 Ananova Ltd
  Copyright (C) 1999,2000,2001 BrightStation PLC
  Copyright (C) 2002 Intercede 1749 Ltd
  Copyright (C) 2001,2003,2008 Lemur Consulting Ltd.
  Copyright (C) 2008 Thomas Viehmann
  Copyright (C) 2009 Frank J Bruzzaniti
  Copyright (C) 2012 Mihai Bivol
  Copyright (C) 2013 Aarsh Shah

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 2 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

  See /usr/share/common-licenses/GPL-2 for the full text of the GNU GPL.

The cdb reading code in files cdb.h, cdb_int.h, cdb_init.cc, cdb_find.cc,
cdb_hash.cc, and cdb_unpack.cc is based on code from tinycdb released into the
public domain by Michael Tokarev:

  This file is a part of tinycdb package by Michael Tokarev, mjt@corpit.ru.
  Public domain.

The code to produce MD5 message-digests in files md5.cc and md5.h is based on
code written and released into the public domain by Colin Plumb:

  This code was written by Colin Plumb in 1993, no copyright is claimed.
  This code is in the public domain; do with it what you wish.

A portable replacement for mkdtemp() (not used in Debian builds) in
portability/mkdtemp.cc has a 3-clause BSD licence:

  Copyright (c) 1987, 1993
	The Regents of the University of California.  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.

An implementation of dirent functions using WIN32 API in msvc_dirent.cc has
the following licence:

    Copyright Kevlin Henney, 1997, 2003. All rights reserved.

    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose is hereby granted without fee, provided
    that this copyright and permissions notice appear in all copies and
    derivatives.
    
    This software is supplied "as is" without express or implied warranty.

    But that said, if there are any problems please get in touch.

The file m4/ax_cxx_compile_stdcxx_11.m4 is licensed as follows:

   Copyright (c) 2008 Benjamin Kosnik <bkoz@redhat.com>
   Copyright (c) 2012 Zack Weinberg <zackw@panix.com>
   Copyright (c) 2013 Roy Stogner <roystgnr@ices.utexas.edu>
   Copyright (c) 2014, 2015 Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>

   Copying and distribution of this file, with or without modification, are
   permitted in any medium without royalty provided the copyright notice
   and this notice are preserved. This file is offered as-is, without any
   warranty.

The Debian packaging is licensed under the MIT/X licence:

  Copyright (C) 2004,2005,2006 Lemur Consulting Ltd
  Copyright (C) 2006-2017 Olly Betts

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.

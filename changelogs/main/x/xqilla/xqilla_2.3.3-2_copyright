Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XQilla
Source: http://xqilla.sourceforge.net

Files: *
Copyright: 2001-2008 DecisionSoft Limited.
 2004-2008 Oracle
License: Apache-2.0

Files: src/yajl
Copyright: 2007 Lloyd Hilaiel
License: yajl

Files: src/mapm include/xqilla/mapm/*
Copyright: 1999 - 2003   Michael C. Ring
License: mapm

Files: debian/*
Copyright: 2008 Daniel Ruoso
 2009- Tommi Vainikainen
License: Apache-2.0

License: Apache-2.0
 Licensed to the Apache Software Foundation (ASF) under one or more
 contributor license agreements.  See the NOTICE file distributed with
 this work for additional information regarding copyright ownership.
 The ASF licenses this file to You under the Apache License, Version 2.0
 (the "License"); you may not use this file except in compliance with
 the License.  You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache Software License version 2 can
 be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: mapm
 This software is Freeware.
 .
 Permission to use, copy, and distribute this software and its 
 documentation for any purpose with or without fee is hereby granted, 
 provided that the above copyright notice appear in all copies and 
 that both that copyright notice and this permission notice appear 
 in supporting documentation.
 .
 Permission to modify the software is granted. Permission to distribute 
 the modified code is granted. Modifications are to be distributed by 
 using the file 'license.txt' as a template to modify the file header. 
 'license.txt' is available in the official MAPM distribution.
 .
 To distribute modified source code, insert the file 'license.txt' 
 at the top of all modified source code files and edit accordingly.
 .
 This software is provided "as is" without express or implied warranty.

License: yajl
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
 .
  3. Neither the name of Lloyd Hilaiel nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

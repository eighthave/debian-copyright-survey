The sources where downloaded from
http://pigeonholdings.com/projects/flyingsaucer/R8/downloads/flyingsaucer-R8-src.zip.

Files: *
Copyright: © 2004-2009, Joshua Marinacci <joshua@marinacci.org>,
 © 2004-2009, Patrick Wright,
 © 2004-2009, The Flying Saucer Team,
 © 2004-2008, Torbjörn Gannholm,
 © 2004-2008, Wisconsin Court System,
 © 2007,2009, Christophe Marchand,
 © 2007-2008, Sean Bright,
 © 2005, Scott Cytacki,
 © 2007, Jason Blumenkrantz,
 © 2007, Nick Reddel
License: LGPL-2.1
Comment: Since several source code files don't have an header indicating
 copyright, license or author, upstream was contacted for clarification.
 Below is the email exchange with them:

 Date: Thu, 20 Jan 2011 10:40:31 -0430
 From: Miguel Landaeta <miguel@miguel.cc>
 To: Josh Marinacci <joshua@marinacci.org>
 Cc: tony mancill <tmancill@debian.org>
 Message-ID: <AANLkTi=7u_=jy4vT0cKoWcYyhe2QaoCuxdp0RVCu5XzV@mail.gmail.com>
 Subject: xhtmlrenderer Debian package
 
 Hi Joshua,
 
 I'm just emailing you to let you know about a Debian package that I'm
 preparing for xhtmlrenderer and to ask you some questions about the
 licensing and copyright of some files included in the source tarball released
 by your project.
 
 In the LICENSE file is stated the following:
 
 "All source code to Flying Saucer itself is licensed under the GNU Lesser
 General Public License (LGPL); you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 2.1 of the License, or (at your option)
 any
 later version."
 
 However there are some files without a header and they don't have an author
 or copyright statement available:
 
 ./src/java/org/xhtmlrenderer/tool/Boxer.java
 ./src/java/org/xhtmlrenderer/simple/PDFRenderer.java
 ./src/java/org/xhtmlrenderer/pdf/DefaultPDFCreationListener.java
 ./src/java/org/xhtmlrenderer/simple/XHTMLPrintable.java
 ./src/java/org/xhtmlrenderer/util/PermutationGenerator.java
 ./src/java/org/xhtmlrenderer/util/GenerateBigFile.java
 several files under ./src/java/org/xhtmlrenderer/test/*
 several files under ./src/java/org/xhtmlrenderer/pdf/*
 
 (There are probably more, the check could be more exhaustive if
 necessary).
 
 Are those files also licensed under LGPL-2.1 license?
 If not, how are licensed those files?
 
 [snip]
 
 
 Date: Thu, 20 Jan 2011 07:22:47 -0800
 From: Josh Marinacci <joshua@marinacci.org>
 To: Miguel Landaeta <miguel@miguel.cc>
 Cc: tony mancill <tmancill@debian.org>, Patrick Wright <patrick@pdoubleya.com>
 In-Reply-To: <AANLkTi=7u_=jy4vT0cKoWcYyhe2QaoCuxdp0RVCu5XzV@mail.gmail.com>
 References: <AANLkTi=7u_=jy4vT0cKoWcYyhe2QaoCuxdp0RVCu5XzV@mail.gmail.com>
 Message-Id: <F11DDD18-F6BF-41C7-BB4C-47C8B4416249@marinacci.org>
 Subject: Re: xhtmlrenderer Debian package
 
 Hi Miguel. A Debian package sounds great.   I'm actually not the project =
 leader anymore (not for a few years now). I've CC'ed Patrick who can =
 give you the info you need.  The site is down due to the Java.net =
 transition, though I believe most of the site is actually stored other =
 places now.
 
 good luck!
 	J


 Date: Thu, 20 Jan 2011 18:43:43 +0100
 From: Patrick Wright <patrick@pdoubleya.com>
 To: Miguel Landaeta <miguel@miguel.cc>
 Cc: tony mancill <tmancill@debian.org>, "Peter Brant" <peter.brant@gmail.com>,
  Josh Marinacci <joshua@marinacci.org>
 In-Reply-To: <F11DDD18-F6BF-41C7-BB4C-47C8B4416249@marinacci.org>
 References: <AANLkTi=7u_=jy4vT0cKoWcYyhe2QaoCuxdp0RVCu5XzV@mail.gmail.com>
 <F11DDD18-F6BF-41C7-BB4C-47C8B4416249@marinacci.org>
 Message-Id: <C607FD3D-B5DA-4882-A68D-0194E59772A3@pdoubleya.com>
 Subject: Re: xhtmlrenderer Debian package
 
 Hi Miguel
 
 1) Regarding licensing - our intention has always been that all the  
 code for Flying Saucer itself is licensed under the LGPL. If any are  
 missing the license header, this is most likely an oversight on our  
 part which no one has brought to our attention. The only exception(s)  
 might be the documentation and perhaps some of the demo code, but I  
 would need to double-check that. The library itself is LGPL. I believe  
 we are comfortable that our CVS (and now GitHub) repository shows the  
 origin of the files in our source tree. We can correct any licensing  
 issues in the source tree.
 
 [snip]


Files: demos/docbook/MozBook/MozBook.css
Author: David Horton
License: Public Domain

Files: demos/docbook/docbook-css-0.4/*
Copyright: © 2004, David Holroyd and contributors 
License: other (BSD-like)
 Permission to use, copy, modify and distribute this software and its
 documentation for any purpose and without fee is hereby granted in
 perpetuity, provided that the above copyright notice appear in all
 copies, and that both the copyright notice and this permission notice
 appear in supporting documentation. The contributors make no
 representations about the suitability of this software for any
 purpose. It is provided "as is" without express or implied warranty.
 
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: demos/docbook/wysiwygdocbook1.01/*
Copyright: © 2004, David Holroyd and contributors,
 © 2005 Michael Thiele, and contributors
License: other (BSD-like)
 Permission to use, copy, modify and distribute this software and its
 documentation for any purpose and without fee is hereby granted in
 perpetuity, provided that the above copyright notice appear in all
 copies, and that both the copyright notice and this permission notice
 appear in supporting documentation. The contributors make no
 representations about the suitability of this software for any
 purpose. It is provided "as is" without express or implied warranty.
 
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: demos/docbook/dtd/*,
 src/schema/docbook/*.mod,
 src/schema/docbook/*.dtd
Copyright: © 1992-2004, HaL Computer Systems, Inc.,
 © 1992-2002, O'Reilly & Associates, Inc.,
 © 1992-2002, ArborText, Inc.,
 © 1992-2002, Fujitsu Software Corporation,
 © 1992-2002, Norman Walsh,
 © 1992-2002, Sun Microsystems, Inc.,
 © 1992-2002, Organization for the Advancement of Structured Information Standards (OASIS)
License: other
 Permission to use, copy, modify and distribute the DocBook XML DTD
 and its accompanying documentation for any purpose and without fee
 is hereby granted in perpetuity, provided that the above copyright
 notice and this paragraph appear in all copies.  The copyright
 holders make no representation about the suitability of the DTD for
 any purpose.  It is provided "as is" without expressed or implied
 warranty.

 If you modify the DocBook XML DTD in any way, except for declaring and
 referencing additional sets of general entities and declaring
 additional notations, label your DTD as a variant of DocBook.  See
 the maintenance documentation for more information.

Files: src/schema/*.ent
Copyright: © 1986, International Organization for Standardization
License: other
 Permission to copy in any form is granted for use with
 conforming SGML systems and applications as defined in
 ISO 8879, provided this notice is included in all copies.

Files: src/schema/xhtml/*.mod,
 src/schema/xhtml/*.dtd
Copyright: © 1998-2001, W3C (MIT, INRIA, Keio)
Comment: Even though those *.mod and *.dtd files don't indicate a license,
 those files are copies of the ones distributed in w3c-dtd-xhtml package.
 Those files are known to be licensed under W3C Software Licence which
 is compatible with the GPL2 license.
License: W3C Software License
 W3C Software License

 According to http://www.w3.org/Consortium/Legal/IPR-FAQ-20000620.html,
 "while schemas and DTDs are frequently part of our specifications and
 seemingly fall under the document copyright terms, you may use them under
 the W3C Software License."

 http://www.w3.org/Consortium/Legal/copyright-software-19980720 :


                       W3C ® SOFTWARE NOTICE AND LICENSE

  Copyright © 1994-2002 World Wide Web Consortium, (Massachusetts
  Institute of Technology, Institut National de Recherche en Informatique
  et en Automatique, Keio University). All Rights Reserved.
  http://www.w3.org/Consortium/Legal/

   This W3C work (including software, documents, or other related items)
   is being provided by the copyright holders under the following
   license. By obtaining, using and/or copying this work, you (the
   licensee) agree that you have read, understood, and will comply with
   the following terms and conditions:

   Permission to use, copy, modify, and distribute this software and its
   documentation, with or without modification,  for any purpose and
   without fee or royalty is hereby granted, provided that you include
   the following on ALL copies of the software and documentation or
   portions thereof, including modifications, that you make:
    1. The full text of this NOTICE in a location viewable to users of
       the redistributed or derivative work.
    2. Any pre-existing intellectual property disclaimers, notices, or
       terms and conditions. If none exist, a short notice of the
       following form (hypertext is preferred, text is permitted) should
       be used within the body of any redistributed or derivative code:
       "Copyright © [$date-of-software] World Wide Web Consortium,
       (Massachusetts Institute of Technology, Institut National de
       Recherche en Informatique et en Automatique, Keio University).
       All Rights Reserved. http://www.w3.org/Consortium/Legal/"
    3. Notice of any changes or modifications to the W3C files, including
       the date changes were made. (We recommend you provide URIs to the
       location from which the code is derived.)
   THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT
   HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED,
   INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS
   FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE OR
   DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS,
   TRADEMARKS OR OTHER RIGHTS.

   COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL
   OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR
   DOCUMENTATION.

   The name and trademarks of copyright holders may NOT be used in
   advertising or publicity pertaining to the software without specific,
   written prior permission. Title to copyright in this software and any
   associated documentation will at all times remain with copyright
   holders.

   ____________________________________

   This formulation of W3C's notice and license became active on August
   14 1998 so as to improve compatibility with GPL. This version ensures
   that W3C software licensing terms are no more restrictive than GPL and
   consequently W3C software may be distributed in GPL packages. See the
   older formulation for the policy prior to this date. Please see our
   Copyright FAQ for common questions about using materials from our
   site, including specific terms and conditions for packages like
   libwww, Amaya, and Jigsaw. Other questions about this notice can be
   directed to site-policy@w3.org.

Files: debian/*
Copyright: © 2011, Miguel Landaeta <miguel@miguel.cc>
License: LGPL-2.1

License: LGPL-2.1
 The full text of the GNU Lesser General Public License is
 distributed in /usr/share/common-licenses/LGPL-2.1 on Debian systems.

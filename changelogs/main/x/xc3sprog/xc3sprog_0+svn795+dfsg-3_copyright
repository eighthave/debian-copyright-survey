Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xc3sprog
Source: https://sourceforge.net/p/xc3sprog/code/
Files-Excluded: *.exe
Upstream-Contact: https://sourceforge.net/p/xc3sprog/mailman/

Files: *
Copyright: 2004 Andrew Rogers
           2005-2018 Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
           2001 AJ Erasmus <antone@sentechsa.com>
License: GPL-2+

Files: debian/*
Copyright: 2018 Ricardo Ribalda Delgado <ricardo@ribalda.com>
License: GPL-2+

Files: atxmega128a1_nvm_regs.h xmega_pdi_nvm.cpp
Copyright: 2009, Atmel Corporation.
License: BSD-3-clause

Files: FindlibFTD2XX.cmake
Copyright: 2009 SoftPLC Corporation, Dick Hollenbeck <dick@softplc.com>
License: GPL-2+

Files: bitfile.cpp
Copyright: 2004 Andrew Rogers
           2007 Dominic Rath <Dominic.Rath@gmx.de>
           2007,2008 Øyvind Harboe <oyvind.harboe@zylin.com>
           2008 Spencer Oliver <spen@spen-soft.co.uk>
License: GPL-2+

Files: io_exception.h
Copyright: 2008 Thomas Preusser
License: GPL-2+

Files: ioftdi.h
Copyright: 2005-2013 Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
           2006 Dmitry Teytelman
License: GPL-2+

Files: ioxpc.cpp
Copyright: 2005-2011 Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
           2008 Kolja Waschk
License: GPL-2+

Files: jedecfile.cpp jedecfile.h
Copyright: 2009 Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
           2000 Stephen Williams <steve@icarus.com>
License: GPL-2+

Files: libusb_dyn.c
Copyright: 2002-2005 Stephan Meyer <ste_meyer@web.de>
License: LGPL-2+

Files: progalgxc95x.cpp
Copyright: 2008-2009 Uwe Bonnes <bon@elektron.ikp.physik.tu-darmstadt.de>
           2001 Nahitafu, Naitou Ryuji
License: GPL-2+

Files: progalgxcfp.cpp progalgxcfp.h
Copyright: 2010 Joris van Rantwijk
License: GPL-2+

Files: xc3sprog.1
Copyright: 2011 Joris van Rantwijk
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the Institute nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: LGPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 The complete text of the GNU Lesser General Public License
 can be found in /usr/share/common-licenses/LGPL-2 file.

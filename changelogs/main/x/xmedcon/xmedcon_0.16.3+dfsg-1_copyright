Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: (X)MedCon
Upstream-Contact: Erik Nolf <enlf@users.sourceforge.net>
Source: http://xmedcon.sourceforge.net/Main/Download
Files-Excluded: */libs/nifti
                */Makefile.in
                */aclocal.m4
                */compile
                */config.*
                */configure
                */depcomp
                */install-sh
                */ltconfig
                */ltmain.sh
                */missing
                */mkinstalldirs

Files: *
Copyright: © 1997-2013 Erik Nolf <enlf@users.sourceforge.net>
License: GPL-2+

Files: libs/dicom/*
Copyright: © 1998-2002 Tony Voet <tony.voet@uzgent.be>
           © 2002-2009 Erik Nolf <enlf@users.sourceforge.net>
License: LGPL-2.1+

Files: libs/dicom/dict-dicom.dic
Copyright: © 1994-2005 Andrew Hewett, Marco Eichelberg (OFFIS)
           © 2006 Erik Nolf <enlf@users.sourceforge.net>
License: OFFISdicom
 This software and supporting documentation were developed by
 .
    Kuratorium OFFIS e.V.
    Healthcare Information and Communication Systems
    Escherweg 2
    D-26121 Oldenburg, Germany
 .
 THIS SOFTWARE IS MADE AVAILABLE,  AS IS,  AND OFFIS MAKES NO  WARRANTY
 REGARDING  THE  SOFTWARE,  ITS  PERFORMANCE,  ITS  MERCHANTABILITY  OR
 FITNESS FOR ANY PARTICULAR USE, FREEDOM FROM ANY COMPUTER DISEASES  OR
 ITS CONFORMITY TO ANY SPECIFICATION. THE ENTIRE RISK AS TO QUALITY AND
 PERFORMANCE OF THE SOFTWARE IS WITH THE USER.
 .
 Copyright of the software  and  supporting  documentation  is,  unless
 otherwise stated, owned by OFFIS, and free access is hereby granted as
 a license to  use  this  software,  copy  this  software  and  prepare
 derivative works based upon this software.  However, any  distribution
 of this software source code or supporting documentation or derivative
 works  (source code and  supporting documentation)  must  include  the
 three paragraphs of this copyright notice.

Files: libs/ljpg/*
Copyright: © 2002 Jaslet Bertrand
           © 2002-2009 Erik Nolf <enlf@users.sourceforge.net>
License: LGPL-2.1+

Files: libs/tpc/ecat7*.*
Copyright: © 2005-2013 by Turku PET Centre
License: LGPL-2.1+
Comment: This is a stripped down version from the Turku PET Centre libraries
 The whole set of files can be downloaded from
    http://www.turkupetcentre.net/software/libsrc/libtpcimgio_2_1_8_src.zip

Files: libs/tpc/petc99.* libs/tpc/swap.*
Copyright: © 2000-2005 by Turku PET Centre
License: LGPL-2.1+
Comment: This is a stripped down version from the Turku PET Centre libraries
 The whole set of files can be downloaded from
    http://www.turkupetcentre.net/programs/libpet_src.zip

Files: source/m-qmedian.c
Copyright: © 1988-1997 Sam Leffler
           © 1991-1997 Silicon Graphics, Inc.
License: SClicense
  "Permission to use, copy, modify, distribute, and sell this software and
   its documentation for any purpose is hereby granted without fee,
   provided that (i) the above copyright notices and this permission notice
   appear in all copies of the software and related documentation, and
   (ii) the names of Sam Leffler and Silicon Graphics may not be used in
   any advertising or publicity relating to the software without the
   specific, prior written permission of Sam Leffler and Silicon Graphics."
Comment: code was adapted from 'tiffmedian.c' (see http://www.libtiff.org)

Files: debian/*
Copyright: © 2002-2010 Roland Marcus Rutschmann <Roland.M.Rutschmann@uni-oldenburg.de>
           © 2013 Andreas Tille <tille@debian.org>
License: GPL-2+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems you can find a full copy of LGPL 2.1+ at
 /usr/share/common-licenses/LGPL-2.1 .

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems you can find a full copy of GPL 2+ at
 /usr/share/common-licenses/GPL-2 .


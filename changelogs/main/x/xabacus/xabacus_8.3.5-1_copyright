Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: Abacus
Upstream-Contact: David A. Bagley <bagleyd AT verizon.net>
Source: http://www.sillycycle.com/abacus/

Files: *
Copyright: 1992-2021, David A. Bagley <bagleyd AT verizon.net>
	   with Thanks to,
           Luis Fernandes <elf AT ee.ryerson.ca>
            - independent xabacus 1.00, initial demo
           Sarat Chandran <saratcmahadevan AT yahoo.com>
            - collaboration on a few ideas
  .
  The files file.{c,h} and sound.{c,h} are taken from xlock, many authors...
  .
  For the files rngs.{c,h} the following is given:
  Dr. Park's algorithm published in the Oct. '88 ACM
  "Random Number Generators: Good Ones Are Hard To Find"
  His version available at ftp://cs.wm.edu/pub/rngs.tar
  Present form by many authors.
License: Custom

Files: install-sh
Copyright: 1994,  X Consortium
License: MIT

Files: debian/*
Copyright: 2019-2021 Jonathan Carter <jcc@debian.org>
	   2017-2018 Innocent De Marchi <tangram.peces@gmail.com>
           2017 Florian Ernst <florian@debian.org>
           2006 Jose Carlos Medeiros <debian@psabs.com.br>
           2005 Florian Ernst <florian@debian.org>
           2004 Eric Van Buggenhaut <ericvb@debian.org>
           2001 Adrian Bunk <bunk@fs.tum.de>
	   2000 Christian Kurz <shorty@debian.org>
           1998-2000, Darren Benham <gecko@debian.org>
License: GPL-3+

License: Custom
  All Rights Reserved
  .
  Permission to use, copy, modify, and distribute this software and
  its documentation for any purpose and without fee is hereby granted,
  provided that the above copyright notice appear in all copies and
  that both that copyright notice and this permission notice appear in
  supporting documentation, and that the name of the author not be
  used in advertising or publicity pertaining to distribution of the
  software without specific, written prior permission.
  .
  This program is distributed in the hope that it will be "useful",
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: MIT
 Copyright (c) 1994 X Consortium
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.
 .
 FSF changes to this file are in the public domain.

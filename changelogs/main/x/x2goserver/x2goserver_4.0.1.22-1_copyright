Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: X2Go Server
Upstream-Contact: Heinz-M. Graesing <heinz-m.graesing@obviously-nice.de>
Source: http://code.x2go.org/releases/source/x2goserver/

Files: x2goserver-compat/bin/x2gosuspend
 x2goserver-compat/bin/x2gosuspend-agent
 x2goserver-compat/share/x2gofeature.d/x2goserver-compat.features
 x2goserver-extensions/share/x2gofeature.d/x2goserver-extensions.features
 x2goserver-fmbindings/share/x2go/x2gofeature.d/x2goserver-fmbindings.features
 x2goserver-printing/bin/x2goprint
 x2goserver-printing/share/x2gofeature.d/x2goserver-printing.features
 x2goserver-pyhoca/share/x2gofeature.d/x2goserver-pyhoca.features
 x2goserver-xsession/share/x2gofeature.d/x2goserver-xsession.features
 x2goserver/bin/x2gocmdexitmessage
 x2goserver/bin/x2gofeature
 x2goserver/bin/x2gogetapps
 x2goserver/bin/x2gogetservers
 x2goserver/bin/x2golistdesktops
 x2goserver/bin/x2golistmounts
 x2goserver/bin/x2golistsessions
 x2goserver/bin/x2golistshadowsessions
 x2goserver/bin/x2gomountdirs
 x2goserver/bin/x2goresume-session
 x2goserver/bin/x2goruncommand
 x2goserver/bin/x2gosessionlimit
 x2goserver/bin/x2gostartagent
 x2goserver/bin/x2gosuspend-session
 x2goserver/bin/x2goterminate-session
 x2goserver/bin/x2goumount-session
 x2goserver/bin/x2goversion
 x2goserver/lib/x2gochangestatus
 x2goserver/lib/x2gocreatesession
 x2goserver/lib/x2godbwrapper.pm
 x2goserver/lib/x2gogetagent
 x2goserver/lib/x2gogetagentstate
 x2goserver/lib/x2gogetdisplays
 x2goserver/lib/x2gogetports
 x2goserver/lib/x2gogetstatus
 x2goserver/lib/x2goinsertport
 x2goserver/lib/x2goinsertsession
 x2goserver/lib/x2golistsessions_sql
 x2goserver/lib/x2gologlevel
 x2goserver/lib/x2gologlevel.pm
 x2goserver/lib/x2goresume
 x2goserver/lib/x2gormforward
 x2goserver/lib/x2gormport
 x2goserver/lib/x2gosqlitewrapper.pl
 x2goserver/lib/x2gosuspend-agent
 x2goserver/lib/x2gosyslog
 x2goserver/sbin/x2gocleansessions
 x2goserver/sbin/x2godbadmin
 x2goserver/sbin/x2golistsessions_root
 x2goserver/sbin/x2goshowblocks
 x2goserver/share/x2gofeature.d/x2goserver.features
Copyright: 2007-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2007-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
  2007-2015, X2Go Project - http://wiki.x2go.org
  2011-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2011-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
License: GPL-2+

Files: x2goserver-extensions/bin/x2goserver-run-extensions
 x2goserver/bin/x2gosetkeyboard
 x2goserver/lib/x2goutils.pm
Copyright: 2007-2015, X2Go Project - http://wiki.x2go.org
  2011-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2011-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2011-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
  2013-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2013-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2013-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
  2013-2015, X2Go Project - http://wiki.x2go.org
License: GPL-2+

Files: x2goserver-compat/bin/x2goterminate
Copyright: 2007-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2007-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
  2007-2015, X2Go Project - http://wiki.x2go.org
  2010-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2013-2015, Guangzhou Nianguan Electronics Technology Co.Ltd. <opensource@gznianguan.com>
License: GPL-2+

Files: x2goserver/bin/x2gopath
Copyright: 2007-2015, X2Go Project - http://wiki.x2go.org
  2012-2015, Heinz-Markus Graesing <heinz-m.graesing@obviously-nice.de>
  2012-2015, Jan Engelhard <jengelh@inai.de>
  2012-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2012-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
License: GPL-2+

Files: x2goserver-x2goagent/share/x2go/x2gofeature.d/x2goserver-x2goagent.features
Copyright: 2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: x2goserver/x2gosqlitewrapper.c
Copyright: 2011-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2011-2015, Moritz 'Morty' Strübe <morty@gmx.net>
License: GPL-2+

Files: x2goserver/bin/x2gofeaturelist
Copyright: 2007-2015, X2Go Project - http://wiki.x2go.org
  2011-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: x2goserver-fmbindings/bin/x2gofm
Copyright: 2007-2015, X2Go Project - http://wiki.x2go.org
  2012-2015, Milan Knížek <knizek.confy@gmail.com>
  2012-2015, Oleksandr Shneyder <oleksandr.shneyder@obviously-nice.de>
License: GPL-2+

Files: Makefile.docupload
Copyright: 2010-2015, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: x2goserver-x2goagent/bin/x2goagent
Copyright: 2012-2016, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3+

Files: ChangeLog
 INSTALL
 Makefile
 README.i18n
 UNINSTALL
 rpm/x2goserver-rpmlintrc
 rpm/x2goserver.init
 x2goserver-compat/Makefile
 x2goserver-compat/VERSION.x2goserver-compat
 x2goserver-compat/man/man8/x2gosuspend-agent.8
 x2goserver-compat/man/man8/x2gosuspend.8
 x2goserver-compat/man/man8/x2goterminate.8
 x2goserver-extensions/Makefile
 x2goserver-extensions/VERSION.x2goserver-extensions
 x2goserver-extensions/lib/extensions/fail-resume.d/.placeholder
 x2goserver-extensions/lib/extensions/fail-runcommand.d/.placeholder
 x2goserver-extensions/lib/extensions/fail-start.d/.placeholder
 x2goserver-extensions/lib/extensions/fail-suspend.d/.placeholder
 x2goserver-extensions/lib/extensions/fail-terminate.d/.placeholder
 x2goserver-extensions/lib/extensions/post-resume.d/.placeholder
 x2goserver-extensions/lib/extensions/post-runcommand.d/.placeholder
 x2goserver-extensions/lib/extensions/post-start.d/.placeholder
 x2goserver-extensions/lib/extensions/post-suspend.d/.placeholder
 x2goserver-extensions/lib/extensions/post-terminate.d/.placeholder
 x2goserver-extensions/lib/extensions/pre-resume.d/.placeholder
 x2goserver-extensions/lib/extensions/pre-runcommand.d/.placeholder
 x2goserver-extensions/lib/extensions/pre-start.d/.placeholder
 x2goserver-extensions/lib/extensions/pre-suspend.d/.placeholder
 x2goserver-extensions/lib/extensions/pre-terminate.d/.placeholder
 x2goserver-extensions/man/man8/x2goserver-run-extensions.8
 x2goserver-fmbindings/Makefile
 x2goserver-fmbindings/VERSION.x2goserver-fmbindings
 x2goserver-fmbindings/man/man8/x2gofm.8
 x2goserver-fmbindings/share/applications/x2gofm.desktop
 x2goserver-fmbindings/share/mime/packages/sshfs-x2go.xml
 x2goserver-printing/Makefile
 x2goserver-printing/VERSION.x2goserver-printing
 x2goserver-printing/man/man8/x2goprint.8
 x2goserver-pyhoca/Makefile
 x2goserver-pyhoca/VERSION.x2goserver-pyhoca
 x2goserver-pyhoca/bin/.placeholder
 x2goserver-pyhoca/man/man8/.placeholder
 x2goserver-x2goagent/Makefile
 x2goserver-x2goagent/VERSION.x2goserver-x2goagent
 x2goserver-x2goagent/etc/keystrokes.cfg
 x2goserver-x2goagent/etc/x2goagent.keyboard
 x2goserver-x2goagent/etc/x2goagent.options
 x2goserver-x2goagent/man/man1/x2goagent.1
 x2goserver-x2goagent/share/pixmaps/x2go.xpm
 x2goserver-xsession/Makefile
 x2goserver-xsession/VERSION.x2goserver-xsession
 x2goserver-xsession/doc/README.Xsession-X2Go
 x2goserver-xsession/etc/Xsession
 x2goserver.service
 x2goserver/Makefile
 x2goserver/VERSION.x2goserver
 x2goserver/bin/x2gobasepath
 x2goserver/doc/README.sudoers
 x2goserver/doc/README.upgrade-pgsql-database
 x2goserver/etc/logcheck/ignore.d.server/x2goserver
 x2goserver/etc/sudoers.d/x2goserver
 x2goserver/etc/x2go_logout
 x2goserver/etc/x2go_logout.d/010_userscripts.sh
 x2goserver/etc/x2goserver.conf
 x2goserver/etc/x2gosql/sql
 x2goserver/man/man5/x2goserver.conf.5
 x2goserver/man/man8/x2gobasepath.8
 x2goserver/man/man8/x2gocmdexitmessage.8
 x2goserver/man/man8/x2godbadmin.8
 x2goserver/man/man8/x2gofeature.8
 x2goserver/man/man8/x2gofeaturelist.8
 x2goserver/man/man8/x2gogetapps.8
 x2goserver/man/man8/x2gogetservers.8
 x2goserver/man/man8/x2golistdesktops.8
 x2goserver/man/man8/x2golistmounts.8
 x2goserver/man/man8/x2golistsessions.8
 x2goserver/man/man8/x2golistshadowsessions.8
 x2goserver/man/man8/x2golistsessions_root.8
 x2goserver/man/man8/x2gomountdirs.8
 x2goserver/man/man8/x2gopath.8
 x2goserver/man/man8/x2goresume-session.8
 x2goserver/man/man8/x2goruncommand.8
 x2goserver/man/man8/x2gosessionlimit.8
 x2goserver/man/man8/x2gosetkeyboard.8
 x2goserver/man/man8/x2goshowblocks.8
 x2goserver/man/man8/x2gostartagent.8
 x2goserver/man/man8/x2gosuspend-session.8
 x2goserver/man/man8/x2goterminate-session.8
 x2goserver/man/man8/x2goumount-session.8
 x2goserver/man/man8/x2goversion.8
Copyright: *No copyright*
License: GPL-2+
Comment:
 From upstream commits, most of the listed files are copyrighted by Mike
 Gabriel <mike.gabriel@das-netzwerkteam.de> between 2011 and 2014.

Files: debian/*
Copyright: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License:GPL-3
 This software is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; version 3 only of the License.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA 
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

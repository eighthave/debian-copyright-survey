Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libevdev
Source: http://xye.sourceforge.net
Files-Excluded: res/DejaVuSans-Bold.ttf res/DejaVuSans.ttf

Files: *
Copyright: 2006 Victor Hugo Soliz Kuncar
License: Xye

Files: res/detailed*
Copyright: 2011 Miguel Angel Soliz Kuncar
License: Ancestral
 This resource is provided as-is, without any express or implied warranty. In no
 event will the authors be held liable for any damages arising from the use of
 them.
 .
 Permission is granted to anyone to use this resource for any purpose, and to
 alter it and redistribute it freely, subject to the following restrictions:
 .
     1. When publicly distributing the original resource or an altered version
 of the resource as part of a software product, the source code of the software
 must be made available and the software product must be in the public domain or
 be released under a license that follows the Free Software definition and the
 Open Source software definition as of September, 2011. The software product may
 be used commercially.
 .
     2. The origin of this resource must not be misrepresented; you must not
 claim that you are the original creator of the resource. If you use this
 resource in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
     3. When publically distributing the original resource or an altered version
 of it for third party use, this notice must be included unaltered in the
 resource itself or as a separate file distributed with it.
 .
     4. Altered versions must be plainly marked as such, and must not be
 misrepresented as being the original resource.

Files: src/tinyxml/*
Copyright: Lee Thomason (www.grinninglizard.com)
License: zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

Files: levels/Phipps.xye
Copyright: 2004 Colin Phipps
License: Phipps
 Colin Phipps has released these levels in his site (http://games.moria.org.uk/kye/mylevels)
 Under this license:
 .
 All of my levels here are copyright 2004 by myself, Colin Phipps. You may do
 whatever you want with them as long as I get a credit as the original author.

Files: debian/*
Copyright: 2011-2012 Bart Martens <bartm@debian.org>
           2013 Andreas Rönnquist <gusnan@gusnan.se>
           2018-2019 Stephen Kitt <skitt@debian.org>
License: Xye

License: Xye
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
     1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software in a
 product, an acknowledgment in the product documentation would be appreciated
 but is not required.
 .
     2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
     3. This notice may not be removed or altered from any source distribution.

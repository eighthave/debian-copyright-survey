Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: flightgear-data
Upstream-Contact: Curtis L. Olson <curt@flightgear.org>
Source: http://www.flightgear.org
Files-Excluded:
 .gitignore
 .git
 AI
 Aircraft/ufo
 Aircraft/Generic
 Aircraft/Instruments
 Aircraft/Instruments-3d
 Aircraft/c172p/c172*
 Aircraft/c172p/*.jpg
 Aircraft/c172p/*.md
 Aircraft/c172p/*.png
 Aircraft/c172p/dev
 Aircraft/c172p/Engines
 Aircraft/c172p/gui
 Aircraft/c172p/io_scene_ac3d.zip
 Aircraft/c172p/LICENSE
 Aircraft/c172p/Models
 Aircraft/c172p/Nasal
 Aircraft/c172p/Paintkit
 Aircraft/c172p/Panels
 Aircraft/c172p/Sounds
 Aircraft/c172p/states
 Aircraft/c172p/Systems
 Aircraft/c172p/Thanks
 Aircraft/c172p/Tutorials
 Aircraft/c172p/WebPanel/bootstrap
 Aircraft-uiuc
 Airports
 Astro
 ATC
 AtlasPalette
 AUTHORS
 ChangeLog
 COPYING
 Docs
 Effects
 Environment
 Fonts
 Geodata
 gui
 HLA
 httpd-settings.xml
 Huds
 Input
 joysticks.xml
 keyboard.xml
 Lighting
 location-presets.xml
 Materials
 mice.xml
 Models
 MP
 Nasal
 Navaids
 NEWS
 options.xml
 preferences.xml
 Phi/3rdparty
 Protocol
 README
 README-textures-ng
 runfgfs.bat
 Scenery
 Shaders
 Sounds
 Textures
 Thanks
 Timezone
 Translations
 version
 Video

Files: *
Copyright: 1999-2012 Curtis L. Olson <curt@flightgear.org>
 and the FlightGear team (see the Thanks file).
License: GPL-2+

Files: debian/*
Copyright: 2001-2012 Ove Kaaven <ovek@arcticnet.no>
           2013 Scott Howard <showard@debian.org>
           2013-2016 Markus Wanner <markus@bluegap.ch>
           2016 Rebecca N. Palmer <rebecca_palmer@zoho.com>
License: GPL-2+

Files: debian/missing-sources/sammy.full.js
Copyright: 2008 Aaron Quint, Quirkey NYC, LLC
License: MIT

Files: debian/missing-sources/knockout-jqueryui-2.2.2/*
Copyright: 2012,2014 Vas GÃ¡bor
License: MIT

Files: debian/missing-sources/knockout-3.4.0/*
Copyright: Steven Sanderson, the Knockout.js team, and other contributors
License: MIT

Files: debian/missing-sources/knockout-3.4.0/src/tasks.js
Copyright: 2014 Petka Antonov
           2012 Barnesandnoble.com, llc, Donavon West, and Domenic Denicola
License: MIT

Files: debian/missing-sources/clockpicker-gh-pages/*
Copyright: 2014 Wang Shenwei
           2011-2014 Twitter, Inc
License: MIT

Files: debian/missing-sources/jui_theme_switch-master/*
Copyright: Christos Pontikis <christos@pontikis.net>
License: MIT

Files: debian/missing-sources/Leaflet.Geodesic-master/*
Copyright: 2014 Henry Thasler
           2014 Chris Veness
License: GPL-3+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 A copy of the license is available on Debian systems at
 /usr/share/common-licenses/GPL-2

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 .
 A copy of the license is available on Debian systems at
 /usr/share/common-licenses/GPL-3

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

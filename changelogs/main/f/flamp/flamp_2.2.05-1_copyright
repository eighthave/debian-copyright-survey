Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: flamp
Upstream-Contact: Dave Freese <w1hkj@w1hkj.com>
Source: http://www.w1hkj.com/download.html

Files: *
Copyright: 2008-2013 Dave Freese <w1hkj@w1hkj.com>
           2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-2+

Files: debian/*
Copyright: 2014 Kamal Mostafa <kamal@whence.com>
License: GPL-2+

Files: src/utils/base*
Copyright:  2010 Dave Freese <w1hkj@w1hkj.com>
License: GPL-1+

Files: src/utils/circular_queue.cxx
Copyright: 2013 Dave Freese <w1hkj@w1hkj.com>
           2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-3+

Files: src/utils/debug.cxx
Copyright: 2008, 2012 Stelios Bounanos, M0GLD
           2008, 2012 Dave Freese <w1hkj@w1hkj.com>
License: GPL-3+

Files: src/utils/file_io.cxx
Copyright: 2012 Dave Freese <w1hkj@w1hkj.com>
           2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-3+

Files: src/utils/mingw.c
Copyright: 2009 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/utils/socket.cxx
Copyright: 2008-2009 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/utils/status.cxx
Copyright: 2010 Dave Freese <w1hkj@w1hkj.com>
           2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-1+

Files: src/utils/tagSearch.cxx
Copyright: 2013 Dave Freese <w1hkj@w1hkj.com>
           2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-3+

Files: src/utils/timeops.cxx
Copyright: 2007-2009 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/utils/time_table.cxx
Copyright: 2013 Robert Stiles <kk5vd@yahoo.com>
License: GPL-1+

Files: src/utils/util.cxx
Copyright: 2010 Stelios Bounanos, M0GLD
           2010 Dave Freese <w1hkj@w1hkj.com>
License: GPL-1+





Files: src/widgets/calendar.cxx
Copyright: 2010 Dave Freese <w1hkj@w1hkj.com>
License: GPL-1+

Files: src/widgets/date.cxx
Copyright: 1998, 2010 Dave Freese <w1hkj@w1hkj.com>
License: GPL-2+

Files: src/widgets/fileselect.cxx
Copyright: 2008-2009 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/widgets/Fl_BlockMap.cxx
Copyright: 2012 Dave Freese <w1hkj@w1hkj.com>
License: LGPL-2+

Files: src/widgets/flinput2.cxx
Copyright: 2010 Stelios Buononos, M0GLD
License: GPL-1+

Files: src/widgets/Fl_Native_File_Chooser.cxx
Copyright: 2004 by Greg Ercolano
License: LGPL-2+

Files: src/widgets/Fl_Native_File_Chooser_FLTK.cxx
Copyright: 2004 by Greg Ercolano
License: LGPL-2+

Files: src/widgets/Fl_Native_File_Chooser_MAC.cxx
Copyright: 2004, 2007 by Greg Ercolano
License: LGPL-2+

Files: src/widgets/Fl_Native_File_Chooser_WIN32.cxx
Copyright: 2004 by Greg Ercolano
           2005 Nathan Vander Wilt
License: LGPL-2+

Files: src/widgets/flnfc_common.cxx
Copyright: 2004 by Greg Ercolano
License: LGPL-2+

Files: src/widgets/flslider2.cxx
Copyright: 2010 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/widgets/Fl_Text_Buffer_mod_1_1.cxx
Copyright: 2001-2005 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/Fl_Text_Buffer_mod_1_3.cxx
Copyright: 2001-2010 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/Fl_Text_Display_mod_1_1.cxx
Copyright: 2001-2006 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/Fl_Text_Display_mod_1_3.cxx
Copyright: 2001-2010 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/Fl_Text_Editor_mod_1_1.cxx
Copyright: 2001-2006 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/Fl_Text_Editor_mod_1_3.cxx
Copyright: 2001-2010 by Bill Spitzak, Mark Edel, et al.
License: LGPL-2+

Files: src/widgets/font_browser.cxx
Copyright: 2007 Dave Freese <w1hkj@w1hkj.com>
License:GPL-3+

Files: src/widgets/FTextView.cxx
Copyright: 2007-2009 Stelios Bounanos, M0GLD
           2008-2009 Dave Freese <w1hkj@w1hkj.com>
License: GPL-3+

Files: src/widgets/icons.cxx
Copyright: 2008 Stelios Bounanos, M0GLD
License: GPL-3+

Files: src/widgets/missing_strings.c
Copyright: 1998-2005 by Bill Spitzak and others
License: LGPL-2+

Files: src/xmlrpcpp/*
Copyright: 2002-2008 by Chris Morley
License: LGPL-2.1+

License: GPL-1+
 On Debian systems, the complete text of the GNU General
 Public License version 1 can be found in "/usr/share/common-licenses/GPL-1".

License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 On Debian systems, the complete text of the GNU General Lesser Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU General Lesser Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Author: Daniel Friesel <derf+feh@finalrewind.org>
                 The former author which provided the main part of the code was
                 Tom Gilbert <tom@linuxbrit.co.uk>
Packaged-By: Laszlo Bszormenyi (GCS) <gcs@debian.org> since 2014
             Andreas Tille <tille@debian.org> from 2008 to 2010
             Laurence J. Lane <ljlane@debian.org> from 2000 to 2007
Original-Source-Location: https://feh.finalrewind.org/
                          versions before 1.3.5 can be found at the old homepage
                          http://linuxbrit.co.uk/software/feh/

Files: *
Copyright:  © 2000-2005 Tom Giilbert <tom@linuxbrit.co.uk>,
            © 2010-2018 Daniel Friesel <derf+feh@finalrewind.org>
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies of the Software, its documentation and marketing & publicity
 materials, and acknowledgment shall be given in the documentation, materials
 and software packages that this Software was used.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: © 2008-2010 Andreas Tille <tille@debian.org>,
 © 2014- Laszlo Boszormenyi (GCS) <gcs@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

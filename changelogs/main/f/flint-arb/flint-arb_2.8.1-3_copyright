Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: arb
Upstream-Contact: Fredrik Johansson
Source: https://github.com/fredrik-johansson/arb

Files: *
Copyright: 2011-2015 Fredrik Johansson
License: GPL-2+

Files: acb_mat/transpose.c acb_mat/ne.c acb_mat/set_arb_mat.c acb_mat/eq.c acb_mat/set_round_arb_mat.c acb_mat/set_round_fmpz_mat.c acb_mat/transpose.c arb_mat/ne.c acb_mat/eq.c arb_mat/set_round_fmpz_mat.c arb_mat/transpose.c arb_mat/eq.c
Copyright: 2012-2015 Tommy Hofmann
License: GPL-2+

Files: acb_poly/divrem.c acb_poly/test/t-shift_left_right.c acb_poly/test/t-get_set_coeff_fmprb.c acb_poly/mullow_classical.c acb_poly/div_root.c acb_poly/product_roots.c arb_poly/div_root.c arb_poly/test/t-shift_left_right.c arb_poly/test/t-get_set_coeff_arb.c arb_poly/divrem.c arb_poly/product_roots.c
Copyright:  2009-2011 William Hart
License: GPL-2+

Files: acb_mat/trace.c acb_mat/sqr.c acb_mat/test/t-sqr.c acb_mat/test/t-trace.c arb_mat/sqr.c arb_mat/sqr_classical.c arb_mat/trace.c arb_mat/test/t-sqr.c arb_mat/test/t-trace.c arb/test/t-sinc.c arb/sinc.c
Copyright: 2015 Arb authors
License: GPL-2+

Files: acb_poly/compose_divconquer.c acb_poly/compose_horner.c acb_poly/shift_right.c acb_poly/compose.c acb_poly/shift_left.c arb_poly/mullow_classical.c arb_poly/compose_divconquer.c arb_poly/compose.c arb_poly/shift_left.c arb_poly/shift_right.c arb_poly/compose_horner.c
Copyright:
  2008-2010 William Hart
  2010-2012 Sebastian Pancratz
License: GPL-2+

Files: acb_poly/test/t-get_coeff_ptr.c acb_poly/evaluate_horner.c acb_mat/charpoly.c arb_poly/evaluate_horner.c arb_poly/test/t-get_coeff_ptr.c arb_poly/evaluate_acb_horner.c
Copyright: 2010,2012 Sebastian Pancratz
License: GPL-2+

Files: arf.h
Copyright: 1991-2007 Free Software Foundation, Inc.
License: GPL-2+

Files: examples/*
Copyright: Fredrik Johansson
License: public-domain
  This file is public domain. Author: Fredrik Johansson

Files: debian/*
Copyright: 2015 Julien Puydt
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

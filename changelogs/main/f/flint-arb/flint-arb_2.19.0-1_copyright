Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: arb
Upstream-Contact: Fredrik Johansson
Source: https://github.com/fredrik-johansson/arb

Files: *
Copyright: 2011-2020 Fredrik Johansson
License: LGPL-2.1+

Files: acb_mat/set_arb_mat.c acb_mat/set_round_arb_mat.c acb_mat/set_round_fmpz_mat.c acb_mat/transpose.c arb_mat/ne.c arb_mat/set_round_fmpz_mat.c arb_mat/transpose.c arb_mat/eq.c
Copyright: 2012-2015 Tommy Hofmann
License: LGPL-2.1+

Files: acb_poly/divrem.c acb_poly/test/t-shift_left_right.c acb_poly/test/t-get_set_coeff_fmprb.c acb_poly/mullow_classical.c acb_poly/div_root.c acb_poly/product_roots.c arb_poly/div_root.c arb_poly/test/t-shift_left_right.c arb_poly/test/t-get_set_coeff_arb.c arb_poly/divrem.c arb_poly/product_roots.c
Copyright:  2009-2011 William Hart
License: LGPL-2.1+

Files: acb_mat/trace.c acb_mat/sqr.c acb_mat/test/t-sqr.c acb_mat/test/t-trace.c arb_mat/sqr.c arb_mat/sqr_classical.c arb_mat/trace.c arb_mat/test/t-sqr.c arb_mat/test/t-trace.c arb/test/t-sinc.c arb/sinc.c
Copyright: 2015 Arb authors
License: LGPL-2.1+

Files: acb_poly/compose_divconquer.c acb_poly/compose_horner.c acb_poly/shift_right.c acb_poly/compose.c acb_poly/shift_left.c arb_poly/mullow_classical.c arb_poly/compose_divconquer.c arb_poly/compose.c arb_poly/shift_left.c arb_poly/shift_right.c arb_poly/compose_horner.c
Copyright:
  2008-2010 William Hart
  2010-2012 Sebastian Pancratz
License: LGPL-2.1+

Files: acb_poly/test/t-get_coeff_ptr.c acb_poly/evaluate_horner.c acb_mat/charpoly.c arb_poly/evaluate_horner.c arb_poly/test/t-get_coeff_ptr.c arb_poly/evaluate_acb_horner.c
Copyright: 2010,2012 Sebastian Pancratz
License: LGPL-2.1+

Files: arf.h
Copyright: 1991-2007 Free Software Foundation, Inc.
License: LGPL-2.1+

Files: acb_dirichlet/* arb_poly/sinc_pi_series.c arb_poly/test/t-sinc_pi_series.c
Copyright: 2019 D.H.J Polymath
License: LGPL-2.1+

Files: acb_dirichlet/hardy_z_zero.c acb_dirichlet/platt_local_hardy_z_zeros.c
Copyright: 2019 D.H.J Polymath
	   2019 Fredrik Johansson
License: LGPL-2.1+

Files: arb/dump_file.c arb/dump_str.c arb/test/t-dump_file.c arb/test/t-dump_str.c arb/load_str.c arb/load_file.c arf/load.c arf/dump_file.c arf/dump_str.c arf/test/t-dump_file.c arf/test/t-dump_str.c mag/load.c mag/dump_file.c mag/dump_str.c mag/test/t-dump_file.c mag/test/t-dump_str.c
Copyright: 2019 Julian Rüth
License: LGPL-2.1+

Files: examples/*
Copyright: Fredrik Johansson
License: public-domain
  This file is public domain. Author: Fredrik Johansson

Files: debian/*
Copyright: 2015-2020 Julien Puydt
License: LGPL-2.1+

License: LGPL-2.1+
 This file is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or (at
 your option) any later version.
 .
 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this file. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

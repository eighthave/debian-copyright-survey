Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fplll
Upstream-Contact: Damien Stehlé <damien.stehle@ens-lyon.fr>
Source: https://github.com/fplll/fplll/

Files: *
Copyright: 2007, David Cade <david.cade@ens.fr>
	   2005-2008, Damien Stehle  <damien.stehle@gmail.com>
           2008-2011, Xavier Pujol <xavier.pujol@ens-lyon.fr>
License: LGPL-2.1+

Files: fplll/nr/dpe.h
Copyright: 2004, 2005, 2006, 2008 Patrick Pelissier, Paul Zimmermann, LORIA/INRIA Nancy - Grand-Est.
License: LGPL-2.1+

Files: fplll/pruner.* tests/*
Copyright: 2015-2016 Martin Albrecht, Leo Ducas
License: LGPL-2.1+

Files: fplll/io/json.hpp
Copyright: 2013-2016 Niels Lohmann
License: Expat

Files: debian/*
Copyright: 2008, Tim Abbott <tabbott@mit.edu>
	   2012-2016, Julien Puydt <julien.puydt@laposte.net>
License: LGPL-2.1+

License: LGPL-2.1+
 This file is part of fplll. fplll is free software: you
 can redistribute it and/or modify it under the terms of the GNU Lesser
 General Public License as published by the Free Software Foundation,
 either version 2.1 of the License, or (at your option) any later version.
 .
 fplll is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with fplll. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

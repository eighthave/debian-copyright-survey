Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FastML
Upstream-Contact: Haim Ashkenazy <haim.ashkenazy@gmail.com>
Source: http://fastml.tau.ac.il/source.php#download

Files: *
Copyright: 1996-2014 Tal Pupko <talp@post.tau.ac.il>
License: GPL-2+
Comment: Upstream confirmed GPL-2 via e-mail and has updated
 the wab page accordingly.
 .
 Until now the "Copyrights" paragraph at the end of the README is
 not yet changed in a new download tarball but the change of the
 licence on the web page was triggered after an e-mail to the
 copyright holders
   https://lists.debian.org/debian-med/2015/11/msg00042.html
 and was confirmed in private mail.

Files: programs/gainLoss/*
Copyright: 2011 Tal Pupko <TalP@tauex.tau.ac.il>
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems you can find the complete text of the GNU General
 Public License version 3 at /usr/share/common-licenses/GPL.

Files: libs/phylogeny/getopt*
Copyright: 1987-2001 Free Software Foundation, Inc.
License: LGPL-2.1+
   This file is part of the GNU C Library.
 .
   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.
 .
   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
 .
 On Debian systems you can find the complete text of the GNU Lesser
 General Public License version 3 at /usr/share/common-licenses/LGPL-2.1.

Files: debian/*
Copyright: 2015 Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 On Debian systems you can find the complete text of the GNU General
 Public License version 2 at /usr/share/common-licenses/GPL-2.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fcrackzip
Upstream-Contact: Marc Lehmann <schmorp@schmorp.de>, <pcg@goof.com>
Source: http://oldhome.schmorp.de/marc/fcrackzip.html

Files: *
Copyright: 1998-2008 Marc Lehmann <schmorp@schmorp.de>
License: GPL-2

Files: cpmask.c
Copyright: 1998 Hirotsuna Mizuno <s1041150@u-aizu.ac.jp>
           ?    Marc Lehmann <schmorp@schmorp.de>
License: GPL-2

Files: getopt.c
       getopt.h
       getopt1.c
Copyright: 1987-1996 Free Software Foundation, Inc.
License: LGPL-2+

Files: debian/*
Copyright: 2004      Francesco Donadon <francesco.donadon@katamail.com>
           2010-2013 Jari Aalto <jari.aalto@cante.net>
           2013      Tony Mancill
           2015      Adam Borowski <kilobyte@angband.pl>
           2016-2021 Giovani Augusto Ferreira <giovani@debian.org>
           2016      Joao Eriberto Mota Filho <eriberto@debian.org>
           2019-2020 Samuel Henrique <samueloph@debian.org>
License: GPL-2+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

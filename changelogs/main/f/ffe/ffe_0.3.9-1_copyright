Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ffe - flat file extractor
Upstream-Contact: Timo Savinen <tjsa@iki.fi>
Source: https://sourceforge.net/projects/ff-extractor/files/

Files: *
Copyright: 2006-2018 Timo Savinen <tjsa@iki.fi>
License: GPL-2+

Files: debian/*
Copyright: 2006 Alexis Bezverkhyy <bezverky@gmail.com>
License: GPL-2+

Files: doc/ffe.texi doc/ffe.html doc/ffe.info
Copyright: 2014 Alexis Bezverkhyy <bezverky@gmail.com>
License: permissive
 Permission is granted to make and distribute verbatim copies of
 this manual provided the copyright notice and this permission notice
 are preserved on all copies.
 .
 Permission is granted to copy and distribute modified versions of this
 manual under the conditions for verbatim copying, provided that the entire
 resulting derived work is distributed under the terms of a permission
 notice identical to this one.
 .
 Permission is granted to copy and distribute translations of this manual
 into another language, under the above conditions for modified versions.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

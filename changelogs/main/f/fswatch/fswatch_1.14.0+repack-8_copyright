Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fswatch
Source: https://github.com/emcrisostomo/fswatch
Files-Excluded: fswatch/doc/fswatch.info
                fswatch/doc/stamp-vti
                fswatch/doc/version.texi
                Makefile.in
                config/compile
                config/install-sh
                config/ltmain.sh
                config/mdate-sh
                config/missing
                config/texinfo.tex
                configure
                fswatch/Makefile.in
                fswatch/doc/Makefile.in
                fswatch/src/Makefile.in
                libfswatch/Makefile.in
                libfswatch/doc/doxygen/Makefile.in
                libfswatch/src/libfswatch/Makefile.in
                m4/libtool.m4
                man/fswatch.7
Comment: Multiple Files-Excluded would be helpful
 - first fswatch/doc are prebuild
 - Makefile.in and follow are removed by debclean

Files: *
Copyright: 2013-2018, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
License: GPL-3+

Files: aclocal.m4
       config/*
       m4/gettext.m4
       m4/iconv.m4
       m4/intlmacosx.m4
       m4/lib-ld.m4
       m4/lib-link.m4
       m4/lib-prefix.m4
       m4/lt~obsolete.m4
       m4/ltoptions.m4
       m4/ltsugar.m4
       m4/ltversion.m4
       m4/nls.m4
       m4/po.m4
       m4/progtest.m4
Copyright: 1995-2017, Free Software Foundation, Inc
License: FSFULLR

Files: config/config.guess
       config/config.sub
Copyright: 1992-2017, Free Software Foundation, Inc
License: GPL-3+ with AutoConf exception

Files: config/ar-lib
       config/depcomp
Copyright: 1999-2017, Free Software Foundation, Inc
License: GPL-2+ with AutoConf exception

Files: fswatch/src/gettext.h
       libfswatch/src/libfswatch/gettext.h
Copyright: 1995-2011, Free Software Foundation, Inc
License: GPL-3+

Files: man/*
Copyright: 2013-2018, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
License: GPL-3+

Files: fswatch/doc/fswatch.texi
Copyright: 2013-2015 Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
License: GFDL-NIV-1.3

Files: m4/ax_cxx_compile_stdcxx.m4
       m4/ax_cxx_compile_stdcxx_11.m4
Copyright: 2008, Benjamin Kosnik <bkoz@redhat.com>
           2012, Zack Weinberg <zackw@panix.com>
           2013, Roy Stogner <roystgnr@ices.utexas.edu>
           2014-2015, Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
           2015, Paul Norman <penorman@mac.com>
           2015, Moritz Klammler <moritz@klammler.eu>
License: FSFAP

Files: m4/ax_git_current_branch.m4
       m4/emc_path_prog.m4
Copyright: 2015-2016, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
License: GPL-3+ with AutoConf exception

Files: m4/ax_cxx_have_thread_local.m4
       m4/ax_fsevents_have_file_events.m4
Copyright: 2014, Enrico M. Crisostomo <enrico.m.crisostomo@gmail.com>
License: FSFAP

Files: m4/ax_append_flag.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
           2011, Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+ with AutoConf exception

Files: m4/ax_cflags_warn_all.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
           2010, Rhys Ulerich <rhys.ulerich@gmail.com>
License: GPL-3+ with AutoConf exception

Files: m4/ax_require_defined.m4
Copyright: 2014, Mike Frysinger <vapier@gentoo.org>
License: FSFAP

Files: debian/*
Copyright: 2016-2019 Alf Gaida <agaida@siduction.org>
License: GPL-3+

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

License: FSFULLR
 This file is free software; the Free Software Foundation gives unlimited
 permission to copy and/or distribute it, with or without modifications, as
 long as this notice is preserved.

License: GFDL-NIV-1.3
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 .
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GFDL-1.3".

License: GPL-2+ with AutoConf exception
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in
 "/usr/share/common-licenses/GPL-2".
 .
 As a special exception, the Free Software Foundation gives unlimited permission
 to copy, distribute and modify the configure scripts that are the output of
 Autoconf. You need not follow the terms of the GNU General Public License when
 using or distributing such scripts, even though portions of the text of
 Autoconf appear in them. The GNU General Public License (GPL) does govern all
 other use of the material that constitutes the Autoconf program.
 .
 Certain portions of the Autoconf source text are designed to be copied (in
 certain cases, depending on the input) into the output of Autoconf. We call
 these the "data" portions. The rest of the Autoconf source text consists of
 comments plus executable code that decides which of the data portions to
 output in any given case. We call these comments and executable code the
 "non-data" portions. Autoconf never copies any of the non-data portions into
 its output.
 .
 This special exception to the GPL applies to versions of Autoconf released by
 the Free Software Foundation. When you make and distribute a modified version
 of Autoconf, you may extend this special exception to the GPL to apply to your
 modified version as well, *unless* your modified version has the potential to
 copy into its output some of the text that was the non-data portion of the
 version that you started with. (In other words, unless your change moves or
 copies text from the non-data portions to the data portions.) If your
 modification has such potential, you must delete any notice of this special
 exception to the GPL from your modified version.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-3+ with AutoConf exception
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 .
 As a special exception, the Free Software Foundation gives unlimited permission
 to copy, distribute and modify the configure scripts that are the output of
 Autoconf. You need not follow the terms of the GNU General Public License when
 using or distributing such scripts, even though portions of the text of
 Autoconf appear in them. The GNU General Public License (GPL) does govern all
 other use of the material that constitutes the Autoconf program.
 .
 Certain portions of the Autoconf source text are designed to be copied (in
 certain cases, depending on the input) into the output of Autoconf. We call
 these the "data" portions. The rest of the Autoconf source text consists of
 comments plus executable code that decides which of the data portions to
 output in any given case. We call these comments and executable code the
 "non-data" portions. Autoconf never copies any of the non-data portions into
 its output.
 .
 This special exception to the GPL applies to versions of Autoconf released by
 the Free Software Foundation. When you make and distribute a modified version
 of Autoconf, you may extend this special exception to the GPL to apply to your
 modified version as well, *unless* your modified version has the potential to
 copy into its output some of the text that was the non-data portion of the
 version that you started with. (In other words, unless your change moves or
 copies text from the non-data portions to the data portions.) If your
 modification has such potential, you must delete any notice of this special
 exception to the GPL from your modified version.

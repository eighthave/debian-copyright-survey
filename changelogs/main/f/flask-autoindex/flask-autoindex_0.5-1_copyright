Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Flask-AutoIndex
Source: https://pypi.python.org/pypi/Flask-AutoIndex
Comment:
 Flask-AutoIndex uses Flask-Silk (available from python-flask-silk) to serve
 icons. Per default, the icons from the Silk icon set by Mark James are used.
 These icons are released under CC-BY-3.0. Before using the icons, read their
 license which can be found in /usr/share/doc/python-flask-silk/copyright.

Files: *
Copyright: 2010-2013 Heungsub Lee
License: BSD-3-clause

Files: docs/_themes/*
Copyright: 2010 Armin Ronacher <armin.ronacher@active-4.com>
License: BSD-3-clause

Files: debian/*
Copyright: 2013 Sebastian Ramacher <sramacher@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. The name of the author may not be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This is the Debian GNU/Linux r-cran-fbasics package of fBasics, a set
of functions for basic statistics, date and time management, and part
of Rmetrics, a collection of packages for financial engineering and
computational finance. Both fBasics and Rmetrics were written and
compiled primarily by Diethelm Wuertz, with code by others (see below
for fBasics).

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://www.rmetrics.org
and are also available from
	http://cran.r-project.org/src/contrib/
and all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'fBasics' to
'r-cran-fbasics' to fit the pattern of CRAN (and non-CRAN) packages
for R.

Copyright (C) 1999 - 2008 Diethelm Wuertz
Copyright (C) 1999 - 2008 Rmetrics Foundation

License: GPL

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

   Package: fBasics
   Version: 190.10056
   Date: 1996 - 2004
   Title: Financial Software Collection - fBasics
   Author: Diethelm Wuertz and many others, see the SOURCE file
   Depends: R (>= 1.9.0), methods
   Maintainer: Diethelm Wuertz <wuertz@itp.phys.ethz.ch>
   Description: fBasics Library from Rmetrics -
   	Rmetrics is an Environment and 
   	Software Collection for teaching
   	"Financial Engineering and Computational Finance"
   License: GPL Version 2 or later
   URL: http://www.itp.phys.ethz.ch/econophysics/R/1.9
   Packaged: Tue Jun 15 06:17:06 2004; myself

and the following segment was extracted from the header of src/zzz.R:

   # Copyrights (C)
   # for this R-port: 
   #   Diethelm Wuertz <wuertz@itp.phys.ethz.ch>
   # for the code accessed (or partly included) from other R-ports:
   #   R: see R's copyright and license file
   #   date: Terry Therneau <therneau@mayo.edu>
   #     R port by Th. Lumley <thomas@biostat.washington.edu>  K. Halvorsen 
   #       <khal@alumni.uv.es>, and Kurt Hornik <Kurt.Hornik@R-project.org>
   #   ts: Collected by Brian Ripley. See SOURCES
   #   tseries: Compiled by Adrian Trapletti <a.trapletti@bluewin.ch>
   # for ical:
   #   libical: Libical is an Open Source implementation of the IETF's 
   #     iCalendar Calendaring and Scheduling protocols. (RFC 2445, 2446, 
   #     and 2447). It parses iCal components and provides a C API for 
   #     manipulating the component properties, parameters, and subcomponents.
   #   Olsen's VTIMEZONE: These data files are released under the GNU 
   #     General Public License, in keeping with the license options of 
   #     libical. 
   # for the holiday database:
   #   holiday information collected from the internet and governmental 
   #   sources obtained from a few dozens of websites  




Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: freedroidrpg
Source: http://www.freedroid.org

Files: *
Copyright: 1994, 2002-2004 Johannes Prix
           1994, 2002-2004 Reinhard Prix
           2009-2010 the Freedroid dev team
           2003 Pete Spicer
           2006 Arvid Picciani
           2007-2010 Stefan Huszics
           2007 Karol Swietlicki
           1989-2010 Free Software Foundation, Inc.
           2004-2013 Arthur Huillet
           2008-2016 Samuel Degrande
           2011 Miles McCammon
           2006 Angelo Theodorou
           2004 Scott James Remnant <scott@netsplit.co>
           2010 Maleesh Prasan
           2010 Alexander Solovets
           2010 Ari Mustonen
           2010-2011 Samuel Pitoiset
           2011 Catalin Badea
           2006 Pekka Enberg
           2010 Stefan Kangas
           2001 H. Peter Anvin
           2016 freedroidRPG Team
           2014-2015 Alexander Forsberg
           2014 Matthias Krueger
           2014-2016 Link-Mario
           2014-2016 chmelarz
           2014 Anelise D. J.
           2014-2015 Jonatas L. Nogueira
           1995-1997, 2000-2007, 2009-2010 Ulrich Drepper
           2014-2016 fluzz
           2014 Sebastien Adam
           2016 Steven D.
           2008 Diego Billi
           2014 Xenux
           2016 Sebastiano Pistore
           2015 Lua Marchante Tello
           2015 Pedro Arana
           2014 Duong Khang Nguyen
           2016 Gino Ingras
           2015-2016 Carlos Daniel Landin Montaperto
           2011 Matei Pavaluca
License: GPL-2+

Files: lua/*
Copyright: 1994-2015 Lua.org, PUC-Rio
License: Expat

Files: m4/clang.m4
Copyright: 2013 Erik de Castro Lopo
License: Expat

Files: m4/lua.m4
Copyright: 2011 Arthur Huillet
           2011 Google Inc.
License: BSD-3-clause

Files: src/keyboard.c
Copyright: 2006-2008 Edgar Simo Serra
           2008 Arthur Huillet
License: GPL-3

Files: src/gen_savestruct.py
Copyright: 2008 Pierre Bourdon
           2009 Arthur Huillet
           2011 Samuel Degrande
License: GPL-3

Files: pkgs/freedesktop/freedroidrpg.appdata.xml
Copyright: Freedroid developers
License: CC0-1.0
 See /usr/share/common-licenses/CC0-1.0 for further information.

Files: debian/*
Copyright: 2005 Daniel Milstein <djmilstein@gmail.com>
           2006-2012 Bart Martens <bartm@knars.be>
           2016 Julien Puydt
License: GPL-2+

License: GPL-2+
 This software is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This software is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License
 along with mwrank; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 3. Neither the name of the authors nor the copyright holders may be used to
 endorse or promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER "AS IS" AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the version 3 of the GNU General Public License
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'

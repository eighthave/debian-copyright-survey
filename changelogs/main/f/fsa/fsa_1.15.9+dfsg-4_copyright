Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fsl
Upstream-Contact: mail to rbradley@mit.edu bounced with
      Final-Recipient: rfc822; rbradley@mit.edu
      Diagnostic-Code: smtp; 550 5.1.1 <rbradley@mit.edu>... User unknown
Source: http://sourceforge.net/projects/fsa/files/
Files-Excluded: */*.jar
                */*.class
                */display
                doc

Files: *
Copyright: © 2010-2014 Ariel Schwartz, Chuong Do, Robert Bradley, Jaeyoung Do, Colin Dewey, Ian Holmes, Lars Barquist
License: GPL-3
Comment: Some file headers are specifying GPL-2+ explicitly but
 according to COPYING, README and FAQ.html the license is GPL-3
 and thus we rely on the GPL-2 *or higher* option.

Files: MW/*
Copyright: 1990-2004, Condor Team, Computer Sciences Department,
                      University of Wisconsin-Madison, WI.
License: Apache-2
 According to http://www.condorproject.org the code is licensed
 Apache-2.
 .
 On Debian systems, the complete text of the Apache License
 Version 2.0 can be found at /usr/share/common-licenses/Apache-2.0.

Files: MW/src/RMComm/MW-File/chirp*
Copyright: 1990-2004 Condor Team, Computer Sciences Department,
                     University of Wisconsin-Madison, WI
License: Condor
 This source code is covered by the Condor Public License, which can
 be found in the accompanying LICENSE.TXT file, or online at
 www.condorproject.org.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 AND THE UNIVERSITY OF WISCONSIN-MADISON "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY, OF SATISFACTORY QUALITY, AND FITNESS
 FOR A PARTICULAR PURPOSE OR USE ARE DISCLAIMED. THE COPYRIGHT
 HOLDERS AND CONTRIBUTORS AND THE UNIVERSITY OF WISCONSIN-MADISON
 MAKE NO MAKE NO REPRESENTATION THAT THE SOFTWARE, MODIFICATIONS,
 ENHANCEMENTS OR DERIVATIVE WORKS THEREOF, WILL NOT INFRINGE ANY
 PATENT, COPYRIGHT, TRADEMARK, TRADE SECRET OR OTHER PROPRIETARY
 RIGHT.
Comment: The website specified above is showing Apache-2 license.

Files: src/util/regexp.cc
Copyright: 1986 by University of Toronto. Written by Henry Spencer.
License: any_purpose
 Permission is granted to anyone to use this software for any
 purpose on any computer system, and to redistribute it freely,
 subject to the following restrictions:
 .
  1. The author is not responsible for the consequences of use of
     this software, no matter how awful, even if they arise
     from defects in it.
 .
  2. The origin of this software must not be misrepresented, either
     by explicit claim or by omission.
 .
  3. Altered versions must be plainly marked as such, and must not
     be misrepresented as being the original software.

Files: debian/*
Copyright: 2014 Andreas Tille <tille@debian.org>
License: GPL-3

License: GPL-3
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.
 .
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found at /usr/share/common-licenses/GPL-3.

This is the Debian GNU/Linux r-cran-ftrading package of fTrading, a
set of function for technical trading analysis for Rmetrics, a
collection of packages for financial engineering and computational
finance. Both fTrading and Rmetrics were written and compiled primarily
by Diethelm Wuertz.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'fTrading' to
'r-cran-ftrading' to fit the pattern of CRAN (and non-CRAN) packages
for R.

Copyright (C) 1999 - 2008 Diethelm Wuertz
Copyright (C) 1999 - 2008 Rmetrics Foundation

License: GPL

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

   Package: fTrading
   Version: 260.72
   Date: 1997 - 2007
   Title: Rmetrics - Technical Trading Analysis
   Author: Diethelm Wuertz and many others, see the SOURCE file
   Depends: R (>= 2.4.0), fBasics
   Maintainer: Diethelm Wuertz and Rmetrics Core Team <Rmetrics-core@r-project.org>
   Description: Environment for teaching "Financial Engineering and Computational Finance"
   NOTE: SEVERAL PARTS ARE STILL PRELIMINARY AND MAY BE CHANGED IN THE FUTURE.
         THIS TYPICALLY INCLUDES FUNCTION AND ARGUMENT NAMES,
         AS WELL AS DEFAULTS FOR ARGUMENTS AND RETURN VALUES.
   LazyLoad: yes
   LazyData: yes
   License: GPL Version 2 or later
   URL: http://www.rmetrics.org
   Packaged: Mon Oct  8 14:08:07 2007; myself
   
and the following segment was extracted from the header of R/zzz.R [
and yes, that is the old FSF address but I am quoting here ]

   # This library is free software; you can redistribute it and/or
   # modify it under the terms of the GNU Library General Public
   # License as published by the Free Software Foundation; either
   # version 2 of the License, or (at your option) any later version.
   #
   # This library is distributed in the hope that it will be useful,
   # but WITHOUT ANY WARRANTY; without even the implied warranty of
   # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
   # GNU Library General Public License for more details.
   #
   # You should have received a copy of the GNU Library General 
   # Public License along with this library; if not, write to the 
   # Free Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
   # MA  02111-1307  USA
   
   # Copyrights (C)
   # for this R-port: 
   #   1999 - 2007, Diethelm Wuertz, GPL
   #   Diethelm Wuertz <wuertz@itp.phys.ethz.ch>
   #   info@rmetrics.org
   #   www.rmetrics.org
   # for the code accessed (or partly included) from other R-ports:
   #   see R's copyright and license files
   # for the code accessed (or partly included) from contributed R-ports
   # and other sources
   #   see Rmetrics's copyright file 

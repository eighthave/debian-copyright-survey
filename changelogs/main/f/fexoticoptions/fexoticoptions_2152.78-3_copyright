This is the Debian GNU/Linux r-cran-foptions package of
fExoticOptions, a set of functions for pricing and hedging of exotic
option financial options, and part of Rmetrics, a collection of
packages for financial engineering and computational finance. Both
fExoticOptions and Rmetrics were written and compiled primarily by
Diethelm Wuertz.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from 
	http://www.rmetrics.org
and are also available from
	http://cran.r-project.org/src/contrib/
and all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'fExoticOptions' to
'r-cran-fexoticoptions' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Copyright (C) 1999 - 2008 Diethelm Wuertz
Copyright (C) 1999 - 2008 Rmetrics Foundation

License: GPL

On a Debian GNU/Linux system, the GPL license is included in the file
/usr/share/common-licenses/GPL.

For reference, the upstream DESCRIPTION file is included below:

   Package: fExoticOptions
   Version: 260.72
   Date: 1997 - 2007
   Title: Rmetrics - Exotic Option Valuation
   Author: Diethelm Wuertz and many others, see the SOURCE file
   Depends: R (>= 2.4.0), fOptions
   Maintainer: Diethelm Wuertz and Rmetrics Core Team <Rmetrics-core@r-project.org>
   Description: Environment for teaching "Financial Engineering and Computational Finance"
   NOTE: SEVERAL PARTS ARE STILL PRELIMINARY AND MAY BE CHANGED IN THE FUTURE.
         THIS TYPICALLY INCLUDES FUNCTION AND ARGUMENT NAMES,
         AS WELL AS DEFAULTS FOR ARGUMENTS AND RETURN VALUES.
   LazyLoad: yes
   LazyData: yes
   License: GPL Version 2 or later
   URL: http://www.rmetrics.org
   Packaged: Mon Oct  8 14:08:34 2007; myself

and the following segment was extracted from the header of src/zzz.R:

   # Copyrights (C)
   # for this R-port: 
   #   1999 - 2004, Diethelm Wuertz, GPL
   #   Diethelm Wuertz <wuertz@itp.phys.ethz.ch>
   #   info@rmetrics.org
   #   www.rmetrics.org
   # for the code accessed (or partly included) from other R-ports:
   #   see R's copyright and license files
   # for the code accessed (or partly included) from contributed R-ports
   # and other sources
   #   see Rmetrics's copyright file  
   
   
   
   
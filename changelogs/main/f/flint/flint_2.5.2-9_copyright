Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: flint
Upstream-Contact: William Hart
Source: http://flintlib.org

Files: *
Copyright:
  2008 Martin Albrecht
  2013 Thomas Bachmann
  2014 Abhinav Baid
  2014 Alex J. Best
  2009 Thomas Boothby
  2007 David Cade
  2012 Thomas M. DuBuisson
  2014 Jean-Pierre Flori
  2012-2013 Andres Goens
  2013 Qingwen Guan
  2013 Mike Hansen
  2006-2015 William Hart
  2007-2008 David Harvey
  2015 Tommy Hofmann
  2007 David Howden
  2008 Richard Howell-Peak
  2014-2015 Dana Jacobsen
  2010-2015 Fredrik Johansson
  2014 Ashish Kedia
  2015 Dharak Kharod
  2012 Lina Kulakova
  2013-2014 Martin Lee
  2009 Jason Moxham
  2009-2011 Andy Novocin
  2009-2013 Sebastian Pancratz
  2006 Jason Papadopoulos
  2008 Clément Pernet
  2015 Elena Sergeicheva
  2008 Peter Shrimpton
  2015 Kushagra Singh
  2015 Anubhav Srivastava
  2005-2009 Damien Stehle
  2011 Jan Tuitman
License: GPL-2+

Files: fft.h
Copyright: 2009-2011 William Hart
License: BSD-2-clause

Files: fft/*
Copyright: 2009-2011 William Hart
License: BSD-2-clause

Files: nmod_poly/KS2_*.c nmod_poly/mul_KS?.c
Copyright:
    2007-2008 David Harvey
    2013 William Hart
License: BSD-2-clause

Files: longlong.h
Copyright:
    2009 William Hart
    2011 Fredrik Johansson
License: LGPL-2.1+

Files: clz_tab.c fmpz/fits_si.c
Copyright: 1991-2002 Free Software Foundation
License: LGPL-2.1+

Files: debian/*
Copyright: 2008 Tim Abbott
           2013-2014 Felix Salfelder
           2014 Julien Puydt
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this list of
 conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice, this list
 of conditions and the following disclaimer in the documentation and/or other materials
 provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL William Hart OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This file is free software: you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or (at
 your option) any later version.
 .
 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this file. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FooBillard++
Upstream-Contact: Holger Schäkel <foobillardplus@go4more.de>
Source: http://sourceforge.net/projects/foobillardplus
Comment:
 The non-free music files had to be removed.

Files: *
Copyright: 2001-2003, Florian Berger
           2010-2013, Holger Schäkel <foobillardplus@go4more.de>
License: GPL-2

Files: contrib/fastexp.c
Copyright: 2010, Naoaki Okazaki
License: BSD-3-clause

Files: src/getopt_long.*
Copyright: 2001, 2002, 2008 Arthur de Jong
License: LGPL-2.1+

Files: data/LinBiolinum*
Copyright: 2003-2009, Philipp H. Poll <philthelion@users.sourceforge.net>
License: GPL-2+-with-Font-exception

Files: debian/*
Copyright: 2013-2016, Markus Koschany <apo@debian.org>
License: GPL-2

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License 2 as published by
 the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 Neither the name of the original author; nor the names of any contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-2+-with-Font-exception
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation; either version 2 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc., 51
 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 As a special exception, if you create a document which uses this font, and
 embed this font or unaltered portions of this font into the document, this
 font does not by itself cause the resulting document to be covered by the
 GNU General Public License. This exception does not however invalidate any
 other reasons why the document might be covered by the GNU General Public
 License. If you modify this font, you may extend this exception to your
 version of the font, but you are not obligated to do so. If you do not wish
 to do so, delete this exception statement from your version.
 .
 On Debian systems, the complete text of the GNU General Public License can
 be found in /usr/share/common-licenses/GPL-2 file.

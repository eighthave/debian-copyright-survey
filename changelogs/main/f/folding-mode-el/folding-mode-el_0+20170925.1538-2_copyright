Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: folding-mode
Source: https://github.com/jaalto/project-emacs--folding-mode

Files: *
Copyright: 2000-2016 Jari Aalto
           1995, 1996, 1997, 1998, 1999 Jari Aalto, Anders Lindgren.
           1994 Jari Aalto
           1992, 1993 Jamie Lokier, All rights reserved.
License: GPL-2+

Files: debian/*
Copyright: 2018 Nicholas D Steeves <nsteeves@gmail.com>
License: GPL-2+

Files: debian/patches/0002-Add-folding.texi.patch
Copyright: 2000-2003 Roland Mas <lolando@debian.org>,
           2005-2014 Peter S Galbraith <psg@debian.org>
           2014 Julian Gilbey <jdg@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'

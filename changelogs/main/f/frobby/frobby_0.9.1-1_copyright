Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: frobby
Source: https://github.com/Macaulay2/frobby

Files: *
Copyright: 2007, 2009-2011 Bjarke Hammersholt Roune <bjarke.roune@gmail.com>
License: GPL-2+

Files: src/PivotEulerAlg.h src/SliceParams.h src/BigattiHilbertAlgorithm.cpp
 src/CommonParams.h src/Task.cpp src/BigattiState.h src/BigattiPivotStrategy.h
 src/ActionPrinter.h src/HashPolynomial.h src/UniHashPolynomial.cpp
 src/TaskEngine.h src/SliceLikeParams.h src/LibPrimaryDecomTest.cpp
 src/BigattiParams.cpp src/CommonParamsHelper.h src/BigattiState.cpp
 src/Deformer.cpp src/display.h src/ScarfFacade.cpp
 src/BigattiHilbertAlgorithm.h src/Arena.h src/IdealTree.cpp src/IdealWriter.h
 src/ArenaTest.cpp src/display.cpp src/LibAssociatedPrimesTest.cpp
 src/ScarfParams.cpp src/RawSquareFreeTerm.cpp src/BigattiBaseCase.cpp
 src/Matrix.cpp src/CliParams.h src/LocalArray.h src/TermPredicate.h src/Task.h
 src/PivotEulerAlg.cpp src/PolyWriter.h src/PolyWriter.cpp
 src/RawSquareFreeIdeal.h src/RawSquareFreeTerm.h src/RawSquareFreeIdeal.cpp
 src/CliParams.cpp src/HashMap.h src/SliceLikeParams.cpp src/BigattiBaseCase.h
 src/Arena.cpp src/IdealTree.h src/TermExtra.cpp src/ActionPrinter.cpp
 src/CommonParamsHelper.cpp src/IdealWriter.cpp src/Matrix.h
 src/BigattiParams.h src/IdealOrderer.h src/UniHashPolynomial.h src/Deformer.h
 src/RawSquareFreeTermTest.cpp src/IOHandlerCommon.cpp src/BigIntVector.cpp
 src/ScarfHilbertAlgorithm.h src/TermPredicate.cpp src/CommonParams.cpp
 src/LocalArray.cpp src/HashPolynomial.cpp src/MatrixTest.cpp
 src/SliceParams.cpp src/BigattiPivotStrategy.cpp src/TaskEngine.cpp
 src/BigIntVector.h src/IdealOrderer.cpp src/ScarfFacade.h
 src/RawSquareFreeIdealTest.cpp src/ScarfHilbertAlgorithm.cpp
 src/ObjectCache.cpp src/BigattiFacade.cpp src/BigattiFacade.h
 src/ObjectCache.h src/TermExtra.h src/IOHandlerCommon.h src/ScarfParams.h
Copyright: 2009-2011 University of Aarhus
License: GPL-2+

Files: src/hash_map/*
Copyright: 2001-2007 Free Software Foundation, Inc.
	   1996-1998 Silicon Graphics Computer Systems, Inc.
	   1994 Hewlett-Packard Company
License: GPL-2+ with Library exception or HPND

Files: debian/*
Copyright: 2015 Doug Torrance <dtorrance@piedmont.edu>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+ with Library exception
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
 .
 As a special exception, you may use this file as part of a free software
 library without restriction.  Specifically, if other files instantiate
 templates or use macros or inline functions from this file, or you compile
 this file and link it with other files to produce an executable, this
 file does not by itself cause the resulting executable to be covered by
 the GNU General Public License.  This exception does not however
 invalidate any other reasons why the executable file might be covered by
 the GNU General Public License.

License: HPND
 Permission to use, copy, modify, distribute and sell this software
 and its documentation for any purpose is hereby granted without fee,
 provided that the above copyright notice appear in all copies and
 that both that copyright notice and this permission notice appear
 in supporting documentation. The copyright holder makes no
 representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied warranty.

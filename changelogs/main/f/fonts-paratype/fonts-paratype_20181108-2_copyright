Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fonts-paratype
Source: https://company.paratype.com/pt-sans-pt-serif

Files: *
Copyright: 2009, ParaType Ltd. All Rights Reserved.
License: ParaType-Free-Font-License
 GRANT OF LICENSE
 ParaType Ltd grants you the right to use, copy, modify the fonts and distribute
 modified and unmodified copies of the fonts by any means, including placing
 on Web servers for free downloading, embedding in documents and Web pages,
 bundling with commercial and non commercial products, if it does not conflict
 with the conditions listed below:
 .
 - You may bundle the font with commercial software, but you may not sell the
   fonts by themselves. They are free.
 .
 - You may distribute the fonts in modified or unmodified version only together
   with this Licensing Agreement and with above copyright notice. You have no
   right to modify the text of Licensing Agreement. It can be placed in a separate
   text file or inserted into the font file, but it must be easily viewed by users.
 .
 - You may not distribute modified version of the font under the Original name
   or a combination of Original name with any other words without explicit written
   permission from ParaType.
 .
 TERMINATION & TERRITORY
 This license has no limits on time and territory, but it becomes null and void 
 if any of the above conditions are not met.
 .
 DISCLAIMER
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF 
 ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY  
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT, TRADEMARK, 
 OR OTHER RIGHT. IN NO EVENT SHALL PARATYPE BE LIABLE FOR ANY 
 CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, 
 INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, WHETHER IN 
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT 
 OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER 
 DEALINGS IN THE FONT SOFTWARE.
 .
 ParaType Ltd
 http://www.paratype.ru

Files: debian/*
Copyright: 2018 Gürkan Myczko <gurkan@phys.ethz.ch>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

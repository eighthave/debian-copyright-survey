Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fdroidserver
Upstream-Contact: F-Droid Limited <admin@f-droid.org>
Source: https://gitlab.com/fdroid/fdroidserver

Files: *
Copyright: 2010-2016 Ciaran Gultnieks
           2010-2011, David Sterry
           2011, William Theaker
           2011, Henrik Tunedal
           2011, Michael Haas
           2011, John Sullivan
           2012, Robert Martinez
           2013-2016, Daniel Martí
           2013-2016, Hans-Christoph Steiner
           2013, Frans Gifford
           2015, Boris Kraut
License: AGPL-3+

Files: fdroidserver/asynchronousfilereader/__init__.py
Copyright: 2014, Stefaan Lippens
License: MIT

Files: debian/*
Copyright: public-domain
License: public-domain

License: public-domain
 To the best of our knowledge, with the exceptions noted below or
 within the files themselves, the files that constitute PyCrypto are in
 the public domain. Most are distributed with the following notice:
 .
 The contents of this file are dedicated to the public domain. To
 the extent that dedication to the public domain is not available,
 everyone is granted a worldwide, perpetual, royalty-free,
 non-exclusive license to exercise all rights associated with the
 contents of this file for any purpose whatsoever.
 No rights are reserved.

License: AGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fdroidserver
Upstream-Contact: F-Droid Limited <admin@f-droid.org>
Source: https://gitorious.org/f-droid/fdroidserver

Files: *
Copyright: 2010-2013 Ciaran Gultnieks
           2010-2011, David Sterry
           2011, William Theaker
           2011, Henrik Tunedal
           2011, Michael Haas
           2011, John Sullivan
           2012, Robert Martinez
           2013, Daniel Martí
           2013, Frans Gifford
License: AGPL-3+

Files: docs/*
Copyright: 2010-2013 Ciaran Gultnieks
           2011, Henrik Tunedal
           2011, Michael Haas
           2011, John Sullivan
           2013, David Black
           2013, Daniel Martí
License: GFDL-1.3

Files: docs/gendocs.sh
Copyright: 2003-2010 Free Software Foundation, Inc.
License: GPL-3+

Files: debian/*
Copyright: public-domain
License: public-domain

Files: wp-fdroid/AndroidManifest.xml wp-fdroid/strings.xml
Copyright: 2006, The Android Open Source Project
License: Apache-2.0

License: GFDL-1.3
 Permission is granted to copy, distribute and/or modify this document under
 the terms of the GNU Free Documentation License, Version 1.3 or any later
 version published by the Free Software Foundation; with no Invariant
 Sections, no Front-Cover Texts, and no Back-Cover Texts.  A copy of the
 license is included in the section entitled "GNU Free Documentation License".
X-Comment: On Debian systems, the complete text of the GNU Free Documentation
 1.3 License can be found in `/usr/share/common-licenses/GFDL-1.3

License: public-domain
 To the best of our knowledge, with the exceptions noted below or
 within the files themselves, the files that constitute PyCrypto are in
 the public domain. Most are distributed with the following notice:
 .
 The contents of this file are dedicated to the public domain. To
 the extent that dedication to the public domain is not available,
 everyone is granted a worldwide, perpetual, royalty-free,
 non-exclusive license to exercise all rights associated with the
 contents of this file for any purpose whatsoever.
 No rights are reserved.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
X-Comment: On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: AGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 .
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
   http://www.apache.org/licenses/LICENSE-2.0
X-Comment: On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in `/usr/share/common-licenses/Apache-2.0.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DBD-ODBC
Upstream-Contact: Martin J. Evans <mjevans@cpan.org>
Source: https://metacpan.org/release/DBD-ODBC

Files: *
Copyright:
 2007-2014, Martin J. Evans <mjevans@cpan.org>
 1997-2002, Jeff Urlwin <jurlwin@bellatlantic.net>
 1994-1998, Tim Bunce <Tim.Bunce@pobox.com>
 1997, Thomas K. Wenrich <wenrich@ping.at>
License: Artistic or GPL-1+

Files: ConvertUTF.*
Copyright: 1991-2017, Unicode Inc.
License: Unicode
Comment:
 See also #864729.

Files: debian/*
Copyright: 2000-2004, Dirk Eddelbuettel <edd@debian.org>
 2005-2006, Carlo Segre <segre@iit.edu>
 2008, Gunnar Wolf <gwolf@debian.org>
 2009, Brian Cassidy <brian.cassidy@gmail.com>
 2009-2010, Jonathan Yu <jawnsy@cpan.org>
 2010, Nicholas Bamber <nicholas@periapt.co.uk>
 2006-2016, gregor herrmann <gregoa@debian.org>
 2012-2014, Xavier Guimard <yadd@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Unicode
 Copyright © 1991-2017 Unicode, Inc. All rights reserved.
 Distributed under the Terms of Use in https://www.unicode.org/copyright.html.
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Unicode data files and any associated documentation
 (the "Data Files") or Unicode software and any associated documentation
 (the "Software") to deal in the Data Files or Software
 without restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, and/or sell copies of
 the Data Files or Software, and to permit persons to whom the Data Files
 or Software are furnished to do so, provided that either
 (a) this copyright and permission notice appear with all copies
 of the Data Files or Software, or
 (b) this copyright and permission notice appear in associated
 Documentation.
 .
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF
 ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS
 NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL
 DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THE DATA FILES OR SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in these Data Files or Software without prior
 written authorization of the copyright holder.

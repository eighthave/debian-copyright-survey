Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Data-ICal
Upstream-Contact: Alex Vandiver <alexmv+pause@mit.edu>
Source: https://metacpan.org/release/Data-ICal
Files-Excluded: doc/rfc*
Comment:
  Upstream sources are repackaged to remove a file which cannot be
  redistributed:
    + doc/rfc2445.txt

Files: *
Copyright: 2005-2020, Best Practical Solutions
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2014, Adam Kennedy <adamk@cpan.org>
 2002-2014, Audrey Tang <autrijus@autrijus.org>
 2002-2014, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2007, Peter Makholm <peter@makholm.net>
 2009, Jonathan Yu <jawnsy@cpan.org>
 2009, Antonio Radici <antonio@dyne.org>
 2011-2020, gregor herrmann <gregoa@debian.org>
 2013-2015, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

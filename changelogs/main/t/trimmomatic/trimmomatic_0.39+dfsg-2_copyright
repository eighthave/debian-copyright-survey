Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Trimmomatic
Upstream-Contact: Federico Giorgi <federico.giorgi@gmail.com>,
                  Anthony Bolger <bolger@mpimp-golm.mpg.de>
Source: http://www.usadellab.org/cms/index.php?page=trimmomatic
Files-Excluded:
    lib/*.jar
    old

Files: *
Copyright: © 2009-2011 Bjoern Usadel <usadel@mpimp-golm.mpg.de>,
                       Anthony Bolger <Bolger@mpimp-golm.mpg.de>
           © 2011-2013 Federico Giorgi <federico.giorgi@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: © 2011, Andreas Tille <tille@debian.org>
License: GPL-3+

Files: debian/tests/test_data/*
Copyright: © 2016, Fred Hutchinson Cancer Research Center
License: INSDC
Comment: 275-nt paired-end reads of HIV envelope PCR amplicons
 ENA ID: SRX1814913, available at http://www.ebi.ac.uk/ena

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in /usr/share/common-licenses/GPL-3.

License: INSDC
  INSDC License - as described in ENA site - August 7, 2017
  .
  The INSDC will not attach statements to records that restrict access to the
  data, limit the use of the information in these records, or prohibit certain
  types of publications based on these records. Specifically, no use
  restrictions or licensing requirements will be included in any sequence data
  records, and no restrictions or licensing fees will be placed on the
  redistribution or use of the database by any party.
  .
  License's text is available on ENA database site (http://www.ebi.ac.uk/ena),
  on page named "Standards and policies"
  URL: http://www.ebi.ac.uk/ena/standards-and-policies

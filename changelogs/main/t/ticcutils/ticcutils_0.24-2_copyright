Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ticcutils
Upstream-Contact: Ko van der Sloot <ko.vandersloot@let.ru.nl>
Source: https://github.com/LanguageMachines/ticcutils

Files: *
Copyright: Copyright © 2006 - 2020, Centre for Language and Speech
 Technology, Radboud University, Nijmegen, The Netherlands)
License: GPL-3+

Files: debian/*
Copyright: Copyright © 2018-2020 Maarten van Gompel
License: GPL-3+

Files: m4/*
Copyright: Copyright (C) 2004-2005, 2007-2009, 2011-2015 Free Software Foundation, Inc.
License: permissive

Files: m4/ac_osx_pkg.m4
Copyright: Copyright © 2018 Ko van der Sloot
License: GPL-3+

Files: m4/ax_check_zlib.m4
Copyright: Copyright (c) 2008 Loic Dachary <loic@senga.org>
  Copyright (c) 2010 Bastien Chevreux <bach@chevreux.org>
License: GPL-3+

Files: m4/ax_lib_socket_nsl.m4
Copyright: Copyright (c) 2008 Russ Allbery <rra@stanford.edu>
 Copyright (c) 2008 Stepan Kasal <kasal@ucw.cz>
 Copyright (c) 2008 Warren Young <warren@etr-usa.com>
License: permissive

Files: m4/ax_type_socklen_t.m4
Copyright: Copyright (c) 2008 Lars Brinkhoff <lars@nocrew.org>
License: GPL-3+

Files: m4/pkg.m4
Copyright: Copyright © 2004 Scott James Remnant <scott@netsplit.com>
License: GPL-3+

Files: include/ticcutils/gzstream.h
Copyright: Copyright (C) 2001  Deepak Bandyopadhyay, Lutz Kettner
License: LGPL-2.1+

Files: include/ticcutils/bz2stream.h
Copyright: Copyright (C) 2002 Aaron Isotton
License: GPL-3+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

License: GPL-3+
 libticcutils is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 libticcutils is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: permissive
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty

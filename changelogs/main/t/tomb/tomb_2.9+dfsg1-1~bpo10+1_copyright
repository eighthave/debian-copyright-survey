Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tomb
Upstream-Contact: Dyne.org Foundation <info@dyne.org>
Source: https://github.com/dyne/Tomb
Files-Excluded:
 doc/New_methods_in_HD_encryption.pdf
 doc/TKS1-draft.pdf
 doc/bertini_thesis.pdf
 doc/Luks_on_disk_format.pdf
 doc/tomb_manpage.pdf
 doc/LinuxHDEncSettings.txt

Files: *
Copyright: 2007-2021 Dyne.org Foundation <info@dyne.org>
           2007-2021 Denis Roio <jaromil@dyne.org>
           2015      Gianluca Montecchi <gian@grys.it>
           2015-2016 Parazyd <parazyd@dyne.org>
License: GPL-3+

Files: doc/literate/shocco
Copyright: 2010      Ryan Tomayko <http://tomayko.com/about>
License: Expat

Files: extras/docker/*
Copyright: 2019      Greg Tczap (aka Greg Tzar) <https://gregtczap.com>
License: Expat

Files: extras/test/aggregate-results
       extras/test/Makefile
       extras/test/sharness
Copyright: 2005-2012 Git project <https://git-scm.com>
           2005-2012 Junio C Hamano <gitster@pobox.com>
           2011-2012 Mathias Lafeldt <mathias.lafeldt@gmail.com>
License: GPL-2+

Files: extras/tomber/tomber/*
Copyright: 2014      Federico Cardoso <reiven@gmail.com>
License: BSD-3-clause

Files: debian/*
Copyright: 2017-2018 ChangZhuo Chen (陳昌倬) <czchen@debian.org>
           2018      Andreas Henriksson <andreas@fatal.se>
           2018      Raphaël Hertzog <hertzog@debian.org>
           2018-2019 SZ Lin (林上智) <szlin@debian.org>
           2019-2020 Samuel Henrique <samueloph@debian.org>
           2019-2021 Sven Geuer <debmaint@g-e-u-e-r.de>
License: GPL-3+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-3-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of copyright holders nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

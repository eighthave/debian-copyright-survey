Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tDOM
Source: http://tdom.org/downloads/
Comment: This package was adopted by Hector Romojaro <hector.romojaro@gmail.com>
 , Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>, Avni Khatri
 <avni321@gmail.com>, and Carl Blesius <carl@blesius.org> on Sun, 16
 Mar 2008 12:13:12 -0400.
 .
 This package was originally debianized by David N. Welton <davidw@debian.org>
 on Mon, 26 Jan 2004 19:38:53 +0100.

Files: *
Copyright: 2003-2008 Rolf Ade <rolf@pointsman.de>
           2002      Rolf Ade <rolf@pointsman.de> ,
                     Zoran Vasiljevic <zv@archiware.com>
           1999-2001 Jochen C. Loewer* <loewerj@hotmail.com> ,
                     Rolf Ade <rolf@pointsman.de> ,
                     Zoran Vasiljevic <zv@archiware.com>
                     (*) Author of the original tDOM code
License: MPL-2.0

Files: debian/*
Copyright: 2008, Hector Romojaro <hector.romojaro@gmail.com>,
                 Stefan Sobernig <stefan.sobernig@wu-wien.ac.at>,
                 Avni Khatri <avni321@gmail.com>,
                 Carl Blesius <carl@blesius.org>
           2004  David N. Welton <davidw@debian.org>
License: GPL-2+

Files: generic/domhtml.c
Copyright: 1998-1999 D. Richard Hipp <drh@acm.org>
           2000      Jochen Loewer <loewerj@hotmail.com>
License: MPL-2.0

Files: generic/xmlsimple.c
Copyright: 1998-1999 Jochen Loewer <loewerj@hotmail.com>
License: MPL-2.0

License: MPL-2.0
 The complete text of the Mozilla Public License 2.0 can be found in
 the file `/usr/share/common-licenses/MPL-2.0'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: expat/*
Copyright: 1998-2000 Thai Open Source Software Center Ltd and Clark Cooper
           2001-2017 Expat maintainers
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


COPYRIGHT
---------

This package was originally an `alien', debianized by the alien
program by converting the GPLed RPMS Package `tmpwatch-1.2'
on Thu, 13 Nov 1997 02:16:28 -0800.

Authors: Erik Troan, Karl M. Hegbloom, Paul Slootman
Copyright 1997 Erik Troan
Copyright 1997, 1998 Karl M. Hegbloom
Copyright 1998-2019 Paul Slootman

tmpreaper is released under the terms of the GNU General Public License
version 2, found in the file:

  /usr/share/common-licenses/GPL-2

on Debian systems, or at 

  http://www.gnu.org/licenses/old-licenses/gpl-2.0.html



 Please read the changelog.

 The original `tmpwatch-1.2' was downloaded from ftp.redhat.com by
 karlheg@inetarena.com.
 (You can find the current repository at https://fedorahosted.org/tmpwatch/)

 Thank You, Erik Troan.  This program was sorely needed.  We always
 appreciate the things you provide for the Linux community.  I could
 not have even begun to write a program like this one from scratch.
 Without the initial program to build on, it would not have been
 possible.  I've learned a lot from this experience.

karlheg: Sun, Dec 7, 1997:
 I am renaming this program from `tmpwatch' to `tmpreaper' to split
 away from RedHat, who released a `tmpwatch-1.4' with RedHat v5.0 that
 had zero of the patches I sent for `1.2-1.4'.  They made one small
 patch:

--- tmpwatch-1.2.c	Mon Nov 17 21:16:37 1997
+++ tmpwatch-1.4/tmpwatch.c	Thu Nov  6 13:58:34 1997
@@ -157,7 +157,7 @@
 		message(LOG_VERBOSE, "removing directory %s\n", ent->d_name);

 		if (!(flags & FLAGS_TEST)) {
-		    if (!(flags & FLAGS_ALLFILES)) {
+		    if (flags & FLAGS_ALLFILES) {
 			if (rmdir(ent->d_name)) {
 			    message(LOG_ERROR, "failed to rmdir %s: %s\n",
 					dirname, ent->d_name);

 This does not reflect any of the changes I made and submitted to
 them, including the bug that prompted me to begin working on
 `tmpwatch' to begin with.  The `--test' option was broken.  The
 getopt loop fills a variable with some flags to be passed to
 cleanDirectory, but when that function is called for the first time,
 he passes a 0 as the flags argument.

 In main(), it goes:
-	    cleanupDirectory(argv[optind], killTime, 0);
+	    cleanupDirectory (argv[optind], killTime, flags);

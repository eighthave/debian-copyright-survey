Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Packaged-By: Stefano Zacchiroli <zack@debian.org>
Packaged-Date: Sun, 04 May 2008 14:49:32 +0200
Source: https://forge.ocamlcore.org/projects/type-conv

Files: *
Copyright: 2007-2008, Markus Mottl <markus.mottl@gmail.com>
           2007-2008, Jane Street Holding, LLC <opensource@janestcapital.com>
           2004-2005, Martin Sandin <msandin@hotmail.com>
License: BSD-3 and LGPL-2.1-exception
 As a special exception to the GNU Lesser General Public License, you
 may link, statically or dynamically, a "work that uses the Library"
 with a publicly distributed version of the Library to produce an
 executable file containing portions of the Library, and distribute
 that executable file under terms of your choice, without any of the
 additional requirements listed in clause 6 of the GNU Lesser General
 Public License.  By "a publicly distributed version of the Library",
 we mean either the unmodified Library as distributed by the authors,
 or a modified version of the Library that is distributed under the
 conditions defined in clause 3 of the GNU Lesser General Public
 License.  This exception does not however invalidate any other
 reasons why the executable file might be covered by the GNU Lesser
 General Public License.
 .
 The Library is distributed under the terms of the GNU Lesser General
 Public License version 2.1, with the special exception to it reported
 above.
 .
 The full text of the GNU Lessere General Public License version 2.1
 can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 Previous versions of this Library, formerly known as "Tywith", were
 released by Martin Sandin terms of the 3-clauses BSD.  The Library is
 also released under such a license:
 .
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
   3. The name of the author may not be used to endorse or promote
      products derived from this software without specific prior
      written permission.
   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
   OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
   GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2008, Stefano Zacchiroli <zack@debian.org>
License: GPL-3+
 The Debian packaging is copyright Stefano Zacchiroli, and licensed
 under the GNU General Public License, see
 `/usr/share/common-licenses/GPL-3' for the full text.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Twinkle
Source: https://github.com/LubosD/twinkle/releases
 The orig tarball excludes the gsm library, which has been packaged for Debian.
Files-Excluded: src/audio/gsm/*

Files: *
Copyright: 2005-2009 Michel de Boer <michel@twinklephone.com>
           2015 Luboš Doležel <lubos@dolezel.info>
           2015 Michal Kubecek <mkubecek@suse.cz>
License: GPL-2+

Files: debian/*
Copyright: 2005-2009 Mark Purcell <msp@debian.org>
           2013-2014 Clint Adams <clint@debian.org>
           2015 Peter Colberg <peter@colberg.org>
           2016 Rolf Leggewie <foss@rolf.leggewie.org>
License: GPL-2+

Files: src/audio/g* src/audio/README_G711
Copyright: Sun Microsystems, Inc.
License: public-domain
 This source code is released by Sun Microsystems, Inc. to the public domain.
 Please give your acknowledgement in product literature if this code is used
 in your product implementation.

Files: src/sockets/dnssrv.cpp
Copyright: 2002 Rick van Rein
           2005 Michel de Boer
License: GPL-2+

Files: src/stun/stun.* src/stun/udp.*
Copyright: 2000 Vovida Networks, Inc.
License: Vovida

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'.

License: Vovida
 The Vovida Software License, Version 1.0
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. The names "VOCAL", "Vovida Open Communication Application Library",
    and "Vovida Open Communication Application Library (VOCAL)" must
    not be used to endorse or promote products derived from this
    software without prior written permission. For written
    permission, please contact vocal@vovida.org.
 .
 4. Products derived from this software may not be called "VOCAL", nor
    may "VOCAL" appear in their name, without prior written
    permission of Vovida Networks, Inc.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
 NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL VOVIDA
 NETWORKS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT DAMAGES
 IN EXCESS OF $1,000, NOR FOR ANY INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.

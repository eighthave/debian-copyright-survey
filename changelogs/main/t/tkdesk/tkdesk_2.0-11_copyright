Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: TkDesk
Upstream-Contact: Christian Bolik <Christian.Bolik@mainz.netsurf.de>
Source: https://sourceforge.net/projects/tkdesk/files/TkDesk/

Files: *
Copyright: 1996 Christian Bolik
License: GPL-2+

Files: blt/*
Copyright: 1993-1996 AT&T Bell Laboratories.
License: ATT

Files: tkAppInit.c
Copyright: 1993 The Regents of the University of California
           1994 Sun Microsystems, Inc.
License: TCLTK

Files: debian/*
Copyright: 1998-2007 Daniel Martin <fizbin@debian.org>
           2017 Reiner Herrmann <reiner@reiner-h.de>
License: GPL-2+

License: ATT
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies and that both that the copyright notice and warranty
 disclaimer appear in supporting documentation, and that the
 names of AT&T Bell Laboratories any of their entities not be used
 in advertising or publicity pertaining to distribution of the
 software without specific, written prior permission.
 .
 AT&T disclaims all warranties with regard to this software, including
 all implied warranties of merchantability and fitness.  In no event
 shall AT&T be liable for any special, indirect or consequential
 damages or any damages whatsoever resulting from loss of use, data
 or profits, whether in an action of contract, negligence or other
 tortuous action, arising out of or in connection with the use or
 performance of this software.

License: TCLTK
 This software is copyrighted by the Regents of the University of
 California, Sun Microsystems, Inc., and other parties.  The following
 terms apply to all files associated with the software unless explicitly
 disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 RESTRICTED RIGHTS: Use, duplication or disclosure by the government
 is subject to the restrictions as set forth in subparagraph (c) (1) (ii)
 of the Rights in Technical Data and Computer Software Clause as DFARS
 252.227-7013 and FAR 52.227-19.

License: GPL-2+
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

Format: http://dep.debian.net/deps/dep5
Upstream-Name: tulip
Source: http://tulip-software.org/

Files: *
Copyright: 2001-2011 David Auber <auber@tulip-software.org>
License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
Comments: Contributors include: Sophie Bardet, Gerald Gainant, Chrys
 Myers, Bertrand Mathieu, Sebastien Grivet, David Duke, Loïc Jézéquel,
 Daniel Archambault, Maylis Delest, Jean Philippe Domenger, Romain
 Bourqui, Patrick Mary.

Files: plugins/import/facebook/facebook.py
Copyright: 2010 Facebook
License: Apache-2.0
 On Debian systems, the full text of the Apache
 License version 2.0 can be found in the file
 `/usr/share/common-licenses/Apache-2.0`.

Files: FindSIP.*
Copyright: 2007, Simon Edwards <simon@simonzone.com>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: doc/common/_static/customized.css
Copyright: 2007-2013 the Sphinx team
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: thirdparty/OGDF/*
Copyright: 2005-2010 The OGDF Team <info@ogdf.net>
License: GPL-2-or-3-with-additional-permissions
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 Version 2 or 3 as published by the Free Software Foundation
 and appearing in the files LICENSE_GPL_v2.txt and
 LICENSE_GPL_v3.txt included in the packaging of this file.
 .
 In addition, as a special exception, you have permission to link
 this software with
 .
   - the libraries of the COIN-OR Osi project (see
     http://www.coin-or.org/projects/Osi.xml);
   - all libraries required by Osi;
   - and all LP-solver libraries directly supported by the
     COIN-OR Osi project,
 .
 and distribute executables, as long as you follow the requirements
 of the GNU General Public License in regard to all of the software
 in the executable aside from these third-party libraries.
 .
 See also: http://www.ogdf.net/license.html

Files: thirdparty/gzstream/*
Copyright: 2001-2003 Deepak Bandyopadhyay
           2001-2003 Lutz Kettner
License: LGPL-2.1+
  On Debian systems, the complete text of the GNU Library
  General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: thirdparty/libqhull/*
Copyright: 1993-2012 The Geometry Center
License: permissive
                     Qhull, Copyright (c) 1993-2012
 .
                             C.B. Barber
                            Arlington, MA 
 .
                                and
 .
        The National Science and Technology Research Center for
         Computation and Visualization of Geometric Structures
                         (The Geometry Center)
                        University of Minnesota
 .
                        email: qhull@qhull.org
 .
 This software includes Qhull from C.B. Barber and The Geometry Center.  
 Qhull is copyrighted as noted above.  Qhull is free software and may 
 be obtained via http from www.qhull.org.  It may be freely copied, modified, 
 and redistributed under the following conditions:
 .
 1. All copyright notices must remain intact in all files.
 .
 2. A copy of this text file must be distributed along with any copies 
    of Qhull that you redistribute; this includes copies that you have 
    modified, or copies of programs or other software products that 
    include Qhull.
 .
 3. If you modify Qhull, you must include a notice giving the
    name of the person performing the modification, the date of
    modification, and the reason for such modification.
 .
 4. When distributing modified versions of Qhull, or other software 
    products that include Qhull, you must provide notice that the original 
    source code may be obtained as noted above.
 .
 5. There is no warranty or other guarantee of fitness for Qhull, it is 
    provided solely "as is".  Bug reports or fixes may be sent to 
    qhull_bug@qhull.org; the authors may or may not act on them as 
    they desire.

Files: thirdparty/quazip/*
Copyright: 2005-2011 Sergey A. Tachenov
License: LGPL-2.1+
  On Debian systems, the complete text of the GNU Library
  General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: thirdparty/qxt/*
Copyright: 2007 Qxt Foundation
           2007-2008 Ariya Hidayat
License: GPL-2+ or IBM-CPL
 This library is free software; you can redistribute it and/or modify it
 under the terms of the Common Public License, version 1.0, as published by IBM 
 or under  the terms of the  GNU Lesser General Public License, version 2.1, 
 as published by the Free Software Foundation
 .
 This file is provided "AS IS", without WARRANTIES OR CONDITIONS OF ANY
 KIND, EITHER EXPRESS OR IMPLIED INCLUDING, WITHOUT LIMITATION, ANY
 WARRANTIES OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR
 FITNESS FOR A PARTICULAR PURPOSE.
 .
 You should have received a copy of the CPL along with this file.
 See the LICENSE file and the cpl1.0.txt file included with the source
 distribution for more information. If you did not receive a copy of the
 license, contact the Qxt Foundation.
 .
 You should have received a copy of the LGPL along with this file.
 See the LICENSE file and the lgpl-2.1.txt file included with the source
 distribution for more information. If you did not receive a copy of the
 license, contact the Qxt Foundation.
 .
 Parts of Qxt depend on Qt 4 and/or other libraries that have their own
 licenses. Qxt is independent of these licenses; however, use of these other
 libraries is subject to their respective license agreements.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
 .
 The full text of the CPL 1.0 license is available as cpl1.0.txt in this
 directory.

Files: thirdparty/yajl/*
Copyright: 2007-2011, Lloyd Hilaiel <lloyd@hilaiel.com>
License: permissive
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

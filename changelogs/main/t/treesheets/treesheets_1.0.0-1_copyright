Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: treesheets
Source: http://strlen.com/treesheets/

Files: *
Copyright: 2008-2015 Wouter van Oortmerssen <aardappel@gmail.com>
           2013-2015 Treesheets contributors
License: ZLIB

Files: TS/images/nuvola/*
Copyright: 2003-2004 David Vignoni <david@icon-king.com>
License: LGPL-2.1 with Nuvola exception

Files: debian/*
Copyright: 2015 Ximin Luo <infinity0@debian.org>
License: ZLIB

License: ZLIB
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
    1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
    2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
    3. This notice may not be removed or altered from any source
    distribution.

License: LGPL-2.1 with Nuvola exception
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation,
 version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 In addition, note the following exception:
 .
 Source code, for this library means:
  - raster png image*
 .
 The LGPL in some sections obliges you to make the files carry
 notices. With images this is in some cases impossible or hardly useful.
 With this library a notice is placed at a prominent place in the directory
 containing the elements. You may follow this practice.
 The exception in section 6 of the GNU Lesser General Public License covers
 the use of elements of this art library in a GUI.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

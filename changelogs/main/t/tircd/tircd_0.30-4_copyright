Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/drags/tircd
Upstream-Contact: Tim Sogard <tircd@timsogard.com>
Upstream-Name: tircd

Files: *
Copyright: 2009 Chris Nelson <tircd@crazybrain.org>
           2010-2011 Tim Sogard <tircd@timsogard.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009-2010 Decklin Foster <decklin@red-bean.com>
           2012-2013 Michael Stapelberg <stapelberg@debian.org>
           2017      Mattia Rizzolo <mattia@debian.org>
           2017      Axel Beckert <abe@debian.org>
License: GPL-1+
Comment: It is assumed that without further specification "The Debian
 packaging is licensed under the GPL" means "licensed under any
 version of the GPL", i.e. version 1 or later.

Files: debian/examples/tweetlen.pl
Copyright: 2009 Decklin Foster <decklin@red-bean.com>
License: ISC

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for
 any purpose with or without fee is hereby granted, provided that the
 above copyright notice and this permission notice appear in all
 copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.

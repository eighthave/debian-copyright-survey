Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tpm2-pkcs11
Source:https://github.com/tpm2-software/tpm2-pkcs11

Files: *
Copyright: tpm2-software
License: BSD-2-Clause

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

Files: src/lib/twist.c
         src/lib/twist.h
         test/unit/test_twist.c
Copyright: William Roberts
License: BSD-2-Clause

Files: debian/*
Copyright: 2020, Alvin Chen <sonoma001@gmail.com>
License: BSD-2-Clause

Files: src/pkcs11.h
Copyright: 2006, 2007 g10 Code GmbH
           2006, Andreas Jellinghaus
           2017, Red Hat, Inc.
License: FSF

Files: m4/ax_ac_append_to_file.m4
        m4/ax_ac_print_to_file.m4
Copyright: Copyright (c) 2009 Allan Caffee <allan.caffee@gmail.com>
License: FSF

Files: m4/ax_add_am_macro_static.m4
        m4/ax_am_macros_static.m4
Copyright: Copyright (c) 2009 Allan Caffee <allan.caffee@gmail.com>
            Copyright (c) 2009 Tom Howard <tomhoward@users.sf.net>
License: FSF

Files: m4/ax_add_fortify_source.m4
Copyright: Copyright (c) 2017 David Seifert <soap@gentoo.org>
License: FSF

Files: m4/ax_check_enable_debug.m4
Copyright: Copyright (c) 2011 Rhys Ulerich <rhys.ulerich@gmail.com>
            Copyright (c) 2014, 2015 Philip Withnall <philip@tecnocode.co.uk>
License: FSF

Files: m4/ax_file_escapes.m4
Copyright: Copyright (c) 2009 Tom Howard <tomhoward@users.sf.net>
License: FSF

Files: m4/ax_is_release.m4
Copyright: Copyright (c) 2015 Philip Withnall <philip@tecnocode.co.uk>
            Copyright (c) 2016 Collabora Ltd.
License: FSF

Files: m4/ax_try_compile_java.m4
Copyright: Copyright (c) 2008 Devin Weaver <ktohg@tritarget.com>
License: FSF

Files: m4/ld-version-script.m4
Copyright: Simon Josefsson
License: FSF

Files: m4/lt~obsolete.m4
        m4/ltversion.m4
Copyright: Scott James Remnant, 2004
License: FSF

Files: m4/ltoptions.m4
        m4/ltsugar.m4
Copyright: Gary V. Vaughan, 2004
License: FSF

License: FSF
 This file is free software; as a special exception the author gives
 unlimited permission to copy and/or distribute it, with or without
 modifications, as long as this notice is preserved.
 .
 This file is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY, to the extent permitted by law; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE. */

Files: m4/ax_code_coverage.m4
Copyright:  Copyright (c) 2012, 2016 Philip Withnall
            Copyright (c) 2012 Xan Lopez
            Copyright (c) 2012 Christian Persch
            Copyright (c) 2012 Paolo Borelli
            Copyright (c) 2012 Dan Winship
            Copyright (c) 2015 Bastien ROUCARIES
License: LGPL-2.1+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser 
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Files: m4/ax_prog_javac.m4
        m4/ax_check_class.m4
        m4/ax_prog_javac_works.m4
        m4/ax_prog_java.m4
        m4/ax_prog_java_works.m4
Copyright: Copyright (c) 2008 Stephane Bortzmeyer <bortzmeyer@pasteur.fr>	
License: GPL-2.0+-with-autoconf-exception

Files: m4/ax_normalize_path.m4
Copyright: Copyright (c) 2008 Alexandre Duret-Lutz <adl@gnu.org>
License: GPL-2.0+-with-autoconf-exception

License: GPL-2.0+-with-autoconf-exception
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

Files: m4/ax_pthread.m4
Copyright: Copyright (c) 2011 Daniel Richard G. <skunk@iSKUNK.ORG>		
            Copyright (c) 2008 Steven G. Johnson <stevenj@alum.mit.edu>
License: GPL-3.0+-with-autoconf-exception

Files: m4/ax_check_link_flag.m4
        m4/ax_check_compile_flag.m4
Copyright: Copyright (c) 2011 Maarten Bosmans <mkbosmans@gmail.com>		
           Copyright (c) 2008 Guido U. Draheim <guidod@gmx.de>
License: GPL-3.0+-with-autoconf-exception

License: GPL-3.0+-with-autoconf-exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

Files: install-sh
Copyright: Copyright (C) 1994 X Consortium
License: X11

License: X11
 Copyright (C) 1994 X Consortium
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X  # Consor-
 tium.

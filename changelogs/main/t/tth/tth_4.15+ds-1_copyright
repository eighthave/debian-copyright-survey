Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tth
Upstream-Contact: Ian Hutchinson <ihutch@mit.edu>
Source: https://sourceforge.net/projects/tth
Comment:
 The upstream source tarball is repacked to reduce drastically its weight
 by cleaning up and by wiping out material not meant for UN*X source.
Files-Excluded:
 tth*.tar.gz.asc
 tthfunc
 tthgif
 TtMdir
 ttmC
 tth-gui
 *.bat
 *.lnk
 *.tlg
 *.log
 *.ilg
 *.idx
 *.toc
 *.aux
 *.dvi
 *.bak
 *.res
 *.exe
 *~
 manual/split/*.html
 manual/ttm_manual.tex
 manual/mozilla.tex
 ttmL/ttm.gif
 ttmL/ttm_icon.gif
 ttmL/ttm_mini.gif
 ttmL/ttm
 ttmL/ps2gif
 ttmL/ps2png
 ttmL/latex2gif
 ttmL/ttm_manual.html
 ttmL/ttm.kdelnk
 ttmL/uninstall
 ttmL/ttminstall
 tthgold/golddesc
 tthgold/tthsplit.c
 tthgold/tthsplit
 tthgold/tthrfcat
 tthgold/latex2gif
 tthgold/tth
 tthgold/tth.c
 tthgold/tth.1
 tthgold/tth.gif
 tthgold/tth_icon.bmp
 tthgold/tthntbib.sty
 tthgold/tth_man.html
 tthgold/gold_man.html
 tthgold/tth_man.tex
 tools/ps2gif_transparent
 tools/structure
 tools/numbering
 tools/choice.c
 latex2gif
 ps2gif
 transfer
 tth16.png
 tth32.png
 tth_screen.gif
 tars

Files: *
Copyright: 1997-2018 Ian Hutchinson <ihutch@mit.edu>
License: GPL-2+

Files: debian/*
Copyright:
 2012-2018 Jerome Benoit <calculus@rezozer.net>
 2001-2011 Ian Maclaine-cross <iml@debian.org>
License: GPL-2+
Comment:
 This package was originally assembled by Ian Maclaine-cross <iml@debian.org>.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tiled
Upstream-Contact: Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
Source: http://www.mapeditor.org/

Files: *
Copyright: 2009 Tiled (Qt) developers
License: GPL-2+

Files: translations/*.ts
Copyright: 2008-2009 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
License: GPL-2+

Files: src/libtiled/* src/tmxviewer/* util/java/tmxviewer-java/*
Copyright: 2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
License: BSD-2-clause

Files: src/libtiled/map.h src/libtiled/map.cpp
Copyright: 2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2008 Roderic Morris <roderic@ccs.neu.edu>
           2010 Andrew G. Crowell <overkill9999@gmail.com>
License: BSD-2-clause

Files: src/libtiled/objectgroup.h src/libtiled/objectgroup.cpp
Copyright: 2008 Roderic Morris <roderic@ccs.neu.edu>
           2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2009-2010 Jeff Bland <jksb@member.fsf.org>
License: BSD-2-clause

Files: src/libtiled/tile.h
Copyright: 2008-2009 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2009 Edward Hutchins <eah1@yahoo.com>
License: BSD-2-clause

Files: src/libtiled/mapobject.cpp src/libtiled/mapobject.h
Copyright: 2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2008 Roderic Morris <roderic@ccs.neu.edu>
License: BSD-2-clause

Files: src/libtiled/tilelayer.cpp src/libtiled/layer.cpp src/libtiled/tilelayer.h src/libtiled/layer.h
Copyright: 2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2009 Jeff Bland <jksb@member.fsf.org>
License: BSD-2-clause

Files: src/libtiled/mapwriter.cpp src/libtiled/mapreader.cpp src/libtiled/mapwriter.h
Copyright: 2008-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2010 Jeff Bland <jksb@member.fsf.org>
           2010 Dennis Honeyman <arcticuno@gmail.com>
License: BSD-2-clause

Files: util/java/libtiled-java/*
Copyright: 2004-2010 Thorbjørn Lindeijer <thorbjorn@lindeijer.nl>
           2004-2006 Adam Turk <aturk@biggeruniverse.com>
License: LGPL-2.1+

Files: debian/*
Copyright: 2010 Ying-Chun Liu (PaulLiu) <paulliu@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   1. Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
 .
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library;  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: twine
Source: https://github.com/pypa/twine

Files: *
Copyright:
 Donald Stufft <donald@stufft.io> (https://caremad.io/)
 Jannis Leidel <jannis@leidel.info>
 Ralf Schmitt <ralf@systemexit.de>
 Ian Cordasco <graffatcolmingov@gmail.com>
 Marc Abramowitz <msabramo@gmail.com> (http://marc-abramowitz.com/)
 Tom Myers <tom.stephen.myers@gmail.com>
 Rodrigue Cloutier <rodcloutier@gmail.com>
 Tyrel Souza <tyrelsouza@gmail.com> (https://tyrelsouza.com)
 Adam Talsma <adam@talsma.ca>
 Jens Diemer <github@jensdiemer.de> (http://jensdiemer.de/)
 Andrew Watts <andrewwatts@gmail.com>
 Anna Martelli Ravenscroft <annaraven@gmail.com>
 Sumana Harihareswara <sh@changeset.nyc>
 Dustin Ingram <di@di.codes> (https://di.codes)
 Jesse Jarzynka <jesse@jessejoe.com> (https://www.jessejoe.com/)
 László Kiss Kollár <kiss.kollar.laszlo@gmail.com>
 Frances Hocutt <frances.hocutt@gmail.com>
 Tathagata Dasgupta <tathagatadg@gmail.com>
 Wasim Thabraze <wasim@thabraze.me>
 Varun Kamath <varunkamath18@gmail.com>
 Brian Rutledge <bhrutledge@gmail.com>
 Peter Stensmyr <peter.stensmyr@gmail.com> (http://www.peterstensmyr.com)
 Felipe Mulinari Rocha Campos <felipecampos@google.com>
 Devesh Kumar Singh <deveshkusingh@gmail.com>
 Yesha Maggi <yesha.maggic@gmail.com>
 Cyril de Catheu <cdecatheu@gmail.com> (https://catheu.tech/)
 Thomas Miedema <thomasmiedema@gmail.com>
 Hugo van Kemenade (https://github.com/hugovk)
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License, Version 2.0 can be
 found in the file `/usr/share/common-licenses/Apache-2.0'.

Files: debian/*
Copyright: 2014-2015, Zygmunt Krynicki <zygmunt.krynicki@canonical.com>
           2019-2020, Stefano Rivera <stefanor@debian.org>
License: GPL-3
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3,
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian-based systems the full text of the GPL, version 3, can be found at
 /usr/share/common-licenses/GPL-3.

This package was debianized by Kenshi Muto <kmuto@debian.org> on
Sat,  3 Dec 2005 11:22:32 +0900.

Copyright Holder: Kenshi Muto <kmuto@debian.org> and Hidetaka Iwai <tyuyu@debian.or.jp>

License:
  po2ul.rb, stripttf, and makettf are distributed under GNU GPL2.
  See /usr/share/common-licenses/GPL.

  Licenses of each TTF files are same as original.

ja.ttf (ttf-vlgothic)
           (c) 1990-2003 Wada Laboratory, the University of Tokyo
           (c) 2003-2004 Electronic Font Open Laboratory (/efont/)
           (c) 2003-2006 M+ FONTS PROJECT
           (c) 2006 Daisuke SUZUKI <daisuke@vinelinux.org>
           (c) 2006 Project Vine <Vine@vinelinux.org>

Upstream author's license information (In Japanese, UTF-8)

  バイナリは、ソースコードのライセンスと同等とします。ただし、フォントの
  文書への埋め込みなど、フォントとしての再使用を目的としない用途においては、
  以下で言う Redistribution には当たらず、制限なく行えるものとします。
  
  Copyright (c) 1990-2003
  	Wada Laboratory, the University of Tokyo. All rights reserved.
  Copyright (c) 2003-2004
  	Electronic Font Open Laboratory (/efont/). All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  3. Neither the name of the Wada Laboratory, the University of Tokyo nor
     the names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.
  
  THIS SOFTWARE IS PROVIDED BY WADA LABORATORY, THE UNIVERSITY OF TOKYO AND
  CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE LABORATORY OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  埋め込まれたビットマップフォントのライセンスについては、各フォントの添付
  ドキュメントを同梱しておりますので、そちらをご参照ください。
  
  本フォントに含まれる東風フォント由来のグリフは、古川泰之さんが単独で作成した
  オリジナルデザインに基づく Public Domain 相当のデータのみが含まれています。
  日立製作所とタイプバンクにより開発された 32 ドットビットマップに由来する
  漢字データ、ビットマップデータ、現在の CLWFK の ライセンスと異なるライセンス
  で配布されていた和田研フォントに由来するデータは含まれていません。


Upstream author's license information (original under Japanese, translated to
English by GOTO Masanori <gotom@debian.org> for convenience)

  Binary data is handled equivalent to source code license.  However,
  there is no limitation and the below description is not applied
  as for in order not to reuse as font (ex: font is embeddd to documents).


  Copyright (c) 1990-2003
  	Wada Laboratory, the University of Tokyo. All rights reserved.
  Copyright (c) 2003-2004
  	Electronic Font Open Laboratory (/efont/). All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
  3. Neither the name of the Wada Laboratory, the University of Tokyo nor
     the names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.
  
  THIS SOFTWARE IS PROVIDED BY WADA LABORATORY, THE UNIVERSITY OF TOKYO AND
  CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
  NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE LABORATORY OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  
  
  Licenses for embedded bitmap font are described each font license files.
  
  This font includes glyphs derived from Kochi font which is created
  by only Yasuhiro Furukawa with original design and is Public Domain data.



Mplus font license information (In Japanese, UTF-8)

	これらのフォントはフリー（自由な）ソフトウエアです。
	あらゆる改変の有無に関わらず、また商業的な利用であっても、自由にご利用、
	複製、再配布することができますが、全て無保証とさせていただきます。

Mplus font license information (original under Japanese, translated to
English by GOTO Masanori <gotom@debian.org> for convenience)

	These fonts are free softwares.
	Unlimited permission is granted to use, copy, and distribute it, with
	or without modification, either commercially and noncommercially.
	THESE FONTS ARE PROVIDED "AS IS" WITHOUT WARRANTY.



Shinonome font license information (In Japanese UTF-8)

  このアーカイブに含まれるすべてのフォントデータ、ドキュメント、スクリプ
  ト類はすべて Public Domain で提供されています 。
  
  但し、日本に於いては現時点で著作権を放棄することは法律上不可能であり、
  AUTHORS に列挙されている作者がその権利を行使しないと宣言することで実質
  的な Public Domain であるとします。
  
  自由な改造、他フォーマットへの変換、組込み、再配布を行うことができます。
  同時に、これらはすべて完全に無保証です。

Shinonome font license information (original under Japanese, translated to
English by GOTO Masanori <gotom@debian.org> for convenience)

  Summary: Public Domain

ko.ttf (UnDotum)
Released under the terms of the GNU General Public License version 2.

zh.ttf (ttf-arphic-uming)
ARPHIC PUBLIC LICENSE

Copyright (C) 1999 Arphic Technology Co., Ltd.
11Fl. No.168, Yung Chi Rd., Taipei, 110 Taiwan
All rights reserved except as specified below.

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is forbidden.

Preamble

   The licenses for most software are designed to take away your
   freedom to share and change it.  By contrast, the ARPHIC PUBLIC
   LICENSE specifically permits and encourages you to use this
   software, provided that you give the recipients all the rights that
   we gave you and make sure they can get the modifications of this
   software.
Legal Terms

0. Definitions:
   Throughout this License, "Font" means the TrueType fonts "AR PL
   Mingti2L Big5", "AR PL KaitiM Big5" (BIG-5 character set) and "AR PL
   SungtiL GB", "AR PL KaitiM GB" (GB character set) which are
   originally distributed by Arphic, and the derivatives of those fonts
   created through any modification including modifying glyph,
   reordering glyph, converting format, changing font name, or
   adding/deleting some characters in/from glyph table.

   "PL" means "Public License".

   "Copyright Holder" means whoever is named in the copyright or
   copyrights for the Font.

   "You" means the licensee, or person copying, redistributing or
   modifying the Font.

   "Freely Available" means that you have the freedom to copy or modify
   the Font as well as redistribute copies of the Font under the same
   conditions you received, not price.  If you wish, you can charge for
   this service.

1. Copying & Distribution
   You may copy and distribute verbatim copies of this Font in any
   medium, without restriction, provided that you retain this license
   file (ARPHICPL.TXT) unaltered in all copies.

2. Modification
   You may otherwise modify your copy of this Font in any way,
   including modifying glyph, reordering glyph, converting format,
   changing font name, or adding/deleting some characters in/from glyph
   table, and copy and distribute such modifications under the terms of
   Section 1 above, provided that the following conditions are met:

   a) You must insert a prominent notice in each modified file stating
      how and when you changed that file.

   b) You must make such modifications Freely Available as a whole to
      all third parties under the terms of this License, such as by
      offering access to copy the modifications from a designated
      place, or distributing the modifications on a medium customarily
      used for software interchange.
   c) If the modified fonts normally reads commands interactively when
      run, you must cause it, when started running for such interactive
      use in the most ordinary way, to print or display an announcement
      including an appropriate copyright notice and a notice that there
      is no warranty (or else, saying that you provide a warranty) and
      that users may redistribute the Font under these conditions, and
      telling the user how to view a copy of this License.

   These requirements apply to the modified work as a whole.  If
   identifiable sections of that work are not derived from the Font,
   and can be reasonably considered independent and separate works in
   themselves, then this License and its terms, do not apply to those
   sections when you distribute them as separate works.  Therefore,
   mere aggregation of another work not based on the Font with the Font
   on a volume of a storage or distribution medium does not bring the
   other work under the scope of this License.

3. Condition Subsequent
   You may not copy, modify, sublicense, or distribute the Font except
   as expressly provided under this License.  Any attempt otherwise to
   copy, modify, sublicense or distribute the Font will automatically
   retroactively void your rights under this License.  However, parties
   who have received copies or rights from you under this License will
   keep their licenses valid so long as such parties remain in full
   compliance.

4. Acceptance
   You are not required to accept this License, since you have not
   signed it.  However, nothing else grants you permission to copy,
   modify, sublicense or distribute the Font.  These actions are
   prohibited by law if you do not accept this License.  Therefore, by
   copying, modifying, sublicensing or distributing the Font, you
   indicate your acceptance of this License and all its terms and
   conditions.

5. Automatic Receipt
   Each time you redistribute the Font, the recipient automatically
   receives a license from the original licensor to copy, distribute or
   modify the Font subject to these terms and conditions.  You may not
   impose any further restrictions on the recipients' exercise of the
   rights granted herein.  You are not responsible for enforcing
   compliance by third parties to this License.

6. Contradiction
   If, as a consequence of a court judgment or allegation of patent
   infringement or for any other reason (not limited to patent issues),
   conditions are imposed on you (whether by court order, agreement or
   otherwise) that contradict the conditions of this License, they do
   not excuse you from the conditions of this License.  If you cannot
   distribute so as to satisfy simultaneously your obligations under
   this License and any other pertinent obligations, then as a
   consequence you may not distribute the Font at all.  For example, if
   a patent license would not permit royalty-free redistribution of the
   Font by all those who receive copies directly or indirectly through
   you, then the only way you could satisfy both it and this License
   would be to refrain entirely from distribution of the Font.

   If any portion of this section is held invalid or unenforceable
   under any particular circumstance, the balance of the section is
   intended to apply and the section as a whole is intended to apply in
   other circumstances.

7. NO WARRANTY
   BECAUSE THE FONT IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
   FOR THE FONT, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT
   WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS OR OTHER
   PARTIES PROVIDE THE FONT "AS IS" WITHOUT WARRANTY OF ANY KIND,
   EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
   FONT IS WITH YOU.  SHOULD THE FONT PROVE DEFECTIVE, YOU ASSUME THE
   COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

8. DAMAGES WAIVER
   UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING, IN NO
   EVENT WILL ANY COPYRIGHTT HOLDERS, OR OTHER PARTIES WHO MAY COPY,
   MODIFY OR REDISTRIBUTE THE FONT AS PERMITTED ABOVE, BE LIABLE TO YOU
   FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, INCIDENTAL, SPECIAL OR
   EXEMPLARY DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE
   FONT (INCLUDING BUT NOT LIMITED TO PROCUREMENT OF SUBSTITUTE GOODS
   OR SERVICES; LOSS OF USE, DATA OR PROFITS; OR BUSINESS
   INTERRUPTION), EVEN IF SUCH HOLDERS OR OTHER PARTIES HAVE BEEN
   ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: treeview
Source:  http://treeview.sourceforge.net/
Files-Excluded: */windows-installer.tar.gz
                */*.jar
                compile_lib
                */.cvsignore
                */install.osX
                */*.bat
                */*.icns

Files: *
Copyright: 2001-2003 Alok Saldanha <alok@caltech.edu>
License: GPL-2

Files: src/LinkedView.java
Copyright: 2001-2003 Alok Saldanha <alok@caltech.edu>
           2013 Alex Segal, Lawrence Berkeley Lab
License: GPL-2

Files: src/edu/*
Copyright: 2001-2003 Alok Saldanha <alok@caltech.edu>
           2013 Alex Segal, Lawrence Berkeley Lab
License: GPL-2

Files: src/edu/stanford/genetics/treeview/NatField.java
Copyright: 1997 Rob Arthan, Lemma 1 Ltd.
           2001-2003 Alok Saldanha <alok@caltech.edu>
           2013 Alex Segal, Lawrence Berkeley Lab
License: GPL-2

Files: src/com/gurge/amd/Quantize.java
Copyright: 2000 Adam Doppelt, E. I. du Pont de Nemours and Company
License: ImageMagick
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files ("ImageMagick"),
 to deal in ImageMagick without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of ImageMagick, and to permit persons to whom the
 ImageMagick is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of ImageMagick.
 .
 The software is provided "as is", without warranty of any kind, express or
 implied, including but not limited to the warranties of merchantability,
 fitness for a particular purpose and noninfringement.  In no event shall
 E. I. du Pont de Nemours and Company be liable for any claim, damages or
 other liability, whether in an action of contract, tort or otherwise,
 arising from, out of or in connection with ImageMagick or the use or other
 dealings in ImageMagick.
 .
 Except as contained in this notice, the name of the E. I. du Pont de
 Nemours and Company shall not be used in advertising or otherwise to
 promote the sale, use or other dealings in ImageMagick without prior
 written authorization from the E. I. du Pont de Nemours and Company.

Files: src/edu/stanford/genetics/treeview/QSortAlgorithm.java
Copyright: 1994-1995 James Gosling, Sun Microsystems, Inc.
License: SUN
 Permission to use, copy, modify, and distribute this software
 and its documentation for NON-COMMERCIAL or COMMERCIAL purposes and
 without fee is hereby granted.
 Please refer to the file http://java.sun.com/copy_trademarks.html
 for further important copyright and trademark information and to
 http://java.sun.com/licensing.html for further important licensing
 information for the Java (tm) Technology.
 .
 SUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SUN SHALL NOT BE LIABLE FOR
 ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 .
 THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES").  SUN
 SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 HIGH RISK ACTIVITIES.


Files: debian/*
Copyright: 2004 Steffen Moeller <moeller@pzr.uni-rostock.de>
           2013 Thorsten Alteholz <debian@alteholz.de>
License: GPL-2

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.


Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Tenmado
Upstream-Contact: Oohara Yuuma <oohara@libra.interq.or.jp>
Source: http://www.interq.or.jp/libra/oohara/tenmado/index.html

Files: *
Copyright: 2002-2012, Oohara Yuuma <oohara@libra.interq.or.jp>
License: GPL-2 or Artistic-License-2.0beta5

Files: install-sh
Copyright: 1994, X Consortium
License: Expat
 Copyright (C) 1994 X Consortium Permission is hereby granted, free of charge,
 to any person obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE X
 CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC- TION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not be
 used in advertising or otherwise to promote the sale, use or other deal- ings
 in this Software without prior written authorization from the X Consor- tium.

Files: debian/*
Copyright: 2002-2012, Oohara Yuuma <oohara@libra.interq.or.jp>
           2015-2018, Markus Koschany <apo@debian.org>
License: GPL-2 or Artistic-License-2.0beta5

License: GPL-2 or Artistic-License-2.0beta5
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
 a) the GNU General Public License version 2 as published by the Free Software
   Foundation (no other version is allowed), or
 .
 b) the Artistic License Version 2.0beta5.
 .
 A copy of the GNU General Public License version 2 is included in the
 source code as GPL-2. A copy of the Artistic License Version 2.0beta5 is
 included in the source code as Artistic-2.
 .
 On Debian systems, a copy of the GNU General Public License version 2 can be
 found at /usr/share/common-licenses/GPL-2.
Comment:
 Debian distributes Tenmado and the debian directory under the terms of the GNU
 General Public license 2. Please see /usr/share/common-licenses/GPL-2 for the
 complete license text.

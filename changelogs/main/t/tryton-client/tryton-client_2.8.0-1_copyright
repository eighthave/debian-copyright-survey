Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2010-2013 Nicolas Évrard
           2007-2013 Cédric Krier
           2007-2013 Bertrand Chenal
           2008-2013 B2CK SPRL
           2008-2011 Udo Spallek
           2008-2011 virtual things - Preisler & Spallek GbR
           2011-2012 Rodrigo Hübner
           2007-2009 Lorenzo Gil Sanchez
           2004-2008 Tiny SPRL
License: GPL-3+

Files: doc/*
Copyright: 2008-2011 Anne Krings
           2008-2011 Bertrand Chenal
           2008-2011 Cédric Krier
           2008-2011 Mathias Behrle
           2008-2011 Tobias Paepke
           2008-2011 Udo Spallek
License: GPL-3+

Files: share/pixmaps/tryton/*.svg
Copyright: not applicable
License: public-domain

Files: debian/*
Copyright: 2009-2012 Daniel Baumann <daniel@debian.org>
           2010-2013 Mathias Behrle <mathiasb@m9s.biz>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 The complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-3 file.

License: public-domain
 The icons used are generally taken from
 http://tango.freedesktop.org/releases/tango-icon-theme-0.8.90.tar.gz
 .
 Some icons were adapted by Cedric Krier 	2011
 .
 Upstream Authors:				2005-2009
 .
 Ulisse Perusin <uli.peru@gmail.com>
 Steven Garrity <sgarrity@silverorange.com>
 Lapo Calamandrei <calamandrei@gmail.com>
 Ryan Collier <rcollier@novell.com>
 Rodney Dawes <dobey@novell.com>
 Andreas Nilsson <nisses.mail@home.se>
 Tuomas Kuosmanen <tigert@tigert.com>
 Garrett LeSage <garrett@novell.com>
 Jakub Steiner <jimmac@novell.com>
 .
 In the preparation of the 0.8.90 release Novell took care of tracking
 down all the contributors to get them to relicense their artwork
 into Public Domain.
 .
 The COPYING file of the tarball states the following:
 | The icons in this repository are herefore released into the Public Domain.
 .
 Additionally the copyright status of the files was tracked in the CVS and the
 rdf properties of the SVGs adjusted for all files that were put into Public
 Domain (see rdf:about and rdf:resource).  Both fields contain a link to the
 Creative Commons Public Domain Dediciation[0] as reproduced below:
 | Public Domain Dedication
 |
 | Copyright-Only Dedication (based on United States law) or Public Domain
 | Certification
 |
 | The person or persons who have associated work with this document (the
 | "Dedicator" or "Certifier") hereby either (a) certifies that, to the best
 | of his knowledge, the work of authorship identified is in the public
 | domain of the country from which the work is published, or (b)
 | hereby dedicates whatever copyright the dedicators holds in the work
 | of authorship identified below (the "Work") to the public domain. A
 | certifier, moreover, dedicates any copyright interest he may have in
 | the associated work, and for these purposes, is described as a
 | "dedicator" below.
 |
 | A certifier has taken reasonable steps to verify the copyright
 | status of this work. Certifier recognizes that his good faith efforts
 | may not shield him from liability if in fact the work certified is not
 | in the public domain.
 |
 | Dedicator makes this dedication for the benefit of the public at
 | large and to the detriment of the Dedicator's heirs and successors.
 | Dedicator intends this dedication to be an overt act of relinquishment
 | in perpetuity of all present and future rights under copyright law,
 | whether vested or contingent, in the Work. Dedicator understands that
 | such relinquishment of all rights includes the relinquishment of all
 | rights to enforce (by lawsuit or otherwise) those copyrights in the
 | Work.
 |
 | Dedicator recognizes that, once placed in the public domain, the Work
 | may be freely reproduced, distributed, transmitted, used, modified,
 | built upon, or otherwise exploited by anyone for any purpose, commercial
 | or non-commercial, and in any way, including by methods that have not
 | yet been invented or conceived.
 0] http://creativecommons.org/licenses/publicdomain/

This package was debianized by David Engel <david@debiang.org>
from sources obtained from:
http://sourceforge.net/project/showfiles.php?group_id=12883

Files: *
Copyright 1998-2007 Ajuba Solutions and other parties
License: BSD

Files: debian/*
Copyright: 2000      David Engel <david@debian.org>
           2000-2002 Mike Markley <mike@markley.org>
	   2003-2007 Chris Waters <xtifr@debian.org>
	   2007-2016 Sergei Golovan <sgolovan@debian.org>
License: BSD

Files: apps/*, modules/bee/*, modules/cache/*, modules/devtools/*, modules/doctools*/*,
       modules/dtplite/*, modules/exif/*, modules/grammar_fa/*, modules/grammar_me/*,
       modules/grammar_peg/*, modules/interp/*, modules/lambda/*, modules/log/log.man,
       modules/map/map_slippy.tcl, modules/nns/*, modules/oometa/oometa.test,
       modules/ooutil/*, modules/page/*, modules/pluginmgr/*, modules/pt/*,
       modules/report/*, modules/string/*, modules/struct/*, modules/term/*,
       modules/tie/*, modules/transfer/*, modules/uev/*, modules/v*, modules/w*
Copyright: 2001-2014 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: examples/bibtex/*
Copyright: 2005 Neil Madden
License: BSD

Files: examples/ftp/*
Copyright: 1997,1998 Steffen Traeger
License: BSD

Files: examples/ftp/hpupdate.tcl
Copyright: 1998 Steffen Traeger
License: GPL-2+

Files: examples/math/*
Copyright: 2003-2005 Stephane Arnold
License: BSD

Files: examples/dns/*, examples/htmlparse/*, examples/mime/*.tcl, examples/ntp/*,
       examples/sasl/*, examples/smtpd/*, modules/md4/*, modules/md5/*,
       modules/md5crypt/*, modules/ntp/*, modules/otp/*, modules/rc4/*,
       modules/ripemd/*, modules/sasl/*, modules/smtpd/*, modules/uuid/*
Copyright: 2001-2006 Pat Thoyts <patthoyts@users.sf.net>
License: BSD

Files: examples/mime/mbot/*
Copyright: 1999-2002 Marshall T. Rose
License: MTR
 Hold harmless the author, and any lawful use is allowed.

Files: examples/tepam/*
Copyright: 2009-2013 Andreas Drollinger
License: BSD

Files: Makefile.in
Copyright: 1999-2000 Ajuba Solutions
           2001      ActiveState Tool Corp.
License: BSD

Files: modules/aes/*
Copyright: 2005 Thorsten Schloermann
           2005 Pat Thoyts <patthoyts@users.sourceforge.net>
           2012-2014 Andreas Kupries
License: BSD

Files: modules/amazon-s3/*
Copyright: 2006,2008 Darren New
License: BSD

Files: modules/amazon-s3/*.test
Copyright: 2006,2008 Darren New
           2008 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/asn/*
Copyright: 1999-2004 Jochen C. Loewer (loewerj@web.de)
           2004 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2004-2011 Michael Schlenker (mic42@users.sourceforge.net)
License: BSD

Files: modules/asn/laymans_guide.txt
Copyright: 1991-1993 RSA Laboratories, a division of RSA Data Security, Inc.
License: RSA

Files: modules/base32/*
Copyright: Public domain

Files: modules/base64/*
Copyright: 1998-2000 Ajuba Solutions
           2000 Eric Melski
           2001 Miguel Sofer
           2002 2003 Pat Thoyts <patthoyts@users.sourceforge.net>
           2010 Emiliano Gavilan
License: BSD

Files: modules/bench/*
Copyright: 2000, 2001 Jeffrey Hobbs
           2005-2008 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/bibtex/*
Copyright: 2005 Neil Madden
           2005 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/blowfish/*
Copyright: 2004 Frank Pilhofer
           2003-2004 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/calendar/*, modules/clock/*
Copyright: 2002-2004 Kevin Kenny
License: BSD

Files: modules/cmdline/*
Copyright: 1998-2000 Ajuba Solutions
           2000 Ross Palmer Mohn
	   2001-2015 Andreas Kupries <andreas_kupries@users.sf.net>
	   2003 David N. Welton  <davidw@dedasys.com>
License: BSD

Files: modules/comm/*
Copyright: 1995-1998 The Open Group
	   2003-2007 ActiveState Corporation
	   2006-2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>
modules/comm/
License: OSF

Files: modules/control/*
Copyright: 2001 Reinhard Max <Reinhard.Max@gmx.de>
License: BSD

Files: modules/coroutine/*
Copyright: 2009,2014-2015 Andreas Kupries
	   2009 Colin Macleod
	   2009 Colin McCormack
	   2009 Donal Fellows
	   2009 Kevin Kenny
	   2009 Neil Madden
	   2009 Peter Spjuth
License: BSD

Files: modules/counter/*
Copyright: 1998-2000 Ajuba Solutions
License: BSD

Files: modules/crc/*
Copyright: 2002 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/cron/*, modules/dicttool/*
Copyright: 2015 Sean Woods <yoda@etoyoc.com>
License: BSD

Files: modules/csv/*
Copyright: 2001 Jeffrey Hobbs
	   2001-2015 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/debug/*
Copyright: 2012 Colin McCormack, Wub Server Utilities
	   2012-2015, Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/des/*
Copyright: 2003, 2004 by Mac A. Cody
	   1996 Andreas Kupries (a.kupries@westend.com)
	   2003-2005 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/dns/*
Copyright: 2002,2004 Pat Thoyts <patthoyts@users.sourceforge.net>
	   2002 Emmanuel Frecon <emmanuel@sics.se>
	   2004 Cisco Systems, Inc.
	   2005 Aamer Akhter <aakhter@cisco.com>
	   2015 Martin Heinrich <martin.heinrich@frequentis.com>
License: BSD

Files: modules/docstrip/*
Copyright: 2003-2010 Lars Hellstrom
License: BSD

Files: modules/fileutil/decode.tcl
Copyright: 2008-2009 ActiveState Software Inc
License: BSD

Files: modules/fileutil/*.test
Copyright: 1998-2000 Ajuba Solutions
	   2001 ActiveState Tool Corp
	   2005-2013 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/fileutil/traverse.*
Copyright: 2006-2015 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/fileutil/fileutil.tcl
Copyright: 1998-2000 Ajuba Solutions
	   2002 Phil Ehrens <phil@slug.org>
	   2005-2013 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/ftp/*
Copyright: 1996-1999 Steffen Traeger <Steffen.Traeger@t-online.de>
	   2000 Ajuba Solutions
	   2000 Zveno Pty Ltd
License: BSD

Files: modules/ftp/ftp_geturl.*
Copyright: 2001 by Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/fumagic/*
Copyright: 2004-2005 Colin McCormack <coldstore@users.sourceforge.net>
	   2005-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/generator/*
Copyright: 2009 Neil Madden <nem@cs.nott.ac.uk>
License: BSD

Files: modules/gpx/*
Copyright: 2010 Keith Vetter <kvetter@gmail.com>
License: BSD

Files: modules/gpx/gpx.test
Copyright: 2010 Keith Vetter <kvetter@gmail.com>
	   2010-2015 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/grammar_aycock/*
Copyright: 2006 Kevin B. Kenny
License: BSD

Files: modules/hook/*
Copyright: 2010 Will Duquette
License: BSD

Files: modules/html/*
Copyright: 1998-2000 Ajuba Solutions
	   2006 Michael Schlenker <mic42@users.sourceforge.net>
License: BSD

Files: modules/htmlparse/*
Copyright: 1995 Sun Microsystems
	   2001-2005 ActiveState Tool Corp
License: BSD

Files: modules/http/autoproxy.tcl
Copyright: 2002-2008 Pat Thoyts <patthoyts@users.sf.net>
License: BSD

Files: modules/httpd/*
Copyright: 2015 Sean Woods
License: BSD

Files: modules/ident/*
Copyright: 2004 Reinhard Max <max@tclers.tk>
License: BSD

Files: modules/imap4/*
Copyright: 2004 Salvatore Sanfilippo <antirez@invece.org>
	   2013 Magnatune <magnatune@users.sourceforge.net>
	   2013 Nicola Hall <nicci.hall@gmail.com>
License: MIT

Files: modules/inifile/*
Copyright: 2003-2007 Aaron Faupell <afaupell@users.sourceforge.net>
	   2008-2012 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/irc/*
Copyright: 2001-2003 David N. Welton <davidw@dedasys.com>
License: BSD

Files: modules/irc/picoirc.*
Copyright: 2004 Richard Suchenwirth
	   2004 Salvatore Sanfillipo
	   2007 Patrick Thoyts
License: BSD

Files: modules/javascript/*
Copyright: 1998-2000 Ajuba Solutions
License: BSD

Files: modules/jpeg/*
Copyright: 2004-2005 Aaron Faupell <afaupell@users.sourceforge.net>
	   2004-2009 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2011 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/json/*
Copyright: 2009-2013 Andreas Kupries <andreas_kupries@sourceforge.net>
License: BSD

Files: modules/json/c/*
Copyright: 2013 Mikhail Teterin <mi@aldan.algebra.com>
License: BSD

Files: modules/json/jsonc.tcl
Copyright: 2013 Mikhail Teterin <mi@aldan.algebra.com>
	   2013,2015 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/json/json.test
Copyright: 2006 ActiveState Software Inc
License: BSD

Files: modules/json/json.man
Copyright: 2006 ActiveState Software Inc
	   2009 Thomas Maeder, Glue Software Engineering AG
License: BSD

Files: modules/ldap/*
Copyright: 1999-2004 Jochen C. Loewer (loewerj@web.de)
	   2006 Michael Schlenker (mic42@users.sourceforge.net)
License: BSD

Files: modules/ldap/ldapx.*
Copyright: 2006 Pierre David <pdav@users.sourceforge.net>
License: BSD

Files: modules/log/loggerAppender.tcl, modules/log/loggerUtils.*
Copyright: 2005 Cisco Systems, Inc
License: BSD

Files: modules/log/loggerAppender.man
Copyright: 2005 Aamer Akhter <aakhter@cisco.com>
License: BSD

Files: modules/log/log.*
Copyright: 2001 ActiveState Tool Corp
License: BSD

Files: modules/log/logger.*, modules/log/logger_trace.*
Copyright: 2002 David N. Welton <davidw@dedasys.com>
	   2004,2005 Michael Schlenker <mic42@users.sourceforge.net>
License: BSD

Files: modules/log/logger.tcl
Copyright: 2003 David N. Welton <davidw@dedasys.com>
	   2004-2011 Michael Schlenker <mic42@users.sourceforge.net>
	   2006,2015 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/mapproj/*
Copyright: 2007 Kevin B. Kenny <kennykb@acm.org>
License: BSD

Files: modules/markdown/*
Copyright: 2014 Caius Project
License: BSD

Files: modules/math/bessel.test, modules/math/calculus.man, modules/math/constants.*,
       modules/math/elliptic.test, modules/math/polynomials.man,
       modules/math/qcomplex.man, modules/math/qcomplex.test,
       modules/math/rational_funcs.man, modules/math/special.*
Copyright: 2002-2005 Arjen Markus
License: BSD

Files: modules/math/bigfloat*
Copyright: 2004-2008 Stephane Arnold <stephanearnold@yahoo.fr>
License: BSD

Files: modules/math/bignum.*
Copyright: 2004 Arjen Markus <arjenmarkus@users.sourceforge.net>
	   2004 Salvatore Sanfilippo <antirez@invece.org>
License: BSD

Files: modules/math/calculus.*, modules/math/interpolate.*, modules/math/optimize.*
Copyright: 2002-2006 Kevin B. Kenny
	   2004 Arjen Markus
License: BSD

Files: modules/math/combinatorics.*, modules/math/exact.*, modules/math/symdiff.*,
       modules/math/romberg.man
Copyright: 2001-2015 Kevin B. Kenny <kennykb@acm.org>
License: BSD

Files: modules/math/decimal.*
Copyright: 2011-2013 Mark Alston <mark@beernut.com>
License: BSD

Files: modules/math/*geometry.*
Copyright: 2001 Ideogramic ApS and other parties
	   2004 Arjen Markus
	   2010 Andreas Kupries
	   2010 Kevin Kenny
License: BSD

Files: modules/math/linalg.man
Copyright: 2004-2008 Arjen Markus <arjenmarkus@users.sourceforge.net>
	   2004 Ed Hume
	   2008 Michael Buadin <relaxkmike@users.sourceforge.net>
License: BSD

Files: modules/math/machineparameters.*
Copyright: 2008 Michael Baudin <michael.baudin@sourceforge.net>
License: BSD

Files: modules/math/math.tcl
Copyright: 1998-2000 Ajuba Solutions
	   2002 Kevin B. Kenny
License: BSD

Files: modules/math/mvlinreg.tcl
Copyright: 2007 Eric Kemp-Benedict
License: BSD

Files: modules/math/numtheory.*
Copyright: 2010 Lars Hellstrom
License: BSD

Files: modules/math/roman.man
Copyright: 2005 Kenneth Green <kenneth.green@gmail.com>
License: BSD

Files: modules/md4/md4.c, modules/md4/md4.h
Copyright: 1991-1992 RSA Data Security, Inc
License: RSA2

Files: modules/md5/md5.c, modules/md5/md5.h
Copyright: 1990 RSA Data Security, Inc
License: RSA3

Files: modules/md5/*.test
Copyright: 2001 ActiveState Tool Corp
License: BSD

Files: modules/mime/smtp.tcl
Copyright: 1999-2000 Marshall T. Rose
	   2003-2006 Pat Thoyts
License: BSD

Files: modules/mime/mime.man
Copyright: 1999-2000 Marshall T. Rose
License: BSD

Files: modules/multiplexer/*
Copyright: 2001-2003 David N. Welton <davidw@dedasys.com>
License: BSD

Files: modules/namespacex/*
Copyright: 200? Neil Madden (http://wiki.tcl.tk/12790)
	   200? Various (http://wiki.tcl.tk/1489)
	   2010 Documentation, Andreas Kupries
License: BSD

Files: modules/ncgi/*
Copyright: 2000 Ajuba Solutions
	   2012 Richard Hipp
	   2012-2014 Andreas Kupries
License: BSD

Files: modules/nettool/*
Copyright: 2015 Sean Woods <yoda@etoyoc.com>
License: BSD

Files: modules/nmea/*
Copyright: 2006-2009, Aaron Faupell <afaupell@users.sourceforge.net>
License: BSD

Files: modules/oauth/*
Copyright: 2014 Javier Perez <hxm@eggdrop.es>
License: BSD

Files: modules/oodialect/*
Copyright: 2015 Sean Woods, Donald K Fellows
License: BSD

Files: modules/pki/*
Copyright: 2010-2013, Roy Keene, Andreas Kupries
License: BSD

Files: modules/png/*, modules/rest/*
Copyright: 2004-2012 Aaron Faupell <afaupell@users.sourceforge.net>
License: BSD

Files: modules/png/*.man, modules/png/*.test
Copyright: 2004 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/pop3d/*
Copyright: 2002-2009 Andreas Kupries
	   2005 Reinhard Max
License: BSD

Files: modules/pop3/*
Copyright: 2000 by Ajuba Solutions
	   2000 Scott Beasley
	   2010-2012 Andreas Kupries
License: BSD

Files: modules/pop3/pop3.test
Copyright: 2002-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/processman/*
Copyright: 2015 Sean Woods <yoda@etoyoc.com>
License: BSD

Files: modules/rcs/*
Copyright: 2005 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2005 Colin McCormack <coldstore@users.sourceforge.net>
License: BSD

Files: modules/sasl/scram.*
Copyright: 2013 Sergei Golovan <sgolovan@nes.ru>
License: BSD

Files: modules/sha1/sha1c.tcl, modules/sha1/sha1.man, modules/sha1/sha256.tcl
Copyright: 2005 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/sha1/sha1.tcl, modules/sha1/sha1v1.tcl
Copyright: 2001 Don Libes <libes@nist.gov>
	   2003 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/sha1/sha1v1.test, modules/sha1/sha1.test
Copyright: 2001 ActiveState Tool Corp
License: BSD

Files: modules/sha1/sha256.?
Copyright: 2001,2002 Allan Saddi <allan@saddi.com>
License: BSD

Files: modules/sha1/sha256.man
Copyright: 2008 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/sha1/sha256c.tcl
Copyright: 2005 Pat Thoyts <patthoyts@users.sourceforge.net>
	   2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/simulation/*
Copyright: 2004-2011 Arjen Markus <arjenmarkus@users.sourceforge.net>
License: BSD

Files: modules/snit/*
Copyright: 2003-2006 by William H. Duquette
License: BSD

Files: modules/snit/snit_tcl83_utils.tcl
Copyright: 2005 Kenneth Green
License: BSD

Files: modules/soundex/*
Copyright: 1998 Evan Rempel <erempel@uvic.ca>
	   2003 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/stooop/*
Copyright: 2001-2002 Jean-Luc Fontaine <jfontain@free.fr>
License: BSD

Files: modules/stringprep/stringprep.*, modules/stringprep/unicode.*
Copyright: 2007-2009 Sergei Golovan
License: BSD

Files: modules/stringprep/stringprep.test
Copyright: 2007 Pat Thoyts
	   2007 Sergei Golovan
License: BSD

Files: modules/stringprep/*_data.*
Copyright: 1998-1999 Scriptics Corporation
	   2007 Alexey Shchepin
	   2007-2008 Sergei Golovan
License: BSD

Files: modules/struct/disjointset.*, modules/struct/graphops.*,
       modules/struct/graph_tcl.tcl
Copyright: 2008 Alejandro Paz <vidriloco@gmail.com>
	   2008-2009 Andreas Kupries
License: BSD

Files: modules/struct/graph/test/Xcontrol, modules/struct/graph/tests/Xsupport,
       modules/struct/queue_oo.tcl, modules/struct/queue.*,
       modules/struct/stack.tcl, modules/struct/tree.testsuite
Copyright: 1998-2000 Ajuba Solutions
	   2006-2010 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/struct/list.*
Copyright: 2003 Kevin B. Kenny
License: BSD

Files: modules/struct/matrix*.tcl
Copyright: 2001-2013 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2003 Edwin A. Suominen <ed@eepatents.com>
License: BSD

Files: modules/struct/pool.man
Copyright: 2002 Erik Leunissen <e.leunissen@hccnet.nl>
License: BSD

Files: modules/struct/prioqueue.*
Copyright: 2002,2003 Michael Schlenker
	   2008 Alejandro Paz <vidriloco@gmail.com>
License: BSD

Files: modules/struct/prioqueue.test
Copyright: 1998-2000 Ajuba Solutions
	   2002 Michael Schlenker
License: BSD

Files: modules/struct/record.*
Copyright: 2002 Brett Schwarz
License: BSD

Files: modules/struct/skiplist.*
Copyright: 2000 Keith Vetter
License: BSD

Files: modules/struct/struct_list.man
Copyright: 2003-2005 Kevin B. Kenny
	   2003-2012 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/tar/*
Copyright: 2004 Aaron Faupell <afaupell@users.sourceforge.net>
	   2013 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/tepam/*
Copyright: 2009-2013 Andreas Drollinger
License: BSD

Files: modules/textutil/adjust.tcl
Copyright: 2000 Ajuba Solutions
	   2000 Eric Melski <ericm@ajubasolutions.com>
	   2001-2003 Dr.Johannes-Heinrich Vogeler (jhv)
	   2001-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2002-2004 Johannes-Heinrich Vogeler <vogeler@users.sourceforge.net>
License: BSD

Files: modules/textutil/dehypht.tex
Copyright: 1988,1991 Rechenzentrum der Ruhr-Universitaet Bochum
	   1993,1994,1999 Bernd Raichle/DANTE e.V.
License: BSD

Files: modules/textutil/expander*
Copyright: 2001 William H. Duquette
License: BSD

Files: modules/textutil/ithyph.tex
Copyright: 1998,2001 Claudio Beccari
License: BSD

Files: modules/textutil/repeat.tcl
Copyright: 2000 Ajuba Solutions
	   2001-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/textutil/split.tcl
Copyright: 2000 Ajuba Solutions
	   2000 Eric Melski <ericm@ajubasolutions.com>
	   2001-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2001 Reinhard Max <max@suse.de>
	   2003 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: modules/textutil/string.tcl, modules/textutil/textutil.tcl
Copyright: 2000 Ajuba Solutions
	   2000 Eric Melski <ericm@ajubasolutions.com>
	   2001-2014 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2002 Joe English <jenglish@users.sourceforge.net>
License: BSD

Files: modules/textutil/trim.tcl
Copyright: 2000 Ajuba Solutions
	   2000 Eric Melski <ericm@ajubasolutions.com>
	   2001-2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/tiff/*
Copyright: 2004-2006 Aaron Faupell <afaupell@users.sourceforge.net>
License: BSD

Files: modules/tiff/tiff.test
Copyright: 2008 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

Files: modules/tool/*
Copyright: 2015 Sean Woods <yoda@etoyoc.com>
License: BSD

Files: modules/tool/meta.man
Copyright: 2011-2015 Andreas Kupries
License: BSD

Files: modules/treeql/*
Copyright: 2004 Colin McCormack <coldstore@users.sourceforge.net>
License: BSD

Files: modules/treeql/treeql.man
Copyright: 2004 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2004 Colin McCormack <coldstore@users.sourceforge.net>
License: BSD

Files: modules/try/*try.*
Copyright: 2008-2011 Donal K. Fellows
	   2008-2011 Andreas Kupries
License: BSD

Files: modules/try/*throw.*
Copyright: 2015 Miguel Martinez Lopez
License: BSD

Files: modules/units/*
Copyright: 2000-2005 Mayo Foundation
License: BSD

Files: modules/uri/uri*
Copyright: 2000 Zveno Pty Ltd
	   2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
	   2006 Pierre DAVID <Pierre.David@crc.u-strasbg.fr>
License: BSD

Files: modules/uri/urn*
Copyright: 2001 Pat Thoyts <patthoyts@users.sf.net>
License: BSD

Files: modules/yaml/*
Copyright: 2008 by KATO Kanryu <kanryu6@users.sourceforge.net>
License: BSD

Files: modules/yaml/huddle.*
Copyright: 2008-2011 KATO Kanryu <kanryu6@users.sourceforge.net>
	   2015 Miguel Martinez Lopez <aplicacionamedida@gmail.com>
License: BSD

Files: modules/zip/*code.tcl
Copyright: 2008-2012 ActiveState Software Inc
License: BSD

Files: modules/zip/*code.man
Copyright: 2008-2014 Andreas Kupries
License: BSD

Files: modules/zip/mkzip.*
Copyright: 2009 Pat Thoyts <patthoyts@users.sourceforge.net>
License: BSD

Files: support/devel/sack/registry/*
Copyright: 2006 Andreas Kupries <andreas_kupries@users.sourceforge.net>
License: BSD

License: BSD
 The following terms apply to all files associated with the software unless
 explicitly disclaimed in individual files.
 .
 The authors hereby grant permission to use, copy, modify, distribute,
 and license this software and its documentation for any purpose, provided
 that existing copyright notices are retained in all copies and that this
 notice is included verbatim in any distributions. No written agreement,
 license, or royalty fee is required for any of the authorized uses.
 Modifications to this software may be copyrighted by their authors
 and need not follow the licensing terms described here, provided that
 the new terms are clearly indicated on the first page of each file where
 they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY
 FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY
 DERIVATIVES THEREOF, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE
 IS PROVIDED ON AN "AS IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE
 NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 MODIFICATIONS.
 .
 GOVERNMENT USE: If you are acquiring this software on behalf of the
 U.S. government, the Government shall have only "Restricted Rights"
 in the software and related documentation as defined in the Federal
 Acquisition Regulations (FARs) in Clause 52.227.19 (c) (2).  If you
 are acquiring the software on behalf of the Department of Defense, the
 software shall be classified as "Commercial Computer Software" and the
 Government shall have only "Restricted Rights" as defined in Clause
 252.227-7013 (c) (1) of DFARs.  Notwithstanding the foregoing, the
 authors grant the U.S. Government and others acting in its behalf
 permission to use and distribute the software in accordance with the
 terms specified in this license.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: RSA
 License to copy this document is granted
 provided that it is identified as "RSA Data Security, Inc.
 Public-Key Cryptography Standards (PKCS)" in all material
 mentioning or referencing this document.

License: OSF
 This software was developed by the Open Group Research Institute
 ("RI").  This software, both binary and source (hereafter, Software)
 is copyrighted by The Open Group Research Institute and ownership
 remains with the RI.
 .
 The RI hereby grants you (hereafter, Licensee) permission to use,
 copy, modify, distribute, and license this Software and its
 documentation for any purpose, provided that existing copyright
 notices are retained in all copies and that this notice is included
 verbatim in any distributions. No written agreement, license, or
 royalty fee is required for any of the authorized uses provided
 that the RI is publicly and prominently acknowledged as the source
 of this software.
 .
 Licensee may make derivative works.  However, if Licensee distributes
 any derivative work based on or derived from the Software, then
 Licensee will (1) notify the RI regarding its distribution of the
 derivative work, (2) clearly notify users that such derivative work
 is a modified version and not the original software distributed by
 the RI, and (3) the RI is publicly and prominently acknowledged as
 the source of this software.
 .
 THE RI MAKES NO REPRESENTATIONS ABOUT THE SERVICEABILITY OF THIS
 SOFTWARE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS
 OR IMPLIED WARRANTY.  THE RI SHALL NOT BE LIABLE FOR ANY DAMAGES
 SUFFERED BY THE USERS OF THIS SOFTWARE.
 .
 By using or copying this Software, Licensee agrees to abide by the
 copyright law and all other applicable laws of the U.S. including,
 but not limited to, export control laws, and the terms of this
 license.  The RI shall have the right to terminate this license
 immediately by written notice upon Licensee's breach of, or
 non-compliance with, any of its terms.  Licensee may be held legally
 responsible for any copyright infringement that is caused or
 encouraged by Licensee's failure to abide by the terms of this
 license.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, provided that the above
 copyright notice(s) and this permission notice appear in all copies of
 the Software and that both the above copyright notice(s) and this
 permission notice appear in supporting documentation.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL
 INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING
 FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder
 shall not be used in advertising or otherwise to promote the sale, use
 or other dealings in this Software without prior written authorization
 of the copyright holder.

License: RSA2
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD4 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD4 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

License: RSA3
 License to copy and use this software is granted provided that
 it is identified as the "RSA Data Security, Inc. MD5 Message-
 Digest Algorithm" in all material mentioning or referencing this
 software or this function.
 .
 License is also granted to make and use derivative works
 provided that such works are identified as "derived from the RSA
 Data Security, Inc. MD5 Message-Digest Algorithm" in all
 material mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning
 either the merchantability of this software or the suitability
 of this software for any particular purpose.  It is provided "as
 is" without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

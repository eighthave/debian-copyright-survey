Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/LanguageMachines/timbl

Files: *
Comment: Ko van der Sloot with Peter Berck, Antal van den Bosch,
 Walter Daelemans, Maarten van Gompel, Ton Weijters and Jakub Zavrel.
Copyright: Copyright © 1998 - 2020, ILK Research Group (Tilburg University,
 The Netherlands) and CLiPS Research Centre (University of Antwerp, Belgium)
 Centre of Language and Speech Technology (Radboud University, The Netherlands)
License: GPL-3+

Files: debian/*
Copyright: Copyright © 2008, 2009, 2010 Joost van Baal <joostvb+timbl@uvt.nl>,
 Copyright © 2016 - 2020 Maarten van Gompel <proycon@anaproy.nl>
License: GPL-3+

Files: m4/*
Copyright: Copyright (C) 2004-2005, 2007-2009, 2011-2015 Free Software Foundation, Inc.
License: permissive

Files: m4/pkg.m4
Copyright: Copyright © 2004 Scott James Remnant <scott@netsplit.com>
License: GPL-3+

License: GPL-3+
 TiMBL is free software; you can redistribute it and/or modify it under the
 terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License, or (at your option) any later
 version.
 .
 TiMBL is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 TiMBL. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: permissive
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Net-DHCPv6-DUID-Parser
Upstream-Contact: Tom Wright <twright@cpan.org>
Upstream-Name: Net-DHCPv6-DUID-Parser

Files: *
Copyright: 2010, Tom Wright <twright@cpan.org>
License: BSD-2-clause

Files: debian/*
Copyright: 2014, Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
 .
     2. Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials
        provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY TOM WRIGHT ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL TOM WRIGHT OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 The views and conclusions contained in the software and documentation
 are those of the authors and should not be interpreted as
 representing official policies, either expressed or implied, of Tom
 Wright.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

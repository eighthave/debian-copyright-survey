Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XML-Stream
Upstream-Contact: Darian Anthony Patrick <dapatrick@cpan.org>
Source: https://metacpan.org/release/XML-Stream

Files: *
Copyright: 1998-2004, Jabber Software Foundation <http://jabber.org/>
 2010-2014, Darian Anthony Patrick <dapatrick@cpan.org>
License: LGPL-2+

Files: debian/*
Copyright: 2000, 2001, 2002, 2003, Ardo van Rangelrooij <ardo@debian.org>
 2004, 2007, Jay Bonci <jaybonci@debian.org>
 2007, Andreas Barth <aba@not.so.argh.org>
 2010, Ansgar Burchardt <ansgar@43-1.org>
 2010, Franck Joncourt <franck@debian.org>
 2010, Jonathan Yu <jawnsy@cpan.org>
 2011, Fabrizio Regalli <fabreg@fabreg.it>
 2012-2018, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; either version 2.1, or (at your
 option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2'

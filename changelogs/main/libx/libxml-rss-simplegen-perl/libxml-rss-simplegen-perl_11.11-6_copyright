Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Sean M. Burke <sburke@cpan.org>
Source: https://metacpan.org/release/XML-RSS-SimpleGen
Upstream-Name: XML-RSS-SimpleGen

Files: *
Copyright: 2003, 2004 Sean M. Burke <sburke@cpan.org>
License: Artistic or GPL-1+
Comment:
 This library is free software; you can redistribute it and/or modify
 it under the same terms as Perl itself.
 .
 Portions of the data tables in this module are derived from the
 entity declarations in the W3C XHTML specification.
 .
 Currently (January 2004), that's these three:
 .
       http://www.w3.org/TR/xhtml1/DTD/xhtml-lat1.ent
       http://www.w3.org/TR/xhtml1/DTD/xhtml-special.ent
       http://www.w3.org/TR/xhtml1/DTD/xhtml-symbol.ent
 .
 Portions of the code in this module were adapted from parts of
 Gisle Aas's L<LWP::Simple> and the old (v2.x) version of his
 L<HTML::Parser>.

Files: debian/*
Copyright: 2008-2015 Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+
Comment: Licensed under the same terms as the software itself.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

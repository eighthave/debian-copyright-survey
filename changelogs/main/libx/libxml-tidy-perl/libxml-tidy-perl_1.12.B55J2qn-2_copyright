Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XML-Tidy
Upstream-Contact: Pip Stuart <Pip@CPAN.Org>
Source: https://metacpan.org/release/XML-Tidy/

Files: *
Copyright: 2004-2011, Pip Stuart.
License: GPL-3

Files: t/02large.t (The contained "RELAX NG schema for XHTML 2.0")
Copyright: 2003-2004, W3C(R) (MIT, ERCIM, Keio), All Rights Reserved.
License: W3C-Software

Files: debian/*
Copyright: 
 2009, Ryan Niebur <ryanryan52@gmail.com>
 2010-2011, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+ or GPL-3

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'

License: W3C-Software
 By obtaining, using and/or copying this work, you (the licensee) agree
 that you have read, understood, and will comply with the following
 terms and conditions.
 .
 Permission to copy, modify, and distribute this software and its
 documentation, with or without modification, for any purpose and
 without fee or royalty is hereby granted, provided that you include the
 following on ALL copies of the software and documentation or portions
 thereof, including modifications:
   * The full text of this NOTICE in a location viewable to users of the
     redistributed or derivative work.
   * Any pre-existing intellectual property disclaimers, notices, or
     terms and conditions. If none exist, the W3C Software Short Notice
     should be included (hypertext is preferred, text is permitted)
     within the body of any redistributed or derivative code.
   * Notice of any changes or modifications to the files, including the
     date changes were made. (We recommend you provide URIs to the
     location from which the code is derived.)
 .
 Disclaimers
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT
 HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE OR
 DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS,
 TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL
 OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR
 DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in
 advertising or publicity pertaining to the software without specific,
 written prior permission. Title to copyright in this software and any
 associated documentation will at all times remain with copyright
 holders.
 .
 Notes
 .
 This version:
 http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231
 .
 This formulation of W3C's notice and license became active on December
 31 2002. This version removes the copyright ownership notice such that
 this license can be used with materials other than those owned by the
 W3C, reflects that ERCIM is now a host of the W3C, includes references
 to this specific dated version of the license, and removes the
 ambiguous grant of "use". Otherwise, this version is the same as the
 previous version and is written so as to preserve the Free Software
 Foundation's assessment of GPL compatibility and OSI's certification
 under the Open Source Definition.


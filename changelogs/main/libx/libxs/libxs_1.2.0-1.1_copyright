Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

License: LGPL-3.0+ with linking exception
 Crossroads I/O is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 Crossroads is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 As a special exception, copyright holders give you permission to link this
 library with independent modules to produce an executable, regardless of
 the license terms of these independent modules, and to copy and distribute
 the resulting executable under terms of your choice, provided that you also
 meet, for each linked independent module, the terms and conditions of
 the license of that module. An independent module is a module which is not
 derived from or based on this library. If you modify this library, you must
 extend this exception to your version of the library.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

Files: *
Copyright: 2009-2012 250bpm s.r.o.
           2007-2011 iMatix Corporation
License: LGPL-3.0+ with linking exception

Files: include/zmq.h include/zmq_utils.h src/xszmq.cpp
Copyright: 2012 Martin Lucina
License: LGPL-3.0+ with linking exception

Files: src/pgm_receiver.cpp src/pgm_sender.cpp
Copyright: 2012 Lucina & Associates
License: LGPL-3.0+ with linking exception

Files: src/mtrie.cpp src/mtrie.hpp
Copyright: 2011-2012 Spotify AB
License: LGPL-3.0+ with linking exception

Files: src/pgm_receiver.cpp src/pgm_receiver.hpp src/pgm_sender.cpp src/pgm_sender.hpp src/pgm_socket.cpp src/pgm_socket.hpp
Copyright: 2010-2011 Miru Limited
License: LGPL-3.0+ with linking exception

Files: tests/wireformat.cpp
Copyright: 2012 Paul Colomiets
License: LGPL-3.0+ with linking exception

Files: include/xs/xs.h src/dist.cpp src/encoder.cpp src/fq.cpp src/lb.cpp src/msg.hpp src/options.cpp src/options.hpp src/pipe.cpp src/pipe.hpp src/req.cpp src/req.hpp src/session_base.cpp src/session_base.hpp src/socket_base.cpp src/socket_base.hpp src/xpub.cpp src/xrep.cpp src/xrep.hpp src/xreq.cpp src/xsub.cpp tests/invalid_rep.cpp tests/reqrep_device.cpp
Copyright: 2011 VMware, Inc.
License: LGPL-3.0+ with linking exception

Files: *
Copyright: 2007-2011 Contributors as listed in AUTHORS
License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2012 Robert S. Edmonds <edmonds@debian.org>
License: permissive
 Copying and distribution of this package, with or without modification,
 are permitted in any medium without royalty provided the copyright notice
 and this notice are preserved.

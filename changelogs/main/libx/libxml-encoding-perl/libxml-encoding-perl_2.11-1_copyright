Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: XML-Encoding
Upstream-Contact: Steve Hay <shay@cpan.org>
Source: https://metacpan.org/release/XML-Encoding

Files: *
Copyright:
 1998, Clark Cooper <coopercc@netheaven.com>
 2007-2020, Steve Hay <shay@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2001-2004, 2006, 2008, Ardo van Rangelrooij <ardo@debian.org>
 2008-2009, Daniel Leidert (dale) <daniel.leidert@wgdd.de>
 2011, Nicholas Bamber <nicholas@periapt.co.uk>
 2014-2020, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Matt Sergeant
Source: https://metacpan.org/release/XML-Filter-XSLT
Upstream-Name: XML-Filter-XSLT

Files: *
Copyright: © 2001, Matt Sergeant <matt@sergeant.org>
License: Artistic or GPL-1+
Comment:
  The license is missing from the upstream sources but was confirmed by the
  author in a mail (http://bugs.debian.org/567364#15):
  .
  From: Matt Sergeant <matt@sergeant.org>
  Date: Sun, 31 Jan 2010 11:42:33 -0500
  Subject: Re: Copyright and license for Perl module XML::Filter::XSLT
  .
  I can confirm the license. If I ever get to updating the module I'll add the
  claim.
  .
  > [...]
  > the Debian package for XML::Filter::XSLT claims that the module is
  > distributed under the same terms as perl (ie. Artistic License and
  > General Public License), but this is not documented anywhere else in the
  > distribution.
  >
  > As you are listed as the author for the module, could you please clarify
  > under which terms the module is distributed?  It would be nice if you
  > could include years for the copyright.
  >
  > A reply like
  >
  >    The module ... is free software; you can redistribute it and/or modify
  >    it under the same terms as Perl itself.
  >
  >    It is copyright<year>  by ...
  >
  > would be enough.

Files: debian/*
Copyright:
 © 2002-2003, Ardo van Rangelrooij <ardo@debian.org>
 © 2004,      Michael K. Edwards <medwards-debian@sane.net>
 © 2004,      Jay Bonci <jaybonci@debian.org>
 © 2005,      Florian Ragwitz <rafl@debian.org>
 © 2010,      Ansgar Burchardt <ansgar@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jquery.tmpl.js
Source: https://github.com/BorisMoore/jquery-tmpl

Files: *
Copyright: Copyright Software Freedom Conservancy, Inc.
License: GPL-2 or MIT

Files: demos/movies/*/jquery-ui-*.custom*:
Copyright: 2008 Paul Bakaus (ui.jquery.com)
 Brandon Aaron
 David Bolter
 Rich Caloggero
 Chi Cheng (cloudream@gmail.com)
 Colin Clark (http://colin.atrc.utoronto.ca/)
 Michelle D'Souza
 Aaron Eisenberger (aaronchi@gmail.com)
 Ariel Flesler
 Bohdan Ganicky
 Scott González
 Marc Grabanski (m@marcgrabanski.com)
 Klaus Hartl (stilbuero.de)
 Scott Jehl
 Cody Lindley
 Eduardo Lundgren (eduardolundgren@gmail.com)
 Todd Parker
 John Resig
 Patty Toland
 Ca-Phun Ung (yelotofu.com)
 Keith Wood (kbwood@virginbroadband.com.au)
 Maggie Costello Wachs
 Richard D. Worth (rdworth.org)
 Jörn Zaefferer (bassistance.de)
License: GPL-2 or MIT

Files: demos/movies/components/jquery.pager.js
Copyright: 2008-2009 Jon Paul Davies
License: GPL-2 or MIT

Files: debian/*
Copyright: 2016 W. Martin Borgert <debacle@debian.org>
License: GPL-2 or MIT

License: GPL-2
 You can redistribute this software and/or modify it under the terms of
 the GNU General Public License as published by the Free Software
 Foundation; version 2 dated June, 1991.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in /usr/share/common-licenses/GPL-2 file.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

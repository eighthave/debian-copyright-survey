Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jTDS
Source: http://jtds.sourceforge.net

Files: *
Copyright: 2004, The jTDS Project
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-2.1'

Files: debian/*
Copyright: 2007-2013, Martín Ferrari <tincho@debian.org>
License: GPL-2
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'

Files: src/main/net/sourceforge/jtds/util/GeneralDigest.java
       src/main/net/sourceforge/jtds/util/MD4Digest.java
       src/main/net/sourceforge/jtds/util/MD5Digest.java
Copyright: 2000, The Legion Of The Bouncy Castle (http://www.bouncycastle.org)
License: MIT
 Copyright (c) 2000 The Legion Of The Bouncy Castle (http://www.bouncycastle.org)
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/main/net/sourceforge/jtds/util/DESEngine.java
Copyright: 2003, Matt Brinkley
           2000, The Legion Of The Bouncy Castle (http://www.bouncycastle.org)
License: MIT-Bouncycastle
 This file is a slightly modified version of
 org.bouncycastle.crypto.engines.DESEngine from the Bouncy Castle project. It
 lacks a copyright notice on it, and the original version does too; so I
 understand this was a unintentional omission. The Bouncy Castle project states
 that "Except where otherwise stated, software produced by this site is
 covered by the following license."
 .
 Copyright (c) 2000 The Legion Of The Bouncy Castle (http://www.bouncycastle.org)
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 USE OR OTHER DEALINGS IN THE SOFTWARE.



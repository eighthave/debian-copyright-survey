Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: TiongHiang Lee
Upstream-Name: Jxp
Source: http://sourceforge.net/projects/jxp/

Files: *
Copyright: 2004, TiongHiang Lee
License: LGPL-2.1+

Files: src/java/org/onemind/jxp/parser/JxpParser.jjt
 src/java/org/onemind/jxp/parser/JxpParser.jj
Copyright: 2006, Sun Microsystems, Inc.
License: BSD-3-clause
Comment: The files as distributed in the
 upstream tarball include the following notice:
 .
 *                 Sun Public License Notice
 *
 * The contents of this file are subject to the Sun Public License
 * Version 1.0 (the "License"). You may not use this file except in
 * compliance with the License. A copy of the License is available at
 * http://www.sun.com/
 *
 * The Original Code is JavaCC. The Initial Developer of the Original
 * Code is Sun Microsystems, Inc. Portions Copyright 1996-2002 Sun
 * Microsystems, Inc. All Rights Reserved.
 */
 /*
 * Altered by Tiong Lee (thlee@onemindsoft.org) extensive to be a JJTree grammar
 */
 .
 We have contacted the upstream author TiongHiang Lee regarding the source
 from which these derived works were created. The original file is Java1.1.jj,
 part of the JavaCC software (https://javacc.dev.java.net/)
 .
 Java1.1.jj is also present in the Debian package javacc-doc (source
 package javacc: examples/JavaGrammars/Java1.1.jj).  The file is no longer
 distributed under the SPL, but instead under a BSD-like license.

Files: debian/*
Copyright: 2009-2013, gregor herrmann <gregoa@debian.org>
 2009-2012, tony mancill <tmancill@debian.org>
License: LGPL-2.1+

License: BSD-3-clause
 /* Copyright (c) 2006, Sun Microsystems, Inc.
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted provided that the following conditions are met:
  *
  *     * Redistributions of source code must retain the above copyright notice,
  *       this list of conditions and the following disclaimer.
  *     * Redistributions in binary form must reproduce the above copyright
  *       notice, this list of conditions and the following disclaimer in the
  *       documentation and/or other materials provided with the distribution.
  *     * Neither the name of the Sun Microsystems, Inc. nor the names of its
  *       contributors may be used to endorse or promote products derived from
  *       this software without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
  * THE POSSIBILITY OF SUCH DAMAGE.
  */

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 On Debian systems the GNU Lesser General Public License, version 2.1 can be
 found at /usr/share/common-licenses/LGPL-2.1.

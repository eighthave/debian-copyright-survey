Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: genparse
Upstream-Contact: Michael Geng <linux@michaelgeng.de>, <mgeng@users.sourceforge.net>
Source: https://sourceforge.net/projects/genparse
X-Upstream-Vcs: https://sourceforge.net/projects/genparse/develop
X-Upstream-Bugs: https://sourceforge.net/p/genparse/bugs/

Files: *
Copyright:
 2006-2010 Michael Geng <linux@michaelgeng.de>, <mgeng@users.sourceforge.net>
 1999-2000 Mike Borella <Mike_Borella@mw.3com.com>, <mborella@users.sourceforge.net>
License: GPL-2+

Files: debian/*
Copyright:
 2010-2016 Jari Aalto <jari.aalto@cante.net>
 2008      Luk Claes <luk@debian.org>
 2005      Lars Wirzenius <liw@iki.fi>
 2005      Víctor Pérez Pereira <vperez@debianvenezuela.org>
 2004      Christoph Berg <cb@df7cb.de>
 2004      Andrew Pollock <apollock@debian.org>
 2001-2003 Luca Filipozzi <lfilipoz@debian.org>
 1999-2001 James R. Van Zandt <jrv@vanzandt.mv.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

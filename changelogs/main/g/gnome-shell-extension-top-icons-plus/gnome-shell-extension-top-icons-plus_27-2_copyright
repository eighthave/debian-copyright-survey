Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: TopIcons Plus
Source: https://github.com/phocean/TopIcons-plus

Files: *
Copyright:
 © 2013 Adel Gadllah
 © 2014 Mohammad Javad Naderi
 © 2016 Jean-Christophe Baptiste (phocean)
License: GPL-2+
Comment:
 Adel Gadllah and Mohammad Javad Naderi are the authors of the extensions
 on which this one is based (Top Icons and TopTray). There is no copyright
 information in the source code of those extensions, but both were uploaded
 to extensions.gnome.org, which requires the uploader to confirm that the
 extension may be distributed under the terms of the GPLv2+.

Files:
 convenience.js
Copyright:
 © 2011-2012 Giovanni Campagna
License: BSD-3-clause

Files: debian/*
Copyright:
 © 2014-2015 Tobias Frost
 © 2016 Simon McVittie
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
   * Neither the name of the GNOME nor the
     names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

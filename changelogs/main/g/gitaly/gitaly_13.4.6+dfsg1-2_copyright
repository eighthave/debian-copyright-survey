Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gitaly
Source: https://gitlab.com/gitlab-org/gitaly
Comment: Add vendor to add dependencies not yet packaged
Files-Excluded: ruby/proto/gitaly/*_pb.rb ruby/Gemfile.lock

Files: *
Copyright: 2016-2017 GitLab B.V.
License: Expat

Files: debian/*
Copyright: 2017 Pirate Praveen <praveen@debian.org>
           2018 Dmitry Smirnov <onlyjob@debian.org>
License: Expat

Files: vendor/github.com/honnef.co/*
Copyright: 2016 Dominik Honnef
License: Expat

Files: vendor/github.com/getsentry/*
Copyright: 2019 Sentry (https://sentry.io) and individual contributors.
License: BSD-2-clause

Files: vendor/github.com/grpc-ecosystem/go-grpc-middleware/*
Copyright: 2016 Michal Witkowski
License: Apache-2.0

Files: vendor/github.com/grpc-ecosystem/go-grpc-prometheus/*
Copyright: 2016 Michal Witkowski
License: Apache-2.0

Files: vendor/github.com/kardianos/*
Copyright: 2015 The Go Authors.
License: BSD-3-clause

Files: vendor/github.com/konsorten/*
Copyright: 2017 marvin + konsorten GmbH (open-source@konsorten.de)
License: Expat

Files: vendor/github.com/lightstep/lightstep-tracer-go/*
Copyright: 2016 Lightstep, Inc.
License: Expat
Comment: Copyright derived from github username

Files: vendor/github.com/lightstep/thrift/*
Copyright: 2006-2010 The Apache Software Foundation.
License: Apache-2.0

Files: vendor/github.com/mwitkow/*
Copyright: Michal Witkowski
License: Apache-2.0

Files: vendor/github.com/opentracing/opentracing-go/*
Copyright: OpenTracing API for Go contributors
License: Apache-2.0
Comment: Copyright derived from github project name

Files: vendor/github.com/philhofer/*
Copyright: 2014-2015, Philip Hofer
License: Expat

Files: vendor/github.com/pkg/*
Copyright: 2015, Dave Cheney <dave@cheney.net>
License: BSD-2-clause

Files: vendor/google.golang.org/*
Copyright: 2014-2018 Google Inc
License: Apache-2.0

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla Public License,
 v. 2.0. If a copy of the MPL was not distributed with this file, You can
 obtain one at http://mozilla.org/MPL/2.0/.
 .
 On Debian systems, the complete text of the Mozilla Publlic License, v. 2.0
 can be found in "/usr/share/common-licenses/MPL-2.0".

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following disclaimer
       in the documentation and/or other materials provided with the
       distribution.
     * Neither the name of Google Inc. nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

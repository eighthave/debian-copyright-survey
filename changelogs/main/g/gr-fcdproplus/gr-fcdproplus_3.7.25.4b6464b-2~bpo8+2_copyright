Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gr-fcdproplus
Upstream-Contact: https://github.com/dl1ksv/gr-fcdproplus
Source:
 git clone https://github.com/dl1ksv/gr-fcdproplus
 The upstream package source tarball was generated from the version:
 git archive --format=tar --prefix=gr-fcdproplus-3.7.25.4b6464b/ 4b6464b | xz > ../../gr-fcdproplus_3.7.25.4b6464b.orig.tar.xz
Comment:
 Debian packages by A. Maitland Bottoms <bottoms@debian.org>,
 .
 Upstream Authors:
 Copyright 2013 Volker Schroer, DL1KSV
License: GPL-3+

Files: COPYING
Copyright: 2007 Free Software Foundation, Inc. <http://fsf.org/>
License: FSF-COPYING
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

Files: *CMakeLists.txt cmake/Modules/FindGnuradioModules.cmake
 cmake/cmake_uninstall.cmake.in
 python/__init__.py docs/* gnuradio-fcdproplus.pc.in
 include/fcdproplus/api.h lib/qa_fcdproplus.* lib/test_fcdproplus.cc
 swig/fcdproplus_swig.i
Copyright: 2011,2012 Free Software Foundation, Inc.
Comment: From GNU Radio gr_modtool templates
License: GPL-3+

Files: debian/*
Copyright: 2013 A. Maitland Bottoms <bottoms@debian.org>
License: GPL-3+

Files: example/fm.grc
 include/fcdproplus/fcdproplus.h lib/fcdcmd.h lib/fcdproplus_impl.*
 README .gitignore grc/fcdproplus_fcdproplus.xml.in
Copyright: 2013 Volker Schroer, DL1KSV
License: GPL-3+

Files: cmake/Modules/CMakeParseArgumentsCopy.cmake
Copyright: 2010 Alexander Neundorf <neundorf@kde.org>
Comment: copy of CMakeParseArguments.cmake from cmake 2.8.x
License: Kitware-BSD
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the names of Kitware, Inc., the Insight Software Consortium,
   nor the names of their contributors may be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: lib/hid/hidapi.h lib/hid/hidmac.c lib/hid/hid.c
Copyright: 2009,2010 Alan Ott, Signal 11 Software
License: GPL-3+
Comment:
 At the discretion of the user of this library,
 this software may be licensed under the terms of the
 GNU General Public License v3, a BSD-Style license, or the
 original HIDAPI license as outlined in the LICENSE.txt,
 LICENSE-gpl3.txt, LICENSE-bsd.txt, and LICENSE-orig.txt
 files located at the root of the source distribution.
 These files may also be found in the public source
 code repository located at:
 http://github.com/signal11/hidapi .

License: GPL-3+
 GNU Radio is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 GNU Radio is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License (GPL) version 3 can be found in the file
 '/usr/share/common-licenses/GPL-3'.

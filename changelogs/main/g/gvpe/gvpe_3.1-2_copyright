Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GVPE
Source: http://software.schmorp.de/pkg/gvpe.html

Files: *
Copyright: 2003-2009 Marc Lehmann <pcg@goof.com>
 1998-2002 Ivo Timmermans <ivo@o2w.nl>
 2000-2002 Guus Sliepen <guus@sliepen.eu.org>
License: GPL-3.0+ with OpenSSL exception

Files: src/lzf/*
Copyright: 2003-2009 Marc Lehmann <pcg@goof.com>
License: BSD-2-clause or GPL-2.0+

Files: src/tincd/*
Copyright: 2001-2004 Ivo Timmermans <ivo@tinc-vpn.org>,
 2001-2004 Guus Sliepen <guus@tinc-vpn.org>
License: GPL-2.0+

Files: debian/*
Copyright: 2011 TANIGUCHI Takaki <takaki@debian.org>
License: GPL-2.0+

Files: lib/*
Copyright: 2000,2001 Ivo Timmermans <ivo@o2w.nl>,
 2000,2001 Guus Sliepen <guus@sliepen.eu.org>
 2003,2007,2008,2009 Marc Lehmann <gvpe@schmorp.de>
License: GPL-2.0+

Files: lib/getopt1.c lib/gettext.h
Copyright: Free Software Foundation, Inc.
License: GPL-2.0+

Files: lib/pidfile.c lib/pidfile.h
Copyright: 1995  Martin Schulze <Martin.Schulze@Linux.DE>
License: GPL-2.0+

Files: lib/alloca.c
Copyright: public-domain
License: public-domain

Files: libev/*
Copyright: 2003,2007,2008,2009 Marc Lehmann <gvpe@schmorp.de>
License: BSD-2-clause or GPL-2.0+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modifica-
 tion, are permitted provided that the following conditions are met:
 .
 1.  Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 .
 2.  Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MER-
 CHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPE-
 CIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTH-
 ERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3.0+ with OpenSSL exception
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
 .
 Additional permission under GNU GPL version 3 section 7
 .
 If you modify this Program, or any covered work, by linking or
 combining it with the OpenSSL project's OpenSSL library (or a modified
 version of that library), containing parts covered by the terms of the
 OpenSSL or SSLeay licenses, the licensors of this Program grant you
 additional permission to convey the resulting work.  Corresponding
 Source for a non-source form of such a combination shall include the
 source code for the parts of OpenSSL used as well as that of the
 covered work.

License: public-domain
 public-domain


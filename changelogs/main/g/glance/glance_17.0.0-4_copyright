Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: glance
Source: https://github.com/openstack/glance
Files-Excluded: https://github.com/openstack/glance/archive/2011.2.tar.gz

Files: *
Copyright: (c) 2010-2011, US Government as represented by the Administrator of NASA
	(c) 2010-2016, OpenStack Foundation <openstack-dev@lists.openstack.org>
        (c) 2012-2017, Red Hat, Inc
        (c) 2013-2016, Hewlett Packard Enterprise Software, LLC
        (c) 2013-2014, IBM Corp.
        (c) 2013-2016, Intel Corp
        (c) 2015, Mirantis, Inc.
        (c) 2013, Yahoo! Inc.
        (c) 2016-2018, NTT DATA
        (c) 2013-2016, Rackspace Hosting
        (c) 2012, Piston Cloud Computing, Inc.
        (c) 2012-2013, Canonical Ltd.
        (c) 2014, SoftLayer Technologies, Inc.
        (c) 2012, Justin Santa Barbara
        (c) 2017, Huawei Technologies Co., Ltd.
License: Apache-2

Files: debian/*
Copyright: (c) 2010-2018, Thomas Goirand <zigo@debian.org>
	(c) 2012, Mehdi Abaakouk <sileht@sileht.net>
	(c) 2011-2012, Ghe Rivero <ghe@debian.org>
	(c) 2012, Ola Lundqvist <ola@inguza.com>
	(c) 2011, Julien Danjou <acid@debian.org>
	(c) 2011, Chuck Short <zulcss@ubuntu.com>
	(c) 2011, Soren Hansen <soren@ubuntu.com>
	(c) 2011, Canonical
	(c) 2017-2018, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gqrx-sdr
Upstream-Contact: http://www.oz9aec.net/index.php/gnu-radio/gqrx-sdr
Source: http://sourceforge.net/projects/gqrx/
 The upstream package source tarball was generated from git:
 git clone https://github.com/csete/gqrx.git
 git archive --format=tar --prefix=gqrx-sdr-2.3.2.193/  bbed2c9  | xz > ../../gqrx-sdr_2.3.2.193.orig.tar.xz
Copyright: Copyright 2011-2016 Alexandru Csete OZ9AEC.
License: GPL-3+

Files: debian/*
Copyright: © 2011-2016 A. Maitland Bottoms <bottoms@debian.org>
License: GPL-3+

Files: .gitignore .travis.yml src/applications/*  src/dsp/*  gqrx.desktop  gqrx.pro
 src/interfaces/* src/osxaudio/*  src/portaudio/*  src/pulseaudio/* src/qtgui/*
 README.md  src/receivers/*  resources/textfiles.qrc gqrx.appdata.xml
 resources/icons.qrc resources/news.txt CMakeLists.txt cmake/*
 src/CMakeLists.txt resources/remote-control.txt new_logo/*
Copyright: Copyright 2011-2017 Alexandru Csete OZ9AEC.
License: GPL-3+

Files: src/qtgui/ctk/*
Copyright: Kitware Inc.
License: Apache-2.0

Files: src/dsp/afsk1200/*
Copyright: Copyright (C) 1996 Thomas Sailer, Copyright (C) 2011 Alexandru Csete
License: GPL-2+

Files: src/dsp/agc_impl.cpp src/qtgui/meter.cpp src/qtgui/freqctrl.cpp
 src/qtgui/plotter.cpp src/qtgui/meter.h
Copyright: Copyright 2010 Moe Wheatley
License: MOEBSD

Files: src/dsp/rx_demod_am.*
Copyright:  2011 Alexandru Csete OZ9AEC, 2013 Vesa Solonen OH2JCP.
License: GPL-3+

Files: src/dsp/rx_noise_blanker_cc.*
Copyright: 2004-2008 by Frank Brickle, AB2KT and Bob McGwier, N4HY
           2011-2012 Alexandru Csete OZ9AEC
License: GPL-3+

Files: resources/icons/*
Copyright: 2005-2009 freedesktop.org
Comment: derived from freedesktop.org's Tango Desktop Theme
License: Tango-PD

Files: COPYING
Copyright: 1989, 1991 Free Software Foundation, Inc.
License: COPYING
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

Files: LICENSE-CTK
Copyright: 2004 Apache
License: LICENSE-CTK
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

Files: src/dsp/rds/*
Copyright: 2013 Bastian Bloessl <bloessl@ccs-labs.org>
License: GPL-3+

License: Tango-PD
  In the preparation of the 0.8.90 release Novell took care of tracking
  down all the contributors to get them to relicense their artwork
  into Public Domain.
 .
  The COPYING file of the tarball states the following:
  | The icons in this repository are herefore released into the Public Domain.
 .
  Additionally the copyright status of the files was tracked in the CVS and the
  rdf properties of the SVGs adjusted for all files that were put into Public
  Domain (see rdf:about and rdf:resource).  Both fields contain a link to the
  Creative Commons Public Domain Dediciation[0] as reproduced below:
  | Public Domain Dedication
  |
  | Copyright-Only Dedication (based on United States law) or Public Domain
  | Certification
  |
  | The person or persons who have associated work with this document (the
  | "Dedicator" or "Certifier") hereby either (a) certifies that, to the best
  | of his knowledge, the work of authorship identified is in the public
  | domain of the country from which the work is published, or (b)
  | hereby dedicates whatever copyright the dedicators holds in the work
  | of authorship identified below (the "Work") to the public domain. A
  | certifier, moreover, dedicates any copyright interest he may have in
  | the associated work, and for these purposes, is described as a
  | "dedicator" below.
  |
  | A certifier has taken reasonable steps to verify the copyright
  | status of this work. Certifier recognizes that his good faith efforts
  | may not shield him from liability if in fact the work certified is not
  | in the public domain.
  |
  | Dedicator makes this dedication for the benefit of the public at
  | large and to the detriment of the Dedicator's heirs and successors.
  | Dedicator intends this dedication to be an overt act of relinquishment
  | in perpetuity of all present and future rights under copyright law,
  | whether vested or contingent, in the Work. Dedicator understands that
  | such relinquishment of all rights includes the relinquishment of all
  | rights to enforce (by lawsuit or otherwise) those copyrights in the
  | Work.
  |
  | Dedicator recognizes that, once placed in the public domain, the Work
  | may be freely reproduced, distributed, transmitted, used, modified,
  | built upon, or otherwise exploited by anyone for any purpose, commercial
  | or non-commercial, and in any way, including by methods that have not
  | yet been invented or conceived.
  [0] http://creativecommons.org/licenses/publicdomain/

License: MOEBSD
  + + +   This Software is released under the "Simplified BSD License"  + + +
 Copyright 2010 Moe Wheatley. All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 .
    1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
        provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY Moe Wheatley ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Moe Wheatley OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 The views and conclusions contained in the software and documentation are those of the
 authors and should not be interpreted as representing official policies, either expressed
 or implied, of Moe Wheatley.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http:www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: Apache-2.0
 On Debian systems, the complete text of the Apache 2.0
 License can be found in `/usr/share/common-licenses/Apache-2.0'.

This is the Debian GNU/Linux package of gpredict.

Gnome Predict was written by:
Alexandru Csete, <alexc@ifa.au.dk>, programmer and project administrator.

Contributors:
- William J Beksi, KC2EXL, Map view.
- Dr. T.S. Kelso, the author original SGP4/SDP4 algorithms in FORTRAN and Pascal.
- Neoklis Kyriazis, 5B4AZ, conversion of the algorithms to C.
- Damon Chaplin is the author of GooCanvas, which is used extensively in gpredict
  see http://www.dachaplin.dsl.pipex.com/goocanvas/
- Maidenhead locator functions are based on work done by Stephane Fillod,
  Nate Bargmann, Dave Hines, Mirko Caserta, and S. R. Sampson.

Copyright (C)  2001-2017  Alexandru Csete, OZ9AEC.

The Debian package was initially created by Hamish Moffatt
<hamish@debian.org> from sources obtained at:
http://sourceforge.net/projects/groundstation
Packaging updated by A. Maitland Bottoms <bottoms@debian.org> for
gpredict v2.0 from https://github.com/csete/gpredict.git
git archive --format=tar --prefix=gpredict-2.0/ v2.0  | xz > ../../gpredict_2.0.orig.tar.xz

This program is free software; you may redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This is distributed in the hope that it will be useful, but without
any warranty; without even the implied warranty of merchantability or
fitness for a particular purpose. See the GNU General Public License
for more details.

A copy of the GNU General Public License is available as
/usr/share/common-licenses/GPL in the Debian GNU/Linux distribution.

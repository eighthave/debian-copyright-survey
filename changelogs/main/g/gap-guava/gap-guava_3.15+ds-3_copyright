Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: guava package for GAP
Upstream-Contact: Joe Fields <fieldsj1@southernct.edu>
Source: http://www.gap-system.org/Packages/guava.html
X-Source: https://gap-packages.github.io/guava/
X-Source-Downloaded-From: https://github.com/gap-packages/guava/releases
X-Upstream-Bugs: https://github.com/gap-packages/guava/issues
Comment:
 The upstream source tarball is repacked to drop off the regenerated
 material, mainly the documentation, to substantially reduce the size.
Files-Excluded:
 src/leon/src/Makefile
 src/leon/src/leon_config.h.in
 src/leon/src/leon_config.h
 src/leon/install-sh
 src/leon/missing
 src/leon/aclocal.m4
 src/leon/config.guess
 src/leon/config.sub
 src/leon/Makefile
 src/leon/configure
 src/leon/doc/manual.dvi
 src/leon/doc/manual.pdf
 doc/chooser.html
 doc/*.css
 doc/*.js
 doc/chap*.txt
 doc/chap*.html
 doc/guava_bib.xml.bib
 doc/manual.six
 doc/manual.pdf

Files: *
Copyright: The GUAVA Group
 2009,2012,2018-2020 Joe Fields
 2007      Robert L. Miller
           Tom Boothby
 2004      David Joyner
           Cen Tjhai
           Jasper Cramwinckel
           Erik Roijackers
           Reinald Baart
           Eric Minkes
           Lea Ruscio
 1992-2003 Jasper Cramwinckel
           Erik Roijackers
           Reinald Baart
           Eric Minkes
           Lea Ruscio
           Jeffrey Leon
License: GPL-2+

Files: debian/*
Copyright:
 2014-2020 Jerome Benoit <calculus@rezozer.net>
 2008 Tim Abbott <tabbott@mit.edu>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

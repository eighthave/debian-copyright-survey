This is the Debian GNU/Linux prepackaged version of groundhog, a simple logic
game. Groundhog was written by Maurits Rijk.

This package was originally put together by Ruud de Rooij <ruud@debian.org>.
It was maintained by Stephen M Moraco <stephen@debian.org>.
It is maintained by Ying-Chun Liu (PaulLiu) <grandpaul@gmail.com>.

It was downloaded from http://home-2.consunet.nl/~cb007736/groundhog.html

Upstream Author: 

    Maurits Rijk <lpeek.mrijk@consunet.nl>

Copyright:

    Copyright (C) 1998 Maurits Rijk

License:

    Groundhog is Copyright (C) 1998 Maurits Rijk.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software Foundation,
    Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

On Debian GNU/Linux systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

There are files or directories which have a different copyright/license.
Listed as the following:

License for intl/*:
    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published
    by the Free Software Foundation; either version 2, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public 
    Licence along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
    02110-1301, USA.

    On Debian GNU/Linux systems, the complete text of the GNU Library
    General Public License may be found in
    /usr/share/common-licenses/LGPL-2

License for m4/{gettext.m4,lcmessage.m4,progtest.m4} :
    Copyright (C) 1995-2002 Free Software Foundation, Inc.
    This file is free software, distributed under the terms of the GNU
    General Public License.  As a special exception to the GNU General
    Public License, this file may be distributed as part of a program
    that contains a configuration script generated by Autoconf, under
    the same distribution terms as the rest of that program.

    This file can can be used in projects which are not available under
    the GNU General Public License or the GNU Library General Public
    License but which still want to provide support for the GNU gettext
    functionality.
    Please note that the actual code of the GNU gettext library is covered
    by the GNU Library General Public License, and the rest of the GNU
    gettext package package is covered by the GNU General Public License.
    They are *not* in the public domain.

License for other m4/*:
    Copyright (C) 2001-2002 Free Software Foundation, Inc.
    This file is free software, distributed under the terms of the GNU
    General Public License.  As a special exception to the GNU General
    Public License, this file may be distributed as part of a program
    that contains a configuration script generated by Autoconf, under
    the same distribution terms as the rest of that program.

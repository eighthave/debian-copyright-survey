Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gridsite
Source: https://github.com/CESNET/gridsite/archive/d51b2fd0601c966660a2784c381f2d44d78f53f4.tar.gz

Files: *
Copyright: 2002-2012, Andrew McNab, Shiv Kaushal, Joseph Dada and Yibiao Li,
           University of Manchester. All rights reserved.
License: BSD-2-clause

Files: src/gsexec.c src/gsexec.h
Copyright: 1999-2004 The Apache Software Foundation
License: Apache-2.0

Files: src/canl_mod_gridsite.c
Copyright: 2003-2007, Andrew McNab, Shiv Kaushal, Joseph Dada and Yibiao Li,
           University of Manchester. All rights reserved.
           2000-2005 The Apache Software Foundation or its licensors, as
           applicable.
License: BSD-2-clause and Apache-2.0

Files: src/canl_mod_ssl-private.h
Copyright: 2003-2008, Andrew McNab
           University of Manchester. All rights reserved.
           2000-2004 The Apache Software Foundation
License: BSD-2-clause and Apache-2.0

Files: build.xml project/gridsite.core.csf.xml project/properties.xml project/taskdefs.xml
Copyright: 2004 on behalf of the EU EGEE Project
           The European Organization for Nuclear Research (CERN)
           Istituto Nazionale di Fisica Nucleare (INFN), Italy
           Datamat Spa, Italy
           Centre National de la Recherche Scientifique (CNRS), France
           CS Systeme d'Information (CSSI), France
           Royal Institute of Technology, Center for Parallel Computers
            (KTH-PDC), Sweden
           Universiteit van Amsterdam (UvA), Netherlands
           University of Helsinki (UH.HIP), Finland
           University of Bergen (UiB), Norway
           Council for the Central Laboratory of the Research Councils
            (CCLRC), United Kingdom
License: Apache-2.0

Files: debian/*
Copyright: 2010-2017 Mattias Ellert <mattias.ellert@fysast.uu.se>
License: Apache-2.0

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the following
 conditions are met:
 .
   o Redistributions of source code must retain the above
     copyright notice, this list of conditions and the following
     disclaimer.
   o Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials
     provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems the full text of the Apache-2.0 license can be found in
 the /usr/share/common-licenses/Apache-2.0 file.

This is the Debian package of the GNATS bug monitoring system.

GNATS was originally packaged for Debian by Brian White <bcwhite@pobox.com>.
It had been maintained by Milan Zamazal <pdm@debian.org>, but it is now
maintained by Chad Walstrom <chewie@debian.org>.

The GNATS project home page is hosted at
http://savannah.gnu.org/projects/gnats.  The software can normally be obtained
via ftp://ftp.gnu.org/gnu/gnats/ or through the CVS repository at
http://savannah.gnu.org/cvs/?group=gnats.

Because of the 2003 crack of ftp.gnu.org, the current version was fetched from:
http://www.yngve.com/gnats/gnats-4.0.tar.gz

It's unclear who are the copyright holders of GNATS.

License:

  GNU GNATS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  GNU GNATS is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

The full text of GNU General Public License can be found under
/usr/share/common-licenses/GPL-2 on Debian systems.

The Debian control files in the `debian' directory of the source package were
mostly written by Brian White, Milan Zamazal, and Chad Walstrom.  They are
placed in public domain unless a particular file contains another copyright
notice.  You may use, modify and distribute their unmodified and/or modified
versions.  All the files are provided WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gitflow
Upstream-Contact: Peter van der Does <peter@avirtualhome.com>
Source: Upstream sources come from two repositories:
 git-flow: git://github.com/petervanderdoes/gitflow.git
 completion: git://github.com/petervanderdoes/git-flow-completion.git

Files: *
Copyright: Copyright 2010 Vincent Driessen,
 Copyright 2012-2019 Peter van der Does
License: BSD-2-clause

Files: gitflow-shFlags
Copyright: Copyright 2008 Kate Ward. All Rights Reserved.
License: LGPL-2.1

Files: completion/*
Copyright: Copyright (c) 2010 Justin Hileman,
 Copyright (C) 2012-2013 Peter van der Does
License: MIT

Files: debian/*
Copyright: Copyright (C) 2015- Laszlo Boszormenyi (GCS) <gcs@debian.org>,
 Copyright (C) 2011, 2013, 2014, 2015 Software Freedom Conservancy, Inc.
License: BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
 .
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY VINCENT DRIESSEN ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL VINCENT DRIESSEN OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1
 Copyright 2008 Kate Ward. All Rights Reserved.
 Released under the LGPL (GNU Lesser General Public License)
 .
 See /usr/share/common-licenses/LGPL-2.1 for the full text of the
 license.

License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

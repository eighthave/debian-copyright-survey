Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gwc
Upstream-Contact: Jeffrey J. Welty <jeff@redhawk.org>
Source: http://sourceforge.net/projects/gwc/files/
Copyright:
 2001-2006 Jeffrey J. Welty
 1985, 1987 by Stephen L. Moshier
 1993 David E. Stewart & Zbigniew Leyk, all rights reserved
 1997 David Mosberger
 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 1997 Tim Janik
 1998 Gregory McLean
 2004 Tom Szilagyi
 2007 robs@users.sourceforge.net
License: GPL-2+
Comment: The original tarball was repack to remove the following files:
 audio_alsa.c.bak
 icons/CVS/Entries
 icons/CVS/Repository
 icons/CVS/Root
 meschach/autom4te.cache/output.0
 meschach/autom4te.cache/requests
 meschach/autom4te.cache/traces.0
 meschach/config.cache
 meschach/config.log
 meschach/macheps
 meschach/maxint

Files: *
Copyright:
 2001-2006 Jeffrey J. Welty <jeff@redhawk.org>
 2004 Tom Szilagyi
License: GPL-2+

Files:
 gtkcurve.* gtkgamma.*
 gtkled.* gtkledbar.*
Copyright:
 1998 Gregory McLean
 1997 David Mosberger
 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 1997 Tim Janik
License: LGPL-2+

Files:
 mp3-duration.h
 sox/duration.h
Copyright:
 2007 robs@users.sourceforge.net
License: LGPL-2.1+

Files:
 chbevl.c
 i0.c
 i1.c
Copyright:
 1984-200 by Stephen L. Moshier
License: other-1

Files:
 err.h
 iter.h
 matrix.h
 matrix2.h
 meminfo.h
 sparse.h
 sparse2.h
 zmatrix.h
 zmaytrix2.h
 meschach/*
Copyright: 1993 David E. Stewart & Zbigniew Leyk, all rights reserved
License: other-2

Files: debian/*
Copyright:
 2011       Alessio Treglia <alessio@debian.org>
 2011       Jaromír Mikeš <mira.mikes@seznam.cz>
 2008-2011  Barry deFreese <bdefreese@debian.org>
 2004-2006  Helmut Grohne <helmut@subdivi.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Library General Public
 License can be found in `/usr/share/common-licenses/LGPL-2'.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 General Public License for more details.
Comment:
 On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: other-1
 Some software in this archive may be from the book _Methods and
 Programs for Mathematical Functions_ (Prentice-Hall, 1989) or
 from the Cephes Mathematical Library, a commercial product. In
 either event, it is copyrighted by the author.  What you see here
 may be used freely but it comes with no support or guarantee.

License: other-2
 This Meschach Library is provided "as is" without any express
 or implied warranty of any kind with respect to this software.
 In particular the authors shall not be liable for any direct,
 indirect, special, incidental or consequential damages arising
 in any way from use of the software.
 .
 Everyone is granted permission to copy, modify and redistribute this
 Meschach Library, provided:
  1.  All copies contain this copyright notice.
  2.  All modified copies shall carry a notice stating who
      made the last modification and the date of such modification.
  3.  No charge is made for this software or works derived from it.
      This clause shall not be construed as constraining other software
      distributed on the same medium as this software, nor is a
      distribution fee considered a charge.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ansiterm
Source: https://github.com/juju/ansiterm
Files-Excluded: vendor Godeps/_workspace

Files: *
Copyright: 2016 Canonical Ltd
License: LGPL-3.0-with-exception

Files: tabwriter/*
Copyright: 2009 The Go Authors
License: BSD-3-Clause
Comment: I notified upstream about missing a BSD
 license: https://github.com/juju/ansiterm/pull/6

Files: debian/*
Copyright: 2018 Alexandre Viau <aviau@debian.org>
License: LGPL-3.0-with-exception and BSD-3-Clause
Comment: Debian packaging is licensed under the same terms as upstream

License: LGPL-3.0-with-exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation; version 3 of the License.
 .
 On Debian systems, the complete text of the latest version of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 As a special exception to the GNU Lesser General Public License version 3
 ("LGPL3"), the copyright holders of this Library give you permission to
 convey to a third party a Combined Work that links statically or dynamically
 to this Library without providing any Minimal Corresponding Source or
 Minimal Application Code as set out in 4d or providing the installation
 information set out in section 4e, provided that you comply with the other
 provisions of LGPL3 and provided that you meet, for the Application the
 terms and conditions of the license(s) which apply to the Application.
 .
 Except as stated in this special exception, the provisions of LGPL3 will
 continue to comply in full to this Library. If you modify this Library, you
 may apply this exception to your version of this Library, but you are not
 obliged to do so. If you do not wish to do so, delete this exception
 statement from your version. This exception does not (and cannot) modify any
 license terms which apply to the Application, with which you must still
 comply.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
    * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the
 distribution.
    * Neither the name of Google Inc. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This package was debianized by Christian T. Steigies <cts@debian.org> on
Sat,  8 Jan 2005 11:31:38 +0100.

It was downloaded from glx.sf.net

Files: debian/*
Copyright: (c) 2005-2011, Christian T. Steigies <cts@debian.org>

Upstream Authors:
 A. S. Budden: programming (QGLE), packager for Arch Linux
 Andrey G. Grozin: packager for Gentoo Linux
 Axel Rohde: 3.3f-h versions (these were 32 bit DOS and OS/2 ports)
 Bryn Jeffries: programming (Linux)
 Chris Pugmire: original program creation and design
 Christoph Brendes: programming
 David Parfitt: documentation (GLE users guide)
 Edd Edmondson: packager for Mac OS/X
 Jan Struyf: programming (and current 4.x series maintainer)
 Laurence Abbott: programming
 Michal Vyskocil: packager for OpenSUSE
 Stephen Blundell: documentation (GLE users guide)
 Steve Wilkinson: programming (user interface)
 Terje Røsten: packager for Fedora Core
 Vincent LaBella: resurrected 3.3h to GLE 4.0 C++ code base
 Zbigniew Kisiel: testing

Files: *
Copyright: (c) 2009 Jan Struyf
License: Modified BSD
License for GLE (Modified BSD)
------------------------------

Copyright (C) 2009 GLE.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

   3. The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Files: src/gui/*
Copyright: (c) 2006 A. S. Budden & J. Struyf
License: GPL-2 or later

Files: src/gui/qgs.[cpp|h]
Copyright: (c) 2005 Piotr Szymanski
License: LGPL-2.1 or later

Files: contrib/editors/highlighting/gle-emacs.el
Copyright: (c) 2009 Kai Nordlund
License: GPL-2 or later

Files: platform/autoconf/config.[guess|sub]
Copyright: (c) 1992-2010 Free Software Foundation, Inc.
License: GPL-2 or later

Files: src/gui/ellipse.cpp
Copyright: (c) 2006 A. S. Budden, S. Wilkinson & J. Struyf
License: GPL-2 or later

Files: src/gui/gsinc/*
Copyright: (c) 2001-2006 artofcode LLC
License: GPL-2

Files: src/gui/qgslibloader.cpp
Copyright: (c) 2006 J. Struyf
License: GPL-2

Files: contrib/packagers/nsis/AddToPath.nsi
Copyright: (c)
License: ZLIB/LIBPNG License
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                                        ;;
;; * All NSIS source code, plug-ins, documentation, examples, header files and graphics,  ;;
;; with the exception of the compression modules and where otherwise noted, are licensed  ;;
;; under the zlib/libpng license.                                                         ;;
;;                                                                                        ;;
;; ZLIB/LIBPNG LICENSE                                                                    ;;
;; -------------------                                                                    ;;
;;                                                                                        ;;
;; This software is provided 'as-is', without any express or implied warranty. In no      ;;
;; event will the authors be held liable for any damages arising from the use of this     ;;
;; software.                                                                              ;;
;;                                                                                        ;;
;; Permission is granted to anyone to use this software for any purpose, including        ;;
;; commercial applications, and to alter it and redistribute it freely, subject to the    ;;
;; following restrictions:                                                                ;;
;;                                                                                        ;;
;; 1. The origin of this software must not be misrepresented; you must not claim that you ;;
;; wrote the original software. If you use this software in a product, an acknowledgment  ;;
;; in the product documentation would be appreciated but is not required.                 ;;
;;                                                                                        ;;
;; 2. Altered source versions must be plainly marked as such, and must not be             ;;
;; misrepresented as being the original software.                                         ;;
;;                                                                                        ;;
;; 3. This notice may not be removed or altered from any source distribution.             ;;
;;                                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Files: platform/autoconf/install-sh
Copyright: (c) 1994 X Consortium
License: X11 License
# This originates from X11R5 (mit/util/scripts/install.sh), which was
# later released in X11R6 (xc/config/util/install.sh) with the
# following copyright and license.
#
# Copyright (C) 1994 X Consortium
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
# AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
# TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name of the X Consortium shall not
# be used in advertising or otherwise to promote the sale, use or other deal-
# ings in this Software without prior written authorization from the X Consor-
# tium.
#
# FSF changes to this file are in the public domain.
#
# Calling this script install-sh is preferred over install.sh, to prevent
# `make' implicit rules from creating a file called install from it
# when there is no Makefile.
#
# This script is compatible with the BSD install script, but was written
# from scratch.


Files: src/fonts/Adobe-Core35_AFMs-314/
Copyright: (c) 1985-1997 Adobe Systems Incorporated
License:
# Adobe Core 35 AFM Files with 314 Glyph Entries - ReadMe
#
# This file and the 35 PostScript(R) AFM files it
# accompanies may be used, copied, and distributed for any purpose and
# without charge, with or without modification, provided that all
# copyright notices are retained; that the AFM files are not
# distributed without this file; that all modifications to this file
# or any of the AFM files are prominently noted in the modified
# file(s); and that this paragraph is not modified. Adobe Systems has
# no responsibility or obligation to support the use of the AFM
# files.

Files: src/makefmt/parseAFM.[cpp|h]
Copyright: (c) 1988, 1989, 1990 Adobe Systems Incorporated
License: Adobe License
*** Adobe License for parseAFM.{c,h} ***

* (C) 1988, 1989, 1990 by Adobe Systems Incorporated. All rights reserved.
*
* This file may be freely copied and redistributed as long as:
*   1) This entire notice continues to be included in the file,
*   2) If the file has been modified in any way, a notice of such
*      modification is conspicuously indicated.
*
* PostScript, Display PostScript, and Adobe are registered trademarks of
* Adobe Systems Incorporated.
*
* ************************************************************************
* THE INFORMATION BELOW IS FURNISHED AS IS, IS SUBJECT TO CHANGE WITHOUT
* NOTICE, AND SHOULD NOT BE CONSTRUED AS A COMMITMENT BY ADOBE SYSTEMS
* INCORPORATED. ADOBE SYSTEMS INCORPORATED ASSUMES NO RESPONSIBILITY OR
* LIABILITY FOR ANY ERRORS OR INACCURACIES, MAKES NO WARRANTY OF ANY
* KIND (EXPRESS, IMPLIED OR STATUTORY) WITH RESPECT TO THIS INFORMATION,
* AND EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR PARTICULAR PURPOSES AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
* ************************************************************************

Files: Makefile.in
Copyright: (c) 2006-2009 Remko Tronçon
License: MIT
* Copyright (c) 2006-2009 Remko Tronçon
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.


Files: src/gle/bitmap/lzwencode.cpp
Copyright (c) 1985, 1986 The Regents of the University of California
License:
* Rev 5.0 Lempel-Ziv & Welch Compression Support
* Based on the LZW compression code from libtiff <www.libtiff.org>
*
* Copyright (c) 1985, 1986 The Regents of the University of California.
* All rights reserved.
*
* This code is derived from software contributed to Berkeley by
* James A. Woods, derived from original work by Spencer Thomas
* and Joseph Orost.
*
* Redistribution and use in source and binary forms are permitted
* provided that the above copyright notice and this paragraph are
* duplicated in all such forms and that any documentation,
* advertising materials, and other materials related to such
* distribution and use acknowledge that the software was developed
* by the University of California, Berkeley.  The name of the
* University may not be used to endorse or promote products derived
* from this software without specific prior written permission.
* THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
* WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

Files: src/lib/electronics.gle
Copyright: (c) 2007 A. S. Budden
License:
! Electronic Circuit diagrams with GLE
! Author: A. S. Budden <abudden@NOSPAMgataki.co.uk>
!
! Date: 17th August 2007
! Version: 1.3
!
! Copyright:  Copyright (C) 2005 A. S. Budden
!             Permission is hereby granted to use and distribute this code,
!             with or without modifications, provided that this copyright
!             notice is copied with it. Like anything else that's free,
!             electronics.gle is provided *as is* and comes with no warranty
!             of any kind, either expressed or implied. By using this
!             plugin, you agree that in no event will the copyright
!             holder be liable for any damages resulting from the use
!             of this software.


Files: src/fonts/texcm*.[afm|gle]
Copyright:Prof. Donald E. Knuth
License: Modified BSD license
*** TeX Computer Modern Font AFM and GLE files ***

The following files were automatically generated based on the TeX Computer
Modern Font files, which were released by Prof. Donald E. Knuth into
the public domain.


On Debian GNU/Linux systems, the complete text of the GNU General
Public License version 2 can be found in `/usr/share/common-licenses/GPL-2',
and the complete text of the GNU Lesser General Public License version 2.1
can be found in `/usr/share/common-licenses/LGPL-2.1'.

This package was debianized by Michal Čihař <nijel@debian.org> on
Wed, 14 May 2008 10:00:34 +0200.

It was downloaded from http://geeqie.sourceforge.net/

Upstream Authors: 

    John Ellis <johne@verizon.net>  (Original author and maintainer of GQview)
    Vladimir Nadvornik <nadvornik at suse dot cz> (Maintainer of Geeqie)
    Petr Ostadal <postadal at suse dot cz>
    Laurent Monin <zas at norz dot org>

Copyright: 

    Copyright © 2004-2006 John Ellis
    Copyright © 2008-2012 The Geeqie Team

License:

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
 
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 
    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.


Files: src/md5-util.*
Copyright: Copyright © 1993 Branko Lankester
           Copyright © 1993 Colin Plumb
           Copyright © 1995 Erik Troan
License:

    This code is in the public domain; do with it what you wish.


Files: src/cellrenderericon.*
Copyright: Copyright © 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
License: 

    This package is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU Lesser General
Public License can be found in `/usr/share/common-licenses/LGPL-2'.

Files: doc/*
Copyright: Copyright © 2006 John Ellis
License:

   Permission is granted to copy, distribute and/or modify this document
   under the terms of the GNU Free Documentation License, Version 1.2 or any
   later version published by the Free Software Foundation; with no Invariant
   Sections, no Front-Cover Texts, and no Back-Cover Texts.

Even though OpenOffice.org document is mentioned as source, new upstream
uses HTML files as source.

On Debian systems, the complete text of the GNU Free Documentation
License can be found in `/usr/share/common-licenses/GFDL-1.2'.

Files: po/*.po
Copyright: see below
License:

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
		 
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
				  
    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

Detailed copyright holder report (were available):
po/ar.po: Copyright © 2004 John Ellis
po/be.po: Copyright © 2006 Pavel Piatruk <berserker@neolocation.com>
po/bg.po: Copyright © 2005 John Ellis, Vladimir Petrov <vladux@users.sourceforge.net>
po/ca.po: Copyright © 2003 Penbwrdd yn Gymraeg
po/cs.po: Copyright © 2001,2004,2005 Jan Raska <jan.raska@tiscali.cz>,
          Copyright © 2002 Aktualizace Michal Bukovjan <bukm@centrum.cz>
          Copyright © 2009 Vladimír Nádvorník <nadvornik@suse.cz>
po/da.po: Copyright © 2001 Birger Langkjer <birger.langkjer@image.dk>
po/de.po: Copyright © 1999 John Ellis, Matthias Warkus <mawa@iname.com>
          Copyright © 2005 Danny Milosavljevic
          Copyright © 2006 Ronny Steiner
          Copyright © 2009 Klaus Ethgen
po/eo.po: Copyright © 2006 Antonio C. Codazzi "la Filozofo" <f_sophia@libero.it>
po/es.po: Copyright © 2000 Rodrigo Sancho Senosiain <ruy_ikari@bigfoot.com>
          Copyright © 2003,2004,2005 Ariel Fermani <the_end@bbs.frc.utn.edu.ar>
po/et.po: Copyright © 2001,2002 Ilmar Kerm <ikerm@hot.ee>
po/eu.po: Copyright © 2006 Piarres Beobide <pi@beobide.net>
po/fi.po: Copyright © 2001-2006 Lauri Nurmi <lanurmi@iki.fi>
po/fr.po: Copyright © 2000 Jean-Pierre Pedron <jppedron@club-internet.fr>
          Copyright © 2003 Pascal Bleser <pascal.bleser@atosorigin.com>
          Copyright © 2003 Nicolas Boos <nicolas.boos@wanadoo.fr>
          Copyright © 2003-2004 Nicolas Bonifas <nicolas_bonifas@users.sf.net>
          Copyright © 2001-2002,2004-2005 Eric Lassauge <lassauge@users.sourceforge.net>
          Copyright © 2008 Laurent Monin <i18n@norz.org>
po/hu.po: Copyright © 2001 Mátyás Tibor <templar@tempi.scene.hu>
          Copyright © 2004 Koblinger Egmont <egmont@uhulinux.hu>
          Copyright © 2004 Sári Gábor <saga@externet.hu>
po/id.po: Copyright © 2003 John Ellis
          Copyright © 2003 Tedi Heriyanto <tedi_h@gmx.net>
po/it.po: Copyright © 2006 Kostantino <ciclope10ATalice.it>
          Copyright © 2000-2001 Christopher R. Gabriel <cgabriel@pluto.linux.it>
po/ja.po: Copyright © 2001 SATO Satoru <ss@gnome.gr.jp>
          Copyright © 2002 Junichi Uekawa <dancer@debian.org>
          Copyright © 2000-2005 Takeshi Aihana <aihana@gnome.gr.jp>
po/ko.po: Copyright © 2006 Hyun-Jin Moon <moonhyunjin@gmail.com>
po/nb.po: Copyright © 2001 Torgeir Ness Sundli <torgeir@mp3bil.no>
po/nl.po: Copyright © 2001 Hette J Visser <H.J.Visser@harrie.mine.nu>
          Copyright © 2003,2004,2005 Tino Meinen <a.t.meinen@chello.nl>
po/pl.po: Copyright © 2004 Wit Wiliński <madman@linux.bydg.org>
po/pt_BR.po: Copyright © 2001 Guilherme M. Schroeder <slump@linuxall.org>
             Copyright © 2005 Herval Ribeiro de Azevêdo <heraze@gmail.com>
po/ro.po: Copyright © 2002 Harald Ersch <hersch@romatsa.ro>
          Copyright © 2004 Harald Ersch <harald@ersch.ro>
po/ru.po: Copyright © 1999 Oleg Andryuschenko <oandr@itek.com.ua>
          Copyright © 2000 Michael Bravo <mbravo@acm.org>
          Copyright © 2001 drF_ckoff <dfo@antex.ru>
          Copyright © 2002 Oleg Andryuschenko <oandr@itek.com.ua>
          Copyright © 2003-2004 drF_ckoff <dfo@antex.ru>
          Copyright © 2005 Vitaly Lipatov <lav@altlinux.ru>
po/sk.po: Copyright © 2000 Martin Pekar <cortex@nextra.sk>
          Copyright © 2001,2004 Ivan Priesol <priesol@iris-sk.sk>
po/sl.po: Copyright © 2001 Matej Erman <matej.erman@guest.arnes.si>
po/sv.po: Copyright © 2003 John Ellis
          Copyright © 2003-2005 Hans Öfverbeck <hans.ofverbeck@home.se>
po/th.po: Copyright © 2002 Phanumas Thanyaboon <maskung@hotmail.com>
po/tr.po: Copyright © 1999-2000 Fatih Demir <kabalak@gmx.net>
po/uk.po: Copyright © 2001,2002 Volodymyr M. Lisivka <lvm@mystery.lviv.net>
          Copyright © 2002 Olexander Kunytsa
po/vi.po: Copyright © 2002 pclouds <pclouds@vnlinux.org>
po/zh_CN.GB2312.po: Copyright © 2001 Wu Yulun <migr@operamail.com>
po/zh_TW.po: Copyright © 2000 Kam Tik <kamtik@hongkong.com>
             Copyright © 2001 Abel Cheung <deaddog@deaddog.ws>

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

The Debian packaging is Copyright © 2008-2009, Michal Čihař
<nijel@debian.org> and is licensed under the GPL, see above.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: guix
Source: https://ftp.gnu.org/gnu/guix/

Files: *
Copyright:
 1999-2018 The OpenSSL Project Authors.
 2005-2009 Michel de Boer <michel@twinklephone.com>
 2006-2014 Eelco Dolstra <e.dolstra@tudelft.nl>
 2010-2021 Ludovic Courtès <ludo@gnu.org>
 2012 Stefan Handschuh <handschuh.stefan@googlemail.com>
 2012-2014 Nikita Karetnikov <nikita@karetnikov.org>
 2012-2017 Cyril Roelandt <tipecaml@gmail.com>
 2012-2018 Free Software Foundation, Inc.
 2012-2020 Andreas Enge <andreas@enge.fr>
 2013-2014 Joshua Grant <tadni@riseup.net>
 2013-2015 Aljosha Papsch <misc@rpapsch.de>
 2013-2017 John Darrington <jmd@gnu.org>
 2013-2018 David Thompson <dthompson2@worcester.edu>
 2013-2019 the authors of Guix
 2013-2020 Eric Bavier <bavier@member.fsf.org>
 2013-2021 Mark H Weaver <mhw@netris.org>
 2014 Deck Pickard <deck.r.pickard@gmail.com>
 2014 Ian Denhardt <ian@zenhack.net>
 2014 Kevin Lemonnier <lemonnierk@ulrar.net>
 2014 Marek Benc <merkur32@gmail.com>
 2014 Mathieu Lirzin <mathieu.lirzin@openmailbox.org>
 2014 Sree Harsha Totakura <sreeharsha@totakura.in>
 2014 Sylvain Beucler <beuc@beuc.net>
 2014-2015 Cyrill Schenkel <cyrill.schenkel@gmail.com>
 2014-2015 Tomáš Čech <sleep_walker@suse.cz>
 2014-2016 Raimon Grau <raimonster@gmail.com>
 2014-2016 Taylan Ulrich Bayırlı/Kammer <taylanbayirli@gmail.com>
 2014-2017 Federico Beffa <beffa@fbengineering.ch>
 2014-2018 David Thompson <davet@gnu.org>
 2014-2018 Manolis Fragkiskos Ragkousis <manolis837@gmail.com>
 2014-2018 Sou Bunnbu <iyzsong@gmail.com>
 2014-2019 Alex Kost <alezost@gmail.com>
 2014-2020 Julien Lepiller <julien@lepiller.eu>
 2014-2020 Pjotr Prins <pjotr.guix@thebird.nl>
 2014-2021 Ricardo Wurmus <rekado@elephly.net>
 2015 Alex Sassmannshausen <alex.sassmannshausen@gmail.com>
 2015 Alexander I.Grafov <grafov@gmail.com>
 2015 Claes Wallin <claes.wallin@greatsinodevelopment.com>
 2015 Daniel Pimentel <d4n1@member.fsf.org>
 2015 David Hashe <david.hashe@dhashe.com>
 2015 Dmitry Bogatov <KAction@gnu.org>
 2015 Eric Dvorsak <eric@dvorsak.fr>
 2015 Jeff Mickey <j@codemac.net>
 2015 Joshua S. Grant <jgrant@parenthetical.io>
 2015 Kai-Chung Yan <seamlikok@gmail.com>
 2015 Omar Radwan <toxemicsquire4@gmail.com>
 2015 Paul van der Walt <paul@denknerd.org>
 2015 Pierre-Antoine Rault <par@rigelk.eu>
 2015 Siniša Biđin <sinisa@bidin.eu>
 2015 Vicente Vera Parra <vicentemvp@gmail.com>
 2015 xd1le <elisp.vim@gmail.com>
 2015-2016 Erik Edrosa <erik.edrosa@gmail.com>
 2015-2016 Fabian Harfert <fhmgufs@web.de>
 2015-2016 Florian Paul Schmidt <mista.tapas@gmx.net>
 2015-2016 Steve Sprang <scs@stevesprang.com>
 2015-2017 Andy Patterson <ajpatter@uwaterloo.ca>
 2015-2017 Andy Wingo <wingo@igalia.com>
 2015-2017 Andy Wingo <wingo@pobox.com>
 2015-2017 Christopher Allan Webber <cwebber@dustycloud.org>
 2015-2018 Amirouche Boubekki <amirouche@hypermove.net>
 2015-2018 Ben Woodcroft <donttrustben@gmail.com>
 2015-2018 Christopher Lemmer Webber <cwebber@dustycloud.org>
 2015-2018 Mathieu Lirzin <mthl@gnu.org>
 2015-2018 Stefan Reichör <stefan@xsteve.at>
 2015-2019 Alex Vong <alexvong1995@gmail.com>
 2015-2021 Chris Marusich <cmmarusich@gmail.com>
 2015-2021 Efraim Flashner <efraim@flashner.co.il>
 2015-2020 Kyle Meyer <kyle@kyleam.com>
 2015-2020 Leo Famulari <leo@famulari.name>
 2015-2020 Roel Janssen <roel@gnu.org>
 2016 Al McElrath <hello@yrns.org>
 2016 Albin Söderqvist <albin@fripost.org>
 2016 Bake Timmons <b3timmons@speedymail.org>
 2016 Benz Schenk <benz.schenk@uzh.ch>
 2016 Carlos Sánchez de La Lama <csanchezdll@gmail.com>
 2016 Christopher Andersson <christopher@8bits.nu>
 2016 Daniel Pimentel <d4n1@d4n1.org>
 2016 Dennis Mungai <dmngaie@gmail.com>
 2016 Dmitry Nikolaev <cameltheman@gmail.com>
 2016 Dylan Jeffers <sapientech@sapientech@openmailbox.org>
 2016 Eric Le Bihan <eric.le.bihan.dev@free.fr>
 2016 Francesco Frassinelli <fraph24@gmail.com>
 2016 Ivan Vilata i Balaguer <ivan@selidor.net>
 2016 Jessica Tallon <tsyesika@tsyesika.se>
 2016 Jochem Raat <jchmrt@riseup.net>
 2016 John J. Foerch <jjfoerch@earthlink.net>
 2016 Jookia <166291@gmail.com>
 2016 Lukas Gradl <lgradl@openmailbox.org>
 2016 Matthew Jordan <matthewjordandevops@yandex.com>
 2016 Mckinley Olsen <mck.olsen@gmail.com>
 2016 Patrick Hetu <patrick.hetu@auf.org>
 2016 Peter Feigl <peter.feigl@nexoid.at>
 2016 Raymond Nicholson <rain1@openmailbox.org>
 2016 Steve Webber <webber.sl@gmail.com>
 2016 Toni Reina <areina@riseup.net>
 2016 doncatnip <gnopap@gmail.com>
 2016-2017 Adonay "adfeno" Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno@openmailbox.org>
 2016-2017 Alex Sassmannshausen <alex@pompo.co>
 2016-2017 David Craven <david@craven.ch>
 2016-2017 Jelle Licht <jlicht@fsfe.org>
 2016-2017 José Miguel Sánchez García <jmi2k@openmailbox.org>
 2016-2017 Petter <petter@mykolab.ch>
 2016-2017 Rene Saavedra <rennes@openmailbox.org>
 2016-2017 Rodger Fox <thylakoid@openmailbox.org>
 2016-2017 Thomas Danckaert <post@thomasdanckaert.be>
 2016-2017 Troy Sankey <sankeytms@gmail.com>
 2016-2018 Adriano Peluso <catonano@gmail.com>
 2016-2018 Carlo Zancanaro <carlo@zancanaro.id.au>
 2016-2018 Danny Milosavljevic <dannym+a@scratchpost.org>
 2016-2018 Julian Graham <joolean@gmail.com>
 2016-2018 Mike Gerwitz <mtg@gnu.org>
 2016-2018 Raoul Bonnal <ilpuccio.febo@gmail.com>
 2016-2018 Theodoros Foradis <theodoros@foradis.org>
 2016-2019 Clément Lassieur <clement@lassieur.org>
 2016-2020 Alex Griffin <a@ajgrf.com>
 2016-2020 Alex ter Weele <alex.ter.weele@gmail.com>
 2016-2020 Arun Isaac <arunisaac@systemreboot.net>
 2016-2020 Christopher Baines <mail@cbaines.net>
 2016-2020 Danny Milosavljevic <dannym@scratchpost.org>
 2016-2020 Hartmut Goebel <h.goebel@crazy-compilers.com>
 2016-2020 Jan Nieuwenhuizen <janneke@gnu.org>
 2016-2020 Kei Kebreau <kkebreau@posteo.net>
 2016-2020 Marius Bakke <mbakke@fastmail.com>
 2016-2020 Nicolas Goaziou <mail@nicolasgoaziou.fr>
 2016-2020 Nikita <nikita@n0.is>
 2016-2020 Rene Saavedra <pacoon@protonmail.com>
 2016-2020 Tobias Geerinckx-Rice <me@tobias.gr>
 2016-2020 Vincent Legoll <vincent.legoll@gmail.com>
 2017 Adonay "adfeno" Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno@hyperbola.info>
 2017 Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno@hyperbola.info>
 2017 André <eu@euandre.org>
 2017 Ben Sturmfels <ben@sturm.com.au>
 2017 Corentin Bocquillon <corentin@nybble.fr>
 2017 Dave Love <fx@gnu.org>
 2017 Dave Love <me@fx@gnu.org>
 2017 Eric Bavier <bavier@cray.com>
 2017 Ethan R. Jones <doubleplusgood23@gmail.com>
 2017 Feng Shu <tumashu@163.com>
 2017 Frederick M. Muriithi <fredmanglis@gmail.com>
 2017 Frederick Muriithi <fredmanglis@gmail.com>
 2017 George Clemmer <myglc2@gmail.com>
 2017 Gregor Giesen <giesen@zaehlwerk.net>
 2017 Huang Ying <huang.ying.caritas@gmail.com>
 2017 Kei Kebreau <address@hidden>
 2017 Kristofer Buffington <kristoferbuffington@gmail.com>
 2017 Marek Benc <dusxmt@gmx.com>
 2017 Mekeor Melire <mekeor.melire@gmail.com>
 2017 Mohammed Sadiq <sadiq@sadiqpk.org>
 2017 Muriithi Frederick Muriuki <fredmanglis@gmail.com>
 2017 Peter Mikkelsen <petermikkelsen10@gmail.com>
 2017 Quiliro <quiliro@fsfla.org>
 2017 Raoul J.P. Bonnal <ilpuccio.febo@gmail.com>
 2017 Ryan Moe <ryan.moe@gmail.com>
 2017 Sergei Trofimovich <slyfox@inbox.ru>
 2017 Z. Ren <zren@dlut.edu.cn>
 2017 nee <nee-git@hidamari.blue>
 2017 nee <nee.git@cock.li>
 2017 rsiddharth <s@ricketyspace.net>
 2017 sharlatan <sharlatanus@gmail.com>
 2017-2018 Brendan Tildesley <brendan.tildesley@openmailbox.org>
 2017-2018 Feng Shu <tumashu@163.com>
 2017-2018 Mark Meyer <mark@ofosos.org>
 2017-2018 Nikolai Merinov <nikolai.merinov@member.fsf.org>
 2017-2018 Vasile Dumitrascu <va511e@yahoo.com>
 2017-2019 Caleb Ristvedt <caleb.ristvedt@cune.org>
 2017-2019 Gábor Boskovits <boskovits@gmail.com>
 2017-2019 Rutger Helling <rhelling@mykolab.com>
 2017-2019 nee <nee-git@hidamari.blue>
 2017-2021 Björn Höfling <bjoern.hoefling@bjoernhoefling.de>
 2017-2020 Brendan Tildesley <mail@brendan.scot>
 2017-2020 Jonathan Brielmaier <jonathan.brielmaier@web.de>
 2017-2020 Mathieu Othacehe <m.othacehe@gmail.com>
 2017-2021 Maxim Cournoyer <maxim.cournoyer@gmail.com>
 2017-2021 Oleg Pykhalov <go.wigust@gmail.com>
 2017-2020 Paul Garlick <pgarlick@tourbillion-technology.com>
 2017-2020 Pierre Langlois <pierre.langlois@gmx.com>
 2018 Adam Massmann <massmannak@gmail.com>
 2018 Adam Van Ymeren <adam@vany.ca>
 2018 Alex Branham <alex.branham@gmail.com>
 2018 Benjamin Slade <slade@jnanam.net>
 2018 Eric Brown <brown@fastmail.com>
 2018 Ethan R. Jones <ethanrjones97@gmail.com
 2018 Fis Trivial <ybbs.daans@hotmail.com>
 2018 Gábor Boskovit <boskovits@gmail.com>
 2018 Ison111 <ison111@protonmail.com>
 2018 Joshua Sierles, Nextjournal <joshua@nextjournal.com>
 2018 Laura Lazzati <laura.lazzati.15@gmail.com>
 2018 Lprndn <guix@lprndn.info>
 2018 Madalin Ionel-Patrascu <madalinionel.patrascu@mdc-berlin.de>
 2018 Manuel Graf <graf@init.at>
 2018 Nadya Voronova <voronovank@gmail.com>
 2018 Nam Nguyen <namn@berkeley.edu>
 2018 Peter Kreye <kreyepr@gmail.com>
 2018 Pierre-Antoine Rou <contact@parou.fr>
 2018 Pierre-Antoine Rou <pierre-antoine.rou@inria.fr>
 2018 Raoul Jean Pierre Bonnal <ilpuccio.febo@gmail.com>
 2018 Robin Templeton <robin@igalia.com>
 2018 Sahithi Yarlagadda <sahi@swecha.net>
 2018 Sandeep Subramanian <sandeepsubramanian94@gmail.com>
 2018 Sohom Bhattacharjee <soham.bhattacharjee15@gmail.com>
 2018 Stefan Stefanović <stefanx2ovic@gmail.com>
 2018 Thomas Sigurdsen <tonton@riseup.net>
 2018 Tonton <tonton@riseup.net>
 2018 Vijayalakshmi Vedantham <vijimay12@gmail.com>
 2018 nee <nee.git@hidamari.blue>
 2018 okapi <okapi@firemail.cc>
 2018-2019 Amin Bandali <bandali@gnu.org>
 2018-2019 Brett Gilio <brettg@posteo.net>
 2018-2019 Gabriel Hondet <gabrielhondet@gmail.com>
 2018-2019 Jovany Leandro G.C <bit4bit@riseup.net>
 2018-2019 Meiyo Peng <meiyo.peng@gmail.com>
 2018-2019 Meiyo Peng <meiyo@disroot.org>
 2018-2019 Meiyo Peng <meiyo@riseup.net>
 2018-2019 Timo Eisenmann <eisenmann@fn.de>
 2018-2020 Charlie Ritter <chewzeirta@posteo.net>
 2018-2020 Jack Hill <jackhill@jackhill.us>
 2018-2020 Katherine Cox-Buday <cox.katherine.e@gmail.com>
 2018-2020 Konrad Hinsen <konrad.hinsen@fastmail.net>
 2018-2020 Marius Bakke <marius@gnu.org>
 2018-2020 Mădălin Ionel Patrașcu <madalinionel.patrascu@mdc-berlin.de>
 2018-2020 Nicolò Balzarotti <nicolo@nixo.xyz>
 2018-2020 Peng Mei Yu <i@pengmeiyu.com>
 2018-2020 Pierre Neidhardt <mail@ambrevar.xyz>
 2018-2020 Tim Gesthuizen <tim.gesthuizen@yahoo.de>
 2018-2020 Timothy Sample <samplet@ngyro.com>
 2018-2020 Vagrant Cascadian <vagrant@debian.org>
 2019 Andrew Miloradovsky <andrew@interpretmath.pw>
 2019 Arne Babenhauserheide <arne_bab@web.de>
 2019 Dan Frumin <dfrumin@cs.ru.nl>
 2019 Giovanni Biscuolo <g@xelera.eu>
 2019 Guy Fleury Iteriteka <hoonandon@gmail.com>
 2019 Ivan Petkov <ivanppetkov@gmail.com>
 2019 Jesse Gildersleve <jessejohngildersleve@protonmail.com>
 2019 Léo Le Bouter <lle-bout@zaclys.net>
 2019 Nicolò Balzarotti <anothersms@gmail.com>
 2019 Pierre-Moana Levesque <pierre.moana.levesque@gmail.com>
 2019 Pkill -9 <pkill9@runbox.com>
 2019 Taylan Kammer <taylan.kammer@gmail.com>
 2019 Tim Stahel <swedneck@swedneck.xyz>
 2019 Tobias Geerinck-Rice <me@tobias.gr>
 2019 Vagrant Cascadian <vagrant@reproducible-builds.org>
 2019 Yoshinori Arai <kumagusu08@gmail.com>
 2019 nee <nee@cock.li>
 2019 swedebugia <swedebugia@riseup.net>
 2019-2020 Adrian Malacoda <malacoda@monarch-pass.net>
 2019-2020 Christopher Howard <christopher@librehacker.com>
 2019-2020 Evan Straw <evan.straw99@gmail.com>
 2019-2020 Giacomo Leidi <goodoldpaul@autistici.org>
 2019-2020 Guillaume Le Vaillant <glv@posteo.net>
 2019-2020 Guy Fleury Iteriteka <gfleury@disroot.org>
 2019-2020 Jan Wielkiewicz <tona_kosmicznego_smiecia@interia.pl>
 2019-2020 Jens Mølgaard <jens@zete.tk>
 2019-2020 John Soo <jsoo1@asu.edu>
 2019-2020 Josh Holland <josh@inv.alid.pw>
 2019-2020 L  p R n  d n <guix@lprndn.info>
 2019-2020 Leo Prikler <leo.prikler@student.tugraz.at>
 2019-2020 Martin Becze <mjbecze@riseup.net>
 2019-2020 Miguel Ángel Arruga Vivas <rosen644835@gmail.com>
 2019-2020 Timotej Lazar <timotej.lazar@araneo.si>
 2019-2020 Wiktor Żelazny <wzelazny@vurv.cz>
 2020 Adam Kandur <rndd@tuta.io>
 2020 Alex McGrath <amk@amk.ie>
 2020 Alexandros Theodotou <alex@zrythm.org>
 2020 Alexandru-Sergiu Marton <brown121407@posteo.ro>
 2020 Alexey Abramov <levenson@mmer.org>
 2020 Anders Thuné <asse.97@gmail.com>
 2020 André Batista <nandre@riseup.net>
 2020 Antoine Côté <antoine.cote@posteo.net>
 2020 Arthur Margerit <ruhtra.mar@gmail.com>
 2020 B. Wilson <elaexuotee@wilsonb.com>
 2020 Boris A. Dekshteyn <boris.dekshteyn@gmail.com>
 2020 Boris A. Dekshteyn <harlequin78@gmail.com>
 2020 Brett Gilio <brettg@gnu.org>
 2020-2021 Brice Waegeneire <brice@waegenei.re>
 2020 Dale Mellor <guix-devel-0brg6b@rdmp.org>
 2020 Edouard Klein <edk@beaver-labs.com>
 2020 Ekaitz Zarraga <ekaitz@elenq.tech>
 2020 Eric Bavier <bavier@posteo.net>
 2020 Eric Brown <ecbrown@ericcbrown.com>
 2020 EuAndreh <eu@euandre.org>
 2020 Fredrik Salomonsson <plattfot@gmail.com>
 2020 Gabriel Arazas <foo.dogsquared@gmail.com>
 2020 Google LLC
 2020 Greg Hogan <code@greghogan.com>
 2020 Hamzeh Nasajpour <h.nasajpour@pantherx.org>
 2020 Helio Machado <0x2b3bfa0+guix@googlemail.com>
 2020 Hendur Saga <hendursaga@yahoo.com>
 2020 Holger Peters <holger.peters@posteo.de>
 2020 Ivan Kozlov <kanichos@yandex.ru>
 2020 Jakub Kądziołka <kuba@kadziolka.net>
 2020 James Smith <jsubuntuxp@disroot.org>
 2020 Jean-Baptiste Note <jean-baptiste.note@m4x.org>
 2020 Jesse Dowell <jessedowell@gmail.com>
 2020 Jesse Gibbons <jgibbons2357+guix@gmail.com>
 2020 Jesse Gibbons <jgibbons2357@gmail.com>
 2020 Jonathan Frederickson <jonathan@terracrypt.net>
 2020 Josh Marshall <joshua.r.marshall.1991@gmail.com>
 2020 Julien Lepiler <julien@lepiller.eu>
 2020 Justus Winter <justus@sequoia-pgp.org>
 2020 Lars-Dominik Braun <lars@6xq.net>
 2020 Lars-Dominik Braun <ldb@leibniz-psychology.org>
 2020 Magali Lemes <magalilemes00@gmail.com>
 2020 Malte Frank Gerdes <malte.f.gerdes@gmail.com>
 2020 Marcin Karpezo <sirmacik@wioo.waw.pl>
 2020 Masaya Tojo <masaya@tojo.tokyo>
 2020 Mason Hock <chaosmonk@riseup.net>
 2020 Mathieu Othacehe <othacehe@gnu.org>
 2020 Michael Rohleder <mike@rohleder.de>
 2020 Mike Rosset <mike.rosset@gmail.com>
 2020 Morgan Smith <Morgan.J.Smith@outlook.com>
 2020 Nicolas Goaziou <mail@nicolasgoaziou.com>
 2020 Noisytoot <noisytoot@gmail.com>
 2020 Peng Mei Yu <pengmeiyu@riseup.net>
 2020 Peter Lo <peterloleungyau@gmail.com>
 2020 Prafulla Giri <pratheblackdiamond@gmail.com>
 2020 Rafael Luque Leiva <rafael.luque@osoco.es>
 2020 Raghav Gururajan <raghavgururajan@disroot.org>
 2020 Reza Alizadeh Majd <r.majd@pantherx.org>
 2020 Robin Green <greenrd@greenrd.org>
 2020 Ryan Prior <rprior@protonmail.com>
 2020 Sergey Trofimov <sarg@sarg.org.ru>
 2020 Simen Endsjø <simendsjo@gmail.com>
 2020 Simon South <simon@simonsouth.net>
 2020 Simon Tournier <zimon.toutoune@gmail.com>
 2020 Stefan <stefan-guix@vodafonemail.de>
 2020 Tanguy Le Carrour <tanguy@bioneland.org>
 2020 Tim Howes <timhowes@lavabit.com>
 2020 Tim Van den Langenbergh <tmt_vdl@gmx.com>
 2020 Tom Zander <tomz@freedommail.ch>
 2020 TomZ <tomz@freedommail.ch>
 2020 Valentin Ignatev <valentignatev@gmail.com>
 2020 Vinicius Monego <monego@posteo.net>
 2020 Yuval Kogman <nothingmuch@woobling.org>
 2020 Zhu Zihao <all_but_last@163.com>
 2020 by Amar M. Singh <nly@disroot.org>
 2020 pinoaffe <pinoaffe@airmail.cc>
 2020 pukkamustard <pukkamustard@posteo.net>
 2020 raingloom <raingloom@riseup.net>
 2020 Daniel Brooks <db48x@db48x.net>
 2020-2021 Felix Gruber <felgru@posteo.net>
 2021 Philip McGrath <philip@philipmcgrath.com>
 2021 the Guix authors
License:   GPL-3.0+

Files:
 nix/boost/format/exceptions.hpp
 nix/boost/format/format_class.hpp
 nix/boost/format/format_fwd.hpp
 nix/boost/format/free_funcs.cc
 nix/boost/format/internals_fwd.hpp
 nix/boost/format/macros_default.hpp
 nix/boost/format.hpp
 nix/boost/format/feed_args.hpp
 nix/boost/format/format_implementation.cc
 nix/boost/format/group.hpp
 nix/boost/format/internals.hpp
Copyright: Samuel Krempp 2001 krempp@crans.ens-cachan.fr
License:  PERMISSIVE-VARIANT2

Files: doc/*
Copyright:
 2012-2020 Ludovic Courtès
 2013 Nikita Karetnikov
 2013-2016 Andreas Enge
 2014 Pierre-Antoine Rault
 2014-2016 Alex Kost
 2015 Taylan Ulrich Bayırlı/Kammer
 2015-2016 Mathieu Lirzin
 2015-2017 Leo Famulari
 2015-2019 Ricardo Wurmus
 2016 Alex ter Weele
 2016 Ben Woodcroft
 2016 John Darrington
 2016-2017 Nikita Gillmann
 2016-2018 Chris Marusich
 2016-2019 Christopher Baines
 2016-2019 Efraim Flashner
 2016-2020 Jan Nieuwenhuizen
 2016-2020 Julien Lepiller
 2017 Andy Wingo
 2017 Christopher Allan Webber
 2017 Federico Beffa
 2017 George Clemmer
 2017 Hartmut Goebel
 2017 Maxim Cournoyer
 2017 Thomas Danckaert
 2017 humanitiesNerd
 2017 nee
 2017-2018 Carlo Zancanaro
 2017-2018 Clément Lassieur
 2017-2018 Marius Bakke
 2017-2018 Tobias Geerinckx-Rice
 2017-2020 Arun Isaac
 2017-2020 Mathieu Othacehe
 2018 Alex Vong
 2018 Gábor Boskovits
 2018 Laura Lazzati
 2018 Mike Gerwitz
 2018 Oleg Pykhalov
 2018 Pierre-Antoine Rou
 2018 Rutger Helling
 2018-2020 Florian Pelz
 2019 Efraim Flashner
 2019 Pierre Neidhardt
 2019 Ricardo Wurmus
 2019-2020 Diego Nicola Barbato
 2020 Alexandru-Sergiu Marton
 2020 André Batista
 2020 André Batista
 2020 Brice Waegeneire
 2020 Christopher Lemmer Webber
 2020 Marcin Karpezo
 2020 Matthew Brooks
 2020 Oleg Pykhalov
 2020 Pierre Langlois
 2020 R Veera Kumar
 2020 pinoaffe
 2020 raingloom
 2020 Daniel Brooks
License:   GFDL-1.3+

Files: doc/local.mk
Copyright:
 2012-2020 Ludovic Courtès <ludo@gnu.org>
 2013 Andreas Enge <andreas@enge.fr>
 2016 Eric Bavier <bavier@member.fsf.org>
 2016 Taylan Ulrich Bayırlı/Kammer <taylanbayirli@gmail.com>
 2016-2018 Mathieu Lirzin <mthl@gnu.org>
 2018 Julien Lepiller <julien@lepiller.eu>
License: GPL-3.0+

Files: gnu/packages/patches/audiofile-CVE-2015-7747.patch
Copyright: 2000 Silicon Graphics, Inc.
License: GPL-2.0+

Files: gnu/packages/patches/quassel-qt-514-compat.patch
Copyright: 2005-2020 by the Quassel Project
License: GPL-2or3

Files: gnu/packages/patches/rnp-add-version.cmake.patch
Copyright: 2018 Ribose Inc.
License: BSD-2-Clause

Files: gnu/packages/patches/glibc-2.27-git-fixes.patch
Copyright:
 1985-1993 The Regents of the University of California.
 2016-2017 Free Software Foundation, Inc.
License:   LGPL-2.1+

Files:
 po/guix/Makefile.in.in
 po/packages/Makefile.in.in
Copyright: 1995-2010 Ulrich Drepper <drepper@gnu.ai.mit.edu>
License:   PERMISSIVE

Files:     guix/base64.scm
Copyright: 2009-2010 Göran Weinholt <goran@weinholt.se>
License:   Expat and GPL-3.0+

Files:     gnu/packages/patches/pinentry-efl.patch
Copyright: 2017 Obsidian-Studios, Inc.
License:   GPL-2.0+

Files:     gnu/packages/patches/glibc-CVE-2018-11236.patch
Copyright: 2018 Free Software Foundation, Inc.
License:   LGPL-2.1+

Files:     gnu/packages/patches/glibc-versioned-locpath.patch
Copyright: 2015 Free Software Foundation, Inc.
License:   LGPL-2.1+

Files:     guix/elf.scm
Copyright: 2011-2015 Free Software Foundation, Inc.
License:   LGPL-3.0+

Files:     gnu/packages/patches/runc-CVE-2019-5736.patch
Copyright:
 2019 Aleksa Sarai <cyphar@cyphar.com>
 2019 SUSE LLC
License: Apache-2.0

Files: HACKING
 INSTALL
 NEWS
 ROADMAP
 TODO
 doc/htmlxref.cnf
Copyright:
 1994-2016 Free Software Foundation, Inc.
 2012-2019 Ludovic Courtès <ludo@gnu.org>
 2015-2017 Mathieu Lirzin <mthl@gnu.org>
 2016-2018 Ricardo Wurmus <rekado@elephly.net>
 2017 Arun Isaac <arunisaac@systemreboot.net>
 2017 Leo Famulari <leo@famulari.name>
 2019 Mathieu Othacehe <m.othacehe@gmail.com>
 2021 Maxim Cournoyer <maxim.cournoyer@gmail.com>
License:   PERMISSIVE-VARIANT1

Files:
 nix/boost/assert.hpp
 nix/boost/throw_exception.hpp
 nix/boost/format/parsing.cc
Copyright:
 2001 Samuel Krempp krempp@crans.ens-cachan.fr
 2001-2002 Peter Dimov and Multi Media Ltd.
 2002 Peter Dimov and Multi Media Ltd.
License:   PERMISSIVE-VARIANT2

Files:     d3.v3.js
Copyright: 2010-2016 Mike Bostock
License:   BSD-3-Clause

Files:     build-aux/texinfo.tex
Copyright: 1985-2018 Free Software Foundation, Inc.
License:   GPL-3.0+ with TeX exception

License:   GPL-3.0+
 This file is part of GNU Guix.
 .
 GNU Guix is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 GNU Guix is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License:   BSD-3-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 * Neither the name of the author nor the names of contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License:   GPL-3.0+ with TeX exception
 This texinfo.tex file is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 .
 This texinfo.tex file is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, when this file is read by TeX when processing
 a Texinfo source document, you may use the result without
 restriction. This Exception is an additional permission under section 7
 of the GNU General Public License, version 3 ("GPLv3").
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License:   GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
 copy of the license is included in the section entitled ``GNU Free
 Documentation License''.
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 Version 1.3 can be found in `/usr/share/common-licenses/GFDL-1.3'.

License:   LGPL-2.1+
 This file is part of the GNU C Library.
 .
 The GNU C Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 The GNU C Library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with the GNU C Library; if not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 Version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License:   PERMISSIVE
 This file can be copied and used freely without restrictions.  It can
 be used in projects which are not available under the GNU General Public
 License but which still want to provide support for the GNU gettext
 functionality.

License:   PERMISSIVE-VARIANT1
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.

License:   PERMISSIVE-VARIANT2
 Permission to copy, use, modify, sell and
 distribute this software is granted provided this copyright notice appears
 in all copies. This software is provided "as is" without express or implied
 warranty, and with no claim as to its suitability for any purpose.

License:   Expat
 This file incorporates work covered by the following copyright and
 permission notice:
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License:   Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License Version 2.0
 can be found in `/usr/share/common-licenses/Apache-2.0'.

License:   LGPL-3.0+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 Version 3 can be found in `/usr/share/common-licenses/LGPL-3'.

License:   GPL-2.0+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2or3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) version 3.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

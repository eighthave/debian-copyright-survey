Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnuais
Source: https://github.com/rubund/gnuais/ 

Files: *
Copyright: 1996 Thomas Sailer <sailer@ife.ee.ethz.ch>
           2001-2004 Tomi Manninen <tomi.manninen@hut.fi>
           2008-2014 Heikki Hannikainen <hessu@hes.iki.fi>
           2008-2013 Ruben Undheim <ruben.undheim@gmail.com>
License: GPL-2+

Files: src/gui/osd-utils.h src/gui/osm-gps-map-ais.c src/gui/osm-gps-map-ais.h
Copyright: 2010 John Stowers <john.stowers@gmail.com>
License: GPL-2

Files: src/gui/private.h
Copyright: 2008 Marcus Bauer <marcus.bauer@gmail.com>
           2009 John Stowers <john.stowers@gmail.com>
           2009 Everaldo Canuto <everaldo.canuto@gmail.com>
License: GPL-2

Files: src/crc32.c
Copyright: 1988 Rayan S. Zachariassen
           1999 Matti Aarnio <mea@nic.funet.fi>
License: Artistic or BSD-3-clause or GPL-2 or LGPL-2

Files: src/splay.c
Copyright: 1988 David Brower <daveb@rtech.uucp>
License: Artistic or BSD-3-clause or GPL-2 or LGPL-2

Files: src/spsymbol.c
Copyright: 1988 Rayan S. Zachariassen
License: Artistic or BSD-3-clause or GPL-2 or LGPL-2

Files: debian/*
Copyright: 2014 Ruben Undheim <ruben.undheim@gmail.com>
License: GPL-2+


License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: GPL-2
 This is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: Artistic
 This program is free software; you can redistribute it and/or modify it
 under the terms of the "Artistic License" which comes with Debian.
 .
 THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES
 OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in "/usr/share/common-licenses/Artistic".


License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


License: LGPL-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; version 2.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the 
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian Systems, the full text of the license can be found on
 /usr/share/common-licenses/LGPL-2

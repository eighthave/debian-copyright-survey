This is the debian package of the GNU Compiler Collection
compiled for cross development for the msp430 processor.
It was created by Luca Bruno <lucab@debian.org> from a patchset 
distributed by the mspgcc project and applied to the original 
GCC source packages.

msp430 original changes are available through git at
http://mspgcc.git.sourceforge.net/git/gitweb-index.cgi

Original GCC is available via the gcc-4.5-source package
or at http://ftp.gnu.org/pub/gnu/gcc/

mspgcc patches follow GCC original copyright, as declared by
the author clarifying the license of the patch-set:
http://thread.gmane.org/gmane.comp.hardware.texas-instruments.msp430.gcc.user/9161

"""
From: Peter Bigot <pab@peoplepowerco.com>
Subject: copyright on upstream patches
Date: Sun, 30 Jan 2011 18:30:34 -0600

In all the excitement of getting the new version out, I forgot I'd been
asked to provide a header on the patch files that apply to the upstream
distributions (binutils, gcc, gdb) explicitly noting that the material
therein was copyright Free Software Foundation and subject to licensing
restrictions of the upstream distribution (presumably GPL).  This is
consistent with everything I've been able to find in the source repositories
from CVS onward.
"""

GCC is Copyright © 1986-2011, Free Software Foundation, Inc.
The entire package is distributed under GPL-3+:

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA.

On Debian GNU/Linux systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

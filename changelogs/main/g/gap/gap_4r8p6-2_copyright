This package was debianized by Bill Allombert <ballombe@debian.org> on
Thu, 26 Sep 2002 20:53:12 +0200

--Previous GAP packages (not GPL)--
GAP 4r2 was packaged by Markus Hetzmannseder <hetzi@hetzi.at> on Wed, 29 Dec
1999 14:08:12 +0100.

GAP 3.4 was packaged by John Lapeyre <lapeyre@physics.arizona.edu> on
Wed, 21 Jan 1998 20:20:03 -0700.
-----------------------------------

It was downloaded from <ftp://ftp-gap.dcs.st-and.ac.uk/pub/gap/gap4/>
Website: <http://www.gap-system.org/>

Copyright: GPL

On Debian GNU/Linux systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL'.

Full copyright statement:

     Copyright copyright (1987--2013) by the GAP Group, 

     incorporating  the  Copyright  copyright  1999,  2000  by  School  of
Mathematical  and  Computational  Sciences, University of St Andrews, North
Haugh, St Andrews, Fife KY16 9SS, Scotland

     being  the  Copyright  copyright  1992  by Lehrstuhl D für
Mathematik,  RWTH, 52056 Aachen, Germany, transferred to St Andrews on July
21st, 1997.

     except for files in the distribution, which have an explicit different 
copyright  statement.  In particular, the copyright of packages distributed
with GAP is usually with the package authors or their institutions.

     
     

     GAP  is free software; you can redistribute it and/or modify it under
the  terms  of  the  GNU  General  Public  License as published by the Free
Software  Foundation;  either version 2 of the License, or (at your option)
any  later  version. For details, see the file `GPL' in the `etc' directory
of the GAP distribution or see URL <http://www.gnu.org/licenses/gpl.html>

     If you obtain GAP please send us a short notice to that effect, e.g., 
an  e-mail  message  to the address <gap@dcs.st-and.ac.uk>, containing
your  full  name and address. This allows us to keep track of the number of
GAP users.

     If  you  publish  a mathematical result that was partly obtained using
GAP,  please cite GAP, just as you would cite another paper that you used
(see  below  for  sample  citation).  Also we would appreciate if you could
inform us about such a paper.

     Specifically, please refer to

     [GAP]  The  GAP  Group,  GAP  ---  Groups,  Algorithms,  and
Programming, Version 4.6.5; 2013 (http://www.gap-system.org) 

     (Should  the  reference  style  require  full  addresses  please  use:
``Centre   for   Interdisciplinary   Research   in  Computational  Algebra,
University of St Andrews, North Haugh, St Andrews, Fife KY16 9SS, Scotland; 
Lehrstuhl  D  für Mathematik, Rheinisch Westfälische
Technische Hochschule, Aachen, Germany'')

     GAP  is  distributed  by  us  without  any  warranty,  to  the extent
permitted  by  applicable  state  law.  We  distribute GAP *as is* without
warranty  of  any  kind,  either  expressed  or implied, including, but not
limited  to,  the  implied  warranties of merchantability and fitness for a
particular purpose.

     The  entire  risk  as to the quality and performance of the program is
with you. Should GAP prove defective, you assume the cost of all necessary 
servicing, repair or correction.

     In no case unless required by applicable law will we, and/or any other 
party who may modify and redistribute GAP as permitted above, be liable to 
you  for  damages,  including  lost  profits, lost monies or other special,
incidental  or consequential damages arising out of the use or inability to
use GAP.

     You  are  permitted  to  modify and redistribute GAP, but you are not
allowed  to  restrict  further  redistribution.  That is to say proprietary
modifications  will  not be allowed. We want all versions of GAP to remain
free.

     If  you modify any part of GAP and redistribute it, you must supply a
`README' document. This should specify what modifications you made in which 
files. We do not want to take credit or be blamed for your modifications.

     Of  course  we  are  interested  in  all  of  your  modifications.  In
particular  we would like to see bug-fixes, improvements and new functions.
So  again  we  would  appreciate  it  if  you  would  inform  us  about all
modifications you make.

     

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: git-imerge
Source: <https://github.com/mhagger/git-imerge>

Files: *
Copyright: 2012-2018 Michael Haggerty <mhagger@alum.mit.edu>
License: GPL-2+

Files: doc/presentations/GitMerge-2013/ui/small-white/*
Copyright: Public Domain
License: public-domain

Files: doc/presentations/GitMerge-2013/ui/small-white/iepngfix.htc
Copyright: 2005 Angus Turnbull
License: LGPL-2.1+

Files: debian/*
Copyright: 2018 Jessica Clarke <jrtc27@debian.org>
           2020 Paul Wise <pabs@debian.org>
License: GPL-2+

License: public-domain
 The source files tagged with this license all state that they are in the
 public domain, with the exception of blank.gif which is a 1x1 px image
 consisting entirely of information that is common property and contains no
 original authorship, thus is ineligible for copyright.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

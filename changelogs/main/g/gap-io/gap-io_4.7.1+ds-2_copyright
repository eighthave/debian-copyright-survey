Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: io package for GAP
Upstream-Contact:
 Max Neunhoeffer <max@9hoeffer.de>
 Max Horn <max.horn@math.uni-giessen.de>
Source: https://www.gap-system.org/Packages/io.html
X-Source: https://gap-packages.github.io/io/
X-Source-Downloaded-From: https://github.com/gap-system/io/releases
X-Upstream-Vcs: git clone https://github.com/gap-system/io
X-Upstream-Bugs: https://github.com/gap-system/io/issues
Comment:
 The upstream source tarball is repacked to drop off the regenerated
 material, mainly the documentation, to gain substantial weight.
Files-Excluded:
 .clang-format
 .mailmap
 gen
 configure
 doc/IO.toc
 doc/chap*.txt
 doc/chap*.html
 doc/chooser.html
 doc/manual.pdf
 doc/manual.six
 doc/manual.lab
 doc/lefttoc.css
 doc/manual.css
 doc/manual.js
 doc/nocolorprompt.css
 doc/ragged.css
 doc/rainbow.js
 doc/times.css
 doc/toggless.css
 doc/toggless.js

Files: *
Copyright:
 2006-2021 Max Neunhoeffer <max@9hoeffer.de>
License: GPL-3+

Files: debian/*
Copyright:
 2014-2021 Jerome Benoit <calculus@rezozer.net>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

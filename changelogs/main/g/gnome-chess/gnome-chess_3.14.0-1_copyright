Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnome-chess
Source: http://ftp.gnome.org/pub/GNOME/sources/gnome-chess/

Files: *
Copyright: Jonathan Blandford <jrb@redhat.com>
           Ryu Changwoo <cwryu@eve.kaist.ac.kr>
           Ian Peters <ipeters@acm.org>
           Sean MacIsaac <sjm@acm.org>
           Mark Rae <Mark.Rae@ed.ac.uk>
           Carsten Schaar <nhadcasc@fs-maphy.uni-hannover.de>
           J. Marcin Gorycki <janusz.gorycki@intel.com>
           Pista <szekeres@cyberspace.mht.bme.hu>
           Lars Rydlinge <Lars.Rydlinge@HiG.SE>
           Scott Heavner <sdh@po.cwru.edu>
           Francisco Bustamante <pancho@nuclecu.unam.mx>
           Michael Meeks <michael@imaginator.com>
           Christopher James Lahey <clahey@umich.edu>
           Alan Cox <alan@cymru.net>
           James Henstridge <james@daa.com.au>
           Szabolcs Ban <shooby@gnome.hu>
           Robert Szokovacs <szo@appaloosacorp.hu>
           Shooby Ban <shooby@gnome.hu>
           Callum McKenzie <callum@spooky-possum.org>
           Jason Clinton <me@jasonclinton.com>
           Andreas Røsdal <andreasr@gnome.org>
           Robert Ancell <robert.ancell@gmail.com>
           Kenneth Christiansen <kenneth@gnu.org>
           Richard Hoelscher <rah@rahga.com>
           Software in the Public Interest, Inc.
           Free Software Foundation, Inc.
           Red Hat, Inc.
License: GPL-2+

Files: src/gl*.vapi
Copyright: 2008 Matias De la Puente <mfpuente.ar@gmail.com>
License: LGPL-2.1+

Files: data/pieces/fancy/*.svg
Copyright: 2010 Alexey Kryukov
License: GPL-2+

Files: data/pieces/simple/*.svg
Copyright: 1994-2002 World Wide Web Consortium
           1994-2002 Massachusetts Institute of Technology
           1994-2002 Institut National de Recherche en Informatique et en
                     Automatique
           1994-2002 Keio University
License: W3C
 This W3C work (including software, documents, or other related
 items) is being provided by the copyright holders under the
 following license. By obtaining, using and/or copying this work,
 you (the licensee) agree that you have read, understood, and will
 comply with the following terms and conditions:
 .
 Permission to use, copy, modify, and distribute this software
 and its documentation, with or without modification, for any
 purpose and without fee or royalty is hereby granted, provided that
 you include the following on ALL copies of the software and
 documentation or portions thereof, including modifications, that
 you make:
 .
  - The full text of this NOTICE in a location viewable to users of
    the redistributed or derivative work.
 .
  - Any pre-existing intellectual property disclaimers, notices, or
    terms and conditions. If none exist, a short notice of the
    following form (hypertext is preferred, text is permitted) should
    be used within the body of any redistributed or derivative code:
    "Copyright © 1994-2002 World Wide Web Consortium, (Massachusetts
    Institute of Technology, Institut National de Recherche en Informatique
    et en Automatique, Keio University. All Rights Reserved."
 .
  - Notice of any changes or modifications to the W3C files,
    including the date changes were made. (We recommend you provide
    URIs to the location from which the code is derived.
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND
 COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES OF
 MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE
 USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD
 PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT,
 SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE
 SOFTWARE OR DOCUMENTATION.
 . 
 The name and trademarks of copyright holders may NOT be used in
 advertising or publicity pertaining to the software without
 specific, written prior permission. Title to copyright in this
 software and any associated documentation will at all times remain
 with copyright holders.

Files: help/*
Copyright: Andreas Røsdal <andrearo@pvv.ntnu.no>       
License: GPL-2+ or GFDL-NIV

License: GFDL-NIV
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.1
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License can be found in `/usr/share/common-licenses/GFDL'.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

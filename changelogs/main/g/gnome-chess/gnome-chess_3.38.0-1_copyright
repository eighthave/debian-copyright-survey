Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gnome-chess
Source: https://download.gnome.org/sources/gnome-chess/

Files: *
Copyright: 2013–2016 Michael Catanzaro
           2015–2016 Sahil Sareen
           2010-2016 Robert Ancell
License: GPL-3+

Files: po/*
Copyright:      Croatiann team
           1998-2016 Free Software Foundation, Inc.
           2000 Gaute Hvoslef Kvalnes.
           2000 Jesús Bravo Álvarez.
           2003 Mətin Əmirov
           2005,2006, Thomas M. Hinkle.
           2005 Canonical Ltd.
           2005, Thomas M. Hinkle.
           2005-2008 Robert Ancell (and contributors)
           2006 Lukas Novotny <lukasnov@cvs.gnome.org>
           2006 Miloslav Trmac <mitr@volny.cz>
           2006 Sharif Farsiweb, Inc.
           2006,2010 The GNOME Foundation
           2007–2011 The GNOME Project.
           2007 Samuel Murray
           2009 Tim Horton
           2012 Krasimir Chonov <mk2616@abv.bg>.
           2012 Swecha Telugu Localisation Team <localisation@swecha.net>
           2015 GNOME I18N Project for Vietnamese.
License: GPL-3+

Files: data/pieces/fancy/*.svg
Copyright: 2010 Alexey Kryukov
License: GPL-2+

Files: data/pieces/simple/*.svg
Copyright: 1994-2002 World Wide Web Consortium
           1994-2002 Massachusetts Institute of Technology
           1994-2002 Institut National de Recherche en Informatique et en
                     Automatique
           1994-2002 Keio University
License: W3C
 This W3C work (including software, documents, or other related
 items) is being provided by the copyright holders under the
 following license. By obtaining, using and/or copying this work,
 you (the licensee) agree that you have read, understood, and will
 comply with the following terms and conditions:
 .
 Permission to use, copy, modify, and distribute this software
 and its documentation, with or without modification, for any
 purpose and without fee or royalty is hereby granted, provided that
 you include the following on ALL copies of the software and
 documentation or portions thereof, including modifications, that
 you make:
 .
  - The full text of this NOTICE in a location viewable to users of
    the redistributed or derivative work.
 .
  - Any pre-existing intellectual property disclaimers, notices, or
    terms and conditions. If none exist, a short notice of the
    following form (hypertext is preferred, text is permitted) should
    be used within the body of any redistributed or derivative code:
    "Copyright © 1994-2002 World Wide Web Consortium, (Massachusetts
    Institute of Technology, Institut National de Recherche en Informatique
    et en Automatique, Keio University. All Rights Reserved."
 .
  - Notice of any changes or modifications to the W3C files,
    including the date changes were made. (We recommend you provide
    URIs to the location from which the code is derived.
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND
 COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES OF
 MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE
 USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD
 PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT,
 SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE
 SOFTWARE OR DOCUMENTATION.
 . 
 The name and trademarks of copyright holders may NOT be used in
 advertising or publicity pertaining to the software without
 specific, written prior permission. Title to copyright in this
 software and any associated documentation will at all times remain
 with copyright holders.

Files: help/*
Copyright: Andreas Røsdal <andrearo@pvv.ntnu.no>
License: GPL-3+ or GFDL-NIV

License: GFDL-NIV
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.1
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License can be found in `/usr/share/common-licenses/GFDL'.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

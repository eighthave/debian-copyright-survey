Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gr-air-modes
Upstream-Contact: Nick Foster <bistromath@gmail.com>
Source:
 git clone https://github.com/bistromath/gr-air-modes.git
 The upstream package source tarball was generated from the version:
 git archive --format=tar --prefix=gr-air-modes-0.0.2.65e5bd1/ 65e5bd1 | xz > ../gr-air-modes_0.0.2.65e5bd1.orig.tar.xz
Comment:
 Debian packages by A. Maitland Bottoms <bottoms@debian.org>
 .
 Upstream Authors:
 Copyright 2010, 2011, 2012 Nick Foster <bistromath@gmail.com>
License: GPL-3+

Files: COPYING
Copyright: 2007 Free Software Foundation, Inc. <http://fsf.org/>
License: COPYING
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

Files: debian/*
Copyright: 2013 A. Maitland Bottoms <bottoms@debian.org>
License: GPL-3+

Files: apps/* cmake/* docs/* grc/* include/* lib/* python/* res/* swig/*
 AUTHORS Dockerfile README
Copyright: 2010-2019 Nick Foster
License: GPL-3+

Files: CMakeLists.txt docs/doxygen/* python/__init__.py
Copyright: 2004,2007-2013 Free Software Foundation, Inc.
Comment: GNU Radio gr_modtool derived files
License: GPL-3+

Files: python/Quaternion.py
Copyright: 2009 Smithsonian Astrophysical Observatory
License: SAO-BSD
 Released under New BSD / 3-Clause BSD License
 All rights reserved
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
    Neither the name of the Smithsonian Astrophysical
    Observatory nor the names of its contributors may be used to
    endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3+
 gr-air-modes is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 gr-air-modes is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with gr-air-modes; see the file COPYING.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public
 License (GPL) version 3 can be found in the file
 '/usr/share/common-licenses/GPL-3'.

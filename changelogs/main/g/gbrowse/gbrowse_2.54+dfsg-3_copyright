Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://search.cpan.org/dist/GBrowse/

Files: *
Copyright: © 2002 Cold Spring Harbor Laboratory and University of California, Berkeley
 © 2008 H. Mark Okada <hmokada@hotmail.com>.
 © 2001,2003,2005,2008 Cold Spring Harbor Laboratory
 © 2010 Ontario Institute for Cancer Research
License: GPL-1+ or Artistic-2.0
 The Generic Genome Browser package versions between 1.0 and 1.8 and
 all associated files are Copyright (c) 2002 Cold Spring Harbor
 Laboratory and University of California, Berkeley. Versions 1.9 and
 higher are copyright (c) 2010 Ontario Institute for Cancer Research.
 .
 This package and its accompanying libraries is free software; you can
 redistribute it and/or modify it under the terms of the GPL (either
 version 1, or at your option, any later version) or the Artistic
 License 2.0.  Refer to LICENSE for the full license text.
 .
 CSHL makes no representations whatsoever as to the SOFTWARE contained
 herein.  It is experimental in nature and is provided WITHOUT WARRANTY
 OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR ANY OTHER
 WARRANTY, EXPRESS OR IMPLIED.  CSHL MAKES NO REPRESENTATION OR
 WARRANTY THAT THE USE OF THIS SOFTWARE WILL NOT INFRINGE ANY PATENT OR
 OTHER PROPRIETARY RIGHT.
 .
 By downloading this SOFTWARE, your Institution hereby indemnifies CSHL
 against any loss, claim, damage or liability, of whatsoever kind or
 nature, which may arise from your Institution's respective use,
 handling or storage of the SOFTWARE.
 .
 If publications result from research using this SOFTWARE, we ask that
 CSHL, University of California, Berkeley and the Ontario Institute for
 Cancer Research be acknowledged and/or credit be given to
 CSHL/Berkeley/OICR scientists, as scientifically appropriate.

Files: bin/auto_install_databases.pl
 bin/bed2gff3.pl
 bin/bp_load_gff.pl
 bin/gtf2gff3.pl
 bin/load_genbank.pl
 bin/make_das_conf.pl
 bin/process_gadfly.pl
 bin/process_ncbi_human.pl
 bin/process_ncbi_human.pl
 bin/process_sgd.pl
 bin/ucsc_genes2gff.pl
 cgi-bin/gbrowse_img
 conf/plugins/Aligner.pm
 conf/plugins/AttributeHiliter.pm
 conf/plugins/RestrictionAnnotator.pm
 conf/plugins/SequenceDumper.pm
 conf/plugins/SimpleTrackFinder.pm
 conf/plugins/Spectrogram.pm
 conf/plugins/TrackFinder.pm
 contrib/Coverage/plugin/Coverage.pm
 docs/pod/GBROWSE_IMG.pod
 docs/pod/MAKE_IMAGES_HOWTO.pod
 docs/pod/PLUGINS_HOWTO.pod
 htdocs/gbrowse_karyotype_help.html
 lib/Bio/DB/GFF/Aggregator/reftranscript.pm
 lib/Bio/DB/GFF/Aggregator/waba_alignment.pm
 lib/Bio/DB/GFF/Aggregator/wormbase_gene.pm
 lib/Bio/Graphics/Browser2/CAlign.pm
 lib/Bio/Graphics/Browser2/Markup.pm
 lib/Bio/Graphics/Browser2/OptionPick.pm
 lib/Bio/Graphics/Browser2/PadAlignment.pm
 lib/Bio/Graphics/Browser2/Realign.pm
 lib/CGI/Toggle.pm
 libalign/CAlign.pm
Copyright: © 2001–4 Cold Spring Harbor Laboratory
 © Jason Stajich and Cold Spring Harbor Laboratories 2002
 © 2009 Ontario Institute for Cancer Research
 © 2007, University of Utah
 © 2002 Regional Blood Center of Ribeirao Preto
 © 2002 Advanced Biomedical Computing Center, SAIC/NCI-Frederick.
License: GPL-1+ or Artistic
 This library is free software; you can redistribute it and/or modify
 it under the same terms as Perl itself.  See DISCLAIMER.txt for
 disclaimers of warranty.

Files: t/Test.pm
Copyright: © 1998-1999 Joshua Nathaniel Pritikin.  All rights reserved.
License: Artistic
 This package is free software and is provided "as is" without express
 or implied warranty.  It may be used, redistributed and/or modified
 under the terms of the Perl Artistic License (see
 http://www.perl.com/perl/misc/Artistic.html)

Files: htdocs/css/dropdown/default_theme.css htdocs/css/dropdown/dropdown.css
Copyright: © 2008 Live Web Institute. All Rights Reserved.
License: GPL-3+
Comment: Checked on http://lwis.net/free-css-drop-down-menu/: “This CSS Drop-Down Menu
 is free for commercial or personal use and is licensed under the GNU General
 Public License.”

Files: docs/pod/INSTALL.MacOSX.pod
Copyright: © 2003 Cold Spring Harbor Laboratory
License: missing-most-probably-same-as-Perl
 Material in this document is copyright 2001-2003 by the Cold Spring
 Harbor Laboratory. This information is provided "AS-IS" without any
 warranty, expressed or implied.

Files: htdocs/js/*.js
Copyright: © 2005-2010 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
 © 2005-2010 Ivan Krstic (http://blogs.law.harvard.edu/ivan)
 © 2005-2010 Jon Tirsen (http://www.tirsen.com)
 © 2007,2008 Sheldon McKay, Cold Spring Harbor Laboratory
License: MIT

Files: htdocs/js/prototype.js
Copyright: © 2005-2010 Sam Stephenson
 © 2009, The Dojo Foundation
License: Dojo
 Released under the MIT, BSD, and GPL Licenses.
 More information: http://sizzlejs.com/

Files: htdocs/js/x_cook.js
Copyright: © 2004 Michael Foster
License: LGPL-2.1+

Files: htdocs/subtracktable.js
Copyright: © 2005-2009 Matt Kruse (javascripttoolbox.com)
License: MIT or GPL
 Dual licensed under the MIT and GPL licenses. 
 This basically means you can use this code however you want for
 free, but don't claim to have written it yourself!
 Donations always accepted: http://www.JavascriptToolbox.com/donate/
 . 
 Please do not link to the .js files on javascripttoolbox.com from
 your site. Copy the files locally to your server instead.

Files: debian/*
Copyright: 2011, Olivier Sallou <olivier.sallou@irisa.fr>
License: Artistic-2.0 or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian GNU/Linux systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'.

License: Artistic-2.0
 Copyright (c) 2000-2006, The Perl Foundation.
 .
 Everyone is permitted to copy and distribute verbatim copies of this license 
 document, but changing it is not allowed.
 .
 .
  Preamble
 This license establishes the terms under which a given free software Package may 
 be copied, modified, distributed, and/or redistributed. The intent is that the 
 Copyright Holder maintains some artistic control over the development of that 
 Package while still keeping the Package available as open source and free 
 software.
 .
 You are always permitted to make arrangements wholly outside of this license 
 directly with the Copyright Holder of a given Package. If the terms of this 
 license do not permit the full use that you propose to make of the Package, you 
 should contact the Copyright Holder and seek a different licensing arrangement.
 .
 .
  Definitions
 "Copyright Holder" means the individual(s) or organization(s) named in the 
 copyright notice for the entire Package.
 .
 "Contributor" means any party that has contributed code or other material to the 
 Package, in accordance with the Copyright Holder's procedures.
 .
 "You" and "your" means any person who would like to copy, distribute, or modify 
 the Package.
 .
 "Package" means the collection of files distributed by the Copyright Holder, and 
 derivatives of that collection and/or of those files. A given Package may 
 consist of either the Standard Version, or a Modified Version.
 .
 "Distribute" means providing a copy of the Package or making it accessible to 
 anyone else, or in the case of a company or organization, to others outside of 
 your company or organization.
 .
 "Distributor Fee" means any fee that you charge for Distributing this Package or 
 providing support for this Package to another party. It does not mean licensing 
 fees.
 .
 "Standard Version" refers to the Package if it has not been modified, or has 
 been modified only in ways explicitly requested by the Copyright Holder.
 .
 "Modified Version" means the Package, if it has been changed, and such changes 
 were not explicitly requested by the Copyright Holder.
 .
 "Original License" means this Artistic License as Distributed with the Standard 
 Version of the Package, in its current version or as it may be modified by The 
 Perl Foundation in the future.
 .
 "Source" form means the source code, documentation source, and configuration 
 files for the Package.
 .
 "Compiled" form means the compiled bytecode, object code, binary, or any other 
 form resulting from mechanical transformation or translation of the Source form.
 .
 .
  Permission for Use and Modification Without Distribution
 (1) You are permitted to use the Standard Version and create and use Modified 
 Versions for any purpose without restriction, provided that you do not 
 Distribute the Modified Version.
 .
 .
  Permissions for Redistribution of the Standard Version
 (2) You may Distribute verbatim copies of the Source form of the Standard 
 Version of this Package in any medium without restriction, either gratis or for 
 a Distributor Fee, provided that you duplicate all of the original copyright 
 notices and associated disclaimers. At your discretion, such verbatim copies may 
 or may not include a Compiled form of the Package.
 .
 (3) You may apply any bug fixes, portability changes, and other modifications 
 made available from the Copyright Holder. The resulting Package will still be 
 considered the Standard Version, and as such will be subject to the Original 
 License.
 .
 .
  Distribution of Modified Versions of the Package as Source
 (4) You may Distribute your Modified Version as Source (either gratis or for a 
 Distributor Fee, and with or without a Compiled form of the Modified Version) 
 provided that you clearly document how it differs from the Standard Version, 
 including, but not limited to, documenting any non-standard features, 
 executables, or modules, and provided that you do at least ONE of the 
 following:
 .
 (a) make the Modified Version available to the Copyright Holder of the Standard 
 Version, under the Original License, so that the Copyright Holder may include 
 your modifications in the Standard Version.
 .
 (b) ensure that installation of your Modified Version does not prevent the user 
 installing or running the Standard Version. In addition, the Modified Version 
 must bear a name that is different from the name of the Standard Version.
 .
 (c) allow anyone who receives a copy of the Modified Version to make the Source 
 form of the Modified Version available to others under
 .
 (i) the Original License or
 .
 (ii) a license that permits the licensee to freely copy, modify and redistribute 
 the Modified Version using the same licensing terms that apply to the copy that 
 the licensee received, and requires that the Source form of the Modified 
 Version, and of any works derived from it, be made freely available in that 
 license fees are prohibited but Distributor Fees are allowed.
 .
  Distribution of Compiled Forms of the Standard Version or Modified Versions without the Source
 .
 (5) You may Distribute Compiled forms of the Standard Version without the 
 Source, provided that you include complete instructions on how to get the 
 Source of the Standard Version. Such instructions must be valid at the time of 
 your distribution. If these instructions, at any time while you are carrying out 
 such distribution, become invalid, you must provide new instructions on demand 
 or cease further distribution. If you provide valid instructions or cease 
 distribution within thirty days after you become aware that the instructions are 
 invalid, then you do not forfeit any of your rights under this license.
 .
 (6) You may Distribute a Modified Version in Compiled form without the Source, 
 provided that you comply with Section 4 with respect to the Source of the 
 Modified Version.
 .
 .
  Aggregating or Linking the Package
 (7) You may aggregate the Package (either the Standard Version or Modified 
 Version) with other packages and Distribute the resulting aggregation provided 
 that you do not charge a licensing fee for the Package. Distributor Fees are 
 permitted, and licensing fees for other components in the aggregation are 
 permitted. The terms of this license apply to the use and Distribution of the 
 Standard or Modified Versions as included in the aggregation.
 .
 (8) You are permitted to link Modified and Standard Versions with other works, 
 to embed the Package in a larger work of your own, or to build stand-alone 
 binary or bytecode versions of applications that include the Package, and 
 Distribute the result without restriction, provided the result does not expose a 
 direct interface to the Package.
 .
 .
  Items That are Not Considered Part of a Modified Version
 (9) Works (including, but not limited to, modules and scripts) that merely 
 extend or make use of the Package, do not, by themselves, cause the Package to 
 be a Modified Version. In addition, such works are not considered parts of the 
 Package itself, and are not subject to the terms of this license.
 .
 .
  General Provisions
 (10) Any use, modification, and distribution of the Standard or Modified 
 Versions is governed by this Artistic License. By using, modifying or 
 distributing the Package, you accept this license. Do not use, modify, or 
 distribute the Package, if you do not accept this license.
 .
 (11) If your Modified Version has been derived from a Modified Version made by 
 someone other than you, you are nevertheless required to ensure that your 
 Modified Version complies with the requirements of this license.
 .
 (12) This license does not grant you the right to use any trademark, service 
 mark, tradename, or logo of the Copyright Holder.
 .
 (13) This license includes the non-exclusive, worldwide, free-of-charge patent 
 license to make, have made, use, offer to sell, sell, import and otherwise 
 transfer the Package with respect to any patent claims licensable by the 
 Copyright Holder that are necessarily infringed by the Package. If you institute 
 patent litigation (including a cross-claim or counterclaim) against any party 
 alleging that the Package constitutes direct or contributory patent 
 infringement, then this Artistic License to you shall terminate on the date that 
 such litigation is filed.
 .
 (14) Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER AND 
 CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES. THE IMPLIED 
 WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR 
 NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY YOUR LOCAL LAW. 
 UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR CONTRIBUTOR WILL BE LIABLE FOR 
 ANY DIRECT, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING IN ANY WAY 
 OUT OF THE USE OF THE PACKAGE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 DAMAGE.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 .
 On Debian GNU/Linux systems, the complete text of version 1 of the
 General Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>
 or </usr/share/common-licenses/GPL-3>.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian GNU/Linux systems, the complete text of version 2.1 of the
 Lesser General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

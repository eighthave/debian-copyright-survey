Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gfal2
Source: https://gitlab.cern.ch/dmc/gfal2/repository/archive.tar.gz?ref=v2.16.2

Files: *
Copyright:
 CERN 2013-2017
 Members of the EMI Collaboration 2010-2013
 .
 Members of the EMI Collaboration
 --------------------------------
 European Organization for Nuclear Research (CERN), Switzerland
 Centro de Supercomputación de Galicia (CESGA), Spain
 CESNET, zajmove sdruzeni pravnickych osob, Czech Republic
 CINECA - Consorzio Interuniversitario, Italy
 Agencia Estatal Consejo Superior de Investigaciones Cientificas (CSIC), Spain
 Deutsches Elektronen-Synchrotron (DESY), Germany
 Fundamenteel Onderzoek der Materie (FOM), The Netherlands
 Forschungszentrum Jülich GmbH (FZJ), Germany
 Greek Research and Technology Network SA (GRNET), Greece
 Istituto Nazionale di Fisica Nucleare (INFN), Italy
 Lunds Universitet (LU), Sweden
 National Information Infrastructure Development Institute (NIIF), Hungary
 Science and Technology Facilities Council (STFC), United Kingdom
 Teleinformatikdienste fuer Lehre und Forschung (SWITCH), Switzerland
 Trinity College Dublin (TCD), Ireland
 Technische Universität Dresden (TUD), Germany
 University of Copenhagen (UCPH), Denmark
 University of Helsinki – Helsinki institute of Physics (UH.HIP), Finland
 Universitetet i Oslo (UiO), Norway
 Univerzita Pavla Jozefa Šafarika V Kosiciach (UPJS), Slovak Republic
 Uppsala Universitet (UU), Sweden
 Uniwersytet Warszawski (UWAR), Poland
 Korea Institute of Science and Technology Information (KISTI), South Korea
 Academia Sinica Grid Computing (ASGC), Taiwan
License: Apache-2.0

Files: cmake/modules/MacroCopyFile.cmake
Copyright:
 2006-2007 Wengo
 2006-2008 Andreas Schneider <mail@cynapses.org>
License: BSD-3-clause
 Redistribution and use is allowed according to the terms of the BSD license.

Files: src/plugins/gridftp/gridftp_ns_xattr.cpp
Copyright:
 University of Nebraska-Lincoln 2016
License: Apache-2.0

Files: debian/*
Copyright:
 2012-2019, Mattias Ellert <mattias.ellert@physics.uu.se>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems the full text of the Apache-2.0 license can be found in
 the /usr/share/common-licenses/Apache-2.0 file.

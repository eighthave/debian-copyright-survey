Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pkcs11
Source: https://github.com/miekg/pkcs11

Files: *
Copyright: 2013 Miek Gieben
License: BSD-3-clause

Files: pkcs11*.h
Copyright: 1994 RSA Security Inc.
License: Cryptoki
Comment: http://www.emc.com/emc-plus/rsa-labs/standards-initiatives/pkcs-11-cryptographic-token-interface-standard.htm

Files: debian/*
Copyright: 2015 Tianon Gravi <tianon@debian.org>
License: BSD-3-clause
Comment: Debian packaging is licensed under the same terms as upstream

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   * Redistributions of source code must retain the above copyright notice, this
     list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Miek Gieben nor the names of its contributors may be
     used to endorse or promote products derived from this software without
     specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Cryptoki
 License to copy and use this software is granted provided that it is identified
 as "RSA Security Inc. PKCS #11 Cryptographic Token Interface (Cryptoki)" in all
 material mentioning or referencing this software.
 .
 License is also granted to make and use derivative works provided that such
 works are identified as "derived from the RSA Security Inc. PKCS #11
 Cryptographic Token Interface (Cryptoki)" in all material mentioning or
 referencing the derived work.
 .
 RSA Security Inc. makes no representations concerning either the
 merchantability of this software or the suitability of this software for any
 particular purpose. It is provided "as is" without express or implied warranty
 of any kind.

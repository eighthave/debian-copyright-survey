Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://www.geuz.org/gmsh/
Files-Excluded:
 contrib/Tetgen1.4
 contrib/Tetgen1.5
 contrib/Metis

Files: *
Copyright: 1997-2015 Christophe Geuzaine <geuzaine@acm.caltech.edu>
                     Jean-Francois Remacle <remacle@gce.ucl.ac.be>
License: GPL-2+
 GPL-2 license is applied here with the following exception.
 .
 The copyright holders of Gmsh give you permission to combine Gmsh
 with code included in the standard release of TetGen (from Hang Si),
 Netgen (from Joachim Sch"oberl), Chaco (from Bruce Hendrickson and
 Robert Leland at Sandia National Laboratories), METIS (from
 George Karypis at the University of Minnesota) and OpenCASCADE (from
 Open CASCADE S.A.S) under their respective licenses. You may copy
 and distribute such a system following the terms of the GNU GPL
 for Gmsh and the licenses of the other code concerned, provided
 that you include the source code of that other code when and as the
 GNU GPL requires distribution of source code.
 .
 Note that people who make modified versions of Gmsh are not
 obligated to grant this special exception for their modified
 versions; it is their choice whether to do so. The GNU General
 Public License gives permission to release a modified version
 without this exception; this exception also makes it possible to
 release a modified version which carries forward this exception.
 .
 End of exception.
 .
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or any later version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: contrib/Chaco/*
Copyright: 1997-2011 Sandia Corporation
License: LGPL-2.1

Files: contrib/Netgen/*
Copyright: 1997-2011 Dr. Joachim Schöberl <js@jku.at>
License: LGPL-2.1

Files: contrib/blossom/*
Copyright: 1997-2011 Prof. Cook <bico@isye.gatech.edu>
License: other
 Free to use for both academic and non-academic work.
 Concorde license is granted only for use within the Gmsh system.
 .
 For more informatoin, please, refer contrib/blossom/README.txt

Files: contrib/voro++/*
Copyright: 2008 The Regents of the University of California
License: BSD-3-clause
 Voro++ Copyright (c) 2008, The Regents of the University of California, through
 Lawrence Berkeley National Laboratory (subject to receipt of any required
 approvals from the U.S. Dept. of Energy). All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 (1) Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 (3) Neither the name of the University of California, Lawrence Berkeley
 National Laboratory, U.S. Dept. of Energy nor the names of its contributors may
 be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 You are under no obligation whatsoever to provide any bug fixes, patches, or
 upgrades to the features, functionality or performance of the source code
 ("Enhancements") to anyone; however, if you choose to make your Enhancements
 available either publicly, or directly to Lawrence Berkeley National
 Laboratory, without imposing a separate written license agreement for such
 Enhancements, then you hereby grant the following license: a  non-exclusive,
 royalty-free perpetual license to install, use, modify, prepare derivative
 works, incorporate into other computer software, distribute, and sublicense
 such enhancements or derivative works thereof, in binary and source code form.

Files: contrib/rtree/*
Copyright: 1983 Antonin Guttman, UC Berkely
           1983 Michael Stonebraker, UC Berkely
           1994 Melinda Green <melinda@superliminal.com>
           1995 Paul Brook
           2004 Greg Douglas
License: public-domain
 contrib/rtree/README says:
 Entirely free for all uses.

Files: contrib/Revoropt/*
Copyright: (C) 2013 Vincent Nivoliers <vincent.nivoliers@univ-lyon1.fr>
License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla
 Public License v. 2.0. If a copy of the MPL was not distributed
 with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

Files: contrib/zipper/*
Copyright: (c) 1998-2010 - Gilles Vollant
                           Mathias Svensson
License: zlib
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
  .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
  .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


License: LGPL-2.1
 On Debian systems, the full text of the GNU LESSER GENERAL PUBLIC
 LICENSE version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

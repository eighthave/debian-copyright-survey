Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: prose
Source: https://github.com/jdkato/prose

Files: *
Copyright: 2017 Joseph Kato
License: Expat

Files: tag/aptag.go
Copyright: 2013 Matthew Honnibal
License: Expat
Comment:
 Averaged Perceptron Tagger was ported from Matthew Honnibal’s Python code.
 See his blog "A Good Part-of-Speech Tagger in about 200 Lines of Python"
 https://explosion.ai/blog/part-of-speech-pos-tagger-in-python

Files: tokenize/punkt.go
Copyright: 2015 Eric Bower
License: Expat
Comment:
 PunktSentenceTokenizer is an extension of Eric Bower’s Go implementation
 of the Punkt sentence tokenizer (https://github.com/neurosnap/sentences),
 with a few minor improvements
 (see https://github.com/neurosnap/sentences/pull/18).

Files: tokenize/pragmatic.go
Copyright: 2015 Kevin S. Dias
License: Expat
Comment:
 PragmaticSegmenter is a multilingual, rule-based sentence boundary detector.
 This is a port of the Ruby library by Kevin S. Dias
 (https://github.com/diasks2/pragmatic_segmenter).

Files: debian/*
Copyright: 2017 Anthony Fok <foka@debian.org>
License: Expat
Comment: Debian packaging is licensed under the same terms as upstream

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

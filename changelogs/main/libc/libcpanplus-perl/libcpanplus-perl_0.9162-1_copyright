Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CPANPLUS
Source: https://metacpan.org/release/CPANPLUS
Upstream-Contact: Chris Williams <chris@bingosnet.co.uk>

Files: *
Copyright: 2001-2007, Jos Boumans <kane@cpan.org>
 Chris Williams <chris@bingosnet.co.uk>
License: Artistic or GPL-1+

Files: lib/CPANPLUS/Config/HomeEnv.pm
Copyright: Chris Williams <chris@bingosnet.co.uk>
 Jos Boumans <kane@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/bundle/CPAN/Meta/YAML.pm
Copyright: 2010, by Adam Kennedy
License: Artistic or GPL-1+

Files: inc/bundle/Devel/InnerPackage.pm
Copyright: 2005, Simon Wistow
License: Artistic or GPL-1+

Files: inc/bundle/File/Spec/Functions.pm
 inc/bundle/File/Spec/Unix.pm
 inc/bundle/File/Spec/OS2.pm
 inc/bundle/File/Spec/Mac.pm
 inc/bundle/File/Spec/Epoc.pm
Copyright: 2004 by the Perl 5 Porters
License: Artistic or GPL-1+

Files: inc/bundle/File/Spec/VMS.pm
Copyright: 2004-14 by the Perl 5 Porters
License: Artistic or GPL-1+

Files: inc/bundle/File/Spec/Cygwin.pm
 inc/bundle/File/Spec/Win32.pm
 inc/bundle/File/Spec/Cygwin.pm
Copyright: 2004,2007 by the Perl 5 Porters
License: Artistic or GPL-1+

Files: inc/bundle/File/Spec.pm
 inc/bundle/File/Spec.pm
Copyright: 2004-2013 by the Perl 5 Porters
License: Artistic or GPL-1+

Files: inc/bundle/HTTP/Tiny.pm
Copyright: 2016, Christian Hansen
License: Artistic or GPL-1+

Files: inc/bundle/IO/Zlib.pm
Copyright: 1998-2004, Tom Hughes <tom@compton.nu>
License: Artistic or GPL-1+

Files: inc/bundle/IO/String.pm
Copyright: 1998-2005, Gisle Aas
License: Artistic or GPL-1+

Files: inc/bundle/JSON/PP/Compat5006.pm
Copyright: 2007-2010, Makamaka Hannyaharamitu
License: Artistic or GPL-1+

Files: inc/bundle/JSON/PP.pm
Copyright: 2007-2016, Makamaka Hannyaharamitu
License: Artistic or GPL-1+

Files: inc/bundle/Locale/Maketext/Simple.pm
Copyright: 2003, 2004, 2005, 2006 by Audrey Tang <cpan@audreyt.org>
License: other
 This software is released under the MIT license cited below.  Additionally,
 when this software is distributed with Perl Kit, Version 5, you may also
 redistribute it and/or modify it under the same terms as Perl itself.
 .
 The "MIT" License
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: inc/bundle/Module/CoreList/Utils.pm
Copyright: 2013, Chris Williams
License: Artistic or GPL-1+

Files: inc/bundle/Module/Pluggable/Object.pm
 inc/bundle/Module/Pluggable.pm
Copyright: 2006, Simon Wistow
License: Artistic or GPL-1+

Files: inc/bundle/Module/Metadata.pm
Copyright: Original code Copyright (c) 2001-2011 Ken Williams.
 Additional code Copyright (c) 2010-2011 Matt Trout and David Golden.
License: Artistic or GPL-1+

Files: inc/bundle/Parse/CPAN/Meta.pm
Copyright: 2010, David Golden, Ricardo Signes, Adam Kennedy and Contributors
License: Artistic or GPL-1+

Files: inc/bundle/version/typemap
Copyright: 2001, John Peacock
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2013-2017, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

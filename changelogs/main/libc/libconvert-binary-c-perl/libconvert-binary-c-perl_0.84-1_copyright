Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Convert-Binary-C
Upstream-Contact: Marcus Holland-Moritz <mhx@cpan.org>
Source: https://metacpan.org/release/Convert-Binary-C

Files: *
Copyright: 2002-2020, Marcus Holland-Moritz <mhx@cpan.org>
License: Artistic or GPL-1+

Files: ctlib/y_pragma.c
Copyright: 2002–2020, Marcus Holland-Moritz
 1984, 1989-1990, 2000-2015, 2018-2020, Free Software Foundation, Inc.
License: GPL-3+-plus-Bison-exception
 Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
 Inc.
 .
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.
 .
 This special exception was added by the Free Software Foundation in
 version 2.2 of Bison.
 .
 .
 Copyright (c) 2002-2020 Marcus Holland-Moritz. All rights reserved.
 This program is free software; you can redistribute it and/or modify
 it under the same terms as Perl itself.

Files: ctlib/y_parser.c
Copyright: 2002–2020, Marcus Holland-Moritz
 1989,1990, James A. Roskind
 1984, 1989-1990, 2000-2015, 2018-2020, Free Software Foundation, Inc.
License: GPL-3+-plus-Bison-exception-Roskind
 Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
 Inc.
 .
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 As a special exception, you may create a larger work that contains
 part or all of the Bison parser skeleton and distribute that work
 under terms of your choice, so long as that work isn't itself a
 parser generator using the skeleton or a modified version thereof
 as a parser skeleton.  Alternatively, if you modify or redistribute
 the parser skeleton itself, you may (at your option) remove this
 special exception, which will cause the skeleton and the resulting
 Bison output files to be licensed under the GNU General Public
 License without this special exception.
 .
 This special exception was added by the Free Software Foundation in
 version 2.2 of Bison.
 .
 .
 Portions Copyright (c) 1989, 1990 James A. Roskind.
 .
 This grammar was developed  and  written  by  James  A.  Roskind.
 Copying  of  this  grammar  description, as a whole, is permitted
 providing this notice is intact and applicable  in  all  complete
 copies.   Translations as a whole to other parser generator input
 languages  (or  grammar  description  languages)   is   permitted
 provided  that  this  notice is intact and applicable in all such
 copies,  along  with  a  disclaimer  that  the  contents  are   a
 translation.   The reproduction of derived text, such as modified
 versions of this grammar, or the output of parser generators,  is
 permitted,  provided  the  resulting  work includes the copyright
 notice "Portions Copyright (c)  1989,  1990  James  A.  Roskind".
 Derived products, such as compilers, translators, browsers, etc.,
 that  use  this  grammar,  must also provide the notice "Portions
 Copyright  (c)  1989,  1990  James  A.  Roskind"  in   a   manner
 appropriate  to  the  utility,  and in keeping with copyright law
 (e.g.: EITHER displayed when first invoked/executed; OR displayed
 continuously on display terminal; OR via placement in the  object
 code  in  form  readable in a printout, with or near the title of
 the work, or at the end of the file).  No royalties, licenses  or
 commissions  of  any  kind are required to copy this grammar, its
 translations, or derivative products, when the copies are made in
 compliance with this notice. Persons or corporations that do make
 copies in compliance with this notice may charge  whatever  price
 is  agreeable  to  a  buyer, for such copies or derivative works.
 THIS GRAMMAR IS PROVIDED ``AS IS'' AND  WITHOUT  ANY  EXPRESS  OR
 IMPLIED  WARRANTIES,  INCLUDING,  WITHOUT LIMITATION, THE IMPLIED
 WARRANTIES  OF  MERCHANTABILITY  AND  FITNESS  FOR  A  PARTICULAR
 PURPOSE.
 .
 James A. Roskind
 Independent Consultant
 516 Latania Palm Drive
 Indialantic FL, 32903
 (407)729-4348
 jar@ileaf.com
 .
 ACKNOWLEDGMENT:
 .
 Without the effort expended by the ANSI C standardizing committee,  I
 would  have been lost.  Although the ANSI C standard does not include
 a fully disambiguated syntax description, the committee has at  least
 provided most of the disambiguating rules in narratives.
 .
 Several  reviewers  have also recently critiqued this grammar, and/or
 assisted in discussions during it's preparation.  These reviewers are
 certainly not responsible for the errors I have committed  here,  but
 they  are responsible for allowing me to provide fewer errors.  These
 colleagues include: Bruce Blodgett, and Mark Langley.
 .
 .
 Copyright (c) 2002-2020 Marcus Holland-Moritz. All rights reserved.
 This program is free software; you can redistribute it and/or modify
 it under the same terms as Perl itself.

Files: ctlib/parser.y
Copyright: 2002–2020, Marcus Holland-Moritz
 1989,1990, James A. Roskind
License: Artistic or GPL-1+
Comment:
 Portions Copyright (c) 1989, 1990 James A. Roskind.
 .
 This grammar was developed  and  written  by  James  A.  Roskind.
 Copying  of  this  grammar  description, as a whole, is permitted
 providing this notice is intact and applicable  in  all  complete
 copies.   Translations as a whole to other parser generator input
 languages  (or  grammar  description  languages)   is   permitted
 provided  that  this  notice is intact and applicable in all such
 copies,  along  with  a  disclaimer  that  the  contents  are   a
 translation.   The reproduction of derived text, such as modified
 versions of this grammar, or the output of parser generators,  is
 permitted,  provided  the  resulting  work includes the copyright
 notice "Portions Copyright (c)  1989,  1990  James  A.  Roskind".
 Derived products, such as compilers, translators, browsers, etc.,
 that  use  this  grammar,  must also provide the notice "Portions
 Copyright  (c)  1989,  1990  James  A.  Roskind"  in   a   manner
 appropriate  to  the  utility,  and in keeping with copyright law
 (e.g.: EITHER displayed when first invoked/executed; OR displayed
 continuously on display terminal; OR via placement in the  object
 code  in  form  readable in a printout, with or near the title of
 the work, or at the end of the file).  No royalties, licenses  or
 commissions  of  any  kind are required to copy this grammar, its
 translations, or derivative products, when the copies are made in
 compliance with this notice. Persons or corporations that do make
 copies in compliance with this notice may charge  whatever  price
 is  agreeable  to  a  buyer, for such copies or derivative works.
 THIS GRAMMAR IS PROVIDED ``AS IS'' AND  WITHOUT  ANY  EXPRESS  OR
 IMPLIED  WARRANTIES,  INCLUDING,  WITHOUT LIMITATION, THE IMPLIED
 WARRANTIES  OF  MERCHANTABILITY  AND  FITNESS  FOR  A  PARTICULAR
 PURPOSE.
 .
 James A. Roskind
 Independent Consultant
 516 Latania Palm Drive
 Indialantic FL, 32903
 (407)729-4348
 jar@ileaf.com
 .
 ACKNOWLEDGMENT:
 .
 Without the effort expended by the ANSI C standardizing committee,  I
 would  have been lost.  Although the ANSI C standard does not include
 a fully disambiguated syntax description, the committee has at  least
 provided most of the disambiguating rules in narratives.
 .
 Several  reviewers  have also recently critiqued this grammar, and/or
 assisted in discussions during it's preparation.  These reviewers are
 certainly not responsible for the errors I have committed  here,  but
 they  are responsible for allowing me to provide fewer errors.  These
 colleagues include: Bruce Blodgett, and Mark Langley.
 .
 .
 Copyright (c) 2002-2007 Marcus Holland-Moritz. All rights reserved.
 This program is free software; you can redistribute it and/or modify
 it under the same terms as Perl itself.

Files: tests/include/pdclib/*
Copyright: n/a
License: CC0-1.0
 PDCLib is distributed unter the Creative Commons CC0 License. You
 should have received a copy of the full legal text of this license
 as part of this distribution (COPYING.CC0). It is also available at
 https://creativecommons.org/publicdomain/zero/1.0/legalcode
 .
 On Debian systems, the complete text of the CC0 1.0 Universal license
 can be found in `/usr/share/common-licenses/CC0-1.0'.

Files: ucpp/*
Copyright: 2002, Thomas Pornin
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 4. The name of the authors may not be used to endorse or promote
    products derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: ucpp/ppdir.pl
Copyright: 2004-2007, Marcus Holland-Moritz
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009, Charles Plessy <plessy@debian.org>
 2009, Ryan Niebur <ryan@debian.org>
 2011-2020, gregor herrmann <gregoa@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

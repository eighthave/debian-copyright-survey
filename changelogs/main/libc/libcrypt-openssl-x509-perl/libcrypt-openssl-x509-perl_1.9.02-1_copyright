Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Crypt-OpenSSL-X509
Upstream-Contact: Dan Sully, <daniel@cpan.org>
Source: https://metacpan.org/release/Crypt-OpenSSL-X509

Files: *
Copyright: 2004-2019, Dan Sully, <daniel@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2007, Micah Anderson <micah@debian.org>
 2007-2020, gregor herrmann <gregoa@debian.org>
 2008, Damyan Ivanov <dmn@debian.org>
 2008, Niko Tyni <ntyni@debian.org>
 2008, Roberto C. Sanchez <roberto@debian.org>
 2010, Ansgar Burchardt <ansgar@debian.org>
 2010-2018, Salvatore Bonaccorso <carnil@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Config-Model-LcdProc
Upstream-Contact: Dominique Dumont
Source: http://search.cpan.org/dist/Config-Model-LcdProc/

Files: *
Copyright: 2011-2016, Dominique Dumont <ddumont@cpan.org>
License: LGPL-2.1+

Files: debian/*
Copyright: 2013-2016, Dominique Dumont <dod@debian.org>
License: LGPL-2.1+

Files: lib/*
Copyright: 2013-2017, Dominique Dumont.
License: LGPL-2.1

Files: lib/Config/Model/models/LCDd.pod
Copyright: 1999-2013, William Ferrell and others
License: GPL-2

Files: lib/Config/Model/models/LCDd/CFontz.pod
  lib/Config/Model/models/LCDd/CFontzPacket.pod
  lib/Config/Model/models/LCDd/CwLnx.pod
  lib/Config/Model/models/LCDd/EyeboxOne.pod
  lib/Config/Model/models/LCDd/IOWarrior.pod
  lib/Config/Model/models/LCDd/IrMan.pod
  lib/Config/Model/models/LCDd/MD8800.pod
  lib/Config/Model/models/LCDd/MtxOrb.pod
  lib/Config/Model/models/LCDd/NoritakeVFD.pod
  lib/Config/Model/models/LCDd/Olimex_MOD_LCD1x9.pod
  lib/Config/Model/models/LCDd/SureElec.pod
  lib/Config/Model/models/LCDd/bayrad.pod
  lib/Config/Model/models/LCDd/curses.pod
  lib/Config/Model/models/LCDd/ea65.pod
  lib/Config/Model/models/LCDd/futaba.pod
  lib/Config/Model/models/LCDd/g15.pod
  lib/Config/Model/models/LCDd/glcd.pod
  lib/Config/Model/models/LCDd/glcdlib.pod
  lib/Config/Model/models/LCDd/glk.pod
  lib/Config/Model/models/LCDd/hd44780.pod
  lib/Config/Model/models/LCDd/icp_a106.pod
  lib/Config/Model/models/LCDd/imon.pod
  lib/Config/Model/models/LCDd/imonlcd.pod
  lib/Config/Model/models/LCDd/irtrans.pod
  lib/Config/Model/models/LCDd/joy.pod
  lib/Config/Model/models/LCDd/lb216.pod
  lib/Config/Model/models/LCDd/lcdm001.pod
  lib/Config/Model/models/LCDd/lcterm.pod
  lib/Config/Model/models/LCDd/linux_input.pod
  lib/Config/Model/models/LCDd/lirc.pod
  lib/Config/Model/models/LCDd/lis.pod
  lib/Config/Model/models/LCDd/mdm166a.pod
  lib/Config/Model/models/LCDd/menu.pod
  lib/Config/Model/models/LCDd/ms6931.pod
  lib/Config/Model/models/LCDd/mtc_s16209x.pod
  lib/Config/Model/models/LCDd/mx5000.pod
  lib/Config/Model/models/LCDd/picolcd.pod
  lib/Config/Model/models/LCDd/pyramid.pod
  lib/Config/Model/models/LCDd/rawserial.pod
  lib/Config/Model/models/LCDd/sdeclcd.pod
  lib/Config/Model/models/LCDd/sed1330.pod
  lib/Config/Model/models/LCDd/sed1520.pod
  lib/Config/Model/models/LCDd/serialPOS.pod
  lib/Config/Model/models/LCDd/serialVFD.pod
  lib/Config/Model/models/LCDd/server.pod
  lib/Config/Model/models/LCDd/shuttleVFD.pod
  lib/Config/Model/models/LCDd/sli.pod
  lib/Config/Model/models/LCDd/stv5730.pod
  lib/Config/Model/models/LCDd/svga.pod
  lib/Config/Model/models/LCDd/t6963.pod
  lib/Config/Model/models/LCDd/text.pod
  lib/Config/Model/models/LCDd/tyan.pod
  lib/Config/Model/models/LCDd/ula200.pod
  lib/Config/Model/models/LCDd/vlsys_m428.pod
  lib/Config/Model/models/LCDd/xosd.pod
  lib/Config/Model/models/LCDd/yard2LCD.pod
Copyright: 1999-2013, William Ferrell and others
License: GPL-2

Files: lib/Config/Model/system.d/*
Copyright: 2011-2016, Dominique Dumont <ddumont@cpan.org>
License: LGPL-2.1+

Files: script/*
Copyright: 2013-2017, Dominique Dumont.
License: LGPL-2.1

Files: t/model_tests.d/lcdd-test-conf.pl
Copyright: 2013-2017, Dominique Dumont.
License: LGPL-2.1

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Config-Augeas
Upstream-Contact: Dominique Dumont <ddumont@cpan.org>
                  Raphaël Pinson <raphink@cpan.org>
Source: https://metacpan.org/release/Config-Augeas

Files: *
Copyright: 2008-2010, Dominique Dumont <ddumont@cpan.org>
 2011-2013, Raphaël Pinson <raphink@cpan.org>
License: LGPL-2+

Files: lib/Config/ppport.h
Copyright: 2004-2009, Marcus Holland-Moritz <mhx-cpan@gmx.net>
           2001, Paul Marquess <pmqs@cpan.org> (Version 2.x)
           1999, Kenneth Albanowski <kjahds@kjahds.com> (Version 1.x)
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2008-2013, Dominique Dumont <dod@debian.org>
 2009-2010, Jonathan Yu <jawnsy@cpan.org>
 2009-2015, gregor herrmann <gregoa@debian.org>
License: LGPL-2+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian GNU/Linux systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL'

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2, or (at
 your option) any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'

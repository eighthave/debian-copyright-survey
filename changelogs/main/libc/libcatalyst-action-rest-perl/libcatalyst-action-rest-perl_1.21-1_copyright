Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Arthur Axel "fREW" Schmidt <frioux@gmail.com>
Source: https://metacpan.org/release/Catalyst-Action-REST
Upstream-Name: Catalyst-Action-REST

Files: *
Copyright:
 2006-2015, Adam Jacob <adam@stalecoffee.org>
 2006-2015, Marchex, Inc. <http://www.marchex.com/>
 2006-2015, Arthur Axel Schmidt <frioux@gmail.com>
 2006-2015, Christopher Laco <claco@cpan.org>
 2006-2015, Luke Saunders <lsaunders@cpan.org>
 2006-2015, John Goulah <jgoulah@cpan.org>
 2006-2015, Daisuke Maki <daisuke@endeworks.jp>
 2006-2015, J. Shirley <jshirley@gmail.com>
 2006-2015, Hans Dieter Pearcey <hdp@cpan.org>
 2006-2017, Tomas Doran <bobtfish@bobtfish.net>
 2006-2015, Gavin Henry <ghenry@surevoip.co.uk>
 2006-2015, Gerv http://www.gerv.net/
 2006-2015, Colin Newell <colin@opusvl.com>
 2006-2015, Wallace Reis <wreis@cpan.org>
 2006-2015, André Walker (andrewalker) <andre@cpan.org>
License: Artistic or GPL-1+

Files: lib/Catalyst/Request/REST/ForBrowsers.pm
Copyright: 2008-2009, Dave Rolsky
License: Artistic or GPL-1+

Files: lib/Catalyst/TraitFor/Request/REST/ForBrowsers.pm
Copyright: 2008-2010, Dave Rolsky
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009-2011, Jonathan Yu <jawnsy@cpan.org>
 2010, Franck Joncourt <franck@debian.org>
 2010-2015, gregor herrmann <gregoa@debian.org>
 2011-2012, Ansgar Burchardt <ansgar@debian.org>
 2012-2015, Florian Schlichting <fsfs@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

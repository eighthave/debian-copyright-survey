Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded: priv/soap.xsd priv/wsdl.xsd priv/wsdl11soap12.xsd

Files: *
Copyright: 2006 Claes Wikstrom, klacke@hyber.org. All Rights Reserved
License: BSD-YAWS

Files: debian/*
Copyright: 2005-2006 Will Newton
           2006 Francois-Denis Gonthier
           2006 Torsten Werner
           2006-2015 Sergei Golovan
License: BSD-YAWS

Files: ac-aux/*
Copyright: 1999-2013 Free Software Foundation, Inc.
License: GPL-2+

Files: ac-aux/config.*
Copyright: 1992-2013 Free Software Foundation, Inc.
License: GPL-3+

Files: ac-aux/install-sh
Copyright: 1994 X Consortium
License: X11

Files: c_src/hashtable_private.h c_src/hashtable.c c_src/hashtable.h
Copyright: 2002, 2004 Christopher Clark <firstname.lastname@cl.cam.ac.uk>
License: BSD-3

Files: doc/overview.edoc
Copyright: 2008 Torbjorn Tornkvist
License: BSD-YAWS

Files: examples/src/authmod_gssapi.erl
Copyright: 2007 Mikael Magnusson
License: BSD-3

Files: src/jsonrpc.erl
Copyright: 2006 Gaspar Chilingarov <nm@web.am>
                Gurgen Tumanyan <barbarian@armkb.com>
License: BSD-2

Files: src/yaws_jsonrpc.erl src/yaws_rpc.erl src/yaws_xmlrpc.erl
Copyright: 2003 Joakim Grebenö <jocke@gleipnir.com>
           2006 Gaspar Chilingarov <nm@web.am>
                Gurgen Tumanyan <barbarian@armkb.com>
License: BSD-2

Files: two-mode-mode.el
Copyright: 1999-2004 The Apache Software Foundation
License: Apache-2

Files: */Makefile.in aclocal.m4 m4/* configure
Copyright: 1994-2013 Free Software Foundation, Inc.
License: permissive
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: src/haxe.erl src/json2.erl src/json.erl
Copyright: 2005-2006, A2Z Development USA, Inc. All Rights Reserved
License: EPL-1.1

Files: contrib/benchmarks/all.erl contrib/benchmarks/bench.erl contrib/benchmarks/bench.hrl
Copyright: 1999, Ericsson Utvecklings AB. All Rights Reserved
License: EPL-1.1

Files: priv/envelope.xsd priv/soap-envelope.xsd
Copyright: 2001-2003 W3C(R) (MIT, ERCIM, Keio), All Rights Reserved.
License: W3C
 By obtaining, using and/or copying this work, you (the licensee) agree
 that you have read, understood, and will comply with the following terms
 and conditions:
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation, with or without modification,  for any purpose and without
 fee or royalty is hereby granted, provided that you include the following
 on ALL copies of the software and documentation or portions thereof,
 including modifications, that you make:
 .
 1.  The full text of this NOTICE in a location viewable to users of
 the redistributed or derivative work.
 .
 2.  Any pre-existing intellectual property disclaimers, notices, or
 terms and conditions. If none exist, a short notice of the following
 form (hypertext is preferred, text is permitted) should be used within
 the body of any redistributed or derivative code: "Copyright © 2001
 World Wide Web Consortium, (Massachusetts Institute of Technology,
 Institut National de Recherche en Informatique et en Automatique, Keio
 University). All Rights Reserved. http://www.w3.org/Consortium/Legal/"
 .
 3.  Notice of any changes or modifications to the W3C files, including
 the date changes were made. (We recommend you provide URIs to the
 location from which the code is derived.)
 .
 Original W3C files; http://www.w3.org/2001/06/soap-envelope
 Changes made:
     - reverted namespace to http://schemas.xmlsoap.org/soap/envelope/
     - reverted mustUnderstand to only allow 0 and 1 as lexical values
     - made encodingStyle a global attribute 20020825
     - removed default value from mustUnderstand attribute declaration
 .
 THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT
 HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE OR
 DOCUMENTATION WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS,
 TRADEMARKS OR OTHER RIGHTS.
 .
 COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL
 OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE SOFTWARE OR
 DOCUMENTATION.
 .
 The name and trademarks of copyright holders may NOT be used in
 advertising or publicity pertaining to the software without specific,
 written prior permission. Title to copyright in this software and any
 associated documentation will at all times remain with copyright holders.

License: BSD-YAWS
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of "Yaws" nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 * Neither the name of the original author; nor the names of any contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: X11
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
        http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache
 License version 2.0 can be found in the file
 `/usr/share/common-licenses/Apache-2.0'.

License: EPL-1.1
 ERLANG PUBLIC LICENSE
 Version 1.1
 .
 1. Definitions.
 .
 1.1. ``Contributor'' means each entity that creates or contributes to
 the creation of Modifications.
 .
 1.2. ``Contributor Version'' means the combination of the Original
 Code, prior Modifications used by a Contributor, and the Modifications
 made by that particular Contributor.
 .
 1.3. ``Covered Code'' means the Original Code or Modifications or the
 combination of the Original Code and Modifications, in each case
 including portions thereof.
 .
 1.4. ``Electronic Distribution Mechanism'' means a mechanism generally
 accepted in the software development community for the electronic
 transfer of data.
 .
 1.5. ``Executable'' means Covered Code in any form other than Source
 Code.
 .
 1.6. ``Initial Developer'' means the individual or entity identified
 as the Initial Developer in the Source Code notice required by Exhibit
 A.
 .
 1.7. ``Larger Work'' means a work which combines Covered Code or
 portions thereof with code not governed by the terms of this License.
 .
 1.8. ``License'' means this document.
 .
 1.9. ``Modifications'' means any addition to or deletion from the
 substance or structure of either the Original Code or any previous
 Modifications. When Covered Code is released as a series of files, a
 Modification is:
 .
 A. Any addition to or deletion from the contents of a file containing
    Original Code or previous Modifications.
 .
 B. Any new file that contains any part of the Original Code or
    previous Modifications.
 .
 1.10. ``Original Code'' means Source Code of computer software code
 which is described in the Source Code notice required by Exhibit A as
 Original Code, and which, at the time of its release under this
 License is not already Covered Code governed by this License.
 .
 1.11. ``Source Code'' means the preferred form of the Covered Code for
 making modifications to it, including all modules it contains, plus
 any associated interface definition files, scripts used to control
 compilation and installation of an Executable, or a list of source
 code differential comparisons against either the Original Code or
 another well known, available Covered Code of the Contributor's
 choice. The Source Code can be in a compressed or archival form,
 provided the appropriate decompression or de-archiving software is
 widely available for no charge.
 .
 1.12. ``You'' means an individual or a legal entity exercising rights
 under, and complying with all of the terms of, this License. For legal
 entities,``You'' includes any entity which controls, is controlled by,
 or is under common control with You. For purposes of this definition,
 ``control'' means (a) the power, direct or indirect, to cause the
 direction or management of such entity, whether by contract or
 otherwise, or (b) ownership of fifty percent (50%) or more of the
 outstanding shares or beneficial ownership of such entity.
 .
 2. Source Code License.
 .
 2.1. The Initial Developer Grant.
 The Initial Developer hereby grants You a world-wide, royalty-free,
 non-exclusive license, subject to third party intellectual property
 claims:
 .
 (a) to use, reproduce, modify, display, perform, sublicense and
     distribute the Original Code (or portions thereof) with or without
     Modifications, or as part of a Larger Work; and
 .
 (b) under patents now or hereafter owned or controlled by Initial
     Developer, to make, have made, use and sell (``Utilize'') the
     Original Code (or portions thereof), but solely to the extent that
     any such patent is reasonably necessary to enable You to Utilize
     the Original Code (or portions thereof) and not to any greater
     extent that may be necessary to Utilize further Modifications or
     combinations.
 .
 2.2. Contributor Grant.
 Each Contributor hereby grants You a world-wide, royalty-free,
 non-exclusive license, subject to third party intellectual property
 claims:
 .
 (a) to use, reproduce, modify, display, perform, sublicense and
     distribute the Modifications created by such Contributor (or
     portions thereof) either on an unmodified basis, with other
     Modifications, as Covered Code or as part of a Larger Work; and
 .
 (b) under patents now or hereafter owned or controlled by Contributor,
     to Utilize the Contributor Version (or portions thereof), but
     solely to the extent that any such patent is reasonably necessary
     to enable You to Utilize the Contributor Version (or portions
     thereof), and not to any greater extent that may be necessary to
     Utilize further Modifications or combinations.
 .
 3. Distribution Obligations.
 .
 3.1. Application of License.
 The Modifications which You contribute are governed by the terms of
 this License, including without limitation Section 2.2. The Source
 Code version of Covered Code may be distributed only under the terms
 of this License, and You must include a copy of this License with
 every copy of the Source Code You distribute. You may not offer or
 impose any terms on any Source Code version that alters or restricts
 the applicable version of this License or the recipients' rights
 hereunder. However, You may include an additional document offering
 the additional rights described in Section 3.5.
 .
 3.2. Availability of Source Code.
 Any Modification which You contribute must be made available in Source
 Code form under the terms of this License either on the same media as
 an Executable version or via an accepted Electronic Distribution
 Mechanism to anyone to whom you made an Executable version available;
 and if made available via Electronic Distribution Mechanism, must
 remain available for at least twelve (12) months after the date it
 initially became available, or at least six (6) months after a
 subsequent version of that particular Modification has been made
 available to such recipients. You are responsible for ensuring that
 the Source Code version remains available even if the Electronic
 Distribution Mechanism is maintained by a third party.
 .
 3.3. Description of Modifications.
 You must cause all Covered Code to which you contribute to contain a
 file documenting the changes You made to create that Covered Code and
 the date of any change. You must include a prominent statement that
 the Modification is derived, directly or indirectly, from Original
 Code provided by the Initial Developer and including the name of the
 Initial Developer in (a) the Source Code, and (b) in any notice in an
 Executable version or related documentation in which You describe the
 origin or ownership of the Covered Code.
 .
 3.4. Intellectual Property Matters
 .
 (a) Third Party Claims.
     If You have knowledge that a party claims an intellectual property
     right in particular functionality or code (or its utilization
     under this License), you must include a text file with the source
     code distribution titled ``LEGAL'' which describes the claim and
     the party making the claim in sufficient detail that a recipient
     will know whom to contact. If you obtain such knowledge after You
     make Your Modification available as described in Section 3.2, You
     shall promptly modify the LEGAL file in all copies You make
     available thereafter and shall take other steps (such as notifying
     appropriate mailing lists or newsgroups) reasonably calculated to
     inform those who received the Covered Code that new knowledge has
     been obtained.
 .
 (b) Contributor APIs.
     If Your Modification is an application programming interface and
     You own or control patents which are reasonably necessary to
     implement that API, you must also include this information in the
     LEGAL file.
 .
 3.5. Required Notices.
 You must duplicate the notice in Exhibit A in each file of the Source
 Code, and this License in any documentation for the Source Code, where
 You describe recipients' rights relating to Covered Code. If You
 created one or more Modification(s), You may add your name as a
 Contributor to the notice described in Exhibit A. If it is not
 possible to put such notice in a particular Source Code file due to
 its structure, then you must include such notice in a location (such
 as a relevant directory file) where a user would be likely to look for
 such a notice. You may choose to offer, and to charge a fee for,
 warranty, support, indemnity or liability obligations to one or more
 recipients of Covered Code. However, You may do so only on Your own
 behalf, and not on behalf of the Initial Developer or any
 Contributor. You must make it absolutely clear than any such warranty,
 support, indemnity or liability obligation is offered by You alone,
 and You hereby agree to indemnify the Initial Developer and every
 Contributor for any liability incurred by the Initial Developer or
 such Contributor as a result of warranty, support, indemnity or
 liability terms You offer.
 .
 3.6. Distribution of Executable Versions.
 You may distribute Covered Code in Executable form only if the
 requirements of Section 3.1-3.5 have been met for that Covered Code,
 and if You include a notice stating that the Source Code version of
 the Covered Code is available under the terms of this License,
 including a description of how and where You have fulfilled the
 obligations of Section 3.2. The notice must be conspicuously included
 in any notice in an Executable version, related documentation or
 collateral in which You describe recipients' rights relating to the
 Covered Code. You may distribute the Executable version of Covered
 Code under a license of Your choice, which may contain terms different
 from this License, provided that You are in compliance with the terms
 of this License and that the license for the Executable version does
 not attempt to limit or alter the recipient's rights in the Source
 Code version from the rights set forth in this License. If You
 distribute the Executable version under a different license You must
 make it absolutely clear that any terms which differ from this License
 are offered by You alone, not by the Initial Developer or any
 Contributor. You hereby agree to indemnify the Initial Developer and
 every Contributor for any liability incurred by the Initial Developer
 or such Contributor as a result of any such terms You offer.
 .
 3.7. Larger Works.
 You may create a Larger Work by combining Covered Code with other code
 not governed by the terms of this License and distribute the Larger
 Work as a single product. In such a case, You must make sure the
 requirements of this License are fulfilled for the Covered Code.
 .
 4. Inability to Comply Due to Statute or Regulation.
 If it is impossible for You to comply with any of the terms of this
 License with respect to some or all of the Covered Code due to statute
 or regulation then You must: (a) comply with the terms of this License
 to the maximum extent possible; and (b) describe the limitations and
 the code they affect. Such description must be included in the LEGAL
 file described in Section 3.4 and must be included with all
 distributions of the Source Code. Except to the extent prohibited by
 statute or regulation, such description must be sufficiently detailed
 for a recipient of ordinary skill to be able to understand it.
 .
 5. Application of this License.
 .
 This License applies to code to which the Initial Developer has
 attached the notice in Exhibit A, and to related Covered Code.
 .
 6. CONNECTION TO MOZILLA PUBLIC LICENSE
 .
 This Erlang License is a derivative work of the Mozilla Public
 License, Version 1.0. It contains terms which differ from the Mozilla
 Public License, Version 1.0.
 .
 7. DISCLAIMER OF WARRANTY.
 .
 COVERED CODE IS PROVIDED UNDER THIS LICENSE ON AN ``AS IS'' BASIS,
 WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 WITHOUT LIMITATION, WARRANTIES THAT THE COVERED CODE IS FREE OF
 DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE OR
 NON-INFRINGING. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF
 THE COVERED CODE IS WITH YOU. SHOULD ANY COVERED CODE PROVE DEFECTIVE
 IN ANY RESPECT, YOU (NOT THE INITIAL DEVELOPER OR ANY OTHER
 CONTRIBUTOR) ASSUME THE COST OF ANY NECESSARY SERVICING, REPAIR OR
 CORRECTION. THIS DISCLAIMER OF WARRANTY CONSTITUTES AN ESSENTIAL PART
 OF THIS LICENSE. NO USE OF ANY COVERED CODE IS AUTHORIZED HEREUNDER
 EXCEPT UNDER THIS DISCLAIMER.
 .
 8. TERMINATION.
 This License and the rights granted hereunder will terminate
 automatically if You fail to comply with terms herein and fail to cure
 such breach within 30 days of becoming aware of the breach. All
 sublicenses to the Covered Code which are properly granted shall
 survive any termination of this License. Provisions which, by their
 nature, must remain in effect beyond the termination of this License
 shall survive.
 .
 9. DISCLAIMER OF LIABILITY
 Any utilization of Covered Code shall not cause the Initial Developer
 or any Contributor to be liable for any damages (neither direct nor
 indirect).
 .
 10. MISCELLANEOUS
 This License represents the complete agreement concerning the subject
 matter hereof. If any provision is held to be unenforceable, such
 provision shall be reformed only to the extent necessary to make it
 enforceable. This License shall be construed by and in accordance with
 the substantive laws of Sweden. Any dispute, controversy or claim
 arising out of or relating to this License, or the breach, termination
 or invalidity thereof, shall be subject to the exclusive jurisdiction
 of Swedish courts, with the Stockholm City Court as the first
 instance.
 .
 EXHIBIT A.
 .
 ``The contents of this file are subject to the Erlang Public License,
 Version 1.1, (the "License"); you may not use this file except in
 compliance with the License. You should have received a copy of the
 Erlang Public License along with this software. If not, it can be
 retrieved via the world wide web at http://www.erlang.org/.
 .
 Software distributed under the License is distributed on an "AS IS"
 basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 the License for the specific language governing rights and limitations
 under the License.
 .
 The Initial Developer of the Original Code is Ericsson Utvecklings AB.
 Portions created by Ericsson are Copyright 1999, Ericsson Utvecklings
 AB. All Rights Reserved.''

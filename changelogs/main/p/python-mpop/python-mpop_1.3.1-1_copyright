Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mpop
Source: https://github.com/mraspaud/mpop

Files: *
Copyright: 2009-2016 Martin Raspaud <martin.raspaud@smhi.se>
           2010-2016 Adam Dybbroe <adam.dybbroe@smhi.se>
           2012-2016 SMHI, Folkborgsvagen 1, Norrkoping, Sweden
                2016 Christian Kliche <christian.kliche@ebp.de>
           2014-2015 Abhay Devasthale
           2014-2015 Panu Lahtinen <panu.lahtinen@fmi.fi>
           2010-2014 Kristian Rune Larssen <krl@dmi.dk>
           2013-2014 Lars Ørum Rasmussen <ras@dmi.dk>
                2013 Space Science and Engineering Center (SSEC)
                     Esben S. Nielsen <esn@dmi.dk>
                     Sara Hornquist <sara.hornquist@smhi.se>
License: GPL-3+

Files: doc/*
Copyright: 2009-2013 The Pytroll crew
License: GPL-3+

Files: doc/examples/*
Copyright: 2011, 2014 SMHI
License: GPL-3+

Files: mpop/imageo/formats/tifffile.py
Copyright: 2008-2014, The Regents of the University of California
                      Christoph Gohlke
License: BSD-3-clause

Files: debian/*
Copyright: 2014 Antonio Valentino <antonio.valentino@tiscali.it>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: BSD-3-clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 a. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 b. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 c. Neither the name of the PyLibTiff project nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.


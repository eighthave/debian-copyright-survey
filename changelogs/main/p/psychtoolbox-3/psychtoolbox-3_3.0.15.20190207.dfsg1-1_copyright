Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Psychtoolbox-3
Upstream-Contact: Mario Kleiner <mario.kleiner@tuebingen.mpg.de>
Source: http://psychtoolbox-3.googlecode.com/svn

Files: *
Copyright:
  1996-2007, David Brainard
  1996-2007, Denis Pelli
  1996-2007, Allen Ingling  <allen.ingling@nyu.edu>
  2005-2015, Mario Kleiner <mario.kleiner@tuebingen.mpg.de>
License: Expat

Files: Psychtoolbox/PsychRadiometric/PsychISO2007MPE
Copyright: 2013, David Brainard
License: Expat

Files: PsychSourceGL/Source/Common/PsychCV/OpenEyesCVEyeTracker/cvEyeTracker.cc
       PsychSourceGL/Source/Common/PsychCV/OpenEyesCVEyeTracker/r*
Copyright:
  2004-2006, Dongheng Li <dhli@iastate.edu>
  2004-2006, Derrick Parkhurst <derrick.parkhurst@hcvl.hci.iastate.edu>
  2004-2006, Jason Babcock <babcock@nyu.edu>
  2004-2006, David Winfield <dwinfiel@iastate.edu>
License: GPL-2+


Files: PsychSourceGL/Source/Common/PsychCV/OpenEyesCVEyeTracker/svd.*
Copyright: 2003, Division of Imaging Science and Biomedical Engineering, University of Manchester, UK
License: BSD-3


Files: PsychSourceGL/Cohorts/FTGLTextRenderer/OGLFT.*
Copyright:
  2002, lignum Computing, Inc. <oglft@lignumcomputing.com>
  2008, Allen Barnett
License: LGPL-2+


Files: PsychSourceGL/Cohorts/PortAudio/pa_*
       PsychSourceGL/Source/Common/PsychPortAudio/pa_*
       PsychSourceGL/Source/Common/PsychPortAudio/portaudio.h
Copyright:
  1999-2006 Ross Bencina, Phil Burk
  1999-2000 Robert Marsanyi
  1999-2004 Andrew Baldwin
  2002 Joshua Haberman <joshua@haberman.com>
  2003 Fred Gleason
  2004 Stefan Westerfeld <stefan@space.twc.de>
  2004-2006 Arve Knudsen <aknuds-1@broadpark.no>
  2005-2006 Ludwig Schwardt
  2006 David Viens
License: Expat-customized
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files
  (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge,
  publish, distribute, sublicense, and/or sell copies of the Software,
  and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  .
  The text above constitutes the entire PortAudio license; however,
  the PortAudio community also makes the following non-binding requests:
  .
  Any person wishing to distribute modifications to the Software is
  requested to send the modifications to the original developer so that
  they can be incorporated into the canonical version. It is also
  requested that these non-binding requests be included along with the
  license above.
Comment: PTB-3 requires patched version of portaudio.  Patches are yet to be
  adopted and verified for compatibility with current head of portaudio
  for submission upstream.

Files: PsychSourceGL/Source/Common/PsychHID/PsychHIDKbQueueCreate.c
Copyright: 2001-2008, Jens Ayton <jens@ayton.se>
License: Expat
Comment: Origin for embedded copy: https://github.com/Ahruman/KeyNaming

Files: Psychtoolbox/PsychOpenGL/MOGL
Copyright: 2006, Richard F. Murray
           2006-2010, Mario Kleiner
License: Expat

Files: Psychtoolbox/PsychOpenGL/MOGL/source/headers/glext_edit.h
Copyright:  2007-2012 The Khronos Group Inc.
License: Expat


Files: Psychtoolbox/PsychDemos/OpenGL4MatlabDemos/GLSLDemoShaders
Copyright: 2002-2006  3Dlabs Inc. Ltd
License: BSD-3

Files: Psychtoolbox/PsychDemos/SoundFiles
Copyright: 1999-2007 OpenAl Authors
License: LGPL-2+
Comment: Origin: http://www.openal.org


Files: Psychtoolbox/PsychContributed/ple.m
Copyright:
  2006, The MathWorks, Inc.
  2006, Mario Kleiner
License: BSD-3
Comment: Origin for the source: http://www.mathworks.com/matlabcentral/fileexchange/9525

Files: Psychtoolbox/PsychContributed/read_rle_rgbe.m
Copyright: Lawrence A. Taplin <taplin@cis.rit.edu>
License: Expat
Comment:
 Based loosely on the c-code RGBE implementation written by Bruce
 Walters http://www.graphics.cornell.edu/~bjw/rgbe.html made available
 under following "license terms":
 /* THIS CODE CARRIES NO GUARANTEE OF USABILITY OR FITNESS FOR ANY PURPOSE.
  * WHILE THE AUTHORS HAVE TRIED TO ENSURE THE PROGRAM WORKS CORRECTLY,
  * IT IS STRICTLY USE AT YOUR OWN RISK.  */


Files: Psychtoolbox/PsychHardware/DatapixxToolbox
Copyright: 2009, VPixx Technologies http://www.vpixx.com
           2009-2010, Mario Kleiner
License: GPL-2+

Files: Psychtoolbox/PsychHardware/iViewXToolbox/tcp_udp_ip
Copyright: 1998-2003,  Peter Rydesäter et al
License: GPL-2+

Files: PsychSourceGL/Source/Common/Screen/gl*ew.*
       Psychtoolbox/PsychOpenGL/MOGL/source/*gl*ew.h
Copyright: 2008-2014, Nigel Stewart <nigels[]users sourceforge net>
           2002-2008, Milan Ikits <milan ikits[]ieee org>
           2002-2008, Marcelo E. Magallon <mmagallo[]debian org>
           2002, Lev Povalahev
           1999-2007,  Brian Paul
           2007, The Khronos Group Inc.
License: BSD-3

Files: Psychtoolbox/PsychOpenGL/MOGL/source/headers/glu_edit.h
Copyright: 1991-2000, Silicon Graphics, Inc.
License: SGI-FSL-B-1.1

Files: debian/*
Copyright: 2010-2015, Yaroslav Halchenko <debian@onerussian.com>
License: Expat


License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to permit
  persons to whom the Software is furnished to do so, subject to the
  following conditions:
  .
  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
  NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
  USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2+
  On Debian systems, the complete text of the GNU Lesser General
  Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: BSD-3
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  .
  * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.
  * Neither the name of the University of Manchester nor the names of any
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: SGI-FSL-B-1.1
  License Applicability. Except to the extent portions of this file are
  made subject to an alternative license as permitted in the SGI Free
  Software License B, Version 1.1 (the "License"), the contents of this
  file are subject only to the provisions of the License. You may not use
  this file except in compliance with the License. You may obtain a copy
  of the License at Silicon Graphics, Inc., attn: Legal Services, 1600
  Amphitheatre Parkway, Mountain View, CA 94043-1351, or at:
  .
  http://oss.sgi.com/projects/FreeB
  .
  Note that, as provided in the License, the Software is distributed on an
  "AS IS" basis, with ALL EXPRESS AND IMPLIED WARRANTIES AND CONDITIONS
  DISCLAIMED, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES AND
  CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A
  PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
  .
  Original Code. The Original Code is: OpenGL Sample Implementation,
  Version 1.2.1, released January 26, 2000, developed by Silicon Graphics,
  Inc. The Original Code is Copyright (c) 1991-2000 Silicon Graphics, Inc.
  Copyright in any portions created by third parties is as indicated
  elsewhere herein. All Rights Reserved.
  .
  Additional Notice Provisions: This software was created using the
  OpenGL(R) version 1.2.1 Sample Implementation published by SGI, but has
  not been independently verified as being compliant with the OpenGL(R)
  version 1.2.1 Specification.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Pyevolve
Upstream-Contact: Christian S. Perone <christian.perone@gmail.com>
Source: https://github.com/perone/Pyevolve
Files-Excluded:
 docs/source/sphinx06_code_patch.py
 docs/source/.static/bottom.png
 docs/source/.static/*_icon.png
 docs/source/.static/fg.menu.*
 docs/source/.static/hover.png
 docs/source/.static/index_slide*.jpg
 docs/source/.static/jquery*.js
 docs/source/.static/nivo*
 docs/source/.static/theme
 docs/source/.static/tip_images
 docs/source/imgs/Thumbs.db
 docs/source/imgs/original/Thumbs.db
Comment:
 For DFSG purposes, some files have been removed from the upstream source,
 either because the licensing terms were unknown or too complicated. All of
 these files belonged exclusively to the sphinx template introduced in version
 0.6 of Pyevolve. The sphinx template from version 0.5 is used here instead.

Files: *
Copyright: 2007-2015, Christian S. Perone <christian.perone@gmail.com>
License: Python-CNRI-Derivative
 The Pyevolve license is based on the Python-CNRI license, which is GPL
 compatible license. See http://www.python.org/psf/license.
 .
 1. This LICENSE AGREEMENT is between Christian S. Perone ("CSP"), and the
 Individual or Organization ("Licensee") accessing and otherwise using
 Pyevolve software in source or binary form and its associated
 documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, CSP
 hereby grants Licensee a nonexclusive, royalty-free, world-wide license
 to reproduce, analyze, test, perform and/or display publicly, prepare
 derivative works, distribute, and otherwise use Pyevolve 0.6
 alone or in any derivative version, provided, however, that CSP's
 License Agreement and CSP's notice of copyright, i.e., "Copyright (c)
 2007-2010 Christian S. Perone; All Rights Reserved" are retained in
 Pyevolve 0.6 alone or in any derivative version prepared by
 Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on or
 incorporates Pyevolve 0.6 or any part thereof, and wants to
 make the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Pyevolve 0.6.
 .
 4. CSP is making Pyevolve 0.6 available to Licensee on an "AS
 IS" basis.  CSP MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, CSP MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF Pyevolve 0.6
 WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. CSP SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF Pyevolve
 0.6 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR
 LOSS AS A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING
 Pyevolve 0.6, OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF
 THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between CSP and
 Licensee.  This License Agreement does not grant permission to use CSP
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Pyevolve 0.6,
 Licensee agrees to be bound by the terms and conditions of this License
 Agreement.

Files: debian/*
Copyright: 2010-2019, Christian Kastner <ckk@debian.org>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the 
 documentation and/or other materials provided with the distribution.
 .
 3. Neither name of copyright holders nor the names of its contributors
 may be used to endorse or promote products derived from this software
 without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

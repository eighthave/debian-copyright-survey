Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Biopython
Upstream-Contact: https://github.com/biopython/biopython/issues
                  Biopython discussion list <biopython@lists.open-bio.org>
                     (subscription required -> thus GitHub is preferred)
                  Peter Cock <p.j.a.cock@googlemail.com>
Source: http://biopython.org/DIST/
Files-Excluded: Bio/Entrez/DTDs/mathml2-qname-1.mod

Files: *
Copyright: © 2002-2009 Biopython contributors
License: other
 Permission to use, copy, modify, and distribute this software and its
 documentation with or without modifications and for any purpose and
 without fee is hereby granted, provided that any copyright notices
 appear in all copies and that both those copyright notices and this
 permission notice appear in supporting documentation, and that the
 names of the contributors or copyright holders not be used in
 advertising or publicity pertaining to distribution of the software
 without specific prior permission.
 .
 THE CONTRIBUTORS AND COPYRIGHT HOLDERS OF THIS SOFTWARE DISCLAIM ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL THE
 CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY SPECIAL, INDIRECT
 OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE
 OR PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: © 2009      Charles Plessy <plessy@debian.org>
             2004-2009 Philipp Benner <philipp@debian.org>
License: GPL-2+
 The Debian packaging information is under the GPL, version 2 or later.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL' and the
 complete text of the GNU Lesser General Public License can be found in
 `/usr/share/common-licenses/LGPL'.

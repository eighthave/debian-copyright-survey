Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pax-britannica
Upstream-Contact: <nofungames@gmail.com>
Source: https://github.com/henkboom/pax-britannica

Files: *
Copyright: 2010 Ben Abraham
           2010 Renaud Bédard
           2010 Henk Boom
           2010 Daniel Burton
           2010 Matthew Gallant
License: Expat

Files: dokidoki-support/gl.c dokidoki-support/glu.c
 dokidoki-support/lua_stb_image.c dokidoki-support/memarray.c
 dokidoki-support/mixer.c
Copyright: 1994-2008 Lua.org, PUC-Rio
           2005-2006 Varol Kaptan
           2009 Henk Boom
License: Expat

Files: dokidoki-support/luaglfw.*
Copyright: 2002-2005 Camilla Berglund
License: Zlib

Files: dokidoki-support/stb_*
Copyright: 2007 Sean Barrett
License: public-domain
 Placed in the public domain April 2007 by the author: no copyright is
 claimed, and you may use it for any purpose you like.

Files: debian/*
Copyright: 2012 Joseph Nahmias
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would
    be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not
    be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.

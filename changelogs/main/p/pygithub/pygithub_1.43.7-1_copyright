Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pygithub
Upstream-Contact: Vincent Jacques <vincent@vincent-jacques.net>
Source: https://pypi.python.org/pypi/PyGithub

Files: *
Copyright: 2013 Vincent Jacques <vincent@vincent-jacques.net>
License: LGPL-3+

Files: debian/*
Copyright: 2018 Emmanuel Arias <emmanuelarias30@gmail.com>
           2014-2015 Dmitry Bogatov <KAction@gnu.org>
License: GPL-3+

Files: github/*
License: LGPL-3+
Copyright: 2013 Adrian Petrescu <adrian.petrescu@maluuba.com>
           2013 AKFish <akfish@gmail.com>
           2012 Andrew Bettison <andrewb@zip.com.au>
           2013 Bill Mill <bill.mill@gmail.com>
           2012 Christopher Gilbert <christopher.john.gilbert@gmail.com>
           2013 davidbrai <davidbrai@gmail.com>
           2012 Dima Kukushkin <dima@kukushkin.me>
           2013 Ed Jackson <ed.jackson@gmail.com>
           2013 Jonathan J Hunt <hunt@braincorporation.com>
           2013 Mark Roddy <markroddy@gmail.com>
           2013 martinqt <m.ki2@laposte.net>
           2012-2013 Michael Stead <michael.stead@gmail.com>
           2012 Michael Woodworth <mwoodworth@upverter.com>
           2013 Peter Golm <golm.peter@gmail.com>
           2012 Petteri Muilu
           2012 Philip Kimmey <philip@rover.com>
           2013 Srijan Choudhary <srijan4@gmail.com>
           2012 Steve English <steve.english@navetas.com>
           2013 Stuart Glaser <stuglaser@gmail.com>
           2012-2013 Vincent Jacques <vincent@vincent-jacques.net>
           2012 Zearin <zearin@gonk.net>

License: LGPL-3+
 PyGithub is free software: you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 PyGithub is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with PyGithub. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian system, copy of GNU Lesser General Public License version 3
 is also located at `/usr/share/common-licenses/LGPL-3'

License: GPL-3+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3
 of the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian system, copy of GNU Lesser General Public License version 3
 is also located at `/usr/share/common-licenses/GPL-3'

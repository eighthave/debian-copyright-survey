Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-trezor
Source: https://github.com/trezor/trezor-firmware

Files: *
Copyright: 2012-2016 Pavol Rusnak <stick@satoshilabs.com>
           2012-2016 Marek Palatinus <slush@satoshilabs.com>
           2016 Jochen Hoenicke <hoenicke@gmail.com>
License: LGPL-3

Files: debian/*
Copyright: 2019 Emmanuel Arias <emmnuelarias30@gmail.com>
           2014-2015 Richard Ulrich <richi@paraeasy.ch>
License: GPL-3+

Files: debian/io.trezor.trezorctl.metainfo.xml
Copyright: 2018 Tristan Seligmann <mithrandi@debian.org>
License: MIT-0

License: MIT-0
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-3
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.

License: GPL-3+
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

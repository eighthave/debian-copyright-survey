Format: http://www.debian.org/doc/copyright-format/1.0
Upstream-Name: python-quantities
Upstream-Contact: Darren Dale <dsdale24@gmail.com>
Source: https://github.com/python-quantities/python-quantities

Files: *
Copyright: 2008-2011 Darren S. Dale
License: BSD-like
 1. This LICENSE AGREEMENT is between Darren S. Dale (“DSD”), and the Individual
 or Organization (“Licensee”) accessing and otherwise using Quantites software in
 source or binary form and its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, DSD hereby
 grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
 analyze, test, perform and/or display publicly, prepare derivative works,
 distribute, and otherwise use quantities 0.10.0 alone or in any derivative
 version, provided, however, that DSD’s License Agreement and DSD’s notice of
 copyright, i.e., “Copyright (c) 2008-2009 Darren S. Dale; All Rights Reserved”
 are retained in Quantities 0.10.0 alone or in any derivative version prepared by
 Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on or
 incorporates Quantities 0.10.0 or any part thereof, and wants to make the
 derivative work available to others as provided herein, then Licensee hereby
 agrees to include in any such work a brief summary of the changes made to
 Quantities 0.10.0.
 .
 4. DSD is making Quantities 0.10.0 available to Licensee on an “AS IS” basis.
 DSD MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED. BY WAY OF
 EXAMPLE, BUT NOT LIMITATION, DSD MAKES NO AND DISCLAIMS ANY REPRESENTATION OR
 WARRANTY OF MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE
 USE OF QUANTITIES 0.10.0 WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. DSD SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF QUANTITIES 0.10.0
 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS A RESULT OF
 MODIFYING, DISTRIBUTING, OR OTHERWISE USING QUANTITIES 0.10.0, OR ANY DERIVATIVE
 THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material breach of
 its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any relationship
 of agency, partnership, or joint venture between DSD and Licensee. This License
 Agreement does not grant permission to use DSD trademarks or trade name in a
 trademark sense to endorse or promote products or services of Licensee, or any
 third party.
 .
 8. By copying, installing or otherwise using Quantities 0.10.0, Licensee agrees
 to be bound by the terms and conditions of this License Agreement.

Files: quantities/constants/NIST_codata.txt
Copyright: None
License: None
  This list of "Fundamental Physical Constants" has been downloaded from
  http://physics.nist.gov/constants. Given that these are facts this information
  is not copyrightable.

Files: debian/*
Copyright: 2012 Andrea Palazzi <palazziandrea@yahoo.it>
License: BSD-3-clause
 Copyright (c) 2012, Andrea Palazzi <palazziandrea@yahoo.it>
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

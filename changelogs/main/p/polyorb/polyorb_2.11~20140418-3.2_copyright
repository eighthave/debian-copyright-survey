Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: polyorb
Authors: PolyORB is primarily developed by Jerome Hugues, Thomas
 Vergnaud, Khaled Barbaria, Laurent Pautet (ENST Paris), and
 Thomas Quinot (AdaCore). Fabrice Kordon (LIP6) also participates
 in the project. Vadim Godunko regularly contributes by submitting
 patches. To email developers : polyorb-devel@lists.adacore.com.
Sources: http://libre.adacore.com/libre/tools/polyorb/
Comment: All the content available on libre.adacore.com is licensed
 under the terms of the pure GPL, despite the fact that AdaCore have
 not yet adjusted the licensing boilerplate in source files.
 .
 This is stated for example at
 http://lists.adacore.com/pipermail/gtkada/2009-June/003789.html. The
 actual license is a decision of AdaCore. Please contact them at
 libre@adacore.com for any clarification. .
 .
 Version 2014 of PolyORB is shipped with a verbatim copy of the GPL-3
 in the top directory.
 .
 Maintainer's note:
 .
 PolyORB is licensed under the terms of the pure GPL. As a
 consequence, it isn't legal to distribute proprietary software
 that incorporates this library. If your program uses PolyORB,
 and if you wish your program to use licensing terms incompatible
 with the GPL, you have three choices:
 .
 - Distribute your program in source form only, and require your
   licensees to build it for themselves. Your sources need not be
   under GPL, since they are not linked to PolyORB.
 .
 - Contact AdaCore, the upstream author, at sales@adacore.com,
   and purchase a GMGPL license for PolyORB.
 .
 - Do not distribute your program at all.
 .
 You may be tempted to retrieve the sources from AdaCore's
 subversion repository. Be warned that doing so will still grant
 you a pure GPL license, despite the fact that AdaCore have not
 yet adjusted the licensing boilerplate in source files. In
 Debian, the "special exception" language has been removed only
 to remove confusion. It is not the Debian package maintainers
 who changed the actual license, it is AdaCore. Contact them at
 libre@adacore.com for any clarification.

Files: *
Copyright: (C) 1999-2012, Free Software Foundation, Inc.
License: GPL-3+
 PolyORB is free software; you can redistribute it and/or modify
 it under terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your
 option) any later version. PolyORB is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY -- or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: debian/*
Copyright: (C) 2009 Reto Buerki <reet@codelabs.ch>
           (C) 2008-2014 Xavier Grave <xavier.grave@ipno.in2p3.fr>
License: GPL-3+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this program. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in /usr/share/common-licenses/GPL-3 file.

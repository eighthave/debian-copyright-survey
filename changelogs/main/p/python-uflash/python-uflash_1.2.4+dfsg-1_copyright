Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/ntoll/uflash
Upstream-Contact: Nicholas H.Tollervey <ntoll@ntoll.org>
Upstream-Name: uflash
Comment: We repack the upstream source via debian/repack.sh because:
 1. Remove upstream .gitignore and Travis CI configuration
 .
 2. The upstream source tarball contains a precompiled MicroPython firmware
 file (./firmware.hex) for the BBC micro:bit, which we build and package
 separately in package firmware-microbit-micropython; and
 .
 3. ./uflash.py includes a 600KiB convenience hex string representation of the
 same precompiled MicroPython firmware, which we remove

Files: *
Copyright: 2015-2018 Nicholas H.Tollervey and others.
License: Expat

Files: debian/*
Copyright: 2018 Nick Morrott <knowledgejunkie@gmail.com>
License: Expat
Comment: The firmware-microbit-micropython-dl.{postinst,prerm} scripts are
 adapted from those provided in bladerf. The repack script is adapted from
 that provided in ruby-fakeweb.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

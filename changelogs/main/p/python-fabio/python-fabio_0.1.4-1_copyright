Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fabio
Source: http://sourceforge.net/projects/fable/files/fabio/0.1.3/

Files: *
Copyright: 2007-2009 Henning O. Sorensen & Erik Knudsen
               Center for Fundamental Research: Metal Structures in Four Dimensions
               Risoe National Laboratory
               Frederiksborgvej 399
               DK-4000 Roskilde
               email:erik.knudsen@risoe.dk
           2009-2012 Jonathan P. Wright, Jerome Kieffer and Gael Goret
               European Synchrotron Radiation Facility
               6 rue Jules Horowitz
               38000 Grenoble, France
License: GPL-2.0+

Files: fabio-src/cbfimage.py
       fabio-src/hdf5image.py
       fabio-src/mrcimage.py
       fabio-src/tifimage.py
       fabio-src/xsdimage.py
       src/byte_offset.*
Copyright: 2011-2014 Jerome Kieffer
                     European Synchrotron Radiation Facility
License: GPL-3.0+

Files: fabio-src/pixiimage.py
Copyright: 2014 Jon Wright, Jérôme Kieffer
                European Synchrotron Radiation Facility
License: GPL-3.0+

Files: fabio-src/raxisimage.py
Copyright: 2013-2014 Brian R. Pauw <brian@stack.nl>
License: GPL-3.0+

Files: scripts/fabio_viewer
Copyright: 2012 Gael Goret <gael.goret@esrf.fr>
                European Synchrotron Radiation Facility
License: GPL-3.0+

Files: src/ccp4_pack
Copyright: 1995 Jan Pieter Abrahams
                MRC Cambridge (UK)
           2007-2009 Henning O. Sorensen & Erik Knudsen
                Risoe National Laboratory (DK)
           2012 Jerome Kieffer and Gael Goret
                European Synchrotron Radiation Facility (FR)
License: LGPL-3.0+

Files: src/mar345_IO.*
Copyright: 2012 Jerome Kieffer and Gael Goret
           European Synchrotron Radiation Facility
License: LGPL-3.0+

Files: fabio-src/TiffIO.py
Copyright: 2011 V. Armando Sole
           European Synchrotron Radiation Facility
License: LGPL-2.0+

Files: test/utilstest.py
Copyright: 2011 European Synchrotron Radiation Facility
License: LGPL-3.0+

Files: src/stdint.h
Copyright: 2006-2008 Alexander Chemeris
License: BSD

Files: fabio-src/argparse.py
Copyright: © 2006-2009 Steven J. Bethard <steven.bethard@gmail.com>
License: Apache-2.0

Files: debian/*
Copyright: 2011 Jerome Kieffer <jerome.kieffer@esrf.fr>
           2013-2014 Picca Frederic-Emmanuel <picca@debian.org>
License: GPL-2.0+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy
 of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations
 under the License.
 .
 On Debian systems, the complete text of the Apache-2.0 License
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-3.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. The name of the author may be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

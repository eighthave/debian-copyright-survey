Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Proj4js
Upstream-Contact: MetaCRS Developers <metacrs@lists.osgeo.org>
Source: https://github.com/proj4js/proj4js/releases
Comment: The minified javascript is removed in favor of the plain source
Files-Excluded: dist/proj4.js

Files: *
Copyright: 2014, Mike Adair <madair@dmsolutions.ca>
           2014, Richard Greenwood <rich@greenwoodmap.com>
           2014, Didier Richard <didier.richard@ign.fr>
           2014, Stephen Irons <stephen.irons@clear.net.nz>
           2014, Olivier Terral <oterral@gmail.com>
           2014, Calvin Metcalf <calvin.metcalf@gmail.com>
Comment: The AUTHORS lists additional credits:
 .
 Richard Marsden (http://www.winwaed.com)
  src/projCode/gnom.js
  src/projCode/cea.js
 .
 T. Mittan
  src/projCode/eqdc.js
  src/projCode/equi.js
  src/projCode/merc.js
  src/projCode/mill.js
  src/projCode/omerc.js
  src/projCode/ortho.js
  src/projCode/poly.js
 .
 D. Steinwand
  src/projCode/merc.js
  src/projCode/laea.js
  src/projCode/moll.js
 .
 S. Nelson
  src/projCode/moll.js
License: MIT

Files: debian/*
Copyright: 2014-2015, Bas Couwenberg <sebastic@debian.org>
License: GPL-2+

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-2' file.


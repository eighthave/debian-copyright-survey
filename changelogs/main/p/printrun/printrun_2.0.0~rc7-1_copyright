Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Printrun
Source: https://github.com/kliment/Printrun

Files:     *
Copyright: 2011-2020, Kliment Yanev
           2011-2020, Guillaume Seguin <guillaume@segu.in>
License:   GPL-3.0+
 Printrun is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 Printrun is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with Printrun.  If not, see <http://www.gnu.org/licenses/>.
 .
 All scripts should contain this license note, if not, feel free to ask
 us.  Please note that files where it is difficult to state this
 license note  (such as images) are distributed under the same terms.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

Files:     debian/*
Copyright: 2015-2020, Rock Storm <rockstorm@gmx.com>
           2020, Christoph Berg
           2020, Sven Bartscher
           2020, Boyuan Yang
License:   GPL-3.0+

Files:     printrun/spoolmanager/*
Copyright: 2017, Rock Storm <rockstorm@gmx.com>
License:   GPL-3.0+

Files:     *.appdata.xml
Copyright: 2011-2020, Kliment Yanev
           2011-2020, Guillaume Seguin <guillaume@segu.in>
License:   CC0-1.0
 On Debian systems, the full text of the Creative Commons Zero v1.0
 Universal license can be found in the file
 `/usr/share/common-licenses/CC0-1.0'.

Files:     calibrateextruder.py
Copyright: 2011, Nathan Zadoks
License:   GPL-3.0+

Files:     locale/fr/LC_MESSAGES/pronterface.po
           locale/de/LC_MESSAGES/pronterface.po
           README.i18n
Copyright: 2011, Jonathan Marsden <jmarsden@fastmail.fm>
License:   GPL-3.0+

Files:     locale/nl/LC_MESSAGES/pronterface.po
Copyright: 2011, Ruben Lubbes <ruben.lubbes@gmx.net>
License:   GPL-3.0+

Files:     printrun/power/osx.py
Copyright: 2014, Joseph Benden
License:   GPL-3.0+

Files:     printrun/gl/libtatlin/actors.py
Copyright: 2011, Denis Kobozev
           2013, Guillaume Seguin
License:   GPL-2.0+

Files:     printrun/packer.py
Copyright: 2013, Max Retter
License:   GPL-3.0+

Files:     printrun/gui/bufferedcanvas.py
Copyright: 2005-2006, Daniel Keep
           2011, Duane Johnson
License:   GPL-3.0+

License:   GPL-3.0+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License:   GPL-2.0+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

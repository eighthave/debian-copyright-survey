Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Pygame_sdl2
Source: https://github.com/renpy/pygame_sdl2

Files: *
Copyright: 2012 Andreas Schiffler
                Philip D. Bober
           2014 Tom Rothamel <tom@rothamel.us>
           2014 Patrick Dawson <pat@dw.is>
License: zlib

Files: src/SDL_gfxPrimitives.c
       src/SDL_gfxPrimitives.h
       src/SDL_gfxPrimitives_font.h
       src/alphablit.c
       src/pygame_sdl2/sprite.py
       src/pygame_sdl2/sysfont.py
       src/pygame_sdl2/threads/__init__.py
       src/surface.h
Copyright: A. Schiffler
           2000-2003, 2007  Pete Shinners
           2006 Rene Dudfield
           2007 Marcus von Appen
           2004 Joe Wreschnig
License: LGPL-2+
Comment:
 src/pygame_sdl2/threads/__init__.py states as license "Python license" which
 is misleading. This module was taken from Pygame and is licensed under
 the LGPL-2+ license too.

Files: src/pygame_sdl2/DejaVuSans.ttf
Copyright: Bitstream and Tavmjong Bah
License: DejaVu-License

Files: debian/*
Copyright: 2016-2017, Markus Koschany <apo@debian.org>
License: zlib

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: LGPL-2+
 On Debian systems, the full text of the GNU Library General Public License
 version 2 can be found in the file '/usr/share/common-licenses/LGPL-2'.

License: DejaVu-License
 Bitstream Vera Fonts Copyright
 ------------------------------
 .
 Copyright (c) 2003 by Bitstream, Inc. All Rights Reserved. Bitstream Vera is
 a trademark of Bitstream, Inc.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of the fonts accompanying this license ("Fonts") and associated
 documentation files (the "Font Software"), to reproduce and distribute the
 Font Software, including without limitation the rights to use, copy, merge,
 publish, distribute, and/or sell copies of the Font Software, and to permit
 persons to whom the Font Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright and trademark notices and this permission notice shall
 be included in all copies of one or more of the Font Software typefaces.
 .
 The Font Software may be modified, altered, or added to, and in particular
 the designs of glyphs or characters in the Fonts may be modified and
 additional glyphs or characters may be added to the Fonts, only if the fonts
 are renamed to names not containing either the words "Bitstream" or the word
 "Vera".
 .
 This License becomes null and void to the extent applicable to Fonts or Font
 Software that has been modified and is distributed under the "Bitstream
 Vera" names.
 .
 The Font Software may be sold as part of a larger software package but no
 copy of one or more of the Font Software typefaces may be sold by itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT,
 TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL BITSTREAM OR THE GNOME
 FOUNDATION BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING
 ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE
 FONT SOFTWARE.
 .
 Except as contained in this notice, the names of Gnome, the Gnome
 Foundation, and Bitstream Inc., shall not be used in advertising or
 otherwise to promote the sale, use or other dealings in this Font Software
 without prior written authorization from the Gnome Foundation or Bitstream
 Inc., respectively. For further information, contact: fonts at gnome dot
 org.
 .
 Arev Fonts Copyright
 ------------------------------
 .
 Copyright (c) 2006 by Tavmjong Bah. All Rights Reserved.
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the fonts accompanying this license ("Fonts") and
 associated documentation files (the "Font Software"), to reproduce
 and distribute the modifications to the Bitstream Vera Font Software,
 including without limitation the rights to use, copy, merge, publish,
 distribute, and/or sell copies of the Font Software, and to permit
 persons to whom the Font Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright and trademark notices and this permission notice
 shall be included in all copies of one or more of the Font Software
 typefaces.
 .
 The Font Software may be modified, altered, or added to, and in
 particular the designs of glyphs or characters in the Fonts may be
 modified and additional glyphs or characters may be added to the
 Fonts, only if the fonts are renamed to names not containing either
 the words "Tavmjong Bah" or the word "Arev".
 .
 This License becomes null and void to the extent applicable to Fonts
 or Font Software that has been modified and is distributed under the
 "Tavmjong Bah Arev" names.
 .
 The Font Software may be sold as part of a larger software package but
 no copy of one or more of the Font Software typefaces may be sold by
 itself.
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL
 TAVMJONG BAH BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.
 .
 Except as contained in this notice, the name of Tavmjong Bah shall not
 be used in advertising or otherwise to promote the sale, use or other
 dealings in this Font Software without prior written authorization
 from Tavmjong Bah. For further information, contact: tavmjong @ free
 . fr.


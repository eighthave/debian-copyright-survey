Format-Specification: http://svn.debian.org/wsvn/dep/web/deps/dep5.mdwn?op=file&rev=135
Name: phpBB
Maintainer: Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
Source: http://www.phpbb.com/downloads/olympus.php
	http://www.phpbb.com/languages/

Copyright: 2000, 2002-2011 phpBB Group
	2001, 2004 Tom Beddard, subBlue Design http://www.subblue.com/
License: GPL-2
X-Debian-Comment: phpBB team: http://www.phpbb.com/about/team
	phpBB Lead Developer: naderman (Nils Adermann)
	.
	phpBB Developers: Acyd Burn (Meik Sievertsen) [Lead 09/2005 - 01/2010]
		bantu (Andreas Fischer)
		ckwalsh (Cullen Walsh)
		igorw (Igor Wiedler)
		kellanved (Henry Sudhof)
		nickvergessen (Joas Schilling)
		Oleg (Oleg Pudeyev)
		rxu (Ruslan Uzdenov)
		ToonArmy (Chris Smith)
	.
	Contributions by: leviatan21 (Gabriel Vazquez)
		Raimon (Raimon Meuldijk)
		Xore (Robert Hetzler)
	.
	-- Former Contributors --
	.
	phpBB Project Manager: theFinn (James Atkinson) [Founder - 04/2007]
		SHS` (Jonathan Stanley)
	.
	phpBB Lead Developer: psoTFX (Paul S. Owen) [2001 - 09/2005]
	.
	phpBB Developers: A_Jelly_Doughnut (Josh Woody) [01/2010 - 11/2010]
		APTX (Marek A. Ruszczyński)   [12/2007 - 04/2011]
		Ashe (Ludovic Arnaud)         [10/2002 - 11/2003, 06/2006 - 10/2006]
		BartVB (Bart van Bragt)       [11/2000 - 03/2006]
		DavidMJ (David M.)            [12/2005 - 08/2009]
		dhn (Dominik Dröscher)        [05/2007 - 01/2011]
		GrahamJE (Graham Eames)       [09/2005 - 11/2006]
		TerraFrost (Jim Wigginton)    [04/2009 - 01/2011]
		Vic D'Elfant (Vic D'Elfant)   [04/2007 - 04/2009]

Files: */editor.js
Copyright: 2005, 2007 phpBB Group
	2004 Tom Beddard, subBlue Design http://www.subblue.com/
	2004, 2005 Jim Wigginton (TerraFrost) http://www.frostjedi.com/phpbb
	Christopher Jason Wetherell <chris@massless.org>
License: GPL-2
X-Debian-Comment: http://www.frostjedi.com/terra/scripts/demo/caretBug.html
	http://www.massless.org/mozedit/
	http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130

Files: adm/style/tooltip.js
Copyright: 2006 Alessandro Fulciniti http://web-graphics.com/mtarchive/001717.php
	phpBB Group
License: GPL-2

Files: include/functions_compress.php
Copyright: 2005 phpBB Group
	Zip creation class:
		1998-2000 Tobias Ratschiller <tobias@ratschiller.com>
		2001, 2002 Olivier Müller <om@omnis.ch>
		2001, 2002 Loïc Chapeaux <lolo@phpheaven.net>
		2001, 2002 Marc Delisle <marc@infomarc.info>
	Zip extraction function:
		Alexandre Tedeschi, <alexandrebr@gmail.com>
		2003 phpBB Group (psoTFX and DavidMJ)
		Eric Mueller and Denis125
	Tar/tar.gz compression routine:
		1994 Tom Horsley <tom@ssd.csd.harris.com>
License: GPL-2

Files: include/functions_jabber.php
Copyright: 2007 phpBB Group
	2006 Flyspray.org - Florian Schmitz (floele) http://www.flyspray.org/
	Hans Anderson http://www.hansanderson.com/php/xml
License: GPL-2

Files: include/functions_messenger.php
Copyright: 2005 phpBB Group
	1997-2004 PHP Group, http://pear.php.net
License: GPL-2 and PHP
X-Debian-Comment: http://pear.php.net/package/Net_SMTP
	maybe some BSD licenced authentication methods from
		2002-2003 Richard Heyes
		http://pear.php.net/package/Auth_SASL/

Files: include/functions.php
Copyright: 2005 phpBB Group
	2004-2006 Solar Designer <solar@openwall.com> http://www.openwall.com/phpass
	2006 Project Minerva Team - Chris Smith <chris@project-minerva.org>
License: GPL-2 and PD

Files: include/functions_template.php
Copyright: 2005 phpBB Group
	2001 ispi of Lincoln Inc
	PHPLib templates
License: GPL-2 and LGPL-2.1

Files: include/functions_user.php
Copyright: 2005 phpBB Group
	2006 Flyspray.org http://www.flyspray.org/
License: GPL-2

Files: include/session.php
Copyright: 2005 phpBB Group
	2005-2006 satmd
License: GPL-2
X-Debian-Comment: http://www.php.net/manual/en/function.checkdnsrr.php#61220
	written by satmd, do what you want with it, but keep the author please

Files: include/template.php
Copyright: 2005 phpBB Group
	2001 ispi of Lincoln Inc
License: GPL-2 and LGPL-2.1

Files: includes/captcha/captcha_gd.php
Copyright: 2006 phpBB Group
	2009 Robert Hetzler (Xore) http://xore.ca
License: GPL-2

Files: includes/captcha/plugins/phpbb_recaptcha_plugin.php
Copyright: 2006, 2008 phpBB Group
	2007 reCAPTCHA, Mike Crawford, Ben Maurer, http://recaptcha.net
License: GPL-2 and MIT

Files: include/diff/diff.php
Copyright: 2006 phpBB Group
	2004 Geoffrey T. Dairiki <dairiki@dairiki.org>
	2004-2008 The Horde Project http://www.horde.org
License: GPL-2 and LGPL-2.1

Files: include/diff/engine.php
Copyright: 2006 phpBB Group
	2004 Geoffrey T. Dairiki <dairiki@dairiki.org>
	2004-2008 The Horde Project http://www.horde.org
	2000-2002 Ned Konz
	1988, 1989, 1992, 1993 Free Software Foundation, Inc.
License: GPL-2 and LGPL-2.1 and Perl
X-Debian-Comment: http://search.cpan.org/dist/Algorithm-Diff/lib/Algorithm/Diff.pm#LICENSE
	This program is free software; you can redistribute it and/or modify it under the
	same terms as Perl itself.

Files: include/diff/renderer.php
Copyright: 2006 phpBB Group
	2004 Geoffrey T. Dairiki <dairiki@dairiki.org>
	2004-2008 The Horde Project http://www.horde.org
	2000-2006 Chora http://horde.org/chora/
License: GPL-2 and LGPL-2.1

Files: include/questionnaire/questionnaire.php
Copyright: 2006 phpBB Group
	2007-2008 Johannes Schlueter <johannes@php.net>
License: GPL-2

Files: styles/prosilver/theme/buttons.css
Copyright: 2005, 2007 phpBB Group
	2004 Tom Beddard, subBlue Design http://www.subblue.com/
	2004 Petr Staníček http://www.pixy.cz
License: GPL-2
X-Debian-Comment: http://wellstyled.com/css-nopreload-rollovers.html
	any technique, program code and example used on this page is free for use.

Files: styles/prosilver/theme/tweaks.css
Copyright: 2005, 2007 phpBB Group
	2004 Tom Beddard, subBlue Design http://www.subblue.com/
	2004-2008 positioniseverything.net http://www.positioniseverything.net/easyclearing.html
	2003-2009 Dustin Diaz http://www.dustindiaz.com/min-height-fast-hack
License: GPL-2 and CC-BY-SA-2.5
X-Debian-Comment: stuff from positioniseverything commented out.

Files: styles/prosilver/template/forum_fn.js
Copyright: 2005, 2007 phpBB Group
	2004 Tom Beddard, subBlue Design http://www.subblue.com/
	2005-2008 John Resig, Brandon Aaron & Jörn Zaefferer
	2010 GreatWebGuy http://greatwebguy.com
License: GPL-2 and jQuery
X-Debian-Comment: http://greatwebguy.com/programming/dom/default-html-button-submit-on-enter-with-jquery/

Files: l10n-ar/*
Copyright: 2005-2010 phpBB Group
	2007, 2008, 2010 phpBBArabia.com الدعم العربي لمنتديات phpBB
License: GPL-2

Files: l10n-be/*
Copyright: 2005, 2006, 2009 phpBB Group
	2010 Dionisiy
License: GPL-2

Files: l10n-bg/*
Copyright: 2005, 2006, 2009 phpBB Group
	IoanFilipov SEO http://na4o.com
License: GPL-2

Files: l10n-ca/*
Copyright: 2005, 2006, 2009 phpBB Group
	Isaac Garcia Abrodos http://abrodos.wordpress.com/phpbb
License: GPL-2

Files: l10n-cs/*
Copyright: 2007, 2009, 2010 phpBB.cz http://www.phpbb.cz
	2005, 2006, 2009 phpBB Group
	Ameeck JIRKAS Strongy! Ike_Blaster TCH christian seqwence Majkl vP. Preston
License: GPL-2

Files: l10n-da/*
Copyright: 2006-2009 Olympus DK Team http://www.phpBB3.dk
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-de/*, l10n-de-x-sie/*
Copyright: 2006 phpBB.de http://www.phpbb.de/go/ubersetzerteam
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-el/*
Copyright: 2005, 2006, 2009 phpBB Group
	http://phpbbgr.com/team/
License: GPL-2

Files: l10n-en-us/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-es/*
Copyright: 2005-2007, 2009 phpBB Group
	Huan Manwe for http://www.phpbb-es.com/
	ImagePack by Xoom http://www.muchografico.com
	2005, 2006, 2009 phpBB Group
License: GPL-2+

Files: l10n-es-x-tu/*
Copyright: 2005-2007, 2009, 2010 phpBB Group
	Huan Manwe for http://www.phpbb-es.com/
	ImagePack by Xoom http://www.muchografico.com
License: GPL-2+

Files: l10n-es-ar-x-vos/*
Copyright: 2005-2007, 2009, 2010 phpBB Group
	Huan Manwe for http://www.phpbb-es.com/
	ImagePack by Xoom http://www.muchografico.com
License: GPL-2

Files: l10n-et/*
Copyright: 2005-2009 phpBB Group
	2007, 2008 Amphor - phpbb.ee
License: GPL-2

Files: l10n-eu/*
Copyright: 2005-2007 phpBB Group
	2011 Bixerdo http://www.librezale.org
License: GPL-2

Files: l10n-fa/*
Copyright: 2005, 2006, 2008, 2009 phpBB Group
	2005, 2008 www.Maghsad.net http://www.phpBB.Maghsad.com
License: GPL-2

Files: l10n-fi/*
Copyright: 2005, 2006, 2009 phpBB Group
	2007 Martti Lokka
	2005, 2006 Lurttinen@phpbbsuomi.com
License: GPL-2

Files: l10n-fr/*
Copyright: 2005, 2006, 2009 phpBB Group
	2005, 2009 phpBB.fr
License: GPL-2

Files: l10n-gl/*
Copyright: 2005, 2006, 2009 phpBB Group
	2007-2010 CiberIrmandade da Fala http://www.ciberirmandade.org/foros
License: GPL-2

Files: l10n-he/*
Copyright: 2008 phpBB הישראלי - www.phpBB.co.il
	2007 phpBBHebrew Group
	2006, 2007 phpBB Group
License: GPL-2

Files: l10n-hr/*
Copyright: 2005, 2011 phpBB Group
License: GPL-2

Files: l10n-hu/*
Copyright: 2007, 2009 Magyar phpBB Közösség fordítók http://phpbb.hu/
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-id/*
Copyright: 2009, 2010 Zourbuth Studio
	2005, 2006 phpBB Group
License: GPL-2

Files: l10n-it/*
Copyright: 2011 phpBBItalia.net
	2010 phpBB.it
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-ja/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-ku/*
Copyright: 2005, 2006, 2008, 2009, 2011 phpBB Group
License: GPL-2

Files: l10n-lt/*
Copyright: 2003, 2005, 2007 Vilius Šumskas http://www.tekila.lt
	2005-2012 phpBB Group
License: GPL-2

Files: l10n-nl/*
Copyright: 2005, 2006, 2009 phpBB Group
	2007, 2009 phpBB.nl Vertaling <vertaalteam@phpbb.nl>
License: GPL-2

Files: l10n-nl-x-formal/*
Copyright: 2005, 2006, 2008 phpBB Group
	2007, 2009 phpBBservice.nl http://www.phpBBservice.nl/
License: GPL-2

Files: l10n-pl/*
Copyright: 2006-2011 phpBB3.PL Group http://phpbb3.pl
	2005 phpBB Group
License: GPL-2

Files: l10n-pt/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-pt-br/*
Copyright: 2005, 2006 phpBB Group
	2012 Suporte phpBB
License: GPL-2

Files: l10n-ro/*, l10n-ru/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-sk/*
Copyright: 2005-2007 phpBB Group
License: GPL-2

Files: l10n-sl/*
Copyright: 2005, 2006, 2008, 2009, 2011 phpBB Group
License: GPL-2

Files: l10n-sr-cyrl/*, l10n-sr/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-sv/*
Copyright: 2006-2011 Peetra & phpBB Sweden http://www.phpbb-se.com/forum/viewtopic.php?t=6096
	2006-2011 Swedish translation team http://www.phpbb-se.com
	2008-2011 Petra Brandt
	2006-2008 Jonathan Gulbrandsen
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-th/*, l10n-tr/*
Copyright: 2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-uk/*
Copyright: 2005-2011 Українська підтримка phpBB http://phpbb.com.ua/
	2005, 2006, 2009 phpBB Group
License: GPL-2

Files: l10n-ur/*
Copyright: 2005, 2006, 2009-2011 phpBB Group
License: GPL-2

Files: l10n-vi/*
Copyright: 2009, 2010 ytuongsangtaovn.com
	2009 phpBB Group
	2006, 2007 phpBBVietNam Group
License: GPL-2

Files: l10n-zh-cmn-hans/*
Copyright: 2005, 2007, 2009 phpbbchina.com 
	2005 phpBB Group
License: GPL-2

Files: l10n-zh-cmn-hant/*
Copyright: 2001-2007, 2009 竹貓星球 phpBB TW Group (心靈捕手, Mac, yoshika, 動機不明) http://phpbb-tw.net/
	2005, 2006 phpBB Group
License: GPL-2

Files: debian/*
Copyright: 2004-2008 Jeroen van Wolffelaar <jeroen@wolffelaar.nl>
	2005-2009 Thijs Kinkhorst <thijs@debian.org>
	2010, 2011 Jean-Marc Roth <jm@roth.lu>
	2010-2012 David Prévot <taffit@debian.org>
License: GPL-2
X-Debian-Comment: Thanks a lot to Paul Slootman <paul@debian.org> who
	provided numerous tips and hints, and did lots of testing.

Files: debian/po/cs.po
Copyright: 2004, 2010 Miroslav Kure <kurem@debian.cz>
License: GPL-2

Files: debian/po/da.po
Copyright: 2010 Joe Hansen <joedalton2@yahoo.dk>
License: GPL-2

Files: debian/po/de.po
Copyright: 2006, 2010 Matthias Julius <mdeb@julius-net.net>
License: GPL-2

Files: debian/po/es.po
Copyright: 2010 Software in the Public Interest
	2010 Francisco Javier Cuadrado <fcocuadrado@gmail.com>
License: GPL-2

Files: debian/po/fr.po
Copyright: 2004, 2010 Debian French l10n team <debian-l10n-french@lists.debian.org>
	2004 Eric Madesclair <eric-m@wanadoo.fr>
	2010 David Prévot <david@tilapin.org>
License: GPL-2

Files: debian/po/it.po
Copyright: 2005, 2010 Luca Monducci <luca.mo@tiscali.it>
License: GPL-2

Files: debian/po/ja.po
Copyright: 2004, 2010 Hideki Yamane (Debian-JP) <henrich@debian.or.jp>
License: GPL-2

Files: debian/po/nl.po
Copyright: 2006, 2010 Thijs Kinkhorst <kink@squirrelmail.org>
License: GPL-2

Files: debian/po/pt.po
Copyright: 2008, 2010 Américo Monteiro <a_monteiro@netcabo.pt>
License: GPL-2

Files: debian/po/ru.po
Copyright: 2010 Yuri Kozlov <yuray@komyakino.ru>
	2006 Yuriy Talakan' <yt@amur.elektra.ru>
License: GPL-2

Files: debian/po/sv.po
Copyright: 2010 Martin Bagge <brother@bsnet.se>
	2010 Martin Ågren <martin.agren@gmail.com>
	2005 Daniel Nylander <po@danielnylander.se>
License: GPL-2

Files: debian/po/vi.po
Copyright: 2005-2010 Clytie Siddall <clytie@riverland.net.au>
License: GPL-2


License: Artistic
 On Debian systems, the full text of the Artistic License can be found in
 the file `/usr/share/common-licenses/Artistic'.

License: CC-BY-SA-2.5
 Attribution-ShareAlike 2.5
 .
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS LICENSE DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE INFORMATION PROVIDED, AND
 DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM ITS USE.
 .
 License
 .
 THE WORK (AS DEFINED BELOW) IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS
 PUBLIC LICENSE ("CCPL" OR "LICENSE"). THE WORK IS PROTECTED BY COPYRIGHT AND/OR
 OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER THIS
 LICENSE OR COPYRIGHT LAW IS PROHIBITED.
 .
 BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE
 BOUND BY THE TERMS OF THIS LICENSE. THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED
 HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.
 .
 1. Definitions
 .
    a. "Collective Work" means a work, such as a periodical issue, anthology or
       encyclopedia, in which the Work in its entirety in unmodified form, along
       with a number of other contributions, constituting separate and independent
       works in themselves, are assembled into a collective whole. A work that
       constitutes a Collective Work will not be considered a Derivative Work (as
       defined below) for the purposes of this License.
 .
    b. "Derivative Work" means a work based upon the Work or upon the Work and
       other pre-existing works, such as a translation, musical arrangement,
       dramatization, fictionalization, motion picture version, sound recording,
       art reproduction, abridgment, condensation, or any other form in which the
       Work may be recast, transformed, or adapted, except that a work that
       constitutes a Collective Work will not be considered a Derivative Work for
       the purpose of this License. For the avoidance of doubt, where the Work is
       a musical composition or sound recording, the synchronization of the Work
       in timed-relation with a moving image ("synching") will be considered a
       Derivative Work for the purpose of this License.
 .
    c. "Licensor" means the individual or entity that offers the Work under the
       terms of this License.
 .
    d. "Original Author" means the individual or entity who created the Work.
 .
    e. "Work" means the copyrightable work of authorship offered under the terms
       of this License.
 .
    f. "You" means an individual or entity exercising rights under this License
       who has not previously violated the terms of this License with respect to
       the Work, or who has received express permission from the Licensor to
       exercise rights under this License despite a previous violation.
 .
    g. "License Elements" means the following high-level license attributes as
       selected by Licensor and indicated in the title of this License:
       Attribution, ShareAlike.
 .
 2. Fair Use Rights. Nothing in this license is intended to reduce, limit, or 
    restrict any rights arising from fair use, first sale or other limitations on
    the exclusive rights of the copyright owner under copyright law or other
    applicable laws.
 .
 3. License Grant. Subject to the terms and conditions of this License, Licensor
    hereby grants You a worldwide, royalty-free, non-exclusive, perpetual (for
    the duration of the applicable copyright) license to exercise the rights in
    the Work as stated below:
 .
    a. to reproduce the Work, to incorporate the Work into one or more Collective
       Works, and to reproduce the Work as incorporated in the Collective Works;
 .
    b. to create and reproduce Derivative Works;
 .
    c. to distribute copies or phonorecords of, display publicly, perform
       publicly, and perform publicly by means of a digital audio transmission the
       Work including as incorporated in Collective Works;
 .
    d. to distribute copies or phonorecords of, display publicly, perform
       publicly, and perform publicly by means of a digital audio transmission
       Derivative Works.
 .
    e. For the avoidance of doubt, where the work is a musical composition:
  .
       i. Performance Royalties Under Blanket Licenses. Licensor waives the
           exclusive right to collect, whether individually or via a performance
           rights society (e.g. ASCAP, BMI, SESAC), royalties for the public
           performance or public digital performance (e.g. webcast) of the Work.
 .
       ii. Mechanical Rights and Statutory Royalties. Licensor waives the 
           exclusive right to collect, whether individually or via a music rights
           society or designated agent (e.g. Harry Fox Agency), royalties for any
           phonorecord You create from the Work ("cover version") and distribute,
           subject to the compulsory license created by 17 USC Section 115 of the
           US Copyright Act (or the equivalent in other jurisdictions).
 .
    f. Webcasting Rights and Statutory Royalties. For the avoidance of doubt,
       where the Work is a sound recording, Licensor waives the exclusive right to
       collect, whether individually or via a performance-rights society (e.g.
       SoundExchange), royalties for the public digital performance (e.g. webcast)
       of the Work, subject to the compulsory license created by 17 USC Section
       114 of the US Copyright Act (or the equivalent in other jurisdictions).
 .
    The above rights may be exercised in all media and formats whether now known
    or hereafter devised. The above rights include the right to make such
    modifications as are technically necessary to exercise the rights in other
    media and formats. All rights not expressly granted by Licensor are hereby
    reserved.
 .
 4. Restrictions.The license granted in Section 3 above is expressly made subject
    to and limited by the following restrictions:
 .
    a. You may distribute, publicly display, publicly perform, or publicly
       digitally perform the Work only under the terms of this License, and You
       must include a copy of, or the Uniform Resource Identifier for, this
       License with every copy or phonorecord of the Work You distribute, publicly
       display, publicly perform, or publicly digitally perform. You may not offer
       or impose any terms on the Work that alter or restrict the terms of this
       License or the recipients' exercise of the rights granted hereunder. You
       may not sublicense the Work. You must keep intact all notices that refer to
       this License and to the disclaimer of warranties. You may not distribute,
       publicly display, publicly perform, or publicly digitally perform the Work
       with any technological measures that control access or use of the Work in a
       manner inconsistent with the terms of this License Agreement. The above
       applies to the Work as incorporated in a Collective Work, but this does not
       require the Collective Work apart from the Work itself to be made subject
       to the terms of this License. If You create a Collective Work, upon notice
       from any Licensor You must, to the extent practicable, remove from the
       Collective Work any credit as required by clause 4(c), as requested. If You
       create a Derivative Work, upon notice from any Licensor You must, to the
       extent practicable, remove from the Derivative Work any credit as required
       by clause 4(c), as requested.
 .
   b. You may distribute, publicly display, publicly perform, or publicly
      digitally perform a Derivative Work only under the terms of this License, a
      later version of this License with the same License Elements as this
      License, or a Creative Commons iCommons license that contains the same
      License Elements as this License (e.g. Attribution-ShareAlike 2.5 Japan).
      You must include a copy of, or the Uniform Resource Identifier for, this
      License or other license specified in the previous sentence with every copy
      or phonorecord of each Derivative Work You distribute, publicly display,
      publicly perform, or publicly digitally perform. You may not offer or impose
      any terms on the Derivative Works that alter or restrict the terms of this
      License or the recipients' exercise of the rights granted hereunder, and You
      must keep intact all notices that refer to this License and to the
      disclaimer of warranties. You may not distribute, publicly display, publicly
      perform, or publicly digitally perform the Derivative Work with any
      technological measures that control access or use of the Work in a manner
      inconsistent with the terms of this License Agreement. The above applies to
      the Derivative Work as incorporated in a Collective Work, but this does not
      require the Collective Work apart from the Derivative Work itself to be made
      subject to the terms of this License.
 .
   c. If you distribute, publicly display, publicly perform, or publicly digitally
      perform the Work or any Derivative Works or Collective Works, You must keep
      intact all copyright notices for the Work and provide, reasonable to the
      medium or means You are utilizing: (i) the name of the Original Author (or
      pseudonym, if applicable) if supplied, and/or (ii) if the Original Author
      and/or Licensor designate another party or parties (e.g. a sponsor
      institute, publishing entity, journal) for attribution in Licensor's
      copyright notice, terms of service or by other reasonable means, the name of
      such party or parties; the title of the Work if supplied; to the extent
      reasonably practicable, the Uniform Resource Identifier, if any, that
      Licensor specifies to be associated with the Work, unless such URI does not
      refer to the copyright notice or licensing information for the Work; and in
      the case of a Derivative Work, a credit identifying the use of the Work in
      the Derivative Work (e.g., "French translation of the Work by Original
      Author," or "Screenplay based on original Work by Original Author"). Such
      credit may be implemented in any reasonable manner; provided, however, that
      in the case of a Derivative Work or Collective Work, at a minimum such
      credit will appear where any other comparable authorship credit appears and
      in a manner at least as prominent as such other comparable authorship
      credit.
 .
 5. Representations, Warranties and Disclaimer
 .
    UNLESS OTHERWISE AGREED TO BY THE PARTIES IN WRITING, LICENSOR OFFERS THE WORK
    AS-IS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND CONCERNING THE
    MATERIALS, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT
    LIMITATION, WARRANTIES OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR
    PURPOSE, NONINFRINGEMENT, OR THE ABSENCE OF LATENT OR OTHER DEFECTS, ACCURACY,
    OR THE PRESENCE OF ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE. SOME
    JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO SUCH
    EXCLUSION MAY NOT APPLY TO YOU.
 .
 6. Limitation on Liability. EXCEPT TO THE EXTENT REQUIRED BY APPLICABLE LAW, IN
    NO EVENT WILL LICENSOR BE LIABLE TO YOU ON ANY LEGAL THEORY FOR ANY SPECIAL,
    INCIDENTAL, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES ARISING OUT OF THIS
    LICENSE OR THE USE OF THE WORK, EVEN IF LICENSOR HAS BEEN ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGES.
 .
 7. Termination
 .
    a. This License and the rights granted hereunder will terminate automatically
       upon any breach by You of the terms of this License. Individuals or
       entities who have received Derivative Works or Collective Works from You
       under this License, however, will not have their licenses terminated
       provided such individuals or entities remain in full compliance with those
       licenses. Sections 1, 2, 5, 6, 7, and 8 will survive any termination of
       this License.
 .
    b. Subject to the above terms and conditions, the license granted here is
       perpetual (for the duration of the applicable copyright in the Work).
       Notwithstanding the above, Licensor reserves the right to release the Work
       under different license terms or to stop distributing the Work at any time;
       provided, however that any such election will not serve to withdraw this
       License (or any other license that has been, or is required to be, granted
       under the terms of this License), and this License will continue in full
       force and effect unless terminated as stated above.
 .
 8. Miscellaneous
 .
    a. Each time You distribute or publicly digitally perform the Work or a
       Collective Work, the Licensor offers to the recipient a license to the Work
       on the same terms and conditions as the license granted to You under this
       License.
 .
    b. Each time You distribute or publicly digitally perform a Derivative Work,
       Licensor offers to the recipient a license to the original Work on the same
       terms and conditions as the license granted to You under this License.
 .
    c. If any provision of this License is invalid or unenforceable under
       applicable law, it shall not affect the validity or enforceability of the
       remainder of the terms of this License, and without further action by the
       parties to this agreement, such provision shall be reformed to the minimum
       extent necessary to make such provision valid and enforceable.
 .
    d. No term or provision of this License shall be deemed waived and no breach
       consented to unless such waiver or consent shall be in writing and signed
       by the party to be charged with such waiver or consent.
 .
    e. This License constitutes the entire agreement between the parties with
       respect to the Work licensed here. There are no understandings, agreements
       or representations with respect to the Work not specified here. Licensor
       shall not be bound by any additional provisions that may appear in any
       communication from You. This License may not be modified without the mutual
       written agreement of the Licensor and You.
 .
 Creative Commons is not a party to this License, and makes no warranty whatsoever
 in connection with the Work. Creative Commons will not be liable to You or any
 party on any legal theory for any damages whatsoever, including without
 limitation any general, special, incidental or consequential damages arising in
 connection to this license. Notwithstanding the foregoing two (2) sentences, if
 Creative Commons has expressly identified itself as the Licensor hereunder, it
 shall have all rights and obligations of Licensor.
 .
 Except for the limited purpose of indicating to the public that the Work is
 licensed under the CCPL, neither party will use the trademark "Creative Commons"
 or any related trademark or logo of Creative Commons without the prior written
 consent of Creative Commons. Any permitted use will be in compliance with
 Creative Commons' then-current trademark usage guidelines, as may be published on
 its website or otherwise made available upon request from time to time.
 .
 Creative Commons may be contacted at http://creativecommons.org/.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version. 
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
 
License: jQuery
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
    a) the GNU General Public License as published by the Free Software
       Foundation; version 2.
 .
    b) the MIT license.
 .
 cf. GPL-2+ or MIT

License: LGPL-2.1
 Section 3 of the LGPL states that any code derived from an LGPL
 application may be relicenced under the GPL, this applies to this source
 .
 On Debian systems, the full text of the Lesser GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: Perl
 This program is free software; you can redistribute it and/or modify
 it under the terms of either:
 .
    a) the GNU General Public License as published by the Free Software
       Foundation; either version 1, or (at your option) any later
       version, or
 .
    b) the "Artistic License" which comes with Perl.
 .
 cf. GPL-2+ or Artistic

License: PD
 There's absolutely no warranty.
 .
 Please be sure to update the Version line if you edit this file in any way.
 It is suggested that you leave the main version number intact, but indicate
 your project name (after the slash) and add your own revision information.
 .
 Please do not change the "private" password hashing method implemented in
 here, thereby making your hashes incompatible.  However, if you must, please
 change the hash type identifier (the "$P$") to something different.
 .
 Obviously, since this code is in the public domain, the above are not
 requirements (there can be none), but merely suggestions.

License: PHP
 -------------------------------------------------------------------- 
                  The PHP License, version 3.01
 Copyright (c) 1999 - 2010 The PHP Group. All rights reserved.
 -------------------------------------------------------------------- 
 .
 Redistribution and use in source and binary forms, with or without
 modification, is permitted provided that the following conditions
 are met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
 .
  3. The name "PHP" must not be used to endorse or promote products
     derived from this software without prior written permission. For
     written permission, please contact group@php.net.
 .
  4. Products derived from this software may not be called "PHP", nor
     may "PHP" appear in their name, without prior written permission
     from group@php.net.  You may indicate that your software works in
     conjunction with PHP by saying "Foo for PHP" instead of calling
     it "PHP Foo" or "phpfoo"
 .
  5. The PHP Group may publish revised and/or new versions of the
     license from time to time. Each version will be given a
     distinguishing version number.
     Once covered code has been published under a particular version
     of the license, you may always continue to use it under the terms
     of that version. You may also choose to use such covered code
     under the terms of any subsequent version of the license
     published by the PHP Group. No one other than the PHP Group has
     the right to modify the terms applicable to covered code created
     under this License.
 .
  6. Redistributions of any form whatsoever must retain the following
     acknowledgment:
     "This product includes PHP software, freely available from
     <http://www.php.net/software/>".
 .
 THIS SOFTWARE IS PROVIDED BY THE PHP DEVELOPMENT TEAM ``AS IS'' AND 
 ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE PHP
 DEVELOPMENT TEAM OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 -------------------------------------------------------------------- 
 .
 This software consists of voluntary contributions made by many
 individuals on behalf of the PHP Group.
 .
 The PHP Group can be contacted via Email at group@php.net.
 .
 For more information on the PHP Group and the PHP project, 
 please see <http://www.php.net>.
 .
 PHP includes the Zend Engine, freely available at
 <http://www.zend.com>.
 

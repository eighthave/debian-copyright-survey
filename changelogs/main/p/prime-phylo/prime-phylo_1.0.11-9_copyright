Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PrIME
Upstream-Contact: Erik Sjolund <erik.sjolund@gmail.com>
Source: http://prime.sbc.su.se/download/

Files: *
Copyright: 2002-2012 Lars Arvestad<arve@csc.kth.se>,
                    Bengt Sennblad<bengt.sennblad@ki.se>,
                    Jens Lagergren,
                    Peter Andersson,
                    Mattias Franberg
                    Erik Sjolund
                    Joel Sjostrand,
                    Ann-Charlotte Sonnhammer,
                    Ikram Ullah,
                    Orjan Akerborg
License: GPL-3

Files: debian/*
Copyright: 2012 Erik Sjolund <erik.sjolund@gmail.com>
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/cxx/libraries/prime/PRNG.hh
Copyright: 1997 - 2002 Takuji Nishimura, Makoto Matsumoto
           2003 - 2012 Lars Arvestad<arve@csc.kth.se>,
                    Bengt Sennblad<bengt.sennblad@ki.se>,
                    Mattias Franberg
License: BSD-3-clause

Files: src/cxx/libraries/prime/beep2blas.hh
Copyright: 2001-2005 The Trustees of Indiana University. All rights reserved.
           1998-2001 University of Notre Dame. All rights reserved
                     Andrew Lumsdaine, Jeremy G. Siek, Lie-Quan Lee
           2007-2012 Bengt Sennblad <bengt.sennblad@ki.se>
License: MTL

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. The names of its contributors may not be used to endorse or promote
 products derived from this software without specific prior written
 permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MTL
 Indiana University has the exclusive rights to license this product under the following license.
 .
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
         1.      All redistributions of source code must retain the above  copyright notice,
                 the list of authors in the original source code,  this list of conditions and
                 the disclaimer listed in this license;
         2.      All redistributions in binary form must reproduce the above  copyright notice,
                 this list of conditions and the disclaimer listed  in this license in the
                 documentation and/or other materials  provided with the distribution;
         3.      Any documentation included with all redistributions must include  the
                 following acknowledgement:
                 "This product includes software developed at the University of Notre Dame and
                  the Pervasive Technology Labs at Indiana University. For technical information
                  contact Andrew Lumsdaine at the Pervasive Technology Labs at Indiana University.
                  For administrative and license questions contact the  Advanced Research and
                  Technology Institute at 1100 Waterway  Blvd. Indianapolis, Indiana 46202,
                  phone 317-274-5905, fax  317-274-5902."
                  Alternatively, this acknowledgement may appear in the software itself, and
                  wherever such third-party acknowledgments normally appear.
         4.      The name "MTL" shall not be used to endorse or promote  products derived from
                 this software without prior written  permission from Indiana University. For
                 written permission, please  contact Indiana University Advanced Research
                 & Technology  Institute.
         5.      Products derived from this software may not be called "MTL", nor may "MTL"
                 appear in their name, without  prior written permission of Indiana University
                 Advanced Research & Technology Institute.
 .
 Indiana University provides no reassurances that the source code provided does not infringe the
 patent or any other intellectual property rights of any other entity. Indiana University disclaims
 any liability to any recipient for claims brought by any other entity based on infringement of
 intellectual property rights or otherwise.
 .
 LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH NO WARRANTIES AS TO CAPABILITIES
 OR ACCURACY ARE MADE. INDIANA UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT
 SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR OTHER PROPRIETARY RIGHTS.
 INDIANA UNIVERSITY MAKES NO WARRANTIES THAT SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN
 HORSES", "TRAP DOORS", "WORMS", OR OTHER HARMFUL CODE. LICENSEE ASSUMES THE ENTIRE RISK AS TO THE
 PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS, AND TO THE PERFORMANCE AND VALIDITY OF
 INFORMATION GENERATED USING SOFTWARE.

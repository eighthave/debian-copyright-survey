Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PageEdit
Source: https://github.com/Sigil-Ebook/PageEdit
Files-Excluded:
 gumbo_subtree/benchmarks
 javascript/jquery-2.2.4.min.js
 javascript/jquery.scrollTo-2.1.2-min.js
 dictionaries

Files: *
Copyright:
           2009-2011 Strahinja Markovic  <strahinja.markovic@gmail.com>
           2011-2013 John Schember <john@nachtimwald.com>
           2012      Grant Drake
           2012-2013 Dave Heiland
           2015-2020 Kevin B. Hendricks
           2019-2020 Doug Massay
License: GPL-3+

Files: gumbo_subtree/*
Copyright: 2010-2013 Google Inc.
           2015-2016 Kevin B. Hendricks, Stratford, Ontario Canada
           2017 Kovid Goyal <kovid@kovidgoyal.net>
License: Apache-2.0

Files: gumbo_subtree/src/utf8.c
Copyright: 2008-2009 Bjoern Hoehrmann <bjoern@hoehrmann.de>
           2010 Google Inc.
License: Apache-2.0 and MIT

Files: debian/*
Copyright: 2020 Mattia Rizzolo <mattia@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pytsk
Source: https://github.com/py4n6/pytsk/

Files: *
Copyright: 2010-2013 Michael Cohen <scudette@gmail.com>
License: Apache-2.0

Files: generate_bindings.py
Copyright: 2012 Joachim Metz <joachim.metz@gmail.com>
License: Apache-2.0

Files: run_tests.py
Copyright: 2012 Kristinn Gudjonsson <kiddi@kiddaland.net>
           2013 Joachim Metz <joachim.metz@gmail.com>
License: Apache-2.0

Files: tests/*
Copyright: 2013 Joachim Metz <joachim.metz@gmail.com>
License: Apache-2.0

Files: talloc/*
Copyright: 2004 Andrew Tridgell
           2006 Stefan Metzmacher 
License: LGPL-3.0+

Files: debian/*
Copyright: 2015 Hilko Bengen <bengen@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: LGPL-3.0+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

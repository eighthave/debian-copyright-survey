Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PHP_CodeCoverage
Upstream-Contact: Sebastian Bergmann <sb@sebastian-bergmann.de>
Source: https://github.com/sebastianbergmann/php-code-coverage
Files-Excluded: src/CodeCoverage/Report/HTML/Renderer/Template/css/bootstrap.min.css
                src/CodeCoverage/Report/HTML/Renderer/Template/js/bootstrap.min.js
                src/CodeCoverage/Report/HTML/Renderer/Template/js/html5shiv.min.js
                src/CodeCoverage/Report/HTML/Renderer/Template/js/jquery.min.js

Files: *
Copyright: 2009-2014, Sebastian Bergmann <sb@sebastian-bergmann.de>
License: BSD-3-clause

Files: debian/missing-sources/d3.js
       src/CodeCoverage/Report/HTML/Renderer/Template/js/d3.min.js
Copyright: 2010-2014, Michael Bostock
License: BSD-3-clause-MB
Comment: missing source downloaded from <https://github.com/mbostock/d3>
 using the version from the v3.4.11 tag where the minified embedded
 JavaScript checked identical.

Files: debian/missing-sources/nv.d3.js
       src/CodeCoverage/Report/HTML/Renderer/Template/css/nv.d3.css
       src/CodeCoverage/Report/HTML/Renderer/Template/js/nv.d3.min.js
Copyright: 2011, 2012, Novus Partners, Inc. <https://www.novus.com/>
License: Apache-2.0
Comment: missing source downloaded from <https://github.com/novus/nvd3>
 using the version from the v1.1.15-beta tag, where the minified
 embedded JavaScript has been checked identical.

Files: debian/missing-sources/respond.js
       src/CodeCoverage/Report/HTML/Renderer/Template/js/respond.min.js
       src/CodeCoverage/Report/HTML/Renderer/Template/fonts/*
Copyright: 2012, Scott Jehl <scottjehl@gmail.com>
           2011-2014, Twitter, Inc.
License: Expat
Comment: missing source downloaded from <https://github.com/scottjehl/Respond>
 using the version from the 1.4.2 tag, where the minified embedded
 JavaScript has been checked identical.

Files: src/CodeCoverage/Report/HTML/Renderer/Template/js/holder.js
Copyright: 2012-2014, Ivan Malopinsky <http://imsky.co>
           2011-2012, David Chambers <dc@hashify.me>
           Jonathan Neal <jonathantneal+github@gmail.com>
           2005-2010, Diego Perini and NWBOX S.a.s. <diego.perini@gmail.com>
           2011, Jed Schmidt <http://jed.is>
License: Expat with attribution exception and WTF-2 and CC0-1.0 and Expat

License: Apache-2.0
 On Debian systems, the full text of the Apache license version 2 can be found
 in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 .
 * Neither the name of Sebastian Bergmann nor the names of his
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause-MB
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 * The name Michael Bostock may not be used to endorse or promote products
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL MICHAEL BOSTOCK BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 Any copyright is dedicated to the Public Domain.
 .
 1. Copyright and Related Rights.
 .
 A Work made available under CC0 may be protected by copyright and related or
 neighboring rights ("Copyright and Related Rights"). Copyright and Related
 Rights include, but are not limited to, the following:
 .
 i.   the right to reproduce, adapt, distribute, perform, display, communicate,
      and translate a Work;
 .
 ii.  moral rights retained by the original author(s) and/or performer(s);
 .
 iii. publicity and privacy rights pertaining to a person's image or likeness
      depicted in a Work;
 .
 iv.  rights protecting against unfair competition in regards to a Work, subject
      to the limitations in paragraph 4(a), below;
 .
 v.   rights protecting the extraction, dissemination, use and reuse of data in
      a Work;
 .
 vi.  database rights (such as those arising under Directive 96/9/EC of the
      European Parliament and of the Council of 11 March 1996 on the legal
      protection of databases, and under any national implementation thereof,
      including any amended or successor version of such directive); and
 .
 vii. other similar, equivalent or corresponding rights throughout the world
      based on applicable law or treaty, and any national implementations
      thereof.
 .
 2. Waiver.
 .
 To the greatest extent permitted by, but not in contravention of, applicable
 law, Affirmer hereby overtly, fully, permanently, irrevocably and
 unconditionally waives, abandons, and surrenders all of Affirmer's Copyright
 and Related Rights and associated claims and causes of action, whether now
 known or unknown (including existing as well as future claims and causes of
 action), in the Work (i) in all territories worldwide, (ii) for the maximum
 duration provided by applicable law or treaty (including future time
 extensions), (iii) in any current or future medium and for any number of
 copies, and (iv) for any purpose whatsoever, including without limitation
 commercial, advertising or promotional purposes (the "Waiver"). Affirmer makes
 the Waiver for the benefit of each member of the public at large and to the
 detriment of Affirmer's heirs and successors, fully intending that such Waiver
 shall not be subject to revocation, rescission, cancellation, termination, or
 any other legal or equitable action to disrupt the quiet enjoyment of the Work
 by the public as contemplated by Affirmer's express Statement of Purpose.
 .
 3. Public License Fallback.
 .
 Should any part of the Waiver for any reason be judged legally invalid or
 ineffective under applicable law, then the Waiver shall be preserved to the
 maximum extent permitted taking into account Affirmer's express Statement of
 Purpose. In addition, to the extent the Waiver is so judged Affirmer hereby
 grants to each affected person a royalty-free, non transferable, non
 sublicensable, non exclusive, irrevocable and unconditional license to exercise
 Affirmer's Copyright and Related Rights in the Work (i) in all territories
 worldwide, (ii) for the maximum duration provided by applicable law or treaty
 (including future time extensions), (iii) in any current or future medium and
 for any number of copies, and (iv) for any purpose whatsoever, including
 without limitation commercial, advertising or promotional purposes (the
 "License"). The License shall be deemed effective as of the date CC0 was
 applied by Affirmer to the Work. Should any part of the License for any reason
 be judged legally invalid or ineffective under applicable law, such partial
 invalidity or ineffectiveness shall not invalidate the remainder of the
 License, and in such case Affirmer hereby affirms that he or she will not
 (i) exercise any of his or her remaining Copyright and Related Rights in the
 Work or (ii) assert any associated claims and causes of action with respect to
 the Work, in either case contrary to Affirmer's express Statement of Purpose.
 .
 4. Limitations and Disclaimers.
 .
 a. No trademark or patent rights held by Affirmer are waived, abandoned,
    surrendered, licensed or otherwise affected by this document.
 .
 b. Affirmer offers the Work as-is and makes no representations or warranties of
    any kind concerning the Work, express, implied, statutory or otherwise,
    including without limitation warranties of title, merchantability, fitness
    for a particular purpose, non infringement, or the absence of latent or
    other defects, accuracy, or the present or absence of errors, whether or not
    discoverable, all to the greatest extent permissible under applicable law.
 .
 c. Affirmer disclaims responsibility for clearing rights of other persons that
    may apply to the Work or any use thereof, including without limitation any
    person's Copyright and Related Rights in the Work. Further, Affirmer
    disclaims responsibility for obtaining any necessary consents, permissions
    or other rights required for any use of the Work.
 .
 d. Affirmer understands and acknowledges that Creative Commons is not a party
    to this document and has no duty or obligation with respect to this CC0 or
    use of the Work.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: Expat with attribution exception
 Commercial use requires attribution.

License: WTF-2
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 .
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 0. You just DO WHAT THE FUCK YOU WANT TO.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ProbCons
Source: http://probcons.stanford.edu/download.html
Upstream-Contact: Chuong Do <chuongdo@cs.stanford.edu>

Files: *
Copyright: 2005-2009 Arun S Konagurthu <arun@bx.psu.edu>
                     The University of Melbourne.
License: PD
 PROBCONS has been made  freely  available  as  PUBLIC  DOMAIN
 software and hence is not subject to copyright in the  United
 States.  This system and/or any portion of  the  source  code
 may be used, modified, or redistributed without restrictions.
 PROBCONS is distributed WITHOUT WARRANTY, express or implied.
 The authors accept NO LEGAL LIABILITY OR  RESPONSIBILITY  for
 loss due to reliance on the program.

Files: debian/*
Copyright: 2006-2011 Charles Plessy <plessy@debian.org>
           2010-2012 Andreas Tille <tille@debian.org>
License: BSD-3-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 .
 On Debian systems you can find a copy of BSD license text at
 /usr/share/common-licenses/BSD.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyprof2calltree
Source: https://github.com/pwaller/pyprof2calltree/
Upstream-Contact: Peter Waller <p@pwaller.net>

Files: *
Copyright: 2006-2008  David Allouche, Jp Calderone, Itamar Shtull-Trauring
           2006-2017  Johan Dahlin
           2008  Olivier Grisel <olivier.grisel@ensta.org>
           2008  David Glick
           2013  Steven Maude
           2013-2018  Peter Waller <p@pwaller.net>
           2013  Lukas Graf <lukas.graf@4teamwork.ch>
           2013  Jamie Wong <http://jamie-wong.com>
           2013  Yury V. Zaytsev <yury@shurup.com>
           2014  Michael Droettboom <mdroe@stsci.edu>
           2015  Zev Benjamin <zev@mit.edu>
           2018  Jon Dufresne <jon.dufresne@gmail.com>
License: MIT
Notes: Copyright holders were established by examining the original
 lsprofcalltree.py at https://people.gnome.org/~johan/lsprofcalltree.py,
 in combination with pyprof2calltree.py, and then clarified using LICENSE
 and the CONTRIBUTORS.txt file it refers to.  Missing date ranges for
 people listed in the CONTRIBUTORS.txt file were manually populated
 with the consultation of upstream git history; however, email addresses
 that were not on in a ©, (c), or Copyright header, nor in LICENSE or
 CONTRIBUTORS.txt have not been added to this file--to respect the
 privacy of these contributors.

Files: debian/*
Copyright: 2019  Nicholas D Steeves <nsteeves@gmail.com>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: planner
Source: http://ftp.gnome.org/pub/GNOME/sources/planner

Files: eds-backend/* src/planner-task-cmd.h
       src/planner-eds-plugin.c src/planner-resource-cmd.*
Copyright: 2004-2005, Alvaro del Castillo <acs@barrapunto.com>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General Public
 License as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

Files: libplanner/mrp-application.c libplanner/mrp-property.c
       libplanner/mrp-relation.c libplanner/mrp-sql.c
       src/planner-group-dialog.* src/planner-group-model.*
       src/planner-list-model.* src/planner-resource-input-dialog.h
       src/planner-task-input-dialog.h
Copyright: 2002-2004, CodeFactory AB
	   2002,      Richard Hult <richard@imendio.com>
	   2002,      Mikael Hallendal <micke@imendio.com>
	   2002-2004, Alvaro del Castillo <acs@barrapunto.com>
License: GPL-2+

Files: libplanner/mrp-application.h libplanner/mrp-assignment.h libplanner/mrp-calendar.h
       libplanner/mrp-day.* libplanner/mrp-error.*
       libplanner/mrp-group.* libplanner/mrp-object.h
       libplanner/mrp-old-xml.h libplanner/mrp-parser.h
       libplanner/mrp-property.h libplanner/mrp-relation.h
       libplanner/mrp-sql.h libplanner/mrp-storage-module-factory.c
       libplanner/mrp-storage-module-factory.h
       libplanner/mrp-storage-mrproject.*
       libplanner/mrp-storage-sql.* libplanner/mrp-types.*
       libplanner/planner.h src/planner-cell-renderer-list.*
       src/planner-cell-renderer-list.h src/planner-cell-renderer-popup.c
       src/planner-day-type-dialog.h src/planner-default-week-dialog.h
       src/planner-gantt-background.* src/planner-gantt-chart.h
       src/planner-gantt-header.* src/planner-gantt-model.h
       src/planner-gantt-print.h src/planner-gantt-row.h
       src/planner-calendar-dialog.h src/planner-assignment-model.*
       src/planner-calendar-selector.* src/planner-canvas-line.*
       src/planner-phase-dialog.h src/planner-plugin.*
       src/planner-plugin-loader.* src/planner-popup-entry.*
       src/planner-predecessor-model.h src/planner-print-job.*
       src/planner-project-properties.h src/planner-relation-arrow.*
       src/planner-resource-input-dialog.c src/planner-sidebar.*
       src/planner-table-print-sheet.* src/planner-task-input-dialog.c
       src/planner-task-tree.h src/planner-working-time-dialog.h
Copyright: 2001-2003, CodeFactory AB
	   2001-2003, Richard Hult <richard@imendio.com>
	   2001-2003, Mikael Hallendal <micke@imendio.com>
License: GPL-2+

Files: libplanner/mrp-calendar.c libplanner/mrp-file-module.c
       libplanner/mrp-file-module.h libplanner/mrp-old-xml.c
       libplanner/mrp-private.h libplanner/mrp-project.h
       libplanner/mrp-resource.h libplanner/mrp-storage-module.c
       libplanner/mrp-storage-module.h libplanner/mrp-task.*
       libplanner/mrp-task-manager.h libplanner/mrp-time.c
       libplanner/mrp-xml.c libplanner/mrp-xsl.c
       src/planner-cell-renderer-date.* src/planner-day-type-dialog.c
       src/planner-default-week-dialog.c src/planner-format.*
       src/planner-gantt-model.c src/planner-gantt-print.c
       src/planner-gantt-row.c src/planner-gantt-view.c
       src/planner-main.c src/planner-application.*
       src/planner-print-dialog.c src/planner-scale-utils.*
       src/planner-task-view.c src/planner-window.*
Copyright: 2004-2005, Imendio AB
	   2001-2003, CodeFactory AB
	   2001-2003, Richard Hult <richard@imendio.com>
	   2001-2003, Mikael Hallendal <micke@imendio.com>
License: GPL-2+


Files: libplanner/mrp-object.c libplanner/mrp-parser.c
       libplanner/mrp-project.c libplanner/mrp-resource.c
       libplanner/mpr-task-manager.c libplanner/mrp-time.h
       src/planner-gantt-chart.c src/planner-calendar-dialog.c
       src/planner-resource-dialog.c src/planner-resource-view.c
       src/planner-sql-plugin.c src/planner-task-dialog.c
       src/planner-task-tree.c
Copyright: 2001-2005, Imendio AB
	   2001-2003, CodeFactory AB
	   2001-2003, Richard Hult <richard@imendio.com>
	   2001-2002, Mikael Hallendal <micke@imendio.com>
	   2002-2004, Alvaro del Castillo <acs@barrapunto.com>
License: GPL-2+

Files: libplanner/mrp-paths-gnome.c
Copyright: 2005-2008, Imendio AB
	      	      Jani Tiainen
		      Maurice van der Pot
License: GPL-2+

Files: libplanner/mrp-paths.h
Copyright: 2005, Imendio AB
	   	 Jani Tiainen
License: GPL-2+

Files: src/dummy-canvas-item.*
Copyright: 1997-2000, Free Software Foundation
	   2009,      Maurice van der Pot <griffon26@kfk4ever.com>
License:   GPL-2+

Files: src/eel-canvas-rect.*
Copyright: 2002, Alexander Larsson
License:   GPL-2+


Files: src/planner-calendar.c
Copyright: 1995-1997, Peter Mattis, Spencer Kimball and Josh MacDonald
	   1998,      Cesar Miquel, Shawn T. Amundson and Mattias Gr<F6>nlund
	   1995-1998, Steffen Beyer
License: GPL-2+

Files: src/planner-calendar.h
Copyright: 1995-1997, Peter Mattis, Spencer Kimball and Josh MacDonald
	   1998,      Cesar Miquel, Shawn T. Amundson and Mattias Gr<F6>nlund
License: GPL-2+

Files: src/planner-cmd-manager.* src/planner-column-dialog.*
       src/planner-conf-gconf.c src/planner-conf.h
       src/planner-gant-view.h src/planner-html-plugin.c
       src/planner-msp-plugin.c src/planner-popup-button.*
       src/planner-resource-view.h src/planner-task-date-widget.*
       src/planner-task-view.h src/planner-usage-view.h
       src/planner-view.*
Copyright: 2003-2005, Imendio AB
License: GPL-2+

Files: src/planner-phase-dialog.c
Copyright: 2002-2003, Codefactory AB
	   2002,      Mikael Hallendal <micke@imendio.com>
	   2002-2003  Richard Hult <richard@imendio.com>
	   2004       Lincoln Phipps <lincoln.phipps@openmutual.net>
License: GPL-2+

Files: src/planner-predecessor-model.c src/planner-property-model.c
       src/planner-resource-dialog.h src/planner-working-time-dialog.c
Copyright: 2001-2003, Codefactory AB
	   2001-2002, Mikael Hallendal <micke@imendio.com>
	   2001-2003, Richard Hult <richard@imendio.com>
	   2002-2004, Alvaro del Castillo <acs@barrapunto.com>
License: GPL-2+

Files: src/planner-print-dialog.h src/planner-property-dialog.h
       src/planner-property-model.h src/planner-task-dialog.h
Copyright: 2002, Codefactory AB
	   2002, Mikael Hallendal <micke@imendio.com>
License: GPL-2+

Files: src/planner-project-properties.c
Copyright: 2003-2005, Imendio AB
	   2001-2002, CodeFactory AB
	   2001-2002, Richard Hult <richard@imendio.com>
	   2001-2002, Mikael Hallendal <micke@imendio.com>
	   2001-2002, Alvaro del Castillo <acs@barrapunto.com>
	   2004,      Lincoln Phipps <lincoln.phipps@openmutual.net>
License: GPL-2+

Files: src/planner-property-dialog.c
Copyright: 2002, CodeFactory AB
	   2002, Richard Hult <richard@imendio.com>
	   2002, Mikael Hallendal <micke@imendio.com>
	   2002, Alvaro del Castillo <acs@barrapunto.com>
	   2004, Lincoln Phipps <lincoln.phipps@openmutual.net>
	   2004, Imendio AB
License: GPL-2+

Files: src/planner-usage-chart.* src/planner-usage-model.*
       src/planner-usage-tree.* src/planner-usage-view.c
Copyright: 2003-2004, Imendio AB
	   2003,      Benjamin BAYART <benjamin@sitadelle.com>
	   2003,      Xavier Ordoquy <xordoquy@wanadoo.fr>
License: GPL-2+

Files: src/planner-usage-row.c
Copyright: 2003-2004, Imendio AB
	   2003,      Benjamin BAYART <benjamin@sitadelle.com>
	   2003,      Xavier Ordoquy <xordoquy@wanadoo.fr>
	   2006,      Alvaro del Castillo <acs@barrapunto.com>
	   2008,      Lee Baylis <lee@leebaylis.co.uk>
License: GPL-2+

Files: src/planner-usage-row.h
Copyright: 2003, Benjamin BAYART <benjamin@sitadelle.com>
	   2003, Xavier Ordoquy <xordoquy@wanadoo.fr>
License: GPL-2+

Files: src/planner-python-plugin.c
Copyright: 2004, Xavier Ordoquy <xordoquy@wanadoo.fr>
License: GPL-2+

Files: src/planner-task-cmd.c 
Copyright: 2004, Imendio AB
	   2004, Alvaro del Castillo <acs@barrapunto.com>
License: GPL-2+

Files: src/planner-task-popup.c
Copyright: 2005, Imendio AB
	   2004, Chris Ladd <caladd@particlestorm.net>
License: GPL-2+

Files: src/planner-task-popup.h
Copyright: 2004, Chris Ladd <caladd@particlestorm.net>
License: GPL-2+

FIles: src/planner-xml-planner-plugin.c
Copyright: 2003-2004, Imendio AB
	   2004,      Lincoln Phipps <lincoln.phipps@openmutual.net>
License: GPL-2+

Files: src/planner-util.* src/planner-util-win32.c
Copyright: 2005, Francisco Moraes
License: GPL-2+

Files: tests/cmd-manager-test.c
Copyright: 2008, Maurice van der Pot <griffon26@kfk4ever.com>
License: GPL-2+

Files: tests/self-check.c
Copyright: 1999, Eazel, Inc.
License: GPL-2+

Files: ltmain.sh
Copyright: 1996-2008, Free Software Foundation
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: src/valgrind.h
Copyright: 2000-2009, Julian Seward
License:
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. The origin of this software must not be misrepresented; you must
    not claim that you wrote the original software.  If you use this
    software in a product, an acknowledgment in the product
    documentation would be appreciated but is not required.
 .
 3. Altered source versions must be plainly marked as such, and must
    not be misrepresented as being the original software.
 .
 4. The name of the author may not be used to endorse or promote
    products derived from this software without specific prior written
    permission.
 .
   THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
   OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
   DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
   GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: eds-backend/Makefile.in eds-backend/planner-source/Makefile.in
        libplanner/Makefile.in python/Makefile.in tests/Makefile.in
Copyright: 1994-2009, Free Software Foundation
License: 
  This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY, to the extent permitted by law; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE.

Files: data/stylesheets/html1_css.xsl data/stylesheets/html1_gantt.xsl
       data/stylesheets/html1_resources.xsl data/stylesheets/planner2html.xsl
       data/stylesheets/html1_tasks.xsl
Copyright: 2004-2005 Imendio AB
           2003 CodeFactory AB
           2003 Daniel Lundin (daniel@edgewall.com)
           2004 Chris Ladd (caladd@particlestorm.net)
License: GPL-2+

Files: data/stylesheets/msp2planner.xsl
Copyright: 2004 Kurt Maute (kurt@maute.us)
License: GPL-2+

Files: debian/*
Copyright: 2009-2010 Xavier Oswald <xoswald@debian.org>
           2011-2013 Dmitry Smirnov <onlyjob@debian.org>
           2011, Josue Abarca <jmaslibre@debian.org.gt>
           2011, Wences Arana <aranax@debian.org.gt>
License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 1 can be found in the file
 `/usr/share/common-licenses/GPL-1'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: p4est
Source: https://github.com/cburstedde/p4est
Comment: Removed from tarball: doc/*.pdf sc/doc/*.pdf

Files: *
Copyright:
 Carsten Burstedde
 Lucas C. Wilcox
 Tobin Isaac
 The University of Texas System
License: GPL-2.0+

Files: sc/*
Copyright:
 Carsten Burstedde
 Michael H. Buselli
 Wessel Dankers
 The University of Texas System
License: LGPL-2.1+

Files:
 sc/src/sc_builtin/*
 sc/src/sc_getopt*
 sc/src/sc_obstack.c
Copyright: 1988 -- 2005 Free Software Foundation
License: LGPL-2.1+

Files: debian/*
Copyright:
 Matthias Maier
 Ansgar Burchardt
License: LGPL-2.1+

Files: INSTALL
Copyright: 1988 -- 2008 Free Software Foundation
License: FSF-Unlimited
 This file is free documentation; the Free Software Foundation gives
 unlimited permission to copy, distribute and modify it.

Files:
 build-aux/git-version-gen
 sc/build-aux/git-version-gen
Copyright: 2007--2008 Free Software Foundation
License: GPL-3.0+

Files:
 build-aux/git2cl
 sc/build-aux/git2cl
Copyright: 2007 Simon Josefsson
License: GPL-2.0+

Files: sc/config/ax_prefix_config_h.m4
Copyright:
 2014 Reuben Thomas <rrt@sc3d.org>
 2008 Guido U. Draheim <guidod@gmx.de>
 2008 Marten Svantesson
 2008 Gerald Point <Gerald.Point@labri.fr>
License: GPL-3.0+

Files: sc/config/ax_split_version.m4
Copyright: 2008 Tom Howard <tomhoward@users.sf.net>
License: GNU-All-Permissive-License
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

Files:
 sc/config/sc_blas.m4
 sc/config/sc_lapack.m4
Copyright: 2004 Sandia Corporation
License: LGPL-2.1+

Files:
 sc/config/sc_c_check_flag.m4
 sc/config/sc_c_version.m4
Copyright: 2008 Francesco Salvestrini <salvestrini@users.sourceforge.net>
License: GPL-2.0+-with-autoconf-exception
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Macro Archive. When you make and
 distribute a modified version of the Autoconf Macro, you may extend this
 special exception to the GPL to apply to your modified version as well.

Files: sc/config/sc_lint.m4
Copyright: 2005-2006 Sun Microsystems, Inc.  All rights reserved.
License: X11
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, and/or sell copies of the Software, and to permit persons
 to whom the Software is furnished to do so, provided that the above
 copyright notice(s) and this permission notice appear in all copies of
 the Software and that both the above copyright notice(s) and this
 permission notice appear in supporting documentation.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL
 INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING
 FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder
 shall not be used in advertising or otherwise to promote the sale, use
 or other dealings in this Software without prior written authorization
 of the copyright holder.

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1+
  This package is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
  .
  On Debian systems, the complete text of the GNU Lesser General
  Public License can be found in "/usr/share/common-licenses/LGPL-2.1".

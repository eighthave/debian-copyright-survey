Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: probabel
Source: https://github.com/GenABEL-Project/ProbABEL/releases
Files-Excluded: */eigen-3.2.1
                */3.2.1.tar.bz2

Files: *
Copyright: © 2009-2013 The GenABEL team
License: GPL-2.0+

Files: debian/*
Copyright: © 2013 L.C. Karssen <lennart@karssen.org>
License: GPL-2.0+

Files: src/include/R.h
Copyright: © 2000-2007 The R Development Core Team
License: LGPL-2.1+

Files: src/include/Rmath.h
Copyright: © 1998-2007 The R Development Core Team
             2004 The R Foundation
License: LGPL-2.1+

Files: src/include/R_ext/Arith.h
Copyright: © 1995, 1996 Robert Gentleman and Ross Ihaka
             1998--2007 The R Development Core Team.
License: LGPL-2.1+

Files: src/include/R_ext/Boolean.h
Copyright: © 2000, 2001 The R Development Core Team.
License: LGPL-2.1+

Files: src/include/R_ext/Complex.h
Copyright: © 1998-2001 Robert Gentleman, Ross Ihaka
                       and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/Constants.h
Copyright: © 1995, 1996 Robert Gentleman and Ross Ihaka
             1998-2007  The R Development Core Team.
License: LGPL-2.1+

Files: src/include/R_ext/Error.h
Copyright: © 1998-2005 Robert Gentleman, Ross Ihaka
                       and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/libextern.h
Copyright: © 2001, 2004 The R Development Core Team.
License: LGPL-2.1+

Files: src/include/R_ext/Memory.h
Copyright: © 1998-2007 Robert Gentleman, Ross Ihaka
                      and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/Print.h
Copyright: © 1998-2008 Robert Gentleman, Ross Ihaka
                      and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/Random.h
Copyright: © 1998-2002 Robert Gentleman, Ross Ihaka
                      and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/Utils.h
Copyright: © 1998-2005 Robert Gentleman, Ross Ihaka
                      and the R Development Core Team
License: LGPL-2.1+

Files: src/include/R_ext/RS.h
Copyright: © 1999-2007 The R Development Core Team
License: LGPL-2.1+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation; either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

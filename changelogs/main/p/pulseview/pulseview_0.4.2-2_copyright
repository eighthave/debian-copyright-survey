Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pulseview
Upstream-Contact: <sigrok-devel@lists.sourceforge.net>
Source: http://sigrok.org/wiki/PulseView
Comment:
 While most of the PulseView source files are GPLv2+ licensed, it has to be
 linked against at least libsigrok (which is GPLv3+) & usually also against
 libsigrokdecode (also GPLv3+), thus making PulseView as a whole GPLv3+.

Files: *
Copyright: 2012-2015 Joel Holdsworth <joel@airwebreathe.org.uk>
License: GPL-2.0+

Files: CMakeLists.txt test/CMakeLists.txt
Copyright: 2012 Joel Holdsworth <joel@airwebreathe.org.uk>
           2012-2013 Alexandru Gagniuc <mr.nuke.me@gmail.com>
           2020 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: config.h.in
Copyright: 2012 Alexandru Gagniuc <mr.nuke.me@gmail.com>
License: GPL-2.0+

Files: signalhandler.cpp signalhandler.hpp
Copyright: 2013 Adam Reichold
License: GPL-2.0+

Files: android/*.xml android/bundled_libs.xml.in
       android/loghandler.cpp android/loghandler.hpp
       android/res/* android/src/*
Copyright: 2014 Marcus Comstedt <marcus@mc.pp.se>
License: GPL-3.0+

Files: android/assetreader.cpp android/assetreader.hpp
Copyright: 2015 Daniel Elstner <daniel.kitta@gmail.com>
License: GPL-3.0+

Files: contrib/pulseview_cross.nsi.in
Copyright: 2013-2020 Uwe Hermann <uwe@hermann-uwe.de>
License: GPL-2.0+

Files: doc/pulseview.1
Copyright: 2012-2013 Uwe Hermann <uwe@hermann-uwe.de>
License: GPL-2.0+

Files: pv/application.cpp pv/application.hpp
Copyright: 2014 Martin Ling <martin-sigrok@earth.li>
License: GPL-2.0+

Files: pv/strnatcmp.hpp
Copyright: 2000, 2004 Martin Pool <mbp sourcefrog net>
License: Zlib

Files: pv/data/decodesignal.*
Copyright: 2017 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/data/segment.cpp pv/data/segment.hpp
       pv/data/signalbase.cpp pv/data/signalbase.hpp
       pv/views/trace/standardbar.cpp pv/views/trace/standardbar.hpp
       pv/views/viewbase.cpp pv/views/viewbase.hpp
       test/data/segment.cpp
Copyright: 2012-2015 Joel Holdsworth <joel@airwebreathe.org.uk>
           2016-2017 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/dialogs/settings.hpp pv/dialogs/settings.cpp
       pv/globalsettings.cpp pv/globalsettings.hpp
Copyright: 2017 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/logging.*
Copyright: 2018 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/subwindows/*
Copyright: 2018 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/views/decoder_binary/QHexView.*
Copyright: 2015 Victor Anjin <virinext@gmail.com>
           2019 Soeren Apel <soeren@apelpie.net>
License: MIT

Files: pv/views/decoder_binary/view.*
Copyright: 2019 Soeren Apel <soeren@apelpie.net>
License: GPL-2.0+

Files: pv/widgets/timestampspinbox.cpp pv/widgets/timestampspinbox.hpp
       test/test.hpp test/util.cpp test/view/ruler.cpp
Copyright: 2015 Jens Steinhauser <jens.steinhauser@gmail.com>
License: GPL-2.0+

Files: pv/widgets/flowlayout.*
Copyright: 2015 The Qt Company Ltd.
License: BSD-3-clause

Files: pv/widgets/wellarray.cpp pv/widgets/wellarray.hpp
Copyright: 2013 Digia Plc and/or its subsidiary(-ies)
License: LGPL-2.1 with Digia Qt LGPL Exception 1.1 or GPL-3

Files: debian/*
Copyright: 2014 Uwe Hermann <uwe@debian.org>
           2017 Jonathan McDowell <noodles@earth.li>
License: GPL-2.0+

License: BSD-3-clause
 "Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of The Qt Company Ltd nor the names of its
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1 with Digia Qt LGPL Exception 1.1 or GPL-3
 This file is part of the QtGui module of the Qt Toolkit.
 .
 $QT_BEGIN_LICENSE:LGPL$
 Commercial License Usage
 Licensees holding valid commercial Qt licenses may use this file in
 accordance with the commercial license agreement provided with the
 Software or, alternatively, in accordance with the terms contained in
 a written agreement between you and Digia.  For licensing terms and
 conditions see http://qt.digia.com/licensing.  For further information
 use the contact form at http://qt.digia.com/contact-us.
 .
 GNU Lesser General Public License Usage
 Alternatively, this file may be used under the terms of the GNU Lesser
 General Public License version 2.1 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPL included in the
 packaging of this file.  Please review the following information to
 ensure the GNU Lesser General Public License version 2.1 requirements
 will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Digia gives you certain additional
 rights.  These rights are described in the Digia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 GNU General Public License Usage
 Alternatively, this file may be used under the terms of the GNU
 General Public License version 3.0 as published by the Free Software
 Foundation and appearing in the file LICENSE.GPL included in the
 packaging of this file.  Please review the following information to
 ensure the GNU General Public License version 3.0 requirements will be
 met: http://www.gnu.org/copyleft/gpl.html.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 can be found in `/usr/share/common-licenses/LGPL-2.1`.
 .
 On Debian systems, the complete text of the GNU Public Licenses can be found
 in `/usr/share/common-licenses/GPL-3`.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.


License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.


Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PHing
Upstream-Contact: Michiel Rook <michiel.rook@gmail.com>
Source: https://github.com/phingofficial/phing

Files: *
Copyright: Michiel Rook <mrook@php.net>
           Hans Lellelid <hans@xmpl.org>
           Sebastian Bergmann <sb@sebastian-bergmann.de>
           Joakim Bodin <joakim.bodin+phing@gmail.com>
           Johan Van den Brande <johan@vandenbrande.com>
           Bryan Davis <bender@casadebender.com>
           Andrew Eddie <andrew.eddie@jamboworks.com>
           Markus Fischer <markus@fischer.name>
           David Giffin <david@giffin.org>
           Ryan Grove <ryan@wonko.com>
           Frank Kleine <mikey@stubbles.net>
           George Miroshnikov <laggy.luke@gmail.com>
           David Persson <davidpersson at qeweurope dot org>
           Stefan Priebsch <stefan.priebsch@e-novative.de>
           Jorrit Schippers <jorrit at ncode dot nl>
           Alexey Shockov <alexey@shockov.com>
           Dirk Thomas <dirk.thomas@4wdmedia.de>
           Knut Urdalen <knut.urdalen@gmail.com>
           Mike Wittje <mw@mike.wittje.de>
           Benjamin Schultz <bschultz@proqrent.de>
           Andrei Serdeliuc <andrei@serdeliuc.ro>
           Victor Farazdagi <simple.square@gmail.com>
           Christian Weiske <cweiske@cweiske.de>
           Matthias Pigulla <mp@webfactory.de>
           Lineke Kerckhoffs-Willems <lineke@phpassionate.com>
           Utsav Handa <handautsav@hotmail.com>
           Andreas Aderhold <andi@binarycloud.com>
           Alex Black <enigma@turingstudio.com>
           Albert Lash <alash@plateauinnovation.com>
           Charlie Killian <charlie@tizac.com>
           Manuel Holtgrewe <grin@gmx.net>
           Andrzej Nowodworski <a.nowodworski@learn.pl>
           Jason Hines <jason@greenhell.com>
           Jesse Estevez <jesseestevez@earthlink.net>
           Andris Spruds <Andris.Spruds@stud.lba.lv>
           Ronald TAO <ronaldtao@hotmail.com>
           Yannick Lecaillez <yl@seasonfive.com>
           Martin Hujer <mhujer@gmail.com>
           2001, 2002, THYRELL
           2003, seasonfive
License: LGPL-3+

Files: */contrib/DocBlox/Parallel/*
Copyright: 2010-2011, Mike van Riel / Naenius (http://www.naenius.com)
License: Expat

Files: */etc/*.xsl
       */tasks/ext/liquibase/AbstractLiquibaseTask.php
       */tasks/ext/liquibase/LiquibaseChangeLogTask.php
       */tasks/ext/liquibase/LiquibaseDbDocTask.php
       */tasks/ext/liquibase/LiquibaseDiffTask.php
       */tasks/ext/liquibase/LiquibaseRollbackTask.php
       */tasks/ext/liquibase/LiquibaseTagTask.php
       */tasks/ext/liquibase/LiquibaseUpdateTask.php
       */tasks/ext/PatchTask.php
Copyright: 2000-2013, The Apache Software Foundation
           2007-2011, bitExpert AG
License: Apache-2

Files: */Phing.php
Copyright: Michiel Rook <michiel.rook@gmail.com>
           2005-2011, Zend Technologies USA Inc.
License: LGPL-3+ and BSD-3-clause

Files: */tasks/ext/ComposerTask.php
Copyright: nuno costa <nuno@francodacosta.com>
License: LGPL-3+ and Expat

Files: */tasks/ext/GrowlNotifyTask.php
Copyright: 2012-2013, Laurent Laville <pear@laurent-laville.org>
License: BSD-3-clause

Files: */tasks/ext/SymfonyConsole/*
Copyright: nuno costa <nuno@francodacosta.com>
License: LGPL-3+ and GPL-2+

Files: */tasks/system/WaitForTask.php
Copyright: Michiel Rook <michiel.rook@gmail.com>
License: LGPL-3+ and Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License version 2
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
  * Neither the name of the authors nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License
 version 3 or later as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 3 can be found in the file `/usr/share/common-licenses/LGPL-3'.

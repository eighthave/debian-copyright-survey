Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pytest-qt
Upstream-Contact: Bruno Oliveira <nicoddemus@gmail.com>
Source: https://pypi.python.org/pypi/pytest-qt

Files: *
Copyright: 2013-2016 Bruno Oliveira and others
License: Expat

Files: debian/*
Copyright: 2017-2018 Ghislain Antony Vaillant <ghisvail@gmail.com>
License: Expat

Files: pytestqt/modeltest.py
Copyright: 2016 The Qt Company Ltd.
           2017 Klarälvdalens Datakonsult AB, a KDAB Group company <info@kdab.com>
                author Giuseppe D'Angelo <giuseppe.dangelo@kdab.com>
License: LGPL-2.1 or LGPL-3

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 3 of the License.
 .
 On Debian systems, the complete text of version 3 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: LGPL-3+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 3 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.

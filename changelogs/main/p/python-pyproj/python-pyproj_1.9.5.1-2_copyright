Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyproj
Upstream-Contact: Jeffrey Whitaker <jeffrey.s.whitaker@noaa.gov>
Source: https://github.com/jswhit/pyproj

Files: *
Copyright: © 2006-2009, 2013, Jeffrey Whitaker <jeffrey.s.whitaker@noaa.gov>
License: MIT-short

Files: src/*
Copyright:        © 2012-2015, Charles Karney <charles@karney.com>
 © 2000-2002, 2009-2010, 2012, Frank Warmerdam <warmerdam@pobox.com>
                  © 2008-2012, Even Rouault <even dot rouault at mines-paris dot org>
                  © 2011-2012, Martin Lambers <marlam@marlam.de>
                       © 2012, Martin Raspaud
      © 1995, 2003-2004, 2006, Gerald I. Evenden
                       © 2006, Andrey Kiselev
                       © 2001, Thomas Flemming <tf@ttqv.com>
License: MIT

Files: src/proj_etmerc.c
Comment: The code in this file is largly based upon procedures:
 .
 Written by: Knud Poder and Karsten Engsager
 .
 Based on math from: R.Koenig and K.H. Weise, "Mathematische
 Grundlagen der hoeheren Geodaesie und Kartographie,
 Springer-Verlag, Berlin/Goettingen" Heidelberg, 1951.
 .
 Modified and used here by permission of Reference Networks
 Division, Kort og Matrikelstyrelsen (KMS), Copenhagen, Denmark
Copyright: © 2008, Gerald I. Evenden
License: MIT

Files: src/PJ_isea.c
Copyright: disclaimed
License: public-domain
 This code was entirely written by Nathan Wagner
 and is in the public domain.

Files: src/PJ_sch.c
Copyright: 2015, California Institute of Technology.
 Government sponsorship acknowledged.
Comment: From the file header:
 .
 Project:  SCH Coordinate system
 Purpose:  Implementation of SCH Coordinate system
 References : 
      1. Hensley. Scott. SCH Coordinates and various transformations. June 15, 2000.  
      2. Buckley, Sean Monroe. Radar interferometry measurement of land subsidence. 2000..
         PhD Thesis. UT Austin. (Appendix)
      3. Hensley, Scott, Elaine Chapin, and T. Michel. "Improved processing of AIRSAR
         data based on the GeoSAR processor." Airsar earth science and applications
         workshop, March. 2002. (http://airsar.jpl.nasa.gov/documents/workshop2002/papers/T3.pdf)
 .
 Author:   Piyush Agram (piyush.agram@jpl.nasa.gov)
License: public-domain
 NOTE:  The SCH coordinate system is a sensor aligned coordinate system 
 developed at JPL for radar mapping missions. Details pertaining to the 
 coordinate system have been release in the public domain (see references above).
 This code is an independent implementation of the SCH coordinate system
 that conforms to the PROJ.4 conventions and uses the details presented in these
 publicly released documents. All credit for the development of the coordinate
 system and its use should be directed towards the original developers at JPL.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: © 2009, David Paleino <dapal@debian.org>
License: GPL-3+

License: MIT-short
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies and that both the copyright notice and this permission
 notice appear in supporting documentation. THE AUTHOR DISCLAIMS
 ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT
 SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.


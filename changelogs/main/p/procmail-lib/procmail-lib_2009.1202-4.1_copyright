Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0
X-Format: http://dep.debian.net/deps/dep5
Upstream-Name: procmail-lib
Upstream-Contact: Jari Aalto <jari.aalto@cante.net>
Source: http://savannah.nongnu.org/projects/procmail-lib
Comment:
 See also http://freshmeat.net/projects/procmail-lib

Files: *
Copyright:
  1997-2008 Jari Aalto <jari.aalto@cante.net>
License: GPL-2+

Files: *.html *.txt
Copyright:
  1997-2008 Jari Aalto <jari.aalto@cante.net>
License: GPL-2+-or-GFDL-1.2+

Files: lib-stebbens/*
Copyright:
  1995-1997 Alan Stebbens <aks@sgi.com>
License: GPL-2+

Files: debian/*
Copyright:
  2008-2010, 2012-2013 Jari Aalto <jari.aalto@cante.net>
  2007	    Michael Ablassmeier <abi@debian.org>
  2001-2003 Elie Rosenblum <fnord@debian.org>, <fnord@cosanostra.net>
  2000	    Dr. Guenter Bechly <gbechly@debian.org>
  1999	    Raphael Hertzog <rhertzog@hrnet.fr>
  1997-1998 Karl M. Hegbloom <karlheg@inetarena.com>, <karlheg@debian.org>
  1996      Karl Sackett <krs@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+-or-GFDL-1.2+
 Exception: The documentation of project "Procmail Module Library"
 (*.txt and *.html files) is DUAL LICENCED and may be
 distributed under the terms of GNU General Public License (GNU
 GPL) --see above--; *or*, at your option, distributed under
 the terms of GNU Free Documentation License (GNU FDL).
 .
 The end user can continue to distribute the documentation in
 this dual licence form *or* select the other license (GNU GPL,
 GNU FDL) and remove the unwanted one.
 .
 Copyright (C) 1997-2008 Jari Aalto
 .
 Permission is granted to copy, distribute and/or modify
 this document under the terms of the GNU Free
 Documentation License, Version 1.2 or any later version
 published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no
 Back-Cover Texts. A copy of the license is included in the
 section entitled "GNU Free Documentation License";
 see the file COPYING.GNU-GFDL.
 .
 Visit <http://www.gnu.org/licenses/fdl.html> for more information.

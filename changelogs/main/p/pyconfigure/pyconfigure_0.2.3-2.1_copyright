Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyconfigure
Upstream-Contact: Brandon Invergo <brandon@invergo.net>
Source: http://git.savannah.gnu.org/cgit/pyconfigure.git

Files: *
Copyright:
	2012-2014 Brandon Invergo <brandon@invergo.net>
	1994-2013 Free Software Foundation, Inc.
License: GPL-3.0+

Files: src/install-sh
Copyright: 1994 X Consortium
License: MIT/X

Files: src/m4/python.m4
Copyright: 2012-2014 Brandon Invergo <brandon@invergo.net>
	   1999-2012 Free Software Foundation, Inc.
License: GPL-3.0+
Comment:
 With special exception:
 Under Section 7 of GPL version 3, you are granted additional
 permissions described in the Autoconf Configure Script Exception,
 version 3.0, as published by the Free Software Foundation.
 .
                    AUTOCONF CONFIGURE SCRIPT EXCEPTION
                       Version 3.0, 18 August 2009
 .
 Copyright (C) 2009 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.
 .
 This Exception is an additional permission under section 7 of the GNU
 General Public License, version 3 ("GPLv3").  It applies to a given
 file that bears a notice placed by the copyright holder of the file
 stating that the file is governed by GPLv3 along with this Exception.
 .
 The purpose of this Exception is to allow distribution of Autoconf's
 typical output under terms of the recipient's choice (including
 proprietary).
 .
 0. Definitions
 .
 "Covered Code" is the source or object code of a version of Autoconf
 that is a covered work under this License.
 .
 "Normally Copied Code" for a version of Autoconf means all parts of
 its Covered Code which that version can copy from its code (i.e., not
 from its input file) into its minimally verbose, non-debugging and
 non-tracing output.
 .
 "Ineligible Code" is Covered Code that is not Normally Copied Code.
 .
 1. Grant of Additional Permission.
 .
 You have permission to propagate output of Autoconf, even if such
 propagation would otherwise violate the terms of GPLv3.  However, if
 by modifying Autoconf you cause any Ineligible Code of the version you
 received to become Normally Copied Code of your modified version, then
 you void this Exception for the resulting covered work.  If you convey
 that resulting covered work, you must remove this Exception in
 accordance with the second paragraph of Section 7 of GPLv3.
 .
 2. No Weakening of Autoconf Copyleft.
 .
 The availability of this Exception does not imply any general
 presumption that third-party software is unaffected by the copyleft
 requirements of the license of Autoconf.

Files: doc/*
Copyright: 2012-2013 Brandon Invergo <brandon@invergo.net>
License: GFDL-1.2+

Files: debian/*
Copyright: 2015 Afif Elghraoui <afif@ghraoui.name>
License: GPL-3.0+

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this
 document under the terms of the GNU Free Documentation License,
 Version 1.2 or any later version published by the Free Software
 Foundation; with no Invariant Sections, no Front-Cover Texts and no
 Back-Cover Texts.
 .
 On Debian systems, the complete text of the GNU Free Document
 License version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2".

License: MIT/X
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the X Consortium shall not
 be used in advertising or otherwise to promote the sale, use or other deal-
 ings in this Software without prior written authorization from the X Consor-
 tium.

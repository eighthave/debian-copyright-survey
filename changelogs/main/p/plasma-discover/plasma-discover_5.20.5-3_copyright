
Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: discover
Upstream-Contact: Aleix Pol Gonzalez <aleixpol@kde.org>
Source: https://invent.kde.org/plasma/discover

Files: *
Copyright: 2012-2018, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2010-2013, Jonathan Thomas <echidnaman@kubuntu.org>
           2013, Lukas Appelhans <boom1992@chakra-project.org>
License: GPL-2+3+KDEeV

Files: debian/*
Copyright: 2016-2018, Matthias Klumpp <mak@debian.org>
  2015, Jonathan Riddell <jr@jriddell.org>
  2010, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.

Files: discover/UnityLauncher.cpp
  discover/UnityLauncher.h
Copyright: 2016, Kai Uwe Broulik <kde@privat.broulik.de>
License: LGPL-2.1 or LGPL-3

Files: libdiscover/CachedNetworkAccessManager.cpp
  libdiscover/CachedNetworkAccessManager.h
Copyright: 2017, Jan Grulich <jgrulich@redhat.com>
  2013, 2014, 2017, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
License: GPL-2+3+KDEeV

Files: libdiscover/Category/Category.cpp
  libdiscover/Category/Category.h
Copyright: 2010-2012, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/ReviewsBackend/*
Copyright: 2010-2012, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/Transaction/*
Copyright: 2010-2012, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/Transaction/TransactionListener.cpp
  libdiscover/Transaction/TransactionListener.h
Copyright: 2012, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
  2010, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/UpdateModel/*
Copyright: 2010-2012, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/appstream/*
Copyright: 2017, Aleix Pol Gonzalez <aleixpol@kde.org>
License: GPL-2+3+KDEeV

Files: libdiscover/appstream/OdrsReviewsBackend.cpp
  libdiscover/appstream/OdrsReviewsBackend.h
Copyright: 2017, Jan Grulich <jgrulich@redhat.com>
  2013, 2014, 2017, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/DummyBackend/DummyNotifier.cpp
  libdiscover/backends/DummyBackend/DummyNotifier.h
Copyright: 2013, Lukas Appelhans <l.appelhans@gmx.de>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/FlatpakBackend/*
Copyright: 2017, Jan Grulich <jgrulich@redhat.com>
  2013, 2014, 2017, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/FlatpakBackend/FlatpakFetchDataJob.cpp
  libdiscover/backends/FlatpakBackend/FlatpakFetchDataJob.h
Copyright: 2017, Jan Grulich <jgrulich@redhat.com>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/FlatpakBackend/FlatpakNotifier.h
Copyright: 2017, Jan Grulich <jgrulich@redhat.com>
  2013, Lukas Appelhans <l.appelhans@gmx.de>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/PackageKitBackend/PackageKitBackend.cpp
  libdiscover/backends/PackageKitBackend/PackageKitMessages.cpp
  libdiscover/backends/PackageKitBackend/PackageKitResource.cpp
Copyright: 2013, Lukas Appelhans <l.appelhans@gmx.de>
  2012-2014, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/PackageKitBackend/PackageKitNotifier.cpp
Copyright: 2015, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
  2013, Lukas Appelhans <l.appelhans@gmx.de>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/PackageKitBackend/PackageKitNotifier.h
  libdiscover/backends/PackageKitBackend/PackageKitUpdater.cpp
  libdiscover/backends/PackageKitBackend/PackageKitUpdater.h
Copyright: 2013, Lukas Appelhans <l.appelhans@gmx.de>
License: GPL-2+3+KDEeV

Files: libdiscover/backends/PackageKitBackend/pk-offline-private.h
Copyright: 2014, Richard Hughes <richard@hughsie.com>
License: GPL-2+

Files: libdiscover/resources/AbstractResourcesBackend.h
  libdiscover/resources/ResourcesModel.h
Copyright: 2013, Lukas Appelhans <boom1992@chakra-project.org>
  2012-2016, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
  2010-2013, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/resources/ResourcesProxyModel.cpp
  libdiscover/resources/ResourcesProxyModel.h
Copyright: 2012, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
  2010, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: libdiscover/resources/StandardBackendUpdater.cpp
  libdiscover/resources/StandardBackendUpdater.h
Copyright: 2012, 2014, 2015, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
License: GPL-2+3+KDEeV

Files: libdiscover/utils.h
Copyright: 2013, Lukas Appelhans <boom1992@chakra-project.org>
  2012-2016, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
  2010-2013, Jonathan Thomas <echidnaman@kubuntu.org>
License: GPL-2+3+KDEeV

Files: discover/org.kde.discover.appdata.xml
       libdiscover/backends/FlatpakBackend/org.kde.discover.flatpak.appdata.xml
       libdiscover/backends/PackageKitBackend/org.kde.discover.packagekit.appdata.xml
       libdiscover/backends/SnapBackend/org.kde.discover.snap.appdata.xml
Copyright: 2012-2018, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2010-2013, Jonathan Thomas <echidnaman@kubuntu.org>
           2013, Lukas Appelhans <boom1992@chakra-project.org>
License: CC0-1.0

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file `/usr/share/common-licenses/GPL-2', likewise, the full
 text of the GNU General Public License version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-3
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 3 of the License.
 .
 On Debian systems, the complete text of version 3 of the GNU Lesser
 General Public License can be found in `/usr/share/common-licenses/LGPL-3'.

License: CC0-1.0
 The license is stated in the appdata XML files as
   <metadata_license>CC0-1.0</metadata_license>
 .
 On Debian systems, the complete text of the CC0 1.0 Universal License
 can be found in `/usr/share/common-licenses/CC0-1.0'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: puredata
Upstream-Contact: msp@ucsd.edu
Source: http://msp.ucsd.edu/software.html
License: BSD-3-clause

Files: *
Copyright: 1996-2019 Miller Puckette
License: BSD-3-clause

Files: src/s_midi_pm.* src/s_*_oss.* src/s_*_alsa* src/s_*_dummy.*
Copyright: 1997-2016 Miller Puckette
 1997-2003 Guenter Geiger, Larry Troxler, Winfried Ritsch, Karl MacMillan and others
License: BSD-3-clause

Files: src/s_path.c
Copyright: 1999 Guenther Geiger and others
License: BSD-3-clause

Files: src/s_audio_paring.c
Copyright: 1999-2000 Ross Bencina & Phil Burk
 2000-2016 Miller Puckette
License: BSD-3-clause

Files: src/g_*
Copyright: 1996-2016 Miller Puckette
 2000-2001 Thomas Musil, IEM KUG Graz Austria
License: BSD-3-clause

Files: src/g_editor_extras.c
Copyright: 2018-2019, Miller Puckette, IOhannes m zmölnig and others.
License: BSD-3-clause

Files: doc/5.reference/bng-help.pd
 doc/5.reference/hslider-help.pd
 doc/5.reference/my_canvas-help.pd
 doc/5.reference/numbox2-help.pd
 doc/5.reference/toggle-help.pd
 doc/5.reference/vslider-help.pd
 doc/5.reference/vu-help.pd
Copyright: 1996-2016 Miller Puckette
 2000-2001 Thomas Musil, IEM KUG Graz Austria
License: BSD-3-clause

Files: src/x_vexp*
Copyright:
 1989-2016, Shahrokh Yadegari
 1994,1995,1998,1999 IRCAM - Centre George Pompidou, Paris, France
   (Maurizio De Cecco, Francois Dechelle, Enzo Maggi, Norbert Schnell)
 1996-2016 Miller Puckette
License: BSD-3-clause

Files: src/s_net.*
Copyright:
 2019-2020, Dan Wilcox
 2019-2020, Christof Ressi
License: BSD-3-clause

Files: src/d_fft_fftsg.c
Copyright: 1996-2001 Takuya OOURA <ooura@mmm.t.u-tokyo.ac.jp>
License: ooura

Files: tcl/pd_guiprefs.tcl
Copyright: 2017 IOhannes m zmölnig
 1997-2009 Miller Puckette
 2011 Yvan Volochine
 2008 WordTech Communications LLC
License: tcl

Files: tcl/pd_deken.tcl
Copyright: 2015-2016, Chris McCormick & IOhannes m zmölnig
License: BSD-3-clause

Files: tcl/pd_docsdir.tcl
Copyright: 2017, Dan Wilcox <danomatika.com>
License: BSD-3-clause

Files: tcl/pd_menus.tcl
Copyright: 1997-2009 Miller Puckette
 2008 WordTech Communications LLC
License: tcl

Files: portaudio/*
Copyright: 1999-2002 Ross Bencina & Phil Burk
License: Expat

Files: portmidi/*
Copyright: 1999-2002 Ross Bencina & Phil Burk
 2001-2009 Roger B. Dannenberg
License: Expat

Files: po/*.po*
Copyright: N.N.
License: public-domain

Files: debian/*
Copyright: 1999-2008 Guenter Geiger <geiger@debian.org>
 2009-2012 Paul Brossier <piem@debian.org>
 2010-2019 IOhannes m zmölnig <zmoelnig@iem.at>
License: BSD-3-clause

License: BSD-3-clause
 This software is copyrighted by Miller Puckette and others.  The following
 terms (the "Standard Improved BSD License") apply to all files associated with
 the software unless explicitly disclaimed in individual files:
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above
    copyright notice, this list of conditions and the following
    disclaimer in the documentation and/or other materials provided
    with the distribution.
 3. The name of the author may not be used to endorse or promote
    products derived from this software without specific prior
    written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 Any person wishing to distribute modifications to the Software is
 requested to send the modifications to the original developer so that
 they can be incorporated into the canonical version.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: ooura
 You may use, copy, modify this code for any purpose and without fee. You may
 distribute this ORIGINAL package.

License: tcl
 The authors hereby grant permission to use, copy, modify, distribute, and
 license this software and its documentation for any purpose, provided that
 existing copyright notices are retained in all copies and that this notice is
 included verbatim in any distributions. No written agreement, license, or
 royalty fee is required for any of the authorized uses. Modifications to this
 software may be copyrighted by their authors and need not follow the licensing
 terms described here, provided that the new terms are clearly indicated on the
 first page of each file where they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
 THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN
 IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE, AND NON-INFRINGEMENT. THIS SOFTWARE IS PROVIDED ON AN "AS
 IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO OBLIGATION TO PROVIDE
 MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 .
 GOVERNMENT USE: If you are acquiring this software on behalf of the U.S.
 government, the Government shall have only "Restricted Rights" in the software
 and related documentation as defined in the Federal Acquisition Regulations
 (FARs) in Clause 52.227.19 (c) (2). If you are acquiring the software on
 behalf of the Department of Defense, the software shall be classified as
 "Commercial Computer Software" and the Government shall have only "Restricted
 Rights" as defined in Clause 252.227-7013 (c) (1) of DFARs. Notwithstanding
 the foregoing, the authors grant the U.S. Government and others acting in its
 behalf permission to use and distribute the software in accordance with the
 terms specified in this license.

License: public-domain
 This file is put in the public domain.

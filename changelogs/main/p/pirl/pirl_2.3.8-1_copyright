Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PIRL packages
Upstream-Contact: Bradford Castalia <Castalia@Arizona.edu>
Source: http://pirlwww.lpl.arizona.edu/software/PIRL_Java_Packages.shtml

Files: *
Copyright: 2001-2012 © Arizona Board of Regents on behalf of the
 Planetary Image Research Laboratory, Lunar and Planetary Laboratory at
 the University of Arizona.
License: LGPL-3
 The PIRL Java Packages are free software; you can redistribute them
 and/or modify them under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.
 .
 The PIRL Java Packages are distributed in the hope that they will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

Files: PIRL/Database/Icons/*.gif
Copyright: 2000 © Sun Microsystems, Inc. All Rights Reserved.
License:
 Sun grants you ("Licensee") a non-exclusive, royalty free, license to use, and
 redistribute this software graphics artwork, as individual graphics or as a
 collection, as part of software code or programs that you develop, provided
 that i) this copyright notice and license accompany the software graphics
 artwork; and ii) you do not utilize the software graphics artwork in a manner
 which is disparaging to Sun. Unless enforcement is prohibited by applicable
 law, you may not modify the graphics, and must use them true to color and
 unmodified in every way.
 .
 This software graphics artwork is provided "AS IS," without a warranty of any
 kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL
 NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 MODIFYING OR DISTRIBUTING THE SOFTWARE GRAPHICS ARTWORK.
 .
 IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR
 DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE
 DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT
 OF THE USE OF OR INABILITY TO USE SOFTWARE GRAPHICS ARTWORK, EVEN IF SUN HAS
 BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 If any of the above provisions are held to be in violation of applicable law,
 void, or unenforceable in any jurisdiction, then such provisions are waived to
 the extent necessary for this Disclaimer to be otherwise enforceable in such
 jurisdiction.

Files: PIRL/TreeTable/*.java
Copyright: 1997, 2000 © Sun Microsystems, Inc. All Rights Reserved.
License:
 Redistribution and use in source and binary forms, with or
 without modification, are permitted provided that the following
 conditions are met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistribution in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials
   provided with the distribution.
 .
 Neither the name of Sun Microsystems, Inc. or the names of
 contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.
 .
 This software is provided "AS IS," without a warranty of any
 kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND
 WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
 EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY
 DAMAGES OR LIABILITIES SUFFERED BY LICENSEE AS A RESULT OF OR
 RELATING TO USE, MODIFICATION OR DISTRIBUTION OF THIS SOFTWARE OR
 ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE 
 FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT,   
 SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER  
 CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF 
 THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS 
 BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 You acknowledge that this software is not designed, licensed or
 intended for use in the design, construction, operation or
 maintenance of any nuclear facility.

Files: debian/*
Copyright: 2012, Mathieu Malaterre <malat@debian.org>
License: LGPL-3
 Same as above

License: LGPL-3
  On Debian GNU/Linux system you can find the complete text of the
  LGPL-3 license in '/usr/share/common-licenses/LGPL-3'

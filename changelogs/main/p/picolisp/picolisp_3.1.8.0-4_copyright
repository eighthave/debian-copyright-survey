Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: picolisp
Source: http://software-lab.de/down.html

Files: *
Copyright: 2002-2011 Alexander Burger <abu@software-lab.de>
License: Expat

Files: app/loc/* loc/*
Copyright: 2002-2011, Alexander Burger <abu@software-lab.de>
           2008, Mansur Mamkin <mmamkin@mail.ru>
           2009, Armadillo <tc.rucho@gmail.com>
           2010, Jon Kleiser, jon.kleiser@usit.uio.no
License: Expat

Files: lib/glyphlist.txt
Copyright: 1997,1998,2002,2007 Adobe Systems Incorporated
License: Adobe
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this documentation file to use, copy, publish, distribute,
 sublicense, and/or sell copies of the documentation, and to permit
 others to do the same, provided that:
 - No modification, editing or other alteration of this document is
 allowed; and
 - The above copyright notice and this permission notice shall be
 included in all copies of the documentation.
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this documentation file, to create their own derivative works
 from the content of this document to use, copy, publish, distribute,
 sublicense, and/or sell the derivative works, and to permit others to do
 the same, provided that the derived work is not represented as being a
 copy or version of this document.
 .
 Adobe shall not be liable to any party for any loss of revenue or profit
 or for indirect, incidental, special, consequential, or other similar
 damages, whether based on tort (including without limitation negligence
 or strict liability), contract or other legal or equitable grounds even
 if Adobe has been advised or had reason to know of the possibility of
 such damages.Ê The Adobe materials are provided on an "AS IS" basis.Ê
 Adobe specifically disclaims all express, statutory, or implied
 warranties relating to the Adobe materials, including but not limited to
 those concerning merchantability or fitness for a particular purpose or
 non-infringement of any third party rights regarding the Adobe
 materials.

Files: lib/el/*.el
Copyright: 2009, Guillermo R. Palavecino
License: GPL-2.0+

Files: lib/el/picolisp-wiki-mode.el
Copyright: 2012, Thorsten Jolitz <tjolitz@gmail.com>
License: GPL-2.0+

Files: debian/*
Copyright: 2011 Kan-Ru Chen <koster@debian.org>
License: Expat

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

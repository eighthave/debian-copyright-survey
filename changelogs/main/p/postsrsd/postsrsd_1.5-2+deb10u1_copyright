Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: postsrsd
Source: https://github.com/roehling/postsrsd

Files: *
Copyright:
 2012 Timo Röhling <timo.roehling@gmx.de>
License: GPL-2+

Files: sha1.c
Copyright: public-domain
License: public-domain
 NIST Secure Hash Algorithm
 Borrowed from SHA1.xs by Gisle Ass
 heavily modified by Uwe Hollerbach <uh@alumni.caltech edu>
 from Peter C. Gutmann's implementation as found in
 Applied Cryptography by Bruce Schneier
 Further modifications to include the "UNRAVEL" stuff, below
 HMAC functions by Shevek <srs@anarres.org> for inclusion in
 libsrs2, under GPL-2 or BSD license. Combine this lot in any way
 you think will stand up in court. I hope my intent is clear.
 .
 This code is in the public domain

Files: srs2.c srs2.h
Copyright:
 2004 Shevek <srs@anarres.org>
 2014, 2016 Timo Röhling <timo.roehling@gmx.de>
License: GPL-2 or BSD-4-clause

Files: debian/*
Copyright:
 2014 Oxan van Leeuwen <oxan@oxanvanleeuwen.nl>
 2012 Timo Röhling <timo.roehling@gmx.de>
License: GPL-2+

Files: debian/po/*
Copyright:
 2015 Michal Simunek <michal.simunek@gmail.com>
 2015 Joe Hansen <joedalton2@yahoo.dk>
 2015 Chris Leick <c.leick@vollbio.de>
 2015 Software in the Public Interest
 2015 French l10N team <debian-l1On-french@lists.debian.org>
 2015 Beatrice Torracca <beatricet@libero.it>
 2015 Frans Spiesschaert <Frans.Spiesschaert@yucom.be>
 2015 Tomasz Buchert <tomasz@debian.org>
 2015 Łukasz Dulny <BartekChom@poczta.onet.pl>
License: GPL-2+

License: BSD-4-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

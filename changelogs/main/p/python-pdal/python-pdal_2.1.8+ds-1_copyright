Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PDAL's Python Extension
Upstream-Contact: Hobu, Inc. <howard@hobu.co>
Source: https://github.com/PDAL/python
Comment: The upstream sources are repacked to excluded the .egg-info
 directory that is automatically removed by dh_clean.
Files-Excluded: PDAL.egg-info/*

Files: *
Copyright: 2015, Hobu, Inc. <howard@hobu.co>
     2016, 2018, Howard Butler <howard@hobu.co>
           2011, Michael P. Gerlek <mpg@flaxen.com>
License: BSD-3-Clause

Files: debian/*
Copyright: 2016, Bas Couwenberg <sebastic@debian.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1) Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2) Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3) Neither the name of the ORGANIZATION nor the names of its contributors may
    be used to endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.


Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: php-font-lib
Upstream-Contact: Fabien Ménager <fabien.menager@gmail.com>
Source: https://github.com/PhenX/php-font-lib
Files-Excluded: fonts/*
                www/js/jquery-1.5.1.min.js
                www/js/jquery-ui-1.8.14.custom.min.js

Files: *
Copyright: Fabien Ménager <fabien.menager@gmail.com>
License: LGPL-2.1+

Files: www/css/*/jquery-ui-1.8.14.custom.css
Copyright: 2010, 2011, Brandon Aaron
           2010, 2011, Paul Bakaus <paulbakaus.com>
           2010, 2011, David Bolter
           2010, 2011, Rich Caloggero
           2010, 2011, Chi Cheng <cloudream@gmail.com>
           2010, 2011, Colin Clark <http://colin.atrc.utoronto.ca/>
           2010, 2011, Michelle D'Souza
           2010, 2011, Aaron Eisenberger <aaronchi@gmail.com>
           2010, 2011, Ariel Flesler
           2010, 2011, Bohdan Ganicky
           2010, 2011, Scott González
           2010, 2011, Marc Grabanski <m@marcgrabanski.com>
           2010, 2011, Klaus Hartl <stilbuero.de>
           2010, 2011, Scott Jehl
           2010, 2011, Cody Lindley
           2010, 2011, Eduardo Lundgren <eduardolundgren@gmail.com>
           2010, 2011, Todd Parker
           2010, 2011, John Resig
           2010, 2011, Patty Toland
           2010, 2011, Ca-Phun Ung <yelotofu.com>
           2010, 2011, Keith Wood <kbwood@virginbroadband.com.au>
           2010, 2011, Maggie Costello Wachs
           2010, 2011, Richard D. Worth <rdworth.org>
           2010, 2011, Jörn Zaefferer <bassistance.de>
License: Expat or GPL-2

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2
 On Debian systems, the complete text of the GNU General Public License can
 be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian systems, the full text of the GNU Lesser General Public License
 version 2.1 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pveclib
Source: https://github.com/open-power-sdk/pveclib
Files-Excluded: CODE_OF_CONDUCT.md
Comment: File copied from Hyperledger project and license not clear

Files: *
Copyright: 2017-2019 Paul Clarke <pc@us.ibm.com>
           2017-2019 Steven Munroe <munroesj52@gmail.com>
           2017-2019 Tulio Magno Quites Machado Filho <tuliom@linux.ibm.com>
License: Apache-2.0

Files: src/*
Copyright: 2016-2019 IBM Corporation
           2016-2019 Steven Munroe <munroesj52@gmail.com>
License: Apache-2.0

# Even though src/decpowof2.c is licensed under the LGPL-2.1, it is
# composed solely of numerical parameters; thus it falls under the
# exemption clause in Section 5, paragraph 4 of that license; which
# states that use of such files is unrestricted, provided that Section 6
# is also fulfilled. Section 6 is fulfilled, because this package ships
# the complete corresponding code in the source package, it allows user
# modifications, and it ships a copy of the license.
Files: src/decpowof2.c
Copyright: 2006 IBM Corporation
           2007-2015 Free Software Foundation, Inc
License: LGPL-2.1

Files: debian/*
Copyright: 2019 Gabriel F. T. Gomes <gabriel@inconstante.net.br>
License: GPL-3+

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0 can
 be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3.0 can be found in the file `/usr/share/common-licenses/GPL-3'.

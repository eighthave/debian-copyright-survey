Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PyMVPA
Upstream-Contact: pkg-exppsy-pymvpa@lists.alioth.debian.org
Source: https://github.com/PyMVPA/PyMVPA


Files: *
Copyright:
    2006-2012, Michael Hanke <michael.hanke@gmail.com>
    2007-2012, Yaroslav Halchenko <debian@onerussian.com>
    2007-2009, Per B. Sederberg <persed@princeton.edu>
         2008, Emanuele Olivetti <emanuele@relativita.com>
License: Expat

Files: debian/*
Copyright:
    2006-2013, Michael Hanke <michael.hanke@gmail.com>
    2006-2013, Yaroslav Halchenko <debian@onerussian.com>
License: Expat


Files: 3rd/libsvm/*
Copyright: 2000-2007, Chih-Chung Chang and Chih-Jen Lin
License: BSD-3
  The source tarball contains a partial copy of the LIBSVM package.  However,
  the corresponding library is available from the libsvm source package (or
  rather the binary packages it builds). Therefore this code is not used and
  the respective packages are used as dependencies. This copy of the libsvm
  code is, however, left in place, as it does not hurt and does not justify
  repackaging of the upstream sources.
  .
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  SUCH DAMAGE.


Files: tools/pdfbook.c
Copyright:
  Tigran Aivazian <tigran at aivazian.fsnet.co.uk>
  Jaap Eldering <eldering at a-eskwadraat.nl>
  Roman Buchert <roman.buchert at arcor.de>
  Pierre Francois <pf at romanliturgy.org>
License: GPL-2
  On Debian GNU/Linux systems, the complete text of the GPL-2 License
  can be found in `/usr/share/common-licenses/GPL-2'.


License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

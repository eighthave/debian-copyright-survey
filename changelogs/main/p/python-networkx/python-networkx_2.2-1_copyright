Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: networkx
Upstream-Contact: Aric Hagberg <aric.hagberg@gmail.com>
Source: git://github.com/networkx/networkx.git

Files: *
Copyright: (c) 2004-2018, NetworkX Developers
           (c) Aric Hagberg <hagberg@lanl.gov>
           (c) Dan Schult <dschult@colgate.edu>
           (c) Pieter Swart <swart@lanl.gov>
 With additional Authors having left their copyrights to the Upstream Authors
 and Copyright Holders listed just above:
  A. L. Barabási and R. Albert
  A. Steger and N. Wormald
  Brendt Wohlberg
  C. Gkantsidis, M. Mihail, and E. Zegura
  Coen Bron and Joep Kerbosch
  Fan Chung and L. Lu
  Jean-Loup Guillaume and Matthieu Latapy
  Jeong Han Kim and Van H. Vu
  Katy Bold
  Lun Li, David Alderson, Reiko Tanaka, John C. Doyle, and Walter Willinger
  M. E. J. Newman
  P. Holme and B. J. Kim
  P. L. Krapivsky and S. Redner
  Robert Sedgewick
  Russel Merris
  Eben Kenah
  Sasha Gutfraind <ag362@cornell.edu>
  Drew Conway <drew.conway@nyu.edu>
License: BSD-3-clause

Files: debian/*
Copyright: (c) 2006-2009, Cyril Brulebois <cyril.brulebois@enst-bretagne.fr>
           (c) 2010-2018, Sandro Tosi <morph@debian.org>
           (c) 2014, Thomas Goirand <zigo@debian.org>
License: GPL-3

Files: networkx/algorithms/approximation/clique.py
 networkx/algorithms/matching.py
 networkx/algorithms/approximation/dominating_set.py
 networkx/algorithms/approximation/independent_set.py
 networkx/algorithms/approximation/matching.py
 networkx/algorithms/approximation/ramsey.py
 networkx/algorithms/approximation/vertex_cover.py
Copyright: (c) 2011-2012 Nicholas Mancuso <nick.mancuso@gmail.com>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
  * Neither the name of the NetworkX Developers nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-3
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 3 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License v2
 (GPL) can be found in /usr/share/common-licenses/GPL-3.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pyephem
Source: https://pypi.python.org/pypi/ephem/

Files: *
Copyright: 2008, 2009 Brandon Craig Rhodes
 1990-2009 Dr. Craig A. Counterman
 1986-1990 Robert Berger (N3EMO)
 Elwood Charled Downey
License: LGPL-3
 The "libastro" C library, which is contained within a subdirectory of
 this package, is Copyright (C) 2009 by:
 .
  * Dr. Craig A. Counterman, whose code is used in "precess.c".
  * Robert Berger (N3EMO), whose code is used in "earthsat.c".
  * Elwood Charles Downey, who, with the permission of those earlier
    authors, wrote the rest of "libastro" thereby creating a derived
    work which he distributes with his "XEphem" application.
  * Brandon Craig Rhodes, who lightly modified some sections of the code
    to make them behave when run under Python within PyEphem.
 .
 The "PyEphem" Python module, which includes everything else in this
 package, is Copyright (C) 2009 by Brandon Craig Rhodes.
 .
 All of the above authors agree that you can redistribute and/or modify
 this combined work under the terms of the GNU Lesser General Public
 License (LGPL) version 3, as published by the Free Software Foundation.
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 3 can be found in the file
 `/usr/share/common-licenses/LGPL-3'.
Comment: Some other authors contributed to the code which do not appear as
 copyright holders: Paul Schlyter, S. L. Moshier, Michael Sternberg,
 P. Bretagnon, G. Francou, Richard Clark, Atsuo Ohki, Robert Lane, Ernie
 Wright, Chris Marriott, RLM, Chapront, Paul Martz, and Konrad Bernloehr

Files: extensions/dtoa.c
Copyright: 1991-2001 Lucent Technologies (David M. Gay)
License: Expat
 Permission to use, copy, modify, and distribute this software for any
 purpose without fee is hereby granted, provided that this entire notice
 is included in all copies of any software which is or includes a copy
 or modification of this software and in all copies of the supporting
 documentation for such software.
 .
 THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTY.  IN PARTICULAR, NEITHER THE AUTHOR NOR LUCENT MAKES ANY
 REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
 OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.

Files: debian/*
Copyright: 2015 Ole Streicher <olebole@debian.org>
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

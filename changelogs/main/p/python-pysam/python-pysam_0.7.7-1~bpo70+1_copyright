Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pysam
Upstream-Contact: Andreas Heger <andreas.heger@gmail.com>
Source: https://pypi.python.org/packages/source/p/pysam/pysam-0.7.7.tar.gz

Files: *
Copyright: 2008-2010 Genome Research Ltd.
License: MIT

Files: samtools/khash.h samtools/kseq.h
Copyright: 2008-2011 by Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: samtools/kprobaln.c.pysam.c samtools/kaln.c.pysam.c samtools/kaln.h samtools/kprobaln.h
Copyright: 2003-2006, 2008-2010, by Heng Li <lh3lh3@live.co.uk>
License: MIT

Files: tabix/tabix.h
Copyright: 2009 Genome Research Ltd (GRL), 2010 Broad Institute
License: MIT

Files: samtools/bcftools/bcf.h
Copyright: 2010 Broad Institute
License: MIT

Files: samtools/bgzf.c.pysam.c samtools/bgzf.h tabix/bgzf.c.pysam.c tabix/bgzf.h samtools/knetfile.c.pysam.c
Copyright: 2008 Broad Institute / Massachusetts Institute of Technology
           2010-2012 Attractive Chaos <attractor@live.co.uk>
License: MIT

Files: tabix/bgzip.c.pysam.c
Copyright: 2008 Broad Institute / Massachusetts Institute of Technology
License: MIT

Files: samtools/razf.c.pysam.c samtools/razf.h
Copyright: 2008 Jue Ruan <ruanjue@gmail.com>, Heng Li <lh3@sanger.ac.uk>
License: BSDlike1
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: samtools/win32/zconf.h samtools/win32/zlib.h
Copyright: 1995-2005 Jean-loup Gailly <jloup@gzip.org> and Mark Adler <madler@alumni.caltech.edu>
License: BSDlike2
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
 .
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
 .
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
Comment: These files are not used and could be stripped from the source

Files: samtools/win32/xcurses.h
Copyright: 2008 wmcbrine
License: PublicDomain
 This file is in public domain.
Comment: These files are not used and could be stripped from the source

Files: samtools/misc/md5.*
Copyright: 1993 Colin Plumb
License: PublicDomain
 No copyright is claimed.
 This code is in the public domain; do with it what you wish.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.


Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Padre
Upstream-Contact: Gabor Szabo
Source: https://metacpan.org/release/Padre
X-Repackaged-Source: Yes
 Upstream tarball contains several files that don't comply with the DFSG. These 
 were removed. Re-packaging is done by the debian/repack.sh script.
 - share/padre-splash-ccnc.png (uses CC-NC license forbidding non-commercial 
   use)
 - share/languages/perl5/perlapi_current.yml (a bundled copy of a file to be 
   found in libperl-apireference-perl; removed on the grounds of requiring 
   everything to be compiled from source)

Files: *
Copyright: 2008-2013 The Padre development team as listed in Padre.pm
           Aaron Trevena (TEEJAY)
           Ahmad Zawawi أحمد محمد زواوي (AZAWAWI)
           Adam Kennedy (ADAMK) <adamk@cpan.org>
           Alexandr Ciornii (CHORNY)
           Blake Willmarth (BLAKEW)
           Breno G. de Oliveira (GARU)
           Brian Cassidy (BRICAS)
           Burak Gürsoy (BURAK) <burak@cpan.org>
           Cezary Morga (THEREK) <cm@therek.net>
           Chris Dolan (CHRISDOLAN)
           Claudio Ramirez (NXADM) <nxadm@cpan.org>
           Fayland Lam (FAYLAND) <fayland@gmail.com>
           Gábor Szabó - גאבור סבו (SZABGAB) <szabgab@gmail.com>
           Gabriel Vieira (GABRIELMAD)
           Heiko Jansen (HJANSEN) <heiko_jansen@web.de>
           Jérôme Quelin (JQUELIN) <jquelin@cpan.org>n
           Kaare Rasmussen (KAARE) <kaare@cpan.org>
           Keedi Kim - 김도형 (KEEDI)
           Kenichi Ishigaki - 石垣憲一 (ISHIGAKI) <ishigaki@cpan.org>
           Mark Grimes <mgrimes@cpan.org>
           Max Maischein (CORION)
           Olivier Mengué (DOLMEN)
           Patrick Donelan (PDONELAN) <pat@patspam.com>
           Paweł Murias (PMURIAS)
           Petar Shangov (PSHANGOV)
           Ryan Niebur (RSN) <rsn@cpan.org>
           Sebastian Willing (SEWI)
           Steffen Müller (TSEE) <smueller@cpan.org>
           Zeno Gantner
           ExtUtils::MakeMaker-borrowed code:
           Andy Dougherty <doughera@lafayette.edu>
           Andreas König <andreas.koenig@mind.de>
           Tim Bunce <timb@cpan.org>
           Charles Bailey <bailey@newman.upenn.edu>
           Ilya Zakharevich <ilya@math.ohio-state.edu>
           Michael G Schwern <schwern@pobox.com>
           Translators:
           Ahmad M. Zawawi - أحمد محمد زواوي (AZAWAWI)
           Fayland Lam (FAYLAND)
           BlueT - Matthew Lien - 練喆明 (BLUET) <bluet@cpan.org>
           Chuanren Wu
           Dirk De Nijs (ddn123456)
           Jérôme Quelin (JQUELIN)
           Olivier Mengué (DOLMEN)
           Heiko Jansen (HJANSEN)
           Sebastian Willing (SEWI)
           Zeno Gantner
           Omer Zak  - עומר זק
           Shlomi Fish  - שלומי פיש (SHLOMIF)
           Amir E. Aharoni - אמיר א. אהרוני
           György Pásztor (GYU)
           Simone Blandino (SBLANDIN)
           Kenichi Ishigaki - 石垣憲一 (ISHIGAKI)
           Keedi Kim - 김도형 (KEEDI)
           Andrew Shitov
           Anatoly Sharifulin <sharifulin@gmail.com>
           Vladimir Lettiev <thecrux@gmail.com>
           Cezary Morga (THEREK)
           Breno G. de Oliveira (GARU)
           Gabriel Vieira
           Paco Alguacil (PacoLinux)
           Enrique Nell (ENELL) <blas.gordon@gmail.com>
           Marcela Mašláňová (mmaslano)
           Kjetil Skotheim (KJETIL)
           Oren Maurer <meorero@gmail.com>
           Pásztor György <pasztor@linux.gyakg.u-szeged.hu>
License: Artistic or GPL-1+

Files: lib/Padre/DB/Snippets.pod
Copyright: 2009 - 2010 Adam Kennedy
License: Artistic or GPL-1+

Files: lib/Wx/Perl/Dialog/Simple.pm
Copyright: Copyright 2008 Gábor Szabó. L<http://www.szabgab.com/>
License: Artistic or GPL-1+

Files: share/examples/wx/40_draw.pl
       share/examples/wx/41-drag-image.pl
       share/examples/wx/42-drag-image-no-tail.pl
Copyright: (c) 2001, 2003, 2005-2006 Mattia Barbon
License: Artistic or GPL-1+

Files: share/doc/perlopquick/perlopquick.pod
Copyright: perlopquick is Copyright (C) 2010 by Chas. Owens and contains some
           text from the Perl POD documentation which is Copyright (C) 1993, 1994, 1995,
           1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008,
           2009 by Larry Wall and others.  All rights reserved.
License: Artistic or GPL-1+

Files: share/padre-splash.bmp
Copyright: 2009 Ahmad M. Zawawi
License: Artistic or GPL-1+

Files: share/icons/gnome218/*
Copyright: 2007 Lapo Calamandrei <calamandrei@gmail.com>
           Rodney Dawes <dobey@novell.com>
           Luca Ferretti <elle.uca@libero.it>
           Tuomas Kuosmanen <tigert@gimp.org>
           Andreas Nilsson <nisses.mail@home.se>
           Jakub Steiner <jimmac@novell.com>
License: GPL-2
X-Note: these icons are taken from gnome-icon-theme 2.18.
    Licensing/copyright information extracted from
    ftp://ftp.gnome.org/pub/GNOME/sources/gnome-icon-theme/2.18/gnome-icon-theme-2.18.0.tar.bz2

Files: share/icons/padre/16x16/status/*
Copyright: Copyright 2008 Steffen Mueller.
License: Artistic or GPL-1+

Files: share/icons/padre/16x16/actions/toggle-comments.png
Copyright: Copyright 2009 Breno G. de Oliveira.
License: Artistic or GPL-1+

Files: share/icons/padre/16x16/actions/x-document-close.png
Copyright: Copyright (C) 2007 David Vignoni <david@icon-king.com>
           Copyright (C) 2007 Johann Ollivier Lapeyre <johann@oxygen-icons.org>
           Copyright (C) 2007 Kenneth Wimer <kwwii@bootsplash.org>
           Copyright (C) 2007 Nuno Fernades Pinheiro <nf.pinheiro@gmail.com>
           Copyright (C) 2007 Riccardo Iaconelli <riccardo@oxygen-icons.org>
           Copyright (C) 2007 David Miller <miller@oxygen-icons.org>
           and others
License: LGPL-3+
 The GNU Lesser General Public License or LGPL is written for
 software libraries in the first place. We expressly want the LGPL to
 be valid for this artwork library too.
 .
 KDE Oxygen theme icons is a special kind of software library, it is an
 artwork library, it's elements can be used in a Graphical User Interface, or
 GUI.
 .
 Source code, for this library means:
  - where they exist, SVG;
  - otherwise, if applicable, the multi-layered formats xcf or psd, or
 otherwise png.
 .
 The LGPL in some sections obliges you to make the files carry
 notices. With images this is in some cases impossible or hardly useful.
 .
 With this library a notice is placed at a prominent place in the directory
 containing the elements. You may follow this practice.
 .
 The exception in section 5 of the GNU Lesser General Public License covers
 the use of elements of this art library in a GUI.
Note: Taken from the KDE4 Oxygen theme actions/application-exit.

Files: share/icons/padre/16x16/logo.png
       share/icons/padre/all/padre.ico
       win32/padre.ico
Copyright: Copyright 2003 Gregory Phillips
           Copyright 2009, 2010 Adam Kennedy
License: GFDL-1.2+ or CC-SA-3.0
 Original source: 
 http://commons.wikimedia.org/wiki/File:Blue_morpho_butterfly.jpg
 .
 Transform by http://commons.wikimedia.org/wiki/User:Lycaon as transparent 
 image as http://commons.wikimedia.org/wiki/File:Morpho_menelaus.png
 .
 Colour-enhanced and modified for transparent icon use by Adam Kennedy.
 .
 According to 
 http://commons.wikimedia.org/wiki/Commons:Reusing_content_outside_Wikimedia#How_to_comply_with_the_licenses 
 files with several licenses can be used under either one of them. 
 Consequently, the logo image can be used under the terms of either of the 
 following:
 .
 GFDL-1.2:
   Permission is granted to copy, distribute and/or modify this document under 
   the terms of the GNU Free Documentation License, Version 1.2 or any later 
   version published by the Free Software Foundation; with no Invariant 
   Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the 
   license is included in the section entitled "GNU Free Documentation 
   License".
 .
   http://en.wikipedia.org/wiki/GNU_Free_Documentation_License
   The full text of the GNU Free Documentation License can be found at 
   /usr/share/common-licenses/GFDL-1.2 on every Debian system.
 .
 CC-SA-3.0:
   This file is licensed under the Creative Commons Attribution ShareAlike 3.0 
   License. In short: you are free to share and make derivative works of the 
   file under the conditions that you appropriately attribute it, and that you 
   distribute it only under a license identical to this one.
 .
   http://creativecommons.org/licenses/by-sa/3.0/

Files: t/collection/Config-Tiny/*
Copyright: Copyright 2002 - 2007 Adam Kennedy.
License: Artistic or GPL-1+

Files: inc/Module/Install/*
Copyright: Copyright 2002 - 2010 by Brian Ingerson, Audrey Tang and Adam Kennedy
License: Artistic or GPL-1+

Files: inc/Module/Install/PRIVATE/*
       privinc/*
Copyright: 2008-2010 The Padre development team as listed in Padre.pm.
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2008-2010, Damyan Ivanov <dmn@debian.org>
 2011-2013 Dominique Dumont <dod@debian.org>
License: Artistic or GPL-1+

License: Artistic
    This program is free software; you can redistribute it and/or modify
    it under the terms of the Artistic License, which comes with Perl.
 .
    On Debian systems, the complete text of the Artistic License can be found 
    in /usr/share/common-licenses/Artistic

License: GPL-1+
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by 
    the Free Software Foundation; either version 1, or (at your option)
    any later version.
 .
    On Debian systems, the complete text of the GNU General Public License 
    version 1 can be found in `/usr/share/common-licenses/GPL-1'

License: GPL-2
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 dated June, 1991.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
    USA.
 .
    On Debian systems, the complete text of the GNU General Public License 
    version 2 can be found in `/usr/share/common-licenses/GPL-2'

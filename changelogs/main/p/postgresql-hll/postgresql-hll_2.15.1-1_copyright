Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: postgresql-hll
Source: https://github.com/citusdata/postgresql-hll

Files: *
Copyright:
 2013 Timon Karnezos <timon.karnezos@gmail.com>
 Copyright 2013 Aggregate Knowledge, Inc.
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.

Files: src/MurmurHash3.*
Copyright:
 MurmurHash3 was written by Austin Appleby, and is placed in the public
 domain. The author hereby disclaims copyright to this source code.
License: public
 Public domain.

Files: debian/*
Copyright: 2013 Cyril Bouthors <cyril@bouthors.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

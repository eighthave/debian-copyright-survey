Format-Specification: http://svn.debian.org/wsvn/dep/web/deps/dep5.mdwn?op=file&rev=135
Name: pyferret
Maintainer: Alastair McKinstry <mckinstry@debian.org>
Source: http://www.ferret.noaa.gov/

Copyright: 2010  NOAA, US.gov
License: Ferret License Agreement

Files: debian/*
Copyright: 2011 Alastair McKinstry <mckinstry@debian.org>
License: GPL-2

Files: fer/gui/simx.h, fer/gui/xpm*.h
Copyright:  1989-94 GROUPE BULL
License: Expat

Files: fer/gui/Ux*
Copyright: Visual Edge Software, Ltd. 1989 - 1993
License: Visual-Edge

Files: ext_func/vis5d/*
Copyright: 1990 - 2000 Bill Hibbard, Johan Kellum, Brian Paul,  Dave Santek, and Andre Battaiola.
License: GPL-2+

Files: ext_func/src/pnpoly/*, external_functions/pnpoly/*
Copyright: 1970-2003, Wm. Randolph Franklin
License: NOAA-PMEL-TMAP

Files: ext_func/src/extrema/*
Copyright: 2000 University Corporation for Atmospheric Research
License: GPL-2+

Ferret License Agreement:
 This software is distributed under the Open Source Definition, which may be found at http://www.opensource.org/osd.html.
 .
 In particular, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain this notice, this list of conditions, and the following disclaimer.
 * Redistributions in binary form must provide access to this notice, this list of conditions, the following disclaimer, and the underlying source code.
 * All modifications to this software must be clearly documented, and are solely the responsibility of the agent making the modifications.
 * If significant modifications or enhancements are made to this software, the developers should be notified. 
 .
 THIS SOFTWARE AND ITS DOCUMENTATION ARE IN THE PUBLIC DOMAIN AND ARE FURNISHED "AS IS." THE AUTHORS, THE UNITED STATES GOVERNMENT, ITS INSTRUMENTALITIES, OFFICERS, EMPLOYEES, AND AGENTS MAKE NO WARRANTY, EXPRESS OR IMPLIED, AS TO THE USEFULNESS OF THE SOFTWARE AND DOCUMENTATION FOR ANY PURPOSE. THEY ASSUME NO RESPONSIBILITY (1) FOR THE USE OF THE SOFTWARE AND DOCUMENTATION; OR (2) TO PROVIDE TECHNICAL SUPPORT TO USERS.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.


License: IBM-UCAR
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the name of IBM not be used in
 advertising or publicity pertaining to distribution of the software
 without specific, written prior permission.
 .
 IBM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT
 SHALL IBM BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES
 OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose without fee is hereby granted, provided that
 the above copyright notice appears in all copies, that both that copyright
 notice and this permission notice appear in supporting documentation, and
 that the name of UCAR/Unidata not be used in advertising or publicity
 pertaining to distribution of the software without specific, written prior
 permission.  UCAR makes no representations about the suitability of this
 software for any purpose.  It is provided "as is" without express or
 implied warranty.  It is provided with no support and without obligation on
 the part of UCAR or Unidata, to assist in its use, correction,
 modification, or enhancement.

License: Visual-Edge
 ALL  RIGHTS  RESERVED.  Permission  to  use,  copy,  modify,  and
 distribute  this  software  and its documentation for any purpose
 and  without  fee  is  hereby  granted,  provided  that the above
 copyright  notice  appear  in  all  copies  and  that  both  that
 copyright  notice and this permission notice appear in supporting
 documentation,  and that  the name of Visual Edge Software not be
 used  in advertising  or publicity  pertaining to distribution of
 the software without specific, written prior permission. The year
 included in the notice is the year of the creation of the work.

License: IBM
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation, and that the name of IBM not be
 used in advertising or publicity pertaining to distribution of the
 software without specific, written prior permission.
 .
 IBM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 IBM BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.

License: NOAA-PMEL-TMAP
 This software was developed by the Thermal Modeling and Analysis
 Project(TMAP) of the National Oceanographic and Atmospheric
 Administration's (NOAA) Pacific Marine Environmental Lab(PMEL),
 hereafter referred to as NOAA/PMEL/TMAP.
 .
 Access and use of this software shall impose the following
 obligations and understandings on the user. The user is granted the
 right, without any fee or cost, to use, copy, modify, alter, enhance
 and distribute this software, and any derivative works thereof, and
 its supporting documentation for any purpose whatsoever, provided
 that this entire notice appears in all copies of the software,
 derivative works and supporting documentation.  Further, the user
 agrees to credit NOAA/PMEL/TMAP in any publications that result from
 the use of this software or in any product that includes this
 software. The names TMAP, NOAA and/or PMEL, however, may not be used
 in any advertising or publicity to endorse or promote any products
 or commercial entity unless specific written permission is obtained
 from NOAA/PMEL/TMAP. The user also understands that NOAA/PMEL/TMAP
 is not obligated to provide the user with any support, consulting,
 training or assistance of any kind with regard to the use, operation
 and performance of this software nor to provide the user with any
 updates, revisions, new versions or "bug fixes".
 .
 THIS SOFTWARE IS PROVIDED BY NOAA/PMEL/TMAP "AS IS" AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL NOAA/PMEL/TMAP BE LIABLE FOR ANY SPECIAL,
 INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 CONTRACT, NEGLIGENCE OR OTHER TORTUOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE. 

License: Expat

 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * <COPYRIGHT HOLDER> BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name of <COPYRIGHT HOLDER> shall not be
 * used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization from <COPYRIGHT HOLDER>.


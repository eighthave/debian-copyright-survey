Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PrBoom+
Upstream-Contact: http://sourceforge.net/projects/prboom-plus/
Source: https://svn.prboom.org/repos/branches/prboom-plus-24/prboom2/

Files: *
Copyright:
  © 1993-1996, Id Software, Inc.
  © 1993-2011, PrBoom developers
  © 1999-2006, Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
  © 1999, id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
  © 1999, Thomas A. Fine
  © 2000-2004, Underbit Technologies, Inc
  © 2002-2010, The DOSBox Team
  © 2005-2006, Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
  © 2005-2009, Simon Howard
  © 2006-2008, G Jackson, Jaakko Kerônen
  © 2006, Ben Ryves
  © 2006, Neil Stevens
  © 2009, Andrey Budko
  © 2011, Nicholai Main
License: GPL-2+

Files: src/d_server.c
 src/gl_clipper.c
 src/gl_hqresize.c
 src/r_patch.c
Copyright:
  © 1987-1994, The Regents of the University of California
  © 1999, id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
  © 1999-2004, Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
  © 2003, Tim Stump
  © 2004-2006, Randy Heit
  © 2005-2006, Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
  © 2008, Benjamin Berkels
License: GPL-2+ and BSD-3-clause

Files: src/e6y.c
 src/gl_vertex.c
Copyright:
  © 1999, id Software, Chi Hoang, Lee Killough, Jim Flynn, Rand Phares, Ty Halderman
  © 1999-2000, Jess Haas, Nicolas Kalkhof, Colin Phipps, Florian Schulze
  © 2005-2006, Florian Schulze, Colin Phipps, Neil Stevens, Andrey Budko
  © 2004-2006, Christoph Oelckers
License: GPL-2+, and BSD-3-clause or LGPL-2.1+

Files: src/gl_sky.c
Copyright:
  © 2003, Tim Stump
  © 2005, Christoph Oelckers
  © 2009, Andrey Budko
License: BSD-3-clause with disclosure exception
 ** Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions
 ** are met:
 **
 ** 1. Redistributions of source code must retain the above copyright
 **    notice, this list of conditions and the following disclaimer.
 ** 2. Redistributions in binary form must reproduce the above copyright
 **    notice, this list of conditions and the following disclaimer in the
 **    documentation and/or other materials provided with the distribution.
 ** 3. The name of the author may not be used to endorse or promote products
 **    derived from this software without specific prior written permission.
 ** 4. Full disclosure of the entire project's source code, except for third
 **    party libraries is mandatory. (NOTE: This clause is non-negotiable!)
 **
 ** THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 ** IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 ** OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 ** IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 ** INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 ** NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 ** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/md5.*
Copyright: -
License: public-domain
 * This code implements the MD5 message-digest algorithm.
 * The algorithm is due to Ron Rivest.  This code was
 * written by Colin Plumb in 1993, no copyright is claimed.
 * This code is in the public domain; do with it what you wish.
 *
 * Equivalent code is available from RSA Data Security, Inc.
 * This code has been tested against that, and is equivalent,
 * except that you don't need to include two pages of legalese
 * with every copy.
 *
 * To compute the message digest of a chunk of bytes, declare an
 * MD5Context structure, pass it to MD5Init, call MD5Update as
 * needed on buffers full of bytes, and then call MD5Final, which
 * will fill a supplied 16-byte array with the digest.
 *
 * Changed so as no longer to depend on Colin Plumb's `usual.h' header
 * definitions; now uses stuff from dpkg's config.h.
 *  - Ian Jackson <ian@chiark.greenend.org.uk>.
 * Still in the public domain.

Files: src/MAC/ConsoleController.h
 src/MAC/ConsoleController.m
 src/MAC/DrawerButton.h
 src/MAC/DrawerButton.m
 src/MAC/FileButtonController.h
 src/MAC/FileButtonController.m
 src/MAC/LauncherApp.h
 src/MAC/LauncherApp.m
 src/MAC/LauncherMain.m
 src/MAC/WadViewController.h
 src/MAC/WadViewController.m
Copyright: -
License: public-domain
 // This file is hereby placed in the Public Domain -- Neil Stevens

Files: src/MAC/UKFileWatcher.h
 src/MAC/UKKQueue.h
 src/MAC/UKKQueue.m
 src/MAC/UKMainThreadProxy.h
 src/MAC/UKMainThreadProxy.m
Copyright: © 2003-2005, M. Uli Kusterer
License: GPL-2+ or BSD-3-clause

Files: src/MAC/ANSIString.h
 src/MAC/ANSIString.m
 src/MAC/ResolutionDataSource.h
 src/MAC/ResolutionDataSource.m
Copyright: © 2006, Neil Stevens <neil@hakubi.us>
License: Expat
 // Permission is hereby granted, free of charge, to any person obtaining a copy
 // of this software and associated documentation files (the "Software"), to deal
 // in the Software without restriction, including without limitation the rights
 // to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 // copies of the Software, and to permit persons to whom the Software is
 // furnished to do so, subject to the following conditions:
 //
 // The above copyright notice and this permission notice shall be included in
 // all copies or substantial portions of the Software.
 //
 // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 // IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 // FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 // THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 // AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 // CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 //
 // Except as contained in this notice, the name(s) of the author(s) shall not be
 // used in advertising or otherwise to promote the sale, use or other dealings
 // in this Software without prior written authorization from the author(s).

Files: debian/*
Copyright:
  © 2000-2003, Arthur Korn <arthur@korn.ch>
  © 2004-2006, Moritz Muehlenhoff <jmm@inutil.org>
  © 2006-2013, Jon Dowland <jon@alcopop.org>, Debian Games Team
  © 2012-2015, Fabian Greffrath <fabian+debian@greffrath.com>, Debian Games Team
License: GPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
Comment: On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

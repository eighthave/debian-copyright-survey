Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: papi
Source: https://icl.cs.utk.edu/papi/software/index.html

Files: *
Copyright: 2005-2015 University of Tennessee
License: BSD-3-Clause

Files: src/components/appio/tests/iozone/gengnuplot.sh
Copyright: 2001 Yves Rougy <Yves@Rougy.net>
License: GPL-2+

Files: src/components/infiniband/pscanf.h
Copyright: (C) 2011 University of Texas at Austin
License: LGPL-2.1+
Comment:
 https://github.com/TACC/tacc_stats/blob/master/README.md

Files: src/libpfm-3.y/*
Copyright: 2001-2007 Hewlett-Packard Development Company, L.P.
	   2006-2010 Advanced Micro Devices, Inc.
	   2006-2009 IBM Corporation
	   2007      Cray Inc.
	   2007      David S. Miller <davem@davemloft.net>
	   2007      TOSHIBA CORPORATION
	   2008-2010 Google, Inc.
	   2008      Stephane Eranian <eranian@gmail.com>
License: BSD-like
Comment:
 This convenience copy is *not* used to build the Debian packages.

Files: src/libpfm-3.y/*/multiplex*
Copyright: 2002-2006 Hewlett-Packard Development Company, L.P.
License: GPL-2+
Comment:
 This convenience copy is *not* used to build the Debian packages.

Files: src/libpfm-3.y/include/perfmon/perfmon_pebs_p4_smpl.h
       src/libpfm-3.y/include/perfmon/perfmon_pebs_smpl.h
Copyright: 2005-2007 Hewlett-Packard Development Company, L.P.
	   2009 Google, Inc.
License: GPL-2
Comment:
 This convenience copy is *not* used to build the Debian packages.

Files: src/libpfm4/*
Copyright: 2002-2006 Hewlett-Packard Development Company, L.P.
	   2009-2012 Arun Sharma <aruns@google.com>
	   2012-2014 Carl E. Love <cel@linux.vnet.ibm.com>
	   2009-2011 Corey Ashford <cjashfor@linux.vnet.ibm.com>
	   2010      Dan Terpstra <terpstra@eecs.utk.edu>
	   2014      Gary Mohr <Gary.Mohr@bull.com>
	   2012-2014 Hendrik Brueckner <brueckner@linux.vnet.ibm.com>
	   2013      Jacob Shin <jacob.shin@amd.com>
	   2014      James Ralph <ralph@icl.utk.edu>
	   2012      Joerg Henrichs <joerg@luding.org>
	   2012      Maynard Johnson <maynardj@us.ibm.com>
	   2013      Michael Werner <xaseron@users.sf.net>
	   2011      Philip Mucci <phil.mucci@@samaratechnologygroup.com>
	   2010      Robert Richter <robert.richter@amd.com>
	   2009-2015 Stephane Eranian <eranian@gmail.com>
	   2010-2014 Steve Kaufmann <sbk@cray.com>
	   2009-2014 Vince Weaver <vince@csl.cornell.edu>
	   2011-2014 William Cohen <wcohen@redhat.com>
License: BSD-like
Comment:
 This convenience copy is *not* used to build the Debian packages.

Files: src/perfctr-2.*/*
Copyright: 1999-2010 Mikael Pettersson
License: LGPL-2.1+
Comment:
 This convenience copy is *not* used to build the Debian packages.

Files: debian/*
Copyright: 2013 Vincent Danjean <vdanjean@debian.org>
           © 2013-2016 Andreas Beckmann <anbe@debian.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University of Tennessee nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-like
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".

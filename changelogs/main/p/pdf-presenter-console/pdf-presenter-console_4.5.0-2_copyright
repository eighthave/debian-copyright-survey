Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pdfpc
Source:
    Earlies versions were from
    http://github.com/jakobwesthoff/Pdf-Presenter-Console
    git://github.com/jakobwesthoff/Pdf-Presenter-Console.git
    Version 3.0 was downloaded from the fork
    git://github.com/davvil/pdfpc.git
    Current (as of early 2015) epicentre for upstream development is
    http://pdfpc.github.io/pdfpc/
    git://github.com/pdfpc/pdfpc.git

Files: *
Copyright:
    2009-2011 Jakob Westhoff <jakob@westhoffswelt.de>
    2010 Joachim Breitner <mail@joachim-breitner.de>
    2011-2013 David Vilar <davvil@gmail.com>
    2012-2015 Robert Schroll <rschroll@gmail.com>
    2012-2015 Andreas Bilke <andreas@bilke.org>
    2012-2015 Robert Schroll <rschroll@gmail.com>
License: GPL-3

Files: cmake/vala/*.cmake
Copyright:
    2009-2010 Jakob Westhoff
    2010-2011 Daniel Pfeifer
License: BSD-2-clause

Files: debian/*
Copyright: 2010-2015 Barak A. Pearlmutter <bap@debian.org>
License: public-domain
 This material is hereby released to the public domain.

License: BSD-2-clause
    # Redistribution and use in source and binary forms, with or without
    # modification, are permitted provided that the following conditions are met:
    #
    #    1. Redistributions of source code must retain the above copyright notice,
    #       this list of conditions and the following disclaimer.
    #
    #    2. Redistributions in binary form must reproduce the above copyright notice,
    #       this list of conditions and the following disclaimer in the documentation
    #       and/or other materials provided with the distribution.
    #
    # THIS SOFTWARE IS PROVIDED BY JAKOB WESTHOFF ``AS IS'' AND ANY EXPRESS OR
    # IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
    # MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
    # EVENT SHALL JAKOB WESTHOFF OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    # INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    # LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    # PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    # LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    # OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    # ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    #
    # The views and conclusions contained in the software and documentation are those
    # of the authors and should not be interpreted as representing official policies,
    # either expressed or implied, of Jakob Westhoff

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

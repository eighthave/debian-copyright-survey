Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pngquant
Upstream-Contact: Kornel Lesiński <kornel@pngquant.org>
Source: http://pngquant.org/
Comment: pngquant inherits both licenses, one for each source file
 (pngquant.c and rwpng.c). Note that both licenses are basically
 BSD-like; that is, use the code however you like, as long as you
 acknowledge its origins.

Files: mediancut.c pam.c pam.h pngquant.c
Copyright: 1989, 1991 Jef Poskanzer <jef@acme.com>
	   1997, 2000, 2002 Greg Roelofs <newt@pobox.com>
	   2009-2013 Kornel Lesinski <kornel@pngquant.org>
License: BSD-Like
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation.  This software is provided "as is" without express or
 implied warranty.

Files: rwpng.c rwpng.h
Copyright: 1998-2000 Greg Roelofs <newt@pobox.com>
License: BSD-Like
 This software is provided "as is," without warranty of any kind,
 express or implied.  In no event shall the author or contributors
 be held liable for any damages arising in any way from the use of
 this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute
 it freely, subject to the following restrictions:
 .
 1. Redistributions of source code must retain the above copyright
    notice, disclaimer, and this list of conditions.
 2. Redistributions in binary form must reproduce the above copyright
    notice, disclaimer, and this list of conditions in the documenta-
    tion and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this
    software must display the following acknowledgment:
 .
      This product includes software developed by Greg Roelofs
      and contributors for the book, "PNG: The Definitive Guide,"
      published by O'Reilly and Associates.

Files: debian/*
Copyright: 2005-2013 Nelson A. de Oliveira <naoliv@debian.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

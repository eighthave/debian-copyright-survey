This package was debianized by Dmitry Kurochkin <dmitry.kurochkin@measurement-factory.com> on
Sat, 11 Jul 2009 19:59:50 +0400.

It was downloaded from http://www.web-polygraph.org/downloads

Upstream Author:

    The Measurement Factory, Inc. <info@measurement-factory.com>

Copyright:

    Copyright (C) 2003-2006 The Measurement Factory, Inc. <info@measurement-factory.com>

License:

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

On Debian systems, the complete text of the Apache version 2.0 license
can be found in `/usr/share/common-licenses/Apache-2.0'.

Portions of NTLM authentication code (src/client/NtlmAuth.(h|cc) files) are
derived from libcurl of cURL project, circa v7.16.0 (http://curl.haxx.se/):

  Copyright (c) 1996 - 2006, Daniel Stenberg, <daniel@haxx.se>.

  All rights reserved.

  Permission to use, copy, modify, and distribute this software for any purpose
  with or without fee is hereby granted, provided that the above copyright
  notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
  NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
  OR OTHER DEALINGS IN THE SOFTWARE.

  Except as contained in this notice, the name of a copyright holder shall not
  be used in advertising or otherwise to promote the sale, use or other dealings
  in this Software without prior written authorization of the copyright holder.

MD5 checksum code (src/xstd/Checksum.cc) is derived from RFC 1321,
Appendix 3:

  Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
  rights reserved. License to copy and use this software is granted
  provided that it is identified as the "RSA Data Security, Inc. MD5
  Message-Digest Algorithm" in all material mentioning or referencing
  this software or this function. License is also granted to make and
  use derivative works provided that such works are identified as
  "derived from the RSA Data Security, Inc. MD5 Message-Digest
  Algorithm" in all material mentioning or referencing the derived
  work. RSA Data Security, Inc. makes no representations concerning
  either the merchantability of this software or the suitability of
  this software for any particular purpose. It is provided "as is"
  without express or implied warranty of any kind. These notices must
  be retained in any copies of any part of this documentation and/or
  software.

Fowler–Noll–Vo hash function implementation (src/xstd/gadgets.h file)
is based on pseudo code from http://www.isthe.com/chongo/tech/comp/fnv/.
FNV hash algorithms and source code are in public domain.

Packaging:

    Copyright (C) 2009 The Measurement Factory, Inc. <info@measurement-factory.com>

and is licensed under the Apache version 2.0,
see `/usr/share/common-licenses/Apache-2.0'.

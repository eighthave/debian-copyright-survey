Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Net_SmartIRC
Source: http://pear.php.net/package/Net_SmartIRC/download/

Files: *
Copyright: 2002-2005, Mirco Bauer <meebey@meebey.net>
License: LGPL-2.1+

Files: Net_SmartIRC-*/docs/HTML/media/TreeMenu.js
Copyright: 2002, Richard Heyes, Harald Radi
License: BSD-3-clause

Files: Net_SmartIRC-*/docs/HTML/media/lib/tabpane.js
Copyright: 2002, Erik Arvidsson <erik@eae.net>
License: Apache-2.0

Files: Net_SmartIRC-*/docs/HTML/media/lib/ua.js
Copyright: 2001, Bob Clary, Netscape Communications
License: Netscape

License: Apache-2.0
 On Debian systems, the full text of the Apache license version 2 can be found
 in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 o Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 o Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 o The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: Netscape
 Netscape grants you a royalty free license to use, modify and 
 distribute this software provided that this copyright notice 
 appears on all copies.  This software is provided "AS IS," 
 without a warranty of any kind.

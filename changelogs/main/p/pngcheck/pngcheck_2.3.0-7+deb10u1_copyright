Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: pngcheck
Upstream-Contact: Greg Roelofs <newt@pobox.com>, <roelofs@users.sf.net>
Source: http://freshmeat.net/projects/pngcheck
X-Source:
 http://sourceforge.net/projects/png-mng
 http://www.libpng.org/pub/png/apps/pngcheck.html
X-Upstream-Vcs:
 None at sourceforge
X-Upstream-Bugs:
 No bug tracker. The Devel lists exist, but posts are allowed only from
 subscribers:
 http://dir.gmane.org/gmane.comp.graphics.png.devel
 https://lists.sourceforge.net/mailman/listinfo/png-mng-implement
Comment:
 Upstream's personal homepage is at http://www.gregroelofs.com

Files: *
Copyright:
 1995-2005 Greg Roelofs <newt@pobox.com>, <roelofs@users.sf.net>
 1995-2005 Alexander Lehmann <lehmann@usa.net>
 1995-2005 Andreas Dilger <adilger@enel.ucalgary.ca>
 1995-2005 Glenn Randers-Pehrson <randeg@alum.rpi.edu>
 1995-2005 John Bowler <jbowler@acm.org>
 1995-2005 Tom Lane <tgl@sss.pgh.pa.us>
License: Custom-MIT-like
 [File: pngcheck.c]
 .
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted, provided
 that the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation. This software is provided "as is" without express or
 implied warranty.

Files: gpl/png-fix-IDAT-windowsize.c gpl/pngsplit.c
Copyright:
 2005-2006 Greg Roelofs <newt@pobox.com>, <roelofs@users.sf.net>
License: GPL-2+

Files: debian/*
Copyright:
 2009-2013 Jari Aalto <jari.aalto@cante.net>
 2008      Marco Rodrigues <gothicx@sapo.pt>
 2006 	   Lucas Wall <lwall@debian.org>
 2004 	   Kevin M. Rosenberg <kmr@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

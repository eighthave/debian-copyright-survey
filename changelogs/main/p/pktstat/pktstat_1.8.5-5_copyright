Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: pktstat
Upstream-Contact: David Leonard <d@adaptive-enterprises.com>, <leonard@users.sourceforge.net>
Source: https://github.com/dleonard0/pktstat

Files: *
Copyright:
 2002-2006 David Leonard <d@adaptive-enterprises.com>
License: public-domain
 [from file: COPYING]
 .
 PUBLIC DOMAIN
 .
 The pktstat package was written by David Leonard, and contains
 contributions and fixes by various people. The principal author has
 placed the entire work (source) in the public domain, thus choosing to not
 exercise copyright control over it. No prohibition is made regarding
 its use or inclusion in any reasonable situation, subject to the
 notice below.
 .
 Contributed patches and fixes are accepted on the understanding
 that they too will be made available to the public domain.

Files: debian/*
Copyright:
 2010-2016 Jari Aalto <jari.aalto@cante.net>
 2004-2008 Thomas Seyrat <tomasera@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in "/usr/share/common-licenses/GPL-2".

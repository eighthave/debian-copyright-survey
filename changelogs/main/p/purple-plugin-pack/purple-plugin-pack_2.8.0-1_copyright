Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Purple Plugin Pack
Upstream-Contact: Gary Kramlich <grim@reaperworld.com>
Source: https://keep.imfreedom.org/pidgin/purple-plugin-pack

Files: *
Copyright: Gary Kramlich <grim@reaperworld.com>
           Stu Tomlinson <stu@nosnilmot.com>
           Peter Lawler <bleeter@users.sf.net>
           John Bailey <rekkanoryo@users.sourceforge.net>
           Kevin Stange <kstange@users.sourceforge.net>
           Lennert Van Alboom <alverino@users.sourceforge.net>
           Daniel Atallah <datallah@users.sourceforge.net>
           Sadrul H Chowdhury <sadrul@users.sourceforge.net>
           Richard Laager <rlaager@guifications.org>
           Martijn van Oosterhout <kleptog@svana.org>
           Matt Perry <guy@fscked.org>
           Andrew Pangborn <gaim@andrewpangborn.com>
           Casey Ho <me@caseyho.com>
           Paul Aurich <paul@darkrain42.org>
License: GPL-2+

Files: debian/*
Copyright: 2006-2008, Benjamin Seidenberg <benjamin@debian.org>
           2009, Michael Budde <mbudde@gmail.com>
           2009-2012, Felix Geyer <fgeyer@debian.org>
License: GPL-2+

Files:
 album/*.c
 album/*.h
Copyright: 2005-2008, Sadrul Habib Chowdhury <imadil@gmail.com>
           Richard Laager <rlaager@pidgin.im>
License: GPL-2+

Files: autoreply/*.c
Copyright: 2005-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: blistops/*.c
Copyright: 2004-2008, Gary Kramlich <grim@reaperworld.com>
           2007-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: capsnot/*.c
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-2+

Files: colorize/*.c
Copyright: 2005, Ike Gingerich <ike_@users.sourceforge.net>
License: GPL-2+

Files: convbadger/*.c
Copyright: 2007-2008, Gary Kramlich <grim@reaperworld.com>
License: GPL-2+

Files: dewysiwygification/*.c
Copyright: 2004-2008, Tim Ringenbach <omarvo@hotmail.com>
License: GPL-2+

Files: dice/*.c
Copyright: 2005-2008, Gary Kramlich <grim@reaperworld.com>
           2007, Lucas <reilithion@gmail.com>
License: GPL-2+

Files: difftopic/*.c
Copyright: 2006-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: enhancedhist/*.c
Copyright: 2004-2008, Andrew Pangborn <gaim@andrewpangborn.com>
           2007-2008, John Bailey <rekkanoryo@rekkanoryo.org>
           2007, Ankit Singla <ankits@umich.edu>
License: GPL-2+

Files: flip/*.c
Copyright: 2005, Gary Kramlich <grim@reaperworld.com>
License: GPL-2+

Files: google/*.c
Copyright: 2008, Gary Kramlich <grim@reaperworld.com>
License: GPL-2+

Files: gRIM/*.c
Copyright: 2005-2008, Peter Lawler <bleeter@users.sf.net>
           2006-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
           2006-2008, John Bailey <rekkanoryo@rekkanoryo.org>
           2007, Ankit Singla <ankits@umich.edu>
License: GPL-2+

Files: groupmsg/*.c
Copyright: 2004-2008, Stu Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files: highlight/*.c
Copyright: 2007-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: icon-override/*.c
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-2+

Files: ignore/*.c
Copyright: 2007-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: irchelper/*.c
Copyright: 2005-2008, Richard Laager <rlaager@pidgin.im>
           2004-2005, Mathias Hasselmann <mathias@taschenorakel.de>
           2005, Daniel Beardsmore <uilleann@users.sf.net>
           2005, Björn Nilsson <BNI on irc.freenode.net>
           2005, Anthony Sofocleous <itchysoft_ant@users.sf.net>
License: GPL-2+

Files: irc-more/*.c
Copyright: 2007-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
           2007-2008, John Bailey <rekkanoryo@rekkanoryo.org>
License: GPL-2+

Files:
 irssi/*.c
 irssi/*.h
Copyright: 2005-2008, Gary Kramlich <grim@reaperworld.com>
           2006-2008, John Bailey <rekkanoryo@rekkanoryo.org>
           2006-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: lastseen/*.c
Copyright: 2004-2008, Stu Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files: listlog/*.c
Copyright: 2008, John Bailey <rekkanoryo@rekkanoryo.org>
License: GPL-2+

Files: mystatusbox/*.c
Copyright: 2005-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: nicksaid/*.c
Copyright: 2006-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files:
 ning/*.c
 ning/*.h
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-3+

Files:
 okcupid/*.c
 okcupid/*.h
 okcupid/*/okcupid.png
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-3+

Files: oldlogger/*.c
Copyright: 2004-2008, Stu Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files:
 omegle/*.c
 omegle/*.h
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-3+

Files: plonkers/*.c
Copyright: 2005-2008, Peter Lawler <bleeter@users.sf.net>
License: GPL-2+

Files:
 schedule/*.c
 schedule/*.h
Copyright: 2006-2008, Sadrul H Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: sepandtab/*.c
Copyright: 2004-2008, Gary Kramlich <grim@reaperworld.com>
License: GPL-2+

Files: showoffline/*.c
Copyright: 2004-2008, Stu Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files: simfix/*.c
Copyright: 2005-2008, Stu, Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files: slashexec/*.c
Copyright: 2004-2008, Gary Kramlich <grim@reaperworld.com>
           2005-2008, Peter Lawler <bleeter@users.sf.net>
           2005-2008, Daniel Atallah <datallah@users.sourceforge.net>
           2005-2008, John Bailey <rekkanoryo@rekkanoryo.org>
           2006-2008, Sadrul Habib Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files: translate/*.c
Copyright: 2010, Eion Robb <eionrobb@gmail.com>
License: GPL-2+

Files: snpp/*.c
Copyright: 2004-2008, Don Seiler <don@seiler.us>
License: GPL-2+

Files: splitter/*.c
Copyright: 2005-2007, Ike Gingerich <ike_@users.sourceforge.net>
License: GPL-2+

Files: sslinfo/*.c
Copyright: 2004-2008, Gary Kramlich <grim@reaperworld.com>
License: GPL-2+

Files: switchspell/*.c
Copyright: 2007-2008, Alfredo Raul Pena <arpena@users.sourceforge.net>
           2007-2008, Sadrul H Chowdhury <sadrul@users.sourceforge.net>
License: GPL-2+

Files:
 timelog/*.c
 timelog/*.h
Copyright: 2006, Jon Oberheide <jon@oberheide.org>
           2007-2008, Stu Tomlinson <stu@nosnilmot.com>
License: GPL-2+

Files: xmppprio/*.c
Copyright: 2009, Paul Aurich <paul@darkrain42.org>
License: GPL-2+

License: GPL-2+
 On Debian systems the full text of the GNU General Public 
 License can be found in the `/usr/share/common-licenses/GPL-2'
 file.

License: GPL-3+
 On Debian systems the full text of the GNU General Public 
 License can be found in the `/usr/share/common-licenses/GPL-3'
 file.

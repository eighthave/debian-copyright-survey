Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Chemtool
Upstream-Contact: Martin Kroeker <martin@ruby.chemie.uni-freiburg.de>
Source: http://ruby.chemie.uni-freiburg.de/~martin/chemtool/

Files: *
Copyright: 1999 Thomas Volk <formerly Thomas.Volk@student.uni-ulm.de>
           1999-2007,2010,2011,2013 Dr. Martin Kroeker <martin@ruby.chemie.uni-freiburg.de>
           2001 Radek Liboska (partial)
License: GPL-2+

Files: gettext.h intl/*
Copyright: 1995-2004 Free Software Foundation, Inc.
License: LGPL-2+

Files: gtkmenu.c gtkmenu.h gtkfilesel.c gtkfilesel.h
Copyright: 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
License: LGPL-2+

Files: undo.c undo.h
Copyright: 1999 Matt Kimball
License: GPL-2+

Files: debian/*
Copyright: 2001 Guenter Bechly <gbechly@debian.org>
           2001-2006 Michael Banck <mbanck@debian.org>
           2007-2017 The debichem team <debichem-devel@lists.alioth.debian.org>
License: GPL-2+

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
Comment: On Debian systems, the GNU Lesser General Public License (LGPL)
 version 2 is available as the file `/usr/share/common-licenses/LGPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian systems, the GNU General Public License (GPL) version 2
 is available as the file `/usr/share/common-licenses/GPL-2'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: curtain
Source: http://code.google.com/p/ardesia/

Files: *
Copyright: 2010 Pietro Pilolli <pilolli.pietro@gmail.com>
License: GPL-3+

Files: install-sh
Copyright: 1994 X Consortium
License: MIT

Files: ltmain.sh
Copyright: 1996-2001, 2003-2011 Free Software Foundation, Inc.
License: GPL-2+

Files: missing
Copyright: 1996, 1997, 1999, 2000, 2002-2006, 2008-2012 Free Software Foundation, Inc.
License: GPL-2+

Files: depcomp
Copyright: 1999, 2000, 2003-2007, 2009, 2010 Free Software Foundation, Inc.
License: GPL-2+

Files: po/Makefile.in.in
Copyright: 1995-1997 by Ulrich Drepper <drepper@gnu.ai.mit.edu>
           2004-2008 Rodney Dawes <dobey.pwns@gmail.com>
License: custom
 This file can be copied and used freely without restrictions.  It can
 be used in projects which are not available under the GNU General Public
 License but which still want to provide support for the GNU gettext
 functionality.

Files: debian/*
Copyright: 2011 Karolina Kalic <karolina@resenje.org>
           2013 Andrea Colangelo <warp10@debian.org>
License: GPL-3+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: MIT
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

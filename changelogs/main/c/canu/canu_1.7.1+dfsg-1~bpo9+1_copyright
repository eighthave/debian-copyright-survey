Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: canu
Source: https://github.com/marbl/canu
Files-Excluded:
	kmer
	src/mhap/mhap-*.jar
	src/utgcns/libboost

Files: *
Copyright:
	2011-2012,2014-2015 Battelle National Biodefense Institute
	2011 Liliana Florea
	2005-2014 J. Craig Venter Institute
	2004 Brian P. Walenz
	2003-2004 Applera Corporation
License: GPL-2.0+ and BSD-3-Clause-BNBI and Public-Domain
Comment:
 Modifications made by the Battelle National Biodefense Institute are licensed
 under the BSD-3-Clause-BNBI license. Developments from 2016 onward are
 public domain works.

Files: src/utgcns/*
       src/falcon_sense/*
Copyright: 2011-2015 Pacific Biosciences of California
License: BSD-3-Clause-PacBio
 Redistribution and use in source and binary forms, with or without
 modification, are permitted (subject to the limitations in the
 disclaimer below) provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.
 * Neither the name of Pacific Biosciences nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY PACIFIC
 BIOSCIENCES AND ITS CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL PACIFIC BIOSCIENCES OR ITS
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: src/AS_UTL/md5.C
Copyright: 1991-1992 RSA Data Security, Inc.
License: RSA
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: src/AS_UTL/mt19937ar.*
Copyright: 1997 - 2002 Makoto Matsumoto and Takuji Nishimura
License: BSD-3-Clause-BNBI

Files: debian/*
Copyright: 2016-2017 Afif Elghraoui <afif@debian.org>
License: GPL-2.0+

License: Public-Domain
 This software is "United States Government Work" under the terms of the United
 States Copyright Act. It was written as part of the authors' official duties
 for the United States Government and thus cannot be copyrighted. This software
 is freely available to the public for use without a copyright
 notice. Restrictions cannot be placed on its present or future use.
 .
 Although all reasonable efforts have been taken to ensure the accuracy and
 reliability of the software and associated data, the National Human Genome
 Research Institute (NHGRI), National Institutes of Health (NIH) and the
 U.S. Government do not and cannot warrant the performance or results that may
 be obtained by using this software or data. NHGRI, NIH and the U.S. Government
 disclaim all warranties as to performance, merchantability or fitness for any
 particular purpose.
 .
 Please cite the authors in any work or product based on this material.

License: BSD-3-Clause-BNBI
 This Software was prepared for the Department of Homeland Security (DHS) by the
 Battelle National Biodefense Institute, LLC (BNBI) as part of contract
 HSHQDC-07-C-00020 to manage and operate the National Biodefense Analysis and
 Countermeasures Center (NBACC), a Federally Funded Research and Development
 Center.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 * Neither the name of the Battelle National Biodefense Institute nor the names
   of its contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".


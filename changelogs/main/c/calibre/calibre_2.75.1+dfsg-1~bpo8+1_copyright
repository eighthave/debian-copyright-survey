Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: calibre
Upstream-Contact: Kovid Goyal <kovid@kovidgoyal.net>
Source: http://calibre-ebook.com/downloads

Files: *
Copyright: Copyright (C) 2008 Kovid Goyal <kovid@kovidgoyal.net>
License: GPL-3

Files: src/calibre/ebooks/pdf/utils.h
Copyright: Copyright 2009 Kovid Goyal <kovid@kovidgoyal.net>
License: GPL-2+

Files: src/calibre/ebooks/BeautifulSoup.py
Copyright: Copyright (c) 2004-2007, Leonard Richardson
License: BSD-3-Clause

Files: src/calibre/ebooks/chardet/*
Copyright: Copyright (C) 1998-2001 Netscape Communications Corporation
License: LGPL-2.1+

Files: src/calibre/ebooks/hyphenate.py
Copyright: Copyright (C) 1990, 2004, 2005 Gerard D.C. Kuiken.
License: hyphenate-license
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.

Files: /src/cherrypy/*
Copyright: Copyright (c) 2004-2007, CherryPy Team (team@cherrypy.org)
           Copyright (C) 2005, Tiago Cogumbreiro <cogumbreiro@users.sf.net>
License: BSD-3-Clause

Files: src/odf/*
Copyright: Copyright (C) 2006-2008 Søren Roug, European Environment Agency
License: LGPL-2.1+

Files: src/odf/teletype.py
       src/odf/easyliststyle.py
Copyright: Copyright (C) 2008, J. David Eisenberg
License: GPL-2+

Files: src/pyPdf/*
Copyright: Copyright (c) 2006, Mathieu Fenniak
           Copyright (c) 2007, Ashish Kulkarni <kulkarni.ashish@gmail.com>
License: BSD-3-Clause

Files: src/calibre/utils/lzx/*
Copyright: Copyright (C) 2002, Matthew T. Russotto
           Copyright (C) 2008, Marshall T. Vandegrift <llasram@gmail.com>
           Copyright (C) 2006-2008, Alexander Chemeris
License: LGPL-2.1

Files: src/calibre/utils/lzx/msstdint.h
Copyright: Copyright (C) 2006-2008, Alexander Chemeris
License: BSD-3-Clause

Files: src/calibre/utils/pyparsing.py
Copyright: Copyright (c) 2003-2008, Paul T. McGuire
License: MIT

Files: src/calibre/utils/PythonMagickWand.py
Copyright: (c) 2007 - Achim Domma - domma@procoders.net
License: MIT

Files: src/calibre/utils/msdes/d3des.h
       src/calibre/utils/msdes/des.c
Copyright: Copyright (C) 1988,1989,1990,1991,1992, Richard Outerbridge
License: public-domain
 THIS SOFTWARE PLACED IN THE PUBLIC DOMAIN BY THE AUTHOUR

Files: src/calibre/utils/msdes/msdesmodule.c
Copyright: Copyright (C) 2008, Marshall T. Vandegrift <llasram@gmail.com>
License: GPL-3

Files: src/calibre/utils/msdes/spr.h
Copyright: Copyright (C) 2002, Dan A. Jackson
License: GPL-2+

Files: src/calibre/gui2/pictureflow/*
Copyright: (C) Copyright 2007 Trolltech ASA
License: BSD-3-Clause

Files: src/calibre/ebooks/lit/*
Copyright: 2008, Marshall T. Vandegrift <llasram@gmail.com>
License: GPL-3

Files: src/calibre/ebooks/lrf/*
Copyright: 2008, Anatoly Shipitsin <norguhtar at gmail.com>
           copyright 2002 Paul Henry Tremblay
           Copyright (C) 2008 B.Scott Wxby [bswxby]
           Copyright (C) 2007 David Chen SonyReader<at>DaveChen<dot>org
           Copyright (c) 2007 Mike Higgins (Falstaff)
License: GPL-3

Files: src/calibre/ebooks/rtf2xml/*
Copyright: copyright 2002 Paul Henry Tremblay
License: GPL-2

Files: src/calibre/web/feeds/feedparser.py
Copyright: Copyright (c) 2002-2006, Mark Pilgrim
License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS'
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.


Files: src/calibre/web/feeds/recipes/*
Copyright: 2008, Darko Miletic <darko.miletic at gmail.com>
           2008, Mathieu Godlewski <mathieu at godlewski.fr>
           Copyright (C) 2008 B.Scott Wxby [bswxby]
           Copyright (C) 2007 David Chen SonyReader<at>DaveChen<dot>org
           2008, Derry FitzGerald
License: GPL-3

Files: src/calibre/ebooks/metadata/*
Copyright: 2008, Ashish Kulkarni <kulkarni.ashish@gmail.com>
           Copyright (C) 2006 Søren Roug, European Environment Agency
License: GPL-3

Files: src/calibre/translations/*
Copyright: Copyright (C) 2007, Kovid Goyal
           Copyright (C) 2008, Rosetta Contributors and Canonical Ltd.
License: GPL-3

Files: src/calibre/gui2/viewer/jquery.js
       src/calibre/gui2/viewer/jquery_scrollTo.js
       src/calibre/library/static/date.js
Copyright: Copyright (C) 2008, John Resig (jquery.com)
           Copyright (C) 2007-2008, Ariel Flesler - aflesler@gmail.com | http://flesler.blogspot.com
           Copyright (C) 2006-2007, Coolite Inc. (http://www.coolite.com/)
License: MIT

Files: src/calibre/ebooks/lrf/fonts/liberation/*
Copyright: Copyright (C) 2007, Red Hat, Inc. All rights reserved.
License: GPL-2.0-with-font-exception-and-additional-exception
 Copyright (C) 2007, Red Hat, Inc. All rights reserved.
 LIBERATION is a trademark of Red Hat, Inc.
 .
 This agreement governs the use of the Software and any updates to the Software,
 regardless of the delivery mechanism. Subject to the following terms, Red Hat, Inc.
 ("Red Hat") grants to the user ("Client") a license to this work pursuant to
 the GNU General Public License v.2 with the exceptions set forth below and such
 other terms as our set forth in this End User License Agreement.
 .
 1. The Software and License Exception. LIBERATION font software  (the "Software")
 consists of TrueType-OpenType formatted font software for rendering LIBERATION
 typefaces in sans serif, serif, and monospaced character styles. You are licensed
 to  use, modify, copy, and distribute the Software pursuant to the GNU General
 Public License v.2 with the following exceptions:
 .
 (a) As a special exception, if you create a document which uses this font, and
  embed this font or unaltered portions of this font into the document, this
  font does not by itself cause the resulting document to be covered by the
  GNU General Public License.  This exception does not however invalidate any
  other reasons why the document might be covered by the GNU General Public
  License.  If you modify this font, you may extend this exception to your
  version of the font, but you are not obligated to do so. If you do not
  wish to do so, delete this exception statement from your version.
 .
 (b) As a further exception, any distribution of the object code of the Software
  in a physical product must provide you the right to access and modify the
  source code for the Software and to reinstall that modified version of the
  Software in object code form on the same physical product on which you
  received it.
 .
 2. Intellectual Property Rights. The Software and each of its components, including
    the source code, documentation, appearance, structure and organization are owned
    by Red Hat and others and are protected under copyright and other laws. Title to
    the Software and any component, or to any copy, modification, or merged portion
    shall remain with the aforementioned, subject to the applicable license.
    The "LIBERATION" trademark is a trademark of Red Hat, Inc. in the U.S. and other
    countries. This agreement does not permit Client to distribute modified versions
    of the Software using Red Hat's trademarks. If Client makes a redistribution of
    a modified version of the Software, then Client must modify the files names to
    remove any reference to the Red Hat trademarks and must not use the Red Hat
    trademarks in any way to reference or promote the modified Software.
 .
 3. Limited Warranty. To the maximum extent permitted under applicable law, the
    Software is provided and licensed "as is" without warranty of any kind,
    expressed or implied, including the implied warranties of merchantability,
    non-infringement or fitness for a particular purpose. Red Hat does not warrant
    that the functions contained in the Software will meet Client's requirements or
    that the operation of the Software will be entirely error free or appear precisely
    as described in the accompanying documentation.
 .
 4. Limitation of Remedies and Liability.  To the maximum extent permitted by applicable
    law, Red Hat or any Red Hat authorized dealer will not be liable to Client for any
    incidental or consequential damages, including lost profits or lost savings arising
    out of the use or inability to use the Software, even if Red Hat or such dealer has
    been advised of the possibility of such damages.
 .
 5. General. If any provision of this agreement is held to be unenforceable, that shall
    not affect the enforceability of the remaining provisions. This agreement shall be
    governed by the laws of the State of North Carolina and of the United States, without
    regard to any conflict of laws provisions, except that the United Nations Convention
    on the International Sale of Goods shall not apply.


Files: installer/cx_Freeze/*
Copyright: Copyright © 2007-2008, Colt Engineering, Edmonton, Alberta, Canada.
           Copyright © 2001-2006, Computronix (Canada) Ltd., Edmonton, Alberta, Canada.
License: Python-based
 All rights reserved.
 .
 NOTE: this license is derived from the Python Software Foundation License
 which can be found at http://www.python.org/psf/license
 .
 License for cx_Freeze 4.0.1
 ---------------------------
 .
 1. This LICENSE AGREEMENT is between the copyright holders and the Individual
    or Organization ("Licensee") accessing and otherwise using cx_Freeze
    software in source or binary form and its associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, the
    copyright holders hereby grant Licensee a nonexclusive, royalty-free,
    world-wide license to reproduce, analyze, test, perform and/or display
    publicly, prepare derivative works, distribute, and otherwise use cx_Freeze
    alone or in any derivative version, provided, however, that this License
    Agreement and this notice of copyright are retained in cx_Freeze alone or in
    any derivative version prepared by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on or
    incorporates cx_Freeze or any part thereof, and wants to make the derivative
    work available to others as provided herein, then Licensee hereby agrees to
    include in any such work a brief summary of the changes made to cx_Freeze.
 .
 4. The copyright holders are making cx_Freeze available to Licensee on an
    "AS IS" basis.  THE COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR WARRANTIES,
    EXPRESS OR IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, THE COPYRIGHT
    HOLDERS MAKE NO AND DISCLAIM ANY REPRESENTATION OR WARRANTY OF
    MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF
    CX_FREEZE WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. THE COPYRIGHT HOLDERS SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF
    CX_FREEZE FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
    A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING CX_FREEZE, OR ANY
    DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material breach
    of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any relationship
    of agency, partnership, or joint venture between the copyright holders and
    Licensee.  This License Agreement does not grant permission to use
    copyright holder's trademarks or trade name in a trademark sense to endorse
    or promote products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using cx_Freeze, Licensee agrees to be
    bound by the terms and conditions of this License Agreement.
 .
 Computronix® is a registered trademark of Computronix (Canada) Ltd.



License: GPL-2
 The full text of the GPL is distributed as in
 /usr/share/common-licenses/GPL-2 on Debian systems.

License: GPL-2+
 The full text of the GPL is distributed as in
 /usr/share/common-licenses/GPL-2 on Debian systems.

License: GPL-3
 The full text of the GPL is distributed as in
 /usr/share/common-licenses/GPL-3 on Debian systems.

License: LGPL-2.1
 The full text of the LGPL is distributed as in
 /usr/share/common-licenses/LGPL-2.1 on Debian systems.

License: LGPL-2.1+
 The full text of the LGPL is distributed as in
 /usr/share/common-licenses/LGPL-2.1 on Debian systems.

License: BSD-3-Clause
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
     * Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.
     * Neither the name of the CherryPy Team nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

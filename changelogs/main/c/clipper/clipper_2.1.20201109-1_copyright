Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: clipper
Upstream-Contact: Kevin Cowtan <kevin.cowtan@york.ac.uk>
Source: ftp://ftp.ccp4.ac.uk/opensource/

Files: *
Copyright: 2000-2006, Kevin Cowtan <kevin.cowtan@york.ac.uk>
License: LGPL-2.1

Files: debian/*
Copyright: (C) 2013, Picca Frédéric-Emmanuel <picca@debian.org>
License: LGPL-2.1

License: LGPL-2.1
 This library is free software and is distributed under the terms
 and conditions of version 2.1 of the GNU Lesser General Public
 Licence (LGPL) with the following additional clause:
 .
    `You may also combine or link a "work that uses the Library" to
    produce a work containing portions of the Library, and distribute
    that work under terms of your choice, provided that you give
    prominent notice with each copy of the work that the specified
    version of the Library is used in it, and that you include or
    provide public access to the complete corresponding
    machine-readable source code for the Library including whatever
    changes were used in the work. (i.e. If you make changes to the
    Library you must distribute those, but you do not need to
    distribute source or object code to those portions of the work
    not covered by this licence.)'
 .
 Note that this clause grants an additional right and does not impose
 any additional restriction, and so does not affect compatibility
 with the GNU General Public Licence (GPL). If you wish to negotiate
 other terms, please contact the maintainer.
 .
 You can redistribute it and/or modify the library under the terms of
 the GNU Lesser General Public License as published by the Free Software
 Foundation; either version 2.1 of the License, or (at your option) any
 later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the CNS licence and/or GNU
 Lesser General Public License along with this library; if not, write
 to the CNS Secretary, Daresbury Laboratory, Warrington WA4 4AD, UK.
 The GNU Lesser General Public can also be obtained by writing to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301 USA
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

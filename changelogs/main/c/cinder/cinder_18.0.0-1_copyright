Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cinder
Source: https://github.com/openstack/cinder

Files: *
Copyright: (c) 2010-2011, US Gov. as represented by the Administrator of NASA
           (c) 2010-2016, OpenStack Foundation
           (c) 2012-2013, AT&T Labs, Yun Mao <yunmao@gmail.com>
           (c) 2015-2016, Hitachi, Erlon Cruz <erlon.cruz@fit-tecnologia.org.br>
           (c) 2012-2016, Hewlett-Packard Development Company, L.P.
           (c) 2010-2011, Citrix Systems, Inc.
           (c) 2012-2016, EMC Corporation.
           (c) 2011-2016, Intel Corporation
           (c) 2011-2012, Justin Santa Barbara
           (c) 2012-2017, IBM Corporation
           (c) 2013-2017, Dell Inc.
           (c) 2012-2017, Red Hat, Inc.
           (c) 2016-2017, Mirantis Inc.
           (c) 2011-2016, Zadara Storage Inc.
           (c) 2011-2014, eBay Inc.
           (c) 2013, Nebula, Inc.
           (c) 2001-2010, Twisted Matrix Laboratories.
           (c) 2015, The Kubernetes Authors.
           (c) 2015, SimpliVity Corp.
           (c) 2013-2015, Yahoo! Inc.
           (c) 2016, Synology Inc.
           (c) 2014-2016, Clinton Knight
           (c) 2015-2016, Michael Price
           (c) 2015, Yogesh Kshirsagar
           (c) 2014-2016, Alex Meade
           (c) 2014-2015, Navneet Singh
           (c) 2015, Rushil Chugh
           (c) 2014, Adrien Vergé <adrien.verge@numergy.com>
           (c) 2015, Tom Barron
           (c) 2015, Chelsio Communications Inc.
           (c) 2014, Andrew Kerr
           (c) 2016, Mike Rooney
           (c) 2014, Bob Callaway
           (c) 2015, Dustin Schoenbrun
           (c) 2014, Ben Swartzlander
           (c) 2016, Chuck Fouts
           (c) 2014-2015, Pure Storage, Inc.
           (c) 2015, Goutham Pacha Ravi
           (c) 2016, NEC Corporation
           (c) 2017, DataCore Software Corp
           (c) 2013, Zelin.io
           (c) 2016, QNAP Systems, Inc.
           (c) 2011, University of Southern California
           (c) 2015, FUJITSU LIMITED
           (c) 2013-2017, VMware, Inc.
           (c) 2017, Inspur Corp.
           (c) 2015-2016, Industrial Technology Research Institute
           (c) 2015, Nippon Telegraph and Telephone Corporation
           (c) 2016, Infinidat Ltd.
           (c) 2013-2017, Huawei Technologies Co., Ltd.
           (c) 2017, Veritas Technologies LLC
           (c) 2017, Datera
           (c) 2014-2015, Cloudbase Solutions SRL
           (c) 2015, HGST Inc
           (c) 2013, Canonical Ltd.
           (c) 2014, 2016, Oracle
           (c) 2015, Tintri
           (c) 2014-2017, StorPool
           (c) 2016, Seagate Technology or one of its affiliates
           (c) 2015, Odin
           (c) 2012-2013, Josh Durgin
           (c) 2012, NetApp, Inc.
           (c) 2014, ProphetStor, Inc.
           (c) 2013-2016, Nexenta Systems, Inc.
           (c) 2014, Objectif Libre
           (c) 2014, Cisco Systems Inc.
           (c) 2016 by Kaminario Technologies, Ltd.
           (c) 2015, DotHill Systems
           (c) 2012, Pedro Navarro Perez
           (c) 2014-2016, Brocade Communications Systems Inc.
           (c) 2016, Google Inc.
           (c) 2012, Rackspace Hosting
           (c) 2014, LINBIT HA Solutions GmbH
           (c) 2016, Vedams Inc.
           (c) 2013-2017, NTT corp.
           (c) 2013-2014, Deutsche Telekom AG
           (c) 2013, eNovance, Inc.
           (c) 2016, Stratoscale, Ltd.
           (c) 2011, Denali Systems, Inc.
           (c) 2013, The Johns Hopkins University
           (c) 2014, TrilioData, Inc
           (c) 2014, Quobyte Inc.
           (c) 2015-2017, Jose Porrua
           (c) 2014, Glenn Gobeli
           (c) 2014, Jeff Applewhite
           (c) 2015, Dot Hill Systems Corp.
           (c) 2015, Kevin Fox <kevin@efox.cc>
           (c) 2015, Parallels IP Holdings GmbH
           (c) 2011, Ken Pepple
           (c) 2013, SolidFire Inc
           (c) 2011, Piston Cloud Computing, Inc.
           (c) 2005, the Lawrence Journal-World
           (c) 2017, FiberHome Telecommunication Technologies CO.,LTD
License: Apache-2

Files: debian/*
Copyright: (c) 2016-2019, Ondřej Nový <onovy@debian.org>
           (c) 2012-2018, Thomas Goirand <zigo@debian.org>
           (c) 2017-2019, Michal Arbet <michal.arbet@ultimum.io>
           (c) 2017, David Rabel <david.rabel@noresoft.com>
           (c) 2012, Chuck Short <zulcss@ubuntu.com>
           (c) 2016, Ondřej Kobližek <koblizeko@gmail.com>
License: Apache-2

License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Calligra Plain
Source: http://www.calligra-suite.org/get-calligra/

Files: *
Copyright: 1997 Tim D. Gilman <tdgilman@best.org>
           1997-2000 Torben Weis <weis@kde.org>
           1998 Matthias Ettrich <ettrich@kde.org>
           1998-2001 Mirko Boehm <mirko@kde.org>
           1998-2001 Reginald Stadlbauer <reggie@kde.org>
           1998 Thomas Tanghus <tanghus@earthling.net>
           1999-2007 David Faure <faure@kde.org>
           1999-2000 Espen Sand <espen@kde.org>
           1999-2003 Hans Petter Bieker <bieker@kde.org>
           1999-2000 Holger Freyther <freyther@kde.org>
           1999 Waldo Bastian <bastian@kde.org>
           2000 Simon Hausmann <hausmann@kde.org>
           2000-2002 Werner Trobin <trobin@kde.org>
           2001-2007 OpenMFG, LLC <info@openmfg.com>
           2001, 2006-2010 Thomas Zander <zander@kde.org>
           2002 Bo Thorsen <bo@sonofthor.dk>
           2002 Rob Buis <buis@kde.org>
           2003 Clarence Dang <dang@kde.org>
           2003 Lukas Tinkl <lukas@kde.org>
           2003-2004 Nicolas Goutte <goutte@kde.org>
           2004, 2009 Cyrille Berger <cberger@cberger.net>
           2004-2012, 2016-2017 Dag Andersen <danders@get2net.dk>
           2004 Laurent Montel <montel@kde.org>
           2005-2006 Ariya Hidayat <ariya@kde.org>
           2005-2009 Olivier Goffart <ogoffart@kde.org>
           2005-2006 Sven Lüppken <sven@kde.org>
           2006-2007 Alexis Ménard <darktears31@gmail.com>
           2006-2007, 2009-2012, 2014-2015 Boudewijn Rempt <boud@valdyas.org>
           2006 Brad Hards <bradh@kde.org>
           2006-2007 Frederic BECQUIER <frederic.becquier@gmail.com>
           2006 Fredrik Edemar <f_edemar@linux.se>
           2006 Gary Cramblitt <garycramblitt@comcast.net>
           2006 Gábor Lehel <illissius@gmail.com>
           2006 Martin Pfeiffer <hubipete@gmx.net>
           2006 Peter Simonsson <peter.simonsson@gmail.com>
           2006 Raphael Langerhorst <raphael.langerhorst@kdemail.net>
           2006-2007, 2011 Sebastian Sauer <sebsauer@kdab.com>
           2006 Stefan Nikolaus <stefan.nikolaus@kdemail.net>
           2006 Thomas Schaap <thomas.schaap@kdemail.net>
           2006 Tobias Koenig <tokoe@kde.org>
           2007-2010 Adam Pigg <adam@piggz.co.uk>
           2007 Florian Piquemal <flotueur@yahoo.fr>
           2007 Fredy Yanardi <fyanardi@gmail.com>
           2007 Jan Hambrecht <jaham@gmx.net>
           2007 John Layt <john@layt.net>
           2007 Marijn Kruisselbrink <mkruisselbrink@kde.org>
           2007 Matthias Kretz <kretz@kde.org>
           2007-2008, 2010-2011 Thorsten Zachmann <zachmann@kde.org>
           2008-2012 C. Boemann <cbo@kogmbh.com>
           2008 Girish Ramakrishnan <girish@forwardbias.in>
           2008, 2011 Pierre Stirnweiss <pstirnweiss@googlemail.com>
           2009-2012 Inge Wallin <ingwa@kogmbh.com>
           2009-2010, 2013, 2017 Jos van den Oever <jos@vandenoever.info>
           2009 Nokia Corporation and/or its subsidiary(-ies).
           2010 Benjamin Port <port.benjamin@gmail.com>
           2010 Carlos Licea <carlos@kdab.com>
           2010 Jarosław Staniek <staniek@kde.org>
           2010 KO GmbH <jos.van.den.oever@kogmbh.com>
           2011 José Luis Vergara <pentalis@gmail.com>
           2011 Lukáš Tvrdý <lukas.tvrdy@ixonos.com>
           2011 Mojtaba Shahi Senobari <mojtaba.shahi3000@gmail.com>
           2011 Pierre Ducroquet <pinaraf@pinaraf.info>
           2011 Smit Patel <smitpatel24@gmail.com>
           2011 Srikanth Tiyyagura <srikanth.tulasiram@gmail.com>
           2011 Sven Langkamp <sven.langkamp@gmail.com>
           2012, 2015-2016 Friedrich W. H. Kossebau <kossebau@kde.org>
           2013-2014 Yue Liu <yue.liu@mail.com>
           2014 Alexander Potashev <aspotashev@gmail.com>
           2014-2015 Dmitry Kazakov <dimula73@gmail.com>
License: GPL-2+__and__LGPL-2+__and__LGPL-2.1__and__LGPL-2.1+
 These files are licensed under a variety of GNU licenses. For details
 check file's headers and corresponding license paragraphs:
  * GPL-2+
  * LGPL-2+
  * LGPL-2.1
  * LGPL-2.1+
Comment:
 Over 900 Calligra source files are licensed with one of those licenses.
 Files are grouped in one paragraph because they are mixed within the same
 folders and share copyrights of the same authors. Separate paragraphs
 would be too long to maintain.

Files: cmake/*
Copyright: 2006-2010 Alexander Neundorf <neundorf@kde.org>
License: BSD-3-Clause

Files: devtools/release/plan_create_tarball.rb
Copyright: 2005 Mark Kretschmann <markey@web.de>
           2006-2008 Tom Albers <tomalbers@kde.nl>
           2007 Angelo Naselli <anaselli@linux.it>
           2016-2017 Dag Andersen <danders@get2net.dk>
License: GPL-2

Files: devtools/scripts/OpenDocument-manifest-schema-v1.0-os.rng
       devtools/scripts/OpenDocument-manifest-schema-v1.1.rng
       devtools/scripts/OpenDocument-schema-v1.0-os.rng
       devtools/scripts/OpenDocument-schema-v1.1.rng
       devtools/scripts/OpenDocument-strict-schema-v1.0-os.rng
       devtools/scripts/OpenDocument-strict-schema-v1.1.rng
       devtools/scripts/OpenDocument-v1.2-cs01-manifest-schema.rng
       devtools/scripts/OpenDocument-v1.2-cs01-schema-calligra.rng
Copyright: 1999-2007 Sun Microsystems, Inc
           2002-2011 OASIS Open
License: OASIS

Files: src/libs/kundo2/kundo2group.cpp
       src/libs/kundo2/kundo2group.h
       src/libs/kundo2/kundo2stack_p.h
Copyright: 2011 Nokia Corporation and/or its subsidiary(-ies)
License: LGPL-2.1 and exception-LGPL-Nokia-QT-1.1

Files: src/libs/kundo2/kundo2model.cpp
       src/libs/kundo2/kundo2model.h
       src/libs/kundo2/kundo2view.cpp
       src/libs/kundo2/kundo2view.h
Copyright: 2010 Matus Talcik <matus.talcik@gmail.com>
           2010 Nokia Corporation and/or its subsidiary(-ies)
License: LGPL-2+ or LGPL-2.1 and exception-LGPL-Nokia-QT-1.1

Files: src/libs/kundo2/kundo2stack.cpp
       src/libs/kundo2/kundo2stack.h
Copyright: 2014 Dmitry Kazakov <dimula73@gmail.com>
           2014 Mohit Goyal <mohit.bits2011@gmail.com>
           2011 Nokia Corporation and/or its subsidiary(-ies)
License: LGPL-2.1+ and exception-LGPL-Nokia-QT-1.1

Files: src/org.kde.calligraplan.appdata.xml
Copyright: Not-Applicable
License: CC0-1.0

Files: src/plugins/schedulers/rcps/3rdparty/LibRCPS/*
Copyright: Robert Lemmen <robertle@semistable.com>
License: GPL-2

Files: src/plugins/schedulers/tj/taskjuggler/*
Copyright: 2001-2006 Chris Schlaeger <cs@kde.org>
           2011, 2016 Dag Andersen <danders@get2net.dk>
License: GPL-2

Files: debian/*
Copyright: 2017-2018 Pino Toscano <pino@debian.org>
License: LGPL-2+

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 ․
  1. Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
 ․
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 ․
  3. Neither the name of the copyright holder nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission.
 ․
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 On Debian systems, the complete text of the Creative Commons CC0 1.0
 Universal license (CC0-1.0) can be found in
 "/usr/share/common-licenses/CC0-1.0".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 ․
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU Library General Public
 License Version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1 of the License.
 ․
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU Lesser General Public
 License Version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 ․
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU Lesser General Public
 License Version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: OASIS
 All capitalized terms in the following text have the meanings assigned to them
 in the OASIS Intellectual Property Rights Policy (the "OASIS IPR Policy"). The
 full Policy may be found at the OASIS website.
 ․
 This document and translations of it may be copied and furnished to others, and
 derivative works that comment on or otherwise explain it or assist in its
 implementation may be prepared, copied, published, and distributed, in whole or
 in part, without restriction of any kind, provided that the above copyright
 notice and this section are included on all such copies and derivative works.
 However, this document itself may not be modified in any way, including by
 removing the copyright notice or references to OASIS, except as needed for the
 purpose of developing any document or deliverable produced by an OASIS
 Technical Committee (in which case the rules applicable to copyrights, as set
 forth in the OASIS IPR Policy, must be followed) or as required to translate it
 into languages other than English.
 ․
 The limited permissions granted above are perpetual and will not be revoked by
 OASIS or its successors or assigns.
 ․
 This document and the information contained herein is provided on an "AS IS"
 basis and OASIS DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT
 INFRINGE ANY OWNERSHIP RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR
 FITNESS FOR A PARTICULAR PURPOSE.

License: exception-LGPL-Nokia-QT-1.1
 As an additional permission to the GNU Lesser General Public License
 version 2.1, the object code form of a ""work that uses the Library"" may
 incorporate material from a header file that is part of the Library. You
 may distribute such object code under terms of your choice, provided that:
 ․
 (i) the header files of the Library have not been modified; and
 ․
 (ii) the incorporated material is limited to numerical parameters, data
 structure layouts, accessors, macros, inline functions and templates; and
 ․
 (iii) you comply with the terms of Section 6 of the GNU Lesser General
 Public License version 2.1.
 ․
 Moreover, you may apply this exception to a modified version of the
 Library, provided that such modification does not involve copying material
 from the Library into the modified Library's header files unless such
 material is limited to
 ․
 (i) numerical parameters;
 (ii) data structure layouts;
 (iii) accessors; and
 (iv) small macros, templates and inline functions of five lines or less in
 length.
 ․
 Furthermore, you are not required to apply this additional permission to a
 modified version of the Library.

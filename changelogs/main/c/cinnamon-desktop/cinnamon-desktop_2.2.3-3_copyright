Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cinnamon-desktop
Source: https://github.com/linuxmint/cinnamon-desktop

Files: *
Copyright: 2010, Carlos Garcia Campos <carlosgc@gnome.org>
           2012, Colin Walters <walters@verbum.org>
           2000, Eazel, Inc.
           2010, Giovanni Campagna
           2008-2009, Novell, Inc.
           2002-2011, Red Hat, Inc.
           1998, Tom Tromey
           2012, William Jon McCann <mccann@redhat.com>
License: LGPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

Files: libcinnamon-desktop/cdesktop-enums.h
Copyright: 2010, Codethink Limited
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".

Files: libcinnamon-desktop/display-name.c
       libcinnamon-desktop/edid-parse.c
Copyright: 2007, Red Hat, Inc.
License: MIT/X11 (BSD like)
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

Files: libcinnamon-desktop/cinnamon-rr-debug.c
       libcinnamon-desktop/gnome-pnp-ids.c
       libcinnamon-desktop/gnome-pnp-ids.h
       libcinnamon-desktop/gnome-xkb-info.c
       libcinnamon-desktop/gnome-xkb-info.h
       libcinnamon-desktop/test-pnp-ids.c
Copyright: 2012, Bastien Nocera <hadess@hadess.net>
           2012, Red Hat, Inc
           2009-2012, Richard Hughes <richard@hughsie.com>
License: GPL-2+

Files: debian/*
Copyright: 2013, Clement Lefebvre <root@linuxmint.com>
           2013, Maximiliano Curia <maxy@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU General Public License can be
 found in "/usr/share/common-licenses/GPL-2".

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ChargeBee Python
Upstream-Contact:  ChargeBee <support@chargebee.com>
Source: https://github.com/chargebee/chargebee-python/

Files: *
Copyright: Copyright (c) 2011-2018 ChargeBee, Inc.
License: Expat

Files: chargebee/ssl/ca-certs.crt
Copyright: Copyright (C) 1994-2000 Netscape Communications Corporation.
License: MPL or GPL-2.0+ or LGPL-2.1+

Files: debian/*
Copyright: 2016 Scott Kitterman <scott@kitterman.com>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: MPL or GPL-2.0+ or LGPL-2.1+
 ***** BEGIN LICENSE BLOCK *****
 Version: MPL 1.1/GPL 2.0/LGPL 2.1
 .
 The contents of this file are subject to the Mozilla Public License Version
 1.1 (the "License"); you may not use this file except in compliance with
 the License. You may obtain a copy of the License at
 http://www.mozilla.org/MPL/
 .
 Software distributed under the License is distributed on an "AS IS" basis,
 WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 for the specific language governing rights and limitations under the
 License.
 .
 The Original Code is the Netscape security libraries.
 .
 The Initial Developer of the Original Code is
 Netscape Communications Corporation.
 Portions created by the Initial Developer are Copyright (C) 1994-2000
 the Initial Developer. All Rights Reserved.
 .
 Contributor(s):
 .
 Alternatively, the contents of this file may be used under the terms of
 either the GNU General Public License Version 2 or later (the "GPL"), or
 the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 in which case the provisions of the GPL or the LGPL are applicable instead
 of those above. If you wish to allow use of your version of this file only
 under the terms of either the GPL or the LGPL, and not to allow others to
 use your version of this file under the terms of the MPL, indicate your
 decision by deleting the provisions above and replace them with the notice
 and other provisions required by the GPL or the LGPL. If you do not delete
 the provisions above, a recipient may use your version of this file under
 the terms of any one of the MPL, the GPL or the LGPL.
 .
 ***** END LICENSE BLOCK *****
 .
 On Debian systems, the full text of the GNU General Public License can be
 found in the file `/usr/share/common-licenses/GPL-2'.
 .
 On Debian systems, the full text of the GNU Lesser General Public License
 can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

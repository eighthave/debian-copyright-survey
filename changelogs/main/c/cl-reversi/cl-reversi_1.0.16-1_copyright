This package was debianized by Kevin M. Rosenberg <kmr@debian.org> in
Oct 2002.

It was downloaded from http://reversi.kpe.io/
Upstream Authors: Kevin Rosenberg & Peter Norvig


CL-Reversi's Copyright Statement
--------------------------------

Copyright (c) 2000-2002 Kevin Rosenberg
Copyright (c) 1998-2002 Peter Norvig

This code is free software; you can redistribute it and/or modify it
under the terms of the version 2.1 of the GNU Lesser General Public
License as published by the Free Software Foundation, as clarified by
the Franz preamble to the LGPL found in
http://opensource.franz.com/preamble.html. The preambled is copied below.

This code is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.  See the GNU
Lesser General Public License for more details.

The GNU Lessor General Public License can be found in your Debian file
system in /usr/share/common-licenses/LGPL-3.



Peter Norvig's Original Copyright
---------------------------------

Copyright (c) 1998-2002 by Peter Norvig.

Permission is granted to anyone to use this software, in source or
object code form, on any computer system, and to modify, compile,
decompile, run, and redistribute it to anyone else, subject to the
following restrictions:

   1. The author makes no warranty of any kind, either expressed or
implied, about the suitability of this software for any purpose.

   2. The author accepts no liability of any kind for damages or other
consequences of the use of this software, even if they arise from
defects in the software.

   3. The origin of this software must not be misrepresented, either
by explicit claim or by omission.

   4. Altered versions must be plainly marked as such, and must not be
misrepresented as being the original software. Altered versions may be
distributed in packages under other licenses (such as the GNU
license).

If you find this software useful, it would be nice if you let me
(peter@norvig.com) know about it, and nicer still if you send me
modifications that you are willing to share. However, you are not
required to do so.




Preamble to the Gnu Lesser General Public License
-------------------------------------------------
Copyright (c) 2000 Franz Incorporated, Berkeley, CA 94704

The concept of the GNU Lesser General Public License version 2.1
("LGPL") has been adopted to govern the use and distribution of
above-mentioned application. However, the LGPL uses terminology that
is more appropriate for a program written in C than one written in
Lisp. Nevertheless, the LGPL can still be applied to a Lisp program if
certain clarifications are made. This document details those
clarifications. Accordingly, the license for the open-source Lisp
applications consists of this document plus the LGPL. Wherever there
is a conflict between this document and the LGPL, this document takes
precedence over the LGPL.

A "Library" in Lisp is a collection of Lisp functions, data and
foreign modules. The form of the Library can be Lisp source code (for
processing by an interpreter) or object code (usually the result of
compilation of source code or built with some other
mechanisms). Foreign modules are object code in a form that can be
linked into a Lisp executable. When we speak of functions we do so in
the most general way to include, in addition, methods and unnamed
functions. Lisp "data" is also a general term that includes the data
structures resulting from defining Lisp classes. A Lisp application
may include the same set of Lisp objects as does a Library, but this
does not mean that the application is necessarily a "work based on the
Library" it contains.

The Library consists of everything in the distribution file set before
any modifications are made to the files. If any of the functions or
classes in the Library are redefined in other files, then those
redefinitions ARE considered a work based on the Library. If
additional methods are added to generic functions in the Library,
those additional methods are NOT considered a work based on the
Library. If Library classes are subclassed, these subclasses are NOT
considered a work based on the Library. If the Library is modified to
explicitly call other functions that are neither part of Lisp itself
nor an available add-on module to Lisp, then the functions called by
the modified Library ARE considered a work based on the Library. The
goal is to ensure that the Library will compile and run without
getting undefined function errors.

It is permitted to add proprietary source code to the Library, but it
must be done in a way such that the Library will still run without
that proprietary code present. Section 5 of the LGPL distinguishes
between the case of a library being dynamically linked at runtime and
one being statically linked at build time. Section 5 of the LGPL
states that the former results in an executable that is a "work that
uses the Library." Section 5 of the LGPL states that the latter
results in one that is a "derivative of the Library", which is
therefore covered by the LGPL. Since Lisp only offers one choice,
which is to link the Library into an executable at build time, we
declare that, for the purpose applying the LGPL to the Library, an
executable that results from linking a "work that uses the Library"
with the Library is considered a "work that uses the Library" and is
therefore NOT covered by the LGPL.

Because of this declaration, section 6 of LGPL is not applicable to
the Library. However, in connection with each distribution of this
executable, you must also deliver, in accordance with the terms and
conditions of the LGPL, the source code of Library (or your derivative
thereof) that is incorporated into this executable.


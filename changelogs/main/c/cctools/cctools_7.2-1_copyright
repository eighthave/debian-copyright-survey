Format: http://dep.debian.net/deps/dep5/
Upstream-Name: cctools
Upstream-Contact: Douglas Thain <dthain@nd.edu>
Source: http://www.cse.nd.edu/~ccl/software/

Files: *
Copyright: 1999-2011, Douglas Thain,
           1999-2004, University of Wisconsin,
           2005-2011, The University of Notre Dame
License: GPL-2

Files: dttools/src/md5.c
Copyright: 1991-1992, RSA Data Security, Inc
License: Other
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

Files: parrot/src/pfs_location.*
Copyright: 2009-2011, Michael Albrecht and The University of Notre Dame
License: GPL-2

Files: parrot/src/pfs_service_glite.cc
Copyright: 2004, Members of the EGEE Collaboration
           http://eu-egee.org/partners/
License: Apache
 Source: http://glite.cern.ch/license
 .
 On Debian systems, the full text of the Apache License version 2 can be found
 in the file `/usr/share/common-licenses/Apache-2.0'.

Files: sand/src/sand_compress_reads.c
Copyright: 2009-2011, The University of Notre Dame & University of Cambridge
License: GPL-2

License: GPL-2
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published
 by the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

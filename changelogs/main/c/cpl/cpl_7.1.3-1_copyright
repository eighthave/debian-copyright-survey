Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cpl
Source: https://www.eso.org/sci/software/cpl/download.html
Upstream-Author: Ralf Palsa <rpalsa@eso.org>,
 Lars Lundin <llundin@eso.org>
Upstream-Contact: cpl-help@eso.org

Files: *
Copyright: Copyright (C) 2001-2010 European Southern Observatory
License: GPL-2

Files: */Makefile.in admin/* libcext/m4/lt*.m4 aclocal.m4 configure
Copyright: Copyright (C) 1994-2009 Free Software Foundation, Inc.
License: Free
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

Files: libltdl/*
Copyright: Copyright (C) 1994-2009 Free Software Foundation, Inc.
License: LGPL-2+
 GNU Libltdl is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 As a special exception to the GNU Lesser General Public License,
 if you distribute this file as part of a program or library that
 is built using GNU Libtool, you may include this file under the
 same distribution terms that you use for the rest of that program.
 .
 GNU Libltdl is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: libcext/cext/snprintf.c
Copyright: 1995 Patrick Powell, 2008 Holger Weiss
License: FSFULLR-alike
 This code is based on code written by Patrick Powell (papowell@astart.com)
 It may be used for any purpose as long as this notice remains intact
 on all source code distributions
 .
 This version of the code is maintained by Holger Weiss <holger@jhweiss.de>.
 My changes to the code may freely be used, modified and/or redistributed for
 any purpose.  It would be nice if additions and fixes to this file (including
 trivial code cleanups) would be sent back in order to let me include them in
 the version available at <http://www.jhweiss.de/software/snprintf.html>.
 However, this is not a requirement for using or redistributing (possibly
 modified) versions of this file, nor is leaving this notice intact mandatory.

Files: cplcore/cpl_mpfit.*
Copyright: None
License: public-domain
 Original public domain version by B. Garbow, K. Hillstrom, J. More'
 (Argonne National Laboratory, MINPACK project, March 1980)
 .
 Translation to C Language by S. Moshier (moshier.net)
 .
 Enhancements and packaging by C. Markwardt
 (comparable to IDL fitting routine MPFIT
 see http://cow.physics.wisc.edu/~craigm/idl/idl.html)

Files: debian/*
Copyright: 2011 Ole Streicher <olebole@debian.org>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

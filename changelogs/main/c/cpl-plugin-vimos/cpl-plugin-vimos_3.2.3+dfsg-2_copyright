Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vimos
Upstream-Contact: ESO User Support Department <usd-help@eso.org>
Source: ftp://ftp.eso.org/pub/dfs/pipelines/vimos/

Files: *
Copyright: Copyright (C) 2001-2012 European Southern Observatory
 2013 Ole Streicher (Debian packaging)
License: GPL-v2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: external/libwcs/* libpil/pil/getopt*
Copyright: 1995-1999 Mark Calabretta <mcalabre@atnf.csiro.au>,
 1991-1999 John B. Roll jr.,
 1986 association of universities for research in astronomy inc.,
 1994-1999 Associated Universities, Inc. Washington DC, USA.
 1987-2001 Free Software Foundation, Inc.
License: LGPL-2+
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published
 by the Free Software Foundation; either version 2 of the License, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

Files: external/libwcs/fileutil.c external/libwcs/fitsfile.c
 external/libwcs/fortvimoswcs.c external/libwcs/hget.c external/libwcs/hput.c
 external/libwcs/iget.c external/libwcs/imhfile.c external/libwcs/imio.c
 external/libwcs/vimoswcs.c external/libwcs/vimoswcsfort.c
 external/libwcs/vimoswcsinit0.c external/libwcs/vimoswcsinit1.c
 external/libwcs/vimoswcsinit.c external/libwcs/vimoswcsinitm.c
Copyright: 1996-2001 Smithsonian Astrophysical Observatory
License: do-anything-except-remove-copyright
 You may do anything you like with this file except remove this copyright.
 The Smithsonian Astrophysical Observatory makes no representations about the
 suitability of this software for any purpose.  It is provided "as is" without
 express or implied warranty.

Files: libpil/kazlib/*
Copyright: Copyright (C) 1997 Kaz Kylheku <kaz@ashi.footprints.net>
License: Kylheku
 All rights are reserved by the author, with the following exceptions:
 Permission is granted to freely reproduce and distribute this software,
 possibly in exchange for a fee, provided that this copyright notice appears
 intact. Permission is also granted to adapt this software to produce
 derivative works, as long as the modified versions carry this copyright
 notice and additional notices stating that the work has been modified.
 This source code may be translated into executable form and incorporated
 into proprietary software; there is no requirement for such software to
 contain a copyright notice related to this source.

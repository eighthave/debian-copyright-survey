Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ASDF
Upstream-Contact: asdf-devel@common-lisp.net
Source: https://common-lisp.net/project/asdf/

Files: *
Copyright: 2001-2016 Daniel Barlow, Francois-Rene Rideau, Robert P. Goldman, and contributors
Comment: This is the MIT license, but Debian, after GNU, calls it the Expat license
 to unambiguously distinguish it from other licenses once used by MIT.
License: Expat
 (This is the MIT / X Consortium license as taken from
  http://www.opensource.org/licenses/mit-license.html on or about
  Monday; July 13, 2009)
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: debian/*
Copyright: 2002 Kevin M. Rosenberg <kmr@debian.org>
License: GPL-2
 On Debian systems, the full text of the GNU GPL v2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
Comment: This package was initially debianized by Kevin M. Rosenberg <kmr@debian.org>
 on Fri, 16 Aug 2002 23:14:49 -0600.
 Peter Van Eynde took over in 2005. Then from 2010 to 2014, Francois-Rene Rideau
 maintained the debian package as part of the upstream git repository.

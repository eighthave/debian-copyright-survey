Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: curry-base
Upstream-Contact: Michael Hanus <mh@informatik.uni-kiel.de>
Source: https://git.ps.informatik.uni-kiel.de/curry/curry-base

Files: src/Curry/Base/Monad.hs
Copyright: 2014 - 2016, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/FlatCurry/Goodies.hs
Copyright: 2006, Sebastian Fischer
  2011, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Base/Ident.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2011 - 2013, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/FlatCurry.hs
Copyright: 2014, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Syntax/Utils.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2005, Martin Engelke
  2011 - 2014, Björn Peemöller
  2015, Jan Tikovsky
License: BSD-3-clause

Files: src/Curry/Base/Message.hs
Copyright: 2009, Holger Siegel
  2012 - 2015, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/AbstractCurry/Type.hs
Copyright: 2004, Michael Hanus
  2005, Martin Engelke
  2015, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/AbstractCurry/Files.hs
Copyright: 2004, Michael Hanus
  2005, Martin Engelke
  2014, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Files/Filenames.hs
Copyright: 2009, Holger Siegel
  2013 - 2014, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Base/Position.hs
Copyright: , Wolfgang Lux
License: BSD-3-clause

Files: src/Curry/FlatCurry/Files.hs
Copyright: 2014, Björn Peemöller
  2017, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Base/LLParseComb.hs
Copyright: 1999-2004, Wolfgang Lux
  2016, Jan Tikovsky
License: BSD-3-clause

Files: src/Curry/AbstractCurry.hs
Copyright: 2004, Michael Hanus
  2005, Martin Engelke
  2013, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Base/Span.hs
Copyright: 2016, Jan Tikovsky
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Syntax/ShowModule.hs
Copyright: 2008, Sebastian Fischer
  2011 - 2015, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Syntax/Lexer.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2005, Martin Engelke
  2011 - 2013, Björn Peemöller
  2016, Jan Tikovsky
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Syntax/Parser.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2005, Martin Engelke
  2011 - 2015, Björn Peemöller
  2016 - 2017, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Syntax/Pretty.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2005, Martin Engelke
  2011 - 2015, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/Syntax/Type.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2005, Martin Engelke
  2011 - 2015, Björn Peemöller
  2014, Jan Rasmus Tikovsky
License: BSD-3-clause

Files: src/Curry/Base/LexComb.hs
Copyright: 1999 - 2004, Wolfgang Lux
  2012 - 2013, Björn Peemöller
  2016, Jan Tikovsky
License: BSD-3-clause

Files: src/Curry/Syntax.hs
Copyright: 2009, Holger Siegel
  2011 - 2013, Björn Peemöller
  2016, Finn Teegen
  2016, Jan Tikovsky
License: BSD-3-clause

Files: src/Curry/FlatCurry/Pretty.hs
Copyright: 2015, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Files/Unlit.hs
Copyright: 2009, Holger Siegel
  2012  - 2014, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/Syntax/InterfaceEquivalence.hs
Copyright: 2000 - 2007, Wolfgang Lux
  2014 - 2015, Björn Peemöller
  2014, Jan Tikovsky
License: BSD-3-clause

Files: src/Curry/Syntax/Extension.hs
Copyright: 2013 - 2014, Björn Peemöller
  2016, Finn Teegen
License: BSD-3-clause

Files: src/Curry/FlatCurry/Type.hs
Copyright: 2003, Michael Hanus
  2004, Martin Engelke
  2005, Bernd Brassel
License: BSD-3-clause

Files: src/Curry/Files/PathUtils.hs
Copyright: 1999 - 2003, Wolfgang Lux
  2011 - 2014, Björn Peemöller <bjp@informatik.uni-kiel.de>
  2017, Finn teegen <fte@informatik.uni-kiel.de>
License: BSD-3-clause

Files: src/Curry/Base/Pretty.hs
Copyright: 2013 - 2014, Björn Peemöller
License: BSD-3-clause

Files: util/canonfint.hs
Copyright: 2016, Björn Peemöller
License: BSD-3-clause

Files: src/Curry/CondCompile/Parser.hs
 src/Curry/CondCompile/Transform.hs
 src/Curry/CondCompile/Type.hs
Copyright: 2017, Kai-Oliver Prott
  2017, Finn Teegen
License: BSD-3-clause

Files: src/Curry/FlatCurry/Annotated/Goodies.hs
 src/Curry/FlatCurry/Annotated/Type.hs
 src/Curry/FlatCurry/Annotated/Typing.hs
Copyright: 2016-2017, Finn Teegen
  2017, Finn Teegen
License: BSD-3-clause

Files: src/Curry/FlatCurry/InterfaceEquivalence.hs
Copyright: 2006, Martin Engelke
  2011-2014, Björn Peemöller
  2014, Jan Tikovsky
License: BSD-3-clause

Files: test/HaskellRecords.curry
  test/TestBase.hs
  test/Pragmas.curry
  util/lex.hs
  util/parse.hs
  .gitignore
  CHANGELOG.md
  Setup.hs
  curry-base.cabal
  overview.md
Copyright:
  1998-2004, Wolfgang Lux
  2005-2016, Michael Hanus
License: BSD-3-clause
Comment:
 Assuming copyright information as found in LICENSE file.

Files: debian/*
Copyright: 2016, Mike Gabriel <sunweaver@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 - None of the names of the copyright holders and contributors may be
   used to endorse or promote products derived from this software without
   specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

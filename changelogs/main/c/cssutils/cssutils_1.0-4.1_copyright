Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: cssutils
Upstream-Contact: Christof Hoeke
                  Walter Doerwald
Source: http://cthedot.de/cssutils/

Files: *
Copyright: 2004-2013 Christof Hoeke
           2004-2012 Walter Doerwald
License: LGPL-3+
 encutils is free software: you can redistribute it and/or modify it under the
 terms of the GNU Lesser General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option) any
 later version.
 .
 encutils is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with encutils.  If not, see <http://www.gnu.org/licenses/>.
Comment: On Debian systems, the complete text of the GNU Lesser General Public
 License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 Only explicit copyright is from Christof Hoeke, but the mention of Walter
 Doerwald in the credits and the __author__ stanzas in some Python source
 files quite certainly make him a copyright holder.  The upstream licensing
 moved from Creative Commons licensing to LGPL 2.1 and subsequently to LGPL 3.
 Some files such as setup.py mention LGPL 2.1+, but COPYING.LESSER,
 the upstream site, and most files clearly mention LGPL 3+ rendering the
 result LGPL 3+.

Files: debian/*
Copyright: 2008 Bernd Zeimetz <bzed@debian.org>
           2008 Damien Churchill <damoxc@gmail.com>
           2008 Loïc Minier <lool@dooz.org>
           2011-2013 Charlie Smotherman <cjsmo@cableone.net>
           2014-2015 Hugo Lefeuvre <hle@debian.org>
License: LGPL-2.1
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2 of the License.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian systems, the complete text of the GNU Lesser General Public
  License can be found in `/usr/share/common-licenses/LGPL-2.1'.

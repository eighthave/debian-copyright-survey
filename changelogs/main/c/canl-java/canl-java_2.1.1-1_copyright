Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: canl-java
Source: https://github.com/eu-emi/canl-java/archive/canl-2.1.1.tar.gz

Files: *
Copyright: 2010-2012 ICM Uniwersytet Warszawski
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer. Redistributions in binary
 form must reproduce the above copyright notice, this list of conditions and
 the following disclaimer in the documentation and/or other materials provided
 with the distribution. Neither the name of the ICM Uniwersytet Warszawski nor
 the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: src/main/java/eu/emi/security/authn/x509/helpers/proxy/IPAddressHelper.java
       src/main/java/eu/emi/security/authn/x509/helpers/proxy/ProxyAddressRestrictionData.java
       src/main/java/eu/emi/security/authn/x509/helpers/proxy/ProxyCertInfoExtension.java
       src/main/java/eu/emi/security/authn/x509/helpers/proxy/ProxyHelper.java
       src/main/java/eu/emi/security/authn/x509/helpers/proxy/ProxySAMLExtension.java
       src/main/java/eu/emi/security/authn/x509/helpers/proxy/ProxyTracingExtension.java
       src/main/java/eu/emi/security/authn/x509/impl/AbstractHostnameToCertificateChecker.java
       src/main/java/eu/emi/security/authn/x509/proxy/ProxyChainInfo.java
       src/main/java/eu/emi/security/authn/x509/proxy/ProxyPolicy.java
       src/test/java/eu/emi/security/authn/x509/impl/GLiteValidatorTest.java
       src/test/java/eu/emi/security/authn/x509/impl/HostnameCheckerTest.java
Copyright: 2011-2012 ICM Uniwersytet Warszawski
           2004 Members of the EGEE Collaboration 
License: Apache-2.0
 On Debian systems, the complete text of the Apache License version 2.0
 can be found in "/usr/share/common-licenses/Apache-2.0".

Files: src/main/java/eu/emi/security/authn/x509/helpers/pkipath/bc/CertPathValidatorUtilities.java
       src/main/java/eu/emi/security/authn/x509/helpers/pkipath/bc/CertStatus.java
       src/main/java/eu/emi/security/authn/x509/helpers/pkipath/bc/FixedBCPKIXCertPathReviewer.java
       src/main/java/eu/emi/security/authn/x509/helpers/pkipath/bc/ReasonsMask.java
       src/main/java/eu/emi/security/authn/x509/helpers/pkipath/bc/RFC3280CertPathUtilitiesHelper.java
       src/main/java/eu/emi/security/authn/x509/helpers/pkipath/NonValidatingCertPathBuilder.java
Copyright: 2000-2011 The Legion Of The Bouncy Castle
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Files: src/main/java/eu/emi/security/authn/x509/helpers/ReaderInputStream.java
Copyright: Licensed to the Apache Software Foundation (ASF) under one or more
           contributor license agreements.
License: Apache-2.0
 On Debian systems, the complete text of the Apache License version 2.0
 can be found in "/usr/share/common-licenses/Apache-2.0".

Files: debian/*
Copyright: 2014 Mattias Ellert <mattias.ellert@fysast.uu.se>
License: Apache-2.0
 On Debian systems, the complete text of the Apache License version 2.0
 can be found in "/usr/share/common-licenses/Apache-2.0".

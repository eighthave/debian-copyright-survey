Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Packaged-By: Romain Beauxis <toots@rastageeks.org>
Packaged-Date: Thu, 12 Jun 2008 14:14:29 +0200
Source: https://github.com/johnwhitington/camlpdf
Upstream-Contact: John Whitington <john@coherentgraphics.co.uk>

Files: *
Copyright: 2007-2013, Coherent Graphics Ltd
License: LGPL-2.1-exception
 This Library is distributed under the terms of the GNU Lesser General
 Public License (LGPL) version 2.1 or above.
 .
 As a special exception to the GNU Lesser General Public License, you
 may link, statically or dynamically, a "work that uses the Library"
 with a publicly distributed version of the Library to produce an
 executable file containing portions of the Library, and distribute
 that executable file under terms of your choice, without any of the
 additional requirements listed in clause 6 of the GNU Lesser General
 Public License.  By "a publicly distributed version of the Library",
 we mean either the unmodified Library as distributed by Coherent
 Graphics Ltd, or a modified version of the Library that is
 distributed under the conditions defined in clause 3 of the GNU
 Lesser General Public License.  This exception does not however
 invalidate any other reasons why the executable file might be covered
 by the GNU Lesser General Public License.
 .
 The complete text of the GNU Lesser General Public License can be
 found in `/usr/share/common-licenses/LGPL-2.1'.

Files: miniz.c
Copyright: 2012 Rich Geldreich <richgel99@gmail.com>
License: Unlicense
  This is free and unencumbered software released into the public domain.
  .
  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.
  .
  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.
  .
  For more information, please refer to <http://unlicense.org/>


Files: pdfafmdata.ml
Copyright: 1989, 1990, 1991, 1992, 1993, 1997 Adobe Systems Incorporated
License: other
 This file embeds Adobe's AFM data files and their license. The
 following text is extracted from this file:
 .
 This file constitutes a modification of Adobe's AFM data files. This
 file also constitutes a modification of Adobe's license file.
 .
 This file and the 14 PostScript(R) AFM files it accompanies may be
 used, copied, and distributed for any purpose and without charge,
 with or without modification, provided that all copyright notices are
 retained; that the AFM files are not distributed without this file;
 that all modifications to this file or any of the AFM files are
 prominently noted in the modified file(s); and that this paragraph is
 not modified. Adobe Systems has no responsibility or obligation to
 support the use of the AFM files.

Files: OCamlMakefile examples/OCamlMakefile
Copyright: 1999-2012, Markus Mottl
License: LGPL-2.1

Files: rijndael-alg-fst.*
Copyright: Vincent Rijmen <vincent.rijmen@esat.kuleuven.ac.be>
	   Antoon Bosselaers <antoon.bosselaers@esat.kuleuven.ac.be>
	   Paulo Barreto <paulo.barreto@terra.com.br>
License: public-domain
 This code is hereby placed in the public domain.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: sha2.*
Copyright: 2005, 2007 Olivier Gay <olivier.gay@a3.epfl.ch>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the project nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.


Files: stubs-aes.c
Copyright: 2002 INRIA
License: LGPL-2.1

Files: debian/*
Copyright: 2013, Stéphane Glondu
License: LGPL-2.1

License: LGPL-2.1
 This software is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, version 2.1.
 On Debian systems you can find a copy of this license in
 /usr/share/common-licenses/LGPL-2.1

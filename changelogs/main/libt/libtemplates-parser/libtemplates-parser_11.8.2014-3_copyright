Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Templates parser
Source: https://libre.adacore.com/libre/download/
 Upstream sources are also available in a git repository:
 https://forge.open-do.org/anonscm/git/template-parser/template-parser.git

Files: *
Copyright: 1999-2004 Pascal Obry
           2005-2014 AdaCore
License: GPL-3+
Comment:
 All the content available on libre.adacore.com is licensed
 under the terms of the pure GPL, despite the fact that AdaCore have
 not yet adjusted the licensing boilerplate in documentation.
 .
 This is stated for example at
 http://lists.adacore.com/pipermail/gtkada/2009-June/003789.html. The
 actual license is a decision of AdaCore. Please contact them at
 libre@adacore.com for any clarification. .
 .
 As a result, if you use this library in your program, you have four options:
 .
 - do not distribute your program at all (you can always use it for
   yourself).
 .
 - distribute your program under the terms of the GPL, i.e. with full
   sources and the four freedoms: inspection, redistribution,
   modification, and distribution of modified copies.
 .
 - distribute your program in source form only, under licensing terms
   of your choosing (perhaps under non-disclosure, non-redistribution
   terms), and ask your licensees to recompile your program for
   themselves.
 .
 - contact AdaCore (sales@adacore.com) and purchase a GNAT-Modified
   GPL which will allow you to distribute your program in binary-only
   form without disclosing your sources.

Files: debian/*
Copyright: 2003-2008 Ludovic Brenta <lbrenta@debian.org>
           2013-2014 Nicolas Boulenguez <nicolas@debian.org>
License: GPL-3+

License: GPL-3+
 This file is free software; you can redistribute it and/or modify it
 under terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version. It is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY -- or FITNESS FOR A PARTICULAR PURPOSE.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

This package was downloaded from: http://sourceforge.net/projects/tuxcap

Upstream Author:

    W.P. van Paassen <wp.van.paassen@gmail.com>

Copyright:

    PopCap Games Framework:
        Copyright (c) 2005-2008 PopCap Games. All rights reserved.
    TuxCap Games Framework (port, fixes, interfaces and extensions):
        Copyright (c) 2007-2008 W.P. van Paassen
    Chipmunk physics engine:
        Copyright (c) 2007 Scott Lembcke
    Pycap python bindings:
        Copyright (c) 2007 Jarrad Woods
    Hge particle engine:
        Copyright (c) 2003-2006 Relish Games


PopCap Games Framework License, version 1.0:

    Copyright (c) 2005 PopCap Games. All rights reserved.

    Redistribution and use in source and binary forms, with or
    without modification, are permitted provided that the following
    conditions are met:

    1. Redistributions of source code or derived libraries, whether
    source code or binary, must include a copy of this PopCap Games
    Framework License in its entirety.

    2. The end-user documentation included with a work which
    contains any portion of the PopCap Games Framework must include
    the following acknowledgment:

    "This product includes portions of the PopCap Games Framework
    (http://developer.popcap.com/)."

    Alternatively, this acknowledgment may appear in the software
    itself, wherever such third-party acknowledgments normally
    appear.

    3. The names "PopCap" and "PopCap Games" must not be used to
    endorse or promote products derived from this software without
    prior written permission. For written permission, please
    contact bizdev@popcap.com.

    4. Products derived from this software may not be called
    "PopCap", nor may "PopCap" appear in their name, without prior
    written permission of PopCap Games.

    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
    WARRANTIES OF MERCHANTABILITY, NON-INFRINGMENT AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL POPCAP
    GAMES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
    IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    THE POSSIBILITY OF SUCH DAMAGE.

TuxCap Games Framework License:

    Copyright (c) 2007-2008 W.P. van Paassen
       
    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, 
    copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following 
    conditions:
    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.
                           
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
    OTHER DEALINGS IN THE SOFTWARE.
 
Chipmunk Physics engine :

    Copyright (c) 2007 Scott Lembcke

    Permission is hereby granted, free of charge, to any person 
    obtaining a copy of this software and associated documentation 
    files (the "Software"), to deal in the Software without 
    restriction, including without limitation the rights to use, 
    copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the 
    Software is furnished to do so, subject to the following 
    conditions:
    The above copyright notice and this permission notice shall be 
    included in all copies or substantial portions of the Software.
                           
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
    OTHER DEALINGS IN THE SOFTWARE.
 
HGE Particle engine:
                        
    Copyright (c) 2003-2006 Relish Games

    This software is provided 'as-is', without any express or implied
    warranty. 
    In no event will the authors be held liable for any damages 
    arising from the use of this software.
    Permission is granted to anyone to use this software for any 
    purpose, including commercial applications, and to alter it and 
    redistribute it freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented; 
    you must not claim that you wrote the original software. If you 
    use this software in a product, an acknowledgment in the product 
    documentation would be appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and
    must not be misrepresented as being the original software.
    3. This notice may not be removed or altered from any source 
    distribution.


The following files are licensed under both the PopCap Games 
Framework license and the TuxCap Games Framework license:

    Buffer.cpp
    ButtonWidget.cpp
    Checkbox.cpp
    Color.cpp
    Common.cpp
    CursorWidget.cpp
    D3DInterface.cpp
    DDImage.cpp
    DDInterface.cpp
    DescParser.cpp
    DialogButton.cpp
    Dialog.cpp
    EditWidget.cpp
    Flags.cpp
    Font.cpp
    Graphics.cpp
    HyperlinkWidget.cpp
    Image.cpp
    ImageFont.cpp
    ImageLib.cpp
    Insets.cpp
    KeyCodes.cpp
    ListWidget.cpp
    MemoryImage.cpp
    MTRand.cpp
    MusicInterface.cpp
    NativeDisplay.cpp
    NaturalCubicSpline.cpp
    PropertiesParser.cpp
    Quantize.cpp
    Ratio.cpp
    ResourceManager.cpp
    ScrollbarWidget.cpp
    ScrollbuttonWidget.cpp
    SexyAppBase.cpp
    SexyMatrix.cpp
    SharedImage.cpp
    Slider.cpp
    SWTri.cpp
    SWTri_DrawTriangle.cpp
    SWTri_DrawTriangleInc1.cpp
    SWTri_DrawTriangleInc2.cpp
    SWTri_GetTexel.cpp
    SWTri_Loop.cpp
    SWTri_Pixel555.cpp
    SWTri_Pixel565.cpp
    SWTri_Pixel8888.cpp
    SWTri_Pixel888.cpp
    SWTri_TexelARGB.cpp
    TextWidget.cpp
    WidgetContainer.cpp
    Widget.cpp
    WidgetManager.cpp
    XMLParser.cpp
    XMLWriter.cpp
    BltRotatedHelper.inc
    Buffer.h
    ButtonListener.h
    ButtonWidget.h
    Checkbox.h
    CheckboxListener.h
    Color.h
    Common.h
    CursorWidget.h
    D3DInterface.h
    DDI_Additive.inc
    DDI_AlphaBlt.inc
    DDI_BltRotated_Additive.inc
    DDI_BltRotated.inc
    DDI_FastBlt_NoAlpha.inc
    DDI_FastStretch_Additive.inc
    DDI_FastStretch.inc
    DDImage.h
    DDI_NormalBlt_Volatile.inc
    DDInterface.h
    DescParser.h
    DialogButton.h
    Dialog.h
    DialogListener.h
    EditListener.h
    EditWidget.h
    Flags.h
    Font.h
    GENERIC_DrawLineAA.inc
    Graphics.h
    HyperlinkWidget.h
    ImageFont.h
    Image.h
    ImageLib.h
    Insets.h
    KeyCodes.h
    ListListener.h
    ListWidget.h
    MemoryImage.h
    MI_AdditiveBlt.inc
    MI_BltRotated_Additive.inc
    MI_BltRotated.inc
    MI_GetNativeAlphaData.inc
    MI_GetRLAlphaData.inc
    MI_NormalBlt.inc
    MTRand.h
    MusicInterface.h
    NativeDisplay.h
    NaturalCubicSpline.h
    Point.h
    PropertiesParser.h
    Quantize.h
    Ratio.h
    Rect.h
    ResourceManager.h
    ScrollbarWidget.h
    ScrollbuttonWidget.h
    ScrollListener.h
    SexyAppBase.h
    SexyMatrix.h
    SexyVector.h
    SharedImage.h
    Slider.h
    SliderListener.h
    SoundInstance.h
    SoundManager.h
    SWTri.h
    TextWidget.h
    TriVertex.h
    WidgetContainer.h
    Widget.h
    WidgetManager.h
    XMLParser.h
    XMLWriter.h
    PycapApp.cpp
    PycapApp.h
    PycapBoard.cpp
    PycapBoard.h
    PycapResources.cpp
    PycapResources.h

The following files are licensed under the TuxCap Games Framework 
license:

    AudiereLoader.cpp
    AudiereMusicInterface.cpp
    AudiereSoundInstance.cpp
    AudiereSoundManager.cpp
    AudiereLoader.h
    AudiereMusicInterface.h
    AudiereSoundInstance.h
    AudiereSoundManager.h
    SDLMixerMusicInterface.cpp
    SDLMixerSoundInstance.cpp
    SDLMixerSoundManager.cpp
    SDLMixerMusicInterface.h
    SDLMixerSoundInstance.h
    SDLMixerSoundManager.h
    ParticlePhysicsSystem.cpp
    ParticlePhysicsSystem.h
    Physics.h
    Physics.cpp
    PhysicsListener.h

The following files are licensed under the Chipmunk Physics engine 
license:

    tuxcap/chipmunk/*

The following files are licensed under the HGE Particle engine 
license:

    tuxcap/hgeparticle/*

The Debian packaging is (C) 2009-2010, Miriam Ruiz <little_miry@yahoo.es> and
is licensed under the same license as the program (BSD).

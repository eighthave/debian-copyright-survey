Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libtgvoip
Upstream-Contact: John Preston <johnprestonmail@gmail.com>
Source: https://github.com/telegramdesktop/libtgvoip
Files-Excluded: webrtc_dsp/*
Comment: Skip copied code that is packaged in the libtgowt source package.

Files: *
Copyright: 2017-2019, Gregory Klyushnikov <grishka93@gmail.com>
           2020, John Preston <johnprestonmail@gmail.com>
           2020, overtake <overtakeful@gmail.com>
           2020, Ali
License: Unlicense

Files: debian/*
Copyright: 2017-2020, Nicholas Guriev <guriev-ns@ya.ru>
License: Unlicense

Files: debian/patches/Build-scripts.patch
Copyright: 2020, The Desktop App Toolkit Authors
           2020, The Telegram Desktop Authors
           2020, Nicholas Guriev <guriev-ns@ya.ru>
License: GPL-3+ with OpenSSL exception and Unlicense

Files: json11.hpp json11.cpp
Copyright: 2013, Dropbox Inc.
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-3+ with OpenSSL exception
 Desktop App Toolkit is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 It is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 In addition, as a special exception, the copyright holders give permission
 to link the code of portions of this program with the OpenSSL library.
 .
 Full license: https://github.com/desktop-app/legal/blob/master/LICENSE
 .
 On Debian-based systems, full text of the GNU General Public License of
 version 3 can be found in "/usr/share/common-licenses/GPL-3" file.

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
 software, either in source code form or as a compiled binary, for any purpose,
 commercial or non-commercial, and by any means.
 .
 In jurisdictions that recognize copyright laws, the author or authors of this
 software dedicate any and all copyright interest in the software to the public
 domain. We make this dedication for the benefit of the public at large and to
 the detriment of our heirs and successors. We intend this dedication to be an
 overt act of relinquishment in perpetuity of all present and future rights to
 this software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

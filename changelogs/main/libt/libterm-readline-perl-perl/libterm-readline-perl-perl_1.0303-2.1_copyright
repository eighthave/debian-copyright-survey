Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Term-ReadLine-Perl
Upstream-Contact: Ilya Zakharevich <cpan@ilyaz.org>
Source: https://metacpan.org/release/Term-ReadLine-Perl

Files: *
Copyright: 1995, Ilya Zakharevich <cpan@ilyaz.org>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009, Jonathan Yu <jawnsy@cpan.org>
 2006, Krzysztof Krzyzaniak (eloy) <eloy@debian.org>
 2006, Niko Tyni <ntyni@iki.fi>
 2005-2006, Gunnar Wolf <gwolf@debian.org>
 2002, Joerg Jaspert <joerg@debian.org>
 2001, Brendan O'Dea <bod@debian.org>
 1999, Darren Stalder <torin@daft.com>
 1998, Ben Gertzfield <che@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

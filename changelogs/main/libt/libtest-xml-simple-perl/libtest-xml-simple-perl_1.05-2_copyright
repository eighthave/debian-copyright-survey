Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Test-XML-Simple
Upstream-Contact: Joe McMahon <mcmahon@cpan.org>
Source: https://metacpan.org/release/Test-XML-Simple

Files: *
Copyright: 2005-2016, Yahoo! and Joe McMahon
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2009, Jonathan Yu <jawnsy@cpan.org>
 2009, Alex Muntada <alexm@alexm.org>
 2008, Jaldhar H. Vyas <jaldhar@debian.org>
 2011, Peter Pentchev <roam@ringlet.net>
 2012-2013, gregor herrmann <gregoa@debian.org>
 2020, Alex Muntada <alexm@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Text-Markdown
Upstream-Contact: Tomas Doran <bobtfish@bobtfish.net>
Source: https://metacpan.org/release/Text-Markdown

Files: *
Copyright: 2003-2004, John Gruber <comments@daringfireball.net>
 2005-2006, Fletcher T. Penney <owner@fletcherpenney.net>
 2006-2008, Darren Kulp <kulp@cpan.org>
 2006-2008, Tomas Doran <bobtfish@bobtfish.net>
License: BSD-3-Clause

Files: debian/*
Copyright: 2009-2010, Jonathan Yu <jawnsy@cpan.org>
 2008, Ansgar Burchardt <ansgar@debian.org>
 2008-2015, gregor herrmann <gregoa@debian.org>
 2008, Adeodato Simó <dato@net.com.org.es>
 2008, Gunnar Wolf <gwolf@debian.org>
License: BSD-3-Clause

Files: inc/Module/*
Copyright: 2002-2010, Adam Kennedy <adamk@cpan.org>
 2002-2010, Audrey Tang <autrijus@autrijus.org>
 2002-2010, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian GNU/Linux systems, the complete text of the Artistic License
 can be found in `/usr/share/common-licenses/Artistic'

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the name "Markdown" nor the names of its contributors may
   be used to endorse or promote products derived from this software
   without specific prior written permission.
 .
 This software is provided by the copyright holders and contributors "as
 is" and any express or implied warranties, including, but not limited
 to, the implied warranties of merchantability and fitness for a
 particular purpose are disclaimed. In no event shall the copyright owner
 or contributors be liable for any direct, indirect, incidental, special,
 exemplary, or consequential damages (including, but not limited to,
 procurement of substitute goods or services; loss of use, data, or
 profits; or business interruption) however caused and on any theory of
 liability, whether in contract, strict liability, or tort (including
 negligence or otherwise) arising in any way out of the use of this
 software, even if advised of the possibility of such damage.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License version 1 can be found in
 `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://metacpan.org/release/Text-Xslate
Upstream-Contact: Fuji, Goro (gfx) <gfuji@cpan.org>.
Upstream-Name: Text-Xslate

Files: *
Copyright: 2010-2013, Fuji, Goro (gfx) <gfuji@cpan.org>.
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2016, Nick Morrott <knowledgejunkie@gmail.com>
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2012, Adam Kennedy <adamk@cpan.org>
 2002-2012, Audrey Tang <autrijus@autrijus.org>
 2002-2012, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: inc/Module/Install/AuthorTests.pm
Copyright: 2008, Ricardo Signes <rjbs@cpan.org>
License: Artistic or GPL-1+
Comment: Author taken from http://cpansearch.perl.org/src/RJBS/Module-Install-AuthorTests-0.002/lib/Module/Install/AuthorTests.pm

Files: inc/Module/Install/TestTarget.pm
Copyright: 2010, Yuji Shimada <xaicron@cpan.org>
 2010, Goro Fuji (gfx) <gfuji@cpan.org>
 2010, Maki Daisuke (lestrrat)
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
 .
 Authors taken from http://cpansearch.perl.org/src/XAICRON/Module-Install-TestTarget-0.19/lib/Module/Install/TestTarget.pm
 and upstream Changes file.

Files: lib/Text/Xslate/PP/Const.pm
       lib/Text/Xslate/PP/Opcode.pm
       lib/Text/Xslate/PP/State.pm
       lib/Text/Xslate/PP/Type/Raw.pm
       lib/Text/Xslate/PP.pm
       tool/opcode_for_pp.PL
Copyright: 2010, Makamaka Hannyaharamitu (makamaka)
License: Artistic or GPL-1+

Files: lib/Text/Xslate/Runner.pm
       script/xslate
Copyright: 2010, Maki, Daisuke (lestrrat)
 2010, Fuji, Goro (gfx)
License: Artistic or GPL-1+
Comment: These files do not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Type::Tiny::XS
Upstream-Contact: https://rt.cpan.org/Dist/Display.html?Queue=Type-Tiny-XS
Source: https://metacpan.org/release/Type-Tiny-XS
 https://github.com/tobyink/p5-type-tiny-xs

Files: *
Copyright: 2012-2014, Toby Inkster <tobyink@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.
 .
 Comment:
 .
 Perl 5 is licensed under either the 'Artistic license' or the 'GNU
 General Public License' version 1 or later.

Files: t/40-regression/rt85911.t
 t/40-regression/rt86004.t
 t/40-regression/rt90096-2.t
 t/40-regression/rt92571-2.t
 t/40-regression/rt92571.t
 t/40-regression/rt92591.t
 t/40-regression/rt94196.t
Copyright: 2013-2014, Diab Jerius <djerius@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: lib/Type/Tiny/_HalfOp.pm
 t/20-unit/Type-Tiny-_HalfOp/overload-precedence.t
 t/40-regression/73f51e2d.pl
 t/40-regression/73f51e2d.t
Copyright: 2014, Graham Knop <haarg@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/20-unit/Types-Common-Numeric/basic.t
 t/20-unit/Types-Common-String/basic.t
 t/20-unit/Types-Common-String/coerce.t
 t/20-unit/Types-Common-String/unicode.t
Copyright: 2013-2014, Matt S Trout <mst@shadowcatsystems.co.uk>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/40-regression/rt86233.t
 t/40-regression/rt86239.t
Copyright: 2013-2014, Vyacheslav Matyukhin <mmcleric@cpan.org)
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/40-regression/rt98113.t
Copyright: 2014, Dagfinn Ilmari Mannsåker
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/30-integration/Class-InsideOut/basic.t
Copyright: 2013, David Golden
  2013, Toby Inkster <tobyink@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/30-integration/MooseX-Getopt/coercion.t
Copyright: 2014, Alexander Hartmaier <abraxxa@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/99-moose-std-types-test.t
Copyright: 2013-2014, Infinity Interactive, Inc.
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/40-regression/ttxs-gh1.t
Copyright: 2014, Jed Lund
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/40-regression/gh1.t
Copyright: 2013-2014, Richard Simões <rsimoes@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: t/40-regression/rt90096.t
Copyright: 2013-2014, Samuel Kaufman <skaufman@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 This is free software; you can redistribute it and/or modify it under
 the same terms as the Perl 5 programming language system itself.

Files: inc/Test/Fatal.pm
Copyright: Copyright 2013 Ricardo Signes
License: Artistic or GPL-1+
Comment:
 Mangled convenience copy of Test::Fatal.

Files: inc/Test/Requires.pm
Copyright: Copyright 2013 MATSUNO Tokuhiro
License: Artistic or GPL-1+
Comment:
 Mangled convenience copy of Test::Requires.

Files: inc/Try/Tiny.pm
Copyright: Copyright 2014 Yuval Kogman
License: Artistic or GPL-1+
Comment:
 Mangled convenience copy of Try::Tiny.

Files: CONTRIBUTING
Copyright: 2012-2014, Toby Inkster <tobyink@cpan.org>
License: Artistic or GPL-1+
Comment:
 License:
 .
 CONTRIBUTING is available under three different licences permitting its
 redistribution: the CC-BY-SA_UK-2.0 licence, plus the same licences as
 Perl itself, which is distributed under the GNU General Public Licence
 version 1, and the Artistic Licence.
 .
 This file is licensed under the Creative Commons Attribution-ShareAlike
 2.0 UK: England & Wales License. To view a copy of this license, visit
 <https://creativecommons.org/licenses/by-sa/2.0/uk/>.
 .
 This file is free software; you can redistribute it and/or modify it
 under the same terms as the Perl 5 programming language system itself.
 .
 Comment:
 .
 CC-BY-SA_UK-2.0 licensing skipped: Unneeded, and would be 2k chars.

Files: debian/*
Copyright: 2014-2015, Jonas Smedegaard <dr@jones.dk>,
 2018, Clément Hermann <nodens@nodens.org>
License: GPL-3+

License: Artistic
 Comment:
 .
 On Debian systems the 'Artistic License' is located in
 '/usr/share/common-licenses/Artistic'.

License: GPL-1+
 Comment:
 .
 On Debian systems the 'GNU General Public License' version 1 is located
 in '/usr/share/common-licenses/GPL-1'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 3, or (at your option) any
 later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 Comment:
 .
 On Debian systems the 'GNU General Public License' version 3 is located
 in '/usr/share/common-licenses/GPL-3'.
 .
 You should have received a copy of the 'GNU General Public License'
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

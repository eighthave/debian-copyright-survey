Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: licenseutils
Upstream-Contact: Ben Asselstine <bing@nym.hush.com>
Source: http://sv.gnu.org/p/licenseutils

Files: *
Copyright: 2011-2017 Ben Asselstine <bing@nym.hush.com>
License: GPL-3+

Files: bootstrap*
Copyright: 2003-2011 Free Software Foundation, Inc
License: GPL-3+

Files: configure.ac HACKING Makefile.am README styles.ac src/*.am tests/*
Copyright: 2013-2017 Ben Asselstine
License: Permissive

Files: INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013 Free Software Foundation, Inc.
License: Permissive

Files: doc/licenseutils.texi doc/licenseutils.info
Copyright: 2013-2017 Ben Asselstine
License: GFDL-NIV-1.3+

Files: lib/*
Copyright: 1987-2017 Free Software Foundation, Inc.
License: GPL-3+

Files: lib/alloca.c
Copyright: D A Gwyn
License: public-domain
 portable public-domain implementation -- D A Gwyn

Files: lib/m4/* m4/*
Copyright: 1992-2013 Free Software Foundation, Inc.
License: Other

Files: debian/*
Copyright: 2013-2018 Mattia Rizzolo <mattia@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GFDL-NIV-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
 Texts. A copy of the license is included in the ``GNU Free
 Documentation License'' file as part of this distribution.
 .
 On Debian systems, the complete text of the GNU Free Documentation License,
 version 1.3 can be found in "/usr/share/common-licenses/GFDL-1.3".

License: Other
 This file is free software; the Free Software Foundation
 gives unlimited permission to copy and/or distribute it,
 with or without modifications, as long as this notice is preserved.

License: Permissive
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.  This file is offered as-is,
 without any warranty.

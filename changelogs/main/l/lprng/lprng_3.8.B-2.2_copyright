Format: http://dep.debian.net/deps/dep5/
Upstream-Name: LPRng
Source: http://lprng.sourceforge.net/
License: GPL-2+ with OpenSSL exception

Files: *
Copyright: 1986-2001 Patrick Powell
License: GPL-2+ with OpenSSL exception or Artistic

Files: debian/*
Copyright: Craig Small <csmall@debian.org>
License: GPL-2+ with OpenSSL exception

Files: src/common/md5.c
Copyright: 1993 Colin Plumb
           2007 Bernhard R. Link
License: public-domain
 Not copyright is claimed. This code is in the public domain; do with it 
 what you wish.

Files: src/common/merge.c
Copyright: 1998-2003 Patrick Powell
           1992,1993 Regents of the University of California
License: GPL-2+ with OpenSSL exception or Artistic, and BSD
 
Files: src/common/proctitle.c
Copyright: 1998-2003 Patick Powell
           1983,1995-200 Eric P. Allman
           1988, 1993 Regents of Univrsity of California
License: GPL-2+ with OpenSSL exception or Artistic, and BSD

License: GPL-2+ with OpenSSL exception
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 In addition, as a special exception, the author of this
 program gives permission to link the code of its 
 release with the OpenSSL project's "OpenSSL" library (or
 with modified versions of it that use the same license as
 the "OpenSSL" library), and distribute the linked
 executables. You must obey the GNU General Public 
 License in all respects for all of the code used other 
 than "OpenSSL".  If you modify this file, you may extend
 this exception to your version of the file, but you are
 not obligated to do so.  If you do not wish to do so,
 delete this exception statement from your version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: Artistic
 This program is free software; you can redistribute it and/or modify it
 under the terms of the "Artistic License" which comes with Debian.
 .
 THIS PACKAGE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES
 OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 .
 On Debian systems, the complete text of the Artistic License
 can be found in "/usr/share/common-licenses/Artistic".

License: BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: linphone-desktop
Source: https://gitlab.linphone.org/BC/public/linphone-desktop/

Files: *
Copyright: 2010-2020 Belledonne Communications SARL.
License: GPL-3+

Files: debian/*
Copyright: 2020 Bernhard Schmidt <berni@debian.org>
License: GPL-3+

Files: linphone-app/assets/fonts/NotoSans-hinted/*
Copyright: 2010,2012-2020, Google Inc.
License: OFL-1.1

Files: linphone-app/src/utils/QExifImageHeader.*
Copyright: 2010 Nokia Corporation and/or its subsidiary(-ies)
License: LGPL-2.1+

Files: linphone-app/ui/modules/Konami/*
Copyright: 2017-2018 Ronan Abhamon (https://github.com/Wescoeur)
License: Expat

License: Expat
  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:
  .
  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".
 
License: OFL-1.1
 PREAMBLE
 .
 The goals of the Open Font License (OFL) are to stimulate
 worldwide development of collaborative font projects,
 to support the font creation efforts
 of academic and linguistic communities,
 and to provide a free and open framework
 in which fonts may be shared and improved
 in partnership with others.
 .
 The OFL allows the licensed fonts to be used, studied,
 modified and redistributed freely
 as long as they are not sold by themselves.
 The fonts, including any derivative works,
 can be bundled, embedded,
 redistributed and/or sold with any software
 provided that any reserved names are not used
 by derivative works.
 The fonts and derivatives, however,
 cannot be released under any other type of license.
 The requirement for fonts to remain under this license
 does not apply to any document
 created using the fonts or their derivatives.
 .
 DEFINITIONS
 .
 "Font Software" refers to the set of files
 released by the Copyright Holder(s) under this license
 and clearly marked as such.
 This may include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such
 after the copyright statement(s).
 .
 "Original Version" refers to the collection
 of Font Software components
 as distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative
 made by adding to, deleting, or substituting --
 in part or in whole --
 any of the components of the Original Version,
 by changing formats
 or by porting the Font Software to a new environment.
 .
 "Author" refers to any designer, engineer, programmer,
 technical writer or other person
 who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 .
 Permission is hereby granted, free of charge,
 to any person obtaining a copy of the Font Software,
 to use, study, copy, merge, embed, modify, redistribute,
 and sell modified and unmodified copies of the Font Software,
 subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
 in Original or Modified Versions,
 may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software
 may be bundled, redistributed and/or sold with any software,
 provided that each copy contains the above copyright notice
 and this license.
 These can be included either as stand-alone text files,
 human-readable headers or in the appropriate
 machine-readable metadata fields within text or binary files
 as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use
 the Reserved Font Name(s)
 unless explicit written permission is granted
 by the corresponding Copyright Holder.
 This restriction only applies to the primary font name
 as presented to the users.
 .
 4) The name(s) of the Copyright Holder(s)
 or the Author(s) of the Font Software
 shall not be used to promote,
 endorse or advertise any Modified Version,
 except to acknowledge the contribution(s)
 of the Copyright Holder(s) and the Author(s)
 or with their explicit written permission.
 .
 5) The Font Software,
 modified or unmodified, in part or in whole,
 must be distributed entirely under this license,
 and must not be distributed under any other license.
 The requirement for fonts to remain under this license
 does not apply to any document
 created using the Font Software.
 .
 TERMINATION
 .
 This license becomes null and void
 if any of the above conditions are not met.
 .
 DISCLAIMER
 .
 THE FONT SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 ANY WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT,
 INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF THE USE OR INABILITY TO USE
 THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.

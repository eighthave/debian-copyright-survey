This package was debianized by:

    Gürkan Sengün <gurkan@phys.ethz.ch> on Mon, 12 Nov 2007 15:32:32 +0200.

It was downloaded from:

    http://www.xs4all.nl/%7Esalsaman/lives/current/

The package is currently collaboratively maintained by the Debian
Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>

Upstream Authors:

    Gabriel Finch <salsaman@xs4all.nl>
    Marco de la Cruz <marco@reimeika.ca> (multi_encoder and plugins)
    Adrian Frischauf (code cleanup for gcc 2.xx)
    Niels Elburg (colourspace conversion and OSC)
    J@v2v.cc (help with the build system)
    Fukuchi Kentaro <fukuchi@megaui.net>
    Silvano "Kysucix" Galliani
    Denis "Jaromil" Rojo
    Tom Schouten
    Andraz Tori
    Carlo Prelz
    Jun Iio
    Matt Wright, Center for New Music and Audio Technologies - University of California, Berkeley
    Alexej Kolga <aleksej.kolga@gmail.com>
    Alexej Penkov <aleksej.penkov@gmail.com>

Copyright:

    Copyright (C) 2002-2010 Gabriel Finch
    Copyright (C) 2004-2005 Marco De la Cruz
    Copyright (C) 2001-2005 Fukuchi Kentaro
    Copyright (C) 1998 The Regents of the University of California (Regents)
    Copyright (C) 1995-1999, 2000, 2001, 2002 Free Software Foundation, Inc
    Copyright (C) 2009 Carles Carbonell Bernado
    Copyright (C) 2009 Gstreamer
    Copyright (C) 2009 GTK+ team

License:

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    The GNU General Public License which can be found,
    on Debian systems, at /usr/share/common-licenses/GPL-3.


    libOSC is licensed as follows:
    Permission to use, copy, modify, distribute, and distribute modified versions
    of this software and its documentation without fee and without a signed
    licensing agreement, is hereby granted, provided that the above copyright
    notice, this paragraph and the following two paragraphs appear in all copies,
    modifications, and distributions.

    IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
    SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING
    OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS
    BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
    HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
    MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

    The noia icons are licensed under LGPL
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    The GNU Lesser General Public License which can be found,
    on Debian systems, at /usr/share/common-licenses/LGPL-3

The Debian packaging is:

    Copyright (C) 2007-2009 Gürkan Sengün <gurkan@phys.ethz.ch>
    Copyright (C) 2009-2010 Harry Rickards <hrickards@l33tmyst.com>
    Copyright (C) 2010-2014 Alessio Treglia <alessio@debian.org>
    Copyright (C) 2010 Debian Multimedia Packages team <pkg-multimedia-maintainers@lists.alioth.debian.org>

and is licensed under the GPL version 3+,
see `/usr/share/common-licenses/GPL-3'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Limba
Upstream-Contact: Matthias Klumpp <matthias@tenstral.net>
Source: http://people.freedesktop.org/~mak/limba/

Files: *
Copyright: Matthias Klumpp <matthias@tenstral.net>
License: GPL-2+ and LGPL-2.1+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1+
 This library is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

Files: ./src/*
 ./tests/*
Copyright: 2008-2015, Matthias Klumpp <matthias@tenstral.net>
License: LGPL-2.1+

Files: ./src/li-utils-private.h
Copyright: 2009-2014, Matthias Klumpp <matthias@tenstral.net>
  2012, Colin Walters <walters@verbum.org>
License: LGPL-2.1+

Files: ./tools/*
Copyright: 2014-2015, Matthias Klumpp <matthias@tenstral.net>
License: GPL-2+

Files: ./tools/runapp/runapp.c
Copyright: 2012, Alexander Larsson <alexl@redhat.com>
  2014-2015, Matthias Klumpp <matthias@tenstral.net>
License: GPL-2+

Files: ./contrib/licompile/buildlist.c
Copyright: 2009-2010, Jan Niklas Hasse <jhasse@gmail.com>
  2010-2015, Matthias Klumpp <matthias@tenstral.net>
License: GPL-2+

Files: ./contrib/tools/checksym.c
Copyright: 2005, Mike Hearn <mike@plan99.net>
License: LGPL-2.1+

Files: ./contrib/tools/dump-elf-metadata.c
Copyright: 2005, Mike Hearn <mike@plan99.net>
  2005, Mike Hearn <mike@plan99.net>
License: LGPL-2.1+

Files: ./contrib/licompile/ctype.h
Copyright: 1991,92,93,95,96,97,98,99,2001, Free Software Foundation, Inc
License: LGPL-2.1+

Files: ./contrib/licompile/*
Copyright: 2003,2004,2005 Hongli Lai
           2005-2007, Mike Hearn  <mike@plan99.net>
           2009-2010, Jan Niklas Hasse  <jhasse@gmail.com>
           2010-2014, Matthias Klumpp <matthias@tenstral.net>
License: GPL-2+

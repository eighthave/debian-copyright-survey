Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: LordsAWar
Source: http://download-mirror.savannah.gnu.org/releases/lordsawar/

Files: *
Copyright:
 2002, 2003, Michael Bartl
 2002, Mark L. Amidon
 2002, Vibhu Rishi
 2003, 2004, 2005, 2006 Ulf Lorenz
 2003, Marek Publicewicz
 2004, David Barnsdale
 2004, David Sterba
 2004, John Farell
 2004, 2005, 2006 Andrea Paternesi
 2003, 2004, 2005, 2006, Ulf Lorenz
 2004, 2005, Bryan Duff
 2005, Josef Spillner
 2008, Janek Kozicki
 2008, Barry deFreese
 2007, Ole Laursen
 1994-2015, Free Software Foundation, Inc.
 2006, 2007, 2008, 2011, 2014, 2015 Ben Asselstine
License: GPL-3+
Comment:
 Please see the AUTHORS file for a complete list of contributors.

Files: src/ucompose.hpp
       src/boxcompose.h
Copyright: 2002, 03, 04 Ole Laursen <olau@hardworking.dk>
           2015, Ben Asselstine
License: LGPL-2.1+

Files: ltmain.sh
Copyright: 1996-2011, Free Software Foundation, Inc.
License: GPL-2+

Files: help/*.xml
Copyright: 2015, LordsAWar developers
License: GFDL-1.2+

Files: debian/*
Copyright: 2007-2010, Barry deFreese <bddebian@comcast.net>
           2008-2012, Alexander Reichle-Schmehl <tolimar@debian.org>
           2015-2016, Markus Koschany <apo@debian.org>
License: GPL-3+

License: GPL-3+
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-2+
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: GFDL-1.2+
 On Debian systems, the complete text of the GNU Free Documentation License 1.2
 can be found in `/usr/share/common-licenses/GFDL-1.2'.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Packaged-By: Stéphane Glondu <steph@glondu.net>
Packaged-Date: Fri, 27 Jun 2008 00:47:45 +0200
Source: http://ocsigen.org/lwt/
Upstream-Contact: The Ocsigen Team <dev@ocsigen.org>

Files: *
Copyright: 1999-2013, Jérôme Vouillon and others
License: LGPL-2.1-exception
 This program is released under the LGPL version 2.1 with the
 additional exemption that compiling, linking, and/or using OpenSSL
 is allowed.
 .
 As a special exception to the GNU Library General Public License,
 you may also link, statically or dynamically, a "work that uses the
 Library" with a publicly distributed version of the Library to
 produce an executable file containing portions of the Library, and
 distribute that executable file under terms of your choice, without
 any of the additional requirements listed in clause 6 of the GNU
 Library General Public License.  By "a publicly distributed version
 of the Library", we mean either the unmodified Library, or a
 modified version of the Library that is distributed under the
 conditions defined in clause 3 of the GNU Library General Public
 License.  This exception does not however invalidate any other
 reasons why the executable file might be covered by the GNU Library
 General Public License.
 .
 The complete text of the GNU Lesser General Public License can be
 found in `/usr/share/common-licenses/LGPL-2.1'.
 .
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

Files: examples/*
Copyright: 2009, Jérémie Dimino <jeremie@dimino.org>
License: BSD-C3
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.
   * Neither the name of Jeremie Dimino nor the names of his
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS''
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: src/lwt_monitor.ml* src/lwt_mvar.ml*
Copyright: 2009, Metaweb Technologies, Inc
License: BSD-C2
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the following
     disclaimer in the documentation and/or other materials provided
     with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY METAWEB TECHNOLOGIES ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL METAWEB TECHNOLOGIES BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2008-2013, Stéphane Glondu <steph@glondu.net>
License: LGPL-2.1
 The Debian packaging is released under the GNU Lesser General Public
 License version 2.1 or later. Its complete text can be found in
 `/usr/share/common-licenses/LGPL-2.1'.

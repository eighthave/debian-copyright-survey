Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Lookup
Source: http://openlab.jp/edict/lookup/

Files: *
Copyright: 2001-2004 Electronic Dictionary Open Laboratory
           1999-2002 Lookup Development Team <lookup@ring.gr.jp>
           1999 NISHIDA Keisuke <knishida@ring.aist.go.jp>
           1999 TSUCHIYA Masatoshi <tsuchiya@pine.kuee.kyoto-u.ac.jp>
           1999 KOREEDA Kazuyoshi <k_koreed@d2.dion.ne.jp>
           1985-1998 Free Software Foundation, Inc.
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2'.

Files: texi/lookup*.texi texi/lookup*.info*
Copyright: 1997-1999 Keisuke Nishida
           1999-2001 Lookup Development Team
License:
 Permission is granted to make and distribute verbatim copies of this
 manual provided the copyright notice and this permission notice are
 preserved on all copies.
 .
 Permission is granted to copy and distribute modified versions of this
 manual under the conditions for verbatim copying, provided that the
 entire resulting derived work is distributed under the terms of a
 permission notice identical to this one.
 .
 Permission is granted to copy and distribute translations of this manual
 into another language, under the above conditions for modified versions,
 except that this permission notice may be stated in a translation
 approved by the Free Software Foundation.

Files: NEWS
Copyright: 2001-2004 Electronic Dictionary Open Laboratory
           1999-2001 Lookup Development Team
           1998, 1999 NISHIDA Keisuke
License:
 Permission is granted to anyone to make or distribute verbatim copies
 of this document as received, in any medium, provided that the
 copyright notice and this permission notice are preserved, thus giving
 the recipient permission to redistribute in turn.
 .
 Permission is granted to distribute modified versions of this document,
 or of portions of it, under the above conditions, provided also that
 they carry prominent notices stating who last changed them.

Files: debian/*
Copyright: 2007-2014 Tatsuya Kinoshita <tats@debian.org>
           1998-2005 Ryuichi Arafune <arafune@debian.org>
License:
 The Debian packaging is distributed under the same conditions as the
 upstream.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: LM-Fit Development Team
Upstream-Contact: matt.newville@gmail.com
Source: http://lmfit.github.io/lmfit-py/
Files-Excluded: doc/sphinx/apigen.py doc/sphinx/github.py doc/sphinx/numpydoc

Files: debian/*
Copyright: © 2014, Picca Frédéric-Emmanuel <picca@debian.org>
License: Expat

Files: *
Copyright: 2012-2014 Matthew Newville, The University of Chicago
                     Till Stensitzki, Freie Universitat Berlin
License: Expat

Files: ./lmfit/ordereddict.py
Copyright: 2009, Raymond Hettinger
License: Expat

Files: ./lmfit/uncertainties/*
Copyright: 2010-2013, Eric O. LEBIGOT (EOL)
License: BSD-3

License: Expat
 Permission to use and redistribute the source code or binary forms of
 this software and its documentation, with or without modification is
 hereby granted provided that the above notice of copyright, these
 terms of use, and the disclaimer of warranty below appear in the
 source code and documentation, and that none of the names of above
 institutions or authors appear in advertising or endorsement of works
 derived from this software without specific prior written permission
 from all parties.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THIS
 SOFTWARE.

License: BSD-3
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
 .
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
 .
    * The names of its contributors may not be used to endorse or
      promote products derived from this software without specific
      prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

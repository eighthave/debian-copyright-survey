Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Nicholas Bamber <nicholas@periapt.co.uk>

Files: *
Copyright: 2012, 2015, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

Files: lib/Debian/LicenseReconcile/Filter/ChangeLog.pm
Copyright:
 2007-2010, Damyan Ivanov <dmn@debian.org>
 2011-2012, 2015, Nicholas Bamber <nicholas@periapt.co.uk>
License: Artistic or GPL-1+

Files: t/data/rltty.c t/data/flossy t/data/copyright
License: GPL-2+
Copyright: 1992-2005, Free Software Foundation, Inc

Files: t/data/example/debian/copyright
License: GPL-1+
Copyright: TBS
 
Files: t/data/copyright_extract2
Copyright:
 2008, MySQL AB,
 2008-2009, Sun Microsystems, Inc.
License: GPL-2

Files: t/data/copyright_extract1
Copyright:
 2007 Google Inc.
 2008, MySQL AB,
 2008-2009, Sun Microsystems, Inc.
License: GPL-2

Files:
 t/data/example/base
 t/data/example/base.h
Copyright: 2011-2012, Nicholas Bamber <nicholas@periapt.co.uk>
License: GPL-2

Files: t/data/nolicense.c t/data/tty.c
License: BSD-3-clause
Copyright: 1992-1993, The Regents of the University of California.

Files: debian/*
Copyright: 2012, 2015, Nicholas Bamber <nicholas@periapt.co.uk>
 2015, Damyan Ivanov <dmn@debian.org>
 2015. Dominique Dumont <dod@debian.org>
 2013, Salvatore Bonaccorso <carnil@debian.org>
 2013-2016, gregor herrmann <gregoa@debian.org>
 2013, Axel Beckert <abe@debian.org>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

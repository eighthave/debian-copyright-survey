Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: LSP-plugins
Source: https://lsp-plug.in
Upstream-Contact: Vladimir Sadovnikov  https://lsp-plug.in/?page=feedback

Files: *
Copyright: 2016-2020 Vladimir Sadovnikov
License: LGPL-3+

Files: debian/*
Copyright:
 2016-2018 Olivier Humbert <trebmuh@tuxfamily.org>
 2019 Erich Eickmeyer <erich@ericheickmeyer.com>
 2020 Dennis Braun <d_braun@kabelmail.de>
License: LGPL-3+

Files: include/3rdparty/*
Copyright: 2012, David Robillard <http://drobilla.net>
           2016, Robin Gareus <robin@gareus.org>
           2017, Linux Studio Plugins Project <lsp.plugin@gmail.com>
License: ISC

Files: include/3rdparty/ladspa/*
Copyright: 2000-2002, Richard W.E. Furse, Paul Barton-Davis
License: LGPL-2.1+

Files: include/dsp/arch/x86/sse3/graphics.h
Copyright: 2007, Julien Pommier
License: Zlib

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THIS SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: LGPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
Comment:
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/LGPL-3'.
 
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option)
 any later version.
 .
 This library is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this library; if not, see <https://www.gnu.org/licenses/>
 .
 Permission is hereby granted to use or copy this program under the terms of
 the GNU LGPL, provided that the Copyright, this License, and the Availability
 of the original version is retained on all copies. User documentation of any
 code that uses this code or any modified version of this code must cite the
 Copyright, this License, the Availability note, and "Used by permission."
 Permission to modify the code and to distribute modified code is granted,
 provided the Copyright, this License, and the Availability note are retained,
 and a notice that the code was modified is included.
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

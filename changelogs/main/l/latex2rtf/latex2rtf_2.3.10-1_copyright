Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: LaTeX2rtf
Upstream-Contact: Wilfried Hennings
Source: http://sourceforge.net/projects/latex2rtf
Comment: This package was debianized by Drake.Diedrich@anu.edu.au on
 Fri, 22 May 1998 17:19:13 +1000. The current maintainer is
 Chris Lawrence <lawrencc@debian.org>.
Copyright: (original copyright)
 Copyright (c) 1994 Andreas Granzer and Fernando Dorner
 Copyright (c) 1995 Friedrich Polzer and Gerhard Trisko
License: BSD-3
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 3. Neither the name of the Author nor the names of its contributors may be
    used to endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

Files: *
Copyright: Coypright (C) 1994-2012 The Free Software Foundation
           Copyright (C) Scott Prahl (since v1.8+)
           Copyright (C) Georg Lehner (since v1.7+)
License: GPL-2+

Files: debian/*
Copyright: Copyright (c) 1998 Drake Diedrich <Drake.Diedrich@anu.edu.au>
           Copyright (c) 2000-2008 Chris Lawrence <lawrencc@debian.org>
           Copyright (c) 2012,2013 Daniel Leidert <dleidert@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
Comment: On Debian GNU/Linux systems, the complete text of the GNU General
 Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

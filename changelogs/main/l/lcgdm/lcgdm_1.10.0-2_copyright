Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lcgdm
Upstream-Contact: Jean-Philippe Baud, CERN IT-GD/ITR
Source: http://svn.cern.ch/guest/lcgdm/lcg-dm

Files: *
Copyright: Copyright 1990-2011 CERN and Members of the EGEE Collaboration
 Members of the EGEE Collaboration
 ---------------------------------
 European Organization for Particle Physics (CERN)
 Institut für Graphische und Parallele Datenverarbeitung der
   Joh. Kepler Universität Linz (AT)
 Institut für Informatik der Universität Innsbruck (AT)
 CESNET, z.s.p.o. (CZ)
 Budapest University of Technology and Economics (HU)
 Eötvös Loránd University Budapest (HU)
 KFKI Research Institute for Particle and Nuclear Physics (HU)
 Magyar Tudományos Akadémia Számítástechnikai és Automatizálási
   Kutatóintézet (HU)
 Office for National Information and Infrastructure Development (HU)
 Akademickie Centrum Komputerowe CYFRONET akademii Górniczo-Hutniczej
   im. St. Staszica w Krakowie (PL)
 Warsaw University Interdisciplinary Centre for Mathematical and
   Computational Modelling (PL)
 Institute of Bioorganic Chemistry PAN, Poznan Supercomputing and
   Networking Center (PL)
 Ustav Informatiky, Slovenská Akadémia Vied (SK)
 Jožef Stefan Institute (SI)
 The Provost Fellows and Scholars of the College of the Holy and
   Undivided Trinity of Queen Elizabeth near Dublin (IE)
 Council for the Central Laboratory of the Research Councils (GB)
 The University of Edinburgh (GB)
 Particle Physics and Astronomy Research Council (GB)
 University College of London (GB)
 Commissariat l'Energie Atomique, Direction des Sciences de la Matière (FR)
 Compagnie Générale de Géophysique (FR)
 Centre National de la Recherche Scientifique (FR)
 CS Système d'Information Communication & Systèmes (FR)
 Centrale Recherche S.A. (FR)
 Deutsches Elektronen-Synchrotron (DE)
 Deutsches Klimarechenzentrum GmbH (DE)
 Fraunhofer-Gesellschaft zur Förderung der Angewandten Forschung e.V. (DE)
 Forschungszentrum Karlsruhe GmbH (DE)
 Gesellschaft für Schwerionenforschung GmbH (DE)
 DATAMAT S.p.A. (IT)
 Istituto Nazionale di Fisica Nucleare (IT)
 Trans-European Research and Networking Association (NL)
 Vrije Universiteit Brussel (BE)
 Faculty of Science University of Copenhagen (DK)
 University of Helsinki (FI)
 Foundation for Fundamental Research on Matter (NL)
 Stichting Academisch Rekencentrum Amsterdam (NL)
 Universiteit van Amsterdam (NL)
 University of Bergen (NO)
 Vetenskapsrådet, The Swedish Research Council (SE)
 Institute of High Energy Physics (RU)
 Institute of Mathematical Problems of Biology of Russian Academy of
   Sciences (RU)
 Institute of Theoretical and Experimental Physics (RU)
 Joint Institute for Nuclear Research (RU)
 Keldysh Institute of Applied Mathematics of Russian Academy of Sciences
   Moscow (RU)
 Petersburg Nuclear Physics Institute of Russian Academy of Sciences (RU)
 Russian Research Centre "Kurchatov Institute" (RU)
 Skobeltsyn Institute of Nuclear Physics of Moscow State University (RU)
 Central Lab. for Parallel Processing, Bulgarian Academy of Sciences (BG)
 University of Cyprus (CY)
 Greek Research and Technology Network (GR)
 Tel Aviv University (IL)
 National Institute for Research and Development in Informatics (RO)
 Laboratório de Instrumentação e Física Experimental de Partículas (PT)
 S.A.X. Centro de Supercomputación de Galicia (ES)
 Consejo Superior de Investigaciones Cientificas (ES)
 Institut de Física d'Altes Energies (ES)
 Instituto Nacional de Tecnica Aeroespacial (ES)
 Universidad Politécnica de Valencia (ES)
 University of Chicago (US)
 University of Southern California, Marina del Rey (US)
 The Board of Regents for the University of Wisconsin System (US)
 Royal Institute of Technology - Center for Parallel Computers (SE)
 Ente per le Nuove Tecnologie, l'Energia e l'Ambiente (IT)
 Università degli Studi della Calabria (IT)
 Università degli Studi di Lecce (IT)
 Università degli Studi di Napoli Federico II (IT)
 Delivery of Advanced Network Technology to Europe Limited (GB)
 Verein zur Förderung eines Deutschen Forschungsnetzes e.V. (DE)
 Consortium GARR (IT)
License: Apache-2.0

Files: common/Cgetopt.man common/Cgetopt.c win32/getopt.c win32/inet_netof.c
Copyright: Copyright 1983-1996 The Regents of the University of California
License: BSD-4-clause

Files: common/Cregexp.c
Copyright: Copyright (c) 1986 by University of Toronto
Comment: Written by Henry Spencer. Not derived from licensed software.
License: Custom
 Permission is granted to anyone to use this software for any
 purpose on any computer system, and to redistribute it freely,
 subject to the following restrictions:
 1. The author is not responsible for the consequences of use of
    this software, no matter how awful, even if they arise
    from defects in it.
 2. The origin of this software must not be misrepresented, either
    by explicit claim or by omission.
 3. Altered versions must be plainly marked as such, and must not
    be misrepresented as being the original software.

Files: socket/goputils/gop_synfo.c
Copyright: Copyright 1999 by Albert Cahalan; all rights reserved
License: LGPL-2+
 This file may be used subject to the terms and conditions of the
 GNU Library General Public License Version 2, or any later version
 at your option, as published by the Free Software Foundation.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Library General Public License for more details.
 .
 On Debian systems the complete text of the GNU Library General Public License
 version 2 can be found in the /usr/share/common-licenses/LGPL-2 file.

Files: test/File/CounterFile.pm
Copyright: Copyright (c) 1995-1998, 2002, 2003 Gisle Aas. All rights reserved.
License: GPL-1+ or Artistic
 This library is free software; you can redistribute it and/or
 modify it under the same terms as Perl itself.
 .
 On Debian systems the complete text of the GNU General Public License
 version 1 can be found in the /usr/share/common-licenses/GPL-1 file.
 .
 On Debian systems the complete text of the Artistic Licence can be found in
 the /usr/share/common-licenses/Artistic file.

Files: test/python/lfc/*
Copyright: Copyright 2006-2007 Etienne URBAH for the EGEE project
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details at
 http://www.gnu.org/licenses/gpl.html
 .
 On Debian systems the complete text of the GNU General Public License
 version 2 can be found in the /usr/share/common-licenses/GPL-2 file.

Files: debian/*
Copyright: © 2009-2018, Mattias Ellert <mattias.ellert@physics.uu.se>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 either express or implied. See the License for the specific
 language governing permissions and limitations under the
 License.
 .
 On Debian systems the full text of the Apache-2.0 license can be found in
 the /usr/share/common-licenses/Apache-2.0 file.

License: BSD-4-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
    This product includes software developed by the University of
    California, Berkeley and its contributors.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

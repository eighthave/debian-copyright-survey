Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libassa
Source: http://sourceforge.net/projects/libassa/files/

Files: *
Copyright: 1997-2012 Vladislav Grinchenko <vlg@users.sourceforge.net>
License: LGPL-2+

Files: assa/MemDump.h
Copyright: 1995 Vladislav Grinchenko <vlg@users.sourceforge.net>
License: LGPL-2+ or HPND

Files: assa/SigHandlersList.h tests/daytime.cpp tests/echoxdr_testc.cpp  tests/echoxdr_tests.cpp tests/logger_test.cpp tests/reactor_test.cpp tests/rt_test.cpp tests/timeval_test.cpp
Copyright: 1997, 1999, 2001-2005 Vladislav Grinchenko <vlg@users.sourceforge.net>
License: HPND

Files: doc/Doxyfile.in
Copyright: 2002 Bastiaan Bakker <bastiaan.bakker@lifeline.nl>
License: LGPL-2.1+

Files: examples/* tests/* utils/*
Copyright: 1999, 2001-2003, 2005-2007 Vladislav Grinchenko <vlg@users.sourceforge.net>
License: GPL-2+

Files: tests/ttcp_assa.cpp
Copyright: 1984, 1985 Terry Slattery
           1985       Mike Muuss
           1989, 1991 Silicon Graphics, Inc.
           1989       Steve Miller
           1999       Vladislav Grinchenko <vlg@users.sourceforge.net>
License: public-domain

Files: debian/*
Copyright: 2005                 Miriam Ruiz <little_miry@yahoo.es>
           2005-2008, 2012-2014 Eric Dorland <eric@debian.org>
           2009                 David Paleino <dapal@debian.org>
           2012                 Picca Frédéric-Emmanuel <picca@debian.org>
           2012                 Matthias Klose <doko@debian.org>
License: LGPL-2+

License: GPL-2+
 This package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

License: HPND
 Permission to use, copy, modify, and distribute this software
 and its documentation for any purpose and without fee is hereby
 granted, provided that the above copyright notice appear in all
 copies.  The author makes no representations about the suitability
 of this software for any purpose.  It is provided "as is" without
 express or implied warranty.

License: LGPL-2+
 This package is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2 can be found in `/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This package is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or (at
 your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: public-domain
 The file contains the notice:
 .
 Distribution Status -
 Public Domain. Distribution Unlimited.

Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Audio-CD
Upstream-Contact: Jano van Hemert <jano@vanhemert.co.uk>
Source: http://www.vanhemert.co.uk/disc-cover.html

Files: *
Copyright:
 versions <= 0.04, Doug MacEachern <dougjm@gmail.com>
 version 0.05,     Jano van Hemert <jano@vanhemert.co.uk>
License: Artistic or GPL-1+
Comment:
 The upstream source did not contain a copyright/license statement until 0.04
 and a problematic one as of 0.05; the authors relicensed the code to be
 "under the same terms as Perl itself" as documented below:
 .
 1) E-mail from Douglas MacEachern, author of Audio::CD <= 0.04:
 .
 #v+
 .
 Received: from mu-out-0910.google.com ([209.85.134.189])
         by master.debian.org with esmtp (Exim 4.63)
         (envelope-from <dougjm@gmail.com>)
         id 1Kc7ln-0005Bq-8J
         for gregoa@debian.org; Sun, 07 Sep 2008 00:04:47 +0000
 Received: by mu-out-0910.google.com with SMTP id i10so811095mue.7
         for <gregoa@debian.org>; Sat, 06 Sep 2008 17:04:40 -0700 (PDT)
 Received: by 10.103.206.12 with SMTP id i12mr8989876muq.33.1220738353977;
         Sat, 06 Sep 2008 14:59:13 -0700 (PDT)
 Received: by 10.103.160.18 with HTTP; Sat, 6 Sep 2008 14:59:13 -0700 (PDT)
 Message-ID: <9257c4510809061459x16a03155p5a427b440cd8646@mail.gmail.com>
 Date: Sat, 6 Sep 2008 14:59:13 -0700
 From: Douglas MacEachern <dougjm@gmail.com>
 To: gregor herrmann <gregoa@debian.org>
 Subject: Re: Audio::CD - license question
 .
 Hi Gregor,
 0.04 was the last version I released on CPAN, it actually had no license at
 all if you look at those sources:
 http://search.cpan.org/src/DOUGM/Audio-CD-0.04/.  It should have been
 "licensed for use under the same terms as Perl itself" like the majority of
 other modules on CPAN.  I don't maintain the module anymore, you have my
 permission to with 0.04 as you wish.  Afraid I'm not familar with the 0.05
 version.
 .
 Regards,
 -Doug
 .
 #v-
 .
 2) E-mail from Jano van Hemert, author of the changes in 0.05:
 .
 #v+
 .
 Received: from gamma.routhost.com ([67.15.187.34])
         by master.debian.org with esmtp (Exim 4.63)
         (envelope-from <jano@vanhemert.co.uk>)
         id 1KjC93-0006BC-NO
         for gregoa@debian.org; Fri, 26 Sep 2008 12:10:01 +0000
 Received: from staff-119.nesc.ed.ac.uk ([129.215.30.119]:53339)
         by gamma.routhost.com with esmtpa (Exim 4.69)
         (envelope-from <jano@vanhemert.co.uk>)
         id 1KjC8u-0007K8-5G
         for gregoa@debian.org; Fri, 26 Sep 2008 08:09:55 -0400
 Message-Id: <2B01869F-4D91-463E-B63A-F2E453618764@vanhemert.co.uk>
 From: Jano van Hemert <jano@vanhemert.co.uk>
 To: gregor herrmann <gregoa@debian.org>
 Subject: License of Audio::CD version 0.05
 Date: Fri, 26 Sep 2008 13:09:43 +0100
 .
 To whom it may concern,
 .
 I hereby licence Audio::CD version 0.05 under the same terms as Perl
 itself.
 .
 .
 Kind regards,
 .
 Dr Jano I. van Hemert
 Research Leader @ National e-Science Centre, School of Informatics,
 University of Edinburgh, UK
 .
 #v-

Files: cddb_lookup.{c h}
Copyright: (C) 1998-99 Tony Arcieri
License: GPL-2+

Files: debian/*
Copyright: 2002, Bart Warmerdam <bartw@debian.org>
 2003, Bart Warmerdam <bartw@xs4all.nl>
 2005, 2006, Jereme Corrado <jereme@zoion.net>
 2005, Andrew Pollock <apollock@debian.org>
 2008, 2009, 2010, gregor herrmann <gregoa@debian.org>
License: GPL-1+ or Artistic

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

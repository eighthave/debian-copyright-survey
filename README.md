
This is an automated survey of all the _debian/copyright_ files in Debian to see
what licenses are in use in Debian.

This downloads each package's copyright file once, then never again checks for
changes.  If somehow something happens to the original package, and the
copyright file is changed for a given version, then it can be updated here by
deleting it from the git repo, then running the script.

## JSON API

This produces a JSON file per section (main, contrib, and non-free) where the
keys are the license names and the values are a list of URLs pointing to the
package copyright files.

* https://eighthave.pages.debian.net/debian-copyright-survey/main.json
* https://eighthave.pages.debian.net/debian-copyright-survey/contrib.json
* https://eighthave.pages.debian.net/debian-copyright-survey/non-free.json

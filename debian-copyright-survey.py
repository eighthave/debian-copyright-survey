#!/usr/bin/env python3
#
# https://metadata.ftp-master.debian.org/changelogs/main/c/cmake/cmake_3.18.4-2_copyright

import glob
import json
import lzma
import os
import re
import requests
import requests_cache
import sys
import time
import yaml
from urllib.parse import urlparse

try:
    from yaml import CSafeLoader as SafeLoader
except ImportError:
    from yaml import SafeLoader


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return sorted(obj)
        return super().default(obj)


class Decoder(json.JSONDecoder):
    def __init__(self, **kwargs):
        json.JSONDecoder.__init__(self, **kwargs)
        self.parse_array = self.JSONArray
        # Use the python implemenation of the scanner
        self.scan_once = json.scanner.py_make_scanner(self)

    def JSONArray(self, s_and_end, scan_once, **kwargs):
        values, end = json.decoder.JSONArray(s_and_end, scan_once, **kwargs)
        return set(values), end


HOSTURL = 'https://metadata.ftp-master.debian.org/'
BASEURL = HOSTURL + 'changelogs'

LICENSE_PAT = re.compile(r'\nLicense: *(.+)')
COPYRIGHT_PAT = re.compile(
    r'(?:main|contrib|non-free)/[a-zA-Z0-9-]+/[a-zA-Z0-9-]+/[a-zA-Z0-9-]+_[a-zA-Z0-9.~+-]+_copyright'
)
INDEX_DIR_PAT = re.compile(r'alt="\[DIR\]"></td><td><a href="([a-zA-Z0-9-]+)/"')
INDEX_COPYRIGHT_PAT = re.compile(
    r'alt="\[TXT\]"></td><td><a href="([a-zA-Z0-9-]+_[a-zA-Z0-9.~+-]+_copyright)"'
)

SESSION = requests_cache.CachedSession(
    cache_name=('/tmp/' + os.path.basename(__file__))
)


def parse_copyright(licenses, url):
    try:
        r = SESSION.get(url)
        r.raise_for_status()

        parsed = urlparse(url)
        f = parsed.path[1:]
        d = os.path.dirname(f)
        os.makedirs(d, exist_ok=True)
        with open(f, 'wb') as fp:
            fp.write(r.content)

        for license in LICENSE_PAT.findall(r.text):
            license = license.strip()
            if license not in licenses:
                licenses[license] = set()
            licenses[license].add(url)
    except Exception as e:
        print(e)


def write_json(section, licenses):
    with open(section + '.json', 'w') as fp:
        json.dump(licenses, fp, sort_keys=True, indent=2, cls=Encoder)


timeout = 60 * 170  # seconds * minutes
start_timestamp = time.time()
alldata = dict()
urls_read = set()
for f in glob.glob('changelogs/*/*/*/*_copyright'):
    urls_read.add(HOSTURL + f)

filelist = 'filelist.yaml.xz'
if not os.path.exists(filelist):
    os.system('wget %s/%s' % (BASEURL, filelist))

sections = ('main', 'contrib', 'non-free')
if len(sys.argv) > 1:
    sections = sys.argv[1:]

i = 0
with lzma.open(filelist) as fp:
    for token in yaml.scan(fp, Loader=SafeLoader):
        if isinstance(token, yaml.ScalarToken):
            if COPYRIGHT_PAT.match(token.value):
                url = '%s/%s' % (BASEURL, token.value)
                if url in urls_read:
                    continue
                section = token.value[: token.value.index('/')]
                if section not in sections:
                    continue
                print(section, url)
                if section not in alldata:
                    alldata[section] = dict()
                parse_copyright(alldata[section], url)
                urls_read.add(url)
                i += 1
                if i % 1000 == 0:
                    for section in sections:
                        write_json(section, alldata.get(section, {}))

for section in sections:
    write_json(section, alldata.get(section, {}))
